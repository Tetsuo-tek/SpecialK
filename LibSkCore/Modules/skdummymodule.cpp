#include "skdummymodule.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkDummyModuleCfg, SkAbstractModuleCfg)
{
}

void SkDummyModuleCfg::allowedCfgKeysSetup()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkDummyModule, SkAbstractModule)
{}

void SkDummyModule::allowedExtendedCmdsSetup()
{}

void SkDummyModule::allowedExtendedCtrlSetup()
{}

bool SkDummyModule::onFlowChannelsSetup()
{
    return true;
}

bool SkDummyModule::onSetup()
{
    return true;
}

void SkDummyModule::onStart()
{
}

void SkDummyModule::onStop()
{
}

void SkDummyModule::onSetEnabled(bool enabled)
{
    if (enabled)
        eventLoop()->changeFastZone(5000);

    else
        eventLoop()->changeFastZone(500000);
}

void SkDummyModule::onFastTick()
{}

void SkDummyModule::onSlowTick()
{}

void SkDummyModule::onOneSecTick()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

