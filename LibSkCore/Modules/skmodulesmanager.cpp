#include "skmodulesmanager.h"
#include "Modules/skdummymodule.h"

#include <Core/App/skapp.h>

ConstructorImpl(SkModulesManager, SkThreadsPool)
{
    modsCheckInterval = 0;
    tickTimeAverageUS_HOLD = 0;
    highestSpeedOnModules = 1000000;

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);

    SignalSet(moduleThreadInstanced);
    SignalSet(moduleTickAvgChecked);
    SignalSet(appTickAvgChanged);
    SignalSet(appTickAdjusted);
}

bool SkModulesManager::init(SkArgsMap &modules, CStr *superToken, double modulesCheckInterval)
{
    modsCheckInterval = modulesCheckInterval;
    ObjectMessage("Instancing modules ..");

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = modules.iterator();

    while(itr->next())
    {
        SkModuleThread *mTh = new SkModuleThread(this);
        SkString &modName = itr->item().key();
        SkString mod_T = itr->item().value().toString();

        AssertKiller(modName.contains("."));

        mTh->init(modName.c_str(), mod_T.c_str(), superToken);
        add(mTh);

        moduleThreadInstanced(mTh);
    }

    delete itr;

    ObjectMessage("Initialized");
    return true;
}

void SkModulesManager::onStart()
{
    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkAttachMode::SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkQueued);

    ObjectMessage("Started");
}

void SkModulesManager::onStop()
{
    Detach(skApp->fastZone_SIG, pulse, this, fastTick);
    Detach(skApp->slowZone_SIG, pulse, this, slowTick);
    Detach(skApp->oneSecZone_SIG, pulse, this, oneSecTick);

    ObjectMessage("Stopped");
}

SlotImpl(SkModulesManager, fastTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(SkModulesManager, slowTick)
{
    SilentSlotArgsWarning();

    if (isEmpty())
        return;

    checkAndNotifyAppTick();
    checkAndNotifyModsTicks();

    if (avgChrono.stop() > modsCheckInterval)
    {
        autoAdjustAppTick();
        avgChrono.start();
    }
}

SlotImpl(SkModulesManager, oneSecTick)
{
    SilentSlotArgsWarning();
}

void SkModulesManager::checkAndNotifyAppTick()
{
    int64_t tickTimeAverageUS = eventLoop()->getLastPulseElapsedTimeAverage()*1000000;//((eventLoop()->getLastPulseElapsedTimeAverage()*1000000)+tickTimeAverageUS_HOLD)/2;

    if (tickTimeAverageUS == 0)
        return;

    int64_t consumedTimeAverageUS = eventLoop()->getConsumedTimeAverage()*1000000;

    /*SkString key(objectName());
    key.append(".tickTimeAvgUS");*/

    if (tickTimeAverageUS != tickTimeAverageUS_HOLD)
    {
        tickTimeAverageUS_HOLD = tickTimeAverageUS;

        SkVariantVector p;
        p << tickTimeAverageUS_HOLD;
        p << consumedTimeAverageUS;

        appTickAvgChanged(p);
    }

    /*else
        ObjectDebug(key << " is the same as holded: " << tickTimeAverageUS_HOLD << " us")*/
}

void SkModulesManager::checkAndNotifyModsTicks()
{
    if (modsTickTimeAverageUS_HOLD.count() != count())
        modsTickTimeAverageUS_HOLD.clear();

    if (modsTickTimeAverageUS_HOLD.isEmpty())
        for(uint64_t i=0; i<count(); i++)
            modsTickTimeAverageUS_HOLD << 0;

    for(uint64_t i=0; i<threads.count(); i++)
    {
        SkModuleThread *mTh = getModuleThread(i);

        if (mTh->isRunning())
        {
            SkEventLoop *thLoop = mTh->thEventLoop();

            int64_t avgTime = thLoop->getLastPulseElapsedTimeAverage()*1000000;//((thLoop->getLastPulseElapsedTimeAverage()*1000000)+modsTickTimeAverageUS_HOLD[i])/2;
            int64_t consumedAvgTime = thLoop->getConsumedTimeAverage()*1000000;

            SkString key(mTh->objectName());
            key.append(".tickTimeAvgUS");

            if (avgTime > 0)
            {
                if (thLoop->getTimerMode() != SkLoopTimerMode::SK_EVENTLOOP_WAITING
                        && thLoop->getTimerMode() != SkLoopTimerMode::SK_FREELOOP)
                {
                    if (avgTime != modsTickTimeAverageUS_HOLD[i])
                    {
                        modsTickTimeAverageUS_HOLD[i] = avgTime;

                        SkVariantVector p;
                        p << avgTime;
                        p << consumedAvgTime;

                        moduleTickAvgChecked(mTh, p);
                    }

                    /*else
                        ObjectDebug(key << " is the same as holded: " << modsTickTimeAverageUS_HOLD[i] << " us");*/

                    if (modsTickTimeAverageUS_HOLD[i] < highestSpeedOnModules)
                        highestSpeedOnModules = modsTickTimeAverageUS_HOLD[i];
                }

                else
                {
                    SkVariantVector p;
                    p << avgTime;
                    p << consumedAvgTime;

                    moduleTickAvgChecked(mTh, p);
                }
            }
        }
    }
}

void SkModulesManager::autoAdjustAppTick()
{
    if (highestSpeedOnModules == 0)
        return;

    /*if (highestSpeedOnModules/1.3 <= tickTimeAverageUS_HOLD)
    {
        ObjectWarning("Adapting application tick interval [" << tickTimeAverageUS_HOLD
                  << " us] to the fastest module [" << highestSpeedOnModules << " us] ..")

        tickTimeAverageUS_HOLD = highestSpeedOnModules;
        eventLoop()->changeFastZone(highestSpeedOnModules/1.2);

        SkVariantList p;
        p << static_cast<uint64_t>(eventLoop()->getFastInterval());
        appTickAdjusted();
    }*/
}

SkModuleThread *SkModulesManager::getModuleThread(uint64_t index)
{
    return dynamic_cast<SkModuleThread *>(get(index));
}

SkAbstractModuleCfg *SkModulesManager::getModuleCfg(uint64_t index)
{
    return dynamic_cast<SkModuleThread *>(get(index))->config();
}

SkAbstractModule *SkModulesManager::getModule(uint64_t index)
{
    return dynamic_cast<SkModuleThread *>(get(index))->module();
}

SkModuleThread *SkModulesManager::search(CStr *modName)
{
    for(uint64_t i=0; i<threads.count(); i++)
        if (SkString::compare(getModule(i)->objectName(), modName))
            return getModuleThread(i);

    return nullptr;
}
