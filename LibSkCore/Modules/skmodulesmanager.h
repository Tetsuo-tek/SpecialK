#ifndef SKMODULESMANAGER_H
#define SKMODULESMANAGER_H

#include "Core/Containers/sktreemap.h"

#include "Core/System/Thread/skthreadspool.h"
#include "skabstractmodule.h"
#include "Core/System/Network/FlowNetwork/skflowsync.h"

class RobotAbstractApplication;

class SkModulesManager extends SkThreadsPool
{
    public:
        Constructor(SkModulesManager, SkThreadsPool);

        bool init(SkArgsMap &modules, CStr *superToken, double modulesCheckInterval);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

        //MUST BE CONNECTED AS SkDirect AND IN THE SAME THREAD
        Signal(moduleThreadInstanced);
        Signal(moduleTickAvgChecked);
        Signal(appTickAvgChanged);
        Signal(appTickAdjusted);

        SkModuleThread *getModuleThread(uint64_t index);
        SkAbstractModuleCfg *getModuleCfg(uint64_t index);
        SkAbstractModule *getModule(uint64_t index);

        SkModuleThread *search(CStr *modName);

    private:

        int64_t tickTimeAverageUS_HOLD;
        int64_t highestSpeedOnModules;
        SkVector<int64_t> modsTickTimeAverageUS_HOLD;
        double modsCheckInterval;
        SkElapsedTime avgChrono;

        void onStart()      override;
        void onStop()       override;

        void checkAndNotifyAppTick();
        void checkAndNotifyModsTicks();
        void autoAdjustAppTick();
};

#endif // SKMODULESMANAGER_H
