#include "skabstractmodule.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skmodulesmanager.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Filesystem/skfile.h"
#include "Core/App/skapp.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractModuleCfgClose(SkObject *obj)
{
    SkAbstractModuleCfg *f = static_cast<SkAbstractModuleCfg *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkAbstractSocketClose()");

    f->clear();
}

AbstractConstructorImpl(SkAbstractModuleCfg, SkObject)
{
    addDtorCompanion(SkAbstractModuleCfgClose);
}

void SkAbstractModuleCfg::init()
{
    addParam("enableOnStart", "Enable on start", SkVariant_T::T_BOOL, false, true, "Enable module activity on start");

#if defined(ENABLE_HTTP)
    addParam("httpServiceEnabled", "Enable http service", SkVariant_T::T_BOOL, true, false, "Enable http service interface");
    addParam("httpDirEnabled", "Enable static www", T_STRING, true, false, "Enable static www contents");
    addParam("httpDirPath", "Directory for static www", T_STRING, true, "www", "Sets the RELATIVE path for static www contents");
    addParam("httpListenAddr", "Http listen address", T_STRING, true, "0.0.0.0", "Sets Http listen address");
    addParam("httpListenPort", "Flow listen tcp-port", T_UINT16, true, 0, "Sets Http listen tcp-port");
    addParam("httpMaxHeaderSize", "Max http header size", T_UINT64, true, ULong(100000), "Sets maximum bytes length accepted for http requests");
    addParam("httpMaxConnQueued", "Http max queued", T_UINT16, true, UShort(10), "Sets max connection on Http service accepting queue ");
    addParam("httpIdleTimeoutInterval", "IDLE timeout seconds", T_UINT16, true, UShort(20), "Sets max seconds interval to kill an idle http connection");
    addParam("httpSslEnabled", "Enables SSL", T_BOOL, true, false, "Enables/Disables the ssl protocol for Http service");
    addParam("httpSslCertFile", "Http SSL certificate", T_STRING, false, "ssl/selfsigned.crt", "Sets filepath for the ssl-certificate");
    addParam("httpSslKeyFile", "Http SSL key", T_STRING, false, "ssl/selfsigned.key", "Sets filepath for the ssl-key");
#endif

    allowedCfgKeysSetup();
    parameters.keys(allowedConfigKeysList);
}

void SkAbstractModuleCfg::setFlowServerPath(CStr *localPath)
{
    fsPath = localPath;
}

void SkAbstractModuleCfg::createDefaultConfig(SkArgsMap &defaultCfg)
{
    SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = parameters.iterator();

    while(itr->next())
    {
        SkWorkerParam *param = itr->item().value();
        defaultCfg[param->key()] = param->defaultValue();
    }

    delete itr;
}

bool SkAbstractModuleCfg::setup(SkArgsMap &cfg)
{
    SkString cfgStr;
    cfg.toString(cfgStr);

    SkString newHashCfg = ::stringHash(cfgStr);
    ObjectMessage("Grabbed configuration [md5 -> " << newHashCfg << "]: " << cfgStr);

    if (hashCfg == newHashCfg)
    {
        ObjectMessage("Configuration file is NOT changed: robot.json");
        return true;
    }

    hashCfg = newHashCfg;
    return load(cfg);
}

SkWorkerParam *SkAbstractModuleCfg::getParameter(CStr *key)
{
    if (!parameters.contains(key))
    {
        ObjectError("Requested key NOT found: " << key);
        return nullptr;
    }

    return parameters[key];
}

SkTreeMap<SkString, SkWorkerParam *> &SkAbstractModuleCfg::getParameters()
{
    return parameters;
}

CStr *SkAbstractModuleCfg::getFsPath()
{
    return fsPath.c_str();
}

CStr *SkAbstractModuleCfg::getHash()
{
    return hashCfg.c_str();
}

/*CStr *SkAbstractModuleCfg::getPairDbName()
{
    return cfgDbName.c_str();
}*/

SkWorkerParam *SkAbstractModuleCfg::addParam(CStr *key, CStr *label, SkVariant_T t, bool isRequired, const SkVariant &defaultValue, CStr *description)
{
    SkWorkerParam *param = new SkWorkerParam(key, label, t, isRequired, defaultValue, description);
    parameters[param->key()] = param;

    ObjectDebug("Config Key SETUP -> " << param->key()
              << " [isRequired: " << SkVariant::boolToString(param->isRequired())
              << ", type: " << SkVariant::variantTypeName(param->valueType())
              << ", default: " << param->defaultValue() << "]");

    return param;
}

bool SkAbstractModuleCfg::load(SkArgsMap &cfgMap)
{
    SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = parameters.iterator();

    //IT IS VERY INEFFICIENT!

    while(itr->next())
    {
        SkString &key = itr->item().key();
        SkWorkerParam *p = itr->item().value();

        //if (p->valueType() == T_BOOL) //MUST BE FALSE ON DEFAULT
            p->setValue(cfgMap[key]);

        if (p->value().isNull())
            p->setValue(p->defaultValue());

        if (p->isRequired() && p->value().isNull())
        {
            ObjectError("Required key NOT found: " << key);
            delete itr;
            return false;
        }
    }

    delete itr;

    return true;
}

void SkAbstractModuleCfg::clear()
{
    if (!parameters.isEmpty())
    {
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = parameters.iterator();

        while(itr->next())
            delete itr->item().value();

        delete itr;

        parameters.clear();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractModule, SkAbstractFlowSat)
{
    enabled = false;
    started = false;

    SlotSet(quit);
    SignalSet(quitted);

    SlotSet(changeRunningState);
    SlotSet(changeConfig);

    SignalSet(stateChanged);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractModule::init(SkAbstractModuleCfg *config, CStr *superToken)
{
    cfg = config;
    token = superToken;

    setLogin(objectName(), token.c_str());

    SkString addr("local:");
    addr.append(cfg->getFsPath());
    AssertKiller(!buildASync(addr.c_str()));

    SkString workingDirectory;

    if (skApp->existsGlobalPath("MODULES_WORKING_DIR"))
        workingDirectory = skApp->getGlobalPath("MODULES_WORKING_DIR");

    workingDirectory.append(SkFsUtils::adjustPathEndSeparator(objectName()));

    if (!SkFsUtils::exists(workingDirectory.c_str()))
        AssertKiller(!SkFsUtils::mkdir(workingDirectory.c_str()));

    setWorkingDir(workingDirectory.c_str());

    cfgFile = workingDir();
    cfgFile.append("module.json");

    onCheckInternals();

    if (!SkFsUtils::exists(cfgFile.c_str()))
    {
        SkArgsMap defaultCfg;
        cfg->createDefaultConfig(defaultCfg);

        if (!SkFsUtils::writeJSON(cfgFile.c_str(), defaultCfg, true))
            return false;

        cfg->setup(defaultCfg);
    }

    Attach(eventLoop()->changed_SIG, pulse, this, tickParamsChanged, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkDirect);

    onInit();

    ObjectMessage("Module INITIALIZED");
    return true;
}

SlotImpl(SkAbstractModule, quit)
{
    SilentSlotArgsWarning();

    setEnabled(false);

    stop();
    onQuit();
    closeASync();

    ObjectMessage("Module QUITTED");
    quitted();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractModule::start()
{
    ObjectMessage("Module SETUP [type: " << typeName() << "; hash: "<< cfg->getHash() << "] ...");

    if (!onSetup())
    {
        ObjectError("Configuration is NOT valid");
        return false;
    }

    ObjectMessage("Setup ACQUIRED [type: " << typeName() << "]");

#if defined(ENABLE_HTTP)
    SkFlowHttpServiceConfig httpCfg;

    httpCfg.serviceEnabled = cfg->getParameter("httpServiceEnabled")->value().toBool();

    if (httpCfg.serviceEnabled)
    {
        ObjectMessage("Setting up FsHttpService listen address ..");

        /*if (skApp->existsGlobalParameter("SHARED_ADDRESS"))
        {
            httpCfg.listenAddr = skApp->getGlobalParameter("SHARED_ADDRESS").toString();
            ObjectMessage("Robot setup has SHARED_ADDRESS configured, using: " << httpCfg.listenAddr);
        }

        else
        {
            httpCfg.listenAddr = cfg->getParameter("httpListenAddr")->value().toString();
            ObjectWarning("Robot setup has NOT public SHARED_ADDRESS, using: " << httpCfg.listenAddr);
        }*/

        httpCfg.listenAddr = cfg->getParameter("httpListenAddr")->value().toString();
        httpCfg.listenPort = cfg->getParameter("httpListenPort")->value().toUInt16();

        httpCfg.wwwDirEnabled = cfg->getParameter("httpDirEnabled")->value().toBool();
        httpCfg.wwwDir = workingDir();
        httpCfg.wwwDir.append(cfg->getParameter("httpDirPath")->value().toString());

        httpCfg.sslEnabled = cfg->getParameter("httpSslEnabled")->value().toBool();
        httpCfg.sslCertFile = workingDir();
        httpCfg.sslCertFile.append(cfg->getParameter("httpSslCertFile")->value().toString());
        httpCfg.sslKeyFile = workingDir();
        httpCfg.sslKeyFile.append(cfg->getParameter("httpSslKeyFile")->value().toString());
        httpCfg.maxConnQueued = cfg->getParameter("httpMaxConnQueued")->value().toUInt16();
        httpCfg.idleTimeoutInterval = cfg->getParameter("httpIdleTimeoutInterval")->value().toUInt16();
        httpCfg.maxHeaderSize = cfg->getParameter("httpMaxHeaderSize")->value().toUInt64();

        setupHttpService(httpCfg);
        buildHttpService();
    }
#endif

    addSingleControl("RunningState", T_BOOL, true, "Change module running-state", changeRunningState_SLOT);

    {
        SkWorkerCommand *cmd = addCommand("Setup", "Change module configuration", changeConfig_SLOT);
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = cfg->getParameters().iterator();

        while(itr->next())
        {
            SkWorkerParam *p = itr->item().value();
            SkWorkerParam *newParam = cmd->addParam(p->key(), p->label(), p->valueType(), p->isRequired(), p->defaultValue(), p->description());

            newParam->setValue(p->value());
            newParam->setMin(p->minValue());
            newParam->setMax(p->maxValue());
            newParam->setStep(p->stepValue());

            SkList<SkPair<SkString, SkVariant>> &opts = p->options();

            for(uint64_t i=0; i<opts.count(); i++)
                newParam->addOption(opts[i].key().c_str(), opts[i].value());
        }

        delete itr;
    }

    {
        SkBinaryTreeVisit<SkString, SkWorkerCommand *> *itr = commands().iterator();

        while(itr->next())
            updateCommand(itr->item().value());

        delete itr;
    }

    onStart();

    ObjectMessage("Module STARTED");

    started = true;

    /*if (config()->getParameter("enableOnStart")->value().toBool())
    {
        ObjectWarning("Enabling on start");
        requestToEnable(true);
    }*/

    return true;
}

void SkAbstractModule::stop()
{
    if (isEnabled())
        setEnabled(false);

#if defined(ENABLE_HTTP)
    closeHttp();
#endif

    clearCommands();
    //

    onStop();

    started = false;
    ObjectMessage("Module STOPPED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractModule, changeRunningState)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    t->setNoResponse();

    if (analyzeTransaction(t, v))
    {
        bool state = cmdMap["changeRunningState"].toBool();

        if (isEnabled() && state)
            t->setError("Module ALREADY enabled");

        else if (!isEnabled() && !state)
            t->setError("Module ALREADY disabled");

        else
            setEnabled(state);
    }

    else
        t->setError("Command NOT valid");

    t->goBack();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractModule::onChangeConfig()
{
    ObjectWarning("Re-SETUP");

    bool wasStarted = isStarted();

    if (wasStarted)
    {
        ObjectWarning("Re-Starting ..");

        stop();
        start();

        if (config()->getParameter("enableOnStart")->value().toBool())
        {
            ObjectWarning("Enabling on start");
            requestToEnable(true);
        }
    }
}

SlotImpl(SkAbstractModule, changeConfig)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
    {
        ObjectWarning("Configuration is CHANGED from command: changeConfig");

        cfg->setup(cmdMap);
        onChangeConfig();

        t->setResponse(cmdMap);
    }

    else
    {
        t->setError("Configuration NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

bool SkAbstractModule::isStarted()
{
    return started;
}

bool SkAbstractModule::isEnabled()
{
    return enabled;
}

SkAbstractModuleCfg *SkAbstractModule::config()
{
    return cfg;
}

bool SkAbstractModule::setEnabled(bool enable)
{
    if (enable)
    {
        if (enabled)
        {
            ObjectWarning("Module ALREADY enabled");
            return false;
        }

        enabled = enable;
        onSetEnabled(enabled);

        Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkDirect);
        Attach(eventLoop()->slowZone_SIG, pulse, this, slowTick, SkAttachMode::SkDirect);

        ObjectMessage("Module ENABLED");
    }

    else
    {
        if (!enabled)
        {
            ObjectWarning("Module ALREADY disabled");
            return false;
        }

        enabled = enable;
        onSetEnabled(enabled);

        Detach(eventLoop()->fastZone_SIG, pulse, this, fastTick);
        Detach(eventLoop()->slowZone_SIG, pulse, this, slowTick);

        ObjectMessage("Module DISABLED");
    }

    setVariable("enabled", enabled);
    stateChanged();
    return true;
}

void SkAbstractModule::requestToEnable(bool enable)
{
    SkArgsMap m;
    m["changeRunningState"] = enable;

    SkWorkerTransaction *t = new SkWorkerTransaction(this);
    t->setCommand(this, nullptr, nullptr, "changeRunningState", &m);
    t->invoke();

    if (enable)
        ObjectWarning("Request to enable");
    else
        ObjectWarning("Request to disable");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractModule::onCheckInternals()
{
    if (!SkFsUtils::exists(cfgFile.c_str()))
        return;

    SkVariant v;

    if (!SkFsUtils::readJSON(cfgFile.c_str(), v))
        return;

    SkString cfgStr;
    v.toJson(cfgStr);

    SkString newHashCfg = ::stringHash(cfgStr);

    if (!SkString::compare(newHashCfg.c_str(), cfg->getHash()))
    {
        ObjectWarning("Configuration file is CHANGED: " << cfgFile);

        SkArgsMap cmdMap;
        v.copyToMap(cmdMap);

        cfg->setup(cmdMap);
        onChangeConfig();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkModuleThread, SkThread)
{
    m = nullptr;
    cfg = nullptr;

    SlotSet(appFastTick);
    SlotSet(appOneSecTick);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkModuleThread::init(CStr *name, CStr *type, CStr *superToken)
{    
    mod_T = type;
    modName = name;
    token = superToken;

    SkString n(modName);
    n.append(".Th");
    setObjectName(n.c_str());

    ObjectWarning("Initializing module: " << modName << " [" << mod_T.c_str() << "]");
    AssertKiller(!isSuperClassOf("SkAbstractModule", mod_T.c_str()));

    SkString cfgTypeName(type);
    cfgTypeName.append("Cfg");

    AssertKiller(!isSuperClassOf("SkAbstractModuleCfg", cfgTypeName.c_str()));

    n = modName;
    n.append(".Cfg");

    cfg = newObject<SkAbstractModuleCfg *>(cfgTypeName.c_str());
    cfg->setObjectName(n.c_str());
    cfg->setParent(this);
    cfg->init();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractModuleCfg *SkModuleThread::config()
{
    return cfg;
}

SkAbstractModule *SkModuleThread::module()
{
    return m;
}

CStr *SkModuleThread::moduleName()
{
    return modName.c_str();
}

CStr *SkModuleThread::moduleType()
{
    return mod_T.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkModuleThread::customRunSetup()
{
    m = newObject<SkAbstractModule *>(mod_T.c_str());
    m->setObjectName(modName.c_str());

    AssertKiller(!m->init(cfg, token.c_str()));
    AssertKiller(!m->start());

    Attach(skApp->fastZone_SIG, pulse, this, appFastTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, appOneSecTick, SkQueued);

    return true;
}

void SkModuleThread::customClosing()
{
    Detach(skApp->fastZone_SIG, pulse, this, appFastTick);
    Detach(skApp->oneSecZone_SIG, pulse, this, appOneSecTick);

    m->destroyLater();
    m = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkModuleThread, appFastTick)
{
    SilentSlotArgsWarning();

    if (!isRunning() || !m)
        return;

    m->onAppFastTick();
}

SlotImpl(SkModuleThread, appOneSecTick)
{
    SilentSlotArgsWarning();

    if (!isRunning() || !m)
        return;

    m->onAppOneSecTick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //



