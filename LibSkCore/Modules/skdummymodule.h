#ifndef SKDUMMYMODULE_H
#define SKDUMMYMODULE_H

#include <Modules/skabstractmodule.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkDummyModuleCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkDummyModuleCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkDummyModule extends SkAbstractModule
{
    public:
        Constructor(SkDummyModule, SkAbstractModule);

    private:

        void allowedExtendedCmdsSetup()         override;
        void allowedExtendedCtrlSetup()         override;
        bool onFlowChannelsSetup()              override;

        bool onSetup()                          override;
        void onStart()                          override;
        void onStop()                           override;

        void onSetEnabled(bool enabled)         override;

        void onFastTick()                       override;
        void onSlowTick()                       override;
        void onOneSecTick()                     override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKDUMMYMODULE_H
