#ifndef SKABSTRACTMODULE_H
#define SKABSTRACTMODULE_H

#include "Core/System/Thread/skthread.h"
#include "Core/Containers/sktreemap.h"
#include "Core/Object/skabstractworkerobject.h"
#include "Core/System/Network/FlowNetwork/skflowasync.h"
#include "Core/System/Network/FlowNetwork/skabstractflowpublisher.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//CFG OBJECT LIVES IN THE MAIN THREAD
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractModuleCfg extends SkObject
{
    SkString hashCfg;

    SkTreeMap<SkString, SkWorkerParam *> parameters;
    SkStringList allowedConfigKeysList;
    SkString fsPath;

    public:
        void init();

        void setFlowServerPath(CStr *localPath);

        void createDefaultConfig(SkArgsMap &defaultCfg);
        bool setup(SkArgsMap &cfg);

        SkWorkerParam *getParameter(CStr *key);
        SkTreeMap<SkString, SkWorkerParam *> &getParameters();

        CStr *getFsPath();
        CStr *getHash();

        void clear();

    protected:
        AbstractConstructor(SkAbstractModuleCfg, SkObject);

        SkWorkerParam *addParam(CStr *key, CStr *label, SkVariant_T t, bool isRequired, const SkVariant &defaultValue, CStr *description);

        virtual void allowedCfgKeysSetup()                      = 0;

    private:
        bool load(SkArgsMap &cfgMap);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Network/FlowNetwork/skabstractflowsat.h"

class SkAbstractModule extends SkAbstractFlowSat
{
    bool enabled;
    bool started;

    SkString token;

    SkString cfgFile;
    SkAbstractModuleCfg *cfg;

    SkElapsedTime checkChrono;

    public:
        bool init(SkAbstractModuleCfg *config, CStr *superToken);
        Slot(quit);

        bool start();
        void stop();

        bool isStarted();
        bool isEnabled();

        SkAbstractModuleCfg *config();

        Slot(changeRunningState);
        Slot(changeConfig);

        Signal(stateChanged);
        Signal(quitted);

        virtual void onAppFastTick()                            {}
        virtual void onAppOneSecTick()                          {}

    private:
        void onChangeConfig();

    protected:
        AbstractConstructor(SkAbstractModule, SkAbstractFlowSat);

        bool setEnabled(bool enable);
        void requestToEnable(bool enable);

        void onCheckInternals()                                 override;

        virtual void onStart()                                  = 0;
        virtual void onStop()                                   = 0;

        //DEPRECATED
        virtual void allowedExtendedCmdsSetup()                 {}
        virtual void allowedExtendedCtrlSetup()                 {}
        virtual bool onFlowChannelsSetup()                      {return true;}
        //

        virtual void onSetEnabled(bool)                         = 0;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkModuleThread extends SkThread
{
    public:
        Constructor(SkModuleThread, SkThread);

        void init(CStr *name, CStr *type, CStr *superToken);

        SkAbstractModuleCfg *config();
        SkAbstractModule *module();
        CStr *moduleName();
        CStr *moduleType();

        // THESE SLOT IS ATTACHED TO skApp TICKs, BY DEFAULT
        // SO THEY CAN DIALOG WITH  OTHER THINGS ON THE MAIN TH
        Slot(appFastTick);
        Slot(appOneSecTick);

    private:
        SkAbstractModuleCfg *cfg;
        SkAbstractModule *m;
        SkString mod_T;
        SkString modName;
        SkString token;

        bool customRunSetup()                                   override;
        void customClosing()                                    override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


#endif // SKABSTRACTMODULE_H
