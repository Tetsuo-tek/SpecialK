TARGET = SkCore
TEMPLATE = lib

include(LibSkCore.pri)
include(module-src.pri)

DEFINES += SPECIALK_LIBRARY

DISTFILES += \
    Documentation/doc.conf \
    TODO

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkCore/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkCore.pri

headersDataFiles.path = /usr/local/include/LibSkCore/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles
