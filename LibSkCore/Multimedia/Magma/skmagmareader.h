#ifndef SKMAGMAREADER_H
#define SKMAGMAREADER_H

#include "skabstractmagmadecoder.h"

class SkMagmaReader extends SkObject
{
    public:
        Constructor(SkMagmaReader, SkObject);

        bool open(SkDataBuffer &header);
        void close();

        int tick(void *input, UShort sz);

        bool isOpen();

        //Decoders pointers life will be managed here
        SkAbstractMagmaDecoder **streams();
        ULong count();

    private:
        bool enabled;
        SkVector<SkAbstractMagmaDecoder *> mediaStreams;
        SkBufferDevice *reader;
};

#endif // SKMAGMAREADER_H
