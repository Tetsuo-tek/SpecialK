#include "skmagmareader.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_AUDIO)
#include "Multimedia/Audio/Encoders/Magma/skpsyadec.h"
#endif

#if defined(ENABLE_CV)
#include "Multimedia/Image/Encoders/Magma/skpsyvdec.h"
#endif

ConstructorImpl(SkMagmaReader, SkObject)
{
    enabled = false;

    reader = new SkBufferDevice(this);
}

bool SkMagmaReader::open(SkDataBuffer &header)
{
    reader->open(header, SkBufferDeviceMode::BVM_ONLYREAD);

    char cc[4];
    cc[0] = reader->readInt8();
    cc[1] = reader->readInt8();
    cc[2] = reader->readInt8();
    cc[3] = reader->readInt8();

    if (cc[0] != 'M' || cc[1] != 'G' || cc[2] != 'M' || cc[3] != ' ')
    {
        reader->close();
        return false;
    }

    uint8_t streamsCount = reader->readUInt8();

    if (streamsCount == 0)
    {
        reader->close();
        return false;
    }

    FlatMessage("Magma has " << (int) streamsCount << " streams inside");

    for(uint8_t i=0; i<streamsCount; i++)
    {
        uint8_t sz = reader->readUInt16();

        if (sz == 0)
        {
            reader->close();
            return false;
        }

        SkDataBuffer strHdr;

        if (!reader->read(&strHdr, sz))
        {
            reader->close();
            return false;
        }

        SkString n("PSY1_");
        n.concat((int) i);

        if (strHdr.at(0) == 'P' && strHdr.at(1) == 'S' && strHdr.at(2)== 'Y' && strHdr.at(3) == 'A')
        {
#if defined(ENABLE_AUDIO)
            SkPsyADec *dec = new SkPsyADec;
            dec->setObjectName(this, n.c_str());

            if (!dec->open(strHdr))
            {
                reader->close();
                close();
                return false;
            }

            mediaStreams << dec;
#else
            ObjectWarning("Ignoring PSYA (it requires to recompile application with ENABLE_AUDIO flag define)");
#endif
        }

        else if (strHdr.at(0) == 'P' && strHdr.at(1) == 'S' && strHdr.at(2)== 'Y' && strHdr.at(3) == 'V')
        {
#if defined(ENABLE_CV)
            SkPsyVDec *dec = new SkPsyVDec;
            dec->setObjectName(this, n.c_str());

            if (!dec->open(strHdr))
            {
                reader->close();
                close();
                return false;
            }

            mediaStreams << dec;
#else
            ObjectWarning("Ignoring PSYV (it requires to recompile application with ENABLE_CV flag define)");
#endif
        }

    }

    reader->close();

    if (mediaStreams.isEmpty())
    {
        FlatError("Magma container does NOT contain readable streams");
        return false;
    }

    enabled = true;
    return true;
}

void SkMagmaReader::close()
{
    for(ULong i=0; i<mediaStreams.count(); i++)
    {
        mediaStreams[i]->close();
        mediaStreams[i]->destroyLater();
    }

    mediaStreams.clear();
    enabled = false;
}

int SkMagmaReader::tick(void *input, UShort sz)
{
    SkDataBuffer data;
    data.setData(input, sz);

    reader->open(data, SkBufferDeviceMode::BVM_ONLYREAD);

    int streamID = reader->readUInt8();
    bool needDecompression = reader->readUInt8();
    ULong pckSz = reader->readUInt16();

    SkDataBuffer pck;

    if (needDecompression)
    {
        SkDataBuffer compressed;
        AssertKiller(!reader->read(&compressed, pckSz));
        compressed.decompress(pck);
    }

    else
        AssertKiller(!reader->read(&pck, pckSz));

    reader->close();

    streams()[streamID]->setData(pck);

    if (!streams()[streamID]->decode())
        return -1;

    return streamID;
}

bool SkMagmaReader::isOpen()
{
    return enabled;
}

SkAbstractMagmaDecoder **SkMagmaReader::streams()
{
    return mediaStreams.data();
}

ULong SkMagmaReader::count()
{
    return mediaStreams.count();
}
