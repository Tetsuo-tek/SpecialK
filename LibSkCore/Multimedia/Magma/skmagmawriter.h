#ifndef SKMAGMAWRITER_H
#define SKMAGMAWRITER_H

#include "skabstractmagmaencoder.h"

class SkBufferDevice;

class SkMagmaWriter extends SkObject
{
    public:
        Constructor(SkMagmaWriter, SkObject);

        //Encoders pointers life will NOT be managed here
        void addStream(SkAbstractMagmaEncoder *s);

        bool open(SkDataBuffer &header);
        void close();

        void tick(SkDataBuffer &output);

        SkAbstractMagmaEncoder **streams();

        //this is the shortest period between added streams
        float period();

        bool isOpen();

    private:
        SkVector<SkAbstractMagmaEncoder *> mediaStreams;
        SkBufferDevice *writer;
        float tickTimePeriod;
        bool enabled;
};

#endif // SKMAGMAWRITER_H
