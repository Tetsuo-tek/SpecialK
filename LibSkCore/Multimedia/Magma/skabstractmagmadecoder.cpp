#include "skabstractmagmadecoder.h"
#include "Core/System/skbufferdevice.h"

AbstractConstructorImpl(SkAbstractMagmaDecoder, SkObject)
{
    t = MGM_BLOB;
    pckID = 0;
    tickTimePeriod = 0.f;
    enabled = false;

    reader = new SkBufferDevice(this);
}

SkMagmaStreamType SkAbstractMagmaDecoder::decoderType()
{
    return t;
}

bool SkAbstractMagmaDecoder::isEnabled()
{
    return enabled;
}

float SkAbstractMagmaDecoder::period()
{
    return tickTimePeriod;
}

ULong SkAbstractMagmaDecoder::currentPckID()
{
    return pckID;
}
