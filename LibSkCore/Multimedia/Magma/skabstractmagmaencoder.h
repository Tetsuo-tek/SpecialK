#ifndef SKABSTRACTMAGMAENCODER_H
#define SKABSTRACTMAGMAENCODER_H

#include "Core/Containers/skdatabuffer.h"
#include "Core/Object/skobject.h"

enum SkMagmaStreamType
{
    MGM_BLOB,
    MGM_AUDIO,
    MGM_VIDEO,
    MGM_TXT
};

class SkBufferDevice;

class SkAbstractMagmaEncoder extends SkObject
{
    public:
        virtual bool open(SkDataBuffer &header)         = 0;
        virtual void close()                            = 0;

        virtual void setData(void *)                    = 0;
        virtual bool encode(SkDataBuffer &output)       = 0;

        SkMagmaStreamType encoderType();
        bool isEnabled();
        float period();

    protected:
        AbstractConstructor(SkAbstractMagmaEncoder, SkObject);

        SkMagmaStreamType t;
        float tickTimePeriod;

        SkBufferDevice *writer;
        bool enabled;
};

#endif // SKABSTRACTMAGMAENCODER_H
