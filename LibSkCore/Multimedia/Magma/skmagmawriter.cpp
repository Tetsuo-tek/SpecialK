#include "skmagmawriter.h"

#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

ConstructorImpl(SkMagmaWriter, SkObject)
{
    enabled = false;
    tickTimePeriod = 0.999999f;
    writer = new SkBufferDevice(this);
}

void SkMagmaWriter::addStream(SkAbstractMagmaEncoder *s)
{
    mediaStreams << s;
}

bool SkMagmaWriter::open(SkDataBuffer &header)
{
    if (enabled)
    {
        ObjectMessage("Writer is ALREADY open");
        return false;
    }

    writer->open(header, SkBufferDeviceMode::BVM_ONLYWRITE);

    writer->writeInt8('M');
    writer->writeInt8('G');
    writer->writeInt8('M');
    writer->writeInt8(' ');

    writer->writeUInt8(mediaStreams.count());

    for(ULong i=0; i<mediaStreams.count(); i++)
    {
        SkDataBuffer tempBuffer;

        if (!mediaStreams[i]->open(tempBuffer))
        {
            writer->close();
            header.clear();
            return false;
        }

        if (mediaStreams[i]->period() < tickTimePeriod)
            tickTimePeriod = mediaStreams[i]->period();

        writer->writeUInt16(tempBuffer.size());
        writer->write(tempBuffer.data(), tempBuffer.size());
    }

    writer->close();
    enabled = true;
    return true;
}

void SkMagmaWriter::close()
{
    if (!enabled)
    {
        ObjectMessage("Writer is NOT open");
        return;
    }

    for(ULong i=0; i<mediaStreams.count(); i++)
        mediaStreams[i]->close();

    enabled = false;
}

void SkMagmaWriter::tick(SkDataBuffer &output)
{
    writer->open(output, SkBufferDeviceMode::BVM_ONLYWRITE);

    for(ULong streamID=0; streamID<mediaStreams.count(); streamID++)
    {
        SkDataBuffer tempBuffer;
        SkAbstractMagmaEncoder *enc = mediaStreams[streamID];

        if (enc->isEnabled() && enc->encode(tempBuffer) && !tempBuffer.isEmpty())
        {
            ULong sz = tempBuffer.size();
            bool needCompression = (sz >= 256);

            writer->writeUInt8(streamID);
            writer->writeUInt8(needCompression);

            if (needCompression)
            {
                SkDataBuffer compressed;
                tempBuffer.compress(compressed);
                sz = compressed.size();

                writer->writeUInt16(sz);
                writer->write(compressed.data(), compressed.size());
            }

            else
            {
                writer->writeUInt16(sz);
                writer->write(tempBuffer.data(), tempBuffer.size());
            }
        }
    }

    writer->close();
}

SkAbstractMagmaEncoder **SkMagmaWriter::streams()
{
    return mediaStreams.data();
}

float SkMagmaWriter::period()
{
    return tickTimePeriod;
}

bool SkMagmaWriter::isOpen()
{
    return enabled;
}
