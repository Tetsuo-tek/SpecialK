#include "skabstractmagmaencoder.h"
#include "Core/System/skbufferdevice.h"

AbstractConstructorImpl(SkAbstractMagmaEncoder, SkObject)
{
    t = MGM_BLOB;
    tickTimePeriod = 0.f;
    enabled = false;

    writer = new SkBufferDevice(this);
}

SkMagmaStreamType SkAbstractMagmaEncoder::encoderType()
{
    return t;
}

bool SkAbstractMagmaEncoder::isEnabled()
{
    return enabled;
}

float SkAbstractMagmaEncoder::period()
{
    return tickTimePeriod;
}

