#ifndef SKABSTRACTMAGMADECODER_H
#define SKABSTRACTMAGMADECODER_H

#include "skabstractmagmaencoder.h"

class SkAbstractMagmaDecoder extends SkObject
{
    public:
        virtual bool open(SkDataBuffer &header)         = 0;
        virtual void close()                            = 0;

        virtual void setData(SkDataBuffer &input)       = 0;
        virtual bool decode()                           = 0;

        SkMagmaStreamType decoderType();
        bool isEnabled();
        float period();

        ULong currentPckID();

    protected:
        AbstractConstructor(SkAbstractMagmaDecoder, SkObject);

        SkMagmaStreamType t;
        ULong pckID;
        float tickTimePeriod;

        SkBufferDevice *reader;

        bool enabled;
};

#endif // SKABSTRACTMAGMADECODER_H
