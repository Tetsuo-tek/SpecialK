/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKATTACH_H
#define SKATTACH_H


#include "Core/Object/skflatobject.h"

/**
 * @brief The SkAttachMode enum is how a Signal is attached to a Slot
 */
enum SkAttachMode
{
    SkDirect,
    SkOneShotDirect,
    SkQueued,
    SkOneShotQueued
};

class SkObject;
class SkSignal;
class SkSlot;

class SPECIALK SkAttach extends SkFlatObject
{
    public:
        SkAttach(SkSignal *sig, SkObject *tgt, SkSlot *slt, SkAttachMode m);
        ~SkAttach();

        SkSignal *getSignal();
        SkSlot *getSlot();
        SkObject *getSlotOwner();
        SkAttachMode getMode();

        uint queuedTriggeringCount;

        /**
         * @brief Gets the cstring representation of an Attach-mode
         * @param mode the Attach-mode
         * @return the cstring representation
         */
        static CStr *getAttachModeString(SkAttachMode mode);

    private:
        SkSignal *parent;
        SkSlot *attached;
        SkObject *target;
        SkAttachMode mode;
};

#endif // SKATTACH_H
