/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSLOT_H
#define SKSLOT_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skpair.h"
#include "sksignal.h"

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkSlot extends SkFlatObject
{
    public:
        SkSlot(SkMethod *flatMeth=nullptr);
        ~SkSlot();

        void call(SkObject *owner, SkFlatObject *referer=nullptr, SkVariant **p=nullptr, uint64_t pCount=0);

        virtual bool isCanonical(){return true;}

        bool isValid();
        CStr *getName();
        SkClassType *getFlatType();
        SkMethod *getFlatMethod();

    protected:
        SkMethod *m;

        void setMethod(SkMethod *flatMeth);
        virtual void onCall(SkObject *, SkFlatObject *, SkVariant **, uint64_t){}
};

// // // // // // // // // // // // // // // // // // // // //

#endif // SKSLOT_H
