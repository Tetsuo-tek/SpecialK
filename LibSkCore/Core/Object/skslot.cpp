#include "skslot.h"
//#include "sksignal.h"
#include "Core/Containers/skvariant.h"

// // // // // // // // // // // // // // // // // // // // //

SkSlot::SkSlot(SkMethod *flatMeth)
{
    m = flatMeth;
}

SkSlot::~SkSlot()
{
}

void SkSlot::setMethod(SkMethod *flatMeth)
{
    m = flatMeth;
}

void SkSlot::call(SkObject *owner, SkFlatObject *referer, SkVariant **p, uint64_t pCount)
{
    callSlotMethod(owner, referer, *m, p, pCount);
    onCall(owner, referer, p, pCount);
}

bool SkSlot::isValid()
{
    return (m != nullptr);
}

CStr *SkSlot::getName()
{
    return m->name;
}

SkClassType *SkSlot::getFlatType()
{
    return m->t;
}

SkMethod *SkSlot::getFlatMethod()
{
    return m;
}

// // // // // // // // // // // // // // // // // // // // //
