#include "skattach.h"
#include "skobject.h"

SkAttach::SkAttach(SkSignal *sig, SkObject *tgt, SkSlot *slt, SkAttachMode m)
{
    parent = sig;
    target = tgt;
    attached = slt;
    mode = m;

    queuedTriggeringCount = 0;

    ObjectPlusDebug_EXT(parent->getOwner(), "New Attach for Signal: " << parent->typeName() << "::" << parent->getName());
}

SkAttach::~SkAttach()
{
    ObjectPlusDebug_EXT(parent->getOwner(), "Destroyed Attach for Signal: " << parent->typeName() << "::" << parent->getName());
}

SkSignal *SkAttach::getSignal()
{
    return parent;
}

SkSlot *SkAttach::getSlot()
{
    return attached;
}

SkObject *SkAttach::getSlotOwner()
{
    return target;
}

SkAttachMode SkAttach::getMode()
{
    return mode;
}

CStr *SkAttach::getAttachModeString(SkAttachMode mode)
{
    if (mode == SkAttachMode::SkDirect)
        return "SkDirect";

    else if (mode == SkAttachMode::SkQueued)
        return "SkQueued";

    else if (mode == SkAttachMode::SkOneShotDirect)
        return "SkOneShotDirect";

    return "SkOneShotQueued";
}
