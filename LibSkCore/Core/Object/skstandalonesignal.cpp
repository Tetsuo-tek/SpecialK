#include "skstandalonesignal.h"

ConstructorImpl(SkStandaloneSignal, SkObject)
{
    SlotSet(trigger);
}

SlotImpl(SkStandaloneSignal, trigger)
{
    SilentSlotArgsWarning();

    /*va_list vl;
    va_start(vl, o);
    SkVariantVector p;
    p << Arg_AbstractVariadic;
    pulse(this, p);
    va_end(vl);*/

    pulse(this);
}
