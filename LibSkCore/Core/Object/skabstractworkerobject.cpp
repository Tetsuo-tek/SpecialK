#include "skabstractworkerobject.h"
#include "Core/App/skeventloop.h"
#include "Core/System/skabstractdevice.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkWorkerParam::SkWorkerParam()
{
    t = T_NULL;
}

SkWorkerParam::SkWorkerParam(CStr *key, CStr *label, SkVariant_T type, bool isRequired, const SkVariant &defaultValue, CStr *description)
{
    k = key;
    lbl = label;
    t = type;
    required = isRequired;
    dfltVal = defaultValue;
    desc = description;
}

void SkWorkerParam::setValue(const SkVariant &val)
{
    v = val;
}

void SkWorkerParam::setMin(const SkVariant &min)
{
    minVal = min;
}

void SkWorkerParam::setMax(const SkVariant &max)
{
    maxVal = max;
}

void SkWorkerParam::setStep(const SkVariant &step)
{
    stepVal = step;
}

void SkWorkerParam::setDefault(const SkVariant &dflt)
{
    dfltVal = dflt;
}

void SkWorkerParam::addOption(CStr *lbl, const SkVariant &value)
{
    opts << SkPair<SkString, SkVariant>(lbl, value);
}

CStr *SkWorkerParam::label()
{
    return lbl.c_str();
}

CStr *SkWorkerParam::key()
{
    return k.c_str();
}

SkVariant &SkWorkerParam::value()
{
    return v;
}

CStr *SkWorkerParam::description()
{
    return desc.c_str();
}

bool SkWorkerParam::isRequired()
{
    return required;
}

SkVariant_T SkWorkerParam::valueType()
{
    return t;
}

SkVariant &SkWorkerParam::defaultValue()
{
    return dfltVal;
}

SkVariant &SkWorkerParam::minValue()
{
    return minVal;
}

SkVariant &SkWorkerParam::maxValue()
{
    return maxVal;
}

SkVariant &SkWorkerParam::stepValue()
{
    return stepVal;
}

bool SkWorkerParam::hasOptions()
{
    return !opts.isEmpty();
}

SkList<SkPair<SkString, SkVariant>> &SkWorkerParam::options()
{
    return opts;
}

bool SkWorkerParam::toMap(SkArgsMap &m)
{
    if (k.isEmpty())
    {
        FlatError("Requesting a aparameter NOT initialized [k = '']");
        return false;
    }

    m["lbl"] = lbl;
    m["key"] = k;
    m["isRequired"] = required;
    m["type"] = t;
    m["desc"] = desc;

    if (!v.isNull())
        m["value"] = v;

    if (!dfltVal.isNull())
        m["dfltVal"] = dfltVal;

    if (!minVal.isNull())
        m["minVal"] = minVal;

    if (!maxVal.isNull())
        m["maxVal"] = maxVal;

    if (!stepVal.isNull())
        m["stepVal"] = stepVal;

    SkVariantList mOpts;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = opts.iterator();

    while(itr->next())
    {
        SkArgsMap pMap;
        pMap["lbl"] = itr->item().key();
        pMap["val"] = itr->item().value();
        mOpts << pMap;
    }

    delete itr;

    m["options"] = mOpts;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkWorkerCommand::SkWorkerCommand()
{
    ctrlMode = false;
    slt = nullptr;
}

void SkWorkerCommand::set(CStr *label, CStr *description, SkSlot *slot, bool isControl)
{
    lbl = label;
    desc = description;
    slt = slot;
    ctrlMode = isControl;
}

SkWorkerParam *SkWorkerCommand::addParam(CStr *key, CStr *label, SkVariant_T t, bool isRequired, const SkVariant &defaultValue, CStr *description)
{
    SkWorkerParam *p = new SkWorkerParam(key, label, t, isRequired, defaultValue, description);
    params[p->key()] = p;

    FlatDebug("New parameter -> " << p->key()
                << " [isRequired: " << SkVariant::boolToString(p->isRequired())
                << ", label: " << p->label()
                << ", type: " << SkVariant::variantTypeName(p->valueType())
                << ", default: " << p->defaultValue()<< "]");

    return p;
}

bool SkWorkerCommand::load(SkArgsMap &m)
{
    SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = params.iterator();

    //IT IS VERY INEFFICIENT!

    while(itr->next())
    {
        SkString &key = itr->item().key();

        if (!loadParameter(itr->item().value(), m[key]))
        {
            delete itr;
            return false;
        }
    }

    delete itr;
    return true;
}

bool SkWorkerCommand::contains(CStr *key)
{
    return params.contains(key);
}

SkVariant &SkWorkerCommand::value(CStr *key)
{
    if (!params.contains(key))
    {
        FlatError("Requested key NOT found: " << key);
        return nv;
    }

    return params[key]->value();
}

SkTreeMap<SkString, SkWorkerParam *> &SkWorkerCommand::parameters()
{
    return params;
}

void SkWorkerCommand::clear()
{
    if (!params.isEmpty())
    {
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = params.iterator();

        while(itr->next())
            delete itr->item().value();
        delete itr;

        params.clear();
    }
}

bool SkWorkerCommand::loadParameter(SkWorkerParam *p, SkVariant &val)
{
    if (val.isNull())
        p->setValue(p->defaultValue());

    else
        p->setValue(val);

    if (p->isRequired() && p->value().isNull())
    {
        FlatError("Required key NOT found: " << p->key());
        return false;
    }

    return true;
}

CStr *SkWorkerCommand::command()
{
    return slt->getName();
}

CStr *SkWorkerCommand::label()
{
    return lbl.c_str();
}

CStr *SkWorkerCommand::description()
{
    return desc.c_str();
}

SkSlot *SkWorkerCommand::slot()
{
    return slt;
}

bool SkWorkerCommand::isControl()
{
    return ctrlMode;
}

bool SkWorkerCommand::toMap(SkArgsMap &m)
{
    if (!slt)
    {
        FlatError("Requesting a command NOT initialized [*slt = NULL]");
        return false;
    }

    m["lbl"] = lbl;
    m["isControl"] = ctrlMode;
    m["description"] = desc;
    m["slot"] = slt->getName();

    SkVariantList mParams;

    SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = params.iterator();

    while(itr->next())
    {
        SkArgsMap pMap;
        itr->item().value()->toMap(pMap);
        mParams << pMap;
    }

    delete itr;

    m["params"] = mParams;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkWorkerTransaction, SkObject)
{
    abstractCommand = nullptr;
    wrk = nullptr;
    dev = nullptr;
    retSlot = nullptr;
    deviceOpen = false;
    successfull = true;
}

void SkWorkerTransaction::setCommand(SkAbstractWorkerObject *worker, SkAbstractDevice *commander, SkSlot *returningSlot, CStr *command, SkArgsMap *val)
{
    wrk = worker;
    dev = commander;
    retSlot = returningSlot;
    cmd = command;

    if (val)
        cmdArgument = *val;

    SkString s;
    cmdArgument.toString(s);

    SkString objName("Cmd[");
    objName.append(wrk->objectName());
    objName.append("::");
    objName.append(cmd);
    objName.append("]");

    if (dev)
    {
        setObjectName(dev, objName.c_str());

        deviceOpen = dev->isOpen();

        if (!deviceOpen)
        {
            ObjectError("Commander is NOT active, cannot execute Command transaction [args: " << s << "]");
            return;
        }

        Attach(dev, closed, this, deactivate, SkDirect);
        ObjectMessage("Started DEVICE Command transaction [args: " << s << "]");
    }

    else
    {
        setObjectName(parent(), objName.c_str());
        ObjectMessage("Started LOCAL Command transaction: [args: " << s << "]");
    }
}

void SkWorkerTransaction::setResponse(SkArgsMap &response)
{
    rsp = response;
}

void SkWorkerTransaction::setBinaryResponse(SkDataBuffer &response)
{
    rspBin = response;
}

void SkWorkerTransaction::setNoResponse()
{
    rsp.clear();
    rspBin.clear();
}

void SkWorkerTransaction::setError(CStr *errorString)
{
    e = errorString;
    successfull = false;
}

void SkWorkerTransaction::invoke()
{
    wrk->executeTransaction(this);
}

void SkWorkerTransaction::goBack()
{
    SkString s;
    cmdArgument.toString(s);

    if (!retSlot)
    {
        ObjectMessage("EVALUATED command transaction: " << wrk->objectName() << "::" << cmd << " [args: " << s << "; without returning Slot]");
        destroyLater();
        return;
    }

    ObjectMessage("EVALUATED command transaction: " << wrk->objectName() << "::" << cmd << " [args: " << s << "; with returning Slot: " << parent()->objectName() << "::" << retSlot->getName() << "]");
    parent()->eventLoop()->invokeSlot(retSlot, parent(), this);
}

SkAbstractWorkerObject *SkWorkerTransaction::worker()
{
    return wrk;
}

CStr *SkWorkerTransaction::command()
{
    return cmd.c_str();
}

SkWorkerCommand *SkWorkerTransaction::commandSlot()
{
    return abstractCommand;
}

SkArgsMap &SkWorkerTransaction::argument()
{
    return cmdArgument;
}

bool SkWorkerTransaction::isStillAlive()
{
    return deviceOpen;
}

SkAbstractDevice *SkWorkerTransaction::commander()
{
    return dev;
}

bool SkWorkerTransaction::hasErrors()
{
    return !successfull;
}

SkArgsMap &SkWorkerTransaction::response()
{
    return rsp;
}

SkDataBuffer &SkWorkerTransaction::responseBin()
{
    return rspBin;
}

CStr *SkWorkerTransaction::error()
{
    return e.c_str();
}

SlotImpl(SkWorkerTransaction, deactivate)
{
    SilentSlotArgsWarning();
    deviceOpen = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractWorkerObject, SkObject)
{

}

SkWorkerCommand *SkAbstractWorkerObject::addCommand(CStr *label, CStr *description, SkSlot *slot)
{
    SkWorkerCommand *modCmd = new SkWorkerCommand;
    modCmd->setObjectName(this, slot->getName());
    modCmd->set(label, description, slot);
    return addCommand(modCmd);
}

SkWorkerCommand *SkAbstractWorkerObject::addCommand(SkWorkerCommand *modCmd)
{
    modCmds[modCmd->command()] = modCmd;
    ObjectMessage("ADDED module COMMAND: " << modCmd->command() << " -> " << modCmd->description());

    return modCmd;
}

SkWorkerParam *SkAbstractWorkerObject::addSingleControl(CStr *label, SkVariant_T t, const SkVariant &defaultValue, CStr *description, SkSlot *slot)
{
    SkWorkerCommand *modCtrl = new SkWorkerCommand;
    modCtrl->setObjectName(this, slot->getName());
    modCtrl->set(label, description, slot, true);
    SkWorkerParam *p = modCtrl->addParam(slot->getName(), label, t, true, defaultValue, description);

    modCmds[modCtrl->command()] = modCtrl;
    ObjectMessage("ADDED module Sigle-CONTROL: " << modCtrl->command() << " -> " << modCtrl->description());

    return p;
}

SkWorkerCommand *SkAbstractWorkerObject::addMultiControl(CStr *label, CStr *description, SkSlot *slot)
{
    SkWorkerCommand *modCtrl = new SkWorkerCommand;
    modCtrl->setObjectName(this, slot->getName());
    modCtrl->set(label, description, slot, true);

    modCmds[modCtrl->command()] = modCtrl;
    ObjectMessage("ADDED module Multi-CONTROL: " << modCtrl->command() << " -> " << modCtrl->description());

    return modCtrl;
}

void SkAbstractWorkerObject::executeTransaction(SkWorkerTransaction *t)
{
    SkString s;
    t->argument().toString(s);

    if (modCmds.contains(t->command()))
    {
        t->abstractCommand = modCmds[t->command()];

        SkVariantVector p;
        p << t->argument();

        ObjectMessage("INVOKED Command transaction: " << t->command() << " [args: " << s << "]");
        eventLoop()->invokeSlot(t->abstractCommand->slot(), this, t, p);
    }

    else
    {
        ObjectError("Command transaction UNKNOWN: " << t->command() << " [args: " << s << "]");
        t->setError("Proposed command transaction FAILED due to COMMAND-UNKNOWN");
        t->goBack();
    }
}

bool SkAbstractWorkerObject::analyzeTransaction(SkWorkerTransaction *t, SkVariant &v)
{
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    SkWorkerCommand *cmd = modCmds[t->command()];

    if (!cmd->load(cmdMap))
        return false;

    updateCommand(cmd);
    return true;
}

void SkAbstractWorkerObject::clearCommands()
{
    SkBinaryTreeVisit<SkString, SkWorkerCommand *> *itr = modCmds.iterator();

    while(itr->next())
    {
        SkWorkerCommand *cmd = itr->item().value();
        cmd->clear();
        delete cmd;
    }

    delete itr;

    modCmds.clear();
}

SkTreeMap<SkString, SkWorkerCommand *> &SkAbstractWorkerObject::commands()
{
    return modCmds;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
