/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSIGNAL_H
#define SKSIGNAL_H

#include "skattach.h"
#include "Core/Containers/skvariant.h"

#if defined(ENABLE_QTAPP)
    #include "Core/Object/qtstandalonesignal.h"
#endif

class SkSlot;
class SkObject;
class SkSignal;

class SPECIALK SkSignal extends SkFlatObject
{
    public:
        SkSignal();
        ~SkSignal();

        void setup(SkObject *owner, CStr *name);

        void setParameters(SkVariantVector &p);

        SkVariantVector &getParameters();

        bool attach(SkObject *target, SkSlot *slotMeth, SkAttachMode m, CStr *caller);

#if defined(ENABLE_QTAPP)
        bool attach(QObject *target, CStr *slotName, Qt::ConnectionType m, CStr *caller);
#endif

        bool detach(SkObject *target, SkSlot *slotMeth, CStr *caller);
        void detach(SkObject *target);

#if defined(ENABLE_QTAPP)
        bool detach(QObject *target, CStr *slotName, CStr *caller);
#endif

        void detachAll();

        void trigger(SkFlatObject *referer=nullptr);

        SkObject *getOwner();
        CStr *getName();
        bool isConfigured();
        bool isAttached();
        uint64_t count();

    private:
        SkObject *parent;
        CStr *sigName;
        SkMutex attachsEx;
        SkAttach **attachs;
        uint64_t attachsCount;
        SkVariantVector params;
        SkVariant **sendingParams;

#if defined(ENABLE_QTAPP)
        QtStandaloneSignal *qtSig;
#endif

        friend class SkObject;

        void removeAttach(uint64_t id);
};

#endif // SKSIGNAL_H
