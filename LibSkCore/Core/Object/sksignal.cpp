#include "sksignal.h"
#include "skslot.h"
#include "skobject.h"
#include "Core/App/skapp.h"

// // // // // // // // // // // // // // // // // // // // //

SkSignal::SkSignal()
{
    parent = nullptr;
    sigName = nullptr;
    attachs = nullptr;
    attachsCount = 0;
    sendingParams = nullptr;

#if defined(ENABLE_QTAPP)
    qtSig = nullptr;
#endif
}

SkSignal::~SkSignal()
{
    FlatPlusDebug("Signal DIE ..");

#if defined(ENABLE_QTAPP)
    if (qtSig)
    {
        qtSig->deleteLater();
        qtSig = nullptr;
    }
#endif

    if (sendingParams)
        delete [] sendingParams;

    //DESTROY OF ATTACHs HAPPENS ON destroyLater() of parent, through detachAll()
    ObjectPlusDebug_EXT(parent, "Destroyed Signal: " << parent->typeName() << "::" << sigName);
}

void SkSignal::setup(SkObject *owner, CStr *name)
{
    if (isConfigured())
    {
        ObjectError_EXT(parent, "Signal ALREADY configured: " << sigName);
        return;
    }

    parent = owner;
    sigName = name;

    setObjectName(name);

#if defined(ENABLE_QTAPP)
    if (parent->eventLoop() == skApp)
    {
        qtSig = new QtStandaloneSignal;
        SkString n;
        Stringify(n, name << ".qtSig");
        QString qn = QString::fromStdString(n);
        qtSig->setObjectName(qn);
        FlatDebug_EXT(parent, "New Qt standlone-signal: " << parent->typeName() << "::" << sigName);
    }

#endif

    ObjectPlusDebug_EXT(parent, "New Signal: " << parent->typeName() << "::" << sigName);
}

void SkSignal::setParameters(SkVariantVector &p)
{
    if (!isConfigured() || parent->isPreparedToDie())
        return;

    //CONTROL AND WARNING IF THIS METH IS CALLED FROM THREAD NOT EQUAL TO THE parent->eventLoop()
    if (SkThread::currentThID() != parent->eventLoop()->threadID())
    {
        SkEventLoop *thisEvt = parent->eventLoop();
        SkEventLoop *otherEvt = skApp->getEventLoopFromThreadID(SkThread::currentThID());

        FlatError("Signal parameters CANNOT be set from a thread other than the parent (" << parent->objectName() << "): [signalThID: "
                  << thisEvt->threadID() << "(" << thisEvt->objectName() << ")"
                  << "; currThID: " <<  SkThread::currentThID() << "(" << (otherEvt!=nullptr ? otherEvt->objectName() : "") << ")]\n");
        return;
    }

    params = p;

    if (sendingParams)
        delete [] sendingParams;

    sendingParams = new SkVariant * [params.count()];

    SkStringList l;

    for(uint64_t i=0; i<params.count(); i++)
    {
        l << params[i].variantTypeName();
        sendingParams[i] = &params[params.count()-i-1];
    }

    ObjectPlusDebug_EXT(parent, "Setting Signal parameters [" << params.count() << "]: " << l.join(", "));
}

SkVariantVector &SkSignal::getParameters()
{
    return params;
}

bool SkSignal::attach(SkObject *target, SkSlot *slotMeth, SkAttachMode m, CStr *caller)
{
    if (!isConfigured() || parent->isPreparedToDie() || target->isPreparedToDie())
        return false;

    SkAttach *a = nullptr;
    SkMutexLocker locker(&attachsEx);

    for(uint64_t i=0; i<attachsCount; i++)
    {
        a = attachs[i];

        if (a->getSlotOwner() == target && a->getSlot() == slotMeth)
        {
            FlatError("Attach ALREADY exists: "
                      << parent->objectName() << "." << sigName
                      << " -> "
                      << target->objectName() << "." << slotMeth->getName() << " "
                      << "(" << SkAttach::getAttachModeString(m) << "); "
                      << "called from > " << caller);

            return false;
        }
    }

    attachs = static_cast<SkAttach **>(realloc(attachs, (attachsCount+1)*sizeof(SkAttach *)));
    attachs[attachsCount] = new SkAttach(this, target, slotMeth, m);
    attachsCount++;

    target->addAttachHolder(this, slotMeth);

    if (parent->eventLoop() != target->eventLoop() && m == SkAttachMode::SkDirect)
    {
        FlatDebug("Slot (NOT LIVING ON THE SAME THREAD OF THE SIGNAL) ATTACHED: [" << SkAttach::getAttachModeString(m) << "] "
                  << parent->objectName() << "." << sigName
                  << " [" << parent->eventLoop()->objectName() << "] -> "
                  << target->objectName() << "." << slotMeth->getName()
                  << " [" << target->eventLoop()->objectName() << "]");
    }

    else
    {
        FlatDebug("Slot ATTACHED: [" << SkAttach::getAttachModeString(m) << "] "
                   << parent->objectName() << "." << sigName
                   << " -> "
                   << target->objectName() << "." << slotMeth->getName());
    }

    return true;
}

#if defined(ENABLE_QTAPP)

bool SkSignal::attach(QObject *target, CStr *slotName, Qt::ConnectionType m, CStr *caller)
{
    if (!qtSig)
    {
        FlatError("CANNOT attach QtSlot '" << slotName<< "' because Qt standalone-signal is NOT valid; " << "called from > " << caller);
        return false;
    }

    QObject::connect(qtSig, SIGNAL(pulse(SkVariantVector &)), target, slotName, m);
    return true;
}

#endif

bool SkSignal::detach(SkObject *target, SkSlot *slotMeth, CStr *caller)
{
    if (!isConfigured())
        return false;

    SkAttach *a = nullptr;
    SkMutexLocker locker(&attachsEx);

    for(uint64_t i=0; i<attachsCount; i++) // REMOVING WITH O(n)
    {
        a = attachs[i];

        if (a->getSlotOwner() == target && a->getSlot() == slotMeth)
        {
            target->delAttachHolder(this, a->getSlot());
            removeAttach(i);
            return true;
        }
    }

    if (!parent->isPreparedToDie() && !target->isPreparedToDie())
    {
        FlatWarning("Attach NOT found: "
                  << parent->objectName() << "." << sigName
                  << " -> "
                  << target->objectName() << "." << slotMeth->getName() << " ; "
                  << "called from > " << caller);
    }

    else
    {
        FlatDebug("Attach NOT found: "
                  << parent->objectName() << "." << sigName
                  << " -> "
                  << target->objectName() << "." << slotMeth->getName() << " ; "
                  << "called from > " << caller);
    }

    return false;
}

void SkSignal::detach(SkObject *target)
{
    SkAttach *a = nullptr;
    SkMutexLocker locker(&attachsEx);

    for(uint64_t i=0; i<attachsCount; i++) // REMOVING WITH O(n)
    {
        a = attachs[i];

        if (a->getSlotOwner() == target)
            removeAttach(i);
    }
}

#if defined(ENABLE_QTAPP)

bool SkSignal::detach(QObject *target, CStr *slotName, CStr *caller)
{
    if (!qtSig)
    {
        FlatError("CANNOT detach QtSlot '" << slotName<< "' because Qt standalone-signal is NOT valid; " << "called from > " << caller);
        return false;
    }

    QObject::disconnect(qtSig, SIGNAL(pulse(SkVariantVector &)), target, slotName);
    return true;
}

#endif

void SkSignal::removeAttach(uint64_t id)
{
    SkAttach *a = nullptr;
    a = attachs[id];

    SkAttachMode m = a->getMode();
    SkObject *target = a->getSlotOwner();
    SkSlot *slot = a->getSlot();

    if (attachsCount == 1)
    {
        attachsCount = 0;
        free(attachs);
        attachs = nullptr;
    }

    else
    {
        if (id != attachsCount-1)
            attachs[id] = attachs[attachsCount-1]; // REMOVING WITH O(1)

        attachs = static_cast<SkAttach **> (realloc(attachs, (--attachsCount)*sizeof(SkAttach *)));
    }

    FlatDebug("Slot DETACHED: [" << SkAttach::getAttachModeString(m) << "] "
               << parent->objectName() << "." << sigName
               << " -> "
               << target->objectName() << "." << slot->getName());

    delete a;
}

void SkSignal::detachAll()
{
    if (attachs)
    {
        FlatDebug("Detaching all attached Slots [" << attachsCount << "] ..");

        SkAttach *a = nullptr;
        SkObject *target = nullptr;
        SkSlot *slot = nullptr;

        for(uint64_t i=0; i<attachsCount; i++)
        {
            a = attachs[i];
            target = a->getSlotOwner();
            slot = a->getSlot();

            target->delAttachHolder(this, slot);

            FlatDebug("Slot DETACHED: [" << SkAttach::getAttachModeString(a->getMode()) << "] "
                       << parent->objectName() << "." << sigName
                       << " -> "
                       << target->objectName() << "." << slot->getName());

            delete attachs[i];
        }

        free(attachs);
        attachsCount = 0;
    }

#if defined(ENABLE_QTAPP)
    if (qtSig)
        QObject::disconnect(qtSig, SIGNAL(pulse(SkVariantVector &)), nullptr, nullptr);
#endif
}

void SkSignal::trigger(SkFlatObject *referer)
{
    if (!isConfigured())
        return;

    SkAttach *a = nullptr;
    SkObject *o = nullptr;
    SkSlot *slt = nullptr;
    SkAttachMode m;

    for(uint64_t i=0; i<attachsCount; i++)
    {
        a = attachs[i];
        o = a->getSlotOwner();
        slt = a->getSlot();

        if (o->isPreparedToDie())
        {
            FlatDebug("Removing attachs of target prepared to die: [" << SkAttach::getAttachModeString(a->getMode()) << "] "
                       << parent->objectName() << "." << sigName
                       << " -> "
                       << o->objectName() << "." << slt->getName() << " ..");

            detach(o, slt, __PRETTY_FUNCTION__);
        }

        else
        {
            m = a->getMode();

            if (m == SkAttachMode::SkDirect || m == SkAttachMode::SkOneShotDirect)
            {
                if (m == SkAttachMode::SkOneShotDirect)
                    detach(o, slt, __PRETTY_FUNCTION__);

                slt->call(o, referer, sendingParams, params.count());
            }

            else if ((m == SkAttachMode::SkQueued || m == SkAttachMode::SkOneShotQueued) && o->eventLoop())
            {
                if (m == SkAttachMode::SkOneShotQueued)
                    detach(o, slt, __PRETTY_FUNCTION__);

                o->eventLoop()->invokeSlot(slt, o, referer, params);
            }
        }
    }

#if defined(ENABLE_QTAPP)
    if (qtSig)
        qtSig->pulse(params);
#endif
}

SkObject *SkSignal::getOwner()
{
    return parent;
}

CStr *SkSignal::getName()
{
    return sigName;
}

bool SkSignal::isConfigured()
{
    return (parent != nullptr);
}

bool SkSignal::isAttached()
{
    return (attachsCount!=0);
}

uint64_t SkSignal::count()
{
    return attachsCount;
}

// // // // // // // // // // // // // // // // // // // // //
