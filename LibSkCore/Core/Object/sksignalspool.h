/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSIGNALSPOOL_H
#define SKSIGNALSPOOL_H

#include "skstandalonesignal.h"

class SPECIALK SkSignalsPool extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkSignalsPool, SkObject);

        /**
         * @brief Creates new Signal inside the pool
         */
        void createSignal();

        /**
         * @brief Attachs a pool Signal to a target Slot
         * @param index the index of the Signal to Attach
         * @param target the target object
         * @param slotName the Slot name
         * @param mode the attaching mode
         * @return true if the index is valid, otherwise false
         */
        bool attachSignal(uint64_t index,
                          SkObject *target,
                          CStr *slotName,
                          SkAttachMode mode);

        /**
         * @brief Detachs a pool Signal from a target Slot
         * @param index the index of the Signal to Detach
         * @param target the target object
         * @param slotName the Slot name
         * @return true if the index is valid, otherwise false
         */
        bool detachSignal(uint64_t index,
                          SkObject *target,
                          CStr *slotName);

        /**
         * @brief Triggers a pool Signal
         * @param index the index of the Signal to trigger
         */
        void trigger(uint64_t index);

        /**
         * @brief Triggers a pool Signal with a value
         * @param index the index of the Signal to trigger
         * @param val the optional variant to transport
         * @return true if the index is valid, otherwise false
         */
        //bool trigger(uint64_t index, SkVariant &val);

        /**
         * @brief Triggers the entire pool
         */
        void triggerAll();

        /**
         * @brief Triggers the entire pool
         * @param val the optional variant to transport
         */
        //void triggerAll(SkVariant &val);

        //DOES NOT DESTROY INTERNAL SkStandaloneSignal
        /**
         * @brief Gets a pool Signal
         * @param index the index of the Signal
         * @warning do NOT destroy the Signal from extern of this object
         * @return the standalone Signal if the index is valid, otherwise false
         */
        SkStandaloneSignal *getSignal(uint64_t index);

        /**
         * @brief Destroies a pool Signal
         * @param index the index of the Signal
         * @warning the indexes greater than the removing Signal, will be decremented
         * @return true if the index is valid, otherwise false
         */
        bool destroySignal(uint64_t index);

        /**
         * @brief Clear the entire pool, destroying all Signals
         */
        void clear();

    private:
        SkVector<SkStandaloneSignal *> sigs;
};

#endif // SKSIGNALSPOOL_H
