#include "skobject.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skvariant.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkAttachMode)
{
    SetupEnumWrapper(SkAttachMode);

    SetEnumItem(SkAttachMode::SkDirect);
    SetEnumItem(SkAttachMode::SkQueued);
    SetEnumItem(SkAttachMode::SkOneShotQueued);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareSkObjectWrapper(SkObject);

DeclareMeth_INSTANCE_VOID(SkObject, setObjectName, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkObject, objectName, CStr *)

SetupClassWrapper(SkObject)
{
    AddEnumType(SkAttachMode);

    SetClassSuper(SkObject, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkObject, setObjectName);
    AddMeth_INSTANCE_RET(SkObject, objectName);
}

// // // // // // // // // // // // // // // // // // // // //

void SkObjectMetaDestructor(SkObject *obj)
{
    ObjectDebug_EXT(obj, "exec dtorCompanion -> SkObjectDestructor()");

    for(uint64_t i=0; i<obj->getDestructorCompains().count(); i++)
        (*obj->getDestructorCompains().at(i))(obj);
}

// // // // // // // // // // // // // // // // // // // // //

SkObject::SkObject() : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    objInit(nullptr, false);
}

SkObject::SkObject(SkObject *parent) : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    objInit(parent, false);
}

SkObject::SkObject(CStr *name) : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    setObjectName(name);
    objInit(nullptr, false);
}

SkObject::SkObject(CStr *name, SkObject *parent) : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    setObjectName(name);
    objInit(parent, false);
}

SkObject::SkObject(SkObject *parent, bool) : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    objInit(parent, true);
}

SkObject::SkObject(CStr *name, SkObject *parent, bool) : SkFlatObject()
{
    CreateClassWrapper(SkObject);
    setObjectName(name);
    objInit(parent, true);
}

void SkObject::objInit(SkObject *parent, bool inherit)
{
    loop = nullptr;
    isFlat = false;
    properties = nullptr;
    objParent = nullptr;
    objRegistered = false;
    objPreparedToDie = false;

    identifier = uniqueID();

    SkString tempObjName = "SkObject";

    if (inherit)
        tempObjName.append("-InhBase");

    createDefaultObjName(tempObjName);
    registerOnSkApp();

    setParent(parent);

    ObjectSetup(SkObject, inherit);

    SignalSet(pulse);
    SignalSet(died);
}

void SkObject::addSignal(SkSignal *sig)
{
    objSignals[sig->getName()] = sig;
}

SkObject::~SkObject()
{
    ObjectPlusDebug("Destruct [Type: " << typeName() << "; Loop: " << eventLoop()->objectName() << "]");

    if (skApp->existsSharedComponent(this))
        skApp->removeSharedComponent(skApp->getSharedComponentName(this));

    if (objParent && !objParent->objPreparedToDie)
        objParent->removeChild(this);

    if (!objPreparedToDie)
    {
        ObjectWarning("It seems to be a DIRECT DESTROY, and this is NOT a good practice; DEREGISTERING [" << typeName() << "] ..");
        eventLoop()->removeComponent(this);
    }

    if (properties)
    {
        delete properties;
        properties = nullptr;
    }

    for(uint64_t i=0; i<destructorCompains.count(); i++)
        delete destructorCompains[i];
}

CStr *SkObject::uuid()
{
    return identifier.c_str();
}

void SkObject::checkAndBuildProps()
{
    if (properties)
        return;

    properties = new SkArgsMap;
    properties->getInternalVector().setObjectName(this, "Properties");
}

// // // // // // // // // // // // // // // // // // // // //

bool SkObject::registerOnSkApp()
{
    if (objRegistered)
    {
        ObjectError(objName << " is ALREADY registered");
        return false;
    }

    if (objName.empty())
        createDefaultObjName(typeName());

    objRegistered = skApp->registerComponent(this);
    return objRegistered;
}

void SkObject::createDefaultObjName(const SkString &typeName)
{
    objName = typeName;
    objName.append("_");
    objName.append(uuid());
}

void SkObject::setEventLoop(SkEventLoop *eventLoop)
{
    loop = eventLoop;
}

SkEventLoop *SkObject::eventLoop()
{
    return loop;
}

SkOsEnv *SkObject::osEnv()
{
    return skApp->osEnv();
}

bool SkObject::setParent(SkObject *parent)
{
    if (parent && tid != parent->threadID())
    {
        ObjectError("Parent SkObject lives in another thread [parent_T: " << parent->typeName()
                  << " - this_T: " << this->typeName() <<  "]");

        return false;
    }

    if (objParent && parent == objParent)
    {
        ObjectDebug("Object is ALREADY set as parent: " << parent->objectName());
        return false;
    }

    if (objParent)
        objParent->removeChild(this);

    objParent = parent;

    if (objParent)
        objParent->addChild(this);

    return true;
}

SkObject *SkObject::parent()
{
    return objParent;
}

bool SkObject::addChild(SkObject *child)
{
    if (tid != child->threadID())
    {
        ObjectError("Child SkObject lives in another thread");
        return false;
    }

    childrenList.append(child);

    if (child->parent() != this)
        child->setParent(this);

    return true;
}

bool SkObject::removeChild(SkObject *child)
{
    if (child && childrenList.remove(child) == child)
        return true;

    ObjectError("Child NOT found: " << child->objectName());
    return false;
}

SkList<SkObject *> &SkObject::children()
{
    return childrenList;
}

void SkObject::recursiveChildren(SkList<SkObject *> &allChildren)
{
    if (childrenList.isEmpty())
        return;

    SkAbstractListIterator<SkObject *> *itr = childrenList.iterator();

    while(itr->next())
    {
        SkObject *child = itr->item();
        allChildren << child;
        child->recursiveChildren(allChildren);
    }

    delete itr;
}

bool SkObject::setupAsSharedComponent(CStr *name)
{
    if (!objRegistered)
    {
        ObjectError("Cannot share component NOT registered YET");
        return false;
    }

    if (!name)
        name = objName.c_str();

    if (skApp->existsSharedComponent(name))
        skApp->removeSharedComponent(name);

    return skApp->setupSharedComponent(name, this);
}

void SkObject::setProperty(CStr *name, SkVariant v)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    (*properties)[name] = v;
}

SkVariant &SkObject::property(CStr *name)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    return properties->value(name);
}

bool SkObject::existsProperty(CStr *name)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    return properties->contains(name);
}

void SkObject::getPropertiesNames(SkStringList &names)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    properties->keys(names);
}

bool SkObject::removeProperty(CStr *name)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    return (properties->remove(name).value() != properties->nv());//ERRONEOUS
}

void SkObject::setProperties(SkArgsMap &props)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    properties->clear();
    properties->insertMap(props);
}

void SkObject::addProperties(SkArgsMap &props)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    properties->insertMap(props);
}

void SkObject::setProperties(SkObject *other)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    properties->clear();
    properties->insertMap(*other->properties);
}

void SkObject::addProperties(SkObject *other)
{
    SkMutexLocker locker(&propertiesEx);
    checkAndBuildProps();
    properties->insertMap(*other->properties);
}

SkSignal *SkObject::getSignal(CStr *name)
{
    /*SkString str(name);

    for(uint64_t i=0; i<objSigsCount; i++)
    {
        if (str == objSigs[i]->getName())
            return objSigs[i];
    }*/

    if (objSignals.contains(name))
        return objSignals[name];

    return nullptr;
}

bool SkObject::emitSignal(CStr *name)
{
    SkVariantVector params;
    return emitSignal(name, params);
}

bool SkObject::emitSignal(CStr *name, SkVariantVector &params)
{
    if (isPreparedToDie())
        return false;

    SkSignal *sig = getSignal(name);

    if (sig)
    {
        if (!params.isEmpty())
            sig->setParameters(params);

        sig->trigger(this);
        return true;
    }

    ObjectError("Signal NOT found: " << name);
    return false;
}

SkSlot *SkObject::getSlot(CStr *name)
{
    return searchSlot(typeID(), name);
}

bool SkObject::invokeSlot(CStr *name, SkVariant **p, uint64_t pCount)
{
    if (isPreparedToDie())
        return false;

    SkSlot *slt = getSlot(name);

    if (slt)
    {
        slt->call(this, this, p, pCount);
        return true;
    }

    ObjectError("Slot NOT found: " << name);
    return false;
}

bool SkObject::isPreparedToDie()
{
    return objPreparedToDie;
}

SK_OBJLAMBDA_T SkObject::getMetaDestructor()
{
    return destructor;
}

SkVector<SK_OBJLAMBDA_T *> &SkObject::getDestructorCompains()
{
    return destructorCompains;
}

void SkObject::addAttachHolder(SkSignal *sig, SkSlot *slot)
{
    slotAttachsHolderEx.lock();
    slotAttachsHolder.append(SkPair<SkSignal *, SkSlot *>(sig, slot));
    slotAttachsHolderEx.unlock();
}

void SkObject::delAttachHolder(SkSignal *sig, SkSlot *slot)
{
    slotAttachsHolderEx.lock();
    SkAbstractListIterator<SkPair<SkSignal *, SkSlot *>> *itr = slotAttachsHolder.iterator();

    while(itr->next())
        if (itr->item().key() == sig && itr->item().value() == slot)
        {
            itr->remove();
            break;
        }

    delete itr;
    slotAttachsHolderEx.unlock();
}

void SkObject::destroyLater()
{
    if (objPreparedToDie)
    {
        ObjectWarning("Cannot accept DESTROY REQUEST because this object is ALREADY preparedToDie");
        return;
    }

    ObjectDebug("Destroy request ..");

    died();
    onPreparingToDie();

    {
        ObjectDebug("Detaching all Signals [" << objSignals.count() << "] ..");
        SkBinaryTreeVisit<SkString, SkSignal *> *itr = objSignals.iterator();

        while(itr->next())
            itr->item().value()->detachAll();

        delete itr;
    }

    {
        ObjectDebug("Detaching all attached Slots [" << slotAttachsHolder.count() << "] ..");

        slotAttachsHolderEx.lock();
        SkAbstractListIterator<SkPair<SkSignal *, SkSlot *>> *itr = slotAttachsHolder.iterator();

        //NOT EFFICIENT
        while(itr->next())
        {
            SkSignal *sig = itr->item().key();
            sig->detach(this);
        }

        delete itr;
        slotAttachsHolder.clear();
        slotAttachsHolderEx.unlock();
    }

    objPreparedToDie = true;

    ObjectPlusDebug("Let children to go on die way ..");

    SkAbstractListIterator<SkObject *> *itr = childrenList.iterator();

    while(itr->next())
    {
        SkObject *o = itr->item();

        if (!o->isPreparedToDie())
        {
            o->objParent = nullptr;
            o->destroyLater();
        }
    }

    delete itr;
    childrenList.clear();
}

// // // // // // // // // // // // // // // // // //

bool attachByName(SkObject *source, CStr *signalName,
                  SkObject *target, CStr *slotName,
                  SkAttachMode mode, CStr *caller)
{
    SkAttachingInfo *attach = new SkAttachingInfo;
    attach->source = source;
    attach->signalName = signalName;
    attach->target = target;
    attach->slotName = slotName;
    attach->mode = mode;
    attach->caller = caller;

    return source->eventLoop()->addAttachingItem(attach);
}

bool detachByName(SkObject *source, CStr *signalName,
                  SkObject *target, CStr *slotName,
                  CStr *caller)
{
    SkDetachingInfo *detach = new SkDetachingInfo;
    detach->source = source;
    detach->signalName = signalName;
    detach->target = target;
    detach->slotName = slotName;
    detach->caller = caller;

    return source->eventLoop()->addDetachingItem(detach);
}

bool addAttach(SkAttachingInfo *info)
{
    if (info->source->isPreparedToDie() || info->target->isPreparedToDie())
    {
        if (info->source->isPreparedToDie())
            StaticDebug("CANNOT Attach; Source is going to die: "
                      << info->source->objectName() << "." << info->signalName
                      << " -> "
                      << info->target->objectName() << "." << info->slotName << " "
                      << "(" << SkAttach::getAttachModeString(info->mode) << "); "
                      << "called from > " << info->caller);

        if (info->target->isPreparedToDie())
            StaticDebug("CANNOT Attach; Target is going to die: "
                      << info->source->objectName() << "." << info->signalName
                      << " -> "
                      << info->target->objectName() << "." << info->slotName << " "
                      << "(" << SkAttach::getAttachModeString(info->mode) << "); "
                      << "called from > " << info->caller);

        delete info;
        return false;
    }

    SkSignal *sig = info->source->getSignal(info->signalName.c_str());

    if (!sig)
    {
        StaticError("CANNOT Attach; Signal NOT found: "
                  << info->source->typeName() << "::" << info->signalName << "; "
                  << "called from > " << info->caller);

        delete info;
        KillApp();//LET ME HERE!!!
        return false;
    }

    SkSlot *slt = info->target->getSlot(info->slotName.c_str());

    if (!slt)
    {
        StaticError("CANNOT Attach; Slot NOT found: "
                  << info->target->typeName() << "::" << info->slotName << "; "
                  << "called from > " << info->caller);

        delete info;
        KillApp();//LET ME HERE!!!
        return false;
    }

    bool ok = sig->attach(info->target, slt, info->mode, info->caller);
    delete info;

    return ok;
}

bool delAttach(SkDetachingInfo *info)
{
    SkSignal *sig = info->source->getSignal(info->signalName.c_str());

    if (!sig)
    {
        StaticError("CANNOT Detach; Signal NOT found: "
                  << info->source->typeName() << "::" << info->signalName << "; "
                  << "called from > " << info->caller);

        delete info;
        return false;
    }

    SkSlot *slt = info->target->getSlot(info->slotName.c_str());

    if (!slt)
    {
        StaticError("CANNOT Detach; Slot NOT found: "
                  << info->target->typeName() << "::" << info->slotName << "; "
                  << "called from > " << info->caller);

        delete info;
        return false;
    }

    bool ok = sig->detach(info->target, slt, info->caller);
    delete info;
    return ok;
}

#if defined(ENABLE_QTAPP)

bool qtAttachByName(SkObject *source, CStr *signalName,
                    QObject *target, CStr *slotName,
                    Qt::ConnectionType mode, CStr *caller)
{
    SkSignal *sig = source->getSignal(signalName);

    if (!sig)
    {
        StaticError("CANNOT Attach QtSlot; SkSignal NOT found: "
                    << source->typeName() << "::" << signalName << "; "
                    << "called from > " << caller);

        KillApp();//LET ME HERE!!!
        return false;
    }

    return sig->attach(target, slotName, mode, caller);
}

bool qtDetachByName(SkObject *source, CStr *signalName,
                    QObject *target, CStr *slotName, CStr *caller)
{
    SkSignal *sig = source->getSignal(signalName);

    if (!sig)
    {
        StaticError("CANNOT Detach QtSlot; SkSignal NOT found: "
                    << source->typeName() << "::" << signalName << "; "
                    << "called from > " << caller);

        KillApp();//LET ME HERE!!!
        return false;
    }

    return sig->detach(target, slotName, caller);
}

#endif
