#include "sksignalspool.h"

//SK_METATYPE(SkSignalsPool)

void SkSignalsPoolClose(SkObject *obj)
{
    SkSignalsPool *f = static_cast<SkSignalsPool *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkSignalsPoolClose()");

    f->clear();
}

ConstructorImpl(SkSignalsPool, SkObject)
{
    addDtorCompanion(SkSignalsPoolClose);
}

void SkSignalsPool::createSignal()
{
    SkStandaloneSignal *sig = new SkStandaloneSignal(this);
    sigs.append(sig);
}

bool SkSignalsPool::attachSignal(uint64_t index,
                                 SkObject *target,
                                 CStr *slotName,
                                 SkAttachMode mode)
{
    if (index >= sigs.count())
        return false;

    return attachByName(sigs.at(index), "pulse", target, slotName, mode, __PRETTY_FUNCTION__);
}
bool SkSignalsPool::detachSignal(uint64_t index,
                                 SkObject *target,
                                 CStr *slotName)
{
    if (index >= sigs.count())
        return false;

    return detachByName(sigs.at(index), "pulse", target, slotName, __PRETTY_FUNCTION__);
}

void SkSignalsPool::trigger(uint64_t index)
{
    if (index >= sigs.count())
        return;

    sigs.at(index)->pulse();
}

/*bool SkSignalsPool::trigger(uint64_t index, SkVariant &val)
{
    if (index >= sigs.count())
        return false;

    sigs.at(index)->pulse(val);
    return true;
}*/

void SkSignalsPool::triggerAll()
{
    for(uint64_t i=0; i<sigs.count(); i++)
        trigger(i);
}

/*void SkSignalsPool::triggerAll(SkVariant &val)
{
    for(uint64_t i=0; i<sigs.count(); i++)
        trigger(i, val);
}*/

SkStandaloneSignal *SkSignalsPool::getSignal(uint64_t index)
{
    if (index >= sigs.count())
        return nullptr;

    return sigs.at(index);
}

bool SkSignalsPool::destroySignal(uint64_t index)
{
    if (index >= sigs.count())
        return false;

    sigs.at(index)->destroyLater();
    sigs.removeAt(index);
    return true;
}

void SkSignalsPool::clear()
{
    for(uint64_t i=0; i<sigs.count(); i++)
        sigs.at(i)->destroyLater();

    sigs.clear();
}
