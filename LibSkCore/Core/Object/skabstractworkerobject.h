#ifndef SKABSTRACTWORKEROBJECT_H
#define SKABSTRACTWORKEROBJECT_H

#include "skobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkWorkerParam extends SkFlatObject
{
    public:
        SkWorkerParam();
        SkWorkerParam(CStr *key, CStr *label, SkVariant_T type, bool isRequired, const SkVariant &defaultValue, CStr *description);

        void setValue(const SkVariant &val);
        void setMin(const SkVariant &min);
        void setMax(const SkVariant &max);
        void setStep(const SkVariant &step);
        void setDefault(const SkVariant &dflt);

        void addOption(CStr *lbl, const SkVariant &value);

        CStr *label();
        CStr *key();
        SkVariant &value();
        CStr *description();
        bool isRequired();
        SkVariant_T valueType();
        SkVariant &defaultValue();
        SkVariant &minValue();
        SkVariant &maxValue();
        SkVariant &stepValue();
        bool hasOptions();
        SkList<SkPair<SkString, SkVariant>> &options();

        bool toMap(SkArgsMap &m);

    private:
        SkString lbl;
        SkString k;
        SkVariant v;
        SkVariant_T t;
        SkString desc;
        bool required;
        SkVariant dfltVal;
        SkVariant minVal;
        SkVariant maxVal;
        SkVariant stepVal;
        SkList<SkPair<SkString, SkVariant>> opts;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkWorkerCommand extends SkFlatObject
{
    public:
        SkWorkerCommand();

        void set(CStr *label, CStr *description, SkSlot *slot, bool isControl=false);
        SkWorkerParam *addParam(CStr *key, CStr *label, SkVariant_T t, bool isRequired, const SkVariant &defaultValue, CStr *description);
        bool load(SkArgsMap &m);
        SkTreeMap<SkString, SkWorkerParam *> &parameters();

        void clear();

        CStr *command();
        CStr *label();
        CStr *description();
        SkSlot *slot();
        bool isControl();
        bool contains(CStr *key);
        SkVariant &value(CStr *key);

        bool toMap(SkArgsMap &m);

    private:
        bool ctrlMode;
        SkString lbl;
        SkString desc;
        SkSlot *slt;
        SkVariant nv;
        SkTreeMap<SkString, SkWorkerParam *> params;

        bool loadParameter(SkWorkerParam *p, SkVariant &val);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractWorkerObject;
class SkAbstractDevice;

//IT WILL RETURN TO parent()::returningSlot

class SkWorkerTransaction extends SkObject
{
    public:
        Constructor(SkWorkerTransaction, SkObject);

        void setCommand(SkAbstractWorkerObject *worker, SkAbstractDevice *commander, SkSlot *returningSlot, CStr *command, SkArgsMap *val=nullptr);
        void setResponse(SkArgsMap &response);
        void setBinaryResponse(SkDataBuffer &response);
        void setNoResponse();
        void setError(CStr *errorString);

        void invoke();

        void goBack();

        SkAbstractWorkerObject *worker();
        CStr *command();
        SkWorkerCommand *commandSlot();
        SkArgsMap &argument();
        bool isStillAlive();
        SkAbstractDevice *commander();
        bool hasErrors();
        SkArgsMap &response();
        SkDataBuffer &responseBin();
        CStr *error();

        Slot(deactivate);

    private:
        friend class SkAbstractWorkerObject;

        SkWorkerCommand *abstractCommand;

        SkObject *retSlotOwner;
        SkSlot *retSlot;
        SkAbstractWorkerObject *wrk;
        SkAbstractDevice *dev;
        bool deviceOpen;

        SkString cmd;
        SkArgsMap cmdArgument;
        SkArgsMap rsp;
        SkDataBuffer rspBin;
        bool successfull;
        SkString e;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractWorkerObject extends SkObject
{
    SkTreeMap<SkString, SkWorkerCommand *> modCmds;

    public:
        void executeTransaction(SkWorkerTransaction *t);
        bool analyzeTransaction(SkWorkerTransaction *cmd, SkVariant &v);

        void clearCommands();

        SkTreeMap<SkString, SkWorkerCommand *> &commands();

    protected:
        AbstractConstructor(SkAbstractWorkerObject, SkObject);

        SkWorkerCommand *addCommand(CStr *label, CStr *description, SkSlot *slot);
        SkWorkerCommand *addCommand(SkWorkerCommand *modCmd);//(ONLY ADD) BEFORE, THE modCmd MUST BE NAMED and configured
        SkWorkerParam *addSingleControl(CStr *label, SkVariant_T t, const SkVariant &defaultValue, CStr *description, SkSlot *slot);
        SkWorkerCommand *addMultiControl(CStr *label, CStr *description, SkSlot *slot);

        virtual void updateCommand(SkWorkerCommand *cmd)                    = 0;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKABSTRACTWORKEROBJECT_H
