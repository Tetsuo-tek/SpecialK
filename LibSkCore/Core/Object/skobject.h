/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKOBJECT_H
#define SKOBJECT_H

//SkObject RULEs (few but rigorous):
//
//SKOBJECTs AND DERIVATEs CANNOT BE COPIED (OR RETURNED AS COPY),
//THIS MEANs USING the '=' OPERATOR or the "SkObject(const SkObject&)" CTOR
//OTHERWISE COMPILER WILL RETURN AN ERROR WHEN COMPILE IT
//
//WHEN SUBCLASS IT,
//  - DO NOT HIDE BASE-OBJECT WITH private or protected (OTHERWISE MYTERIOUS THINGs WILL HAPPEN)
//  - DO NOT OVERRIDE DESTRUCTOR
//
//ALLOCATE SkObject AND DERIVATEs WITH new OPERATOR
//AND SCHEDULE THEM FOR DELETE CALLING obj->destroryLater()
//
//CALLING delete BY HAND COULD CREATE VARIOUS PROBLEMs;
//  - LAMBDA DTOR AND DTOR-COMPAINs WILL NOT BE TRIGGERED ON DESTROY
//
//THIS CONDITIONs INCLUDE ALSO ALLOCATING THE OBJECT ON THE STACK WITH
//A LIMITED LIFE-SCOPE (WITHOUT NEW)
//
//FOR EXAMPLE:
//  SkObject obj;
//
//INSTEAD, USE ALLOCATION ON THE HEAP:
//  SkObject *obj = new SkObject();
//
//WHEN REQUIRED TO DEALLOCATE THE OBJECT CALL:
//  obj->destroyLater();
//SO THE OBJECT WILL BE DELETED AS POSSIBLE BY THE EVENTLOOP
//

class SkEventLoop;
struct SkDeliveringEvent;
class SkOsEnv;

#include "skattach.h"
#include "sksignal.h"
#include "skslot.h"

#include "Core/Containers/skargsmap.h"
#include "Core/System/Thread/skmutexlocker.h"
#include "Core/sklogmachine.h"

#include "Core/Containers/skstringlist.h"
#include "Core/Containers/sklinkedmap.h"
#include "Core/Containers/sklist.h"
#include "Core/Containers/skarray.h"
#include "Core/Containers/sktreemap.h"

/**
 * @brief The SkObject is the common superclass for all objects capable to use
 * Signal/Slot paradigm, wich also uses a dynamical typeID.
 * SkObjects can live within a hierarchical structure made up of several recursive
 * parents/children, and each object (parent and child at the same time) could
 * contain children; SkObject RULEs are few but rigorous
 * @warning the parent must live in the same thread of the child, having also
 * the same eventLoop
 * @warning all SkObject derived classes CANNOT be copied or returned as copy;
 * this means that CANNOT be assigned with '=' operator or contructors as
 * 'SkObject(const SkObject&)' for example, otherwise the compiler will return
 * an error when compile it
 * @warning all SkObject derived classes MUST be instanced ALWAYS dynamically in
 * the heap using the 'new' operator
 * @warning all SkObject derived classes must NOT be destroyed manually using
 * 'delete' operator, but using the specialized method destroyLater(), that
 * demands for its destruction to the eventLoop garbage collector
 * @warning when an object having children is scheduled for destroy, all its
 * recursive children do the same
 * @warning when SkObjects are subclassed is VERY IMPORTANT to NOT make an
 * overriding of the distructor; instead of the destructor, when there are operations
 * to perform on the death of the object it is necessary to use the macro
 * addDtorCompanion (..) and associated labda functions, which will be executed
 * by the garbage-collector before the actual destruction of the object;
 * destructor companion function MUST have the following prototype:
 * -> 'void <NAME>(SkObject *)'
 * @warning when a eventLoop owning an SkObject derived class instance is going
 * to die, all its objects will be scheduled to die too
 */
class SPECIALK SkObject extends SkFlatObject
{
    protected:
        //inheriting will be changed always to true

        /**
         * @brief Constructs an SkObject inheriting it
         * @param parent the parent of the object
         * @param inheriting does anything inheriting always true (always changed internally to true)
         */
        SkObject(SkObject *parent, bool inheriting);

        /**
         * @brief Constructs an SkObject inheriting it
         * @param name the name for the object instance
         * @param parent the parent of the object
         * @param inheriting does anything inheriting always true (always changed internally to tru
         */
        SkObject(CStr *name, SkObject *parent, bool inheriting);

    public:
        /**
         * @brief Simple contructor, without a name and without a parent
         */
        SkObject();

        /**
         * @brief Constructs an SkObject assigning a parent to it
         * @param parent the SkObject derived class instance becoming the parent
         */
        SkObject(SkObject *parent);

        /**
         * @brief Constructs an SkObject assigning an instance name to it
         * @param name the instance name
         */
        SkObject(CStr *name);

        /**
         * @brief Constructs an SkObject assigning a parent and an instance name
         * to it
         * @param name the instance name
         * @param parent the SkObject derived class instance becoming the parent
         */
        SkObject(CStr *name, SkObject *parent);

        // >>> !!! DO NOT OVERRIDE THE DESTRUCTOR !!! <<<
        //
        //IF IT IS REQUIRED TO CALL SOME FUNCTION ON DESTROYING OBJECT
        //IT IS POSSIBLE USING "addDtorCompanion(..)" IN THE CONSTRUCTOR,
        //PASSING IT A LAMBA FUNCTION "NAME" DEFINED AS "void <NAME>(SkObject *)"
        /**
         * @brief Internally used; destructor; DO NOT OVERRIDE THIS
        */
        ~SkObject() override;

        CStr *uuid();

        /**
         * @brief Gets the owner eventLoop for this object instance
         * @return the owner eventLoop
         */
        SkEventLoop *eventLoop();

        /**
         * @brief Facilities; it will return the static global object for the
         * system environment from skApp
         * @return
         */
        SkOsEnv *osEnv();

        /**
         * @brief Sets the parent for this object instance
         * @warning the parent must live in the same thread of the child, having
         * also the same eventLoop
         * @param parent the object to use as parent
         * @return true if the parent is valid
         */
        bool setParent(SkObject *parent);

        /**
         * @brief Gets the parent object pointer
         * @return the parent pointer, or NULL pointer if this object has not a parent
         */
        SkObject *parent();

        /**
         * @brief Internally used; gets the list of the direct children of this
         * object instance
         * @return the internal list of children
         */
        SkList<SkObject *> &children();
        void recursiveChildren(SkList<SkObject *> &allChildren);

        /*template <class T> T child()
        {}*/

        /**
         * @brief Globalizes this object instances on skApp with a preferred name
         * @param name the preferred name for the globalization
         * @return true if does not exists other globalized object with the same
         * mame, otherwise false
         */
        bool setupAsSharedComponent(CStr *name=nullptr);

        /**
         * @brief Sets a new custom property
         * @param name the name of the property
         * @param v the value of the property
         */
        void setProperty(CStr *name, SkVariant v);

        /**
         * @brief Gets a custom property
         * @param name the name of the requested property
         * @return the value of the property if it exists, otherwise an empty value
         */
        SkVariant &property(CStr *name);

        /**
         * @brief Checks if a custom property exists
         * @param name the name of the property to check
         * @return true if the property exists, otherwise false
         */
        bool existsProperty(CStr *name);

        /**
         * @brief Fills a string-list with all stored custom properties names
         * @param names the string-list to fill
         */
        void getPropertiesNames(SkStringList &names);

        /**
         * @brief Removes a custom properties
         * @param name the name of the custom properties to remove
         * @return true if the property exists, otherwise false
         */
        bool removeProperty(CStr *name);

        /**
         * @brief Sets a custom properties group, deleting previouses
         * @param props the group of properties to set
         */
        void setProperties(SkArgsMap &props);

        /**
         * @brief Add a custom properties group; if some name already exists, the
         * associated value will be updated
         * @param props the group of properties to add
         */
        void addProperties(SkArgsMap &props);

        /**
         * @brief Sets a custom properties group from another object, deleting previouses
         * @param other the owner of the group of properties to set
         */
        void setProperties(SkObject *other);

        /**
         * @brief Add a custom properties group from another object; if some name
         * already exists, the associated value will be updated
         * @param other the owner of the group of properties to add
         */
        void addProperties(SkObject *other);

        /**
         * @brief Internally used; checks if the objects is scheduled for destroy
         * @return true if it is going to die, otherwise false
         */
        bool isPreparedToDie();

        /**
         * @brief Internally used; return the dynamic function owned by the object
         * type that is capable to make the effective instance destroy
         * @return the metadestructor function
         */
        SK_OBJLAMBDA_T getMetaDestructor();

        /**
         * @brief Internally used; return the dynamic set of custom functions
         * to trigger before the effective object destroy; these functions are
         * added with macro 'addDtorCompanion(..)' and must have the following
         * proptotype:
         * -> 'void <NAME>(SkObject *)'
         * @return
         */
        SkVector<SK_OBJLAMBDA_T *> &getDestructorCompains();

        /**
         * @brief Triggers a Signal
         * @param name the name of the Signal
         * @param arguments the custom raw pointer to transport on Signal;
         * if it is NULL the engine will use the this pointer relative to owner object
         * @return true if the signal exists, otherwise false
         */
        SkSignal *getSignal(CStr *name);
        bool emitSignal(CStr *name);
        bool emitSignal(CStr *name, SkVariantVector &params);

        /**
         * @brief Invokes a Slot through a methods-chain.
         * Each type that subclass SkObject overlaps this meth, constructing
         * what i named as SKELEVATOR; it will call recursively between inherited
         * classes using T *subThis until the slot is found
         * @param name the name of the Slot
         * @param arguments the custom raw pointer to transport on the Slot
         * @param val the optional variant pointer to transport on the Slot
         * @param asQueued the invoking mode
         * @return true if the Slot exists, otherwise false
         */
        SkSlot *getSlot(CStr *name);

        //this call directly; if it is required to call in queued mode use EventLoop::invokeSlot
        bool invokeSlot(CStr *name, SkVariant **p=nullptr, uint64_t pCount=0);

        /**
         * @brief Schedules the object destruction through the garbage collector
         */
        void destroyLater();

        /**
         * @brief It is a default Signal usable in various situations
         */
        Signal(pulse);

        //MUST BE ATTACHED DIRECT; ON TICK AFTER THE OBJECT IS PREPARED TO DIE
        Signal(died);

    protected:
        SkMutex propertiesEx;
        SkArgsMap *properties;

        SK_OBJLAMBDA_T destructor;
        SkVector<SK_OBJLAMBDA_T *> destructorCompains;

        //DO NOT REMOVE ELEMENTs FROM SUBCLASSERs
        SkList<SkObject *> childrenList;

        void addSignal(SkSignal *sig);

        void createDefaultObjName(const SkString &typeName);
        bool registerOnSkApp();

        virtual void onEvent(SkDeliveringEvent *){}

        virtual void onPreparingToDie(){}

    private:
        SkString identifier;

        SkObject *objParent;

        SkTreeMap<SkString, SkSignal *> objSignals;

        bool objRegistered;
        bool objPreparedToDie;

        SkEventLoop *loop;

        void objInit(SkObject *parent, bool inherit);

        void checkAndBuildProps();

        bool addChild(SkObject *child);
        bool removeChild(SkObject *child);

        // !!! IMPORTANT NOTE !!!
        //DENY OBJECT-COPY
        //SkObject(const SkObject&);
        //SkObject& operator = (const SkObject&);
        // !!! IMPORTANT NOTE !!!

        SkObject(const SkObject&) = delete;
        void operator=(const SkObject&) = delete;

        friend class SkEventLoop;
        void setEventLoop(SkEventLoop *eventLoop);

        friend class SkSignal;
        void addAttachHolder(SkSignal *sig, SkSlot *slot);
        void delAttachHolder(SkSignal *sig, SkSlot *slot);

        SkMutex slotAttachsHolderEx;
        SkList<SkPair<SkSignal *, SkSlot *>> slotAttachsHolder;
};

typedef struct
{
    SkObject *source;
    SkString signalName;
    SkObject *target;
    SkString slotName;
    SkAttachMode mode;
    CStr *caller;
} SkAttachingInfo;

typedef struct
{
    SkObject *source;
    SkString signalName;
    SkObject *target;
    SkString slotName;
    CStr *caller;
} SkDetachingInfo;

// // // // // // // // // // // // // // // // // // // // //

bool attachByName(SkObject *source, CStr *signalName,
                  SkObject *target, CStr *slotName,
                  SkAttachMode mode, CStr *caller);

bool detachByName(SkObject *source, CStr *signalName,
                  SkObject *target, CStr *slotName, CStr *caller);

#define Attach(SRC, SIGFUNCT, TGT, SLOTFUNCT, MODE) \
    attachByName(SRC, #SIGFUNCT, TGT, #SLOTFUNCT, MODE, __PRETTY_FUNCTION__)

#define Detach(SRC, SIGFUNCT, TGT, SLOTFUNCT) \
    detachByName(SRC, #SIGFUNCT, TGT, #SLOTFUNCT, __PRETTY_FUNCTION__)

//BELOW ARE THE REAL ATTACH AND DETACH ROUTINEs, BUT PAY ATTENTION: THEY USE LOCKs
//THESE METHs ARE ONLY FOR INTERNAL USE
//DOES NOT USE THESE FUNCTIONS INSIDE SLOT CONNECTED AS QUEUED TO A SIGNAL
//LIVING ON THE SAME THREAD (MEANING FOR SOURCE AND TARGET OBJECTS)
//OTHERWISE WE WILL A HAVE A PURE DEADLOCK

bool addAttach(SkAttachingInfo *info);
bool delAttach(SkDetachingInfo *info);

#if defined(ENABLE_QTAPP)

bool qtAttachByName(SkObject *source, CStr *signalName,
                    QObject *target, CStr *slotName,
                    Qt::ConnectionType mode, CStr *caller);

bool qtDetachByName(SkObject *source, CStr *signalName,
                    QObject *target, CStr *slotName, CStr *caller);

#define QtAttach(SRC, SIGFUNCT, QTTGT, QTSLOTNAME, MODE) \
    qtAttachByName(SRC, #SIGFUNCT, QTTGT, QTSLOTNAME, MODE, __PRETTY_FUNCTION__)

#define QtDetach(SRC, SIGFUNCT, QTTGT, QTSLOTNAME) \
    qtDetachByName(SRC, #SIGFUNCT, QTTGT, QTSLOTNAME, __PRETTY_FUNCTION__)

#endif

// // // // // // // // // // // // // // // // // // // // //


#endif // SKOBJECT_H
