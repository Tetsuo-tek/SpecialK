#if defined(SPECIALK_APPLICATION)

#include "skapp.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkOsEnv;

#include <unistd.h>
#include <signal.h>

#include "Core/sklogmachine.h"
#include "Core/System/skosenv.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/Containers/skarraycast.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkLogMachine *logger = nullptr;
SkOsEnv *osEnvironment = nullptr;

#if defined(ENABLE_FLTK)
#include "UI/FLTK/skfltkui.h"
SkFltkUI *ui = nullptr;
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static bool signaled = false;
static int kSig = -1;

static void kSignaNotify(int signal)
{
    //IT DOES NOT RETURN HERE!!
    if (skApp->isChildProcess())
        return;

    SkString sigName = strsignal(signal);//sys_siglist[signal];
    sigName.toUpperCase();

    if (logger->isEnabled())
    {
        if (logger->isEscapesEnabled())
            cerr << "\033[1;31m\n!!! KERNEL-SIGNAL emitted [id: " << signal << "; " << sigName << "] -> pid: " << getpid() << " !!!\n";

        else
            cerr << "\n!!! KERNEL-SIGNAL emitted [id: " << signal << "; " << sigName << "] -> pid: " << getpid() << " !!!\n";
    }

    if (signaled)
        return;

    signaled = true;
    kSig = signal;

    if (logger->isEnabled())
    {
        if (signal == SIGINT)
            cerr << "!!! [CTRL-C] ... \"skApp\" would going to die !!!\n\n";
        else
            cerr << "\n\n";

        if (logger->isEscapesEnabled())
        {
            cout << "\033[0m";
            cerr << "\033[0m";
        }
    }
}

SkApp::SkApp(int argc, char *argv[]) : SkEventLoop()
{
    argsCount = argc;
    argsVector = argv;

    //WARNING SETTING THIS COULD CHANGE FLOAT STRING RAPPRESENTATION from '.' to ','
    currentLocale = setlocale(LC_ALL, "");

    startingPID = ::getpid();
    kSignalsConnected = false;
    isSkChild = false;

    kernel_SIG = nullptr;
    kSig = -1;
    lastKernelSignal = kSig;

    skApp = this;

    setObjectName("App");

    xmlEscapes.getInternalVector().setObjectName(this, "XmlEscapes");
    parameters.getInternalVector().setObjectName(this, "Parameters");
    paths.getInternalVector().setObjectName(this, "Paths");
    knownMimeTypes.getInternalList().setObjectName(this, "KnownMimeTypes");
    threads.getInternalList().setObjectName(this, "Threads");
    sharedComponents.getInternalList().setObjectName(this, "SharedComponents");

    /*logger = */new SkOsEnv;
    osEnvironment->setObjectName(this, "OsEnvironment");
    osEnvironment->init(argsCount, argsVector);

    /*logger = */new SkLogMachine;
    logger->setObjectName(this, "Logger");

    cli = new SkCli(argc, argv);
}

SkApp::~SkApp()
{
    deleteEventLoopInternals();
    deleteAppInternals();
}

void SkApp::deleteAppInternals()
{
    threads.clear();
    knownMimeTypes.clear();
    xmlEscapes.clear();
    sharedComponents.clear();
    parameters.clear();
    paths.clear();

    delete osEnvironment;
}

#if defined(ENABLE_QTAPP)
#include <QtGlobal>
#endif

void SkApp::init(ulong fastTickInterval,
                 ulong slowTickInterval,
                 SkLoopTimerMode timerMode,
                 bool connectKernelSignals)
{

    LoopMessage("SpecialK Version: " << SpecialK_VERSION() << " [CodeName: " << SK_CODENAME << "]");

#if defined(ENABLE_QTAPP)
#if defined(__ANDROID__)
    LoopMessage("Executing under Qt/Android [Version: " << QT_VERSION_MAJOR << "." << QT_VERSION_MINOR << "." << QT_VERSION_PATCH << "]");
#else
    LoopMessage("Executing under Qt [Version: " << QT_VERSION_MAJOR << "." << QT_VERSION_MINOR << "." << QT_VERSION_PATCH << "]");

#endif
#endif

    LoopMessage("Localized as: " << currentLocale);

    kSignalsConnected = connectKernelSignals;

    if (kSignalsConnected)
    {
        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = kSignaNotify;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;

        sigaction(SIGTERM,      &sigIntHandler, nullptr);
        sigaction(SIGSTOP,      &sigIntHandler, nullptr);
        sigaction(SIGABRT,      &sigIntHandler, nullptr);
        sigaction(SIGQUIT,      &sigIntHandler, nullptr);
        sigaction(SIGINT,       &sigIntHandler, nullptr);
        sigaction(SIGCHLD,      &sigIntHandler, nullptr);

        //signal(SIGINT, SIG_IGN);
        signal(SIGPIPE, SIG_IGN);
        signal(SIGCHLD, SIG_IGN);
    }

    setupCostants();
    initLoop(fastTickInterval, slowTickInterval, timerMode);

    kernel_SIG = new SkStandaloneSignal;
    kernel_SIG->setObjectName(this, "Kernel_SIG");

#if defined(ENABLE_FLTK)
    /*ui = */new SkFltkUI;
    ui->start();
#endif
}

void SkApp::propagateEvent(SkObject *owner, CStr *evtFamily, CStr *evtName, SkVariantList *values)
{
    addEvent(owner, evtFamily, evtName, values);

    SkMutexLocker locker(&threadsEx);
    SkAbstractListIterator<SkPair<std::thread::id, SkThread *>> *itr = threads.iterator();

    while(itr->next())
    {
        SkThread *th = itr->item().value();
        th->thEventLoop()->addEvent(owner, evtFamily, evtName, values);
    }

    delete itr;
}

void SkApp::setAsChildProcess()
{
    isSkChild = true;
    LoopWarning("Here we are in a child process skApp");

    if (kSignalsConnected)
    {
        signal(SIGTERM, SIG_DFL);
        signal(SIGSTOP, SIG_DFL);
        signal(SIGABRT, SIG_DFL);
        signal(SIGQUIT, SIG_DFL);
        signal(SIGINT, SIG_DFL);
        signal(SIGCHLD, SIG_DFL);
        signal(SIGPIPE, SIG_DFL);

        kSignalsConnected = false;
    }
}

bool SkApp::isChildProcess()
{
    return isSkChild;
}

pid_t SkApp::getStartingPID()
{
    return startingPID;
}

#include "Core/Containers/skvariant.h"

void SkApp::timeout()
{
    if (kSig > -1 && !isSignaledToQuit())
    {
        lastKernelSignal = kSig;
        SkVariantVector p;
        p << kSig;
        kernel_SIG->pulse(this, p);
        kSig = -1;
        signaled = false;
    }

    onAppTick();
}

int SkApp::getLastKernelSignal()
{
    return lastKernelSignal;
}

void SkApp::loopStarted()
{
    uint64_t count;
    SkClassType **flatTypes = classTypesPtr(count);

    for(uint64_t i=0; i<count; i++)
    {
        const SkClassType &t = *(flatTypes[i]);

        if (t.super)
            LoopPlusDebug("Flat-type registered on start [ID: " << t.id << "; Super: " << t.super->name << "]: " << t.name);
        else
            LoopPlusDebug("Flat-type registered on start [ID: " << t.id << "]: " << t.name);

        for(uint64_t z=0; z<t.staticMethsCount; z++)
            LoopPlusDebug("Flat Static Method registered on start "
                        << "[with ReturnValue: " << SkVariant::boolToString(t.staticMethods[z]->hasReturn) << "]: "
                        << t.name << "::" << t.staticMethods[z]->name);

        for(uint64_t z=0; z<t.methsCount; z++)
            LoopPlusDebug("Flat Method registered on start "
                        << "[with ReturnValue: " << SkVariant::boolToString(t.methods[z]->hasReturn) << "]: "
                        << t.name << "::" << t.methods[z]->name);

        for(uint64_t z=0; z<t.slotsCount; z++)
            LoopPlusDebug("Slot Method registered on start "
                        << "[with ReturnValue: " << SkVariant::boolToString(t.slotsMethods[z]->getFlatMethod()->hasReturn) << "]: "
                        << t.name << "::" << t.slotsMethods[z]->getName());
    }

    SkClassTypeRegLambda **flatTypesRegLambdas = classTypesRegLambdasPtr(count);

    for(uint64_t i=0; i<count; i++)
    {
        const SkClassTypeRegLambda &t = *(flatTypesRegLambdas[i]);
        LoopPlusDebug("Flat-type REG Lambda [ID: " << i << "]: " << t.name);
    }

    SkEnumType **flatEnumTypes = enumTypesPtr(count);

    for(uint64_t i=0; i<count; i++)
    {
        const SkEnumType &t = *(flatEnumTypes[i]);
        LoopPlusDebug("FlatEnum-type registered on start [ID: " << t.id << "]: " << t.name);
        SkArgsMap &e = *t.enumeration;
        SkStringList l;
        e.keys(l);

        for(uint64_t z=1; z<l.count(); z++)
            LoopPlusDebug("FlatEnum-Value registered on start [ID: " << z-1 << "]: " << t.name << "::" << l[z]);
    }

    SkStructType **flatStructTypes = structTypesPtr(count);

    for(uint64_t i=0; i<count; i++)
    {
        const SkStructType &t = *(flatStructTypes[i]);
        LoopPlusDebug("FlatStruct-type registered on start [ID: " << t.id << "]: " << t.name);
    }

    onAppStarted();
}

void SkApp::quitFired()
{
#if defined(ENABLE_FLTK)
    ui->quit();
#endif
}

void SkApp::prepareClosingLoop()
{
}

void SkApp::closingLoop()
{
    SkMutexLocker locker(&threadsEx);
    SkAbstractListIterator<SkPair<std::thread::id, SkThread *>> *itr = threads.iterator();

    while(itr->next())
    {
        SkThread *th = itr->item().value();
        if (th->isRunning())
            LoopError("'" << th->objectName() << "' Thread is running on application close! Problems could happen ..");
    }

    delete itr;

    if (logger->isSysLogEnabled())
        logger->disableSysLog();
}

bool SkApp::registerComponent(SkObject *object)
{
    if (threadID() == object->threadID())
        return assignObjectToLoop(this, object);

    return assignObjectToThreadLoop(object);
}

bool SkApp::assignObjectToLoop(SkEventLoop *loop, SkObject *object)
{
    if (loop->containsComponent(object))
    {
        LoopError_EXT(loop, "Cannot REGISTER SkObject: '" << object->objectName()
                       << "'; it is ALREADY registered on '" << loop->objectName() << "'");

        return false;
    }

    loop->addComponent(object);
    return true;
}

SkEventLoop *SkApp::getEventLoopFromThreadID(SkThID id)
{
    SkMutexLocker locker(&threadsEx);
    SkAbstractListIterator<SkPair<std::thread::id, SkThread *>> *itr = threads.iterator();

    while(itr->next())
    {
        SkThread *th = itr->item().value();

        if (id == th->thEventLoop()->threadID())
        {
            delete itr;
            return th->thEventLoop();
        }
    }

    delete itr;

    if (id == threadID())
        return this;

    LoopError("THIS Thread ID is UNKNOWN for the skApp: " << id);
    return nullptr;
}


bool SkApp::assignObjectToThreadLoop(SkObject *object)
{
    SkMutexLocker locker(&threadsEx);
    SkAbstractListIterator<SkPair<std::thread::id, SkThread *>> *itr = threads.iterator();

    while(itr->next())
    {
        SkThread *th = itr->item().value();

        if (object->threadID() == th->thEventLoop()->threadID())
        {
            delete itr;
            return SkApp::assignObjectToLoop(th->thEventLoop(), object);
        }
    }

    delete itr;
    LoopError("CANNOT REGISTER OBJECT [" << object->objectName() << "] INSIDE LOOP OF ITS EXECUTION THREAD (SkThread UNKNOWN)");

    return false;
}

void SkApp::registerThread(SkThread *thread)
{
    std::thread::id tid = thread->getExecutionThID();
    SkMutexLocker locker(&threadsEx);
    threads.add(tid, thread);
}

void SkApp::deregisterThread(SkThread *thread)
{
    std::thread::id tid = thread->getExecutionThID();
    SkMutexLocker locker(&threadsEx);
    threads.remove(tid);
}

void SkApp::getSharedComponentsNames(SkStringList &names)
{
    SkMutexLocker locker(&sharedComponentsEx);
    sharedComponents.keys(names);
}

bool SkApp::setupSharedComponent(SkString name, SkObject *obj)
{
    SkMutexLocker locker(&sharedComponentsEx);

    if (sharedComponents.contains(name))
    {
        LoopError("Cannot SHARE SkObject: '" << obj->objectName() << "'; its name is ALREADY shared");
        return false;
    }

    sharedComponents[name] = obj;
    LoopPlusDebug("SHARED SkObject: '" << name << "'");
    return true;
}

bool SkApp::existsSharedComponent(SkString name)
{
    SkMutexLocker locker(&sharedComponentsEx);
    return sharedComponents.contains(name);
}

bool SkApp::existsSharedComponent(SkObject *obj)
{
    SkMutexLocker locker(&sharedComponentsEx);
    SkAbstractListIterator<SkPair<SkString, SkObject *>> *itr = sharedComponents.iterator();

    bool exists = false;

    while(itr->next() && !exists)
        if (itr->item().value() == obj)
            exists = true;

    delete itr;
    return exists;
}

SkString SkApp::getSharedComponentName(SkObject *obj)
{
    /*SkMutexLocker locker(&sharedComponentsEx);
    return sharedComponents.key(obj);*/
    SkMutexLocker locker(&sharedComponentsEx);
    SkAbstractListIterator<SkPair<SkString, SkObject *>> *itr = sharedComponents.iterator();

    bool exists = false;
    SkString n;

    while(itr->next() && !exists)
        if (itr->item().value() == obj)
        {
            n = itr->item().key();
            exists = true;
        }

    delete itr;
    return n;
}

SkObject *SkApp::getSharedComponent(SkString name)
{
    SkMutexLocker locker(&sharedComponentsEx);
    return sharedComponents.value(name);
}

bool SkApp::removeSharedComponent(SkString name)
{
    SkMutexLocker locker(&sharedComponentsEx);
    return (sharedComponents.remove(name).value() != sharedComponents.nv());
}

void SkApp::getGlobalParametersList(SkStringList &names)
{
    SkMutexLocker locker(&parametersEx);
    parameters.keys(names);
}

void SkApp::setGlobalParameter(SkString name, const SkVariant &value)
{
    SkMutexLocker locker(&parametersEx);
    parameters.add(name, value);
}

bool SkApp::existsGlobalParameter(SkString name)
{
    SkMutexLocker locker(&parametersEx);
    return parameters.contains(name);
}

bool SkApp::removeGlobalParameter(SkString name)
{
    SkMutexLocker locker(&parametersEx);
    return (parameters.remove(name).key() == name);
}

SkVariant &SkApp::getGlobalParameter(SkString name)
{
    SkMutexLocker locker(&parametersEx);
    return parameters.value(name);
}

void SkApp::getGlobalPathsList(SkStringList &names)
{
    SkMutexLocker locker(&pathsEx);
    paths.keys(names);
}

void SkApp::setGlobalPath(SkString name, SkString path)
{
    SkMutexLocker locker(&pathsEx);
    path = SkFsUtils::adjustPathEndSeparator(path.c_str());
    paths.add(name, path);
}

bool SkApp::existsGlobalPath(SkString name)
{
    SkMutexLocker locker(&pathsEx);
    return paths.contains(name);
}

bool SkApp::removeGlobalPath(SkString name)
{
    SkMutexLocker locker(&pathsEx);
    return (paths.remove(name).key() == name);
}

SkString SkApp::getGlobalPath(SkString name)
{
    SkMutexLocker locker(&pathsEx);
    SkString str = paths.value(name).toString();
    return SkFsUtils::adjustPathEndSeparator(str.c_str());
}

SkLinkedMap<SkString, SkString> *SkApp::getMimeTypes()
{
    return &knownMimeTypes;
}

SkArgsMap &SkApp::getXmlEscapes()
{
    return xmlEscapes;
}

SkOsEnv *SkApp::osEnv()
{
    return osEnvironment;
}

char **SkApp::getArgsVector()
{
    return argsVector;
}

int SkApp::getArgsCount()
{
    return argsCount;
}

SkCli *SkApp::appCli()
{
    return cli;
}

void SkApp::setupCostants()
{
    knownMimeTypes.add("json-app", "application/json");
    knownMimeTypes.add("js-app", "application/javascript");
    knownMimeTypes.add("xhtml-app", "application/xhtml+xml");
    knownMimeTypes.add("xml-app", "application/xml");
    knownMimeTypes.add("dtd-app", "application/xml-dtd");
    knownMimeTypes.add("xop-app", "application/xop+xml");
    knownMimeTypes.add("ps-app", "application/postscript");
    knownMimeTypes.add("pdf-app", "application/pdf");
    knownMimeTypes.add("ogg-app", "application/ogg");
    knownMimeTypes.add("zip-app", "application/zip");
    knownMimeTypes.add("gzip-app", "application/gzip");
    knownMimeTypes.add("gz-app", knownMimeTypes.value("gzip"));

    knownMimeTypes.add("cmd", "text/cmd");
    knownMimeTypes.add("css", "text/css");
    knownMimeTypes.add("csv", "text/csv");
    knownMimeTypes.add("json", "text/json");
    knownMimeTypes.add("js", "text/javascript");
    knownMimeTypes.add("txt", "text/plain");

    knownMimeTypes.add("rtf", "text/rtf");
    knownMimeTypes.add("html", "text/html");
    knownMimeTypes.add("htm", knownMimeTypes.value("html"));
    knownMimeTypes.add("xml", "text/xml");

    knownMimeTypes.add("ico", "image/x-icon");
    knownMimeTypes.add("gif", "image/gif");
    knownMimeTypes.add("jpeg", "image/jpeg");
    knownMimeTypes.add("jpg", knownMimeTypes.value("jpeg"));
    knownMimeTypes.add("jfif", knownMimeTypes.value("html"));
    knownMimeTypes.add("pjpeg", "image/pjpeg");
    knownMimeTypes.add("png", "image/png");
    knownMimeTypes.add("svg", "image/svg+xml");
    knownMimeTypes.add("tiff", "image/tiff");
    knownMimeTypes.add("tif", knownMimeTypes.value("tiff"));

    knownMimeTypes.add("avi", "video/avi");
    knownMimeTypes.add("mpeg", "video/mpg");
    knownMimeTypes.add("mpg", "video/mpg");
    knownMimeTypes.add("mp4", "video/mp4");
    knownMimeTypes.add("mkv", "video/x-matroska");
    knownMimeTypes.add("mov", "video/quicktime");
    knownMimeTypes.add("ogv", "video/ogv");
    knownMimeTypes.add("flv", "video/x-flv");
    knownMimeTypes.add("wmv", "video/x-ms-wmv");

    knownMimeTypes.add("igs", "model/iges");
    knownMimeTypes.add("iges", knownMimeTypes.value("igs"));
    knownMimeTypes.add("msh", "model/mesh");
    knownMimeTypes.add("mesh", knownMimeTypes.value("msh"));
    knownMimeTypes.add("wrl", "model/wrml");
    knownMimeTypes.add("wrml", knownMimeTypes.value("wrl"));

    knownMimeTypes.add("mp4-audio", "audio/mp4");
    knownMimeTypes.add("mpeg-audio", "audio/mpeg");
    knownMimeTypes.add("mka", "audio/x-matroska");
    knownMimeTypes.add("ogg", "audio/ogg");
    knownMimeTypes.add("oga", "audio/ogg");
    knownMimeTypes.add("opus", "audio/opus");
    knownMimeTypes.add("ra", "audio/vnd.rn-realaudio");
    knownMimeTypes.add("wav", "audio/vnd.wave");

    knownMimeTypes.add("eml", "message/rfc822");
    knownMimeTypes.add("email", knownMimeTypes.value("eml"));
    knownMimeTypes.add("mime", knownMimeTypes.value("eml"));
    knownMimeTypes.add("mht", knownMimeTypes.value("eml"));
    knownMimeTypes.add("mhtml", knownMimeTypes.value("eml"));

    //
    //HTML ESCAPEs
    xmlEscapes.add("\"", "&quot;");
    xmlEscapes.add("'", "&apos;");
    xmlEscapes.add("&", "&amp;");
    xmlEscapes.add("<", "&lt;");
    xmlEscapes.add(">", "&gt;");
    xmlEscapes.add(" ", "&nbsp;");
    xmlEscapes.add("¡", "&iexcl;");
    xmlEscapes.add("¢", "&cent;");
    xmlEscapes.add("£", "&pound;");
    xmlEscapes.add("¤", "&curren;");
    xmlEscapes.add("¥", "&yen;");
    xmlEscapes.add("¦", "&brvbar;");
    xmlEscapes.add("§", "&sect;");
    xmlEscapes.add("¨", "&uml;");
    xmlEscapes.add("©", "&copy;");
    xmlEscapes.add("ª", "&ordf;");
    xmlEscapes.add("«", "&laquo;");
    xmlEscapes.add("¬", "&not;");
    xmlEscapes.add(" ­", "&shy;");
    xmlEscapes.add("®", "&reg;");
    xmlEscapes.add("¯", "&macr;");
    xmlEscapes.add("°", "&deg;");
    xmlEscapes.add("±", "&plusmn;");
    xmlEscapes.add("²", "&sup2;");
    xmlEscapes.add("³", "&sup3;");
    xmlEscapes.add("´", "&acute;");
    xmlEscapes.add("µ", "&micro;");
    xmlEscapes.add("¶", "&para;");
    xmlEscapes.add("·", "&middot;");
    xmlEscapes.add("¸", "&cedil;");
    xmlEscapes.add("¹", "&sup1;");
    xmlEscapes.add("º", "&ordm;");
    xmlEscapes.add("»", "&raquo;");
    xmlEscapes.add("¼", "&frac14;");
    xmlEscapes.add("½", "&frac12;");
    xmlEscapes.add("¾", "&frac34;");
    xmlEscapes.add("¿", "&iquest;");
    xmlEscapes.add("×", "&times;");
    xmlEscapes.add("÷", "&divide;");
    xmlEscapes.add("À", "&Agrave;");
    xmlEscapes.add("Á", "&Aacute;");
    xmlEscapes.add("Â", "&Acirc;");
    xmlEscapes.add("Ã", "&Atilde;");
    xmlEscapes.add("Ä", "&Auml;");
    xmlEscapes.add("Å", "&Aring;");
    xmlEscapes.add("Æ", "&AElig;");
    xmlEscapes.add("Ç", "&Ccedil;");
    xmlEscapes.add("È", "&Egrave;");
    xmlEscapes.add("É", "&Eacute;");
    xmlEscapes.add("Ê", "&Ecirc;");
    xmlEscapes.add("Ë", "&Euml;");
    xmlEscapes.add("Ì", "&Igrave;");
    xmlEscapes.add("Í", "&Iacute;");
    xmlEscapes.add("Î", "&Icirc;");
    xmlEscapes.add("Ï", "&Iuml;");
    xmlEscapes.add("Ð", "&ETH;");
    xmlEscapes.add("Ñ", "&Ntilde;");
    xmlEscapes.add("Ò", "&Ograve;");
    xmlEscapes.add("Ó", "&Oacute;");
    xmlEscapes.add("Ô", "&Ocirc;");
    xmlEscapes.add("Õ", "&Otilde;");
    xmlEscapes.add("Ö", "&Ouml;");
    xmlEscapes.add("Ø", "&Oslash;");
    xmlEscapes.add("Ù", "&Ugrave;");
    xmlEscapes.add("Ú", "&Uacute;");
    xmlEscapes.add("Û", "&Ucirc;");
    xmlEscapes.add("Ü", "&Uuml;");
    xmlEscapes.add("Ý", "&Yacute;");
    xmlEscapes.add("Þ", "&THORN;");
    xmlEscapes.add("ß", "&szlig;");
    xmlEscapes.add("à", "&agrave;");
    xmlEscapes.add("á", "&aacute;");
    xmlEscapes.add("â", "&acirc;");
    xmlEscapes.add("ã", "&atilde;");
    xmlEscapes.add("ä", "&auml;");
    xmlEscapes.add("å", "&aring;");
    xmlEscapes.add("æ", "&aelig;");
    xmlEscapes.add("ç", "&ccedil;");
    xmlEscapes.add("è", "&egrave;");
    xmlEscapes.add("é", "&eacute;");
    xmlEscapes.add("ê", "&ecirc;");
    xmlEscapes.add("ë", "&euml;");
    xmlEscapes.add("ì", "&igrave;");
    xmlEscapes.add("í", "&iacute;");
    xmlEscapes.add("î", "&icirc;");
    xmlEscapes.add("ï", "&iuml;");
    xmlEscapes.add("ð", "&eth;");
    xmlEscapes.add("ñ", "&ntilde;");
    xmlEscapes.add("ò", "&ograve;");
    xmlEscapes.add("ó", "&oacute;");
    xmlEscapes.add("ô", "&ocirc;");
    xmlEscapes.add("õ", "&otilde;");
    xmlEscapes.add("ö", "&ouml;");
    xmlEscapes.add("ø", "&oslash;");
    xmlEscapes.add("ù", "&ugrave;");
    xmlEscapes.add("ú", "&uacute;");
    xmlEscapes.add("û", "&ucirc;");
    xmlEscapes.add("ü", "&uuml;");
    xmlEscapes.add("ý", "&yacute;");
    xmlEscapes.add("þ", "&thorn;");
    xmlEscapes.add("ÿ", "&yuml;");
}

#endif
