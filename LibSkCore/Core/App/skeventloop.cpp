#include "skeventloop.h"
#include "skapp.h"
#include "Core/System/Thread/skthread.h"
#include "Core/Object/skstandalonesignal.h"
#include "Core/System/skdatacrypt.h"
#include <unistd.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkLoopTimerMode)
{
    SetupEnumWrapper(SkLoopTimerMode);

    SetEnumItem(SkLoopTimerMode::SK_TIMEDLOOP_RT);
    SetEnumItem(SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING);
    SetEnumItem(SkLoopTimerMode::SK_EVENTLOOP_WAITING);
    SetEnumItem(SkLoopTimerMode::SK_FREELOOP);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_NOT_INSTANCIABLE(SkEventLoop);

DeclareMeth_INSTANCE_VOID(SkEventLoop, changeFastZone, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkEventLoop, changeSlowZone, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkEventLoop, changeTimerMode, Arg_Enum(SkLoopTimerMode))
DeclareMeth_INSTANCE_RET(SkEventLoop, containsComponent, bool, Arg_Custom(SkObject))
//DeclareMeth_INSTANCE_RET(SkEventLoop, getFastInterval, ulong)
//DeclareMeth_INSTANCE_RET(SkEventLoop, getSlowInterval, ulong)
DeclareMeth_INSTANCE_RET(SkEventLoop, getTimerMode, SkLoopTimerMode)
DeclareMeth_INSTANCE_VOID(SkEventLoop, quit)
DeclareMeth_INSTANCE_VOID(SkEventLoop, setExitCode, Arg_Int)
DeclareMeth_INSTANCE_RET(SkEventLoop, getExitCode, int)
DeclareMeth_INSTANCE_RET(SkEventLoop, getOwnerThread, SkThread *)
DeclareMeth_INSTANCE_RET(SkEventLoop, getLastPulseElapsedTime, double)
DeclareMeth_INSTANCE_RET(SkEventLoop, isSignaledToQuit, bool)
/*DeclareMeth_STATIC_VOID(SkEventLoop, sleep_ms, *Arg_Data(ulong))
DeclareMeth_STATIC_VOID(SkEventLoop, sleep_us, *Arg_Data(ulong))*/

SetupClassWrapper(SkEventLoop)
{
    AddEnumType(SkLoopTimerMode);
    SetClassSuper(SkEventLoop, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkEventLoop, changeFastZone);
    AddMeth_INSTANCE_VOID(SkEventLoop, changeSlowZone);
    AddMeth_INSTANCE_VOID(SkEventLoop, changeTimerMode);
    AddMeth_INSTANCE_RET(SkEventLoop, containsComponent);
    //AddMeth_INSTANCE_RET(SkEventLoop, getFastInterval);
    //AddMeth_INSTANCE_RET(SkEventLoop, getSlowInterval);
    AddMeth_INSTANCE_RET(SkEventLoop, getTimerMode);
    AddMeth_INSTANCE_VOID(SkEventLoop, quit);
    AddMeth_INSTANCE_VOID(SkEventLoop, setExitCode);
    AddMeth_INSTANCE_RET(SkEventLoop, getExitCode);
    AddMeth_INSTANCE_RET(SkEventLoop, getOwnerThread);
    AddMeth_INSTANCE_RET(SkEventLoop, getLastPulseElapsedTime);
    AddMeth_INSTANCE_RET(SkEventLoop, isSignaledToQuit);
    /*AddMeth_STATIC_VOID(SkEventLoop, sleep_ms);
    AddMeth_STATIC_VOID(SkEventLoop, sleep_us);*/
}

// // // // // // // // // // // // // // // // // // // // //

static CStr *txtTimerMode_t[] =
{
    "TIMEDLOOP_RT",
    "TIMEDLOOP_SLEEPING",
    "EVENTLOOP_WAITING",
    "FREELOOP"
};

SkLoopTimer::SkLoopTimer()
{
    mode = SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING;
    enabled = false;
    nsecs = 0;
    startTime = 0;
    virtualTime = 0;
    realTime = 0;
    delta = 0;
    tickNumber = 0;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkEventLoop::SkEventLoop(SkThread *owner)
{
    ownerTH = owner;
    started_SIG = nullptr;
    fastZone_SIG = nullptr;
    slowZone_SIG = nullptr;
    oneSecZone_SIG = nullptr;
    changed_SIG = nullptr;
    quitting_SIG = nullptr;
    finished_SIG = nullptr;

    signaledToQuit = false;
    tid = SkThread::currentThID();
    returnCode = 0;

    lastPulseElapsedTime = 0.;
    lastConsumedElapsedTime = 0.;

    slowInterval_USECS = 0;

    fastTickTimeAverage = 0.;
    fastTickTimeSum = 0.;

    consumedTimeAverage = 0.;
    consumedTimeAverageSum = 0.;

    avgCounter = 0;

    CreateClassWrapper(SkEventLoop);
}

SkEventLoop::~SkEventLoop()
{
    deleteEventLoopInternals();
}

void SkEventLoop::deleteEventLoopInternals()
{
    LoopWarning("LOOP DESTROYED");
}

void SkEventLoop::initLoop(ulong fastTickInterval,
                           ulong slowTickInterval,
                           SkLoopTimerMode timerMode)
{
    components.setObjectName(this, "Components");
    invokingSlots.getInternalList().setObjectName(this, "InvokingSlots");
    attachsToCreate.getInternalList().setObjectName(this, "AttachsToCreate");
    attachsToDestroy.getInternalList().setObjectName(this, "AttachsToDestroy");
    descriptors.setObjectName(this, "Descriptors");
    deathReign.setObjectName(this, "DeathReign");

    timer.enabled = true;
    timer.mode = timerMode;
    timer.nsecs = 0;

    ulong intervalNsecs = fastTickInterval * 1000;

    if (intervalNsecs >= 1000000000 || slowTickInterval >= 1000000)
    {
        LoopError("EventLoop CANNOT setup intervals over 1 second");
        return;
    }

    setInterval(intervalNsecs);
    slowInterval_USECS = slowTickInterval;

    LoopPlusDebug("Creating standalone signals");

    //THESE OBJECTs ARE DESTROYED WHEN THE OWNERLOOP (THIS) ARE FINISHING

    started_SIG = new SkStandaloneSignal;
    started_SIG->setObjectName(this, "Started_SIG");
    fastZone_SIG = new SkStandaloneSignal;
    fastZone_SIG->setObjectName(this, "FastZone_SIG");
    slowZone_SIG = new SkStandaloneSignal;
    slowZone_SIG->setObjectName(this, "SlowZone_SIG");
    oneSecZone_SIG = new SkStandaloneSignal;
    oneSecZone_SIG->setObjectName(this, "OneSecZone_SIG");
    changed_SIG = new SkStandaloneSignal;
    changed_SIG->setObjectName(this, "Changed_SIG");
    quitting_SIG = new SkStandaloneSignal;
    quitting_SIG->setObjectName(this, "Quitting_SIG");
    finished_SIG = new SkStandaloneSignal;
    finished_SIG->setObjectName(this, "Finished_SIG");
}

void SkEventLoop::setInterval(ulong tempNsecs)
{
    if (tempNsecs == 0 || tempNsecs > 999999999)
    {
        LoopError("EventLoop-RtTimer CANNOT setup interval; valid interval is (0, 1) seconds");
        return;
    }

    timer.nsecs = tempNsecs;
    timer.tickNumber = 0;
    timer.delta = 0;

    if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_RT)
        LoopWarning("Setup EventLoop-RtTimer interval " << tempNsecs/1000 << " us");

    else if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING)
        LoopWarning("Setup EventLoop-SleepingTimer interval " << tempNsecs/1000 << " us");

    else if (timer.mode == SkLoopTimerMode::SK_EVENTLOOP_WAITING)
        LoopWarning("Setup EventLoop-WaitingTimer interval " << tempNsecs/1000 << " us");

    else if (timer.mode == SkLoopTimerMode::SK_FREELOOP)
        LoopWarning("Setup EventLoop-FreeLoop");
}

void SkEventLoop::changeFastZone(ulong interval)
{
    if (interval < 1000000)
    {
        setInterval(interval * 1000);

        SkTimeSpec temporaryClock;
        ::clock_gettime(CLOCK_MONOTONIC, &temporaryClock);
        timer.startTime = static_cast<double>(temporaryClock.tv_sec) +
                ((static_cast<double>(temporaryClock.tv_nsec)) / 1000000000.);

        LoopWarning("Changing Fast interval " << interval << " us");

        changed_SIG->pulse();
    }

    else
        LoopError("Fast interval MUST be inside [0,1000000] microseconds " << interval);
}

void SkEventLoop::changeSlowZone(ulong interval)
{
    if (slowInterval_USECS > 0/* && slowInterval_USECS < 1000000*/)
    {
        slowInterval_USECS = interval;
        LoopWarning("Changing Slow interval " << interval << " us");

        changed_SIG->pulse();
    }
}
void SkEventLoop::changeTimerMode(SkLoopTimerMode mode)
{
    if (timer.mode == mode)
        return;

#if defined(ENABLE_QTAPP)
    if (this == skApp)
    {
        LoopError("CANNOT change EventLoop mode for skApp main thread");
        return;
    }
#endif

    timer.mode = mode;

    if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_RT)
        LoopWarning("Changing to EventLoop-RtTimer mode");

    else if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING)
        LoopWarning("Changing to EventLoop-SleepingTimer mode");

    else if (timer.mode == SkLoopTimerMode::SK_EVENTLOOP_WAITING)
        LoopWarning("Changing to EventLoop-WaitingTimer mode");

    else if (timer.mode == SkLoopTimerMode::SK_FREELOOP)
        LoopWarning("Changing to EventLoop-FreeLoop mode");

    changed_SIG->pulse();
}

int SkEventLoop::exec(SkEventLoopOnClosingSlot *onClosing, bool withLoop)
{
    /*if (timer.enabled)
        return -1;*/

    LoopPlusDebug("LOOP started");

    loopStarted();
    //timer.enabled = true;

    performAttachs();
    started_SIG->pulse(this);

    //SkTimeSpec temporaryClock;
    fastZoneElapsedTime.start();
    slowZoneElapsedTime.start();
    oneSecZoneElapsedTime.start();

    //USED IN SkLoopTimerMode::SK_TIMEDLOOP_RT
    ::clock_gettime(CLOCK_MONOTONIC, &temporaryClock);
    timer.startTime = static_cast<double>(temporaryClock.tv_sec) +
            ((static_cast<double>(temporaryClock.tv_nsec)) / 1000000000.);

    if (withLoop)
    {
        LoopMessage("Executing WITH loop");

        bool closed = false;
        while(loopStep(closed, onClosing));

        LoopMessage("Expired");
        return returnCode;
    }

    LoopWarning("Executing WITHOUT loop");
    return 0;
}

bool SkEventLoop::loopStep(bool &closed, SkEventLoopOnClosingSlot *onClosing)
{
    if (closed)
    {
        closingLoopProcedure();
        LoopWarning("The peace is come");
        timer.enabled = false;
        return false;
        //break;
    }

    else if (!timer.enabled)
    {
        invokeSlots();
        deliveryEvents();
        performDetachs();

        if (onClosing)
        {
            LoopWarning("Executing onClosing SLOT [" << onClosing->owner->objectName() << ", " << onClosing->name << "] ..");
            SkSlot *slt = searchSlot(onClosing->owner->typeID(), onClosing->name);

            if (slt)
                slt->call(onClosing->owner);
        }

        //CAN TRIGGER ONLY AS DIRECT
        finished_SIG->pulse(this);

        LoopWarning("Watching the light ..");

        garbageCollectorPrepare();
        garbageCollectorDestroy();

        timer.enabled = true;
        closed = true;
    }

    else if (!closed)
    {
        consumedElapsedTime.start();

        invokeSlots();
        deliveryEvents();
        performDetachs();
        performAttachs();
        garbageCollectorPrepare();
        garbageCollectorDestroy();

        fastTickTimeSum += getLastPulseElapsedTime();
        avgCounter++;

        fastZone_SIG->pulse(this);

        lastConsumedElapsedTime = consumedElapsedTime.stop();
        consumedTimeAverageSum += lastConsumedElapsedTime;

        loopTick(&temporaryClock);
        lastPulseElapsedTime = fastZoneElapsedTime.stop();
        fastZoneElapsedTime.start();

        if (slowZoneElapsedTime.stop() > slowInterval_USECS)
        {
            slowZone_SIG->pulse(this);
            slowZoneElapsedTime.start();
        }

        if (oneSecZoneElapsedTime.stop() >= 1.)
        {
            fastTickTimeAverage = (fastTickTimeSum/static_cast<double>(avgCounter));
            fastTickTimeSum = 0.;

            consumedTimeAverage = (consumedTimeAverageSum/static_cast<double>(avgCounter));
            consumedTimeAverageSum = 0.;

            avgCounter = 0;

            oneSecZone_SIG->pulse(this);
            oneSecZoneElapsedTime.start();
        }
    }

    return true;
}

void SkEventLoop::closingLoopProcedure()
{
    LoopDebug("Closing this LOOP");

    prepareClosingLoop();

    for(uint64_t i=0; i<components.count(); i++)
    {
        SkObject *o = components.at(i);

        if (!o->isPreparedToDie())
            o->destroyLater();
    }

    garbageCollectorPrepare();
    garbageCollectorDestroy();

    if (!components.isEmpty())
    {
        LoopWarning("There are some (" << components.count() << ") SkObjects alive on this EventLoop closing ..");
        for(uint64_t i=0; i<components.count(); i++)
        {
            SkObject *o = components.at(i);
            LoopWarning("SkObject alive: "
                       << o->isPreparedToDie() << ", "
                       << o->typeName()
                       << " [" << o->objectName() << "]");
        }
    }

    started_SIG = nullptr;
    fastZone_SIG = nullptr;
    slowZone_SIG = nullptr;
    oneSecZone_SIG = nullptr;
    changed_SIG = nullptr;
    quitting_SIG = nullptr;
    finished_SIG = nullptr;

    closingLoop();

    LoopWarning("LOOP finished [ret: " << returnCode << "]");
}

void SkEventLoop::loopTick(SkTimeSpec *temporaryClock)
{
    /*if (!timer.enabled)
        return;*/

    if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_RT)
    {
        ::clock_gettime(CLOCK_MONOTONIC, temporaryClock);

        timer.realTime = static_cast<double>(temporaryClock->tv_sec) +
                (static_cast<double>(temporaryClock->tv_nsec) / 1000000000.);

        {
            timer.virtualTime = timer.startTime +
                    (static_cast<double>(timer.tickNumber)
                     * (static_cast<double>(timer.nsecs) / 1000000000.));

            timer.delta = (timer.realTime - timer.virtualTime) * 1000000000.;
            temporaryClock->tv_nsec = static_cast<long>(timer.nsecs - static_cast<uint64_t>(timer.delta));
            timer.tickNumber++;
        }

        temporaryClock->tv_sec = 0;
        ::nanosleep(temporaryClock, nullptr);
    }

    else if (timer.mode == SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING)
    {
        ::usleep(static_cast<useconds_t>(timer.nsecs / 1000));
    }

    else if (timer.mode == SkLoopTimerMode::SK_EVENTLOOP_WAITING)
    {
        if (descriptors.isEmpty())
            ::usleep(static_cast<useconds_t>(timer.nsecs / 1000));

        else
        {
            FD_ZERO(&readSet);

            //used for SkLoopTimerMode::SK_EVENTLOOP_WAITING
            selectTimeout.tv_sec = 0;
            selectTimeout.tv_usec = static_cast<suseconds_t>(timer.nsecs / 1000);

            SkAbstractListIterator<int> *itr = descriptors.iterator();

            while(itr->next())
                FD_SET(itr->item(), &readSet);

            delete itr;

            int activity = select(FD_SETSIZE, &readSet, nullptr, nullptr, &selectTimeout);

            if (activity < 0)
                LoopError("Select error");

            else if (activity > 0)
            {
                /*cout << activity << "\n";

                for(uint64_t i=0; i<descriptors.count(); i++)
                    if (FD_ISSET(descriptors[i] , &readSet))
                    {
                        cout << descriptors[i] << " " << selectTimeout.tv_sec << "\n";
                        break;
                    }*/
            }
        }
    }

    else if (timer.mode == SkLoopTimerMode::SK_FREELOOP)
    {}

    timeout();
}

void SkEventLoop::garbageCollectorDestroy()
{
    SkAbstractListIterator<SkObject *> *itr = deathReign.iterator();

    while(itr->next())
    {
        SkObject *component = itr->item();

        LoopDebug("Executing EFFECTIVE-DESTROY "
                   << component->typeName()
                   << " [" << component->objectName() << "] ..");

        delete component;
    }

    deathReign.clear();
    delete itr;
}

void SkEventLoop::garbageCollectorPrepare()
{
    SkMutexLocker locker(&componentsEx);
    uint64_t count = components.count();

    for(uint64_t i=count; i>0; i--)
    {
        SkObject *component = components.at(i-1);

        if (component->isPreparedToDie())
        {
            components.removeAt(i-1);
            componentRemoved(component);

            LoopDebug("Executing META-DESCRUCTOR for "
                       << component->typeName()
                       << " [" << component->objectName() << "]"
                       << " [remains: " << components.count() <<" objects] ..");

            component->getMetaDestructor()(component);
            deathReign << component;
        }
    }
}

/*void SkEventLoop::diffuseSignal(SkSignalDiffused *sigDiffused)
{
    if (sigDiffused->sig->getOwner()->isPreparedToDie())
    {
        delete sigDiffused;
        return;
    }

    SkMutexLocker locker(&diffusingSignalsEx);
    diffusingSignals.enqueue(sigDiffused);
}

void SkEventLoop::processSignals()
{
    SkSignalDiffused *sigDiffused = nullptr;

    diffusingSignalsEx.lock();
    SkQueue<SkSignalDiffused *> d = diffusingSignals;
    diffusingSignals.clear();
    diffusingSignalsEx.unlock();

    while(!d.isEmpty())
    {
        sigDiffused = d.dequeue();
        sigDiffused->sig->triggerFromLoop(this, sigDiffused->referer);
        delete sigDiffused;
    }
}*/

void SkEventLoop::invokeSlot(SkSlot *skSlot, SkObject *owner, SkFlatObject *referer, SkVariant **p, uint64_t pCount)
{
    if (owner->isPreparedToDie())
        return;

    SkInvokingQueuedSlot *queuedSlot = new SkInvokingQueuedSlot;
    queuedSlot->owner = owner;
    queuedSlot->referer = referer;
    queuedSlot->arguments = p;//??? WOULD BE COPIED?
    queuedSlot->argsCount = pCount;
    queuedSlot->slot = skSlot;

    SkMutexLocker locker(&invokingSlotsEx);
    invokingSlots.enqueue(queuedSlot);
}

void SkEventLoop::invokeSlot(SkSlot *skSlot, SkObject *owner, SkFlatObject *referer, SkVariantVector &p)
{
    if (owner->isPreparedToDie())
        return;

    SkInvokingQueuedSlot *queuedSlot = new SkInvokingQueuedSlot;
    queuedSlot->owner = owner;
    queuedSlot->referer = referer;
    queuedSlot->argumentsCopy = p;
    queuedSlot->argsCount = p.count();

    if (!queuedSlot->argumentsCopy.isEmpty())
    {
        queuedSlot->arguments = new SkVariant * [queuedSlot->argumentsCopy.count()];

        for(uint64_t i=0; i<queuedSlot->argumentsCopy.count(); i++)
            queuedSlot->arguments[i] = &queuedSlot->argumentsCopy[p.count()-i-1];
    }

    queuedSlot->slot = skSlot;

    SkMutexLocker locker(&invokingSlotsEx);
    invokingSlots.enqueue(queuedSlot);
}

void SkEventLoop::invokeSlots()
{
    SkInvokingQueuedSlot *queuedSlot = nullptr;

    while(!invokingSlots.isEmpty())
    {
        invokingSlotsEx.lock();
        queuedSlot = invokingSlots.dequeue();
        invokingSlotsEx.unlock();

        queuedSlot->slot->call(queuedSlot->owner, queuedSlot->referer, queuedSlot->arguments, queuedSlot->argsCount);

        if (!queuedSlot->argumentsCopy.isEmpty())
            delete [] queuedSlot->arguments;

        delete queuedSlot;
    }
}

void SkEventLoop::addEvent(SkObject *owner, CStr *evtFamily, CStr *evtName, SkVariantList *values)
{
    SkDeliveringEvent *deliveringEvent = new SkDeliveringEvent;
    deliveringEvent->owner = owner;
    deliveringEvent->evtFamily = evtFamily;
    deliveringEvent->evtName = evtName;

    if (values)
        deliveringEvent->values = *values;

    SkMutexLocker locker(&deliveringEventsEx);
    deliveringEvents.enqueue(deliveringEvent);
}

void SkEventLoop::deliveryEvents()
{
    SkDeliveringEvent *deliveringEvent = nullptr;

    while(!deliveringEvents.isEmpty())
    {
        deliveringEventsEx.lock();
        deliveringEvent = deliveringEvents.dequeue();
        deliveringEventsEx.unlock();

        for(uint64_t i=0; i<components.count(); i++)
        {
            SkObject *component = components[i];

            if (deliveringEvent->owner == component)
                continue;

            component->onEvent(deliveringEvent);
        }

        delete deliveringEvent;
    }
}

bool SkEventLoop::containsComponent(SkObject *object)
{
    SkMutexLocker locker(&componentsEx);
    return components.contains(object);
}

void SkEventLoop::addComponent(SkObject *object)
{
    object->setEventLoop(this);

    LoopPlusDebug("NEW SkObject: "
               << object->typeName()
               << " [" << object->objectName() << "]");

    SkMutexLocker locker(&componentsEx);
    components.append(object);
    componentAdded(object);
}

void SkEventLoop::removeComponent(SkObject *object)
{
    SkMutexLocker locker(&componentsEx);

    if (!components.contains(object))
        return;

    LoopError("DEREGISTERING WITHOUT DESTROY: "
               << object->typeName()
               << " [" << object->objectName() << "]"
               << " [remain: " << components.count() <<" objects]");

    components.remove(object);
    componentRemoved(object);
}

void SkEventLoop::addDescriptor(int descriptor)
{    
    descriptors << descriptor;
    LoopDebug("Added descriptor to check-group: " << descriptor << " [max: " << FD_SETSIZE << "]");
}

void SkEventLoop::delDescriptor(int descriptor)
{
    descriptors.remove(descriptor);
    LoopDebug("Removed descriptor from check-group: " << descriptor);
}

ULong SkEventLoop::getFastInterval()
{
    return timer.nsecs/1000;
}

ULong SkEventLoop::getSlowInterval()
{
    return slowInterval_USECS;
}

SkLoopTimerMode SkEventLoop::getTimerMode()
{
    return timer.mode;
}

CStr *SkEventLoop::getTimerModeName()
{
    return txtTimerMode_t[timer.mode];
}

CStr *SkEventLoop::getTimerModeName(SkLoopTimerMode m)
{
    return txtTimerMode_t[m];
}

bool SkEventLoop::addAttachingItem(SkAttachingInfo *attachingInfo)
{
    SkMutexLocker locker(&attachsToCreateEx);
    attachsToCreate.enqueue(attachingInfo);

    LoopDebug("SkAttachingInfo ADDED: "
                << attachingInfo->source->objectName() << "." << attachingInfo->signalName
                << " -> "
                << attachingInfo->target->objectName() << "." << attachingInfo->slotName << " "
                << "(" << SkAttach::getAttachModeString(attachingInfo->mode) << "); "
                << "called from > " << attachingInfo->caller);

    return true;
}

void SkEventLoop::performAttachs()
{
    SkMutexLocker locker(&attachsToCreateEx);

    while(!attachsToCreate.isEmpty())
        addAttach(attachsToCreate.dequeue());
}

bool SkEventLoop::addDetachingItem(SkDetachingInfo *detachingInfo)
{
    SkMutexLocker locker(&attachsToDestroyEx);
    attachsToDestroy.enqueue(detachingInfo);
    LoopDebug("SkDetachingInfo ADDED: "
                << detachingInfo->source->objectName() << "." << detachingInfo->signalName
                << " -> "
                << detachingInfo->target->objectName() << "." << detachingInfo->slotName << "; "
                << "called from > " << detachingInfo->caller);

    return true;
}

void SkEventLoop::performDetachs()
{
    SkMutexLocker locker(&attachsToDestroyEx);

    while(!attachsToDestroy.isEmpty())
        delAttach(attachsToDestroy.dequeue());
}

void SkEventLoop::quit()
{
    if (!signaledToQuit)
    {
        LoopWarning("SIGNALED TO QUIT");

        quitFired();

        signaledToQuit = true;
        timer.enabled = false;
        quitting_SIG->pulse(this);
    }
}

void SkEventLoop::setExitCode(int code)
{
    returnCode = code;
}

int SkEventLoop::getExitCode()
{
    return returnCode;
}

SkThread *SkEventLoop::getOwnerThread()
{
    return ownerTH;
}

double SkEventLoop::getLastPulseElapsedTime()
{
    return lastPulseElapsedTime;
}

double SkEventLoop::getLastConsumedTime()
{
    return lastConsumedElapsedTime;
}

double SkEventLoop::getLastPulseElapsedTimeAverage()
{
    return fastTickTimeAverage;
}

double SkEventLoop::getConsumedTimeAverage()
{
    return consumedTimeAverage;
}

bool SkEventLoop::isSignaledToQuit()
{
    return signaledToQuit;
}

SkThID SkEventLoop::threadID()
{
    return tid;
}

/*void SkEventLoop::sleep_ms(ulong ms)
{
    usleep(ms*1000);
    //sleep_us(ms*1000);
}

void SkEventLoop::sleep_us(ulong us)
{
    struct timeval tmo;

    tmo.tv_sec = 0;
    tmo.tv_usec = static_cast<long>(us);

    select(0, nullptr, nullptr, nullptr, &tmo);
}*/

