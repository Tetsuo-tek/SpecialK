#include "skabstracttermwidgetlist.h"

SkAbstractTermWidgetList::SkAbstractTermWidgetList(SkTermUI *termUI) : SkAbstractTermLayout(termUI)
{}

uint64_t SkAbstractTermWidgetList::count()
{
    return lst.count();
}

uint64_t SkAbstractTermWidgetList::count(bool enabled)
{
    uint64_t counter = 0;

    for(uint64_t i=0; i<lst.count(); i++)
        if (lst[i]->isEnabled() == enabled)
            counter++;

    return counter;
}

