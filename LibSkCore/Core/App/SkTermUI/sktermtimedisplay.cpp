#include "sktermtimedisplay.h"
#include "sktermuiscreen.h"

SkTermTimeDisplay::SkTermTimeDisplay(SkTermUI *termUI) : SkAbstractTermWidgetList(termUI)
{
    disableCanvas();

    addCipher(new SkTermLcdCipher(screen));//Hours
    addCipher(new SkTermLcdCipher(screen));//Hours
    addCipher(new SkTermLcdCipher(screen));//Minutes
    addCipher(new SkTermLcdCipher(screen));//Minutes
    addCipher(new SkTermLcdCipher(screen));//Seconds
    addCipher(new SkTermLcdCipher(screen));//Seconds

    setSize(SkTermLcdCipherSize::CIP_SMALL);
}

void SkTermTimeDisplay::setSize(SkTermLcdCipherSize size)
{
    for(uint64_t i=0; i<lst.count(); i++)
        dynamic_cast<SkTermLcdCipher *>(lst[i])->setSize(size);

    sz.w = dynamic_cast<SkTermLcdCipher *>(lst[0])->getRealSize().w * 6;
    sz.w += 2;//separators

    sz.h = dynamic_cast<SkTermLcdCipher *>(lst[0])->getRealSize().h;
}

const SkTermSize &SkTermTimeDisplay::getRealSize()
{
    return sz;
}

void SkTermTimeDisplay::add(SkAbstractTermWidget *)
{
    FlatWarning("This add meth DO NOTHING");
}
void SkTermTimeDisplay::rem(uint64_t )
{
    FlatWarning("This rem meth DO NOTHING");
}

void SkTermTimeDisplay::rem(SkAbstractTermWidget *)
{
    FlatWarning("This rem meth DO NOTHING");
}

void SkTermTimeDisplay::addCipher(SkTermLcdCipher *cipher)
{
    SkAbstractTermWidget::setLayout(cipher, this);
    lst << cipher;
}

void SkTermTimeDisplay::setCiphersCoupleValue(uint64_t cipher1_ID, uint64_t cipher2_ID, int8_t val)
{
    SkTermLcdCipher *cipher1 = dynamic_cast<SkTermLcdCipher *>(lst[cipher1_ID]);
    SkTermLcdCipher *cipher2 = dynamic_cast<SkTermLcdCipher *>(lst[cipher2_ID]);

    if (val <= 9)
    {
        cipher1->set(0);
        cipher2->set(val);
    }

    else
    {
        cipher1->set(val/10);
        cipher2->set(val%10);
    }
}

void SkTermTimeDisplay::renderCipher(SkTermRegion *validRegion, SkTermLcdCipher *cipher)
{
    SkAbstractTermWidget::makeWidgetRender(cipher, validRegion);
    validRegion->origin.x += cipher->getRealSize().w;
}

void SkTermTimeDisplay::renderSeparator(SkTermRegion *validRegion, bool dark)
{
    if (dark)
    {
        screen->setFgColor(SkTermTextColor::COL_BLACK_BRIGHT);
        screen->setStyle(SkTermTextStyle::TXT_DIM);
    }

    validRegion->origin.x += 1;

    for(uint64_t i=0; i<2; i++)
        screen->print(validRegion->origin.x, validRegion->origin.y+i+2, U2584);

    validRegion->origin.x += 1;

    if (dark)
    {
        screen->unSetStyle(SkTermTextStyle::TXT_DIM);
        screen->unSetFgColor();
    }
}

void SkTermTimeDisplay::render(SkTermRegion *)
{
    dt = SkDateTime::currentDateTime();
    setCiphersCoupleValue(0, 1, static_cast<int8_t>(dt.getHour()));
    setCiphersCoupleValue(2, 3, static_cast<int8_t>(dt.getMinutes()));
    setCiphersCoupleValue(4, 5, static_cast<int8_t>(dt.getSeconds()));

    SkTermRegion validRegion;
    validRegion.origin.x = getCanvas().origin.x;
    validRegion.origin.y = getCanvas().origin.y;
    validRegion.size.w = 0;
    validRegion.size.h = 0;

    static bool dark = true;

    if (chrono.stop() >= 0.5)
    {
        dark = !dark;
        chrono.start();
    }

    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[0]));
    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[1]));

    renderSeparator(&validRegion, dark);

    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[2]));
    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[3]));

    renderSeparator(&validRegion, dark);

    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[4]));
    renderCipher(&validRegion, dynamic_cast<SkTermLcdCipher *>(lst[5]));
}
