#ifndef SKTERMLCDCIPHER
#define SKTERMLCDCIPHER

#include "skabstracttermwidget.h"

typedef enum
{
    CIP_SMALL,
    CIP_MEDIUM,
    CIP_LARGE,
    CIP_EXTRA
} SkTermLcdCipherSize;

typedef struct
{
    bool a;
    bool b;
    bool c;
    bool d;
    bool e;
    bool f;
    bool g;
} SkTermLcdCipherSegments;

class SkTermLcdCipher extends SkAbstractTermWidget
{
    public:
        SkTermLcdCipher(SkTermUI *termUI);

        void setSize(SkTermLcdCipherSize size);
        void set(int8_t cipher);
        int8_t get();

        SkTermLcdCipherSize getSize();
        const SkTermSize &getRealSize();

    protected:
        SkTermLcdCipherSize szMode;
        SkTermSize sz;
        SkTermLcdCipherSegments cipherSegments[10];
        int8_t v;

    private:
        void switcher(SkTermRegion *validRegion);
        void renderSegment(uint x, uint y, CStr *val, bool enlighted);
        void render(SkTermRegion *validRegion) override;
};

#endif // SKTERMLCDNUM
