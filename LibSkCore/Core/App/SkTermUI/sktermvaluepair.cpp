#include "sktermvaluepair.h"
#include "sktermuiscreen.h"
#include "sktermhorizbar.h"

SkTermValuePair::SkTermValuePair(SkTermUI *termUI, CStr *label, CStr *measureUnit) : SkAbstractTermWidget(termUI)
{
    b = nullptr;
    udm = measureUnit;
    setHorizAlignment(SkTermHorizAlignment::ALH_RIGHT);
    setLabel(label);
}

SkTermValuePair::SkTermValuePair(SkTermUI *termUI, CStr *label, const SkVariant &value, CStr *measureUnit) : SkAbstractTermWidget(termUI)
{
    b = nullptr;
    udm = measureUnit;
    setHorizAlignment(SkTermHorizAlignment::ALH_RIGHT);
    setLabel(label);
    set(value);
}

SkTermValuePair::SkTermValuePair(SkTermUI *termUI, CStr *label, SkAbstractTermBar *bar) : SkAbstractTermWidget(termUI)
{
    b = bar;
    udm = "%";
    setHorizAlignment(SkTermHorizAlignment::ALH_RIGHT);
    setLabel(label);
}

void SkTermValuePair::set(const SkVariant &val)
{
    v = val;

    if (b)
        b->set(v.toDouble());
}

SkVariant &SkTermValuePair::get()
{
    return v;
}

CStr *SkTermValuePair::getUDM()
{
    return udm.c_str();
}

SkAbstractTermBar *SkTermValuePair::getBar()
{
    return b;
}

void SkTermValuePair::render(SkTermRegion *validRegion)
{
    screen->print(lbl.c_str());
    screen->print(": ");

    if (b)
    {
        SkAbstractTermWidget::makeWidgetRender(b, nullptr);
        screen->print(" ");
    }

    else
    {
        if (validRegion || v.variantType() == SkVariant_T::T_STRING)
        {
            SkString str = v.toString();
            truncateText(str, validRegion->size.w);
        }

        else
            screen->print(v);
    }

    screen->print(" ");
    screen->print(udm.c_str());
}
