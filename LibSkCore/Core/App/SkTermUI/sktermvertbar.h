#ifndef SKTERMVERTBAR_H
#define SKTERMVERTBAR_H

#include "skabstracttermbar.h"

class SkTermVertBar extends SkAbstractTermBar
{
    public:
        SkTermVertBar(SkTermUI *termUI, CStr *label, uint length);

    private:
        void render(SkTermRegion *validRegion) override;
};

#endif // SKTERMVERTBAR_H
