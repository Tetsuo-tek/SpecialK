#include "sktermhorizbar.h"
#include "sktermuiscreen.h"

SkTermHorizBar::SkTermHorizBar(SkTermUI *termUI, CStr *label, uint length)
    : SkAbstractTermBar(termUI, label, length)
{
}

//  █  U2588 - 6
//  ▉  U2589
//  ▊  U258A
//  ▋  U258B
//  ▌  U258C
//  ▍  U258D
//  ▎  U258E
//  ▏  U258F - 0

static CStr *subValues[8] = {U258F, U258E, U258D, U258C, U258B, U258A, U2589, U2588};


void SkTermHorizBar::render(SkTermRegion *)
{
    if (!inc)
        return;

    double val = static_cast<double>(v)*inc;
    bool emptyOpen = false;

    for (double i=0; i<len; i++)
        if (i && i == val)
            screen->print(U2588);

        else if (i<val)
        {
            double u = val-i;

            if (u > 1.)
                screen->print(U2588);

            else
                screen->print(subValues[static_cast<int>(u*7.)]);
        }

        else
        {
            if (!emptyOpen)
            {
                emptyOpen = true;
                screen->setStyle(SkTermTextStyle::TXT_DIM);
                screen->setFgColor(SkTermTextColor::COL_BLACK_BRIGHT);
            }

            screen->print(U254D);
        }

    if (emptyOpen)
    {
        screen->unSetStyle(SkTermTextStyle::TXT_DIM);
        screen->unSetFgColor();
    }
}

