#ifndef SKTERMTIMEDISPLAY_H
#define SKTERMTIMEDISPLAY_H

#include "skabstracttermwidgetlist.h"
#include "sktermlcdcipher.h"
#include "Core/System/Time/skdatetime.h"
#include "Core/System/Time/skelapsedtime.h"

class SkTermTimeDisplay extends SkAbstractTermWidgetList
{
    public:
        SkTermTimeDisplay(SkTermUI *termUI);

        void setSize(SkTermLcdCipherSize size);
        const SkTermSize &getRealSize();

        // THEY DO NOTHING
        void add(SkAbstractTermWidget *) override;
        void rem(uint64_t ) override;
        void rem(SkAbstractTermWidget *) override;

    private:
        SkDateTime dt;
        SkTermSize sz;
        SkElapsedTime chrono;

        void render(SkTermRegion *) override;

        void addCipher(SkTermLcdCipher *cipher);
        void setCiphersCoupleValue(uint64_t cipher1_ID, uint64_t cipher2_ID, int8_t val);
        void renderCipher(SkTermRegion *validRegion, SkTermLcdCipher *cipher);
        void renderSeparator(SkTermRegion *validRegion, bool dark);
};

#endif // SKTERMTIMEDISPLAY_H
