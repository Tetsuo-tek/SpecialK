#include "sktermapplogger.h"
#include "sktermuiscreen.h"

/*SkTermAppLogger::SkTermAppLogger(SkTermUI *termUI) : SkAbstractTermLayout(termUI)
{
    logger->setAsLogQueue(&logs);
}

SkTermAppLogger::~SkTermAppLogger()
{
    logger->disableLogQueue();
}

void SkTermAppLogger::render(SkTermRegion *)
{
    uint col = getCanvas().origin.x;
    uint row = getCanvas().origin.y + SkAbstractTermLayout_TITLE_HEIGHT;;
    uint maxRow = getCanvas().size.h-5;

    if (!logs.isEmpty())
    {
        logger->getMutexEx().lock();
        SkAbstractListIterator<string> *itr = logs.iterator();

        while(itr->next())
        {
            logsBuffer.enqueue(itr->item());

            if (logsBuffer.count() > maxRow)
                logsBuffer.dequeue();
        }

        logs.clear();
        logger->getMutexEx().unlock();
        delete itr;
    }

    SkAbstractListIterator<string> *itr = logsBuffer.getInternalList().iterator();

    const char *headLine = "| * ";
    uint64_t headLineSz = SkString::length(headLine);
    CStr *closeLine = "|";
    uint64_t closeLineSz = SkString::length(closeLine);
    uint64_t totalRowSz = 0;

    while(itr->next())
    {
        SkString log = itr->item();
        totalRowSz = log.length();
        uint64_t wAvail = getCanvas().size.w - (headLineSz + closeLineSz);

        if (totalRowSz > wAvail)
        {
            log.chop(totalRowSz - wAvail + 10);
            log.append(" [..]");

            if (logger->isEscapesEnabled())
                log.append("\033[0m");
        }

        screen->print(col, row, headLine);
        screen->print(log.c_str());

        screen->print(getCanvas().origin.x + getCanvas().size.w - 1, row, closeLine);
        row++;
    }

    logger->getMutexEx().unlock();
    delete itr;
}*/

