#include "sktermwidgetvertlist.h"
#include "sktermuiscreen.h"

SkTermWidgetVertList::SkTermWidgetVertList(SkTermUI *termUI) : SkAbstractTermWidgetList(termUI)
{

}

void SkTermWidgetVertList::add(SkAbstractTermWidget *w)
{
    if (w && w->isLayout())
    {
        FlatError("Cannot add Layout as Widget");
        return;
    }

    SkAbstractTermWidget::setLayout(w, this);
    lst << w;
}

void SkTermWidgetVertList::rem(uint64_t id)
{
    lst.removeAt(id);
}

void SkTermWidgetVertList::rem(SkAbstractTermWidget *w)
{
    lst.remove(w);
}

#include "sktermvaluepair.h"

void SkTermWidgetVertList::render(SkTermRegion *)
{
    uint64_t maxLabelLen = 0;
    uint64_t tempLabelLen = 0;

    uint64_t maxUdmLen = 0;
    uint64_t tempUdmLen = 0;

    for(uint64_t i=0; i<lst.count(); i++)
        if (lst[i]->isEnabled())
        {
            SkTermValuePair *p = dynamic_cast<SkTermValuePair *>(lst[i]);

            if (p)
            {
                tempLabelLen = strlen(lst[i]->label());

                if (tempLabelLen > maxLabelLen)
                    maxLabelLen = tempLabelLen;

                tempUdmLen = strlen(p->getUDM());

                if (tempUdmLen > maxUdmLen)
                    maxUdmLen = tempUdmLen;
            }
        }

    if (maxUdmLen)
        maxUdmLen+=2;

    uint col = getCanvas().origin.x;
    uint row = getCanvas().origin.y;

    if (drawCanvas)
        row += SkAbstractTermLayout_TITLE_HEIGHT;

    CStr *headLine = "| ";
    CStr *closeLine = "|";
    tempLabelLen = 0;

    for(uint64_t i=0; i<lst.count(); i++)
        if (lst[i]->isEnabled())
        {
            SkTermRegion validRegion;
            validRegion.size.h = 0;

            validRegion.origin.y = row;
            screen->print(col, row, headLine);

            validRegion.origin.x = screen->getCurrentColumn();
            validRegion.size.w = maxLabelLen;

            SkTermValuePair *p = dynamic_cast<SkTermValuePair *>(lst[i]);

            if (p)
            {
                screen->setStyle(SkTermTextStyle::TXT_ITALIC);
                screen->setStyle(SkTermTextStyle::TXT_DIM);
                renderHorizontalAligned(p->label(), &validRegion, SkTermHorizAlignment::ALH_RIGHT);
                screen->print(": ");
                screen->unSetStyle(SkTermTextStyle::TXT_ITALIC);
                screen->unSetStyle(SkTermTextStyle::TXT_DIM);

                CStr *udm = p->getUDM();

                validRegion.size.w = getCanvas().size.w - (strlen(headLine) + maxLabelLen + 1 + 1 + maxUdmLen + strlen(closeLine));

                SkAbstractTermBar *bar = p->getBar();

                if (bar)
                {
                    SkAbstractTermWidget::makeWidgetRender(bar, nullptr);
                    screen->print(" ");
                    validRegion.size.w -= bar->getLen() + 1;
                }

                validRegion.origin.x = screen->getCurrentColumn();

                SkString str = p->get().toString();
                renderHorizontalAligned(str.c_str(), &validRegion, SkTermHorizAlignment::ALH_RIGHT);

                screen->print(" ");

                screen->setStyle(SkTermTextStyle::TXT_ITALIC);
                screen->setStyle(SkTermTextStyle::TXT_DIM);
                screen->print(udm);
                screen->unSetStyle(SkTermTextStyle::TXT_ITALIC);
                screen->unSetStyle(SkTermTextStyle::TXT_DIM);
            }

            else
            {
                validRegion.origin.x = screen->getCurrentColumn();
                validRegion.size.w = getCanvas().size.w - (strlen(headLine) + strlen(closeLine));

                SkAbstractTermWidget::makeWidgetRender(lst[i], &validRegion);
            }

            screen->print(getCanvas().origin.x + getCanvas().size.w - 1, row, closeLine);
            row++;
        }
}
