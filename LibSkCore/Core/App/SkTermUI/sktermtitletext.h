#ifndef SKTERMTITLETEXT_H
#define SKTERMTITLETEXT_H

#include "sktermlabeltext.h"

class SkTermTitleText extends SkTermLabelText
{
    public:
        SkTermTitleText(SkTermUI *termUI);
        SkTermTitleText(SkTermUI *termUI, CStr *label);

    private:
        void render(SkTermRegion *validRegion) override;
};

#endif // SKTERMTITLETEXT_H
