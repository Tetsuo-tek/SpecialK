#include "skabstracttermbar.h"
#include "sktermuiscreen.h"

SkAbstractTermBar::SkAbstractTermBar(SkTermUI *termUI, CStr *label, uint length) : SkAbstractTermWidget(termUI)
{
    len = length;
    v = 0.;
    inc = 0.;
    setLabel(label);
}

void SkAbstractTermBar::set(double val)
{
    if (val < 0. || val > 100.)
    {
        FlatError("The value must be a percent double [0.; 100.]");
        return;
    }

    v = val;
    inc = static_cast<double>(len)/static_cast<double>(100);
}

double SkAbstractTermBar::get()
{
    return v;
}

uint SkAbstractTermBar::getLen()
{
    return len;
}
