#ifndef SKTERMLABELTEXT_H
#define SKTERMLABELTEXT_H

#include "skabstracttermwidget.h"

class SkTermLabelText extends SkAbstractTermWidget
{
    public:
        SkTermLabelText(SkTermUI *termUI);
        SkTermLabelText(SkTermUI *termUI, CStr *label);

    protected:
        void renderTextLabel(SkTermRegion *validRegion);

    private:
        void render(SkTermRegion *validRegion) override;
};

#endif // SKTERMLABELTEXT_H
