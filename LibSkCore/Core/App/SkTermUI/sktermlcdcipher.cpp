#include "sktermlcdcipher.h"
#include "sktermuiscreen.h"

// ＋ \uFF0B
// − \u2212
// ▏ U258F
// ▕ */ U2595
// ▁ */ U2581

/*
         ▁
  I     ▕▁▏
        ▕▁▏
         ▁▁
  II    ▕▁▁▏
        ▕▁▁▏
         ▁▁▁
  III   ▕ A ▏
       F▕▁▁▁▏B
        ▕ G ▏
       E▕▁▁▁▏C
           D
         ▁▁▁▁
  IV    ▕    ▏
        ▕▁▁▁▁▏
        ▕    ▏
        ▕▁▁▁▁▏
*/
SkTermLcdCipher::SkTermLcdCipher(SkTermUI *termUI) : SkAbstractTermWidget(termUI)
{
    cipherSegments[0] = {true,  true,   true,   true,   true,   true,   false};
    cipherSegments[1] = {false, true,   true,   false,  false,  false,  false};
    cipherSegments[2] = {true,  true,   false,  true,   true,   false,  true};
    cipherSegments[3] = {true,  true,   true,   true,   false,  false,  true};
    cipherSegments[4] = {false, true,   true,   false,  false,  true,   true};
    cipherSegments[5] = {true,  false,  true,   true,   false,  true,   true};
    cipherSegments[6] = {true,  false,  true,   true,   true,   true,   true};
    cipherSegments[7] = {true,  true,   true,   false,  false,  false,  false};
    cipherSegments[8] = {true,  true,   true,   true,   true,   true,   true};
    cipherSegments[9] = {true,  true,   true,   true,   false,  true,   true};

    setSize(SkTermLcdCipherSize::CIP_SMALL);
    v = 8;
}

void SkTermLcdCipher::setSize(SkTermLcdCipherSize size)
{
    szMode = size;

    if (szMode == SkTermLcdCipherSize::CIP_SMALL)
    {
        sz.w = 3;
        sz.h = 3;
    }

    else if (szMode == SkTermLcdCipherSize::CIP_MEDIUM)
    {
        sz.w = 4;
        sz.h = 3;
    }

    else if (szMode == SkTermLcdCipherSize::CIP_LARGE)
    {
        sz.w = 5;
        sz.h = 5;
    }

    else if (szMode == SkTermLcdCipherSize::CIP_EXTRA)
    {
        sz.w = 6;
        sz.h = 5;
    }
}

void SkTermLcdCipher::set(int8_t cipher)
{
    v = cipher;
}

int8_t SkTermLcdCipher::get()
{
    return v;
}

SkTermLcdCipherSize SkTermLcdCipher::getSize()
{
    return szMode;
}

const SkTermSize &SkTermLcdCipher::getRealSize()
{
    return sz;
}

void SkTermLcdCipher::renderSegment(uint x, uint y, CStr *val, bool enlighted)
{
    if (enlighted)
        screen->setStyle(SkTermTextStyle::TXT_BOLD);

    else
    {
        screen->setFgColor(SkTermTextColor::COL_BLACK_BRIGHT);
        screen->setStyle(SkTermTextStyle::TXT_DIM);
    }

    screen->print(x,  y, val);

    if (enlighted)
        screen->unSetStyle(SkTermTextStyle::TXT_BOLD);

    else
    {
        screen->unSetStyle(SkTermTextStyle::TXT_DIM);
        screen->unSetFgColor();
    }
}

void SkTermLcdCipher::switcher(SkTermRegion *validRegion)
{
    SkTermLcdCipherSegments &segs = cipherSegments[v];

    if (szMode == SkTermLcdCipherSize::CIP_SMALL)
    {
        if (segs.a) screen->print(validRegion->origin.x+1,  validRegion->origin.y,      U2581);//A
        if (segs.f) screen->print(validRegion->origin.x,    validRegion->origin.y+1,    U2595);//F
        if (segs.g) screen->print(validRegion->origin.x+1,  validRegion->origin.y+1,    U2581);//G
        if (segs.b) screen->print(validRegion->origin.x+2,  validRegion->origin.y+1,    U258F);//B
        if (segs.e) screen->print(validRegion->origin.x,    validRegion->origin.y+2,    U2595);//E
        if (segs.d) screen->print(validRegion->origin.x+1,  validRegion->origin.y+2,    U2581);//D
        if (segs.c) screen->print(validRegion->origin.x+2,  validRegion->origin.y+2,    U258F);//C
    }

    else if (szMode == SkTermLcdCipherSize::CIP_MEDIUM)
    {
        if (segs.a) screen->print(validRegion->origin.x+1,  validRegion->origin.y,      U2581);//A
        if (segs.a) screen->print(validRegion->origin.x+2,  validRegion->origin.y,      U2581);//A
        if (segs.f) screen->print(validRegion->origin.x,    validRegion->origin.y+1,    U2595);//F
        if (segs.g) screen->print(validRegion->origin.x+1,  validRegion->origin.y+1,    U2581);//G
        if (segs.g) screen->print(validRegion->origin.x+2,  validRegion->origin.y+1,    U2581);//G
        if (segs.b) screen->print(validRegion->origin.x+3,  validRegion->origin.y+1,    U258F);//B
        if (segs.e) screen->print(validRegion->origin.x,    validRegion->origin.y+2,    U2595);//E
        if (segs.d) screen->print(validRegion->origin.x+1,  validRegion->origin.y+2,    U2581);//D
        if (segs.d) screen->print(validRegion->origin.x+2,  validRegion->origin.y+2,    U2581);//D
        if (segs.c) screen->print(validRegion->origin.x+3,  validRegion->origin.y+2,    U258F);//C
    }

    else if (szMode == SkTermLcdCipherSize::CIP_LARGE)
    {
        if (segs.a) screen->print(validRegion->origin.x+1,  validRegion->origin.y,      U2581);//A
        if (segs.a) screen->print(validRegion->origin.x+2,  validRegion->origin.y,      U2581);//A
        if (segs.a) screen->print(validRegion->origin.x+3,  validRegion->origin.y,      U2581);//A
        if (segs.f) screen->print(validRegion->origin.x,    validRegion->origin.y+1,    U2595);//F
        if (segs.f) screen->print(validRegion->origin.x,    validRegion->origin.y+2,    U2595);//F
        if (segs.g) screen->print(validRegion->origin.x+1,  validRegion->origin.y+2,    U2581);//G
        if (segs.g) screen->print(validRegion->origin.x+2,  validRegion->origin.y+2,    U2581);//G
        if (segs.g) screen->print(validRegion->origin.x+3,  validRegion->origin.y+2,    U2581);//G
        if (segs.b) screen->print(validRegion->origin.x+4,  validRegion->origin.y+1,    U258F);//B
        if (segs.b) screen->print(validRegion->origin.x+4,  validRegion->origin.y+2,    U258F);//B
        if (segs.e) screen->print(validRegion->origin.x,    validRegion->origin.y+3,    U2595);//E
        if (segs.e) screen->print(validRegion->origin.x,    validRegion->origin.y+4,    U2595);//E
        if (segs.d) screen->print(validRegion->origin.x+1,  validRegion->origin.y+4,    U2581);//D
        if (segs.d) screen->print(validRegion->origin.x+2,  validRegion->origin.y+4,    U2581);//D
        if (segs.d) screen->print(validRegion->origin.x+3,  validRegion->origin.y+4,    U2581);//D
        if (segs.c) screen->print(validRegion->origin.x+4,  validRegion->origin.y+3,    U258F);//C
        if (segs.c) screen->print(validRegion->origin.x+4,  validRegion->origin.y+4,    U258F);//C
    }

    else if (szMode == SkTermLcdCipherSize::CIP_EXTRA)
    {
        renderSegment(validRegion->origin.x+1,  validRegion->origin.y,      U2581, segs.a);//A
        renderSegment(validRegion->origin.x+2,  validRegion->origin.y,      U2581, segs.a);//A
        renderSegment(validRegion->origin.x+3,  validRegion->origin.y,      U2581, segs.a);//A
        renderSegment(validRegion->origin.x+4,  validRegion->origin.y,      U2581, segs.a);//A
        renderSegment(validRegion->origin.x,    validRegion->origin.y+1,    U2595, segs.f);//F
        renderSegment(validRegion->origin.x,    validRegion->origin.y+2,    U2595, segs.f);//F
        renderSegment(validRegion->origin.x+1,  validRegion->origin.y+2,    U2581, segs.g);//G
        renderSegment(validRegion->origin.x+2,  validRegion->origin.y+2,    U2581, segs.g);//G
        renderSegment(validRegion->origin.x+3,  validRegion->origin.y+2,    U2581, segs.g);//G
        renderSegment(validRegion->origin.x+4,  validRegion->origin.y+2,    U2581, segs.g);//G
        renderSegment(validRegion->origin.x+5,  validRegion->origin.y+1,    U258F, segs.b);//B
        renderSegment(validRegion->origin.x+5,  validRegion->origin.y+2,    U258F, segs.b);//B
        renderSegment(validRegion->origin.x,    validRegion->origin.y+3,    U2595, segs.e);//E
        renderSegment(validRegion->origin.x,    validRegion->origin.y+4,    U2595, segs.e);//E
        renderSegment(validRegion->origin.x+1,  validRegion->origin.y+4,    U2581, segs.d);//D
        renderSegment(validRegion->origin.x+2,  validRegion->origin.y+4,    U2581, segs.d);//D
        renderSegment(validRegion->origin.x+3,  validRegion->origin.y+4,    U2581, segs.d);//D
        renderSegment(validRegion->origin.x+4,  validRegion->origin.y+4,    U2581, segs.d);//D
        renderSegment(validRegion->origin.x+5,  validRegion->origin.y+3,    U258F, segs.c);//C
        renderSegment(validRegion->origin.x+5,  validRegion->origin.y+4,    U258F, segs.c);//C
    }
}

void SkTermLcdCipher::render(SkTermRegion *validRegion)
{
    if (v == 0)
        switcher(validRegion);

    else if (v == 1)
        switcher(validRegion);

    else if (v == 2)
        switcher(validRegion);

    else if (v == 3)
        switcher(validRegion);

    else if (v == 4)
        switcher(validRegion);

    else if (v == 5)
        switcher(validRegion);

    else if (v == 6)
        switcher(validRegion);

    else if (v == 7)
        switcher(validRegion);

    else if (v == 8)
        switcher(validRegion);

    else if (v == 9)
        switcher(validRegion);
}
