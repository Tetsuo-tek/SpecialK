#include "skabstracttermlayout.h"
#include "sktermuiscreen.h"

SkAbstractTermLayout::SkAbstractTermLayout(SkTermUI *termUI) : SkAbstractTermWidget(termUI)
{
    screen = termUI;

    enabled = true;

    drawCanvas = true;
    autoresizeEnabled = true;
    autoResizedMin = false;

    screen->addLayout(this);
}

void SkAbstractTermLayout::setCanvas(SkTermRegion &c)
{
    if (!c.origin.x || !c.origin.y || !c.size.w || !c.size.h)
    {
        FlatWarning("Canvas is NOT valid (some values is ZERO) -> x: " << c.origin.x
                  << " y: " << c.origin.y
                  << " w: " << c.size.w
                  << " h: " << c.size.h);
    }

    /*if (c.size.h < SkAbstractTermLayout_TITLE_HEIGHT+1)
    {
        FlatError("Canvas is NOT valid (height must be > " << SkAbstractTermLayout_TITLE_HEIGHT+1 << ") -> x: " << c.origin.x
                  << " y: " << c.origin.y
                  << " w: " << c.size.w
                  << " h: " << c.size.h);
    }*/

    canvas = c;
}

void SkAbstractTermLayout::setCanvas(uint origin_X, uint origin_Y, uint width, uint height)
{
    SkTermRegion c;
    c.origin = {origin_X, origin_Y};
    c.size = {width, height};

    setCanvas(c);
}

const SkTermRegion &SkAbstractTermLayout::getCanvas()
{
    return canvas;
}

void SkAbstractTermLayout::enableCanvas()
{
    drawCanvas = true;
}

void SkAbstractTermLayout::disableCanvas()
{
    drawCanvas = false;
}

bool SkAbstractTermLayout::isCanvasEnabled()
{
    return drawCanvas;
}

void SkAbstractTermLayout::renderCanvas()
{
    if (!drawCanvas)
        return;

    SkString sep = SkString::buildFilled('-', canvas.size.w);
    screen->print(canvas.origin.x, canvas.origin.y, sep.c_str());

    SkTermRegion c = canvas;
    c.origin.y++;

    renderHorizontalAligned(lbl.c_str(), &c, SkTermHorizAlignment::ALH_CENTER);

    screen->print(c.origin.x, c.origin.y, "|");
    screen->print(c.origin.x+c.size.w-1, c.origin.y, "|");

    screen->print(canvas.origin.x, canvas.origin.y+2, sep.c_str());
}

void SkAbstractTermLayout::checkCanvasResize()
{
    /*lastValidRegion.origin.x = canvas.origin.x;
    lastValidRegion.origin.y = canvas.origin.y;

    lastValidRegion.size.w = screen->getColumns() - lastValidRegion.origin.x;
    lastValidRegion.size.h = screen->getRows() - lastValidRegion.origin.y;

    if (lastValidRegion.size.w < canvas.size.w)
    {
        canvasHolder = canvas;
        canvas = lastValidRegion;
        autoResizedMin = true;
    }

    else if (autoResizedMin)
    {
        canvas = canvasHolder;
        autoResizedMin = false;
    }*/
}
