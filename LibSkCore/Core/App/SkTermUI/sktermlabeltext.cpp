#include "sktermlabeltext.h"
#include "sktermuiscreen.h"

SkTermLabelText::SkTermLabelText(SkTermUI *termUI) : SkAbstractTermWidget(termUI)
{

}

SkTermLabelText::SkTermLabelText(SkTermUI *termUI, CStr *label) : SkAbstractTermWidget(termUI)
{
    setLabel(label);
}

void SkTermLabelText::render(SkTermRegion *validRegion)
{
    renderTextLabel(validRegion);
}

void SkTermLabelText::renderTextLabel(SkTermRegion *validRegion)
{
    for(uint64_t i=0; i<styles.count(); i++)
        screen->setStyle(styles[i]);

    if (currentFgColor != SkTermTextColor::COL_NULL)
        screen->setFgColor(currentFgColor);

    if (currentBgColor != SkTermTextColor::COL_NULL)
        screen->setBgColor(currentBgColor);

    renderHorizontalAligned(lbl.c_str(), validRegion, ah);

    for(uint64_t i=0; i<styles.count(); i++)
        screen->unSetStyle(styles[i]);

    if (currentFgColor != SkTermTextColor::COL_NULL)
        screen->unSetFgColor();

    if (currentBgColor != SkTermTextColor::COL_NULL)
        screen->unSetBgColor();
}
