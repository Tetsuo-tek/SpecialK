/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTEXTSCREEN_H
#define SKTEXTSCREEN_H

#include "Core/Object/skobject.h"
#include "Core/System/Time/skelapsedtime.h"
#include "skabstracttermlayout.h"

class SPECIALK SkTermUI extends SkObject
{
    public:
        Constructor(SkTermUI, SkObject);

        bool init(double tickTimeSEC=.2);
        bool isInitialized();
        void shutdown();

        void setCursorCoord(uint x, uint y);
        void holdCursorCoord();
        void restoreCursorCoord();

        void moveToOrigin();
        void moveToFirstColumn();
        void moveToPrevRow();
        void moveToNextRow();
        void moveToUp(uint rows);
        void moveToDown(uint rows);
        void moveToLeft(uint columns);
        void moveToRight(uint columns);

        void hideCursor();
        void showCursor();

        void setStyle(SkTermTextStyle style);
        void unSetStyle(SkTermTextStyle style);

        void setFgColor(SkTermTextColor color);
        void unSetFgColor();

        void setBgColor(SkTermTextColor color);
        void unSetBgColor();

        void print(uint x, uint y, CStr *txt);
        void print(uint x, uint y, SkVariant &v);

        //ON CURRENT COORDINATEs
        void print(CStr *txt);
        void print(SkVariant &v);

        void printEmptyRow(uint x, uint y, uint64_t charNum);
        void printEmptyRow(uint64_t charNum);

        void clear();
        // 0 means current
        void clearRow(uint y=0);
        void clearColumn(uint x=0);

        uint getColumns();
        uint getRows();
        const SkTermSize &getScreenSize();

        uint getCurrentColumn();
        uint getCurrentRow();

        Signal(sizeChanged);
        Signal(started);
        Signal(halted);

        Slot(tick);

    private:
        bool initialized;
        SkElapsedTime chrono;
        double interval;

        SkList<SkAbstractTermLayout *> layouts;
        SkList<SkAbstractTermWidget *> widgets;

        SkTermPosition currentPosition;
        SkTermPosition holdedPosition;
        SkTermSize currentSize;

        friend class SkAbstractTermWidget;
        void addWidget(SkAbstractTermWidget *w);

        friend class SkAbstractTermLayout;
        void addLayout(SkAbstractTermLayout *l);

        void grabScreenSize();
};

//FOREGROUND ANSI COLORs
#define FG_BLACK            "\x1b[30m"
#define FG_RED              "\x1b[31m"
#define FG_GREEN            "\x1b[32m"
#define FG_YELLOW           "\x1b[33m"
#define FG_BLUE             "\x1b[34m"
#define FG_MAGENTA          "\x1b[35m"
#define FG_CYAN             "\x1b[36m"
#define FG_WHITE            "\x1b[37m"
#define FG_BLACK_BRIGHT     "\x1b[90m"
#define FG_RED_BRIGHT       "\x1b[91m"
#define FG_GREEN_BRIGHT     "\x1b[92m"
#define FG_YELLOW_BRIGHT    "\x1b[93m"
#define FG_BLUE_BRIGHT      "\x1b[94m"
#define FG_MAGENTA_BRIGHT   "\x1b[95m"
#define FG_CYAN_BRIGHT      "\x1b[96m"
#define FG_WHITE_BRIGHT     "\x1b[97m"
//#define FG_COLOR          "\x1b[38m"
#define FG_COLOR_DEFAULT    "\x1b[39m"

//BACKGROUND ANSI COLORs
#define BG_BLACK            "\x1b[40m"
#define BG_RED              "\x1b[41m"
#define BG_GREEN            "\x1b[42m"
#define BG_YELLOW           "\x1b[43m"
#define BG_BLUE             "\x1b[44m"
#define BG_MAGENTA          "\x1b[45m"
#define BG_CYAN             "\x1b[46m"
#define BG_WHITE            "\x1b[47m"
#define BG_BLACK_BRIGHT     "\x1b[100m"
#define BG_RED_BRIGHT       "\x1b[101m"
#define BG_GREEN_BRIGHT     "\x1b[102m"
#define BG_YELLOW_BRIGHT    "\x1b[103m"
#define BG_BLUE_BRIGHT      "\x1b[104m"
#define BG_MAGENTA_BRIGHT   "\x1b[105m"
#define BG_CYAN_BRIGHT      "\x1b[106m"
#define BG_WHITE_BRIGHT     "\x1b[107m"
//#define BG_COLOR          "\x1b[48m"
#define BG_COLOR_DEFAULT    "\x1b[49m"

#define STCOL_RESET         "\x1b[0m"

//STYLE
#define ST_NORMAL           "\x1b[22m"
#define ST_BOLD             "\x1b[1m"
#define ST_DIM              "\x1b[2m"
#define ST_ITALIC           "\x1b[3m"
#define ST_ITALIC_NOT       "\x1b[23m"
#define ST_UNDERLINE        "\x1b[4m"
#define ST_UNDERLINED_NOT   "\x1b[24m"
#define ST_BLINK_SLOW       "\x1b[5m"
#define ST_BLINK_FAST       "\x1b[6m"
#define ST_BLINK_NOT        "\x1b[25m"
#define ST_INVERSE          "\x1b[7m"
#define ST_INVERSE_NOT      "\x1b[27m"
#define ST_DELETED          "\x1b[9m"
#define ST_DELETED_NOT      "\x1b[29m"
#define ST_OVERLINED        "\x1b[53m"
#define ST_OVERLINED_NOT    "\x1b[55m"

//FONT
#define FT_DEFAULT          "\x1b[10m"
#define FT_ALTER_1          "\x1b[11m"
#define FT_ALTER_2          "\x1b[12m"
#define FT_ALTER_3          "\x1b[13m"
#define FT_ALTER_4          "\x1b[14m"
#define FT_ALTER_5          "\x1b[15m"
#define FT_ALTER_6          "\x1b[16m"
#define FT_ALTER_7          "\x1b[17m"
#define FT_ALTER_8          "\x1b[18m"
#define FT_ALTER_9          "\x1b[19m"

// UNICODE STANDARD BOX DRAW CHARACTERs

// \u250x
#define /* ─ */ U2500       "\u2500"
#define /* ━ */ U2501       "\u2501"
#define /* │ */ U2502       "\u2502"
#define /* ┃ */ U2503       "\u2503"
#define /* ┄ */ U2504       "\u2504"
#define /* ┅ */ U2505       "\u2505"
#define /* ┆ */ U2506       "\u2506"
#define /* ┇ */ U2507       "\u2507"
#define /* ┈ */ U2508       "\u2508"
#define /* ┉ */ U2509       "\u2509"
#define /* ┊ */ U250A       "\u250A"
#define /* ┋ */ U250B       "\u250B"
#define /* ┌ */ U250C       "\u250C"
#define /* ┍ */ U250D       "\u250D"
#define /* ┎ */ U250E       "\u250E"
#define /* ┏ */ U250F       "\u250F"

// \u251x
#define /* ┐ */ U2510       "\u2510"
#define /* ┑ */ U2511       "\u2511"
#define /* ┒ */ U2512       "\u2512"
#define /* ┓ */ U2513       "\u2513"
#define /* └ */ U2514       "\u2514"
#define /* ┕ */ U2515       "\u2515"
#define /* ┖ */ U2516       "\u2516"
#define /* ┗ */ U2517       "\u2517"
#define /* ┘ */ U2518       "\u2518"
#define /* ┙ */ U2519       "\u2519"
#define /* ┚ */ U251A       "\u251A"
#define /* ┛ */ U251B       "\u251B"
#define /* ├ */ U251C       "\u251C"
#define /* ┝ */ U251D       "\u251D"
#define /* ┞ */ U251E       "\u251E"
#define /* ┟ */ U251F       "\u251F"

// \u252x
#define /* ┠ */ U2520       "\u2520"
#define /* ┡ */ U2521       "\u2521"
#define /* ┢ */ U2522       "\u2522"
#define /* ┣ */ U2523       "\u2523"
#define /* ┤ */ U2524       "\u2524"
#define /* ┥ */ U2525       "\u2525"
#define /* ┦ */ U2526       "\u2526"
#define /* ┧ */ U2527       "\u2527"
#define /* ┨ */ U2528       "\u2528"
#define /* ┩ */ U2529       "\u2529"
#define /* ┪ */ U252A       "\u252A"
#define /* ┫ */ U252B       "\u252B"
#define /* ┬ */ U252C       "\u252C"
#define /* ┭ */ U252D       "\u252D"
#define /* ┮ */ U252E       "\u252E"
#define /* ┯ */ U252F       "\u252F"

// \u253x
#define /* ┰ */ U2530       "\u2530"
#define /* ┱ */ U2531       "\u2531"
#define /* ┲ */ U2532       "\u2532"
#define /* ┳ */ U2533       "\u2533"
#define /* ┴ */ U2534       "\u2534"
#define /* ┵ */ U2535       "\u2535"
#define /* ┶ */ U2536       "\u2536"
#define /* ┷ */ U2537       "\u2537"
#define /* ┸ */ U2538       "\u2538"
#define /* ┹ */ U2539       "\u2539"
#define /* ┺ */ U253A       "\u253A"
#define /* ┻ */ U253B       "\u253B"
#define /* ┼ */ U253C       "\u253C"
#define /* ┽ */ U253D       "\u253D"
#define /* ┾ */ U253E       "\u253E"
#define /* ┿ */ U253F       "\u253F"

// \u254x
#define /* ╀ */ U2540       "\u2540"
#define /* ╁ */ U2541       "\u2541"
#define /* ╂ */ U2542       "\u2542"
#define /* ╃ */ U2543       "\u2543"
#define /* ╄ */ U2544       "\u2544"
#define /* ╅ */ U2545       "\u2545"
#define /* ╆ */ U2546       "\u2546"
#define /* ╇ */ U2547       "\u2547"
#define /* ╈ */ U2548       "\u2548"
#define /* ╉ */ U2549       "\u2549"
#define /* ╊ */ U254A       "\u254A"
#define /* ╋ */ U254B       "\u254B"
#define /* ╌ */ U254C       "\u254C"
#define /* ╍ */ U254D       "\u254D"
#define /* ╎ */ U254E       "\u254E"
#define /* ╏ */ U254F       "\u254F"

// \u255x
#define /* ═ */ U2550       "\u2550"
#define /* ║ */ U2551       "\u2551"
#define /* ╒ */ U2552       "\u2552"
#define /* ╓ */ U2553       "\u2553"
#define /* ╔ */ U2554       "\u2554"
#define /* ╕ */ U2555       "\u2555"
#define /* ╖ */ U2556       "\u2556"
#define /* ╗ */ U2557       "\u2557"
#define /* ╘ */ U2558       "\u2558"
#define /* ╙ */ U2559       "\u2559"
#define /* ╚ */ U255A       "\u255A"
#define /* ╛ */ U255B       "\u255B"
#define /* ╜ */ U255C       "\u255C"
#define /* ╝ */ U255D       "\u255D"
#define /* ╞ */ U255E       "\u255E"
#define /* ╟ */ U255F       "\u255F"

// \u256x
#define /* ╠ */ U2560       "\u2560"
#define /* ╡ */ U2561       "\u2561"
#define /* ╢ */ U2562       "\u2562"
#define /* ╣ */ U2563       "\u2563"
#define /* ╤ */ U2564       "\u2564"
#define /* ╥ */ U2565       "\u2565"
#define /* ╦ */ U2566       "\u2566"
#define /* ╧ */ U2567       "\u2567"
#define /* ╨ */ U2568       "\u2568"
#define /* ╩ */ U2569       "\u2569"
#define /* ╪ */ U256A       "\u256A"
#define /* ╫ */ U256B       "\u256B"
#define /* ╬ */ U256C       "\u256C"
#define /* ╭ */ U256D       "\u256D"
#define /* ╮ */ U256E       "\u256E"
#define /* ╯ */ U256F       "\u256F"

// \u257x
#define /* ╰ */ U2570       "\u2570"
#define /* ╱ */ U2571       "\u2571"
#define /* ╲ */ U2572       "\u2572"
#define /* ╳ */ U2573       "\u2573"
#define /* ╴ */ U2574       "\u2574"
#define /* ╵ */ U2575       "\u2575"
#define /* ╶ */ U2576       "\u2576"
#define /* ╷ */ U2577       "\u2577"
#define /* ╸ */ U2578       "\u2578"
#define /* ╹ */ U2579       "\u2579"
#define /* ╺ */ U257A       "\u257A"
#define /* ╻ */ U257B       "\u257B"
#define /* ╼ */ U257C       "\u257C"
#define /* ╽ */ U257D       "\u257D"
#define /* ╾ */ U257E       "\u257E"
#define /* ╿ */ U257F       "\u257F"

// \u258x
#define /* ▀ */ U2580       "\u2580"
#define /* ▁ */ U2581       "\u2581"
#define /* ▂ */ U2582       "\u2582"
#define /* ▃ */ U2583       "\u2583"
#define /* ▄ */ U2584       "\u2584"
#define /* ▅ */ U2585       "\u2585"
#define /* ▆ */ U2586       "\u2586"
#define /* ▇ */ U2587       "\u2587"
#define /* █ */ U2588       "\u2588"
#define /* ▉ */ U2589       "\u2589"
#define /* ▊ */ U258A       "\u258A"
#define /* ▋ */ U258B       "\u258B"
#define /* ▌ */ U258C       "\u258C"
#define /* ▍ */ U258D       "\u258D"
#define /* ▎ */ U258E       "\u258E"
#define /* ▏ */ U258F       "\u258F"

// \u259x
#define /* ▐ */ U2590       "\u2590"
#define /* ░ */ U2591       "\u2591"
#define /* ▒ */ U2592       "\u2592"
#define /* ▓ */ U2593       "\u2593"
#define /* ▔ */ U2594       "\u2594"
#define /* ▕ */ U2595       "\u2595"
#define /* ▖ */ U2596       "\u2596"
#define /* ▗ */ U2597       "\u2597"
#define /* ▘ */ U2598       "\u2598"
#define /* ▙ */ U2599       "\u2599"
#define /* ▚ */ U259A       "\u259A"
#define /* ▛ */ U259B       "\u259B"
#define /* ▜ */ U259C       "\u259C"
#define /* ▝ */ U259D       "\u259D"
#define /* ▞ */ U259E       "\u259E"
#define /* ▟ */ U259F       "\u259F"

// MATH
#define /* ＋ */ UPLUS       "\uFF0B"
#define /* ╶╴ */ UMINUS      U2576 U2576
#endif // SKTEXTSCREEN_H
