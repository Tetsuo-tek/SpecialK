#ifndef SKTERMWIDGETHORIZLIST_H
#define SKTERMWIDGETHORIZLIST_H

#include "skabstracttermwidgetlist.h"
#include "Core/Containers/sklist.h"

class SkTermWidgetHorizList extends SkAbstractTermWidgetList
{
    public:
        SkTermWidgetHorizList(SkTermUI *termUI);

        void add(SkAbstractTermWidget *w) override;
        void rem(uint64_t id) override;
        void rem(SkAbstractTermWidget *w) override;

    private:
        void render(SkTermRegion *) override;
};

#endif // SKTERMWIDGETHORIZLIST_H
