#ifndef SKABSTRACTTERMWIDGET_H
#define SKABSTRACTTERMWIDGET_H

#include "Core/Containers/skstring.h"

enum SkTermTextStyle
{
    TXT_NORMAL,
    TXT_BOLD,
    TXT_DIM,
    TXT_ITALIC,
    TXT_UNDERLINED,
    TXT_BLINK_SLOW,
    TXT_BLINK_FAST,
    TXT_INVERSE,
    TXT_DELETED,
    TXT_OVERLINED
};

enum SkTermTextColor
{
    COL_NULL,
    COL_BLACK,
    COL_RED,
    COL_GREEN,
    COL_YELLOW,
    COL_BLUE,
    COL_MAGENTA,
    COL_CYAN,
    COL_WHITE,
    COL_BLACK_BRIGHT,
    COL_RED_BRIGHT,
    COL_GREEN_BRIGHT,
    COL_YELLOW_BRIGHT,
    COL_BLUE_BRIGHT,
    COL_MAGENTA_BRIGHT,
    COL_CYAN_BRIGHT,
    COL_WHITE_BRIGHT
};

struct SkTermPosition
{
    uint x;
    uint y;
};

struct SkTermSize
{
    uint w;
    uint h;
};

struct SkTermRegion
{
    SkTermPosition origin;
    SkTermSize size;
};

#ifdef TIOCGSIZE
    tyoedef struct ttysize TtySize;
#elif defined(TIOCGWINSZ)
    typedef struct winsize TtySize;
#endif

enum SkTermHorizAlignment
{
    ALH_AUTO,
    ALH_LEFT,
    ALH_CENTER,
    ALH_RIGHT
};

enum SkTermVertAlignment
{
    ALV_AUTO,
    ALV_UP,
    ALV_CENTER,
    ALV_BOTTOM
};

class SkTermUI;
class SkAbstractTermLayout;

class SkAbstractTermWidget extends SkFlatObject
{
    public:
        SkAbstractTermWidget(SkTermUI *termUI);

        void setEnabled(bool val);
        bool isEnabled();

        void setLabel(CStr *label);
        CStr *label();

        bool addStyle(SkTermTextStyle style);

        void setFgColor(SkTermTextColor color);
        SkTermTextColor fgColor();

        void setBgColor(SkTermTextColor color);
        SkTermTextColor bgColor();

        void setHorizAlignment(SkTermHorizAlignment a);
        SkTermHorizAlignment horizAlignment();

        void setVertAlignment(SkTermVertAlignment a);
        SkTermVertAlignment vertAlignment();

        virtual bool isLayout(){return false;}

    protected:
        bool enabled;

        SkTermUI *screen;
        SkString lbl;

        SkVector<SkTermTextStyle> styles;
        SkTermTextColor currentFgColor;
        SkTermTextColor currentBgColor;

        SkAbstractTermLayout *layout;

        SkTermVertAlignment av;
        SkTermHorizAlignment ah;

        friend class SkAbstractTermLayout;
        friend class SkTermUI;

        static void makeWidgetRender(SkAbstractTermWidget *w, SkTermRegion *validRegion);
        static void setLayout(SkAbstractTermWidget *w, SkAbstractTermLayout *l);

        virtual void render(SkTermRegion *) = 0;
        void renderHorizontalAligned(CStr *txt, SkTermRegion *validRegion, SkTermHorizAlignment mode);

        void truncateText(SkString &txt, uint availableColumns);
};

#endif // SKABSTRACTTERMWIDGET_H
