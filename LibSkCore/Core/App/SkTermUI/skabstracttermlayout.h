#ifndef SKABSTRACTTERMLAYOUT_H
#define SKABSTRACTTERMLAYOUT_H

#include "skabstracttermwidget.h"

#define SkAbstractTermLayout_TITLE_HEIGHT   3

class SkAbstractTermLayout extends SkAbstractTermWidget
{
    public:
        void setCanvas(SkTermRegion &c);
        void setCanvas(uint origin_X, uint origin_Y, uint width, uint height);
        const SkTermRegion &getCanvas();

        bool isLayout(){return true;}

        void enableCanvas();
        void disableCanvas();
        bool isCanvasEnabled();

    protected:
        SkAbstractTermLayout(SkTermUI *termUI);

        SkTermRegion canvas;
        bool drawCanvas;

        SkTermRegion lastValidRegion;

        bool autoresizeEnabled;
        SkTermRegion canvasHolder;
        bool autoResizedMin;

        friend class SkTermUI;
        void renderCanvas();

        void checkCanvasResize();
};

#endif // SKABSTRACTTERMLAYOUT_H
