#ifndef SKTERMVALUEPAIR_H
#define SKTERMVALUEPAIR_H

#include "skabstracttermbar.h"
#include "skabstracttermwidget.h"
#include "Core/Containers/skvariant.h"

class SkTermHorizBar;

class SkTermValuePair extends SkAbstractTermWidget
{
    public:
        SkTermValuePair(SkTermUI *termUI, CStr *label, CStr *measureUnit);
        SkTermValuePair(SkTermUI *termUI, CStr *label, const SkVariant &value, CStr *measureUnit);
        SkTermValuePair(SkTermUI *termUI, CStr *label, SkAbstractTermBar *bar);

        void set(const SkVariant &val);
        SkVariant &get();

        SkAbstractTermBar *getBar();
        CStr *getUDM();

    private:
        SkVariant v;
        SkString udm;
        SkAbstractTermBar *b;

        void render(SkTermRegion *validRegion) override;
};

#endif // SKTERMVALUEPAIR_H
