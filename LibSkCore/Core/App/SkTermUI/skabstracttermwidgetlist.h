#ifndef SKABSTRACTTERMWIDGETLIST_H
#define SKABSTRACTTERMWIDGETLIST_H

#include "skabstracttermlayout.h"
#include "Core/Containers/sklist.h"

class SkAbstractTermWidgetList extends SkAbstractTermLayout
{
    public:
        virtual void add(SkAbstractTermWidget *) = 0;

        template <class T>
        T get(uint64_t id)
        {
            T w = dynamic_cast<T>(lst[id]);
            return w;
        }

        virtual void rem(uint64_t) = 0;
        virtual void rem(SkAbstractTermWidget *) = 0;

        uint64_t count();
        uint64_t count(bool enabled);

    protected:
        SkAbstractTermWidgetList(SkTermUI *termUI);

        SkVector<SkAbstractTermWidget *> lst;
};

#endif // SKABSTRACTTERMWIDGETLIST_H
