#include "sktermwidgethorizlist.h"
#include "sktermuiscreen.h"

SkTermWidgetHorizList::SkTermWidgetHorizList(SkTermUI *termUI) : SkAbstractTermWidgetList(termUI)
{

}

void SkTermWidgetHorizList::add(SkAbstractTermWidget *w)
{
    if (w->isLayout())
    {
        FlatError("Cannot add Layout as Widget");
        return;
    }

    SkAbstractTermWidget::setLayout(w, this);
    lst << w;
}

void SkTermWidgetHorizList::rem(uint64_t id)
{
    lst.removeAt(id);
}

void SkTermWidgetHorizList::rem(SkAbstractTermWidget *w)
{
    lst.remove(w);
}

void SkTermWidgetHorizList::render(SkTermRegion *)
{
    uint col = getCanvas().origin.x;
    uint row = getCanvas().origin.y;

    if (drawCanvas)
        row += SkAbstractTermLayout_TITLE_HEIGHT;

    uint maxCol = getCanvas().size.w;

    uint64_t inc = maxCol/lst.count();

    if (inc == 0)
    {
        FlatError("Too many widget to show in this lengh: " << maxCol);
        return;
    }

    SkTermRegion validRegion;
    validRegion.origin.y = row;
    validRegion.size.w = inc;
    validRegion.size.h = 0;

    for(uint64_t i=0; i<lst.count(); i++)
        if (lst[i]->isEnabled())
        {
            screen->setCursorCoord(col, row);
            validRegion.origin.x = col;

            SkAbstractTermWidget::makeWidgetRender(lst[i], &validRegion);

            col += inc;

            if (col > screen->getColumns()-1)
                break;
        }
}
