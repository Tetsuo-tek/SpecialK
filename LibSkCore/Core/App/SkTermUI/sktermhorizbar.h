#ifndef SKTERMHORIZBAR_H
#define SKTERMHORIZBAR_H

#include "skabstracttermbar.h"

class SkTermHorizBar extends SkAbstractTermBar
{
    public:
        SkTermHorizBar(SkTermUI *termUI, CStr *label, uint length);

    private:
        CStr *createInterValueSymbol(double index, double val);
        void render(SkTermRegion *) override;
};

#endif // SKTERMHORIZBAR_H
