#include "skabstracttermwidget.h"
#include "sktermuiscreen.h"

SkAbstractTermWidget::SkAbstractTermWidget(SkTermUI *termUI)
{
    layout = nullptr;

    ah = SkTermHorizAlignment::ALH_AUTO;
    av = SkTermVertAlignment::ALV_AUTO;

    currentFgColor = SkTermTextColor::COL_NULL;
    currentBgColor = SkTermTextColor::COL_NULL;

    enabled = true;
    screen = termUI;

    screen->addWidget(this);
}

void SkAbstractTermWidget::setEnabled(bool val)
{
    enabled = val;
}

bool SkAbstractTermWidget::isEnabled()
{
    return enabled;
}

void SkAbstractTermWidget::setLabel(CStr *label)
{
    lbl = label;
}

CStr *SkAbstractTermWidget::label()
{
    return lbl.c_str();
}

bool SkAbstractTermWidget::addStyle(SkTermTextStyle style)
{
    if (style == SkTermTextStyle::TXT_NORMAL)
    {
        FlatError("Cannot add NORMAL style");
        return false;
    }

    for(uint64_t i=0; i<styles.count(); i++)
        if (styles[i] == style)
        {
            FlatError("Style is ALREADY active for this widget");
            return false;
        }

    styles << style;
    return true;
}

void SkAbstractTermWidget::setFgColor(SkTermTextColor color)
{
    currentFgColor = color;
}

SkTermTextColor SkAbstractTermWidget::fgColor()
{
    return currentFgColor;
}

void SkAbstractTermWidget::setBgColor(SkTermTextColor color)
{
    currentBgColor = color;
}

SkTermTextColor SkAbstractTermWidget::bgColor()
{
    return currentBgColor;
}

void SkAbstractTermWidget::setHorizAlignment(SkTermHorizAlignment a)
{
    ah = a;
}

SkTermHorizAlignment SkAbstractTermWidget::horizAlignment()
{
    return ah;
}

void SkAbstractTermWidget::setVertAlignment(SkTermVertAlignment a)
{
    av = a;
}

SkTermVertAlignment SkAbstractTermWidget::vertAlignment()
{
    return av;
}

void SkAbstractTermWidget::makeWidgetRender(SkAbstractTermWidget *w, SkTermRegion *validRegion)
{
    w->render(validRegion);
}

void SkAbstractTermWidget::setLayout(SkAbstractTermWidget *w, SkAbstractTermLayout *l)
{
    w->layout = l;
}

void SkAbstractTermWidget::renderHorizontalAligned(CStr *txt, SkTermRegion *validRegion, SkTermHorizAlignment mode)
{
    SkString str(txt);

    if (validRegion)
    {
        truncateText(str, validRegion->size.w);
        uint64_t offset = validRegion->size.w - str.length();

        if (mode == SkTermHorizAlignment::ALH_RIGHT || mode == SkTermHorizAlignment::ALH_CENTER)
        {
            if (mode == SkTermHorizAlignment::ALH_CENTER)
                offset /= 2;

            screen->setCursorCoord(validRegion->origin.x + offset, validRegion->origin.y);
        }
    }

    screen->print(str.c_str());
}

void SkAbstractTermWidget::truncateText(SkString &txt, uint availableColumns)
{
    uint64_t len = txt.length();

    if (len > availableColumns)
    {
        txt.chop(len-availableColumns+5);
        txt.append(" [..]");
    }
}
