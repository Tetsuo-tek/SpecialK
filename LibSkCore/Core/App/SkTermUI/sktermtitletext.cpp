#include "sktermtitletext.h"
#include "sktermuiscreen.h"

SkTermTitleText::SkTermTitleText(SkTermUI *termUI) : SkTermLabelText(termUI)
{

}

SkTermTitleText::SkTermTitleText(SkTermUI *termUI, CStr *label) : SkTermLabelText(termUI)
{
    setLabel(label);
}

void SkTermTitleText::render(SkTermRegion *validRegion)
{
    renderTextLabel(validRegion);
}
