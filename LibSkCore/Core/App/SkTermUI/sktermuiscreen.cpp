#include "sktermuiscreen.h"
#include "Core/App/skeventloop.h"
#include "Core/sklogmachine.h"

typedef struct
{
    CStr *style;
    CStr *styleNot;
} SkTermStylePair;

static SkTermStylePair *styles = nullptr;
static CStr **fgColors = nullptr;
static CStr **bgColors = nullptr;

struct SkTermDataLoader
{
    SkTermDataLoader()
    {
        styles = (SkTermStylePair *) malloc((SkTermTextStyle::TXT_OVERLINED+1)*sizeof(SkTermStylePair));

        styles[SkTermTextStyle::TXT_NORMAL]     = {ST_NORMAL,       nullptr};
        styles[SkTermTextStyle::TXT_BOLD]       = {ST_BOLD,         ST_NORMAL};
        styles[SkTermTextStyle::TXT_DIM]        = {ST_DIM,          ST_NORMAL};
        styles[SkTermTextStyle::TXT_ITALIC]     = {ST_ITALIC,       ST_ITALIC_NOT};
        styles[SkTermTextStyle::TXT_UNDERLINED] = {ST_UNDERLINE,    ST_UNDERLINED_NOT};
        styles[SkTermTextStyle::TXT_BLINK_SLOW] = {ST_BLINK_SLOW,   ST_BLINK_NOT};
        styles[SkTermTextStyle::TXT_BLINK_FAST] = {ST_BLINK_FAST,   ST_BLINK_NOT};
        styles[SkTermTextStyle::TXT_INVERSE]    = {ST_INVERSE,      ST_INVERSE_NOT};
        styles[SkTermTextStyle::TXT_DELETED]    = {ST_DELETED,      ST_DELETED_NOT};
        styles[SkTermTextStyle::TXT_OVERLINED]  = {ST_OVERLINED,    ST_OVERLINED_NOT};

        fgColors = (CStr **) malloc((SkTermTextColor::COL_WHITE_BRIGHT+1)*sizeof(CStr *));

        fgColors[SkTermTextColor::COL_NULL]             = FG_COLOR_DEFAULT;
        fgColors[SkTermTextColor::COL_BLACK]            = FG_BLACK;
        fgColors[SkTermTextColor::COL_RED]              = FG_RED;
        fgColors[SkTermTextColor::COL_GREEN]            = FG_GREEN;
        fgColors[SkTermTextColor::COL_YELLOW]           = FG_YELLOW;
        fgColors[SkTermTextColor::COL_BLUE]             = FG_BLUE;
        fgColors[SkTermTextColor::COL_MAGENTA]          = FG_MAGENTA;
        fgColors[SkTermTextColor::COL_CYAN]             = FG_CYAN;
        fgColors[SkTermTextColor::COL_WHITE]            = FG_WHITE;
        fgColors[SkTermTextColor::COL_BLACK_BRIGHT]     = FG_BLACK_BRIGHT;
        fgColors[SkTermTextColor::COL_RED_BRIGHT]       = FG_RED_BRIGHT;
        fgColors[SkTermTextColor::COL_GREEN_BRIGHT]     = FG_GREEN_BRIGHT;
        fgColors[SkTermTextColor::COL_YELLOW_BRIGHT]    = FG_YELLOW_BRIGHT;
        fgColors[SkTermTextColor::COL_BLUE_BRIGHT]      = FG_BLUE_BRIGHT;
        fgColors[SkTermTextColor::COL_MAGENTA_BRIGHT]   = FG_MAGENTA_BRIGHT;
        fgColors[SkTermTextColor::COL_CYAN_BRIGHT]      = FG_CYAN_BRIGHT;
        fgColors[SkTermTextColor::COL_WHITE_BRIGHT]     = FG_WHITE_BRIGHT;

        bgColors = (CStr **) malloc((SkTermTextColor::COL_WHITE_BRIGHT+1)*sizeof(CStr *));

        bgColors[SkTermTextColor::COL_NULL]             = BG_COLOR_DEFAULT;
        bgColors[SkTermTextColor::COL_BLACK]            = BG_BLACK;
        bgColors[SkTermTextColor::COL_RED]              = BG_RED;
        bgColors[SkTermTextColor::COL_GREEN]            = BG_GREEN;
        bgColors[SkTermTextColor::COL_YELLOW]           = BG_YELLOW;
        bgColors[SkTermTextColor::COL_BLUE]             = BG_BLUE;
        bgColors[SkTermTextColor::COL_MAGENTA]          = BG_MAGENTA;
        bgColors[SkTermTextColor::COL_CYAN]             = BG_CYAN;
        bgColors[SkTermTextColor::COL_WHITE]            = BG_WHITE;
        bgColors[SkTermTextColor::COL_BLACK_BRIGHT]     = BG_BLACK_BRIGHT;
        bgColors[SkTermTextColor::COL_RED_BRIGHT]       = BG_RED_BRIGHT;
        bgColors[SkTermTextColor::COL_GREEN_BRIGHT]     = BG_GREEN_BRIGHT;
        bgColors[SkTermTextColor::COL_YELLOW_BRIGHT]    = BG_YELLOW_BRIGHT;
        bgColors[SkTermTextColor::COL_BLUE_BRIGHT]      = BG_BLUE_BRIGHT;
        bgColors[SkTermTextColor::COL_MAGENTA_BRIGHT]   = BG_MAGENTA_BRIGHT;
        bgColors[SkTermTextColor::COL_CYAN_BRIGHT]      = BG_CYAN_BRIGHT;
        bgColors[SkTermTextColor::COL_WHITE_BRIGHT]     = BG_WHITE_BRIGHT;
    }
};

static SkTermDataLoader termDataLoader;

void SkTermUIClose(SkObject *obj)
{
    SkTermUI *f = dynamic_cast<SkTermUI *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkTermUIClose()");

    if (f->isInitialized())
        f->shutdown();
}

ConstructorImpl(SkTermUI, SkObject)
{
    initialized = false;

    currentPosition.x = 1;
    currentPosition.y = 1;

    currentSize.w = 1;
    currentSize.h = 1;

    interval = 0.;

    SignalSet(sizeChanged);
    SignalSet(started);
    SignalSet(halted);

    SlotSet(tick);

    addDtorCompanion(SkTermUIClose);
}

bool SkTermUI::init(double tickTimeSEC)
{
    if (interval != 0.)
    {
        ObjectError("TextScreen Already initialized");
        return false;
    }

    interval = tickTimeSEC;

    grabScreenSize();
    hideCursor();
    clear();

    Attach(eventLoop()->fastZone_SIG, pulse, this, tick, SkAttachMode::SkQueued);

    initialized = true;
    started();

    ObjectDebug("Initialized");

    return true;
}

bool SkTermUI::isInitialized()
{
    return initialized;
}

void SkTermUI::shutdown()
{
    initialized = false;
    ObjectDebug("Halted");

    clear();
    Detach(eventLoop()->fastZone_SIG, pulse, this, tick);

    SkAbstractListIterator<SkAbstractTermWidget *> *itr = widgets.iterator();

    while(itr->next())
        delete itr->item();

    delete itr;

    widgets.clear();
    layouts.clear();
    halted();
    showCursor();
}

void SkTermUI::setCursorCoord(uint x, uint y)
{
    if (!x || !y)
    {
        ObjectError("ScreenCoordinates start from 1");
        return;
    }

    printf("%c[%d;%dH", 0x1B, y, x);

    currentPosition.x = x;
    currentPosition.y = y;
}

void SkTermUI::holdCursorCoord()
{
    //printf("%s", "\x1b[s");
    holdedPosition = currentPosition;
}

void SkTermUI::restoreCursorCoord()
{
    //printf("%s", "\x1b[u");
    currentPosition = holdedPosition;
}

void SkTermUI::moveToOrigin()
{
    printf("%s", "\x1b[H");
    currentPosition.x = 1;
    currentPosition.y = 1;
}

void SkTermUI::moveToFirstColumn()
{
    printf("%c", '\r');
    currentPosition.x = 1;
}

void SkTermUI::moveToPrevRow()
{
    if (currentPosition.y > 1)
        return;

    printf("%s", "\x1b[M");
    currentPosition.y--;
}

void SkTermUI::moveToNextRow()
{
    if (currentPosition.y > currentSize.h-2)
        return;

    printf("%s", "\x1b[A");
    currentPosition.y++;
}

void SkTermUI::moveToUp(uint rows)
{
    if (currentPosition.y <= rows)
        return;

    SkString s("\x1b[");
    s.concat(rows);
    s.append("A");
    printf("%s", s.c_str());
    currentPosition.y -= rows;
}

void SkTermUI::moveToDown(uint rows)
{
    if (currentPosition.y + rows < currentSize.h)
        return;

    SkString s("\x1b[");
    s.concat(rows);
    s.append("B");
    printf("%s", s.c_str());
    currentPosition.y += rows;
}

void SkTermUI::moveToLeft(uint columns)
{
    if (currentPosition.x <= columns)
        return;

    SkString s("\x1b[");
    s.concat(columns);
    s.append("D");
    printf("%s", s.c_str());
    currentPosition.x -= columns;
}

void SkTermUI::moveToRight(uint columns)
{
    if (currentPosition.x + columns < currentSize.w)
        return;

    SkString s("\x1b[");
    s.concat(columns);
    s.append("C");
    printf("%s", s.c_str());
    currentPosition.x += columns;
}

void SkTermUI::hideCursor()
{
    printf("%s", "\x1b[?25l");
}

void SkTermUI::showCursor()
{
    printf("%s", "\033[?25h");
}

void SkTermUI::setStyle(SkTermTextStyle style)
{
    printf("%s", styles[style].style);
}

void SkTermUI::unSetStyle(SkTermTextStyle style)
{
    if (styles[style].styleNot)
        printf("%s", styles[style].styleNot);
}

void SkTermUI::setFgColor(SkTermTextColor color)
{
    printf("%s", fgColors[color]);
}

void SkTermUI::unSetFgColor()
{
    printf("%s", FG_COLOR_DEFAULT);
}

void SkTermUI::setBgColor(SkTermTextColor color)
{
    printf("%s", bgColors[color]);
}

void SkTermUI::unSetBgColor()
{
    printf("%s", BG_COLOR_DEFAULT);
}

void SkTermUI::print(uint x, uint y, CStr *txt)
{
    setCursorCoord(x, y);
    printf("%s", txt);
    fflush(stdout);

    currentPosition.x += SkString::length(txt);
}

void SkTermUI::print(CStr *txt)
{
    print(currentPosition.x, currentPosition.y, txt);
}

void SkTermUI::print(uint x, uint y, SkVariant &v)
{
    SkString str(v.toString());
    print(x, y, str.c_str());
}

void SkTermUI::print(SkVariant &v)
{
    print(currentPosition.x, currentPosition.y, v);
}

void SkTermUI::printEmptyRow(uint x, uint y, uint64_t charNum)
{
    SkString s = SkString::buildFilled(' ', charNum);
    print(x, y, s.c_str());
}

void SkTermUI::printEmptyRow(uint64_t charNum)
{
    SkString s = SkString::buildFilled(' ', charNum);
    print(currentPosition.x, currentPosition.y, s.c_str());
}

void SkTermUI::clear()
{
    printf("%s","\x1b[2J");
    fflush(stdout);
}

void SkTermUI::clearRow(uint y)
{
    uint tempCurrentY = currentPosition.y;

    if (y)
        setCursorCoord(currentPosition.x, y);

    printf("%s","\x1b[2K");

    if (y)
        setCursorCoord(currentPosition.x, tempCurrentY);
}

void SkTermUI::clearColumn(uint x)
{
    uint tempCurrentX = currentPosition.x;

    if (x)
        setCursorCoord(x, currentPosition.y);

    //

    if (x)
        setCursorCoord(tempCurrentX, currentPosition.y);
}

uint SkTermUI::getColumns()
{
    return currentSize.w;
}

uint SkTermUI::getRows()
{
    return currentSize.h;
}

const SkTermSize &SkTermUI::getScreenSize()
{
    return currentSize;
}

uint SkTermUI::getCurrentColumn()
{
    return currentPosition.x;
}

uint SkTermUI::getCurrentRow()
{
    return currentPosition.y;
}

void SkTermUI::grabScreenSize()
{
    TtySize ts;
#if defined(TIOCGSIZE)
    ioctl(0, TIOCGSIZE, &ts);
    columns = ts.ts_cols;
    rows = ts.ts_lines;
#elif defined(TIOCGWINSZ)
    ioctl(0, TIOCGWINSZ, &ts);
    currentSize.w = ts.ws_col;
    currentSize.h = ts.ws_row;
#endif
}

SlotImpl(SkTermUI, tick)
{
    SilentSlotArgsWarning();

    if (chrono.stop() > interval)
    {
        SkTermSize holdSize = currentSize;

        //NOT EFFICIENT
        clear();
        grabScreenSize();

        currentPosition.x = 1;
        currentPosition.y = 1;

        if (holdSize.h != currentSize.h || holdSize.w != currentSize.w)
        {
            clear();
            sizeChanged();
            return;
        }

        SkAbstractListIterator<SkAbstractTermLayout *> *itr = layouts.iterator();

        while(itr->next())
        {
            SkAbstractTermLayout &w = *itr->item();

            if (w.isEnabled())
            {
                const SkTermRegion &r = w.getCanvas();

                /*if (r.origin.x > 0 || r.origin.y > 0)
                {
                    if (r.origin.y == 0)
                        setCursorCoord(r.origin.x, currentPosition.y);

                    else if (r.origin.x == 0)
                        setCursorCoord(currentPosition.x, r.origin.y);

                    else
                        setCursorCoord(r.origin.x, r.origin.y);
                }*/

                setCursorCoord(r.origin.x, r.origin.y);

                if (r.origin.x < currentSize.w-5)
                {
                    w.checkCanvasResize();

                    w.render(nullptr);
                    w.renderCanvas();
                }
            }
        }

        delete itr;

        printf("%s", STCOL_RESET);
        pulse();
        chrono.start();
    }
}

void SkTermUI::addWidget(SkAbstractTermWidget *w)
{
    widgets << w;
}

void SkTermUI::addLayout(SkAbstractTermLayout *l)
{
    layouts << l;
}
