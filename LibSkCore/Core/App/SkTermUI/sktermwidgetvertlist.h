#ifndef SKTERMWIDGETVERTLIST_H
#define SKTERMWIDGETVERTLIST_H

#include "skabstracttermwidgetlist.h"
#include "Core/Containers/sklist.h"

class SkTermWidgetVertList extends SkAbstractTermWidgetList
{
    public:
        SkTermWidgetVertList(SkTermUI *termUI);

        void add(SkAbstractTermWidget *w) override;
        void rem(uint64_t id) override;
        void rem(SkAbstractTermWidget *w) override;

    private:
        void render(SkTermRegion *) override;
};

#endif // SKTERMWIDGETVERTLIST_H
