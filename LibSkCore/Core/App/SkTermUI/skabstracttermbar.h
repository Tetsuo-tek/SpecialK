#ifndef SKABSTRACTTERMBAR_H
#define SKABSTRACTTERMBAR_H

#include "skabstracttermwidget.h"

class SkAbstractTermBar extends SkAbstractTermWidget
{
    public:
        void set(double val);
        double get();

        uint getLen();

    protected:
        SkAbstractTermBar(SkTermUI *termUI, CStr *label, uint length);

        uint len;
        double v;
        double inc;
};

#endif // SKABSTRACTTERMBAR_H
