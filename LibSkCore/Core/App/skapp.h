/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAPP_H
#define SKAPP_H

#if defined(SPECIALK_APPLICATION)

/**
 * @file        skapp.h
 * @brief       SpecialK Framework (a C+/- set with a special key added)
 * @author      Di Ottavio Daniele (aka Tetsuo)
 * @copyright   (C) 2019-2024, Daniele Di Ottavio (aka "Tetsuo - The Iron Man")
 * @version     1.0.0
*/

//THE PERFECTION IS IN THE MIDDLE
//C OR C++ ? ... THE ANSWER IS SIMPLE!
//C+/- With a Special-K

#include "skeventloop.h"
#include "Core/System/skcli.h"
#include "Core/System/Filesystem/skmimetype.h"
#include "Core/System/Thread/skthread.h"
#include "Core/Containers/skargsmap.h"

#include <signal.h>

/**
  * @brief SkApp
  *
  * It is the unique and static instance of the application service base for each
  * program that makes use of the LibSk* libraries.
  *
  * It is created in the 'main (..)' function at the start of the program and can be
  * reached in any part of the code including this header, through a pointer named
  * 'skApp'.
  *
  * The skApp, being a global object, can contain various entities to globalize,
  * such as variables (SkVariant), system paths and objects (SkObject objects and
  * their derivates).
  *
  * Its tasks also include a series of global static data initializations.
  *
  * The skApp also represents the main event handler thread and loop (SkEventLoop)
  * of the entire application.
  *
  * The skApp also manages optional threads and their associated event handlers.
  *

  * @code

    #include <Core/App/skapp.h>

    //it is required to make a reference to the global extern skApp pointer
    SkApp *skApp = nullptr;

    int main(int argc, char *argv[])
    {
        skApp = new SkApp(argc, argv);
        skApp->init(5000, 200000, SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING);

        //Where TestProg is a subclass of an SkObject, having an "init" slot

        TestProg *a = new TestProg;
        Attach(skApp->started_SIG, pulse, a, init, SkAttachMode::SkQueued);

        return skApp->exec();
    }

  * @endcode
*/

class SPECIALK SkApp extends SkEventLoop
{
    int argsCount;
    char **argsVector;
    SkCli *cli;

    SkString currentLocale;

    pid_t startingPID;
    bool kSignalsConnected;
    bool isSkChild;
    int lastKernelSignal;

    SkMutex threadsEx;
    SkLinkedMap<std::thread::id, SkThread *> threads;

    SkMutex sharedComponentsEx;
    SkLinkedMap<SkString, SkObject *> sharedComponents;

    SkMutex parametersEx;
    SkArgsMap parameters;

    SkMutex pathsEx;
    SkArgsMap paths;

    SkLinkedMap<SkString, SkString> knownMimeTypes;
    SkArgsMap xmlEscapes;

    public:
        /**
         * @brief Constructor
         * @param argc the number of arguments from main(..)
         * @param argv the list of arguments from main(..)
        */
        SkApp(int argc, char *argv[]);

        /**
         * @brief Destructor
        */
        ~SkApp();

        /**
         * @brief Initializes the the derived SkEventloop timing zones and behaviour;
         * the interval used for FAST-ZONE is an implicit precision attribute
         * about all included timers and chronometers
         * @param fastTickInterval microseconds interval for FAST-ZONE
         * @param slowTickInterval microseconds interval for SLOW-ZONE
         * @param timerMode looping mode
         * @param connectKernelSignals connection to kernel signal (ex: CTRL+C)
        */

        void init(ulong fastTickInterval=10000,
                  ulong slowTickInterval=100000,
                  SkLoopTimerMode timerMode=SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING,
                  bool connectKernelSignals=true);

        void propagateEvent(SkObject *owner, CStr *evtFamily, CStr *evtName, SkVariantList *values=nullptr);

        /**
         * @brief Internally used
         * @deprecated about subprocessing app-parts
        */
        void setAsChildProcess();

        /**
         * @brief Internally used
         * @deprecated about subprocessing app-parts
        */
        bool isChildProcess();

        /**
         * @brief Internally used
         * @deprecated about subprocessing app-parts
        */
        pid_t getStartingPID();

        /**
         * @brief Internally used; each SkObject registers itself on its SkEventLoop,
         * depending by container thread owning the new operator
         * @param object the registering object
        */
        bool registerComponent(SkObject *object);

        /**
         * @brief Internally used; each SkThread registers its thread when it starts
         * @param thread the registering thread
         * @return true if the object is not registered yet, otherwise it returns
         * false
        */
        void registerThread(SkThread *thread);

        /**
         * @brief Internally used; each SkThread deregisters its thread when it quits
         * @param thread the registering thread
         * @return true if the object is registered, otherwise it returns false
        */
        void deregisterThread(SkThread *thread);

        /**
         * Fills the names list of globalized objects shared within the
         * base-application
         * @param names the SkStringList to fill
        */
        void getSharedComponentsNames(SkStringList &names);

        /**
         * @brief Globalizes an object
         * @param name the sharing name for the object
         * @param obj the sharing object pointer
         * @return true if the object is not globalized yet, otherwise it
         * returns false
        */
        bool setupSharedComponent(SkString name, SkObject *obj);

        /**
         * @brief Checks the globalized condition for an object
         * @param name the sharing name for the object
         * @return true if the object is globalized, otherwise it returns false
        */
        bool existsSharedComponent(SkString name);

        /**
         * @brief Checks the globalized condition for an object
         * @param obj the object pointer
         * @return true if the object is globalized, otherwise it returns false
        */
        bool existsSharedComponent(SkObject *obj);

        /**
         * Gets a globalized object name starting from its pointer
         * @param obj the object pointer
         * @return the name of the object; if it does not exist an empty string
         * is returned
        */
        SkString getSharedComponentName(SkObject *obj);

        /**
         * @brief Gets a globalized object name starting from its pointer
         * @param name the sharing name for the object
         * @return the object pointer; if it does not exist a NULL pointer is
         * returned
        */
        SkObject *getSharedComponent(SkString name);

        /**
         * @brief Removes a globalized object from the base-application
         * @param name the sharing name for the object
         * @return true if the object is globalized, otherwise it returns false
        */
        bool removeSharedComponent(SkString name);

        /**
         * @brief Fills the names list of globalized variables shared within the
         * base-application
         * @param names the SkStringList to fill
        */
        void getGlobalParametersList(SkStringList &names);

        /**
         * @brief Globalizes a variable
         * @param name the sharing name for the variable
         * @param value the sharing variable
         * @return true if the variable is not globalized yet, otherwise it returns false
        */
        void setGlobalParameter(SkString name, const SkVariant &value);

        /**
         * @brief Checks the globalized condition for a variable
         * @param name the sharing name for the variable
         * @return true if the variable is globalized, otherwise it returns
         * false
        */
        bool existsGlobalParameter(SkString name);

        /**
         * @brief Removes a globalized variable from the base-application
         * @param name the sharing name for the variable
         * @return true if the variable is globalized, otherwise it returns
         * false
        */
        bool removeGlobalParameter(SkString name);

        /**
         * Gets a globalized variable
         * @param name the sharing name for the variable
         * @return the variable reference; if it does not exist an empty SkVariant is returned
        */
        SkVariant &getGlobalParameter(SkString name);

        /**
         * @brief Fills the names list of globalized paths shared within the
         * base-application
         * @param names the SkStringList to fill
        */
        void getGlobalPathsList(SkStringList &names);

        /**
         * @brief Globalizes a path (it will add '/' at the end if it has not)
         * @param name the sharing name for the path
         * @param value the sharing path
         * @return true if the path is not globalized yet, otherwise it returns
         * false
        */
        void setGlobalPath(SkString name, SkString path);

        /**
         * @brief Checks the globalized condition for a path
         * @param name the sharing name for the path
         * @return true if the path is globalized, otherwise it returns false
        */
        bool existsGlobalPath(SkString name);

        /**
         * @brief Removes a globalized path from the base-application
         * @param name the sharing name for the path
         * @return true if the path is globalized, otherwise it returns false
        */
        bool removeGlobalPath(SkString name);

        /**
         * @brief Gets a globalized path
         * @param name the sharing name for the path
         * @return the path string; if it does not exist an empty string is
         * returned
        */
        SkString getGlobalPath(SkString name);

        /**
         * @deprecated
        */
        SkLinkedMap<SkString, SkString> *getMimeTypes();

        /**
         * @brief Internally used by SkXmlWriter
        */
        SkArgsMap &getXmlEscapes();

        /**
         * @brief Gets the pointer relative to system-environment informations and
         * attributes
         * @return the pointer for the SkOsEnv object
        */
        SkOsEnv *osEnv();

        char **getArgsVector();
        int getArgsCount();

        SkCli *appCli();

        int getLastKernelSignal();

        SkEventLoop *getEventLoopFromThreadID(SkThID id);
        /**
         * @brief Public standalone signal; it notifies signals from kernel
        */
        SkStandaloneSignal *kernel_SIG;

    protected:
        void deleteAppInternals();

    private:
        void setupCostants();

        static bool assignObjectToLoop(SkEventLoop *loop, SkObject *object);
        bool assignObjectToThreadLoop(SkObject *object);

        void timeout()                      override;
        virtual void onAppStarted(){}
        virtual void onAppTick(){}

        void loopStarted()                  override;
        void quitFired()                    override;
        void prepareClosingLoop()           override;
        void closingLoop()                  override;
};

/**
 * Permits to use the global skApp pointer from anywhere in the application code
*/
extern SkApp *skApp;

#endif

#endif // SKAPP_H
