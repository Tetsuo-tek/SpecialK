/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKEVENTLOOP_H
#define SKEVENTLOOP_H

/**
 * @file skeventloop.h
*/

#include "Core/System/Time/skelapsedtime.h"
#include "Core/Object/skstandalonesignal.h"
#include "Core/Containers/skqueue.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/**
 * @brief The SkLoopTimerMode enum
 */
enum SkLoopTimerMode
{
    SK_TIMEDLOOP_RT,
    SK_TIMEDLOOP_SLEEPING,
    SK_EVENTLOOP_WAITING,
    SK_FREELOOP
};

/**
 * @brief The SkLoopTimer struct
 */
struct SkLoopTimer
{
    bool enabled;
    SkLoopTimerMode mode;
    uint64_t nsecs;
    uint64_t tickNumber;
    double delta;
    double startTime;
    double virtualTime;
    double realTime;

    SkLoopTimer();
};

struct SkInvokingQueuedSlot
{
    SkObject *owner;
    SkFlatObject *referer;
    SkVariant **arguments;
    SkVariantVector argumentsCopy;
    uint64_t argsCount;
    SkSlot *slot;
};

struct SkEventLoopOnClosingSlot
{
    SkObject *owner;
    CStr *name;
};

struct SkDeliveringEvent
{
    SkObject *owner;
    SkString evtFamily;
    SkString evtName;
    SkVariantList values;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkThread;

/**
  * @brief SkEventLoop
  *
  * It is the basis of the whole SpecialK framework.
  *
  * It is never used directly as a type since it is used by its handlers (SkApp
  * and SkThread); direct use would not make sense.
  *
  * It provides several features:
  *
  * * record and manage every aspect of the life path of each object derived
  * from the SkObject class, as well as a non-invasive garbage-collector that
  * deals with the actual destruction of objects;
  *
  * * the included Signal / Slot paradigm, which allows you to schedule
  * non-standard method calls between objects that do not necessarily know each
  * other and that do not necessarily live in the same thread (SkThread), without
  * having to use locks when they live in different threads;
  * * sort the Signal calls on the different SkEventLoop existing in the
  * application allowing the invocation of the required Slots on the target
  * objects;
  *
  * * the tick (or temporal pulse) of execution of the loop which can act in 4
  * different ways (SkLoopTimerMode):
  *
  * * pseudo-realtime;
  *
  * * coarse time (sleeping);
  *
  * * waiting with timeout and observing.
  *
  * * waiting with an external blocking timeout.
  *
  * It is strongly based on the calculation of the time that passes, creating 3
  * abstract temporal zones notified through 3 stand-alone signals (SkStandaloneSignal):
  *
  * 1. FAST-ZONE for the fastest pulsation;
  *
  * 2. SLOW-ZONE the slow pulsation;
  *
  * 3. ONESEC-ZONE the pulsation that occurs every second.
  *
  * Through the time zones of the event handler you can easily control repetitive
  * aspects of the application flow, without having to worry about things outside
  * the actual operations to be performed.
  *
  * The FAST-ZONE cannot exceed 1 second, while the SLOW-ZONE can go beyond.
  *
  * Other important stand-alone signals included are:
  *
  * * start notification;
  *
  * * notification of start of work completion;
  *
  * * notification of the end of the works.
  *
  * When an event handler dies, any still registered objects that have not been
  * requested to be destroyed also die before the loop work actually ends.
  *
*/

class SPECIALK SkEventLoop extends SkFlatObject
{
    public:
        /**
         * @brief Constructor (Internally used)
         * @param owner the owning thread
        */
        SkEventLoop(SkThread *owner=nullptr);

        /**
         * @brief Destructor
        */
        ~SkEventLoop() override;

        //INTERVALs MEASURE-UNIT IS 'microseconds' (1/1000000 second)

        /**
         * @brief Internally used; initializes the the SkEventloop timing zones and behaviour
         * @param fastTickInterval microseconds interval for FAST-ZONE
         * @param slowTickInterval microseconds interval for SLOW-ZONE
         * @param timerMode looping mode
        */
        void initLoop(ulong fastTickInterval,
                      ulong slowTickInterval,
                      SkLoopTimerMode timerMode);

        void addEvent(SkObject *owner, CStr *evtFamily, CStr *evtName, SkVariantList *values);

        /**
         * @brief Changes the current FAST-ZONE interval
         * @param interval microseconds
        */
        void changeFastZone(ulong interval);

        /**
         * @brief Changes the current SLOW-ZONE interval
         * @param interval microseconds
        */
        void changeSlowZone(ulong interval);

        /**
         * @brief Changes the current timing mode
         * @param mode looping mode
        */
        void changeTimerMode(SkLoopTimerMode mode);

        /**
         * @brief Start the loop
         * @param onClosing the SK_LOOPCLOSINGLAMBDA_T triggered on closing loop
        */
        int exec(SkEventLoopOnClosingSlot *onClosing=nullptr, bool withLoop=true);
        //WHITOUT LOOP IT NEED TO TICK BY HAND
        bool loopStep(bool &closed, SkEventLoopOnClosingSlot *onClosing=nullptr);

        /**
         * @brief Internally used; stores a slot to invoke as SkAttachMode::SkOneShotQueued
         * @param skSlot the invoked slot
         * @param owner the invoked slot owner
         * @param referer refering flatobject pointer (can be the same as the owner)
         * @param p SkVariants pointer
         * @param pCount the number of parameters
         * @return true on success, otherwise false
        */
        void invokeSlot(SkSlot *skSlot, SkObject *owner, SkFlatObject *referer, SkVariant **p=nullptr, uint64_t pCount=0);

        //PUT ARGUMENTs IN p, USING INVERSE ORDER
        void invokeSlot(SkSlot *skSlot, SkObject *owner, SkFlatObject *referer, SkVariantVector &p);

        /**
         * @brief Internally used; checks the existence of an object inside this loop
         * @param object the checking object
        */
        bool containsComponent(SkObject *object);

        /**
         * @brief Internally used; adds an object to this loop
         * @param mode the adding object
        */
        void addComponent(SkObject *object);

        /**
         * @brief Internally used; removes an object from this loop
         * @param mode the removing object
        */
        void removeComponent(SkObject *object);

        /**
         * @brief Internally used when the mode is SkLoopTimerMode::SK_EVENTLOOP_WAITING; adds a file-descriptor to this loop
         * @param descriptor the adding fd
        */
        void addDescriptor(int descriptor);

        /**
         * @brief Internally used when the mode is SkLoopTimerMode::SK_EVENTLOOP_WAITING; demoves a file-descriptor from this loop
         * @param mode the removing fd
        */
        void delDescriptor(int descriptor);

        /**
         * @brief Public standalone signal; it notifies when the loop starts
        */
        SkStandaloneSignal *started_SIG;

        /**
         * @brief Public standalone signal; it notifies when the FAST-ZONE ticks
        */
        SkStandaloneSignal *fastZone_SIG;

        /**
         * @brief Public standalone signal; it notifies when the SLOW-ZONE ticks
        */
        SkStandaloneSignal *slowZone_SIG;

        /**
         * @brief Public standalone signal; it notifies when the ONE-SEC-ZONE ticks
        */
        SkStandaloneSignal *oneSecZone_SIG;

        /**
         * @brief Public standalone signal; it notifies when the loop parameters change
        */
        SkStandaloneSignal *changed_SIG;

        /**
         * @brief Public standalone signal; it notifies when the loop initialize is quitting phase
        */
        SkStandaloneSignal *quitting_SIG;

        /**
         * @brief Public standalone signal; it notifies when the loop is died
        */
        SkStandaloneSignal *finished_SIG;

        /**
         * @brief Gets the FAST-ZONE interval
         * @return microseconds interval
        */
        ULong getFastInterval();

        /**
         * @brief Gets the SLOW-ZONE interval
         * @return microseconds interval
        */
        ULong getSlowInterval();

        /**
         * @brief Gets the SLOW-ZONE interval
         * @return looping mode
        */
        SkLoopTimerMode getTimerMode();
        CStr *getTimerModeName();
        static CStr *getTimerModeName(SkLoopTimerMode m);

        /**
         * @brief Internally used; stores an Attach that will be permormed to the next tick
         * @param attachingInfo attaching-info stucture
         * @return true if the Attach is not stored yet, otherwise it returns false
        */
        bool addAttachingItem(SkAttachingInfo *attachingInfo);

        /**
         * @brief Internally used; stores an Detach that will be permormed to the next tick
         * @param detachingInfo detaching-info stucture
         * @return true if the Detach is not stored yet, otherwise it returns false
        */
        bool addDetachingItem(SkDetachingInfo *detachingInfo);

        /**
         * Requests the loop quit; it is not blocking and it will be executed when possible
        */
        void quit();

        /**
         * @brief Sets the exit value for this loop
         * @param code the exit value
        */
        void setExitCode(int code);

        /**
         * @brief Gets the exits code for this loop
         * @return the exiting code
        */
        int getExitCode();

        /**
         * Gets the thread where this loop lives, if it is not the main thread
         * @return the owner thread, but if it is the main loop the return will be a NULL pointer
        */
        SkThread *getOwnerThread();

        /**
         * @brief Internally used; preform real destruction for objects prepared-to-die; it will be called on each tick of ONE-SEC-ZONE, ancd on loop quitting phase
        */
        void garbageCollectorPrepare();

        /**
         * @brief Gets the last real interval time for the FAST-ZONE
         * @return the interval in seconds
        */
        double getLastPulseElapsedTime();

        double getLastConsumedTime();

        /**
         * @brief Gets the average of the interval time for the FAST-ZONE
         * @return the average in seconds
        */
        double getLastPulseElapsedTimeAverage();

        double getConsumedTimeAverage();

        /**
         * @brief Checks if this looping i going to die
         * @return true if this loop is in the quitting phase
        */
        bool isSignaledToQuit();

        /**
         * @brief Gets the std::thread::id of the owner thread
         * @return the owner std::thread::id
        */
        SkThID threadID();

        /*static void sleep_ms(ulong ms);
        static void sleep_us(ulong us);*/

        void performAttachs();
        void performDetachs();

    protected:
        SkThread *ownerTH;
        SkThID tid;

        SkLoopTimer timer;

        volatile bool signaledToQuit;
        int returnCode;
        uint64_t slowInterval_USECS;

        SkMutex componentsEx;
        SkVector<SkObject *> components;

        SkMutex invokingSlotsEx;
        SkQueue<SkInvokingQueuedSlot *> invokingSlots;

        SkMutex deliveringEventsEx;
        SkQueue<SkDeliveringEvent *> deliveringEvents;

        SkMutex attachsToCreateEx;
        SkQueue<SkAttachingInfo *> attachsToCreate;

        SkMutex attachsToDestroyEx;
        SkQueue<SkDetachingInfo *> attachsToDestroy;

        double fastTickTimeAverage;
        double fastTickTimeSum;

        double consumedTimeAverage;
        double consumedTimeAverageSum;

        int avgCounter;

        SkElapsedTime fastZoneElapsedTime;
        double lastPulseElapsedTime;

        SkElapsedTime consumedElapsedTime;
        double lastConsumedElapsedTime;

        SkElapsedTimeMicros slowZoneElapsedTime;
        SkElapsedTime oneSecZoneElapsedTime;

        SkTimeSpec temporaryClock;

        //used for SkLoopTimerMode::SK_EVENTLOOP_WAITING
        fd_set readSet;
        //int readMaxD;
        struct timeval selectTimeout;
        SkList<int> descriptors;

        SkList<SkObject *> deathReign;

        void setInterval(ulong tempNsecs);
        void loopTick(SkTimeSpec *temporaryClock);

        void closingLoopProcedure();

        void invokeSlots();
        void deliveryEvents();

        void garbageCollectorDestroy();
        void deleteEventLoopInternals();

        virtual void timeout()                      {}
        virtual void componentAdded(SkObject *)     {}
        virtual void componentRemoved(SkObject *)   {}
        virtual void loopStarted()                  {}
        virtual void quitFired()                    {}
        virtual void prepareClosingLoop()           {}
        virtual void closingLoop()                  {}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKEVENTLOOP_H
