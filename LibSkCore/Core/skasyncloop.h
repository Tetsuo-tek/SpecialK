/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKASYNCLOOP_H
#define SKASYNCLOOP_H

#include "Core/Object/skflatobject.h"

//IT HAS SEMANTIC OF A SIMPLE FOR, BUT IT IS ASYNCHRONOUS

class SPECIALK SkAsyncLoop extends SkFlatObject
{
    public:
        /**
         * @brief Simple constructor
         */
        SkAsyncLoop();

        /**
         * @brief Sets the loop interval, as if it were a for contruct
         * @param from the counter to start from
         * @param to the counter limit (not included in the progression)
         */
        void setInterval(int64_t from, int64_t to);

        /**
         * @brief Sets the callback function/method to call on each loop tick
         * having the prototype as:
         * void callBack(int64_t &i);
         */
        void setCallBack(void (*cb)(int64_t &));

        /**
         * @brief Gets current value of the counter
         * @return the current counter value
         */
        int64_t &getCounter();

        /**
         * @brief Makes a step of the loop
         * @return true if the loop is not finished, otherwise false
         */
        bool tick();

        /**
         * @brief Reset the counter
         */
        void reset();

    protected:
        virtual void onLoop(){}

    private:
        int64_t max;
        int64_t min;
        int64_t counter;

        void (*callBack)(int64_t &);
};

#endif // SKASYNCLOOP_H
