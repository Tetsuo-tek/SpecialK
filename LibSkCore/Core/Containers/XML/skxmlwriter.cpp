#include "skxmlwriter.h"
#include "Core/Containers/skargsmap.h"
#include "Core/App/skapp.h"

#if defined(ENABLE_XML)

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkXmlWriter);

DeclareMeth_INSTANCE_VOID(SkXmlWriter, enableHtmlEscapes, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, setWriterDevice, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, setWriterDevice, 1, Arg_Custom(SkAbstractDevice), Arg_Bool)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, setWriterDevice, 2, Arg_Custom(SkAbstractDevice), Arg_Bool, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, createDocument)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, createDocument, 1, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, createElement, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, createTextElement, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, createTextElement, 1, Arg_CStr, Arg_CStr, Arg_Custom(SkArgsMap))
DeclareMeth_INSTANCE_VOID(SkXmlWriter, addAttribute, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, insertCharacters, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, closeElement)
DeclareMeth_INSTANCE_VOID(SkXmlWriter, closeDocument)

SetupClassWrapper(SkXmlWriter)
{
    SetClassSuper(SkXmlWriter, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkXmlWriter, enableHtmlEscapes);
    AddMeth_INSTANCE_VOID(SkXmlWriter, setWriterDevice);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, setWriterDevice, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, setWriterDevice, 2);
    AddMeth_INSTANCE_VOID(SkXmlWriter, createDocument);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, createDocument, 1);
    AddMeth_INSTANCE_VOID(SkXmlWriter, createElement);
    AddMeth_INSTANCE_VOID(SkXmlWriter, createTextElement);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkXmlWriter, createTextElement, 1);
    AddMeth_INSTANCE_VOID(SkXmlWriter, addAttribute);
    AddMeth_INSTANCE_VOID(SkXmlWriter, insertCharacters);
    AddMeth_INSTANCE_VOID(SkXmlWriter, closeElement);
    AddMeth_INSTANCE_VOID(SkXmlWriter, closeDocument);
}

// // // // // // // // // // // // // // // // // // // // //

SkXmlWriter::SkXmlWriter()
{
    CreateClassWrapper(SkXmlWriter);

    escapeHtml = true;
    lastTagCompleted = false;
    human = false;
}

/*
- Il bit più significativo di ogni sequenza composta di un singolo byte è sempre 0.

- I bit più significativi del primo di una sequenza di più byte indicano la
  lunghezza della sequenza. Questi bit sono 110 per le sequenze di due byte, e
  1110 per quelle di tre.

- I byte successivi al primo in una sequenza composta da più byte hanno sempre
  10 come bit più significativi.
*/

void SkXmlWriter::substituteEscapes(SkString &txt)
{
    SkString conv;
    bitset<8> bs;
    char c;

    for(uint64_t i=0; i<txt.size(); i++)
    {
        c = txt.charAt(i);

        if (c == '"' || c == '\'' || c == '&' || c == '<' || c == '>' /*|| (escapeHtml && c == ' ')*/)
        {
            char s[2];
            s[0] = c;
            s[1] = '\0';
            conv.concat(skApp->getXmlEscapes()[s].toString());
        }

        else
        {
            if (escapeHtml)
            {
                bs = c;

                if (!bs[7])
                    conv.concat(c);

                else if (bs[7] && bs[6] && !bs[5])
                {
                    char t[3];
                    t[0] = c;
                    t[1] = txt.charAt(i+1);
                    t[2] = '\0';

                    //cout << "!!!!!!!! SUBST " << t << "->" << htmlEscapes[t].toString() << "\n";
                    conv.append(skApp->getXmlEscapes()[t].toString());
                }

                else if (bs[7] && bs[6] && bs[5])
                {
                    char t[4];
                    t[0] = c;
                    t[1] = txt.charAt(i+1);
                    t[2] = txt.charAt(i+2);
                    t[3] = '\0';

                    //cout << "!!!!!!!! SUBST " << t << "->" << htmlEscapes[t].toString() << "\n";
                    conv.append(skApp->getXmlEscapes()[t].toString());
                }
            }

            else
                conv.concat(c);
        }


    }

    txt = conv.c_str();
}

void SkXmlWriter::enableHtmlEscapes(bool enable)
{
    escapeHtml = enable;
}

void SkXmlWriter::setWriterDevice(SkAbstractDevice *output,
                                  bool humanReadable,
                                  uint64_t tabulationChars)
{
    dev = output;
    human = humanReadable;
    tab = tabulationChars;
}

void SkXmlWriter::createDocument(CStr *header)
{
    dev->write(header, strlen(header));
}

void SkXmlWriter::createElement(CStr *tag)
{
    checkForTagComplete();

    if (human)
        dev->write("\r\n", 2);

    writeTabString();

    dev->write("<", 1);
    dev->write(tag, strlen(tag));

    currentTags.append(tag);
    lastTagCompleted = false;
}

void SkXmlWriter::createTextElement(CStr *tag, CStr *text, SkArgsMap *attributes, bool encode)
{
    checkForTagComplete();

    if (human)
        dev->write("\r\n", 2);

    writeTabString();

    dev->write("<", 1);
    dev->write(tag, strlen(tag));

    lastTagCompleted = false;

    if (attributes)
    {
        SkArgsMap &a = *attributes;

        if (!a.isEmpty())
        {
            SkStringList l;
            a.keys(l);

            for (uint64_t i=0; i<l.count(); i++)
            {
                SkString s = a[l.at(i)].toString();
                addAttribute(l.at(i).c_str(), s.c_str());
            }
        }
    }

    SkString t = tag;

    if (text //ALOW ALSO ""
            && (t != "img"
                && t != "link"
                && t != "meta"
                && t != "br"
                && t != "hr"
                /*&& t != "script"*/))
    {
        //here also checkForTagComplete() happens
        insertCharacters(text, encode);

        dev->write("</", 2);
        //MUST CONVERT ESCAPES
        dev->write(tag, strlen(tag));
        dev->write(">", 1);
    }

    else
    {
        if (t != "img"
                && t != "link"
                && t != "meta"
                && t != "br"
                && t != "hr"
                /*&& t != "script"*/)
        {
            dev->write("/>", 2);
        }

        else
            dev->write(">", 1);

        if (t == "script")
        {
            dev->write("</", 2);
            dev->write(tag, strlen(tag));
            dev->write(">", 1);
        }
    }

    lastTagCompleted = true;
}

void SkXmlWriter::addAttribute(CStr *name, CStr *value)
{
    if (!currentTags.isEmpty() && !lastTagCompleted)
    {
        dev->write(" ", 1);
        dev->write(name, strlen(name));
        dev->write("=\"", 2);
        dev->write(value, strlen(value));
        dev->write("\"", 1);
    }

    else
        FlatError("Cannot add attributes over the tags");
}

void SkXmlWriter::insertCharacters(CStr *text, bool encode)
{
    checkForTagComplete();

    SkString s = text;

    if (encode)
        substituteEscapes(s);

    dev->write(s);
}

void SkXmlWriter::closeElement()
{
    if (currentTags.isEmpty())
        return;

    checkForTagComplete();

    if (human)
        dev->write("\r\n", 2);

    SkString currTag = currentTags.last();
    currentTags.removeLast();

    writeTabString();

    dev->write("</", 2);
    dev->write(currTag.c_str(), currTag.size());
    dev->write(">", 1);
}

void SkXmlWriter::closeDocument()
{
    while(!currentTags.isEmpty())
        closeElement();
}

void SkXmlWriter::checkForTagComplete()
{
    if (!currentTags.isEmpty() && !lastTagCompleted)
    {
        lastTagCompleted = true;
        dev->write(">", 1);
    }
}

void SkXmlWriter::writeTabString()
{
    if (human)
    {
        uint64_t sz = tab * currentTags.count();

        if (sz)
        {
            SkString tabulation;
            tabulation.fill(' ', sz);
            dev->write(tabulation.c_str(), tabulation.size());
        }
    }
}

#endif

/*
ESCAPES

"	&quot;      quotation mark
'	&apos;      apostrophe
&	&amp;       ampersand
<	&lt;        less-than
>	&gt;        greater-than
    &nbsp;      non-breaking space
¡	&iexcl;     inverted exclamation mark
¢	&cent;	cent
£	&pound;	pound
¤	&curren;	currency
¥	&yen;	yen
¦	&brvbar;	broken vertical bar
§	&sect;	section
¨	&uml;	spacing diaeresis
©	&copy;	copyright
ª	&ordf;	feminine ordinal indicator
«	&laquo;	angle quotation mark (left)
¬	&not;	negation
­	&shy;	soft hyphen
®	&reg;	registered trademark
¯	&macr;	spacing macron
°	&deg;	degree
±	&plusmn;	plus-or-minus
²	&sup2;	superscript 2
³	&sup3;	superscript 3
´	&acute;	spacing acute
µ	&micro;	micro
¶	&para;	paragraph
·	&middot;	middle dot
¸	&cedil;	spacing cedilla
¹	&sup1;	superscript 1
º	&ordm;	masculine ordinal indicator
»	&raquo;	angle quotation mark (right)
¼	&frac14;	fraction 1/4
½	&frac12;	fraction 1/2
¾	&frac34;	fraction 3/4
¿	&iquest;	inverted question mark
×	&times;	multiplication
÷	&divide;	division
À	&Agrave;	capital a, grave accent
Á	&Aacute;	capital a, acute accent
Â	&Acirc;	capital a, circumflex accent
Ã	&Atilde;	capital a, tilde
Ä	&Auml;	capital a, umlaut mark
Å	&Aring;	capital a, ring
Æ	&AElig;	capital ae
Ç	&Ccedil;	capital c, cedilla
È	&Egrave;	capital e, grave accent
É	&Eacute;	capital e, acute accent
Ê	&Ecirc;	capital e, circumflex accent
Ë	&Euml;	capital e, umlaut mark
Ì	&Igrave;	capital i, grave accent
Í	&Iacute;	capital i, acute accent
Î	&Icirc;	capital i, circumflex accent
Ï	&Iuml;	capital i, umlaut mark
Ð	&ETH;	capital eth, Icelandic
Ñ	&Ntilde;	capital n, tilde
Ò	&Ograve;	capital o, grave accent
Ó	&Oacute;	capital o, acute accent
Ô	&Ocirc;	capital o, circumflex accent
Õ	&Otilde;	capital o, tilde
Ö	&Ouml;	capital o, umlaut mark
Ø	&Oslash;	capital o, slash
Ù	&Ugrave;	capital u, grave accent
Ú	&Uacute;	capital u, acute accent
Û	&Ucirc;	capital u, circumflex accent
Ü	&Uuml;	capital u, umlaut mark
Ý	&Yacute;	capital y, acute accent
Þ	&THORN;	capital THORN, Icelandic
ß	&szlig;	small sharp s, German
à	&agrave;	small a, grave accent
á	&aacute;	small a, acute accent
â	&acirc;	small a, circumflex accent
ã	&atilde;	small a, tilde
ä	&auml;	small a, umlaut mark
å	&aring;	small a, ring
æ	&aelig;	small ae
ç	&ccedil;	small c, cedilla
è	&egrave;	small e, grave accent
é	&eacute;	small e, acute accent
ê	&ecirc;	small e, circumflex accent
ë	&euml;	small e, umlaut mark
ì	&igrave;	small i, grave accent
í	&iacute;	small i, acute accent
î	&icirc;	small i, circumflex accent
ï	&iuml;	small i, umlaut mark
ð	&eth;	small eth, Icelandic
ñ	&ntilde;	small n, tilde
ò	&ograve;	small o, grave accent
ó	&oacute;	small o, acute accent
ô	&ocirc;	small o, circumflex accent
õ	&otilde;	small o, tilde
ö	&ouml;	small o, umlaut mark
ø	&oslash;	small o, slash
ù	&ugrave;	small u, grave accent
ú	&uacute;	small u, acute accent
û	&ucirc;	small u, circumflex accent
ü	&uuml;	small u, umlaut mark
ý	&yacute;	small y, acute accent
þ	&thorn;	small thorn, Icelandic
ÿ	&yuml;	small y, umlaut mark
*/
