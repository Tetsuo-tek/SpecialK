/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKXMLWRITER_H
#define SKXMLWRITER_H

#if defined(ENABLE_XML)

#include "Core/System/skabstractdevice.h"

/**
  * @brief SkXmlWriter
  * It is a simple html/xml writer
*/

class SkXmlWriter extends SkFlatObject
{
    public:
        /**
         * @brief Constructor
        */
        SkXmlWriter();

        /**
         * @brief Enables/Disables the the HTML escapes from included characters
         * @param enabled value
        */
        void enableHtmlEscapes(bool enable);

        /**
         * @brief Sets properties for the writer
         * @param output the device
         * @param humanReadable permits to write indented document
         * @param tabulationChars the number of spaces used for the indentation
         */
        void setWriterDevice(SkAbstractDevice *output,
                             bool humanReadable=true,
                             uint64_t tabulationChars=2);

        /**
         * @brief Initializes the document
         * @param header the header for the writing document
         */
        void createDocument(CStr *header="<!DOCTYPE html>");

        /**
         * @brief Opens a tag element
         * @param tag the tag name
         */
        void createElement(CStr *tag);

        /**
         * @brief Opens and closes a tag element including text and attributes
         * @param tag the tag name
         * @param text the included text
         * @param tag the attributes map
         */
        void createTextElement(CStr *tag, CStr *text, SkArgsMap *attributes=nullptr, bool encode=true);

        /**
         * @brief Adds an attribute to an open tag element
         * @param name
         * @param value
         */
        void addAttribute(CStr *name, CStr *value);

        /**
         * @brief Writes characters (utf8 text) inside an open tag element
         * @param text utf8 characters to write
         */
        void insertCharacters(CStr *text, bool encode=true);

        /**
         * @brief Closes the last open tag element
         * @param text utf8 characters to write
         */
        void closeElement();

        /**
         * @brief Closes the document and all tag elements that are open
         */
        void closeDocument();

    private:
        bool human;
        uint64_t tab;
        SkStringList currentTags;
        bool lastTagCompleted;//this NOT mean the closing tag
        bool escapeHtml;

        SkAbstractDevice *dev;

        void checkForTagComplete();
        void writeTabString();
        void substituteEscapes(SkString &txt);
};

#endif

#endif // SKXMLWRITER_H
