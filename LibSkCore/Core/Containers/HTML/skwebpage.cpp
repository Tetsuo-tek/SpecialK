#if defined(ENABLE_XML) && defined(ENABLE_HTTP)

#include "skwebpage.h"
#include "Core/Containers/XML/skxmlwriter.h"
#include "Core/System/skbufferdevice.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkWebPage, setUrl, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, setTitle, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, setFavIconHref, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, setAuthor, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, setDescription, Arg_CStr)
//DeclareMeth_INSTANCE_VOID(SkWebPage, setOptionalHeaderElements, )
DeclareMeth_INSTANCE_VOID(SkWebPage, addOptionalHeaderElement, Arg_Custom(SkWuiElement))
DeclareMeth_INSTANCE_VOID(SkWebPage, setCssHrefsList, *Arg_Custom(SkStringList))
DeclareMeth_INSTANCE_VOID(SkWebPage, addCssFile, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, setScriptFilesList, *Arg_Custom(SkStringList))
DeclareMeth_INSTANCE_VOID(SkWebPage, addScriptFile, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkWebPage, addHttpHeader, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWebPage, getUrl, CStr *)
DeclareMeth_INSTANCE_RET(SkWebPage, getLabel, CStr *)
DeclareMeth_INSTANCE_RET(SkWebPage, getHead, SkWuiElement *)
DeclareMeth_INSTANCE_RET(SkWebPage, getBody, SkWuiElement *)
//DeclareMeth_INSTANCE_RET(SkWebPage, getCookies, SkCookies &)
DeclareMeth_INSTANCE_VOID(SkWebPage, clearCssHrefsList)
DeclareMeth_INSTANCE_VOID(SkWebPage, clearScriptHrefsList)
DeclareMeth_INSTANCE_VOID(SkWebPage, clearHttpHeadersList)
DeclareMeth_INSTANCE_VOID(SkWebPage, build)
DeclareMeth_INSTANCE_VOID(SkWebPage, writeOnDataBuffer, *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDataBuffer, 1, *Arg_Custom(SkDataBuffer), Arg_Bool)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDataBuffer, 2, *Arg_Custom(SkDataBuffer), Arg_Bool, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkWebPage, writeOnDevice, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDevice, 1, Arg_Custom(SkAbstractDevice), Arg_Bool)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDevice, 2, Arg_Custom(SkAbstractDevice), Arg_Bool, Arg_Bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkWebPage, SkWuiElement,
                {
                    AddMeth_INSTANCE_VOID(SkWebPage, setUrl);
                    AddMeth_INSTANCE_VOID(SkWebPage, setTitle);
                    AddMeth_INSTANCE_VOID(SkWebPage, setFavIconHref);
                    AddMeth_INSTANCE_VOID(SkWebPage, setAuthor);
                    AddMeth_INSTANCE_VOID(SkWebPage, setDescription);
                    //AddMeth_INSTANCE_VOID(SkWebPage, setOptionalHeaderElements);
                    AddMeth_INSTANCE_VOID(SkWebPage, addOptionalHeaderElement);
                    AddMeth_INSTANCE_VOID(SkWebPage, setCssHrefsList);
                    AddMeth_INSTANCE_VOID(SkWebPage, addCssFile);
                    AddMeth_INSTANCE_VOID(SkWebPage, setScriptFilesList);
                    AddMeth_INSTANCE_VOID(SkWebPage, addScriptFile);
                    AddMeth_INSTANCE_VOID(SkWebPage, addHttpHeader);
                    AddMeth_INSTANCE_RET(SkWebPage, getUrl);
                    AddMeth_INSTANCE_RET(SkWebPage, getLabel);
                    AddMeth_INSTANCE_RET(SkWebPage, getHead);
                    AddMeth_INSTANCE_RET(SkWebPage, getBody);
                    //AddMeth_INSTANCE_RET(SkWebPage, getCookies);
                    AddMeth_INSTANCE_VOID(SkWebPage, clearCssHrefsList);
                    AddMeth_INSTANCE_VOID(SkWebPage, clearScriptHrefsList);
                    AddMeth_INSTANCE_VOID(SkWebPage, clearHttpHeadersList);
                    AddMeth_INSTANCE_VOID(SkWebPage, build);
                    AddMeth_INSTANCE_VOID(SkWebPage, writeOnDataBuffer);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDataBuffer, 1);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDataBuffer, 2);
                    AddMeth_INSTANCE_VOID(SkWebPage, writeOnDevice);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDevice, 1);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkWebPage, writeOnDevice, 2);
                })
{
    setWuiType("WebPage");
    setTag("html");

    head = nullptr;
    body = nullptr;
}

void SkWebPage::setUrl(SkString tempUrl)
{
    url = tempUrl;
}

void SkWebPage::setTitle(SkString tempTitle)
{
    title = tempTitle;
}

void SkWebPage::setFavIconHref(SkString tempFavIconHref)
{
    favIconHref = tempFavIconHref;
}

void SkWebPage::setAuthor(SkString author)
{
    SkArgsMap props;
    props["name"] = "author";
    props["content"] = author;
    addOptionalHeaderElement(wui_META(props));
}

void SkWebPage::setDescription(SkString description)
{
    SkArgsMap props;
    props["name"] = "description";
    props["content"] = description;
    addOptionalHeaderElement(wui_META(props));
}

void SkWebPage::setOptionalHeaderElements(SkVector<SkWuiElement *> &tempMetaObjectsList)
{
    optionalHeaderElements = tempMetaObjectsList;
}

void SkWebPage::addOptionalHeaderElement(SkWuiElement *tempMetaObject)
{
    optionalHeaderElements.append(tempMetaObject);
}

void SkWebPage::setCssHrefsList(SkStringList &tempCssHrefsList)
{
    cssHrefsList = tempCssHrefsList;
}

void SkWebPage::addCssFile(SkString href)
{
    if (!cssHrefsList.contains(href))
        cssHrefsList.append(href);
}

void SkWebPage::setScriptFilesList(SkStringList &tempScriptsList)
{
    scriptHrefsList = tempScriptsList;
}

void SkWebPage::addScriptFile(SkString href)
{
    if (!scriptHrefsList.contains(href))
        scriptHrefsList.append(href);
}

void SkWebPage::addHttpHeader(SkString header)
{
    if (!httpHeadersList.contains(header))
       httpHeadersList.append(header);
}

CStr *SkWebPage::getUrl()
{
    return url.c_str();
}

CStr *SkWebPage::getLabel()
{
    return title.c_str();
}

SkCookies &SkWebPage::getCookies()
{
    return cookies;
}

void SkWebPage::clearCssHrefsList()
{
    if (!cssHrefsList.isEmpty())
        cssHrefsList.clear();
}

void SkWebPage::clearScriptHrefsList()
{
    if (!scriptHrefsList.isEmpty())
        scriptHrefsList.clear();
}

void SkWebPage::clearHttpHeadersList()
{
    if (!httpHeadersList.isEmpty())
        httpHeadersList.clear();
}

SkWuiElement *SkWebPage::getHead()
{
    return head;
}

SkWuiElement *SkWebPage::getBody()
{
    return body;
}

void SkWebPage::build()
{
    SkWuiElement *lastHead = nullptr;

    if (head)
        lastHead = head;

    SkWuiElement *lastBody = nullptr;

    if (body)
        lastBody = body;

    head = new SkWuiElement("", "head", "", this);

    for(uint64_t i=0; i<optionalHeaderElements.count(); i++)
        head->append(optionalHeaderElements.at(i));

    if (!favIconHref.isEmpty())
    {
        SkWuiElement *favIconLink = new SkWuiElement("", "link", "", head);
        favIconLink->attr("rel", "shortcut icon");
        favIconLink->attr("href", favIconHref.c_str());
    }

    if (!title.isEmpty())
        new SkWuiElement("", "title", title.c_str(), head);

    for (uint64_t i=0; i<cssHrefsList.count(); i++)
    {
        SkWuiElement *cssHref = new SkWuiElement("", "link", "", head);
        cssHref->attr("rel", "stylesheet");
        cssHref->attr("type", "text/css");
        cssHref->attr("href", cssHrefsList.at(i).c_str());
    }

    for (uint64_t i=0; i<scriptHrefsList.count(); i++)
    {
        SkWuiElement *scriptHref = new SkWuiElement("", "script", "", head);
        scriptHref->attr("src", scriptHrefsList.at(i).c_str());
        scriptHref->attr("type", "text/javascript");
    }

    body = new SkWuiElement("", "body", "", this);

    if (lastHead)
        lastHead->destroyLater();

    if (lastBody)
        lastBody->destroyLater();
}

void SkWebPage::writeOnDataBuffer(SkDataBuffer &output, bool putHeaders, bool humanReadable)
{
    SkBufferDevice *bufferDevice = new SkBufferDevice;
    bufferDevice->open(output, SkBufferDeviceMode::BVM_ONLYWRITE);
    writeOnDevice(bufferDevice, putHeaders, humanReadable);
    bufferDevice->close();
    bufferDevice->destroyLater();
}

void SkWebPage::writeOnDevice(SkAbstractDevice *output, bool putHeaders, bool humanReadable)
{
    SkXmlWriter htmlWriter;

    if (putHeaders)
    {
        SkDataBuffer pageDataArray;
        SkBufferDevice *tempBufDevice = new SkBufferDevice;

        tempBufDevice->open(pageDataArray, SkBufferDeviceMode::BVM_ONLYWRITE);

        htmlWriter.setWriterDevice(tempBufDevice, humanReadable);//first must buffer to know page-size
        htmlWriter.createDocument("<!DOCTYPE html>");
        writeHtml(&htmlWriter);
        htmlWriter.closeDocument();

        for (uint64_t i=0; i<httpHeadersList.count(); i++)
        {
            output->write(httpHeadersList.at(i));
            output->write("\r\n", 2);
        }

        if (cookies.count() > 0)
        {
            SkStringList cookiesList;
            cookies.produceResponseHeaders(cookiesList);

            SkString s;
            s.append(cookiesList.join("\r\n"));
            s.append("\r\n");
            output->write(s);
        }

        SkString contentLen("Content-Length: ");
        contentLen.append(SkString::number(pageDataArray.size()));
        contentLen.append("\r\n\r\n");
        output->write(contentLen);

        //SHITTING PATCH TO ERASE '\n' on start of xml
        //pageDataArray.getData().remove(0, 1);
        //-> output->write(&pageDataArray.data()[1], pageDataArray.size()-1);

        output->write(&pageDataArray);
        tempBufDevice->close();
        tempBufDevice->destroyLater();
    }

    else
    {
        htmlWriter.setWriterDevice(output, humanReadable);//directly on output
        htmlWriter.createDocument("<!DOCTYPE html>");
        writeHtml(&htmlWriter);
        htmlWriter.closeDocument();
    }
}

#endif
