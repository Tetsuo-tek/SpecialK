/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKWUIELEMENT_H
#define SKWUIELEMENT_H

#if defined(ENABLE_XML)

#include "Core/Containers/XML/skxmlwriter.h"

enum SkWuiLinkTarget
{
    LTG_BLANK,
    LTG_SELF,
    LTG_PARENT,
    LTG_TOP
};

#include "Core/Containers/skargsmap.h"

class SPECIALK SkWuiElement extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkWuiElement, SkObject);

        /**
         * @brief Additional constructor
         * @param tempID the id atribute string
         * @param tempTag the tag name
         * @param tempText the text to put inside
         * @param parent the parent wuiElement
         */
        SkWuiElement(CStr *tempID,
                     CStr *tempTag,
                     CStr *tempText,
                     SkWuiElement *parent);

        /**
         * @brief Additional constructor
         * @attention this is a simple text-node
         * @param tempText the text data
         * @param parent the parent wuiElement
         */
        SkWuiElement(CStr *tempText,
                     SkWuiElement *parent);

        /**
         * @brief Sets the 'id' attribute
         * @param tempID the cstring value of the 'id'
         * @return the wuiElement itself pointer
         */
        SkWuiElement *setID(CStr *tempID);

        /**
         * @brief Sets the html-tag
         * @param tempTag the cstring value of the html-tag
         * @return the wuiElement itself pointer
         */
        SkWuiElement *setTag(CStr *tempTag);

        SkWuiElement *setClasses(CStr *classes);

        /**
         * @brief Sets a list of 'class' attributes
         * @param classes the classes list
         * @return the wuiElement itself pointer
         */
        SkWuiElement *setClasses(SkStringList &classes);

        /**
         * @brief Adds a 'class' attribute
         * @param className the cstring value of the 'class'
         * @return the wuiElement itself pointer
         */
        SkWuiElement *addClass(CStr *className);

        /**
         * @brief Removes a 'class' attribute
         * @param className the cstring value of the 'class'
         * @return the wuiElement itself pointer
         */
        SkWuiElement *removeClass(CStr *className);

        /**
         * @brief Removes all 'class' attributes
         * @return the wuiElement itself pointer
         */
        SkWuiElement *clearClasses();

        /**
         * @brief Sets an attribute
         * @param name the cstring name of the attribute
         * @param value the value of the attribute
         * @return the wuiElement itself pointer
         */
        SkWuiElement *attr(CStr *name, CStr *value=nullptr);

        /**
         * @brief Gets internal attributes map
         * @return the attributes map reference
         */
        SkArgsMap &getAttributes();

        /**
         * @brief Checks if an attribute exists
         * @param name the cstring name of the attribute
         * @return true
         */
        bool existsAttr(CStr *name);

        /**
         * @brief Gets an attribute value
         * @param name the cstring name of the attribute
         * @return the string value
         */
        SkString getAttr(CStr *name);

        /**
         * @brief Adds a css-style property
         * @param name the cstring name of the css-style property
         * @param value the value of the css-style property
         * @return the wuiElement itself pointer
         */
        SkWuiElement *css(CStr *name, CStr *value=nullptr);

        /**
         * @brief Gets internal css-style map
         * @return the css-style map reference
         */
        SkArgsMap getCssStyle();

        /**
         * @brief Gets a css-style property value
         * @param name the cstring name of the css-style property
         * @return the string value
         */
        SkString getCss(CStr *name);

        /**
         * @brief Appends a child wuiElement
         * @param tempChild the child wuiElement
         * @return the wuiElement itself pointer
         */
        SkWuiElement *append(SkWuiElement *tempChild);

        /**
         * @brief Appends a child nodeText
         * @param tempChild the child nodeText
         * @return the wuiElement itself pointer
         */
        SkWuiElement *appendText(CStr *tempText);
        SkWuiElement *enableTextEncoder(bool value);

        /**
         * @brief Appends a this wuiElement as child to a parent
         * @param tempParent the parent wuiElement
         * @return the wuiElement itself pointer
         */
        SkWuiElement *appendTo(SkWuiElement *tempParent);

        /**
         * @brief Appends a child nodeText
         * @param tempChild the child nodeText
         * @return the wuiElement itself pointer
         */
        SkWuiElement *text(CStr *tempText);

        /**
         * @brief Gets the text inside this node
         * @return the text string value reference
         */
        CStr *getText();

        /**
         * @brief Gets 'id' attribute value
         * @return the 'id' value string reference
         */
        CStr *getID();

        /**
         * @brief Gets the html-tag
         * @return the html-tag string reference
         */
        CStr *getTag();

        /**
         * @brief Gets the wuiType property
         * @attention this method and its property must be refactored
         * @return the wuiType string value reference
         */
        CStr *getType();

        /**
         * @brief Gets the count of the direct text-node children
         * @return the count value
         */
        uint64_t getTextNodesCount();

        /**
         * @brief Gets a text-node by index
         * @param index the index of the text-node
         * @return the text-node if the index is valid, otherwise a NULL pointer
         */
        SkWuiElement *getTextNode(uint64_t index);

        /**
         * @brief Removes a text-node from index
         * @param index the index of the text-node to remove
         */
        void removeTextNode(uint64_t index);

        /**
         * @brief Removes all text-nodes
         */
        void clearTextNodes();

        /**
         * @brief Gets the parent of this wuiElement
         * @return
         */
        SkWuiElement *getWuiParent();

        /*bool containsWuiElementById(CStr *id);
        SkWuiElement *getWuiElementById(CStr *id);*/

        /*virtual void fromJson(SkArgsMap &json);
        virtual void toJson(SkArgsMap &json);*/

        /**
         * @brief Writes the entire subordinated hierachy with an xml-writer
         * @param htmlWriter the xml-writer
         */
        void writeHtml(SkXmlWriter *htmlWriter);

        // // // STATIC HTML-BUILD CONVENCIENCEs  // // //

        /**
         * @brief Gets the cstring representation of a link open-mode
         * @param tgt the link open-mode mode value
         * @return the cstring representation
         */
        static CStr *resolv_LINKOPEN_TARGET(SkWuiLinkTarget tgt);


        /**
         * @brief Makes an abstract wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wuiElement(CStr *id="", SkWuiElement *parent=nullptr);

        /**
         * @brief Makes a text contained wuiElement
         * @param id the 'id' attribute
         * @param text the text cstring to put inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wuiElementWithText(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes a custom wuiElement
         * @param id the 'id' attribute
         * @param tagName the html-tag
         * @param classes the 'class' attributes to set, separed by commas
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *customWuiElement(CStr *id, CStr *tagName, CStr *classes="", SkWuiElement *parent=nullptr);

        //SIMPLEs

        /**
         * @brief Makes META wuiEelement
         * @param attributes the attributes map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_META(SkArgsMap &attributes, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes H wuiEelement
         * @param id the 'id' attribute
         * @param level the level of the H
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_H(CStr *id, int level, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes SPAN wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_SPAN(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes SMALL wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_SMALL(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes STRONG wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_STRONG(CStr *id, CStr *text, SkWuiElement *parent=nullptr);
        static SkWuiElement *wui_EMPHASIS(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes MARK wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_MARK(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes ABBR wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_ABBR(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes CODE wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_CODE(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes KBD wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_KBD(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes BLOCKQUOTE wuiEelement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_BLOCKQUOTE(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes PRE wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_PRE(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes P wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_P(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes A wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param url the linked url
         * @param target the SkWuiLinkTarget cstring resolved value
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_A(CStr *id, CStr *text, CStr *url, CStr *target, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes DIV wuiEelement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_DIV(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TEXTAREA wuiEelement
         * @param id the 'id' attribute
         * @param text the text cstring to add inside
         * @param rows the rows count value
         * @param cols the columns count value
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TEXTAREA(CStr *id, CStr *text, uint64_t rows, uint64_t cols, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes IMG wuiElement
         * @param id the 'id' attribute
         * @param src the source url
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_IMG(CStr *id, CStr *src, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes IFRAME wuiElement
         * @param id the 'id' attribute
         * @param src the source url
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_IFRAME(CStr *id, CStr *src, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes NAV wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_NAV(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes UL wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_UL(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes OL wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_OL(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes LI wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_LI(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes SECTION wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_SECTION(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes HR wuiElement
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_HR(SkWuiElement *parent=nullptr);

        /**
         * @brief Makes BR wuiElement
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_BR(SkWuiElement *parent=nullptr);

        /**
         * @brief Makes BUTTON wuiElement
         * @param id the 'id' attribute
         * @param label the button label cstring
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_BUTTON(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes SCRIPT wuiElement
         * @param src the source url
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_SCRIPT(CStr *src, SkWuiElement *parent=nullptr);
        static SkWuiElement *wui_SCRIPT(SkWuiElement *parent=nullptr);

        /**
         * @brief Makes inline-SCRIPT wuiElement
         * @param code the code cstring to insert
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_INLINESCRIPT(CStr *code, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TABLE wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TABLE(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes THEAD wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_THEAD(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TBODY wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TBODY(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TFOOT wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TFOOT(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TR wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TR(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TH wuiElement
         * @param id the 'id' attribute
         * @param text the text cstring to put inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TH(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes TD wuiElement
         * @param id the 'id' attribute
         * @param text the text cstring to put inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_TD(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        //FORMs AND INPUTs

        /**
         * @brief Makes FORM wuiElement
         * @param id the 'id' attribute
         * @param method the form method attribute to use
         * @param action the form action attribute to use
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_FORM(CStr *id, CStr *method, CStr *action, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes FIELDSET wuiElement
         * @param id the 'id' attribute
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_FIELDSET(CStr *id, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes LEGEND wuiElement
         * @param id the 'id' attribute
         * @param text the text cstring to put inside
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        static SkWuiElement *wui_LEGEND(CStr *id, CStr *text, SkWuiElement *parent=nullptr);

        static SkWuiElement *wui_INPUT(CStr *id, SkWuiElement *parent=nullptr);
        static SkWuiElement *wui_SELECT(CStr *id, SkWuiElement *parent=nullptr);
        static SkWuiElement *wui_OPTION(CStr *id, SkWuiElement *parent=nullptr);
        static SkWuiElement *wui_LABEL(CStr *idFor, CStr *text, SkWuiElement *parent=nullptr);

        //HERE WE COULD ADD ALSO PARTICULAR INPUT ATTRs ON MAP RETURNED (For EX. int the case of a number-spin we could add: "min", "max", "step")
        /**
         * @brief Build a properties map for input wuiElelemts
         * @param type the html-tag for the input wuiElement
         * @param name the 'name' attribute
         * @param placeHolder the 'placeholder' attribute
         * @param defaultValue the default variant 'value' to use
         * @param isChecked if true it will be 'checked' (valid only for checkable elements as: radios and checkboxes)
         * @param isRequired if true it will be 'required'
         * @param isReadOnly if true it will be 'readonly'
         * @param isDisabled if true it will be 'disabled'
         * @return the properties map to pass to wui_INPUT method
         */
        /*static SkArgsMap abstractInputProps(CStr *type,
                                            CStr *name,
                                            CStr *placeHolder,
                                            SkVariant defaultValue,
                                            bool isChecked=false,
                                            bool isRequired=false,
                                            bool isReadOnly=false,
                                            bool isDisabled=false);*/

        /**
         * @brief Makes input wuiElement with custom properties
         * @param props the properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT(SkArgsMap &props, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes form 'submit' button as INPUT wuiElement
         * @param label the button label cstring
         * @param props the properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_FORM_SUBMITBUTTON(CStr *label, SkArgsMap &props, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes form 'reset' button as INPUT wuiElement
         * @param label the button label cstring
         * @param props the properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_FORM_RESETBUTTON(CStr *label, SkArgsMap &props, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes the 'label' for a field
         * @param fieldID the field 'id' attribute to associate
         * @param label the label-text cstring
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_FIELDLABEL(CStr *fieldID, CStr *label, SkWuiElement *parent=nullptr);

        //INPUT type OR SPECIFIC TAG IS SET IN THESE FUNCTIONs; OTHER PROPS COULD BE SET INSIDE optProps
        /**
         * @brief Makes INPUT 'text' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_TEXT(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'password' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_PASSWORD(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'check' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_BOOL(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'radio' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_RADIO(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT file selection with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_BLOB(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        //IT IS NOT input, BUT textarea

        /**
         * @brief Makes TEXTAREA input with optional properties
         * @param text the text to put inside
         * @param rows the rows count
         * @param cols the columns count
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */

        //static SkWuiElement *wui_INPUT_CLOB(CStr *text, uint64_t rows, uint64_t cols, SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes SELECT wuiElement
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_SELECT(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes OPTGROUP wuielement
         * @param label the label-text cstring
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_OPTGROUP(CStr *label, SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes OPT wuielement
         * @param label the label-text cstring
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT_OPT(CStr *label, SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        //HTML-5 INPUTs

        /**
         * @brief Makes INPUT 'color' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_COLOR(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'date' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_DATE(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'datetime-local' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_DATETIMELOCAL(SkArgsMap& optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'email' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_EMAIL(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'month' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_MONTH(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'week' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_WEEK(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'number' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_NUMBER(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'range' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_RANGE(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'search' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_SEARCH(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'tel' (telephone) with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_TEL(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'month' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_TIME(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

        /**
         * @brief Makes INPUT 'url' with optional properties
         * @param optProps the optional properties map
         * @param parent the parent wuiElement
         * @return the wuiElement created
         */
        //static SkWuiElement *wui_INPUT5_URL(SkArgsMap &optProps, SkWuiElement *parent=nullptr);

    private:
        SkString id;
        SkString wuiType;
        SkString tag;
        SkString nodeText;
        bool encodeText;

        SkArgsMap attributes;
        SkArgsMap style;
        SkArgsMap events;

        SkVector<SkWuiElement *> textNodes;

        void upgradeAttributes();

    protected:
        void setWuiType(CStr *tempType);
};

#endif

#endif // SKWUIELEMENT_H
