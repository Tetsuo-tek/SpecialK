/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef MDWEBPAGE_H
#define MDWEBPAGE_H

#if defined(ENABLE_XML) && defined(ENABLE_HTTP)

#include "Core/Containers/HTML/skwuielement.h"
#include "Core/System/Network/TCP/HTTP/skcookies.h"

class SkAbstractDevice;
class SkDataBuffer;

class SPECIALK SkWebPage extends SkWuiElement
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkWebPage, SkWuiElement);

        /**
         * @brief Sets the referring page url
         * @param the page url string
         */
        void setUrl(SkString tempUrl);

        /**
         * @brief Sets the page title
         * @param tempLabel
         */
        void setTitle(SkString tempTitle);

        /**
         * @brief Sets the page favourite icon
         * @param the href of the page favourite icon
         */
        void setFavIconHref(SkString tempFavIconHref);

        /**
         * @brief Sets the page author
         * @param author the page author string
         */
        void setAuthor(SkString author);

        /**
         * @brief Sets the page description
         * @param author the page description string
         */
        void setDescription(SkString description);

        /**
         * @brief Sets a list of META wuiElements for this page
         * @param tempMetaObjectsList the META wuiElements list
         */
        void setOptionalHeaderElements(SkVector<SkWuiElement *> &tempMetaObjectsList);

        /**
         * @brief Adds a single META wuiElements
         * @param tempMetaObject
         */
        void addOptionalHeaderElement(SkWuiElement *tempMetaObject);

        /**
         * @brief Sets a list of style css hrefs
         * @param tempCssHrefsList the style css hrefs list
         */
        void setCssHrefsList(SkStringList &tempCssHrefsList);

        /**
         * @brief Adds a single style css href
         * @param href the style css href
         */
        void addCssFile(SkString href);

        /**
         * @brief Sets a list of script hrefs
         * @param tempScriptsList the style script hrefs list
         */
        void setScriptFilesList(SkStringList &tempScriptsList);

        /**
         * @brief Adds a single script href
         * @param href the script href
         */
        void addScriptFile(SkString href);

        /**
         * @brief Adds an HTTP-SERVER header for the page content
         * @param header
         */
        void addHttpHeader(SkString header);

        /**
         * @brief Gets the referring page url
         * @return the url string reference
         */
        CStr *getUrl();

        /**
         * @brief Gets the page title
         * @return the page title string reference
         */
        CStr *getLabel();

        /**
         * @brief Gets the page HEAD
         * @warning it is valid ONLY after the page is built with 'build()' method
         * @return the HEAD wuiElement if the page is built, otherwise nullptr pointer
         */
        SkWuiElement *getHead();

        /**
         * @brief Gets the page BODY
         * @warning it is valid ONLY after the page is built with 'build()' method
         * @return the BODY wuiElement if the page is built, otherwise NULL pointer
         */
        SkWuiElement *getBody();

        /**
         * @brief Gets internal cookies reference
         * @return the cookies reference
         */
        SkCookies &getCookies();

        /**
         * @brief Clears the style css hrefs
         */
        void clearCssHrefsList();

        /**
         * @brief Clears the script hrefs
         */
        void clearScriptHrefsList();

        /**
         * @brief Clears the HTTP-SERVER headers
         */
        void clearHttpHeadersList();

        /**
         * @brief Build the page HEAD and BODY
         */
        void build();

        /**
         * @brief Writes the page HTML on a data-buffer
         * @param output the output data-buffer reference
         * @param putHeaders if true it will prepend the output with HTTP-SERVER headers
         * @param humanReadable permits to write indented document
         */
        void writeOnDataBuffer(SkDataBuffer &output, bool putHeaders=true, bool humanReadable=true);

        /**
         * @brief Writes the page HTML on a device
         * @param output the output device pointer
         * @param putHeaders if true it will prepend the output with HTTP-SERVER headers
         * @param humanReadable permits to write indented document
         */
        void writeOnDevice(SkAbstractDevice *output, bool putHeaders=true, bool humanReadable=true);

    private:
        SkString url;
        SkString title;

        SkVector<SkWuiElement *> optionalHeaderElements;

        SkString favIconHref;

        SkStringList cssHrefsList;
        SkStringList scriptHrefsList;

        SkVector<SkWuiElement *> endingBodyObjsList;

        SkStringList httpHeadersList;

        SkWuiElement *head;
        SkWuiElement *body;
        SkCookies cookies;
};

#endif

#endif // MDWEBPAGE_H
