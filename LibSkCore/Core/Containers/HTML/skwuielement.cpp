#include "skwuielement.h"

#if defined(ENABLE_XML)

void SkWuiElementClose(SkObject *obj)
{
    SkWuiElement *f = dynamic_cast<SkWuiElement *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkWuiElementClose()");

    f->clearTextNodes();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkWuiLinkTarget)
{
    SetupEnumWrapper(SkWuiLinkTarget);

    SetEnumItem(SkWuiLinkTarget::LTG_BLANK);
    SetEnumItem(SkWuiLinkTarget::LTG_SELF);
    SetEnumItem(SkWuiLinkTarget::LTG_PARENT);
    SetEnumItem(SkWuiLinkTarget::LTG_TOP);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkWuiElement, setID, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, setTag, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, setClasses, SkWuiElement *, *Arg_Custom(SkStringList))
DeclareMeth_INSTANCE_RET(SkWuiElement, addClass, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, removeClass, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, clearClasses, SkWuiElement *)
DeclareMeth_INSTANCE_RET(SkWuiElement, attr, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkWuiElement, attr, 1, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, getAttributes, SkArgsMap &)
DeclareMeth_INSTANCE_RET(SkWuiElement, existsAttr, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, getAttr, SkString, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, css, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkWuiElement, css, 1, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, getCssStyle, SkArgsMap)
DeclareMeth_INSTANCE_RET(SkWuiElement, getCss, SkString, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, append, SkWuiElement *, Arg_Custom(SkWuiElement))
DeclareMeth_INSTANCE_RET(SkWuiElement, appendText, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, appendTo, SkWuiElement *, Arg_Custom(SkWuiElement))
DeclareMeth_INSTANCE_RET(SkWuiElement, text, SkWuiElement *, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkWuiElement, getText, CStr *)
DeclareMeth_INSTANCE_RET(SkWuiElement, getID, CStr *)
DeclareMeth_INSTANCE_RET(SkWuiElement, getTag, CStr *)
DeclareMeth_INSTANCE_RET(SkWuiElement, getType, CStr *)
DeclareMeth_INSTANCE_RET(SkWuiElement, getTextNodesCount, uint64_t)
DeclareMeth_INSTANCE_RET(SkWuiElement, getTextNode, SkWuiElement *, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkWuiElement, removeTextNode, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkWuiElement, clearTextNodes)
DeclareMeth_INSTANCE_RET(SkWuiElement, getWuiParent, SkWuiElement *)
DeclareMeth_INSTANCE_VOID(SkWuiElement, writeHtml, Arg_Custom(SkXmlWriter))
DeclareMeth_STATIC_RET(SkWuiElement, resolv_LINKOPEN_TARGET, CStr *, Arg_Enum(SkWuiLinkTarget))
DeclareMeth_STATIC_RET(SkWuiElement, wuiElement, SkWuiElement *)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElement, 1, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElement, 2, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wuiElementWithText, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElementWithText, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, customWuiElement, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, customWuiElement, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, customWuiElement, 2, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_META, SkWuiElement *, *Arg_Custom(SkArgsMap))
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_META, 1, SkWuiElement *, *Arg_Custom(SkArgsMap), Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_H, SkWuiElement *, Arg_CStr, Arg_Int, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_H, 1, SkWuiElement *, Arg_CStr, Arg_Int, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_SPAN, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SPAN, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_SMALL, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SMALL, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_STRONG, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_STRONG, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_MARK, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_MARK, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_ABBR, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_ABBR, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_CODE, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_CODE, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_KBD, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_KBD, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_BLOCKQUOTE, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BLOCKQUOTE, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_PRE, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_PRE, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_P, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_P, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_A, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_A, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_DIV, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_DIV, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TEXTAREA, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_UInt64, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TEXTAREA, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_UInt64, Arg_UInt64, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_IMG, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_IMG, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_IFRAME, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_IFRAME, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_NAV, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_NAV, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_UL, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_UL, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_OL, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_OL, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_LI, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_LI, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_SECTION, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SECTION, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_HR, SkWuiElement *)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_HR, 1, SkWuiElement *, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_BR, SkWuiElement *)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BR, 1, SkWuiElement *, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_BUTTON, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BUTTON, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_SCRIPT, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SCRIPT, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_INLINESCRIPT, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_INLINESCRIPT, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TABLE, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TABLE, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_THEAD, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_THEAD, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TBODY, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TBODY, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TFOOT, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TFOOT, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TR, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TR, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TH, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TH, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_TD, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TD, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_FORM, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_FORM, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_FIELDSET, SkWuiElement *, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_FIELDSET, 1, SkWuiElement *, Arg_CStr, Arg_Custom(SkWuiElement))
DeclareMeth_STATIC_RET(SkWuiElement, wui_LEGEND, SkWuiElement *, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_LEGEND, 1, SkWuiElement *, Arg_CStr, Arg_CStr, Arg_Custom(SkWuiElement))

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkWuiElement, SkObject,
                {
                    AddEnumType(SkWuiLinkTarget);

                    AddMeth_INSTANCE_RET(SkWuiElement, setID);
                    AddMeth_INSTANCE_RET(SkWuiElement, setTag);
                    AddMeth_INSTANCE_RET(SkWuiElement, setClasses);
                    AddMeth_INSTANCE_RET(SkWuiElement, addClass);
                    AddMeth_INSTANCE_RET(SkWuiElement, removeClass);
                    AddMeth_INSTANCE_RET(SkWuiElement, clearClasses);
                    AddMeth_INSTANCE_RET(SkWuiElement, attr);
                    AddMeth_INSTANCE_RET_OVERLOAD(SkWuiElement, attr, 1);
                    AddMeth_INSTANCE_RET(SkWuiElement, getAttributes);
                    AddMeth_INSTANCE_RET(SkWuiElement, existsAttr);
                    AddMeth_INSTANCE_RET(SkWuiElement, getAttr);
                    AddMeth_INSTANCE_RET(SkWuiElement, css);
                    AddMeth_INSTANCE_RET_OVERLOAD(SkWuiElement, css, 1);
                    AddMeth_INSTANCE_RET(SkWuiElement, getCssStyle);
                    AddMeth_INSTANCE_RET(SkWuiElement, getCss);
                    AddMeth_INSTANCE_RET(SkWuiElement, append);
                    AddMeth_INSTANCE_RET(SkWuiElement, appendText);
                    AddMeth_INSTANCE_RET(SkWuiElement, appendTo);
                    AddMeth_INSTANCE_RET(SkWuiElement, text);
                    AddMeth_INSTANCE_RET(SkWuiElement, getText);
                    AddMeth_INSTANCE_RET(SkWuiElement, getID);
                    AddMeth_INSTANCE_RET(SkWuiElement, getTag);
                    AddMeth_INSTANCE_RET(SkWuiElement, getType);
                    AddMeth_INSTANCE_RET(SkWuiElement, getTextNodesCount);
                    AddMeth_INSTANCE_RET(SkWuiElement, getTextNode);
                    AddMeth_INSTANCE_VOID(SkWuiElement, removeTextNode);
                    AddMeth_INSTANCE_VOID(SkWuiElement, clearTextNodes);
                    AddMeth_INSTANCE_RET(SkWuiElement, getWuiParent);
                    AddMeth_INSTANCE_VOID(SkWuiElement, writeHtml);
                    AddMeth_STATIC_RET(SkWuiElement, resolv_LINKOPEN_TARGET);
                    AddMeth_STATIC_RET(SkWuiElement, wuiElement);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElement, 1);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElement, 2);
                    AddMeth_STATIC_RET(SkWuiElement, wuiElementWithText);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wuiElementWithText, 1);
                    AddMeth_STATIC_RET(SkWuiElement, customWuiElement);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, customWuiElement, 1);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, customWuiElement, 2);
                    AddMeth_STATIC_RET(SkWuiElement, wui_META);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_META, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_H);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_H, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_SPAN);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SPAN, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_SMALL);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SMALL, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_STRONG);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_STRONG, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_MARK);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_MARK, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_ABBR);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_ABBR, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_CODE);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_CODE, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_KBD);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_KBD, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_BLOCKQUOTE);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BLOCKQUOTE, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_PRE);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_PRE, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_P);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_P, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_A);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_A, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_DIV);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_DIV, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TEXTAREA);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TEXTAREA, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_IMG);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_IMG, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_IFRAME);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_IFRAME, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_NAV);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_NAV, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_UL);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_UL, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_OL);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_OL, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_LI);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_LI, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_SECTION);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SECTION, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_HR);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_HR, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_BR);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BR, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_BUTTON);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_BUTTON, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_SCRIPT);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_SCRIPT, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_INLINESCRIPT);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_INLINESCRIPT, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TABLE);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TABLE, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_THEAD);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_THEAD, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TBODY);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TBODY, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TFOOT);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TFOOT, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TR);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TR, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TH);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TH, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_TD);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_TD, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_FORM);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_FORM, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_FIELDSET);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_FIELDSET, 1);
                    AddMeth_STATIC_RET(SkWuiElement, wui_LEGEND);
                    AddMeth_STATIC_RET_OVERLOAD(SkWuiElement, wui_LEGEND, 1);
                })
{

    encodeText = true;

    addDtorCompanion(SkWuiElementClose);
}

SkWuiElement::SkWuiElement(CStr *tempID,
                           CStr *tempTag,
                           CStr *tempText,
                           SkWuiElement *parent) : SkObject(parent, true)
{
    encodeText = true;

    setTag(tempTag);
    setID(tempID);

    appendText(tempText);

    ObjectSetup(SkWuiElement, false);
    addDtorCompanion(SkWuiElementClose);
}

SkWuiElement::SkWuiElement(CStr *tempText,
                           SkWuiElement *parent) : SkObject(parent, true)
{
    encodeText = false;

    tag = "TEXTNODE";
    nodeText = tempText;
    wuiType = "WuiObject";

    SkString n = tag;
    n.append("_");
    n.append(uuid());
    setObjectName(n.c_str());

    ObjectSetup(SkWuiElement, false);
    addDtorCompanion(SkWuiElementClose);
}

SkWuiElement *SkWuiElement::setID(CStr *tempID)
{
    if (SkString::isEmpty(tempID))
        id.clear();

    else
    {
        id = tempID;
        attr("id", tempID);
    }

    return this;
}

SkWuiElement *SkWuiElement::setTag(CStr *tempTag)
{
    if (SkString::isEmpty(tempTag))
        return this;

    tag = tempTag;
    tag.toLowerCase();

    if (tag == "html")
        wuiType = "WebPage";

    return this;
}

void SkWuiElement::setWuiType(CStr *tempType)
{
    wuiType = tempType;
}

SkWuiElement *SkWuiElement::setClasses(CStr *classes)
{
    return attr("class", classes);
}

SkWuiElement *SkWuiElement::setClasses(SkStringList &classes)
{
    SkString classesString = classes.join(" ");
    return attr("class", classesString.c_str());
}

SkWuiElement *SkWuiElement::addClass(CStr *className)
{
    SkString tempClasses = getAttr("class");
    SkStringList tempClassesParsed;

    if (!tempClasses.isEmpty())
        tempClasses.split(' ', tempClassesParsed);

    if (!tempClassesParsed.contains(className))
    {
        tempClassesParsed.append(className);
        SkString classesString = tempClassesParsed.join(" ");
        attr("class", classesString.c_str());
    }

    return this;
}

SkWuiElement *SkWuiElement::removeClass(CStr *className)
{
    if (existsAttr("class"))
    {
        SkStringList tempClassesParsed;
        getAttr("class").split(' ', tempClassesParsed);

        if (tempClassesParsed.contains(className))
        {
            tempClassesParsed.removeAt(tempClassesParsed.indexOf(className));
            SkString classesString = tempClassesParsed.join(" ");
            attr("class", classesString.c_str());
        }
    }

    return this;
}

SkWuiElement *SkWuiElement::clearClasses()
{
    if (existsAttr("class"))
        attr("class", "");

    return this;
}

SkWuiElement *SkWuiElement::attr(CStr *name, CStr *value)
{
    if (attributes.contains(name) && SkString::isEmpty(value))
    {
        attributes.remove(name);
        return this;
    }

    attributes[name] = value;
    return this;
}

SkArgsMap &SkWuiElement::getAttributes()
{
    return attributes;
}

bool SkWuiElement::existsAttr(CStr *name)
{
    if (attributes.contains(name))
        return true;

    return false;
}

SkString SkWuiElement::getAttr(CStr *name)
{
    if (attributes.contains(name))
        return attributes[name].toString();

    return "";
}

SkWuiElement *SkWuiElement::css(CStr *name, CStr *value)
{
    if (style.contains(name) && SkString::isEmpty(value))
    {
        style.remove(name);
        return this;
    }

    style[name] = value;
    return this;
}

SkArgsMap SkWuiElement::getCssStyle()
{
    return style;
}

SkString SkWuiElement::getCss(CStr *name)
{
    if (style.contains(name))
        return style[name].toString();

    return "";
}

SkWuiElement *SkWuiElement::append(SkWuiElement *tempChild)
{
    if (SkString::compare(tempChild->getTag(), "TEXTNODE"))
        textNodes.append(tempChild);

    tempChild->setParent(this);

    return this;
}

SkWuiElement *SkWuiElement::appendText(CStr *tempText)
{
    if (SkString::isEmpty(tempText))
        return this;

    textNodes.append(new SkWuiElement(tempText, this));
    return this;
}

SkWuiElement *SkWuiElement::enableTextEncoder(bool value)
{
    encodeText = value;
    return this;
}

SkWuiElement *SkWuiElement::appendTo(SkWuiElement *tempParent)
{
    if (tempParent)
        this->setParent(tempParent);

    else
        this->setParent(nullptr);

    return this;
}

SkWuiElement *SkWuiElement::text(CStr *tempText)
{
    return appendText(tempText);
}

CStr *SkWuiElement::getText()
{
    return nodeText.c_str();
}

CStr *SkWuiElement::getID()
{
    return id.c_str();
}

CStr *SkWuiElement::getTag()
{
    return tag.c_str();
}

CStr *SkWuiElement::getType()
{
    return wuiType.c_str();
}

uint64_t SkWuiElement::getTextNodesCount()
{
    return textNodes.count();
}

SkWuiElement *SkWuiElement::getTextNode(uint64_t index)
{
    if (index >= textNodes.count())
        return nullptr;

    return textNodes.at(index);
}

void SkWuiElement::removeTextNode(uint64_t index)
{
    if (index >= textNodes.count())
        return;

    textNodes.at(index)->destroyLater();
    textNodes.removeAt(index);
}

void SkWuiElement::clearTextNodes()
{
    if (textNodes.isEmpty())
        return;

    for(uint64_t i=0; i<textNodes.count(); i++)
    {
        SkWuiElement *textNode = textNodes.at(i);

        if (!textNode->isPreparedToDie())
            textNode->destroyLater();
    }

    textNodes.clear();

}

SkWuiElement *SkWuiElement::getWuiParent()
{
    return dynamic_cast<SkWuiElement *>(parent());
}

/*bool SkWuiElement::containsWuiElementById(CStr *id)
{
    for(uint64_t i=0; i<childrenList.count(); i++)
    {
        SkObject *obj = static_cast<SkObject *>(childrenList.at(i));
        SkWuiElement *v = dynamic_cast<SkWuiElement*>(obj);

        if (v && v->getID() == id)
            return true;
    }

    return false;
}

SkWuiElement *SkWuiElement::getWuiElementById(CStr *id)
{
    //ONLY DIRECT CHILDREN
    for(uint64_t i=0; i<childrenList.count(); i++)
    {
        SkObject *obj = static_cast<SkObject *>(childrenList.at(i));
        SkWuiElement *v = dynamic_cast<SkWuiElement*>(obj);

        if (v && v->getID() == id)
            return v;
    }

    return nullptr;
}*/

void SkWuiElement::upgradeAttributes()
{
    if (!style.isEmpty())
    {
        SkStringList l;
        style.keys(l);
        SkString cssCode;

        for (uint64_t i=0; i<l.count(); i++)
        {
            cssCode += l.at(i);
            cssCode += ": ";
            cssCode += style[l.at(i)].toString();

            if (i < l.count()-1)
                cssCode += "; ";
        }

        attr("style", cssCode.c_str());
    }

    // ?
    if (!events.isEmpty())
    {
        SkStringList l;
        events.keys(l);
        for (uint64_t i=0; i<l.count(); i++)
        {
            SkString s = attributes[l.at(i)].toString();
            attr(l.at(i).c_str(), s.c_str());
        }
    }
}

/*void SkWuiElement::fromJson(SkArgsMap &json)
{

}

void SkWuiElement::toJson(SkArgsMap &json)
{

}*/

void SkWuiElement::writeHtml(SkXmlWriter *htmlWriter)
{
    ObjectPlusDebug("Tag: " << tag << " - Writing HTML [children: " << children().count() << "]");

    upgradeAttributes();

    if (!textNodes.isEmpty() && children().count() == textNodes.count())
    {
        SkString txt;

        for(uint64_t i=0; i<textNodes.count(); i++)
            txt.append(textNodes.at(i)->getText());

        if (!txt.isEmpty())
            htmlWriter->createTextElement(tag.c_str(), txt.c_str(), &attributes, encodeText);
        else
        {
            htmlWriter->createElement(tag.c_str());

            if (!attributes.isEmpty())
            {
                SkStringList l;
                attributes.keys(l);

                for (uint64_t i=0; i<l.count(); i++)
                {
                    SkString s = attributes[l.at(i)].toString();
                    htmlWriter->addAttribute(l.at(i).c_str(), s.c_str());
                }
            }

            htmlWriter->closeElement();
        }
    }

    else
    {
        htmlWriter->createElement(tag.c_str());

        if (!attributes.isEmpty())
        {
            SkStringList l;
            attributes.keys(l);

            for (uint64_t i=0; i<l.count(); i++)
            {
                SkString s = attributes[l.at(i)].toString();
                htmlWriter->addAttribute(l.at(i).c_str(), s.c_str());
            }
        }

        for(uint64_t i=0; i<children().count(); i++)
        {
            SkObject *obj = static_cast<SkObject *>(children().at(i));
            SkWuiElement *element = dynamic_cast<SkWuiElement *>(obj);

            if (element)
            {
                if (SkString::compare(element->getTag(), "TEXTNODE"))
                {
                    if (!SkString::isEmpty(element->getText()))
                        htmlWriter->insertCharacters(element->getText(), encodeText);
                }

                else
                    element->writeHtml(htmlWriter);
            }
        }

        htmlWriter->closeElement();
    }
}

CStr *SkWuiElement::resolv_LINKOPEN_TARGET(SkWuiLinkTarget tgt)
{
    if (tgt == SkWuiLinkTarget::LTG_BLANK)
        return "_blank";

    else if (tgt == SkWuiLinkTarget::LTG_SELF)
        return "_self";

    else if (tgt == SkWuiLinkTarget::LTG_PARENT)
        return "_parent";

    else if (tgt == SkWuiLinkTarget::LTG_TOP)
        return "_top";

    return "";
}

SkWuiElement *SkWuiElement::wuiElement(CStr *id, SkWuiElement *parent)
{
    SkWuiElement *element = new SkWuiElement(parent);

    if (!SkString::isEmpty(id))
        element->setID(id);

    return element;
}

SkWuiElement *SkWuiElement::wuiElementWithText(CStr *id, CStr *text, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement(id, parent);

    if (SkString::isEmpty(text))
        element->appendText("");
    else
        element->appendText(text);

    return element;
}

SkWuiElement *SkWuiElement::customWuiElement(CStr *id, CStr *tagName, CStr *classes, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement(id, parent);
    element->setTag(tagName);

    if (!SkString::isEmpty(classes))
    {
        SkString clss(classes);
        SkStringList l;
        clss.split(' ', l);
        element->setClasses(l);
    }

    return element;
}

//SIMPLEs

SkWuiElement *SkWuiElement::wui_META(SkArgsMap &attributes, SkWuiElement *parent)
{
    SkWuiElement *element = new SkWuiElement(parent);
    element->setTag("meta");

    SkStringList keys;
    attributes.keys(keys);

    for(uint64_t i=0; i<keys.count(); i++)
    {
        SkString v = attributes[keys.at(i)].toString();
        element->attr(keys.at(i).c_str(), v.c_str());
    }

    return element;
}

SkWuiElement *SkWuiElement::wui_H(CStr *id, int level, CStr *text, SkWuiElement *parent)
{
    SkString t("h");
    t.append(SkString::number(level));
    return wuiElementWithText(id, text, parent)->setTag(t.c_str());
}

SkWuiElement *SkWuiElement::wui_SPAN(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("span");
}

SkWuiElement *SkWuiElement::wui_SMALL(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("small");
}

SkWuiElement *SkWuiElement::wui_STRONG(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("strong");
}

SkWuiElement *SkWuiElement::wui_EMPHASIS(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("em");
}

SkWuiElement *SkWuiElement::wui_MARK(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("mark");
}

SkWuiElement *SkWuiElement::wui_ABBR(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("abbr");
}

SkWuiElement *SkWuiElement::wui_CODE(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("code");
}

SkWuiElement *SkWuiElement::wui_KBD(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("kbd");
}

SkWuiElement *SkWuiElement::wui_BLOCKQUOTE(CStr *id, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement(id, parent);
    element->setTag("blockquote");

    return element;
}

SkWuiElement *SkWuiElement::wui_PRE(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("pre");
}

SkWuiElement *SkWuiElement::wui_P(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("p");
}

SkWuiElement *SkWuiElement::wui_A(CStr *id, CStr *text, CStr *url, CStr *target, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElementWithText(id, text, parent);
    element->setTag("a");

    if (!SkString::isEmpty(url))
        element->attr("href", url);

    if (!SkString::isEmpty(target))
        element->attr("target", target);

    return element;
}

SkWuiElement *SkWuiElement::wui_DIV(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("div");
}

SkWuiElement *SkWuiElement::wui_TEXTAREA(CStr *id, CStr *text, uint64_t rows, uint64_t cols, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElementWithText(id, text, parent);
    element->setTag("textarea");

    if (rows > 0)
    {
        SkString v = SkString::number(rows);
        element->attr("rows", v.c_str());
    }

    if (cols > 0)
    {
        SkString v = SkString::number(cols);
        element->attr("cols", v.c_str());
    }

    return element;
}

SkWuiElement *SkWuiElement::wui_IMG(CStr *id, CStr *src, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement(id, parent);
    element->setTag("img");

    if (!SkString::isEmpty(src))
        element->attr("src", src);

    return element;
}

SkWuiElement *SkWuiElement::wui_IFRAME(CStr *id, CStr *src, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("iframe")->attr("src", src);
}

SkWuiElement *SkWuiElement::wui_NAV(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("nav");
}

SkWuiElement *SkWuiElement::wui_UL(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("ul");
}

SkWuiElement *SkWuiElement::wui_OL(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("ol");
}

SkWuiElement *SkWuiElement::wui_LI(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("li");
}

SkWuiElement *SkWuiElement::wui_SECTION(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("section");
}

SkWuiElement *SkWuiElement::wui_HR(SkWuiElement *parent)
{
    return wuiElement("", parent)->setTag("hr");
}

SkWuiElement *SkWuiElement::wui_BR(SkWuiElement *parent)
{
    return wuiElement("", parent)->setTag("br");
}

SkWuiElement *SkWuiElement::wui_BUTTON(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("button");
}

SkWuiElement *SkWuiElement::wui_SCRIPT(CStr *src, SkWuiElement *parent)
{
    return wui_SCRIPT(parent)->attr("src", src);
}

SkWuiElement *SkWuiElement::wui_SCRIPT(SkWuiElement *parent)
{
    return wuiElement("", parent)->setTag("script");
}

SkWuiElement *SkWuiElement::wui_INLINESCRIPT(CStr *code, SkWuiElement *parent)
{
    return wuiElementWithText("", code, parent)->setTag("script");
}

SkWuiElement *SkWuiElement::wui_TABLE(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("table");
}

SkWuiElement *SkWuiElement::wui_THEAD(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("thead");
}

SkWuiElement *SkWuiElement::wui_TBODY(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("tbody");
}

SkWuiElement *SkWuiElement::wui_TFOOT(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("tfoot");
}

SkWuiElement *SkWuiElement::wui_TR(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("tr");
}

SkWuiElement *SkWuiElement::wui_TH(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("th");
}

SkWuiElement *SkWuiElement::wui_TD(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("td");
}

//FORMs AND INPUTs

SkWuiElement *SkWuiElement::wui_FORM(CStr *id, CStr *method, CStr *action, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement(id, parent);
    element->setTag("form");

    if (!SkString::isEmpty(method))
        element->attr("method", method);

    if (!SkString::isEmpty(action))
        element->attr("action", action);

    return element;
}

SkWuiElement *SkWuiElement::wui_FIELDSET(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("fieldset");
}

SkWuiElement *SkWuiElement::wui_LEGEND(CStr *id, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText(id, text, parent)->setTag("legend");
}

SkWuiElement *SkWuiElement::wui_INPUT(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("input");
}

SkWuiElement *SkWuiElement::wui_SELECT(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("select");
}

SkWuiElement *SkWuiElement::wui_OPTION(CStr *id, SkWuiElement *parent)
{
    return wuiElement(id, parent)->setTag("option");
}

SkWuiElement *SkWuiElement::wui_LABEL(CStr *idFor, CStr *text, SkWuiElement *parent)
{
    return wuiElementWithText("", text, parent)->setTag("label")->attr("for", idFor);
}

//HERE WE COULD ADD ALSO PARTICULAR INPUT ATTRs ON MAP RETURNED (For EX. int the case of a number-spin we could add: "min", "max", "step")
/*SkArgsMap SkWuiElement::abstractInputProps(CStr *type,
                                           CStr *name,
                                           CStr *placeHolder,
                                           SkVariant defaultValue,
                                           bool isChecked,            //ONLY CHECKABLE INPUT (EX.: radios and checkboxes)
                                           bool isRequired,
                                           bool isReadOnly,
                                           bool isDisabled)
{
    SkArgsMap dataMap;

    if (!SkString::isEmpty(type))//SOME INPUTs ARE NOT CLASSIC input (tag), AS IN THE CASE OG textarea FOR EX.
        dataMap["type"] = type;

    if (!SkString::isEmpty(name))
        dataMap["name"] = name;

    if (!SkString::isEmpty(placeHolder))
        dataMap["placeHolder"] = placeHolder;

    if (!defaultValue.isNull())
        dataMap["value"] = defaultValue;

    if (isChecked)
        dataMap["isChecked"] = isChecked;

    if (isRequired)
        dataMap["isRequired"] = isRequired;

    if (isReadOnly)
        dataMap["isReadOnly"] = isReadOnly;

    if (isDisabled)
        dataMap["isDisabled"] = isDisabled;

    return dataMap;
}*/
/*
static void setStandardInputFieldOpts(SkWuiElement *element, SkArgsMap &props)
{
    if (props.contains("name"))
    {
        SkString str = props["name"].toString();
        element->setID(str.c_str());
        element->attr("name", str.c_str());
    }

    if (props.contains("placeholder"))
    {
        SkString str = props["placeholder"].toString();
        element->attr("placeholder", str.c_str());
    }

    if (props.contains("isChecked") && props["isChecked"].toBool())
        element->attr("checked", "checked");

    if (props.contains("value"))
    {
        SkString str = props["value"].toString();
        element->attr("value", str.c_str());
    }

    if (props.contains("isRequired") && props["isRequired"].toBool())
        element->attr("required", "required");

    if (props.contains("isReadOnly") && props["isReadOnly"].toBool())
        element->attr("readonly", "readonly");

    if (props.contains("isDisabled") && props["isDisabled"].toBool())
        element->attr("disabled", "disabled");
}

SkWuiElement *SkWuiElement::wui_INPUT(SkArgsMap &props, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement("", parent);
    element->setTag("input");
    SkString str = props["type"].toString();
    element->attr("type", str.c_str());

    setStandardInputFieldOpts(element, props);

    return element;
}

SkWuiElement *SkWuiElement::wui_FORM_SUBMITBUTTON(CStr *label, SkArgsMap &props, SkWuiElement *parent)
{
    props["type"] = "submit";
    props["value"] = label;
    return wui_INPUT(props, parent);
}

SkWuiElement *SkWuiElement::wui_FORM_RESETBUTTON(CStr *label, SkArgsMap &props, SkWuiElement *parent)
{
    props["type"] = "reset";
    props["value"] = label;
    return wui_INPUT(props, parent);
}

SkWuiElement *SkWuiElement::wui_FIELDLABEL(CStr *fieldID, CStr *label, SkWuiElement *parent)
{
    return wuiElementWithText("", label, parent)->attr("for", fieldID)->setTag("label");
}

//INPUT type OR SPECIFIC TAG IS SET IN THESE FUNCTIONs; OTHER PROPS COULD BE SET INSIDE optProps
SkWuiElement *SkWuiElement::wui_INPUT_TEXT(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "text";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT_PASSWORD(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "password";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT_BOOL(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "checkbox";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT_RADIO(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "radio";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT_BLOB(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "file";
    return wui_INPUT(optProps, parent);
}

//IT IS NOT input, BUT textarea
SkWuiElement *SkWuiElement::wui_INPUT_CLOB(CStr *text, uint64_t rows, uint64_t cols, SkArgsMap &optProps, SkWuiElement *parent)
{
    SkWuiElement *element = wui_TEXTAREA("",
                                         text,
                                         rows,
                                         cols,
                                         parent);

    setStandardInputFieldOpts(element, optProps);
    return element;
}

SkWuiElement *SkWuiElement::wui_INPUT_SELECT(SkArgsMap &optProps, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement("", parent);
    element->setTag("select");

    setStandardInputFieldOpts(element, optProps);
    return element;
}

SkWuiElement *SkWuiElement::wui_INPUT_OPTGROUP(CStr *label, SkArgsMap &optProps, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElement("", parent);
    element->setTag("optgroup");
    element->attr("label", label);

    setStandardInputFieldOpts(element, optProps);
    return element;
}

SkWuiElement *SkWuiElement::wui_INPUT_OPT(CStr *label, SkArgsMap &optProps, SkWuiElement *parent)
{
    SkWuiElement *element = wuiElementWithText("", label, parent);
    element->setTag("option");

    setStandardInputFieldOpts(element, optProps);
    return element;
}

//HTML-5 INPUTs
SkWuiElement *SkWuiElement::wui_INPUT5_COLOR(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "color";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_DATE(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "date";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_DATETIMELOCAL(SkArgsMap& optProps, SkWuiElement *parent)
{
    optProps["type"] = "datetime-local";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_EMAIL(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "email";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_MONTH(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "month";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_NUMBER(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "number";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_RANGE(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "range";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_SEARCH(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "search";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_TEL(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "tel";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_TIME(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "time";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_URL(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "url";
    return wui_INPUT(optProps, parent);
}

SkWuiElement *SkWuiElement::wui_INPUT5_WEEK(SkArgsMap &optProps, SkWuiElement *parent)
{
    optProps["type"] = "week";
    return wui_INPUT(optProps, parent);
}*/

#endif
