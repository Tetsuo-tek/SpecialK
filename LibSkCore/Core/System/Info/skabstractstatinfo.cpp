#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)
/*
// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkAbstractStatInfo);

DeclareMeth_INSTANCE_VOID(SkAbstractStatInfo, toMap, *Arg_Custom(SkArgsMap))
DeclareMeth_INSTANCE_RET(SkAbstractStatInfo, toString, SkString)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractStatInfo, toString, 1, SkString, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractStatInfo, toString, 2, SkString, Arg_Bool, Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkAbstractStatInfo, getMap, SkArgsMap&)

SetupClassWrapper(SkAbstractStatInfo)
{
    SetClassSuper(SkAbstractStatInfo, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkAbstractStatInfo, toMap);
    AddMeth_INSTANCE_RET(SkAbstractStatInfo, toString);
    AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractStatInfo, toString, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractStatInfo, toString, 2);
    AddMeth_INSTANCE_RET(SkAbstractStatInfo, getMap);
}

// // // // // // // // // // // // // // // // // // // // //

SkAbstractStatInfo::SkAbstractStatInfo()
{
    CreateClassWrapper(SkAbstractStatInfo);
}

SkString SkAbstractStatInfo::toString(bool indented, uint8_t spaces)
{
    map.clear();
    toMap(map);
    SkString json;
    map.toString(json, indented, spaces);
    return json.c_str();
}

SkArgsMap &SkAbstractStatInfo::getMap()
{
    map.clear();
    toMap(map);
    return map;
}*/

#endif
