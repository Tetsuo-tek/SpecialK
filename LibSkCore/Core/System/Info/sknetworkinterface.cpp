#include "sknetworkinterface.h"
#include "Core/App/skapp.h"

#if defined(ENABLE_STATGRAB)
/*
SkNetworkInterface::SkNetworkInterface()
{
    name = nullptr;
    speed = 0;
    factor = 0;

    mode = SkNetInterfaceMode::Unknown;

    isUP = false;

    tx = 0;
    rx = 0;
    inputPackets = 0;
    inputErrors = 0;
    outputPackets = 0;
    outputErrors = 0;
    collisions = 0;
}

SkNetworkInterface::~SkNetworkInterface()
{
}

void SkNetworkInterface::read(sg_network_iface_stats &interface, sg_network_io_stats &stat)
{
    name = interface.interface_name;
    speed = interface.speed;
    factor = interface.factor;

    if (interface.duplex == SG_IFACE_DUPLEX_FULL)
        mode = SkNetInterfaceMode::FullDuplex;

    else if (interface.duplex == SG_IFACE_DUPLEX_HALF)
        mode = SkNetInterfaceMode::HalfDuplex;

    else
        mode = SkNetInterfaceMode::Unknown;

    isUP = (interface.up == SG_IFACE_UP);

    tx = stat.tx;
    rx = stat.rx;
    inputPackets = stat.ipackets;
    inputErrors = stat.ierrors;
    outputPackets = stat.opackets;
    outputErrors = stat.oerrors;
    collisions = stat.collisions;
}

CStr *SkNetworkInterface::getModeName(SkNetInterfaceMode m)
{
    if (m == FullDuplex)
        return "FullDuplex";

    else if (m == HalfDuplex)
        return "HalfDuplex";

    return "Unknown";
}

void SkNetworkInterface::toMap(SkArgsMap &map)
{
    map.add("name", name);
    map.add("Speed", speed*factor);
    map.add("Mode", SkNetworkInterface::getModeName(mode));
    map.add("isUP", isUP);
    map.add("Tx", tx);
    map.add("Rx", rx);
    map.add("IPack", inputPackets);
    map.add("IErr", outputPackets);
    map.add("OPack", outputPackets);
    map.add("OErr", outputErrors);
    map.add("Cl", collisions);
}
*/
#endif
