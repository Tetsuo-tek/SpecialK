/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTSTATINFO_H
#define SKABSTRACTSTATINFO_H

#if defined(ENABLE_STATGRAB)

/*#include "Core/Containers/skargsmap.h"
#include "Core/sklogmachine.h"
#include <statgrab.h>

class SPECIALK SkAbstractStatInfo extends SkFlatObject
{
    public:
        SkAbstractStatInfo();

        virtual void toMap(SkArgsMap &){}

        //THIS METHs MAKES UPDATE OF INTERNAL MAP
        SkString toString(bool indented=false, uint8_t spaces=4);
        SkArgsMap &getMap();

    private:
        SkArgsMap map;
};*/

#endif

#endif // SKABSTRACTSTATINFO_H
