/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKVMSTAT_H
#define SKVMSTAT_H

#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)
/*
struct SPECIALK SkRamStat extends SkAbstractStatInfo
{
    uint64_t total;
    uint64_t used;
    float usedPercent;
    uint64_t cache;
    uint64_t free;

    void toMap(SkArgsMap &map);
};

struct SPECIALK SkSwapStat extends SkAbstractStatInfo
{
    uint64_t total;
    uint64_t used;
    float usedPercent;
    uint64_t free;

    void toMap(SkArgsMap &map);
};

struct SPECIALK SkMemPagesStat extends SkAbstractStatInfo
{
    uint64_t pagesIn;
    uint64_t pagesOut;

    void toMap(SkArgsMap &map);
};

class SPECIALK SkVmStat extends SkAbstractStatInfo
{
    public:
        SkVmStat();

        void update();
        void toMap(SkArgsMap &map);

        SkRamStat ram;
        SkSwapStat swap;
        SkMemPagesStat pages;
};
*/
#endif

#endif // SKVMSTAT_H
