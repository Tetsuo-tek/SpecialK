/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKPROCESSSTAT_H
#define SKPROCESSSTAT_H

#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)
/*
enum SkProcessState
{
    PS_UNKNOWN,
    PS_RUNNING,
    PS_SLEEPING,
    PS_STOPPED,
    PS_ZOMBIE
};

class SPECIALK SkProcessesStat extends SkAbstractStatInfo
{
    public:
        SkProcessesStat();
        ~SkProcessesStat();

        void read(sg_process_stats &process);
        void reset();

        static CStr *getStateName(SkProcessState m);

        void toMap(SkArgsMap &map);

        char *name;
        char *title;

        SkProcessState state;

        pid_t pid;
        pid_t ppid;
        pid_t pgid;
        pid_t sid;

        uid_t uid;
        uid_t effectiveUid;

        gid_t gid;
        gid_t effectiveGid;

        uint64_t size;
        uint64_t res;

        time_t startTime;
        char *startLocalTime;

        time_t elapsedTimeSeconds;

        double cpu;
        int nice;

        uint64_t contextSwitches;
        uint64_t contextVoluntary;
        uint64_t contextInvolutary;
};
*/
#endif

#endif // SKPROCESSSTAT_H
