#include "skvmstat.h"
#include "Core/skmath.h"

#if defined(ENABLE_STATGRAB)
/*
SkVmStat::SkVmStat()
{
    ram.total = 0;
    ram.used = 0;
    ram.cache = 0;
    ram.free = 0;
    ram.usedPercent = 0;

    swap.total = 0;
    swap.used = 0;
    swap.free = 0;
    swap.usedPercent = 0;

    pages.pagesIn = 0;
    pages.pagesOut = 0;
}

void SkVmStat::update()
{
    sg_mem_stats *mem_stats = sg_get_mem_stats(nullptr);
    sg_swap_stats *swap_stats = sg_get_swap_stats(nullptr);

    if (!mem_stats || !swap_stats)
    {
        FlatError("Cannot get VmStat");
        return;
    }

    ram.total = mem_stats->total;
    ram.used = mem_stats->used;
    ram.usedPercent = SkMath::percent(ram.total, ram.used);
    ram.cache = mem_stats->cache;
    ram.free = mem_stats->free;

    swap.total = swap_stats->total;
    swap.used = swap_stats->used;
    swap.usedPercent = SkMath::percent(swap.total, swap.used);
    swap.free = swap_stats->free;

    sg_page_stats *page_stats = sg_get_page_stats_diff(nullptr);

    if (!page_stats)
    {
        FlatError("Cannot get VmStat-pagination");
        return;
    }

    pages.pagesIn = page_stats->pages_pagein;
    pages.pagesOut = page_stats->pages_pageout;
}

void SkRamStat::toMap(SkArgsMap &map)
{
    map.add("total", total);
    map.add("used", used);
    map.add("usedPercent", usedPercent);
    map.add("cache", cache);
    map.add("free", free);
}

void SkSwapStat::toMap(SkArgsMap &map)
{
    map.add("total", total);
    map.add("used", used);
    map.add("usedPercent", usedPercent);
    map.add("cache", free);
}

void SkMemPagesStat::toMap(SkArgsMap &map)
{
    map.add("pagesIn", pagesIn);
    map.add("pagesOut", pagesOut);
}

void SkVmStat::toMap(SkArgsMap &map)
{
    map.add("ram", ram.getMap());
    map.add("swap", swap.getMap());
    map.add("pages", pages.getMap());
}*/

#endif
