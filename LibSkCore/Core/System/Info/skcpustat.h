/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKCPUSTAT_H
#define SKCPUSTAT_H

#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)
/*
struct SPECIALK SkCpuCoreTime extends SkAbstractStatInfo
{
    uint64_t time;
    double percent;

    void toMap(SkArgsMap &map);
};

class SPECIALK SkCpuStat extends SkAbstractStatInfo
{
    public:
        SkCpuStat();

        void update();
        void toMap(SkArgsMap &map);

        SkCpuCoreTime user;
        SkCpuCoreTime kernel;
        SkCpuCoreTime iowait;
        SkCpuCoreTime swap;
        SkCpuCoreTime nice;
        SkCpuCoreTime idle;

        uint64_t contextSwitches;
        uint64_t contextVoluntary;
        uint64_t contextInvolutary;

        uint64_t sysCalls;
        uint64_t hardInterrupts;
        uint64_t softInterrupts;

        double load_1;
        double load_5;
        double load_15;
};
*/
#endif

#endif // SKCPUSTAT_H
