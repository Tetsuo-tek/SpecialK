#include "skdiskstat.h"
#include "Core/App/skapp.h"

#if defined(ENABLE_STATGRAB)

/*DeclareWrapper(SkDiskStat);

DeclareMeth_INSTANCE_VOID(SkDiskStat, read, *Arg_Custom(sg_disk_io_stats))
//DeclareMeth_INSTANCE_VOID(SkDiskStat, reset)
DeclareMeth_INSTANCE_VOID(SkDiskStat, toMap, *Arg_Custom(SkArgsMap))

SetupClassWrapper(SkDiskStat)
{
    SetClassSuper(SkDiskStat, SkAbstractStatInfo);

    AddMeth_INSTANCE_VOID(SkDiskStat, read);
    //AddMeth_INSTANCE_VOID(SkDiskStat, reset);
    AddMeth_INSTANCE_VOID(SkDiskStat, toMap);
}

SkDiskStat::SkDiskStat()
{
    CreateClassWrapper(SkDiskStat);

    name = nullptr;

    tx = 0;
    rx = 0;
}

SkDiskStat::~SkDiskStat()
{
}

void SkDiskStat::read(sg_disk_io_stats &disk)
{
    name = disk.disk_name;
    tx = disk.write_bytes;
    rx = disk.read_bytes;
}

void SkDiskStat::toMap(SkArgsMap &map)
{
    map.add("Name", name);
    map.add("Tx", tx);
    map.add("Rx", rx);
}*/

#endif
