#include "skcpustat.h"

#if defined(ENABLE_STATGRAB)
/*
#include "Core/Containers/sklist.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STRUCT_TOMAP(SkCpuCoreTime)
{
    SetupStructWrapper(SkCpuCoreTime);

    StructToMapProperty(time);
    StructToMapProperty(percent);
}

DeclareWrapper_STRUCT_FROMMAP(SkCpuCoreTime)
{
    MapToStructProperty(time).toUInt64();
    MapToStructProperty(percent).toDouble();
}

DeclareWrapper(SkCpuStat);

DeclareMeth_INSTANCE_VOID(SkCpuStat, update)

SetupClassWrapper(SkCpuStat)
{
    AddStructType(SkCpuCoreTime);
    //SetStructSuper(SkCpuCoreTime, SkAbstractStatInfo);

    AddMeth_INSTANCE_VOID(SkCpuStat, update);
}

// // // // // // // // // // // // // // // // // // // // //

SkCpuStat::SkCpuStat()
{
    CreateClassWrapper(SkCpuStat);

    contextSwitches = 0;
    contextVoluntary = 0;
    contextInvolutary = 0;

    sysCalls = 0;
    hardInterrupts = 0;
    softInterrupts = 0;

    load_1 = 0;
    load_5 = 0;
    load_15 = 0;
}

void SkCpuStat::update()
{
    sg_cpu_stats *cpu_diff_stats = sg_get_cpu_stats_diff(nullptr);
    sg_cpu_percents *cpu_percent = sg_get_cpu_percents_of(sg_last_diff_cpu_percent, nullptr);

    if (!cpu_diff_stats || !cpu_percent)
    {
        FlatError("Cannot get HostInfo");
        return;
    }

    user.time = cpu_diff_stats->user;
    user.percent = cpu_percent->user;

    kernel.time = cpu_diff_stats->kernel;
    kernel.percent = cpu_percent->kernel;

    iowait.time = cpu_diff_stats->iowait;
    iowait.percent = cpu_percent->iowait;

    swap.time = cpu_diff_stats->swap;
    swap.percent = cpu_percent->swap;

    nice.time = cpu_diff_stats->nice;
    nice.percent = cpu_percent->nice;

    idle.time = cpu_diff_stats->idle;
    idle.percent = cpu_percent->idle;

    contextSwitches = cpu_diff_stats->context_switches;
    contextVoluntary = cpu_diff_stats->voluntary_context_switches;
    contextInvolutary = cpu_diff_stats->involuntary_context_switches;

    sysCalls = cpu_diff_stats->syscalls;
    hardInterrupts = cpu_diff_stats->interrupts;
    softInterrupts = cpu_diff_stats->soft_interrupts;

    sg_load_stats *load_stat = sg_get_load_stats(nullptr);
    load_1 = load_stat->min1;
    load_5 = load_stat->min5;
    load_15 = load_stat->min15;
}

void SkCpuCoreTime::toMap(SkArgsMap &map)
{
    map.add("time", time);
    map.add("percent", percent);
}

void SkCpuStat::toMap(SkArgsMap &map)
{
    map.add("user", user.getMap());
    map.add("kernel", kernel.getMap());
    map.add("idle", idle.getMap());
    map.add("nice", nice.getMap());
    map.add("iowait", iowait.getMap());
    map.add("swap", swap.getMap());

    SkList<SkVariant> loadsArray;
    loadsArray.append(load_1);
    loadsArray.append(load_5);
    loadsArray.append(load_15);

    SkVariant l(loadsArray);
    map.add("loadsArray", l);

    map.add("contextSwitches", contextSwitches);
    map.add("contextVoluntary", contextVoluntary);
    map.add("contextInvolutary", contextInvolutary);

    map.add("sysCalls", sysCalls);
    map.add("hardInterrupts", hardInterrupts);
    map.add("softInterrupts", softInterrupts);
}
*/
#endif
