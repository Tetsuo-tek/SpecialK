/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKPROCSTAT_H
#define SKPROCSTAT_H

#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)
/*
class SPECIALK SkProcStat extends SkAbstractStatInfo
{
    public:
        SkProcStat();

        void update();
        void toMap(SkArgsMap &map);

        uint64_t total;
        uint64_t running;
        uint64_t sleeping;
        uint64_t stopped;
        uint64_t zombie;
};
*/
#endif

#endif // SKPROCSTAT_H
