/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSTATGRAB_H
#define SKSTATGRAB_H

#if defined(ENABLE_STATGRAB)

//#include "skhostinfo.h"
/*#include "skcpustat.h"
#include "skprocstat.h"
#include "skprocessstat.h"
#include "skvmstat.h"
#include "skdiskstat.h"
#include "sknetworkinterface.h"
#include "skloggeduser.h"

class SPECIALK SkStatGrab extends SkFlatObject
{
    public:
        SkStatGrab();
        ~SkStatGrab();

        void init();

        SkHostInfo *getHostInfo();
        SkCpuStat *getCpuStat();
        SkProcStat *getProcStat();
        SkVmStat *getVmStat();

        SkProcessStat *monitorProcesses(uint64_t &count, CStr *procNameFilter=nullptr, bool sorted=true);
        SkDiskStat *monitorFileSystems(uint64_t &count, bool sorted=true);
        SkNetworkInterface *monitorNetworks(uint64_t &count);
        SkLoggedUser *monitorLoggedUsers(uint64_t &count);

        void getSupportedFileSystems(SkStringList &fsSupportedTypes);
        void getNetworkAddrs(SkMap<SkString, SkString> &m);

    private:
        SkHostInfo *osInfo;
        SkCpuStat *cpuStat;
        SkProcStat *procStat;
        SkVmStat *vmStat;

        SkProcessStat *processes;
        SkDiskStat *disks;
        SkNetworkInterface *interfaces;
        SkLoggedUser *loggedUsers;
};*/

#include "Core/Containers/skstringlist.h"
#include "Core/Containers/sktreemap.h"

struct SkHostInfo
{
    SkString osName;
    SkString osRelease;
    SkString osVersion;
    SkString platform;
    SkString hostName;

    Long upTime;
    UInt processors;
    UInt processorsOnLine;
    UInt bitsWidth;
};

struct SkCpuCoreTime
{
    ULong time;
    double percent;
};

struct SkSysStat
{
    SkCpuCoreTime user;
    SkCpuCoreTime kernel;
    SkCpuCoreTime iowait;
    SkCpuCoreTime swap;
    SkCpuCoreTime nice;
    SkCpuCoreTime idle;

    ULong contextSwitches;
    ULong contextVoluntary;
    ULong contextInvolutary;

    ULong sysCalls;
    ULong hardInterrupts;
    ULong softInterrupts;

    double load_1;
    double load_5;
    double load_15;
};

struct SkProcessesStat
{
    ULong total;
    ULong running;
    ULong sleeping;
    ULong stopped;
    ULong zombie;
};

struct SkMemoryStat
{
    ULong total;
    ULong used;
    float usedPercent;
    ULong cache;
    ULong free;

    ULong pagesIn;
    ULong pagesOut;
};

struct SkSwapStat
{
    ULong total;
    ULong used;
    float usedPercent;
    ULong free;
};

enum SkNetInterfaceMode
{
    Unknown,
    FullDuplex,
    HalfDuplex
};

struct SkNetworkInterfaceInfo
{
    SkString name;

    ULong speed;
    ULong factor;
    SkNetInterfaceMode mode;
    bool isUP;

    SkStringList ipAddresses_V4;
    SkStringList ipAddresses_V6;

    ULong tx;
    ULong rx;
    ULong inputPackets;
    ULong inputErrors;
    ULong outputPackets;
    ULong outputErrors;
    ULong collisions;
};

enum SkProcessState
{
    PS_UNKNOWN,
    PS_RUNNING,
    PS_SLEEPING,
    PS_STOPPED,
    PS_ZOMBIE
};

struct SkProcessInfo
{
    SkString name;
    SkString title;

    SkProcessState state;

    pid_t pid;
    pid_t ppid;
    pid_t pgid;
    pid_t sid;

    uid_t uid;
    uid_t effectiveUid;

    gid_t gid;
    gid_t effectiveGid;

    ULong size;
    ULong res;

    time_t startTime;
    char *startLocalTime;

    time_t elapsedTimeSeconds;

    double cpu;
    int nice;

    ULong contextSwitches;
    ULong contextVoluntary;
    ULong contextInvolutary;
};

struct SkFsInfo
{
    SkString name;
    ULong tx;
    ULong rx;
};

class SPECIALK SkProcFs extends SkFlatObject
{
    public:
        SkProcFs();
        ~SkProcFs();

        void tick();

        bool grabHostInfo();
        bool grabSystemStat();
        bool grabProcessStat();
        bool grabMemoryStat();
        bool grabSwapStat();
        //bool grabProcessInfo();
        bool grabNetworkStat();
        bool grabFsInfo();

        bool supportedFileSystems(SkStringList &fsTypes);

        SkHostInfo *hostInfo();
        SkSysStat *systemStat();
        SkProcessesStat *processesStat();
        SkMemoryStat *memoryStat();
        SkSwapStat *swapStat();

        void networkInterfacesNames(SkStringList &names);
        void fileSystemsNames(SkStringList &names);

        //SkProcessInfo *processInfo(CStr *name);
        SkNetworkInterfaceInfo *networkInterfaceInfo(CStr *name);
        SkFsInfo *fileSystemInfo(CStr *name);

        //SkBinaryTreeVisit<SkString, SkProcessInfo *> *processesIterator();
        SkBinaryTreeVisit<SkString, SkNetworkInterfaceInfo *> *networkInterfacesIterator();
        SkBinaryTreeVisit<SkString, SkFsInfo *> *fileSystemsIterator();

    private:
        SkHostInfo hInfo;
        SkSysStat sStat;
        SkProcessesStat pStat;
        SkMemoryStat mStat;
        SkSwapStat swStat;
        SkSwapStat pInfo;

        //SkTreeMap<SkString, SkProcessInfo *> processes;
        SkTreeMap<SkString, SkNetworkInterfaceInfo *> networkInterfaces;
        SkTreeMap<SkString, SkFsInfo *> fileSystems;

        bool read(CStr *procFilePath, SkString &content);
};

#endif

#endif // SKSTATGRAB_H
