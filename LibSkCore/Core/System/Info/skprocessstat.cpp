#include "skprocessstat.h"
#include "Core/System/Time/skdatetime.h"
#include "Core/App/skapp.h"

#if defined(ENABLE_STATGRAB)
/*
SkProcessesStat::SkProcessesStat()
{
    name = nullptr;
    title = nullptr;
    startLocalTime = nullptr;

    pid = 0;
    ppid = 0;
    pgid = 0;

    uid = 0;
    effectiveUid = 0;

    gid = 0;
    effectiveGid = 0;

    sid = 0;

    size = 0;
    res = 0;

    startTime = 0;
    startLocalTime = nullptr;
    elapsedTimeSeconds = 0;

    cpu = 0;

    nice = 0;

    contextSwitches = 0;
    contextVoluntary = 0;
    contextInvolutary = 0;

    state = SkProcessState::PS_UNKNOWN;
}

SkProcessesStat::~SkProcessesStat()
{
}

void SkProcessesStat::read(sg_process_stats &process)
{
    name = process.process_name;
    title = process.proctitle;

    pid = process.pid;
    ppid = process.parent;
    pgid = process.pgid;

    uid = process.uid;
    effectiveUid = process.euid;

    gid = process.gid;
    effectiveGid = process.egid;

    size = process.proc_size;
    res = process.proc_resident;

    startTime = process.start_time;

    SkDateTime dt;
    SkString s = dt.toISOString();
    startLocalTime = buildTxtCopy(s.data());

    SkString tempData = SkString::localTime(process.start_time);
    startLocalTime = SK_TXTCOPY(tempData.data());
    elapsedTimeSeconds = process.time_spent;

    cpu = process.cpu_percent;

    nice = process.nice;

    contextSwitches = process.context_switches;
    contextVoluntary = process.voluntary_context_switches;
    contextInvolutary = process.involuntary_context_switches;

    if (process.state == SG_PROCESS_STATE_RUNNING)
        state = SkProcessState::PS_RUNNING;

    else if (process.state == SG_PROCESS_STATE_SLEEPING)
        state = SkProcessState::PS_SLEEPING;

    else if (process.state == SG_PROCESS_STATE_STOPPED)
        state = SkProcessState::PS_STOPPED;

    else if (process.state == SG_PROCESS_STATE_ZOMBIE)
        state = SkProcessState::PS_ZOMBIE;

    else
        state = SkProcessState::PS_UNKNOWN;
}

CStr *SkProcessesStat::getStateName(SkProcessState m)
{
    if (m == SkProcessState::PS_RUNNING)
        return "Running";

    else if (m == SkProcessState::PS_SLEEPING)
        return "Sleeping";

    else if (m == SkProcessState::PS_STOPPED)
        return "Stopped";

    else if (m == SkProcessState::PS_ZOMBIE)
        return "Zombie";

    return "Unknown";
}

void SkProcessesStat::toMap(SkArgsMap &map)
{
    map.add("Name", name);
    map.add("Title", title);

    map.add("RES", res);
    map.add("SZ", size);

    map.add("PID", pid);
    map.add("PPID", ppid);
    map.add("GPID", pgid);
    map.add("SID", sid);

    map.add("UID", uid);
    map.add("EUID", effectiveUid);
    map.add("GID", gid);
    map.add("EGID", effectiveGid);

    //map.insert("StartLocalTime", startLocalTime);
    map.add("StartTime", static_cast<uint64_t>(startTime));
    map.add("Time", static_cast<uint64_t>(elapsedTimeSeconds));
    SkString s = SkString::number(cpu, 1);
    map.add("CpuUsage", s.c_str());
    map.add("Nice", nice);

    map.add("CtxtSwitches", contextSwitches);
    map.add("CtxtVoluntary", contextVoluntary);
    map.add("CtxtInvolutary", contextInvolutary);

    map.add("State", SkProcessesStat::getStateName(state));
}*/

#endif
