/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKHOSTINFO_H
#define SKHOSTINFO_H

#include "skabstractstatinfo.h"

#if defined(ENABLE_STATGRAB)

/*class SPECIALK SkHostInfo extends SkAbstractStatInfo
{
    public:
        SkHostInfo();
        ~SkHostInfo();

        void update();
        void toMap(SkArgsMap &map);

        char *osName;
        char *osRelease;
        char *osVersion;
        char *platform;
        char *hostName;

        int64_t upTime;
        uint32_t processors;
        uint32_t processorsOnLine;
        uint32_t bitsWidth;
};*/

#endif

#endif // SKHOSTINFO_H
