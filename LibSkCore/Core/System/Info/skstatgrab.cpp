#include "skstatgrab.h"

#if defined(ENABLE_STATGRAB)

#include "Core/App/skapp.h"
#include "Core/System/Filesystem/skfsutils.h"

#include <unistd.h>

#include <statgrab.h>

/*#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

SkProcFs::SkProcFs()
{
    osInfo = nullptr;
    cpuStat = nullptr;
    procStat = nullptr;
    vmStat = nullptr;

    processes = nullptr;
    disks = nullptr;
    interfaces = nullptr;
    loggedUsers = nullptr;
}

SkProcFs::~SkProcFs()
{
    if (osInfo)
    {
        sg_shutdown();

        delete osInfo;
        delete cpuStat;
        delete procStat;
        delete vmStat;

        if (processes)
            delete [] processes;
    }
}

void SkProcFs::init()
{
    sg_init(0);

    if (sg_drop_privileges() != SG_ERROR_NONE)
        cerr << "Failed to drop privileges\n";

    osInfo = new SkHostInfo();
    cpuStat = new SkCpuStat();
    procStat = new SkProcStat();
    vmStat = new SkVmStat();

    osInfo->update();
    cpuStat->update();
    procStat->update();
    vmStat->update();
}

SkProcessStat *SkProcFs::monitorProcesses(uint64_t &count, CStr *procNameFilter, bool sorted)
{
    size_t c = 0;
    size_t ps_size = 0;
    sg_process_stats *ps = sg_get_process_stats(&ps_size);

    if (!ps)
    {
        FlatDebug("Cannot get Processes");
        c = 0;
        return nullptr;
    }

    if (ps_size == 0)
    {
        FlatError("Cannot get Processes (count == 0)");
        return nullptr;
    }

    if (sorted)
        qsort(ps, ps_size, sizeof(*ps), sg_process_compare_cpu);

    if (processes)
        delete [] processes;

    processes = new SkProcessStat [ps_size];

    for(uint64_t i=0; i<ps_size; i++)
    {
        bool compared = false;

        if (!procNameFilter ||
                (procNameFilter && (compared = memcmp(procNameFilter, ps[i].process_name, strlen(procNameFilter))) == 0))
        {
            processes[c].read(ps[i]);
            c++;

            if (compared)
                break;
        }
    }

    count = c;
    return processes;
}

SkDiskStat *SkProcFs::monitorFileSystems(uint64_t &count, bool sorted)
{
    size_t c = 0;
    sg_disk_io_stats *diskio_stats = sg_get_disk_io_stats_diff(&c);

    if (!diskio_stats)
    {
        FlatError("Cannot get Disks");
        c = 0;
        return nullptr;
    }

    if (c == 0)
    {
        FlatError("Cannot get Disks (count == 0)");
        return nullptr;
    }

    if (sorted)
        qsort(diskio_stats,
              c,
              sizeof(diskio_stats[0]),
              sg_disk_io_compare_traffic);

    if (disks)
        delete [] disks;

    disks = new SkDiskStat[c];

    for(uint64_t i=0; i<c; i++)
        disks[i].read(diskio_stats[i]);

    count = c;
    return disks;
}

SkNetworkInterface *SkProcFs::monitorNetworks(uint64_t &count)
{
    size_t c = 0;
    sg_network_iface_stats *network_iface_stats = sg_get_network_iface_stats(&c);

    size_t statsCount = 0;
    sg_network_io_stats *network_stats = sg_get_network_io_stats_diff(&statsCount);

    if (!network_iface_stats || !network_stats)
    {
        FlatError("Cannot get Network-interfaces");
        c = 0;
        return nullptr;
    }

    if (c == 0)
    {
        FlatError("Cannot get Network-interfaces (count == 0)");
        return nullptr;
    }

    if (c != statsCount)
    {
        c = 0;
        FlatError("Interfaces count differs from Stats count");
        return nullptr;
    }

    if (interfaces)
        delete [] interfaces;

    interfaces = new SkNetworkInterface[c];

    for(uint64_t i=0; i<c; i++)
        interfaces[i].read(network_iface_stats[i], network_stats[i]);

    count = c;
    return interfaces;
}

SkLoggedUser *SkProcFs::monitorLoggedUsers(uint64_t &count)
{
    size_t c = 0;
    sg_user_stats *users = sg_get_user_stats(&c);

    if (!users)
    {
        FlatError("Cannot get Logged-users");
        c = 0;
        return nullptr;
    }

    if (c == 0)
    {
        FlatError("Cannot get Logged-users (count == 0)");
        return nullptr;
    }

    if (loggedUsers)
        delete [] loggedUsers;

    loggedUsers = new SkLoggedUser[c];

    for(uint64_t i=0; i<c; i++)
        loggedUsers[i].read(users[i]);

    count = c;
    return loggedUsers;
}

void SkProcFs::getSupportedFileSystems(SkStringList &fsSupportedTypes)
{
    size_t count = 0;
    CStr **valid_fs = sg_get_valid_filesystems(&count);

    if (count == 0)
    {
        FlatError("Cannot get supported-fs (count == 0)");
        return;
    }

    for(uint64_t i=0; i<count; i++)
        fsSupportedTypes.append(valid_fs[i]);
}

void SkProcFs::getNetworkAddrs(SkMap<SkString, SkString> &m)
{
    m.clear();

    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    char *addr;

    getifaddrs(&ifap);

    for (ifa=ifap; ifa; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr->sa_family == AF_INET)
        {
            sa = (struct sockaddr_in *) ifa->ifa_addr;
            addr = inet_ntoa(sa->sin_addr);
            //printf("Interface: %s\tAddress: %s\n", ifa->ifa_name, addr);

            m.add(ifa->ifa_name, addr);
        }
    }

    freeifaddrs(ifap);
}

SkHostInfo *SkProcFs::getHostInfo()
{
    return osInfo;
}

SkCpuStat *SkProcFs::getCpuStat()
{
    return cpuStat;
}

SkProcStat *SkProcFs::getProcStat()
{
    return procStat;
}

SkVmStat *SkProcFs::getVmStat()
{
    return vmStat;
}*/

SkProcFs::SkProcFs()
{
    sg_init(0);

    if (sg_drop_privileges() != SG_ERROR_NONE)
        FlatError("Failed to drop privileges");
}

SkProcFs::~SkProcFs()
{
    sg_shutdown();
}

bool SkProcFs::grabHostInfo()
{
    sg_host_info *general_stats = sg_get_host_info(nullptr);

    if (!general_stats)
    {
        FlatError("CANNOT read hostInfo");
        return false;
    }

    if (hInfo.osName.isEmpty())
    {
        hInfo.osName = general_stats->os_name;
        hInfo.osRelease = general_stats->os_release;
        hInfo.osVersion = general_stats->os_version;
        hInfo.platform = general_stats->platform;
        hInfo.hostName = general_stats->hostname;
        hInfo.bitsWidth = general_stats->bitwidth;
    }

    hInfo.upTime = general_stats->uptime;
    hInfo.processors = general_stats->maxcpus;
    hInfo.processorsOnLine = general_stats->ncpus;

    return true;
}

bool SkProcFs::grabSystemStat()
{
    sg_cpu_stats *cpu_diff_stats = sg_get_cpu_stats_diff(nullptr);
    sg_cpu_percents *cpu_percent = sg_get_cpu_percents_of(sg_last_diff_cpu_percent, nullptr);

    if (!cpu_diff_stats || !cpu_percent)
    {
        FlatError("CANNOT read SystemStat");
        return false;
    }

    sStat.user.time = cpu_diff_stats->user;
    sStat.user.percent = cpu_percent->user;

    sStat.kernel.time = cpu_diff_stats->kernel;
    sStat.kernel.percent = cpu_percent->kernel;

    sStat.iowait.time = cpu_diff_stats->iowait;
    sStat.iowait.percent = cpu_percent->iowait;

    sStat.swap.time = cpu_diff_stats->swap;
    sStat.swap.percent = cpu_percent->swap;

    sStat.nice.time = cpu_diff_stats->nice;
    sStat.nice.percent = cpu_percent->nice;

    sStat.idle.time = cpu_diff_stats->idle;
    sStat.idle.percent = cpu_percent->idle;

    sStat.contextSwitches = cpu_diff_stats->context_switches;
    sStat.contextVoluntary = cpu_diff_stats->voluntary_context_switches;
    sStat.contextInvolutary = cpu_diff_stats->involuntary_context_switches;

    sStat.sysCalls = cpu_diff_stats->syscalls;
    sStat.hardInterrupts = cpu_diff_stats->interrupts;
    sStat.softInterrupts = cpu_diff_stats->soft_interrupts;

    sg_load_stats *load_stat = sg_get_load_stats(nullptr);
    sStat.load_1 = load_stat->min1;
    sStat.load_5 = load_stat->min5;
    sStat.load_15 = load_stat->min15;

    return true;
}

bool SkProcFs::grabProcessStat()
{
    sg_process_count *process_stat = sg_get_process_count();

    if (!process_stat)
    {
        FlatError("CANNOT read ProcessesStat");
        return false;
    }

    pStat.total = process_stat->total;
    pStat.running = process_stat->running;
    pStat.sleeping = process_stat->sleeping;
    pStat.stopped = process_stat->stopped;
    pStat.zombie = process_stat->zombie;

    return true;
}

bool SkProcFs::grabMemoryStat()
{
    sg_mem_stats *mem_stats = sg_get_mem_stats(nullptr);

    if (!mem_stats)
    {
        FlatError("CANNOT read MemoryStat");
        return false;
    }

    mStat.total = mem_stats->total;
    mStat.used = mem_stats->used;
    mStat.usedPercent = 0.f;

    if (mStat.used && mStat.total)//formality :)
    {
        float max = mStat.total;
        float val = mStat.used;

        mStat.usedPercent = (val / max) * 100.f;
    }

    mStat.cache = mem_stats->cache;
    mStat.free = mem_stats->free;

    sg_page_stats *page_stats = sg_get_page_stats_diff(nullptr);

    if (!page_stats)
    {
        FlatError("Cannot read MemoryStat pagination");
        return false;
    }

    mStat.pagesIn = page_stats->pages_pagein;
    mStat.pagesOut = page_stats->pages_pageout;

    return true;
}

bool SkProcFs::grabSwapStat()
{
    sg_swap_stats *swap_stats = sg_get_swap_stats(nullptr);

    if (!swap_stats)
    {
        FlatError("CANNOT read SwapStat");
        return false;
    }

    swStat.total = swap_stats->total;
    swStat.used = swap_stats->used;
    swStat.usedPercent = 0.f;

    if (swStat.used && swStat.total)//formality :)
    {
        float max = swStat.total;
        float val = swStat.used;

        swStat.usedPercent = (val / max) * 100.f;
    }

    swStat.free = swap_stats->free;

    return true;
}

#if defined(UMBA)
bool SkProcFs::grabProcessInfo()
{
    size_t c = 0;
    size_t ps_size = 0;
    sg_process_stats *ps = sg_get_process_stats(&ps_size);

    if (!ps)
    {
        FlatDebug("CANNOT read Processes informations");
        c = 0;
        return false;
    }

    if (ps_size == 0)
    {
        FlatError("Cannot get Processes (count == 0)");
        return false;
    }

    /*if (sorted)
        qsort(ps, ps_size, sizeof(*ps), sg_process_compare_cpu);*/

    for(uint64_t i=0; i<ps_size; i++)
    {
        sg_process_stats &processInfo = ps[i];
        SkProcessInfo *process = nullptr;

        if (processes.contains(processInfo.process_name))
            SkProcessInfo = processes[processInfo.process_name];

        else
        {
            process = new SkProcessInfo;

            processes[processInfo.process_name] = process;
        }

        process->name = processInfo.process_name;
        process->title = processInfo.proctitle;

        process->pid = processInfo.pid;
        process->ppid = processInfo.parent;
        process->pgid = processInfo.pgid;

        process->uid = processInfo.uid;
        process->effectiveUid = processInfo.euid;

        process->gid = process.gid;
        process->effectiveGid = processInfo.egid;

        process->size = processInfo.proc_size;
        process->res = processInfo.proc_resident;

        process->startTime = processInfo.start_time;

        /*SkDateTime dt;
    SkString s = dt.toISOString();
    startLocalTime = buildTxtCopy(s.data());*/

        /*SkString tempData = SkString::localTime(process.start_time);
    startLocalTime = SK_TXTCOPY(tempData.data());*/
        process->elapsedTimeSeconds = processInfo.time_spent;

        process->cpu = processInfo.cpu_percent;

        process->nice = processInfo.nice;

        process->contextSwitches = processInfo.context_switches;
        process->contextVoluntary = processInfo.voluntary_context_switches;
        process->contextInvolutary = processInfo.involuntary_context_switches;

        if (processInfo.state == SG_PROCESS_STATE_RUNNING)
            process->state = SkProcessState::PS_RUNNING;

        else if (processInfo.state == SG_PROCESS_STATE_SLEEPING)
            process->state = SkProcessState::PS_SLEEPING;

        else if (processInfo.state == SG_PROCESS_STATE_STOPPED)
            process->state = SkProcessState::PS_STOPPED;

        else if (processInfo.state == SG_PROCESS_STATE_ZOMBIE)
            process->state = SkProcessState::PS_ZOMBIE;

        else
            process->state = SkProcessState::PS_UNKNOWN;
    }

    return true;
}
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netdb.h>

bool SkProcFs::grabNetworkStat()
{
    size_t c = 0;
    sg_network_iface_stats *network_iface_stats = sg_get_network_iface_stats(&c);

    size_t statsCount = 0;
    sg_network_io_stats *network_stats = sg_get_network_io_stats_diff(&statsCount);

    if (!network_iface_stats || !network_stats)
    {
        FlatError("CANNOT read Network-interfaces");
        c = 0;
        return false;
    }

    if (c == 0)
    {
        FlatError("System has NOT Network interfaces (count == 0)");
        return false;
    }

    for(uint64_t i=0; i<c; i++)
    {
        sg_network_iface_stats &interfaceInfo = network_iface_stats[i];
        SkNetworkInterfaceInfo *interface = nullptr;

        if (networkInterfaces.contains(interfaceInfo.interface_name))
            interface = networkInterfaces[interfaceInfo.interface_name];

        else
        {
            interface = new SkNetworkInterfaceInfo;
            interface->name = interfaceInfo.interface_name;
            interface->speed = interfaceInfo.speed;
            interface->factor = interfaceInfo.factor;

            if (interfaceInfo.duplex == SG_IFACE_DUPLEX_FULL)
                interface->mode = SkNetInterfaceMode::FullDuplex;

            else if (interfaceInfo.duplex == SG_IFACE_DUPLEX_HALF)
                interface->mode = SkNetInterfaceMode::HalfDuplex;

            else
                interface->mode = SkNetInterfaceMode::Unknown;

            networkInterfaces[interfaceInfo.interface_name] = interface;
        }

        //

        /*int fd;
        struct ifreq ifr;

        fd = socket(AF_INET, SOCK_DGRAM, 0);

        // I want to get an IPv4 IP address
        ifr.ifr_addr.sa_family = AF_INET;

        // I want IP address attached to "eth0"
        strncpy(ifr.ifr_name, interface->name.c_str(), IFNAMSIZ-1);

        ioctl(fd, SIOCGIFADDR, &ifr);

        close(fd);

        printf("%s %s\n", interface->name.c_str(), inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

        struct ifaddrs *ifap, *ifa;
        struct sockaddr_in6 *sa;
        char addr[INET6_ADDRSTRLEN];

        if (getifaddrs(&ifap) == -1) {
            perror("getifaddrs");
            exit(1);
        }

        for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
            if (ifa->ifa_addr && ifa->ifa_addr->sa_family==AF_INET6) {
                sa = (struct sockaddr_in6 *) ifa->ifa_addr;
                getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in6), addr,
                            sizeof(addr), NULL, 0, NI_NUMERICHOST);
                printf("Interface: %s\tAddress: %s\n", ifa->ifa_name, addr);
            }
        }

        freeifaddrs(ifap);*/

        //

        interface->isUP = (interfaceInfo.up == SG_IFACE_UP);

        sg_network_io_stats &stat = network_stats[i];

        interface->tx = stat.tx;
        interface->rx = stat.rx;

        interface->inputPackets = stat.ipackets;
        interface->inputErrors = stat.ierrors;
        interface->outputPackets = stat.opackets;
        interface->outputErrors = stat.oerrors;
        interface->collisions = stat.collisions;
    }

    return true;
}

bool SkProcFs::grabFsInfo()
{
    size_t c = 0;
    sg_disk_io_stats *diskio_stats = sg_get_disk_io_stats_diff(&c);

    if (!diskio_stats)
    {
        FlatError("Cannot get Disks");
        c = 0;
        return false;
    }

    if (c == 0)
    {
        FlatError("Cannot get Disks (count == 0)");
        return false;
    }

    /*if (sorted)
        qsort(diskio_stats, c, sizeof(diskio_stats[0]), sg_disk_io_compare_traffic);*/

    for(uint64_t i=0; i<c; i++)
    {
        sg_disk_io_stats &diskio = diskio_stats[i];
        SkFsInfo *fs = nullptr;

        if (fileSystems.contains(diskio.disk_name))
            fs = fileSystems[diskio.disk_name];

        else
        {
            fs = new SkFsInfo;
            fs->name = diskio.disk_name;
            fileSystems[diskio.disk_name] = fs;
        }

        fs->tx = diskio.write_bytes;
        fs->rx = diskio.read_bytes;
    }

    return true;
}

bool SkProcFs::read(CStr *procFilePath, SkString &content)
{
    content.clear();

    if (!SkFsUtils::isReadable(procFilePath) || !SkFsUtils::isFile(procFilePath))
    {
        FlatWarning("CANNOT read file: " << procFilePath);
        return false;
    }

    FILE *file = ::fopen(procFilePath, "r");

    if (!file)
    {
        FlatWarning("CANNOT open file: " << procFilePath);
        return false;
    }

    ULong len = 50*1024*1024;
    char *data = new char [len+1];
    memset(data, '\0', len+1);

    //atomic read
    len = ::fread(data, sizeof(char), len, file);
    ::fclose(file);

    if (len)
    {
        content.append(data, len);
        delete [] data;
        return true;
    }

    FlatWarning("File is EMPTY: " << procFilePath);
    delete [] data;
    return false;
}

bool SkProcFs::supportedFileSystems(SkStringList &fsTypes)
{
    size_t count = 0;
    CStr **valid_fs = sg_get_valid_filesystems(&count);

    if (count == 0)
    {
        FlatError("Cannot get supported-fs (count == 0)");
        return false;
    }

    fsTypes.clear();

    for(uint64_t i=0; i<count; i++)
        fsTypes.append(valid_fs[i]);

    return true;
}


SkHostInfo *SkProcFs::hostInfo()
{
    return &hInfo;
}

SkSysStat *SkProcFs::systemStat()
{
    return &sStat;
}

SkProcessesStat *SkProcFs::processesStat()
{
    return &pStat;
}

SkMemoryStat *SkProcFs::memoryStat()
{
    return &mStat;
}

SkSwapStat *SkProcFs::swapStat()
{
    return &swStat;
}

/*SkProcessInfo *SkProcFs::processInfo(CStr *name)
{
    if (processes.contains(name))
        return processes[name];

    return nullptr;
}*/

void SkProcFs::networkInterfacesNames(SkStringList &names)
{
    networkInterfaces.keys(names);
}

void SkProcFs::fileSystemsNames(SkStringList &names)
{
    fileSystems.keys(names);
}

SkNetworkInterfaceInfo *SkProcFs::networkInterfaceInfo(CStr *name)
{
    if (networkInterfaces.contains(name))
        return networkInterfaces[name];

    return nullptr;
}

SkFsInfo *SkProcFs::fileSystemInfo(CStr *name)
{
    if (fileSystems.contains(name))
        return fileSystems[name];

    return nullptr;
}

/*SkBinaryTreeVisit<SkString, SkProcessInfo *> *SkProcFs::processesIterator()
{
    return processes.iterator();
}*/

SkBinaryTreeVisit<SkString, SkNetworkInterfaceInfo *> *SkProcFs::networkInterfacesIterator()
{
    return networkInterfaces.iterator();
}

SkBinaryTreeVisit<SkString, SkFsInfo *> *SkProcFs::fileSystemsIterator()
{
    return fileSystems.iterator();
}

#endif
