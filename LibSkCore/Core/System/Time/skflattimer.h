/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKFLATTIMER_H
#define SKFLATTIMER_H

#include "Core/Object/skobject.h"
#include "Core/System/Time/skelapsedtime.h"

//FLAT PRECISION DEPENDS ON EVENTLOOP FASTTICK INTERVAL ("PULSE" signal)

class SkFlatTimer extends SkObject
{
    public:
        Constructor(SkFlatTimer, SkObject);

        //DEFAULT IS ONE SECOND
        void setInterval(double secs);
        double getInterval();

        bool isActive();

        Signal(started);
        Signal(stopped);

        Slot(start);
        Slot(stop);
        Slot(check);

    protected:
        double interval;

        virtual void onStart(){}
        virtual void timeout(){pulse();}
        virtual void onStop(){}

    private:
        bool enabled;
        SkElapsedTime elapsedTime;
};

#endif // SKFLATTIMER_H
