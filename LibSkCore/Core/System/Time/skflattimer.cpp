#include "skflattimer.h"
#include "Core/App/skeventloop.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkFlatTimer, setInterval, Arg_Double)
DeclareMeth_INSTANCE_RET(SkFlatTimer, getInterval, double)
DeclareMeth_INSTANCE_RET(SkFlatTimer, isActive, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkFlatTimer, SkObject,
                {
                    AddMeth_INSTANCE_VOID(SkFlatTimer, setInterval);
                    AddMeth_INSTANCE_RET(SkFlatTimer, getInterval);
                    AddMeth_INSTANCE_RET(SkFlatTimer, isActive);
                })
{
    enabled = false;
    interval = 1;

    SignalSet(started);
    SignalSet(stopped);

    SlotSet(start);
    SlotSet(stop);
    SlotSet(check);
}

void SkFlatTimer::setInterval(double secs)
{
    interval = secs;
}

double SkFlatTimer::getInterval()
{
    return interval;
}

SlotImpl(SkFlatTimer, start)
{
    SilentSlotArgsWarning();

    if (enabled)
    {
        ObjectWarning("FlatTimer is ALREADY started");
        return;
    }

    enabled = true;
    elapsedTime.start();
    Attach(eventLoop()->fastZone_SIG, pulse, this, check, SkAttachMode::SkQueued);
    started();
    onStart();
}

bool SkFlatTimer::isActive()
{
    return enabled;
}

SlotImpl(SkFlatTimer, stop)
{
    SilentSlotArgsWarning();

    if (!enabled)
    {
        ObjectWarning("FlatTimer is ALREADY stopped");
        return;
    }

    enabled = false;
    Detach(eventLoop()->fastZone_SIG, pulse, this, check);
    stopped();
    onStop();
}

SlotImpl(SkFlatTimer, check)
{
    SilentSlotArgsWarning();

    if (elapsedTime.stop() >= interval)
    {
        timeout();
        elapsedTime.start();
    }
}
