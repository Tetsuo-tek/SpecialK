﻿/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

//This is an approximative life ... then, this is an approximative code!

#include "skdefines.h"

#ifndef SKFILE_H
#define SKFILE_H

/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

/**
 * @file skfile.h
 */

#include "Core/Containers/skstring.h"
#include "Core/System/skabstractdevice.h"
#include "Core/Containers/skdatabuffer.h"
#include "Core/System/Filesystem/skflatfile.h"

class SPECIALK SkFile extends SkAbstractDevice
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkFile, SkAbstractDevice);

        /**
         * @brief Sets the file path to open
         * @param path the path cstring
         */
        void setFilePath(CStr *path);

        /**
         * @brief Sets the file path to open
         * @param path the path string
         */
        void setFilePath(SkString &path);

        /**
         * @brief Gets the file path
         * @return return the path cstring
         */
        CStr *getFilePath();

        /**
         * @brief Opens the file, only after the file path is configured
         * @param m the mode to open the file; if file is not open all supported
         * operations will fail with errors
         * @return true if there are not errors, otherwise false
         */
        bool open(SkFileMode m=SkFileMode::FLM_READWRITE);

        /**
         * @brief Gets the cursor position of the file
         * @return current position if it is valid, otherwise -1
         */
        int64_t pos() override;

        /**
         * @brief Reset the position to 0 on the file
         * @return true if there are not errors, otherwise false
         */
        bool rewind() override;

        /**
         * @brief Seeks a position on the file
         * @param position the position to seek
         * @return true if the position is valid, otherwise false
         */
        bool seek(uint64_t position) override;

        /**
         * @brief Checks if the position is at the end of the file
         * @return true if the position is at the end and there are not errors,
         * otherwise false
         */
        bool atEof() override;

        /**
         * @brief Synchronizes the filesystem
         * @return true if there are not errors, otherwise false
         */
        bool synchronize();

        //void mapToMemory();
        //void unmapFromMemory();

        /**
         * @brief Checks if the implemented device is squencial (not seekable)
         * @return false because a file is not sequencial and always seekable
         */
        bool isSequencial() override {return false;}

        /**
         * @brief Checks if it is possible to read a line, terminating with '\n'
         * @param lineLength the value reference that will be filled with the
         * line size
         * @return true if there is a line available
         */
        bool canReadLine(uint64_t &lineLength) override;

        /**
         * @brief Flushes input/output of the file
         * @return true if there are not errors
         */
        bool flush() override;

        /**
         * @brief Gets the size of the file
         * @warning it could require to flush and/or synchronize data before
         * @return the size if it is valid, otherwise -1
         */
        int64_t size() override;//COULD REQUIRE TO FLUSH BEFORE

        /**
         * @brief Gets available data to read from the file, calculating from
         * current position to the end
         * @return the available size to read
         */
        uint64_t bytesAvailable() override;//REMAINING DATA FROM pos() TO size()

        /**
         * @brief Closes the device
         */
        void close() override;

        /**
         * @brief Gets the current file mode
         * @return the current mode value
         */
        SkFileMode currentMode();

        /**
         * @brief Gets the current file mode as string
         * @return the current mode value as string
         */
        CStr *getCurrentModeString();

        //IF IT IS OPEN IT WILL CLOSE BEFORE TO REMOVE
        /**
         * @brief Removes the file; if it is open the file will be closed before
         * to remove it
         * @return true if there are not errors
         */
        bool remove();

    private:
        FILE *file;
        SkString filePath;
        SkFileMode openMode;

    protected:
        bool readInternal(char *data, uint64_t &len, bool onlyPeek=false) override;
        bool writeInternal(CStr *data, uint64_t &len) override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif // SKFILE_H
