/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTEMPFILE_H
#define SKTEMPFILE_H

#include "skfile.h"

class SPECIALK SkTempFile extends SkFile
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkTempFile, SkFile);

        //BY DEFAULT THIS FILE LIVES INSIDE SYSTEM-TEMP (ex.: "/tmp/" on linux)
        //BUT YOU COULD CHANGE THE FILE PATH ASSIGNING A CUSTOM PATH NAME USING:
        //void SkFile::setFilePath(CStr *path);
        //void SkFile::setFilePath(SkString &path);

        //AUTO REMOVING THE FILE WHEN THE OBJECT IS DESTROYED
        /**
         * @brief Enables to remove the file when the instance of this object
         * is destroyed
         * @param enabled if true the file will be removed on object destruction
         */
        void setAutoRemove(bool enabled);

        /**
         * @brief Checks if the autoRemove flag is configured
         * @return the value of the autoremoveFlag
         */
        bool autoRemove();

    private:
        bool autoremoveOnExit;
};

#endif // SKTEMPFILE_H
