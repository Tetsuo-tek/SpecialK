/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKFILEINFOSLIST_H
#define SKFILEINFOSLIST_H

#include "sktempfile.h"
#include "Core/System/Filesystem/skfsutils.h"

class SPECIALK SkFileInfosList extends SkTempFile
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkFileInfosList, SkTempFile);

        /**
         * @brief Opens the file info temporary file
         * @return true if there are not errors, otherwise false
         */
        bool open();

        /**
         * @brief Appends a new path
         * @param path the cstring for the path to add
         * @param isDir true if the path is a directory, otherwise false
         * @return true if there are not errors, otherwise false
         */
        bool append(CStr *path, bool isDir);

        /**
         * @brief Gets the number of stored paths
         * @return the count of paths
         */
        uint64_t count();

        /**
         * @brief Gets the size for internal indexes vector
         * @return the size of the indexes vector
         */
        uint64_t indexesRepositorySize();

        /**
         * @brief Checks if this temporary file container is empty
         * @return true if it is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Seeks on a specified indexed position
         * @param index the index for a path corresponding to a position on the
         * temporary file
         * @return true if the index is valid and there are not errors, otherwise false
         */
        bool select(uint64_t index);

        /**
         * @brief Moves to the next indexed position
         * @return true if there is a next position and there are not errors,
         * otherwise false
         */
        bool next();

        /**
         * @brief Gets the string reference of the current selected path
         * @return the string reference for the current path
         */
        SkString &currentPath();

        /**
         * @brief Checks if the current path is a directory
         * @return true if there the current path is a directory, otherwise false
         */
        bool currentPathIsDir();

    private:
        //RAM-INDEX-CONTAINER FOR ITEM-POSITIONs WROTE ON FILE
        SkVector<uint64_t> indexesRepository;

        bool currentIsDir;
        SkString current;
};

#endif // SKFILEINFOSLIST_H
