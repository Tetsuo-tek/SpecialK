#include "skfileinfoslist.h"

DeclareMeth_INSTANCE_RET(SkFileInfosList, open, bool)
DeclareMeth_INSTANCE_RET(SkFileInfosList, append, bool, Arg_CStr, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkFileInfosList, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkFileInfosList, indexesRepositorySize, uint64_t)
DeclareMeth_INSTANCE_RET(SkFileInfosList, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkFileInfosList, select, bool, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkFileInfosList, next, bool)
DeclareMeth_INSTANCE_RET(SkFileInfosList, currentPath, SkString &)
DeclareMeth_INSTANCE_RET(SkFileInfosList, currentPathIsDir, bool)

ConstructorImpl(SkFileInfosList, SkTempFile,
                {
                    AddMeth_INSTANCE_RET(SkFileInfosList, open);
                    AddMeth_INSTANCE_RET(SkFileInfosList, append);
                    AddMeth_INSTANCE_RET(SkFileInfosList, count);
                    AddMeth_INSTANCE_RET(SkFileInfosList, indexesRepositorySize);
                    AddMeth_INSTANCE_RET(SkFileInfosList, isEmpty);
                    AddMeth_INSTANCE_RET(SkFileInfosList, select);
                    AddMeth_INSTANCE_RET(SkFileInfosList, next);
                    AddMeth_INSTANCE_RET(SkFileInfosList, currentPath);
                    AddMeth_INSTANCE_RET(SkFileInfosList, currentPathIsDir);
                })
{
    currentIsDir = false;
}

bool SkFileInfosList::open()
{
    return SkFile::open(SkFileMode::FLM_ONLYREAD);
}

bool SkFileInfosList::append(CStr *path, bool isDir)
{
    if (currentMode() != SkFileMode::FLM_ONLYWRITE)
        return false;

    indexesRepository.append(pos());

    uint64_t strSz = strlen(path);
    writeUInt8(isDir);
    writeUInt64(strSz);
    //writeSizeT(strSz);
    write(path, strSz);

    return true;
}

uint64_t SkFileInfosList::count()
{
    return indexesRepository.count();
}

uint64_t SkFileInfosList::indexesRepositorySize()
{
    return indexesRepository.count() * sizeof(uint64_t);
}

bool SkFileInfosList::isEmpty()
{
    return indexesRepository.isEmpty();
}

bool SkFileInfosList::select(uint64_t index)
{
    if (index >= indexesRepository.count())
        return false;

    seek(indexesRepository.at(index));
    return next();
}

bool SkFileInfosList::next()
{
    /*if (currentMode() != SkFileMode::FLM_ONLYREAD)
        return false;*/

    if (pos() == size())
        return false;

    currentIsDir = readUInt8();
    uint64_t strSz = readUInt64();
    //uint64_t strSz = readSizeT();

    char *path = new char [strSz+1];

    if (!read(path, strSz))
    {
        ObjectError("Cannot READ from fileInfoList: [pos: " << pos() << "/" << size() << " B]");
        current.clear();
        delete [] path;
        return false;
    }

    path[strSz] = '\0';
    current = path;
    delete [] path;
    return true;
}

SkString &SkFileInfosList::currentPath()
{
    return current;
}

bool SkFileInfosList::currentPathIsDir()
{
    return currentIsDir;
}
