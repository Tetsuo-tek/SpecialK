#include "skfile.h"
#include "Core/System/Filesystem/skfsutils.h"
#include <unistd.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

// // // // // // // // // // // // // // // // // // // // //

void SkFileClose(SkObject *obj)
{
    SkFile *f = static_cast<SkFile *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkFileClose()");

    if (f->isOpen())
        f->close();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkFileMode)
{
    SetupEnumWrapper(SkFileMode);

    SetEnumItem(SkFileMode::FLM_NOMODE);
    SetEnumItem(SkFileMode::FLM_ONLYREAD);
    SetEnumItem(SkFileMode::FLM_ONLYWRITE);
    SetEnumItem(SkFileMode::FLM_ONLYWRITE_APPENDING);
    SetEnumItem(SkFileMode::FLM_READWRITE);
    SetEnumItem(SkFileMode::FLM_READWRITE_APPENDING);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkFile, setFilePath, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkFile, getFilePath, CStr *)
DeclareMeth_INSTANCE_RET(SkFile, open, bool, Arg_Enum(SkFileMode))
DeclareMeth_INSTANCE_RET(SkFile, pos, int64_t)
DeclareMeth_INSTANCE_RET(SkFile, rewind, bool)
DeclareMeth_INSTANCE_RET(SkFile, seek, bool, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkFile, atEof, bool)
DeclareMeth_INSTANCE_RET(SkFile, synchronize, bool)
DeclareMeth_INSTANCE_RET(SkFile, isSequencial, bool)
//DeclareMeth_INSTANCE_RET(SkFile, canReadLine, bool,  Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkFile, flush, bool)
DeclareMeth_INSTANCE_RET(SkFile, size, int64_t)
DeclareMeth_INSTANCE_RET(SkFile, bytesAvailable, uint64_t)
DeclareMeth_INSTANCE_VOID(SkFile, close)
DeclareMeth_INSTANCE_RET(SkFile, currentMode, SkFileMode)
DeclareMeth_INSTANCE_RET(SkFile, getCurrentModeString, CStr *)
DeclareMeth_INSTANCE_RET(SkFile, remove, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkFile, SkAbstractDevice,
                {
                    AddEnumType(SkFileMode);
                    AddMeth_INSTANCE_VOID(SkFile, setFilePath);
                    AddMeth_INSTANCE_RET(SkFile, getFilePath);
                    AddMeth_INSTANCE_RET(SkFile, open);
                    AddMeth_INSTANCE_RET(SkFile, pos);
                    AddMeth_INSTANCE_RET(SkFile, rewind);
                    AddMeth_INSTANCE_RET(SkFile, seek);
                    AddMeth_INSTANCE_RET(SkFile, atEof);
                    AddMeth_INSTANCE_RET(SkFile, synchronize);
                    AddMeth_INSTANCE_RET(SkFile, isSequencial);
                    //AddMeth_INSTANCE_RET(SkFile, canReadLine);
                    AddMeth_INSTANCE_RET(SkFile, flush);
                    AddMeth_INSTANCE_RET(SkFile, size);
                    AddMeth_INSTANCE_RET(SkFile, bytesAvailable);
                    AddMeth_INSTANCE_VOID(SkFile, close);
                    AddMeth_INSTANCE_RET(SkFile, currentMode);
                    AddMeth_INSTANCE_RET(SkFile, getCurrentModeString);
                    AddMeth_INSTANCE_RET(SkFile, remove);
                })
{
    file = nullptr;
    openMode = SkFileMode::FLM_NOMODE;

    addDtorCompanion(SkFileClose);
}

void SkFile::setFilePath(CStr *path)
{
    filePath = path;
    setObjectName(filePath.c_str());
}

void SkFile::setFilePath(SkString &path)
{
    filePath = path;
    setObjectName(filePath.c_str());
}

CStr *SkFile::getFilePath()
{
    return filePath.c_str();
}

bool SkFile::open(SkFileMode m)
{
    if (isOpen())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (filePath.isEmpty())
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__,
                    "Cannot open file device with an EMPTY name");
        return false;
    }

    if (m == SkFileMode::FLM_ONLYREAD)
    {
        if (!SkFsUtils::exists(filePath.c_str()))
        {
            notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__,
                        "File NOT found");

            return false;
        }

        file = ::fopen(filePath.c_str(), "r");
    }

    else if (m == SkFileMode::FLM_ONLYWRITE)
        file = ::fopen(filePath.c_str(), "w");

    else if (m == SkFileMode::FLM_ONLYWRITE_APPENDING)
        file = ::fopen(filePath.c_str(), "a");

    else if (m == SkFileMode::FLM_READWRITE)
        file = ::fopen(filePath.c_str(), "r+");

    else if (m == SkFileMode::FLM_READWRITE_APPENDING)
        file = ::fopen(filePath.c_str(), "a+");

    if (file)
    {
        openMode = m;
        ObjectDebug("File device is OPEN [mode: " << getCurrentModeString() << "]");
        notifyState(SkDeviceState::Open);
        return true;
    }

    notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
    file = nullptr;

    return false;
}

int64_t SkFile::pos()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return -1;
    }

    int64_t p = ::ftell(file);

    if (p < 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__, "Cannot get file position");
        p = 0;
    }

    return p;

}

bool SkFile::rewind()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return seek(0);
}

bool SkFile::seek(uint64_t position)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (::fseek(file, static_cast<int64_t>(position), SEEK_SET) != 0)
    {
        notifyError(SkDeviceError::CannotSeekDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return true;
}

bool SkFile::atEof()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return true;
    }

    return (bytesAvailable()==0);//(::feof(file) != 0);
}

bool SkFile::flush()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (::fflush(file) != 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__, "Cannot flush file");
        return false;
    }

    return synchronize();
}

bool SkFile::synchronize()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (::fsync(fileno(file)) != 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__, "Cannot synchronize file");
        return false;
    }

    return true;
}

bool SkFile::canReadLine(uint64_t &lineLength)
{
    lineLength = 0;
    int64_t currentPos = pos();

    if (currentPos == -1)
        return false;

    while(!atEof())
    {
        lineLength++;

        if (::fgetc(file) == '\n' || atEof())
            return seek(static_cast<uint64_t>(currentPos));
    }

    lineLength = 0;
    return false;
}

bool SkFile::readInternal(char *data, uint64_t &len, bool onlyPeek)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    /*if (len > bytesAvailable())
        len = bytesAvailable();*/

    len = ::fread(data, sizeof(char), len, file);

    if (onlyPeek)
    {
        int64_t currentPos = this->pos();
        currentPos -= len;
        this->seek(static_cast<uint64_t>(currentPos));
    }

    if (len > 0)
        notifyRead(len);

    bool error = (::ferror(file) != 0);

    if (error)
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);

    return !error;//(len > 0);
}

bool SkFile::writeInternal(CStr *data, uint64_t &len)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t sent = ::fwrite(data, sizeof(char), len, file);

    if (sent > 0)
    {
        len = sent;
        notifyWrite(len);
    }

    bool error = (::ferror(file) != 0);

    if (error)
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);

    return !error;
}

void SkFile::close()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    flush();
    synchronize();
    ::fclose(file);
    file = nullptr;

    openMode = SkFileMode::FLM_NOMODE;

    ObjectDebug("File device is CLOSED");
    notifyState(SkDeviceState::Closed);
}

int64_t SkFile::size()
{
    if (filePath.isEmpty())
        return -1;

    if (isOpen())
    {
        flush();
        synchronize();
    }

    return SkFsUtils::size(filePath.c_str());
}

uint64_t SkFile::bytesAvailable()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    flush();
    synchronize();

    int64_t sz = size();
    int64_t p = pos();

    if (sz <= 0 || p < 0)
        return 0;

    return static_cast<uint64_t>(sz - pos());
}

SkFileMode SkFile::currentMode()
{
    return openMode;
}

CStr *SkFile::getCurrentModeString()
{
    if (openMode == SkFileMode::FLM_ONLYREAD)
        return "OnlyRead";

    else if (openMode == SkFileMode::FLM_ONLYWRITE)
        return "OnlyWrite";

    else if (openMode == SkFileMode::FLM_ONLYWRITE_APPENDING)
        return "OnlyWriteAppending";

    else if (openMode == SkFileMode::FLM_READWRITE)
        return "ReadWrite";

    else if (openMode == SkFileMode::FLM_READWRITE_APPENDING)
        return "ReadWriteAppending";

    return "NoMode";
}

bool SkFile::remove()
{
    if (filePath.isEmpty())
        return false;

    if (isOpen())
        this->close();

    return SkFsUtils::rm(filePath.c_str());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
