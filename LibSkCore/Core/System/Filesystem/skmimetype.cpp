#include "skmimetype.h"
#include "Core/App/skapp.h"

/*SkMimeType::SkMimeType()
{
}*/

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STATIC(SkMimeType);

DeclareMeth_STATIC_RET(SkMimeType, getMimeType, CStr*, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isSimpleText, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isTextDocument, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isImage, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isAudio, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isVideo, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkMimeType, isGenericBinary, bool, Arg_CStr)

// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkMimeType)
{
    AddMeth_STATIC_RET(SkMimeType, getMimeType);
    AddMeth_STATIC_RET(SkMimeType, isSimpleText);
    AddMeth_STATIC_RET(SkMimeType, isTextDocument);
    AddMeth_STATIC_RET(SkMimeType, isImage);
    AddMeth_STATIC_RET(SkMimeType, isAudio);
    AddMeth_STATIC_RET(SkMimeType, isVideo);
    AddMeth_STATIC_RET(SkMimeType, isGenericBinary);
}

// // // // // // // // // // // // // // // // // // // // //

CStr *SkMimeType::getMimeType(CStr *ext)
{
    SkString extStr(ext);
    extStr.toLowerCase();

    SkLinkedMap<SkString, SkString> *knownMimeTypes = skApp->getMimeTypes();
    SkString &mime = knownMimeTypes->value(extStr);

    if (!mime.isEmpty())
        return mime.c_str();

    return "application/octet-stream";
}

bool SkMimeType::isSimpleText(CStr *ext)
{
    return (strcmp(ext, "txt") == 0
            || strcmp(ext, "csv") == 0
            || strcmp(ext, "js") == 0
            || strcmp(ext, "json") == 0);
}

bool SkMimeType::isTextDocument(CStr *ext)
{
    return (strcmp(ext, "pdf") == 0
            || strcmp(ext, "html") == 0
            || strcmp(ext, "htm") == 0
            || strcmp(ext, "xml") == 0
            || strcmp(ext, "rtf") == 0);
}

bool SkMimeType::isImage(CStr *ext)
{
    return (strcmp(ext, "ico") == 0
            || strcmp(ext, "gif") == 0
            || strcmp(ext, "jpeg") == 0
            || strcmp(ext, "jpg") == 0
            || strcmp(ext, "jfif") == 0
            || strcmp(ext, "pjpeg") == 0
            || strcmp(ext, "png") == 0
            || strcmp(ext, "svg") == 0
            || strcmp(ext, "tiff") == 0
            || strcmp(ext, "tif") == 0);
}

bool SkMimeType::isAudio(CStr *ext)
{
    return (strcmp(ext, "wav") == 0
            || strcmp(ext, "aiff") == 0
            || strcmp(ext, "mka") == 0
            || strcmp(ext, "mp2") == 0
            || strcmp(ext, "mp3") == 0
            || strcmp(ext, "oga") == 0
            || strcmp(ext, "opus") == 0
            || strcmp(ext, "wma") == 0);
}

bool SkMimeType::isVideo(CStr *ext)
{
    return (strcmp(ext, "avi") == 0
            || strcmp(ext, "mov") == 0
            || strcmp(ext, "mp4") == 0
            || strcmp(ext, "mpeg") == 0
            || strcmp(ext, "mkv") == 0
            || strcmp(ext, "ogv") == 0
            || strcmp(ext, "opus") == 0
            || strcmp(ext, "flv") == 0
            || strcmp(ext, "wmv") == 0);
}

bool SkMimeType::isGenericBinary(CStr *ext)
{
    return (strcmp(SkMimeType::getMimeType(ext), "application/octet-stream"));
}
