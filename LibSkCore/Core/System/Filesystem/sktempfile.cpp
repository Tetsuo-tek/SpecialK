#include "sktempfile.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/skosenv.h"

void SkTempFileRemove(SkObject *obj)
{
    SkTempFile *f = static_cast<SkTempFile *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkTempFileRemove()");

    if (f->autoRemove() && SkFsUtils::exists(f->getFilePath()))
        SkFsUtils::rm(f->getFilePath());
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkTempFile, setAutoRemove, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkTempFile, autoRemove, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkTempFile, SkFile,
                {
                    AddMeth_INSTANCE_VOID(SkTempFile, setAutoRemove);
                    AddMeth_INSTANCE_RET(SkTempFile, autoRemove);
                })
{    
    autoremoveOnExit = true;

    addDtorCompanion(SkTempFileRemove);

    SkString p = SkFsUtils::adjustPathEndSeparator(osEnv()->getSystemTempPath().c_str());
    p.concat(osEnv()->getExecutableName());
    p.concat("_");
    p.concat(uuid());
    p.concat(".tmp");

    setFilePath(p);
}

void SkTempFile::setAutoRemove(bool enabled)
{
    autoremoveOnExit = enabled;
}

bool SkTempFile::autoRemove()
{
    return autoremoveOnExit;
}

