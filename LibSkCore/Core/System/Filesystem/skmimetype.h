/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKMIMETYPE_H
#define SKMIMETYPE_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"

//IT IS POOR! ... MUST ADD KILOTONS OF MIMEs
//TYPES ARE STORED INSIDE "Core/App/skappcostants.cpp"

class SkMimeType extends SkFlatObject
{
    public:
        //SkMimeType();

        /**
         * @brief Gets a mime type from a file extension
         * @param ext the file extension name
         * @return the mime type cstring
         */
        static CStr *getMimeType(CStr *ext);

        /**
         * @brief Checks if the file extension means simple text
         * @param ext the file extension name
         * @return true if it is simple text
         */
        static bool isSimpleText(CStr *ext);

        /**
         * @brief Checks if the file extension means formatted text
         * @param ext the file extension name
         * @return true if it is formatted text
         */
        static bool isTextDocument(CStr *ext);

        /**
         * @brief Checks if the file extension means an image file
         * @param ext the file extension name
         * @return true if it is an image file
         */
        static bool isImage(CStr *ext);

        /**
         * @brief Checks if the file extension means an audio file
         * @param ext the file extension name
         * @return true if it is an audio file
         */
        static bool isAudio(CStr *ext);

        /**
         * @brief Checks if the file extension means an video file
         * @param ext the file extension name
         * @return true if it is an video file
         */
        static bool isVideo(CStr *ext);

        /**
         * @brief Checks if the file extension is recognized as "application/octet-stream"
         * @param ext the file extension name
         * @return true if it is "application/octet-stream"
         */
        static bool isGenericBinary(CStr *ext);
};

#endif // SKMIMETYPE_H
