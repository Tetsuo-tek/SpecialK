#include "skdeviceredistr.h"
#include "Core/Containers/skringbuffer.h"
#include "Core/System/Network/skabstractsocket.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkDeviceRedistr, setHeader, Arg_CStr, Arg_UInt64)
//DeclareMeth_INSTANCE_RET(SkDeviceRedistr, getHeader, SkDataBuffer&)
DeclareMeth_INSTANCE_VOID(SkDeviceRedistr, addDevice, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_RET(SkDeviceRedistr, existsDevice, bool, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_VOID(SkDeviceRedistr, sendToAll, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDeviceRedistr, sendToAll, 1, Arg_Custom(SkRingBuffer))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDeviceRedistr, sendToAll, 2, Arg_Custom(SkRingBuffer), Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkDeviceRedistr, delDevice, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_RET(SkDeviceRedistr, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkDeviceRedistr, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkDeviceRedistr, getDevice, SkAbstractDevice*, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkDeviceRedistr, clear)


// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkDeviceRedistr, SkObject,
                {
                    AddMeth_INSTANCE_VOID(SkDeviceRedistr, setHeader);
                    //AddMeth_INSTANCE_RET(SkDeviceRedistr, getHeader);
                    AddMeth_INSTANCE_VOID(SkDeviceRedistr, addDevice);
                    AddMeth_INSTANCE_RET(SkDeviceRedistr, existsDevice);
                    AddMeth_INSTANCE_VOID(SkDeviceRedistr, sendToAll);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkDeviceRedistr, sendToAll, 1);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkDeviceRedistr, sendToAll, 2);
                    AddMeth_INSTANCE_VOID(SkDeviceRedistr, delDevice);
                    AddMeth_INSTANCE_RET(SkDeviceRedistr, isEmpty);
                    AddMeth_INSTANCE_RET(SkDeviceRedistr, isEmpty);
                    AddMeth_INSTANCE_RET(SkDeviceRedistr, count);
                    AddMeth_INSTANCE_VOID(SkDeviceRedistr, clear);
                })
{
    SignalSet(targetAdded);
    SignalSet(targetDeleted);
}

void SkDeviceRedistr::setHeader(CStr *data, uint64_t size)
{
    header.setData(data, size, false);
}

SkDataBuffer &SkDeviceRedistr::getHeader()
{
    return header;
}

void SkDeviceRedistr::addDevice(SkAbstractDevice *target)
{
    if (clients.contains(target))
    {
        ObjectError("Device ALREADY exists: " << target->objectName());
        return;
    }

    onAddDevice(target);

    if (!header.isEmpty())
        target->write(header.data(), header.size());

    clients.append(target);

    SkVariantVector p;
    p << clients.count()-1;
    targetAdded(this, p);

    ObjectDebug("Added new device: " << target->objectName());
}

bool SkDeviceRedistr::existsDevice(SkAbstractDevice *target)
{
    return clients.contains(target);
}

void SkDeviceRedistr::sendToAll(CStr *data, uint64_t size)
{
    if (isEmpty())
        return;

    for(uint64_t i=0; i<clients.count(); i++)
    {
        SkAbstractDevice *dev = clients.at(i);

        if (!dev->canWrite())
        {
            ObjectWarning("CANNOT write data [" << size << " B] on target: " << dev->objectName());
            continue;
        }

        //dev->write(data, size);

        //MUST CHECK
        uint64_t blck = 200000;

        if (size < blck)
            dev->write(data, size);

        else
        {
            uint64_t z = 0;

            while (z < size)
            {
                int currentSize = (size - z < blck) ? (size - z) : blck;
                dev->write(&data[z], currentSize);
                z += currentSize;
            }
        }
    }
}

void SkDeviceRedistr::sendToAll(SkRingBuffer *buf, uint64_t size)
{
    if (buf->isEmpty())
    {
        ObjectError("Redistr of an empty content from RingBuffer");
        return;
    }

    if (size == 0)
        size = buf->size();

    char *d = new char [size];
    buf->getCustomBuffer(d, size);
    sendToAll(d, size);
    delete [] d;
}

void SkDeviceRedistr::sendToAll(SkDataBuffer *buf, uint64_t size)
{
    if (buf->isEmpty())
    {
        ObjectError("Redistr of an empty content from DataBuffer");
        return;
    }

    if (size == 0)
        size = buf->size();

    sendToAll(buf->data(), size);
}

void SkDeviceRedistr::delDevice(SkAbstractDevice *target)
{
    if (!clients.contains(target))
    {
        ObjectError("Device does NOT exists: " << target->objectName());
        return;
    }

    onDelDevice(target);

    SkVariantVector p;
    p << clients.indexOf(target);
    targetDeleted(this, p);

    clients.remove(target);

    ObjectDebug("Removed device: " << target->objectName());
}

void SkDeviceRedistr::delDevice(uint64_t index)
{
    if (index >= clients.count())
    {
        ObjectError("Device index NOT valid: " << index);
        return;
    }

    SkAbstractDevice *target = clients[index];
    onDelDevice(target);

    SkVariantVector p;
    p << index;
    targetDeleted(this, p);

    clients.remove(target);

    ObjectDebug("Removed device: " << target->objectName());
}

bool SkDeviceRedistr::isEmpty()
{
    return clients.isEmpty();
}

uint64_t SkDeviceRedistr::count()
{
    return clients.count();
}

SkAbstractDevice *SkDeviceRedistr::getDevice(uint64_t index)
{
    if (index >= clients.count())
        return nullptr;

    return clients.at(index);
}

void SkDeviceRedistr::clear()
{
    for(uint64_t i=0; i<clients.count(); i++)
    {
        SkAbstractDevice *target = clients[i];
        onDelDevice(target);
        clients.remove(target);
        targetDeleted(target);
    }

    clients.clear();
}
