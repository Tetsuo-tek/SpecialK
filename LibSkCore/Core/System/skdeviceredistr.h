/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKDEVICEREDISTR_H
#define SKDEVICEREDISTR_H

#include "skabstractdevice.h"
#include "Core/Containers/skdatabuffer.h"
#include "Core/Containers/skargsmap.h"

class SPECIALK SkDeviceRedistr extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkDeviceRedistr, SkObject);

        /**
         * @brief Sets header data that new client will receive on connection
         * @param data the array data pointer to use as textual or binary header
         * @param size the size of data
         */
        void setHeader(CStr *data, uint64_t size);

        /**
         * @brief Gets the data-buffer containing the header
         * @return the internal data-buffer reference containing the header
         */
        SkDataBuffer &getHeader();

        /**
         * @brief Adds a device to pool
         * @param target the device to add
         */
        void addDevice(SkAbstractDevice *target);

        /**
         * @brief Checks if a device exists
         * @param target the device to check existance
         * @return true if it exists, otherwise false
         */
        bool existsDevice(SkAbstractDevice *target);

        /**
         * @brief Sends an array data pointer to pool
         * @warning if a device is an SkAbstractSocket derivate, and it is NOT ready for write, it will NOT receive data in the selected buffer
         * @param data the array data pointer to send
         * @param size the size of the data
         */
        virtual void sendToAll(CStr *data, uint64_t size);

        /**
         * @brief Sends a ring-buffer content to pool
         * @warning if a device is an SkAbstractSocket derivate, and it is NOT ready for write, it will NOT receive data in the selected buffer
         * @param buf the ring-buffer pointer which to send data
         * @param size the size of the data to send; if it is 0 all datain the
         * ring-buffer will be sent
         */
        void sendToAll(SkRingBuffer *buf, uint64_t size=0);

        /**
         * @brief Sends a data-buffer content to pool
         * @warning if a device is an SkAbstractSocket derivate, and it is NOT ready for write, it will NOT receive data in the selected buffer
         * @param buf the data-buffer pointer which to send data
         * @param size the size of the data to send; if it is 0 all datain the
         * ring-buffer will be sent
         */
        void sendToAll(SkDataBuffer *buf, uint64_t size=0);

        /**
         * @brief Removes a device from pool
         * @warning it will NOT be destroyed or disconnected
         * @param target the device to remove
         */
        void delDevice(SkAbstractDevice *target);
        void delDevice(uint64_t index);

        /**
         * @brief Checks if the pool is empty
         * @return true if it is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Gets the count number of the devices stored in the pool
         * @return the count number value
         */
        uint64_t count();

        /**
         * @brief Gets a device starting from its index in the pool
         * @param index the index of the device to get
         * @return the device pointer object
         */
        SkAbstractDevice *getDevice(uint64_t index);

        /**
         * @brief Remove all devices, but not close them
         */
        void clear();

        /**
         * @brief Triggered when a device is added to the pool
         */
        Signal(targetAdded);

        /**
         * @brief Triggered when a device is removed from the pool
         */
        Signal(targetDeleted);

    protected:
        virtual void onAddDevice(SkAbstractDevice *){}
        virtual void onDelDevice(SkAbstractDevice *){}

    private:
        SkDataBuffer header;
        SkVector<SkAbstractDevice *> clients;
};

#endif // SKDEVICEREDISTR_H
