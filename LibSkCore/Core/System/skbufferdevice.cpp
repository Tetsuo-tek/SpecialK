#include "skbufferdevice.h"

DeclareWrapper_ENUM(SkBufferDeviceMode)
{
    SetupEnumWrapper(SkBufferDeviceMode);

    SetEnumItem(SkBufferDeviceMode::BVM_NOMODE);
    SetEnumItem(SkBufferDeviceMode::BVM_ONLYREAD);
    SetEnumItem(SkBufferDeviceMode::BVM_ONLYWRITE);
    SetEnumItem(SkBufferDeviceMode::BVM_READWRITE);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkBufferDevice, open, bool, *Arg_Custom(SkDataBuffer), Arg_Enum(SkBufferDeviceMode))
DeclareMeth_INSTANCE_RET(SkBufferDevice, pos, int64_t)
DeclareMeth_INSTANCE_RET(SkBufferDevice, rewind, bool)
DeclareMeth_INSTANCE_RET(SkBufferDevice, seek, bool, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkBufferDevice, atEof, bool)
DeclareMeth_INSTANCE_RET(SkBufferDevice, isSequencial, bool)
//DeclareMeth_INSTANCE_RET(SkBufferDevice, canReadLine, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkBufferDevice, size, int64_t)
DeclareMeth_INSTANCE_RET(SkBufferDevice, bytesAvailable, int64_t)
DeclareMeth_INSTANCE_VOID(SkBufferDevice, close)
DeclareMeth_INSTANCE_RET(SkBufferDevice, currentMode, SkBufferDeviceMode)
DeclareMeth_INSTANCE_RET(SkBufferDevice, getCurrentModeString, CStr*)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkBufferDevice, SkAbstractDevice,
                {
                    AddEnumType(SkBufferDeviceMode);

                    AddMeth_INSTANCE_RET(SkBufferDevice, open);
                    AddMeth_INSTANCE_RET(SkBufferDevice, pos);
                    AddMeth_INSTANCE_RET(SkBufferDevice, rewind);
                    AddMeth_INSTANCE_RET(SkBufferDevice, seek);
                    AddMeth_INSTANCE_RET(SkBufferDevice, atEof);
                    AddMeth_INSTANCE_RET(SkBufferDevice, isSequencial);
                    //AddMeth_INSTANCE_RET(SkBufferDevice, canReadLine);
                    AddMeth_INSTANCE_RET(SkBufferDevice, size);
                    AddMeth_INSTANCE_RET(SkBufferDevice, bytesAvailable);
                    AddMeth_INSTANCE_VOID(SkBufferDevice, close);
                    AddMeth_INSTANCE_RET(SkBufferDevice, currentMode);
                    AddMeth_INSTANCE_RET(SkBufferDevice, getCurrentModeString);
                })
{
    buffer = nullptr;
    cursor = 0;
}

bool SkBufferDevice::open(SkDataBuffer &buf, SkBufferDeviceMode m)
{
    if (isOpen())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    buffer = &buf;
    cursor = 0;
    openMode = m;

    ObjectDebug("Buffer device is OPEN [mode: " << getCurrentModeString() << "]");
    notifyState(SkDeviceState::Open);
    return true;
}

int64_t SkBufferDevice::pos()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return -1;
    }

    return static_cast<int64_t>(cursor);
}

bool SkBufferDevice::rewind()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    cursor = 0;
    return true;
}

bool SkBufferDevice::seek(uint64_t position)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (position >= buffer->size())
    {
        notifyError(SkDeviceError::CannotSeekDevice, __PRETTY_FUNCTION__);
        return false;
    }

    cursor = position;
    return true;
}

bool SkBufferDevice::atEof()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return (static_cast<int64_t>(cursor) >= size()-1);
}

bool SkBufferDevice::canReadLine(uint64_t &lineLength)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    int64_t p = buffer->indexOf('\n', cursor);

    if (p >= 0)
    {
        lineLength = static_cast<uint64_t>(p);
        return true;
    }

    return false;
}

int64_t SkBufferDevice::size()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return -1;
    }

    return static_cast<int64_t>(buffer->size());
}

uint64_t SkBufferDevice::bytesAvailable()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    return buffer->size() - cursor;
}

void SkBufferDevice::close()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    buffer = nullptr;
    cursor = 0;
    openMode = SkBufferDeviceMode::BVM_NOMODE;

    ObjectDebug("Buffer device is CLOSED");
    notifyState(SkDeviceState::Closed);
}

SkBufferDeviceMode SkBufferDevice::currentMode()
{
    return openMode;
}

CStr *SkBufferDevice::getCurrentModeString()
{
    if (openMode == SkBufferDeviceMode::BVM_ONLYREAD)
        return "OnlyRead";

    else if (openMode == SkBufferDeviceMode::BVM_ONLYWRITE)
        return "OnlyWrite";

    else if (openMode == SkBufferDeviceMode::BVM_READWRITE)
        return "ReadWrite";

    return "NoMode";
}

bool SkBufferDevice::readInternal(char *data, uint64_t &len, bool onlyPeek)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t tempSizeAvailable = bytesAvailable();

    if (tempSizeAvailable == 0)
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Cannot READ because it has NOT data");
        return false;
    }

    if (len == 0 || len > tempSizeAvailable)
        len = tempSizeAvailable;

    if (!buffer->copyTo(data, cursor, len))
    {
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (!onlyPeek)
        cursor += len;

    notifyRead(len);
    return true;
}

#include "Core/Containers/skarraycast.h"

bool SkBufferDevice::writeInternal(CStr *data, uint64_t &len)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = false;

    //THE CURSOR IS ON THE END
    if (atEof())
    {
        ok = buffer->append(data, len);
        cursor += len;
    }

    else
    {
        uint64_t available = bytesAvailable();

        if (len <= available)
        {
            char *d = SkArrayCast::toChar(buffer->toVoid());
            memcpy(&d[cursor], data, len);
            cursor += len;
            ok = true;
        }

        else
        {
            char *d = SkArrayCast::toChar(buffer->toVoid());
            memcpy(&d[cursor], data, available);
            cursor += available;

            uint64_t remain = len - available;

            ok = buffer->append(&data[available], remain);
            cursor += remain;
        }
    }

    if (ok)
        notifyWrite(len);

    else
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);

    return ok;
}
