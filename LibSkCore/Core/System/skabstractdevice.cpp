#include "skabstractdevice.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skvariant.h"
#include "Core/Containers/skringbuffer.h"
#include "Core/Containers/skdatabuffer.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkDeviceError)
{
    SetupEnumWrapper(SkDeviceError);

    SetEnumItem(SkDeviceError::CannotOpen);
    SetEnumItem(SkDeviceError::CannotSeekDevice);
    SetEnumItem(SkDeviceError::CannotReadFromDevice);
    SetEnumItem(SkDeviceError::CannotWriteToDevice);
    SetEnumItem(SkDeviceError::DeviceAlreadyOpen);
    SetEnumItem(SkDeviceError::CannotUseClosedDevice);
    SetEnumItem(SkDeviceError::TargetNotValid);
    SetEnumItem(SkDeviceError::DeviceInternalError);
}

DeclareWrapper_ENUM(SkDeviceState)
{
    SetupEnumWrapper(SkDeviceState);

    SetEnumItem(Open);
    SetEnumItem(Closed);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkAbstractDevice, isOpen, bool)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, isSequencial, bool)
//DeclareMeth_INSTANCE_RET(SkAbstractDevice, canReadLine, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, canWrite, bool)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, canRead, bool)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, flush, bool)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, size, int64_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, pos, int64_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, bytesAvailable, int64_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, rewind, bool)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, seek, bool, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, atEof, bool)
DeclareMeth_INSTANCE_VOID(SkAbstractDevice, close)
//DeclareMeth_INSTANCE_RET(SkAbstractDevice, readFromWriteHere, bool, Arg_Custom(SkAbstractDevice), Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readHereWriteTo, bool, Arg_Custom(SkAbstractDevice))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readHereWriteTo, 1, bool, Arg_Custom(SkAbstractDevice), Arg_UInt64_REAL)

//DeclareMeth_INSTANCE_RET(SkAbstractDevice, read, bool, Arg_Char_REAL)
//DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 1, bool, Arg_Char_REAL, Arg_Bool)
//DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 2, bool, Arg_Data(char), Arg_UInt64_REAL)
//DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 3, bool, Arg_Data(char), Arg_UInt64_REAL, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 4, bool, Arg_StringRef, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 5, bool, Arg_StringRef, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 6, bool, Arg_Custom(SkDataBuffer), Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 7, bool, Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 8, bool, Arg_Custom(SkRingBuffer), Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 9, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_Bool)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, peek, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 1, bool, Arg_StringRef, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 2, bool, Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 3, bool, Arg_Custom(SkDataBuffer), Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 4, bool, Arg_Custom(SkRingBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 5, bool, Arg_Custom(SkRingBuffer), Arg_UInt64)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, readAll, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readAll, 1, bool, Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readAll, 2, bool, Arg_Custom(SkRingBuffer))

DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekAll, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peekAll, 1, bool, Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peekAll, 2, bool, Arg_Custom(SkRingBuffer))

DeclareMeth_INSTANCE_RET(SkAbstractDevice, write, bool, Arg_Char)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 1, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 2, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 3, bool, Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 4, bool, Arg_Custom(SkDataBuffer), Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 5, bool, Arg_Custom(SkRingBuffer))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 6, bool, Arg_Custom(SkRingBuffer), Arg_UInt64)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeUInt8, bool, Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readUInt8, uint8_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekUInt8, uint8_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeUInt16, bool, Arg_UInt16)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readUInt16, uint16_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekUInt16, uint16_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeUInt32, bool, Arg_UInt32)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readUInt32, uint32_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekUInt32, uint32_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeUInt64, bool, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readUInt64, uint64_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekUInt64, uint64_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeInt8, bool, Arg_Int8)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readInt8, int8_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekInt8, int8_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeInt16, bool, Arg_Int16)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readInt16, int16_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekInt16, int16_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeInt32, bool, Arg_Int32)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readInt32, int32_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekInt32, int32_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeInt64, bool, Arg_Int64)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readInt64, int64_t)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekInt64, int64_t)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeFloat, bool, Arg_Float)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readFloat, float)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekFloat, float)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, writeDouble, bool, Arg_Double)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, readDouble, double)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, peekDouble, double)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, getTxDataCounter, uint64_t)
DeclareMeth_INSTANCE_VOID(SkAbstractDevice, resetTxDataCounter)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, getRxDataCounter, uint64_t)
DeclareMeth_INSTANCE_VOID(SkAbstractDevice, resetRxDataCounter)
DeclareMeth_INSTANCE_VOID(SkAbstractDevice, resetAllDataCounters)

DeclareMeth_INSTANCE_RET(SkAbstractDevice, currentState, SkDeviceState)
DeclareMeth_INSTANCE_RET(SkAbstractDevice, getCurrentStateString, CStr*)
DeclareMeth_STATIC_RET(SkAbstractDevice, getErrorString, CStr*, Arg_Enum(SkDeviceError))

// // // // // // // // // // // // // // // // // // // // //

AbstractConstructorImpl(SkAbstractDevice, SkObject,
                        {
                            AddEnumType(SkDeviceError);
                            AddEnumType(SkDeviceState);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, isOpen);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, isSequencial);
                            //AddMeth_INSTANCE_RET(SkAbstractDevice, canReadLine);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, canWrite);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, canRead);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, flush);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, size);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, pos);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, bytesAvailable);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, rewind);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, seek);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, atEof);
                            AddMeth_INSTANCE_VOID(SkAbstractDevice, close);
                            //AddMeth_INSTANCE_RET(SkAbstractDevice, readFromWriteHere);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readHereWriteTo);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readHereWriteTo, 1);

                            //AddMeth_INSTANCE_RET(SkAbstractDevice, read);
                            //AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 1);
                            //AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 2);
                            //AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 3);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 4);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 5);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 6);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 7);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 8);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, read, 9);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, peek);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 1);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 2);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 3);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 4);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peek, 5);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, readAll);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readAll, 1);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, readAll, 2);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekAll);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peekAll, 1);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, peekAll, 2);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, write);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 1);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 2);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 3);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 4);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 5);
                            AddMeth_INSTANCE_RET_OVERLOAD(SkAbstractDevice, write, 6);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeUInt8);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readUInt8);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekUInt8);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeUInt16);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readUInt16);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekUInt16);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeUInt32);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readUInt32);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekUInt32);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeUInt64);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readUInt64);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekUInt64);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeInt8);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readInt8);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekInt8);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeInt16);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readInt16);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekInt16);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeInt32);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readInt32);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekInt32);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeInt64);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readInt64);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekInt64);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeFloat);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readFloat);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekFloat);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, writeDouble);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, readDouble);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, peekDouble);

                            AddMeth_INSTANCE_RET(SkAbstractDevice, getTxDataCounter);
                            AddMeth_INSTANCE_VOID(SkAbstractDevice, resetTxDataCounter);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, getRxDataCounter);
                            AddMeth_INSTANCE_VOID(SkAbstractDevice, resetRxDataCounter);
                            AddMeth_INSTANCE_VOID(SkAbstractDevice, resetAllDataCounters);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, currentState);
                            AddMeth_INSTANCE_RET(SkAbstractDevice, getCurrentStateString);
                            AddMeth_STATIC_RET(SkAbstractDevice, getErrorString);
                        })
{
    state = SkDeviceState::Closed;
    wroteDataCounter = 0;
    readDataCounter = 0;

    SignalSet(readyRead);
    SignalSet(bytesWritten);
    SignalSet(bytesRead);
    SignalSet(stateChanged);
    SignalSet(errorOccurred);
    SignalSet(open);
    SignalSet(closed);
}

bool SkAbstractDevice::isOpen()
{
    return (state == SkDeviceState::Open);
}

bool SkAbstractDevice::readFromWriteHere(SkAbstractDevice *dev, uint64_t &len)
{
    if (len == 0)
        len = dev->bytesAvailable();

    if (len == 0)
    {
        ObjectError("There are not data to read");
        return false;
    }

    char *b = new char [len];
    bool ok = dev->read(b, len);

    if (ok)
        ok = write(b, len);

    delete [] b;
    return ok;
}

bool SkAbstractDevice::readHereWriteTo(SkAbstractDevice *dev, uint64_t len)
{
    if (len == 0)
        len = bytesAvailable();

    if (len == 0)
    {
        ObjectError("There are not data to read");
        return false;
    }

    char *b = new char [len];
    bool ok = read(b, len);

    if (ok)
        ok = dev->write(b, len);

    delete [] b;
    return ok;
}

bool SkAbstractDevice::read(char &c, bool onlyPeek)
{
    uint64_t sz = 1;
    return readInternal(&c, sz, onlyPeek);
}

bool SkAbstractDevice::read(char *data, uint64_t &len, bool onlyPeek)
{
    return readInternal(data, len, onlyPeek);
}

bool SkAbstractDevice::read(SkString &s, uint64_t len, bool onlyPeek)
{
    uint64_t sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    //DO NOT DE-COMMENT THIS, IT I A WRONG MODE COMPARED TO OTHERs!!!
    /*if (!s.isEmpty())
        s.clear();*/

    char *d = new char [len];

    bool ok = this->readInternal(d, len, onlyPeek);

    if (ok)
        s.append(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractDevice::read(SkDataBuffer *buf, uint64_t len, bool onlyPeek)
{
    uint64_t sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    if (len)
    {
        char *d = new char [len];
        bool ok = this->readInternal(d, len, onlyPeek);

        if (ok)
            buf->append(d, len);

        delete [] d;
        return ok;
    }

    return false;
}

bool SkAbstractDevice::read(SkRingBuffer *buf, uint64_t len, bool onlyPeek)
{
    uint64_t sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    if (len)
    {
        char *d = new char [len];
        bool ok = readInternal(d, len, onlyPeek);

        if (ok)
            buf->addData(d, len);

        delete [] d;
        return ok;
    }

    return false;
}

bool SkAbstractDevice::peek(SkString &s, uint64_t len)
{
    return read(s, len, true);
}

bool SkAbstractDevice::peek(SkDataBuffer *buf, uint64_t len)
{
    return read(buf, len, true);
}

bool SkAbstractDevice::peek(SkRingBuffer *buf, uint64_t len)
{
    return read(buf, len, true);
}

bool SkAbstractDevice::readAll(SkString &s)
{
    uint64_t lastPos = 0;

    if (!isSequencial())
    {
        lastPos = pos();
        seek(0);
    }

    uint64_t len = 0;
    bool ok = read(s, len);

    if (!isSequencial())
        seek(lastPos);

    return ok;
}

bool SkAbstractDevice::readAll(SkDataBuffer *buf)
{
    uint64_t lastPos = 0;

    if (!isSequencial())
    {
        lastPos = pos();
        seek(0);
    }

    uint64_t len = 0;
    bool ok = read(buf, len);

    if (!isSequencial())
        seek(lastPos);

    return ok;
}

bool SkAbstractDevice::readAll(SkRingBuffer *buf)
{
    uint64_t lastPos = 0;

    if (!isSequencial())
    {
        lastPos = pos();
        seek(0);
    }

    uint64_t len = 0;
    bool ok = read(buf, len);

    if (!isSequencial())
        seek(lastPos);

    return ok;
}

bool SkAbstractDevice::peekAll(SkString &s)
{
    uint64_t len = 0;
    return read(s, len, true);
}

bool SkAbstractDevice::peekAll(SkDataBuffer *buf)
{
    uint64_t len = 0;
    return read(buf, len, true);
}

bool SkAbstractDevice::peekAll(SkRingBuffer *buf)
{
    uint64_t len = 0;
    return read(buf, len, true);
}

bool SkAbstractDevice::write(char c)
{
    uint64_t sz = 1;
    return this->writeInternal(&c, sz);
}

bool SkAbstractDevice::write(SkString &s)
{
    if (s.isEmpty())
        return false;

    uint64_t sz = s.size();
    return this->writeInternal(s.c_str(), sz);
}

bool SkAbstractDevice::write(CStr *data, uint64_t len)
{
    return writeInternal(data, len);
}

bool SkAbstractDevice::write(SkDataBuffer *buf, uint64_t len)
{
    if (len == 0)
        len = buf->size();

    if (len == 0)
    {
        ObjectError("You are writing nothing to the device, from DataBuffer");
        return false;
    }

    char *d = new char [len];
    bool ok = buf->copyTo(d, len);

    if (ok)
        writeInternal(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractDevice::write(SkRingBuffer *buf, uint64_t len)
{
    if (len == 0)
        len = buf->size();

    if (len == 0)
    {
        ObjectError("You are writing nothing to the device, from RingBuffer");
        return false;
    }

    char *d = new char [len];
    bool ok = buf->getCustomBuffer(d, len);

    if (ok)
        writeInternal(d, len);

    delete [] d;
    return ok;
}

#include "Core/Containers/skarraycast.h"

#define CASTEDVALUES_DEF(SUFFIX, TYPE) \
    bool SkAbstractDevice::write ## SUFFIX (TYPE val) \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = val; \
        return write(SkArrayCast::toChar(dataVal), sizeof(TYPE)); \
    } \
    \
    TYPE SkAbstractDevice::read ## SUFFIX () \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = 0; \
    \
        uint64_t sz = sizeof(TYPE);\
        if (read(SkArrayCast::toChar(dataVal), sz)) \
            return dataVal[0]; \
     \
        return 0; \
    }\
    TYPE SkAbstractDevice::peek ## SUFFIX () \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = 0; \
    \
        uint64_t sz = sizeof(TYPE);\
        if (read(SkArrayCast::toChar(dataVal), sz, true)) \
            return dataVal[0]; \
     \
        return 0; \
    }

CASTEDVALUES_DEF(UInt8, uint8_t)
CASTEDVALUES_DEF(Int8, int8_t)
CASTEDVALUES_DEF(UInt16, uint16_t)
CASTEDVALUES_DEF(Int16, int16_t)
CASTEDVALUES_DEF(UInt32, uint32_t)
CASTEDVALUES_DEF(Int32, int32_t)
CASTEDVALUES_DEF(UInt64, uint64_t)
CASTEDVALUES_DEF(Int64, int64_t)
/*CASTEDVALUES_DEF(SizeT, uint64_t)
CASTEDVALUES_DEF(SSizeT, int64_t)*/
CASTEDVALUES_DEF(Float, float)
CASTEDVALUES_DEF(Double, double)

void SkAbstractDevice::notifyState(SkDeviceState st)
{
    state = st;
    ObjectDebug("STATE CHANGED TO: " << getCurrentStateString());

    SkVariantVector p;
    p << (static_cast<int>(state));
    stateChanged(this, p);

    if (state == SkDeviceState::Open)
        open();

    else if (state == SkDeviceState::Closed)
        closed();
}

SkDeviceState SkAbstractDevice::currentState()
{
    return state;
}

CStr *SkAbstractDevice::getCurrentStateString()
{
    if (state == SkDeviceState::Open)
        return "Open";

    return "Closed";
}

void SkAbstractDevice::notifyError(SkDeviceError err, CStr *prettyFrom, CStr *comment)
{
    if (comment)
        ObjectError(SkAbstractDevice::getErrorString(err) << ": " << comment << " [from: " << prettyFrom << "]");
    else
        ObjectError(SkAbstractDevice::getErrorString(err) << " [from: " << prettyFrom << "]");

    SkVariantVector p;
    p << (static_cast<int>(err));
    errorOccurred(this, p);
}

CStr *SkAbstractDevice::getErrorString(SkDeviceError error)
{
    if (error == SkDeviceError::CannotOpen)
        return "Cannot open";

    else if (error == SkDeviceError::CannotSeekDevice)
        return "Cannot seek";

    else if (error == SkDeviceError::CannotReadFromDevice)
        return "Cannot read";

    else if (error == SkDeviceError::CannotWriteToDevice)
        return "Cannot write";

    else if (error == SkDeviceError::DeviceAlreadyOpen)
        return "Device is ALREADY open";

    else if (error == SkDeviceError::CannotUseClosedDevice)
        return "Cannot use closed device";

    else if (error == SkDeviceError::TargetNotValid)
        return "Target is NOT valid";

    else if (error == SkDeviceError::DeviceInternalError)
        return "Device internal error";

    return "NoError";
}

void SkAbstractDevice::notifyRead(uint64_t &sz)
{
    readDataCounter += sz;
    SkVariantVector p;
    p << (static_cast<uint32_t>(sz));
    bytesRead(this, p);
}

void SkAbstractDevice::notifyWrite(uint64_t &sz)
{
    wroteDataCounter += sz;
    SkVariantVector p;
    p << (static_cast<uint32_t>(sz));
    bytesWritten(this, p);
}

uint64_t SkAbstractDevice::getTxDataCounter()
{
    return wroteDataCounter;
}

void SkAbstractDevice::resetTxDataCounter()
{
    wroteDataCounter = 0;
}

uint64_t SkAbstractDevice::getRxDataCounter()
{
    return readDataCounter;
}

void SkAbstractDevice::resetRxDataCounter()
{
    readDataCounter = 0;
}

void SkAbstractDevice::resetAllDataCounters()
{
    resetRxDataCounter();
    resetTxDataCounter();
}

/*bool SkAbstractDevice::isErrorValid(CStr *origniMeth)
{
    globalError = errno;

    if (globalError != 0)
    {
        errorOccurred();

        ObjectError("DEVICE ERROR [" << globalError << "]: "
                  << "'" << strerror(globalError) << "' while calling meth '"
                  << origniMeth
                  << "()'");
    }

    return globalError;
}

void SkAbstractDevice::resetCurrentError()
{
    error = SkDeviceError::NoDeviceError;
    globalError = 0;
    errno = 0;
}
*/
