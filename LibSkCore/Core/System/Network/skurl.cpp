#include "skurl.h"

#include <Core/sklogmachine.h>

SkUrl::SkUrl(CStr *url)
{
    urlValid = false;
    port = 0;

    if (url)
        set(url);
}

bool SkUrl::set(CStr *url)
{
    reset();

    SkString completeURL = url;
    SkStringList l;

    //http://127.0.0.1:9999/index?abba=p%20reo
    //Protocol
    completeURL.split("://", l);

    if (l.count() == 2)
    {
        protocol = l.first();
        completeURL = l.last();

        l.clear();

        //User:Passwd
        completeURL.split("@", l);

        if (l.count() == 2)
        {
            completeURL = l.last();

            SkString s = l.first();
            l.clear();

            s.split(":", l);

            if (l.count() == 2)
            {
                userName = l.first();
                password = l.last();
            }
        }
    }

    CStr *d = completeURL.c_str();
    uint64_t i;

    for(i=0; i<completeURL.size(); i++)
    {
        if (d[i] == '/' || d[i] == ':')
            break;

        else
            host.concat(d[i]);
    }

    SkString portString;

    if (d[i] == ':')
    {
        i++;
        for(; i<completeURL.size(); i++)
        {
            if (d[i] == '/')
                break;
            else
                portString.concat(d[i]);
        }

        port = static_cast<uint16_t>(portString.toInt());
    }

    if (d[i] == '/')
    {
        for(; i<completeURL.size(); i++)
        {
            if (d[i] == '?' || d[i] == '#')
                break;
            else
                path.concat(d[i]);
        }
    }

    if (d[i] == '?')
    {
        i++;

        for(; i<completeURL.size(); i++)
        {
            if (d[i] == '#')
                break;
            else
                query.concat(d[i]);
        }

        l.clear();
        query.split("&", l);

        for(uint64_t o=0; o<l.count(); o++)
        {
            SkStringList subL;

            l[o].split("=", subL);

            if (subL.count() == 2)
            {
                SkString k;
                decode(subL.first().c_str(), k);
                SkString v;
                decode(subL.last().c_str(), v);
                queryMap[k] = v;
            }
        }
    }

    if (d[i] == '#')
    {
        i++;
        for(; i<completeURL.size(); i++)
            fragment.concat(d[i]);
    }

    urlValid = true;
    return true;
}

bool SkUrl::isValid()
{
    return urlValid;
}

CStr *SkUrl::getProtocol()
{
    return protocol.c_str();
}

bool SkUrl::hasLogin()
{
    return (!userName.isEmpty() && !password.isEmpty());
}

CStr *SkUrl::getUserName()
{
    return userName.c_str();
}

CStr *SkUrl::getPassword()
{
    return password.c_str();
}

CStr *SkUrl::getHost()
{
    return host.c_str();
}

uint16_t SkUrl::getPort()
{
    return port;
}

CStr *SkUrl::getPath()
{
    return path.c_str();
}

bool SkUrl::hasQuery()
{
    return !query.isEmpty();
}

CStr *SkUrl::getQuery()
{
    return query.c_str();
}

SkArgsMap &SkUrl::getQueryMap()
{
    return queryMap;
}

bool SkUrl::hasFragment()
{
    return !fragment.isEmpty();
}

CStr *SkUrl::getFragment()
{
    return fragment.c_str();
}

SkString SkUrl::completeUrlString()
{
    //A completeURL:
    //  protocol://[username[:password]@]host[:port]</path>[?querystring][#fragment]

    SkString completeURL;

    if (!protocol.isEmpty())
    {
        completeURL = protocol;
        completeURL.append("://");
    }

    if (!userName.isEmpty())
    {
        completeURL.append(userName);

        if (!password.isEmpty())
        {
            completeURL.append(":");
            completeURL.append(password);
        }

        completeURL.append("@");
    }

    completeURL.append(host);

    if (port > 0)
    {
        completeURL.append(":");
        completeURL.append(SkString::number(port));
    }

    completeURL.append(path);

    if (!query.isEmpty())
    {
        completeURL.append("?");
        completeURL.append(query);
    }

    if (!fragment.isEmpty())
    {
        completeURL.append("#");
        completeURL.append(fragment);
    }

    return completeURL.c_str();
}

void SkUrl::reset()
{
    urlValid = false;
    protocol.clear();
    userName.clear();
    password.clear();
    host.clear();
    port = 0;
    path.clear();
    query.clear();
    fragment.clear();
};

bool SkUrl::encode(CStr *urlToEncode, SkString &output)
{
    if (SkString::isEmpty(urlToEncode))
    {
        StaticError("Cannot encode because the url is empty");
        return false;
    }

    CStr *hex = "0123456789ABCDEF";

    while (*urlToEncode)
    {
        const char &c = *urlToEncode;

        if (('a' <= c && c <= 'z')
                || ('A' <= c && c <= 'Z')
                || ('0' <= c && c <= '9')
                || c == '/')
        {
            output.concat(c);
        }

        else
        {
            output.concat('%');
            output.concat(hex[c >> 4]);
            output.concat(hex[c & 15]);
        }

        urlToEncode++;
    }

    return true;
}

bool SkUrl::decode(CStr *urlToDecode, SkString &output)
{
    if (SkString::isEmpty(urlToDecode))
    {
        StaticError("Cannot decode because the url is empty");
        return false;
    }

    char a, b;
    char *dst = new char [strlen(urlToDecode) + 1];
    char *dstHolder = dst;

    while (*urlToDecode)
    {
        if ((*urlToDecode == '%')
                && ((a = urlToDecode[1]) && (b = urlToDecode[2]))
                && (isxdigit(a) && isxdigit(b)))
        {
            if (a >= 'a')
                a -= 'a'-'A';

            if (a >= 'A')
                a -= ('A' - 10);
            else
                a -= '0';

            if (b >= 'a')
                b -= 'a'-'A';

            if (b >= 'A')
                b -= ('A' - 10);
            else
                b -= '0';

            *dst++ = 16*a+b;
            urlToDecode+=3;
        }

        else if (*urlToDecode == '+')
        {
            *dst++ = ' ';
            urlToDecode++;
        }

        else
            *dst++ = *urlToDecode++;
    }

    *dst = '\0';
    output.append(dstHolder);
    delete [] dstHolder;
    return true;
}
