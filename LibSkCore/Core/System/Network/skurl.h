/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKURL_H
#define SKURL_H

#include <Core/Object/skflatobject.h>
#include <Core/Containers/skargsmap.h>

//A completeURL:
//  [protocol://][username[:password]@]host[:port]</path>[?querystring][#fragment]

class SPECIALK SkUrl extends SkFlatObject
{
    public:
        SkUrl(CStr *url=nullptr);

        //url query MUST BE ENCODED
        bool set(CStr *url);

        bool isValid();
        CStr *getProtocol();
        bool hasLogin();
        CStr *getUserName();
        CStr *getPassword();
        CStr *getHost();
        uint16_t getPort();
        CStr *getPath();
        bool hasQuery();
        CStr *getQuery();
        SkArgsMap &getQueryMap();
        bool hasFragment();
        CStr *getFragment();

        SkString completeUrlString();

        void reset();

        static bool encode(CStr *urlToEncode, SkString &output);
        static bool decode(CStr *urlToDecode, SkString &output);

    private:
        bool urlValid;
        SkString protocol;
        SkString userName;
        SkString password;
        SkString host;
        uint16_t port;
        SkString path;
        SkString query;
        SkArgsMap queryMap;
        SkString fragment;
};

#endif // SKURL_H
