#ifndef SKPINGER_H
/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#define SKPINGER_H

#include "Core/System/Time/skflattimer.h"
#include "Core/Containers/skdatabuffer.h"

#include <netinet/ip_icmp.h>

//PING OPERATION IS BLOCKING
//IT A GOOD IDEA TO NOT USE THIS OBJECT INSIDE AN EVENTLOOP
//WITH TIMER MODE == SkLoopTimerMode::SK_TIMEDLOOP_RT;
//USE SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING INSTEAD

class SkPinger extends SkFlatTimer
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkPinger, SkFlatTimer);

        //BY DEFAULT IT WILL SEND 64 CHAR-BYTEs (== '1')
        //OTHERWISE YOU COULD SET CUSTOM SIZE, OR SIZE AND DATA
        /**
         * @brief Sets custom data size and content for sending packets         *
         * @param size the size of data to send
         * @param data the array data pointer; if it is NULL data will contain
         * 64 bytes set to '1'
         */
        void setCustomData(uint64_t size, CStr *data=nullptr);

        //DEFAULT IS 64
        /**
         * @brief Set TimeToLive (TTL) value for the ICMP ping session
         * @param ttl the value; default is 64
         */
        void setTTL(int ttl);

        //target COULD BE AN hostname OR ip
        /**
         * @brief Starts to ping a target
         * @param target the hostName or IP address as cstring
         * @return true if there are not errors, otherwisse false
         */
        bool start(CStr *target);

        /**
         * @brief Gets the ping packet size
         * @return the packet size
         */
        uint64_t pckSize();

        /**
         * @brief Gets the target
         * @return the target as cstring
         */
        CStr *hostTarget();

        /**
         * @brief Gets the IP
         * @return the IP as cstring
         */
        CStr *hostIP();

        /**
         * @brief Gets the reversed hostName
         * @return the hostName as cstring
         */
        CStr *hostTargetReversed();

        /**
         * @brief Checks if there was a DNS reverse lookup
         * @return true if there is lookup, otherwise false
         */
        bool hasReverseLookup();

        /**
         * @brief Gets the total bytes sent in the current session
         * @return the total byte sent
         */
        uint sent();

        /**
         * @brief Gets the total bytes received in the current session
         * @return the total byte received
         */
        uint received();

        /**
         * @brief Gets the current TimeToLive (TTL) value
         * @return the current TTL value
         */
        int ttl();

        /**
         * @brief Gets the last RoundTripTime (RTT) in milliseconds
         * @return the RTT value
         */
        double rtt();

    private:
        int socketFD;
        struct sockaddr_in addr;
        SkString pingTargetHost;
        SkString lookupedIpAddress;
        SkString reversedHostName;
        bool hasReverse;
        int ttlVal;
        uint sentCount;
        uint receivedCount;

        struct icmphdr header;
        SkDataBuffer pingDataBuffer;
        SkDataBuffer pingPckBuffer;
        SkElapsedTime pongElapsedTime;
        double lastElapsedTime;

        bool checkDnsLookups(CStr *target);
        bool open();
        uint16_t pckChkSum();
        void pckPrepare();
        void close();

        void onStart();
        void timeout();
        void onStop();
};

#endif // SKPINGER_H
