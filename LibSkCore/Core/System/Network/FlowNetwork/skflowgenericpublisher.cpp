#include "skflowgenericpublisher.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"
#endif

ConstructorImpl(SkFlowGenericPublisher, SkAbstractFlowPublisher)
{
    channel = nullptr;
    channelID = -1;

#if defined(ENABLE_HTTP)
    mp = nullptr;
#endif

    ft = FT_BLOB;
    dt = T_BYTEARRAY;

    SignalSet(ready);
}

bool SkFlowGenericPublisher::setup(CStr *chName, SkFlow_T flow_t, SkVariant_T data_t, CStr *mime_t, SkArgsMap *properties)
{
    channelName = chName;

    ft = flow_t;
    dt = data_t;

    if (!SkString::isEmpty(mime_t))
        mt = mime_t;

    if (properties)
        channelProps = *properties;

    return true;
}

bool SkFlowGenericPublisher::start()
{
    if (!async)
    {
        ObjectError("Flow NOT valid");
        return false;
    }

    onStart();

    ObjectMessage("STARTED");
    return async->addStreamingChannel(channelID, ft, dt, channelName.c_str(), mt.c_str());
}

void SkFlowGenericPublisher::stop()
{
    if (!async)
    {
        ObjectError("Flow NOT valid");
        return;
    }

    onStop();

    if (channelID > -1)
        async->removeChannel(channelID);

    if (!channelProps.isEmpty())
        channelProps.clear();

    channelID = -1;
    channel = nullptr;
    ObjectMessage("STOPPED");
}

void SkFlowGenericPublisher::onChannelAdded(SkFlowChanID chanID)
{
    if (chanID == channelID)
    {
        channel = async->channel(channelID);

#if defined(ENABLE_HTTP)
        if (http)
            mp = http->redistrMountPoint(channelID);

        else
            ObjectWarning("HTTP service is DISABLED");
#endif

        if (!channelProps.isEmpty())
            async->setChannelProperties(channelID, channelProps);

        onChannelReady();
        ready();

        ObjectMessage("Channel is READY");
    }
}

void SkFlowGenericPublisher::onChannelRemoved(SkFlowChanID chanID)
{
    if (channel && channel->chanID == chanID)
    {
        onChannelExpired();

        channel = nullptr;

#if defined(ENABLE_HTTP)
        mp = nullptr;
#endif
    }
}

void SkFlowGenericPublisher::tick(CVoid *data, ULong sz)
{
    if (!hasTargets())
        return;

    if (SkAbstractFlowPublisher::hasTargets(channel))
        async->publish(channelID, data, sz);

#if defined(ENABLE_HTTP)
    if (SkAbstractFlowPublisher::hasTargets(mp))
        mp->send(SkArrayCast::toCStr(data), sz);
#endif
}

bool SkFlowGenericPublisher::hasTargets()
{
    if (SkAbstractFlowPublisher::hasTargets(channel))
        return true;

#if defined(ENABLE_HTTP)
    if (SkAbstractFlowPublisher::hasTargets(mp))
        return true;
#endif

    return false;
}

SkFlowChanID SkFlowGenericPublisher::getChannelID()
{
    return channelID;
}

SkFlowChannel *SkFlowGenericPublisher::getChannel()
{
    return channel;
}
