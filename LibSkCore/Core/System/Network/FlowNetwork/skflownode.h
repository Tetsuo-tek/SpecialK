#ifndef SKFLOWNODE_H
#define SKFLOWNODE_H

#include "skflowasync.h"

class SkFlowServer;

class SkFlowNode extends SkFlowAsync
{
    public:
        Constructor(SkFlowNode, SkFlowAsync);

        void init(CStr *address, Short port, CStr *name, CStr *password);
        bool replicate();

        Slot(onReplicatedChannelAdded);
        Slot(onReplicatedChannelRemoved);
        Slot(onReplicatedChannelPublishStartRequest);
        Slot(onReplicatedChannelPublishStopRequest);

        Slot(onFlowDisconnected);//not using onDisconnected virtual because it is used from SkFlowAsync

        Slot(onOneSecCheck);

    private:
        SkFlowServer *owner;
        SkString flowNodeAddress;
        Short flowNodePort;
        SkString flowNodeName;
        SkString flowNodePassword;

        SkTreeMap<SkFlowChanID, SkFlowChanID> replicatedChannels;//replicated, local
};

#endif // SKFLOWNODE_H
