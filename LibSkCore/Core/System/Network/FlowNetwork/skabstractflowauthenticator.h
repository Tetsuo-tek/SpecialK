#ifndef SKABSTRACTFLOWAUTHENTICATOR_H
#define SKABSTRACTFLOWAUTHENTICATOR_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Network/FlowNetwork/skabstractflowconnection.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkFlowServicePermission
{
    bool isOsUser;
    bool isVirtualAccount;
    bool isAdmin;
    bool canSaveDatabases;

    SkFlowServicePermission()
    {
        isOsUser = false;
        isVirtualAccount = false;
        isAdmin = false;
        canSaveDatabases = false;
    }
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowAuthorizedAccount extends SkFlatObject
{
    SkString n;
    SkString tk;
    SkString sessionID;
    SkTreeMap<SkAbstractDevice *, SkAbstractFlowConnection *> connections;

    SkFlowServicePermission perm;

    public:
        SkFlowAuthorizedAccount();

        void setup(CStr *name, CStr *token, CStr *sid, SkFlowServicePermission *permissions);

        bool addConnection(SkAbstractFlowConnection *conn);
        bool removeConnection(SkAbstractFlowConnection *conn);

        bool isVirtual();
        bool isOsUser();

        bool canSaveDatabases();

        CStr *userName();
        CStr *token();
        CStr *sid();

        bool hasConnections();
        bool hasConnection(SkAbstractDevice *sck);
        SkAbstractFlowConnection *connection(SkAbstractDevice *sck);

        SkFlowServicePermission &permissions();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractFlowAuthenticator extends SkObject
{
    SkTreeMap<SkString, SkFlowAuthorizedAccount *> auths;//name, auth
    SkTreeMap<SkString, SkString> virtualAccounts;//name, passwd

    public:
        SkAbstractFlowAuthenticator();

        void addVirtualAccount(CStr *username, CStr *token);

        bool authorize(SkAbstractFlowConnection *conn);
        CStr *authorizeSession(CStr *username, CStr *token);
        bool isValidSessionID(CStr *sid);

        void remove(SkAbstractFlowConnection *conn);
        void clear();

        bool isVirtualAccount(CStr *userName);
        bool isLogged(CStr *userName);
        SkFlowAuthorizedAccount *account(CStr *userName);

    protected:

        AbstractConstructor(SkAbstractFlowAuthenticator, SkObject);

        virtual bool onAuthorization(CStr *, CStr *, CStr *, SkFlowServicePermission *)    =0;
        //virtual void addUser(SkArgsMap &user);

        bool authorizeVirtualLogin(CStr *userName, CStr *token, SkFlowServicePermission *permissions);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKABSTRACTFLOWAUTHENTICATOR_H
