#ifndef SKFLOWSERVICEREQUESTSTATUS_H
#define SKFLOWSERVICEREQUESTSTATUS_H

#include "skflowprotocol.h"

class SkFlowRawSvrConnection;

class SkFlowServiceRequestStatus extends SkFlatObject
{
    SkString hashStr;
    bool coreCommand;
    bool e;
    SkString cmd;
    SkArgsMap resp;
    SkFlowRawSvrConnection *r;

    public:
        SkFlowServiceRequestStatus();

        void setup(CStr *command, CStr *hashID, bool isCoreCommand, SkFlowRawSvrConnection *requester);
        void setCoreResponse(bool hasError, CStr *msg);
        void setCoreResponse(bool hasError, CStr *msg, const SkVariant &optData);

        CStr *hashID();
        bool isCoreCommand();
        CStr *command();
        bool hasError();
        SkArgsMap &response();
        SkFlowRawSvrConnection *requester();
};

#endif // SKFLOWSERVICEREQUESTSTATUS_H
