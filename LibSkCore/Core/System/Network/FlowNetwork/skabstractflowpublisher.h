#ifndef SKABSTRACTFLOWPUBLISHER_H
#define SKABSTRACTFLOWPUBLISHER_H

#include "Core/System/Network/FlowNetwork/skflowasync.h"

#if defined(ENABLE_HTTP)
    #include <Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.h>
#endif

class SkAbstractFlowPublisher extends SkObject
{
    public:
        void setClient(SkFlowAsync *flowAsync);
        void unSetClient();

#if defined(ENABLE_HTTP)
        void setHttpService(SkFlowAsyncHttpService *service);
#endif

        virtual bool start()                                            =0;
        virtual void stop()                                             =0;

        Slot(channelAdded);
        Slot(channelRemoved);

        virtual bool hasTargets()                                       =0;

        static bool hasTargets(SkFlowChannel *ch);

#if defined(ENABLE_HTTP)
        static bool hasTargets(SkRawRedistrMountPoint *mp);
#endif

    protected:
        AbstractConstructor(SkAbstractFlowPublisher, SkObject);

        SkFlowAsync *async;

#if defined(ENABLE_HTTP)
        SkFlowAsyncHttpService *http;
#endif

        SkAbstractFlowPublisher *createPublisher(SkAbstractFlowPublisher *publisher);

    private:
        virtual void onChannelAdded(SkFlowChanID)                       {}
        virtual void onChannelRemoved(SkFlowChanID)                     {}
};

#endif // SKABSTRACTFLOWPUBLISHER_H
