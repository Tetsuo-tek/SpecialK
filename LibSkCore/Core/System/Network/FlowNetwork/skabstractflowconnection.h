#ifndef SKABSTRACTFLOWCONNECTION_H
#define SKABSTRACTFLOWCONNECTION_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skflowprotocol.h"
#include "Core/Containers/skset.h"
#include "Core/Containers/sktreemap.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowPairsDatabase;
class SkFlowServer;
class SkAbstractFlowAuthenticator;
class SkFlowAuthorizedAccount;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractFlowConnection extends SkObject
{
    public:
        CStr *name();           //LOGIN
        CStr *token();          //PASWD

        bool authorize();
        void disable();

        void setAuthenticator(SkAbstractFlowAuthenticator *authenticator);//WHITOUT auth ALL IS PERMITTED

        //void abortPendingRequest(CStr *hashStr);

        virtual void notifyChannelReqPublishStart(SkFlowChanID)     =0;
        virtual void notifyChannelReqPublishStop(SkFlowChanID)      =0;

        virtual SkAbstractDevice *getSck()                          =0;

        bool isAuthorized();

        CStr *seed();

        Signal(disabled);

    protected:
        SkAbstractFlowAuthenticator *auth;
        SkFlowServer *svr;

        SkString loginName;
        SkString loginToken;
        SkString lastSeed;
        bool authorized;
        SkFlowAuthorizedAccount *flowAccount;

        SkFlowPairsDatabase *currentDatabase;

        //MUST GO ON AUTHENTICATOR AND MUST HAVE ACCOUNT AS OWNER (NOT THE CONNECTION)
        SkSet<SkString> ownedDatabases;

        SkSet<SkFlowChanID> ownedChannels;
        SkSet<SkFlowChanID> subscribedChannels;
        SkSet<SkFlowChanID> registeredGrabChannels;

        //SkTreeMap<SkString, SkFlowChanID> requestsToChannels;//hash, svrChanID

        AbstractConstructor(SkAbstractFlowConnection, SkObject);

        virtual void notifyAddedChannel(SkFlowChanID)               =0;
        virtual void notifyRemovedChannel(SkFlowChanID)             =0;
        virtual void notifyChannelHeader(SkFlowChanID)              =0;

        virtual void onLogin()                                      =0;
        virtual void onDisable()                                    =0;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKABSTRACTFLOWCONNECTION_H
