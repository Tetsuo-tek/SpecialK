#ifndef SKFLOWBLOBSENDER_H
#define SKFLOWBLOBSENDER_H

#include "Core/Object/skobject.h"
#include "Core/System/Network/FlowNetwork/skflowcommon.h"
#include "Core/System/Time/skelapsedtime.h"

class SkFile;
class SkFlowPairsDatabase;
class SkFlowRawSvrConnection;

class SkFlowBlobSender extends SkObject
{
    SkFlowChanID chanID;
    SkString hashID;
    SkFlowPairsDatabase *db;
    SkFlowRawSvrConnection *flow;
    SkFile *blob;
    ULong sendingBufSz;
    float sendingTickInterval;
    SkElapsedTime sendingTickChrono;

    public:
        Constructor(SkFlowBlobSender, SkObject);

        bool start(CStr *filePath,
                   SkFlowChanID channelID,
                   CStr *reqHashID,
                   SkFlowPairsDatabase *channelDB,
                   SkFlowRawSvrConnection *downloader);

        bool stop();

        bool isRunning();
        CStr *filePath();
        SkFlowRawSvrConnection *downloader();

        Slot(onFastTick);
        Signal(progress);
        Slot(onDownloaderDisabled);
        Signal(terminated);
};

#endif // SKFLOWBLOBSENDER_H
