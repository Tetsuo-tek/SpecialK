#include "skflowserver.h"

#include "unistd.h"

#include "Core/App/skapp.h"
#include "Core/Containers/skdatabuffer.h"
#include <Core/Containers/skarraycast.h>
#include "Core/System/skdeviceredistr.h"
#include "Core/System/Network/LOCAL/sklocalserver.h"
#include "Core/System/Network/TCP/sktcpserver.h"

#if defined(ENABLE_REDIS)
#include <Core/System/Network/RedisNetwork/skredissync.h>
#include <Core/System/Network/RedisNetwork/skredissubscriber.h>
#endif

#include "Core/Containers/skringbuffer.h"

#include "skflowasync.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define CheckForChanID() \
    FakeSingleLine( \
        if (!isChannelValid(chanID)) \
        { \
            ObjectError("ChanID is NOT valid: " << chanID); \
            return false; \
        } \
    )

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowServer, SkObject)
{
    auth = nullptr;
    localWrk = nullptr;

    localSvr = nullptr;
    tcpSvr = nullptr;

    node = nullptr;

#if defined(ENABLE_REDIS)
    r = nullptr;
#endif

    pairsChan = -1;

    SlotSet(onFlowConnection);
    SlotSet(onFlowDisconnection);

    SignalSet(addedFlowConnection);
    SignalSet(removedFlowConnection);

    SlotSet(onPairChange);
    SignalSet(pairChanged);

    SignalSet(addedChannel);
    SignalSet(removedChannel);
    SignalSet(setChannelHeader);

    SlotSet(onSlowTick);
    SlotSet(onOneSecTick);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowServer::init(CStr *serverName, SkAbstractWorkerObject *localWorker, CStr *databasesPath, CStr *usersHomePath)
{
    localWrk = localWorker;
    svrName = serverName;

    SkFileInfo info;
    SkFsUtils::fillFileInfo(databasesPath, info);

    if (!info.isDir || !info.isReadable || !info.isWritable)
    {
        ObjectError("PairsDatabase path is NOT valid: " << databasesPath);
        return false;
    }

    dbPath = SkFsUtils::adjustPathEndSeparator(info.completeAbsolutePath.c_str());

    SkFsUtils::fillFileInfo(usersHomePath, info);

    if (!info.isDir || !info.isReadable || !info.isWritable)
    {
        ObjectError("UsersHome path is NOT valid: " << usersHomePath);
        return false;
    }

    usersPath = SkFsUtils::adjustPathEndSeparator(info.completeAbsolutePath.c_str());

    addPairDatabase("Main");
    getPairDatabase("Main")->setVariable("pid", ::getpid());

    addPairDatabase("Connections");

    pairsChan = addStreamingChannel(FT_PAIRS, T_STRING, "Pairs", "application/json");

    Attach(eventLoop()->slowZone_SIG, pulse, this, onSlowTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick, SkQueued);
    return true;
}

void SkFlowServer::setAuthenticator(SkAbstractFlowAuthenticator *authenticator)
{
    auth = authenticator;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowServer::close()
{
    Detach(eventLoop()->slowZone_SIG, pulse, this, onSlowTick);
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick);

    if (localSvr)
    {
        ObjectWarning("Closing Local DELIVERY ..");

        localSvr->stop();
        localSvr->destroyLater();
        localSvr = nullptr;
    }

    if (tcpSvr)
    {
        ObjectWarning("Closing Tcp DELIVERY ..");

        tcpSvr->stop();
        tcpSvr->destroyLater();
        tcpSvr = nullptr;
    }

    if (node)
    {
        node->close();
        node->destroyLater();
        node = nullptr;
    }

#if defined(ENABLE_REDIS)
    if (r)
    {
        r->close();
        r->destroyLater();
        r = nullptr;
    }
#endif

    ObjectMessage("Removing all clients ..");

    {
        SkAbstractListIterator<SkFlowRawSvrConnection *> *itr = connections.iterator();

        while(itr->next())
        {
            SkFlowRawSvrConnection *conn = itr->item();
            SkAbstractSocket *socket = dynamic_cast<SkAbstractSocket *>(conn->getSck());

            if (socket->isConnected())
            {
                Detach(socket, disconnected, this, onFlowDisconnection);
                socket->disconnect();
            }

            conn->destroyLater();
        }

        delete itr;
        connections.clear();
    }

    {
        SkAbstractListIterator<SkFlowRawSvrConnection *> *itr = deathReign.iterator();

        while(itr->next())
        {
            SkFlowRawSvrConnection *conn = itr->item();
            conn->destroyLater();
        }

        delete itr;
        deathReign.clear();
    }

    ObjectMessage("Removing all channels ..");

    for(uint64_t i=0; i<channels.count(); i++)
    {
        SkAbstractFlowChannel *ch = channels[i];

        if (!ch)
            continue;

        ch->disable();
        ch->destroyLater();
    }

    channels.clear();
    freeChans.clear();
    namedChannels.clear();

    {
        SkBinaryTreeVisit<SkString, SkFlowPairsDatabase *> *itr = databases.iterator();

        while(itr->next())
        {
            SkFlowPairsDatabase *db = itr->item().value();
            db->destroyLater();
        }

        delete itr;
        databases.clear();
    }

    replicatedChannels.clear();

    ObjectMessage("Terminated");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowServer::initLocalServer(CStr *path, int maxQueuedConnections)
{
    if (localSvr)
    {
        ObjectError("Local DELIVERY is ALREADY started: " << localSvr->getServerPath());
        return false;
    }

    localSvr = new SkLocalServer;
    localSvr->setObjectName(this, "LocalDelivery");

    Attach(localSvr, newSvrConnection, this, onFlowConnection, SkAttachMode::SkQueued);

    ObjectDebug("Starting Local DELIVERY: " << path << " ..");

    if (!localSvr->start(path, maxQueuedConnections))
        return false;

    ObjectMessage("Local DELIVERY initialized [maxQueuedConnections: " <<  maxQueuedConnections << "]: " << localSvr->getServerPath());
    return true;
}

CStr *SkFlowServer::getDeliveryLocalPath()
{
    return localSvr->getServerPath();
}

bool SkFlowServer::initTcpServer(CStr *address, uint16_t port, int maxQueuedConnections)
{
    if (tcpSvr)
    {
        ObjectError("Tcp DELIVERY is ALREADY started: " << tcpSvr->getServerAddress() << ":" << tcpSvr->getServerPort());
        return false;
    }

    tcpSvr = new SkTcpServer;
    tcpSvr->setObjectName(this, "TcpDelivery");

    Attach(tcpSvr, newSvrConnection, this, onFlowConnection, SkAttachMode::SkQueued);

    ObjectDebug("Starting Tcp DELIVERY: " << address << ":" << port << " ..");

    if (!tcpSvr->start(address, port, maxQueuedConnections))
        return false;

    ObjectMessage("Tcp DELIVERY initialized [maxQueuedConnections: " <<  maxQueuedConnections << "]: " << address << ":" << port);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowServer::replicate(CStr *address, Short port, CStr *name, CStr *password)
{
    node = new SkFlowNode(this);
    node->init(address, port, name, password);

    return node->replicate();
}

SlotImpl(SkFlowServer, onSlowTick)
{
    SilentSlotArgsWarning();

    {
        SkAbstractListIterator<SkFlowRawSvrConnection *> *itr = deathReign.iterator();

        while(itr->next())
        {
            SkFlowRawSvrConnection *conn = itr->item();
            conn->destroyLater();
            ObjectMessage("Connection expired: " << conn->objectName());
        }

        delete itr;
        deathReign.clear();
    }

    {
        SkAbstractListIterator<SkFlowRawSvrConnection *> *itr = connections.iterator();

        while(itr->next())
        {
            SkFlowRawSvrConnection *conn = itr->item();

            SkAbstractSocket *socket = dynamic_cast<SkAbstractSocket *>(conn->getSck());

            if (!socket->isConnected() && conn->existsProperty("canDie"))
            {
                itr->remove();
                deathReign << conn;
                ObjectMessage("Connection expiring: " << conn->objectName());
            }
        }

        delete itr;
    }
}

SlotImpl(SkFlowServer, onOneSecTick)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_REDIS)
bool SkFlowServer::enableRedis(CStr *serverHostname, uint16_t serverPort)
{
    r = new SkFlowRedisExport(this);

    if (!r->init(serverHostname, serverPort))
    {
        r->destroyLater();
        r = nullptr;
        return false;
    }

    return true;
}

bool SkFlowServer::isRedisEnabled()
{
    return (r!=nullptr);
}
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*bool SkFlowServer::buildPairDatabase(CStr *name, CStr *pathLocation, SkTreeMap<SkString, SkVariant> &database)
{
    SkString n(SkFsUtils::adjustPathEndSeparator(pathLocation));
    n.append(name);
    n.append(".variant");

    if (SkFsUtils::exists(n.c_str()))
    {
        ObjectError("PairDatabase ALREADY exists: " << n);
        return false;
    }

    SkRingBuffer valBuff;
    SkVariant v(database);
    v.toData(&valBuff);
    SkDataBuffer fileBuff;
    valBuff.copyTo(fileBuff);

    if (!SkFsUtils::writeDATA(n.c_str(), fileBuff))
    {
        FlatError("CANNOT save new database to file: " << n.c_str());
        return false;
    }

    return true;
}*/

bool SkFlowServer::addPairDatabase(CStr *name)
{
    if (databases.contains(name))
    {
        ObjectError("PairDatabase ALREADY exists: " << name);
        return false;
    }

    SkFlowPairsDatabase *db = new SkFlowPairsDatabase;
    SkString n(name);
    n.append(".db");
    db->setObjectName(this, n.c_str());
    db->setup(name, DynCast(SkFlowRedistrChannel, channels[pairsChan]), dbPath.c_str());

#if defined(ENABLE_REDIS)
    if (isRedisEnabled())
        db->setRedisExport(r);
#endif

    Attach(db, pairChanged, this, onPairChange, SkQueued);
    databases[db->name()] = db;

    {
        SkVariantList p;
        p << db->name();

        skApp->propagateEvent(this, "Flow", "PairDatabaseAdded", &p);
    }

    SkStringList l;
    databases.keys(l);
    databases["Main"]->setVariable("databases", l);

    ObjectMessage("ADDED PairDatabase: " << name);
    return true;
}

bool SkFlowServer::delPairDatabase(CStr *name)
{
    if (!databases.contains(name))
    {
        ObjectError("PairDatabase NOT found: " << name);
        return false;
    }

    {
        SkVariantList p;
        p << name;

        skApp->propagateEvent(this, "Flow", "PairDatabaseRemoved", &p);
    }

    databases.remove(name).value()->destroyLater();

    SkStringList l;
    databases.keys(l);
    databases["Main"]->setVariable("databases", l);

    ObjectMessage("REMOVED PairDatabase: " << name);
    return true;
}

SlotImpl(SkFlowServer, onPairChange)
{
    SilentSlotArgsWarning();

    SkFlowPairsDatabase *db = dynamic_cast<SkFlowPairsDatabase *>(referer);

    SkVariantVector p;
    p << Arg_CStr << Arg_CStr << Arg_AbstractVariadic << db->name();
    pairChanged(this, p);
}

void SkFlowServer::pairDbNames(SkStringList &names)
{
    databases.keys(names);
}

bool SkFlowServer::existsOptionalPairDb(CStr *name)
{
    return databases.contains(name);
}

SkFlowPairsDatabase *SkFlowServer::getPairDatabase(CStr *name)
{
    SkFlowPairsDatabase *db = nullptr;

    if (!SkString::isEmpty(name))
    {
        if (!databases.contains(name))
        {
            ObjectError("PairDatabase NOT found: " << name);
            return nullptr;
        }

        db = databases[name];
    }

    else
        db = databases["Main"];

    return db;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowServer::addSockets(SkAbstractServer *svr)
{
    while(svr->hasQueuedSockets())
    {
        int sd;
        svr->dequeueSD(sd);
        SkAbstractSocket *sck = nullptr;

        SkString name;

        if (svr == localSvr)
        {
            name = "Local(";
            name.concat(sd);
            name.append(")");
            sck = new SkLocalSocket(svr);
            sck->setObjectName(this, name.c_str());
            ObjectMessage_EXT(sck, "A new Local DELIVERY Client is connected");
        }

        else if (svr == tcpSvr)
        {
            name = "Tcp(";
            name.concat(sd);
            name.append(")");
            sck = new SkTcpSocket(svr);
            sck->setObjectName(this, name.c_str());
            ObjectMessage_EXT(sck, "A new Tcp DELIVERY Client is connected");
        }

        sck->setFileDescriptor(sd);
        sck->setAsServer();

        SkString n("Connection[");
        n.concat(sck->getFileDescriptor());
        n.append("]");

        SkFlowRawSvrConnection *conn = new SkFlowRawSvrConnection(this);
        conn->setObjectName(this, n.c_str());
        conn->setAuthenticator(auth);

        Attach(sck, disconnected, this, onFlowDisconnection, SkQueued);
        conn->setup(this, sck);

        connections << conn;

        addedFlowConnection(conn);

        ObjectMessage("Added new Connection: " << conn->objectName());
    }
}

SlotImpl(SkFlowServer, onFlowConnection)
{
    SilentSlotArgsWarning();
    addSockets(DynCast(SkAbstractServer, referer));
}

void SkFlowServer::removeSocket(SkAbstractSocket *socket)
{
    ObjectDebug("Removing and destroying Socket: " << socket);

    SkFlowRawSvrConnection *conn = static_cast<SkFlowRawSvrConnection *>(socket->property("fwSvrConn").toVoid());
    ObjectMessage("Disabling fsConnection: " << conn->objectName());

    conn->setProperty("canDie", SkVariant());
    conn->disable();

    if (socket->isConnected())
    {
        Detach(socket, disconnected, this, onFlowDisconnection);
        socket->disconnect();
    }

    removedFlowConnection(conn);
    ObjectMessage("Connection is going into the DeathReign: " << conn->objectName());
}

SlotImpl(SkFlowServer, onFlowDisconnection)
{
    SilentSlotArgsWarning();
    removeSocket(dynamic_cast<SkAbstractSocket *>(referer));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowChanID SkFlowServer::channelsCommonSetup(CStr *name, SkAbstractFlowChannel *ch)
{
    if (namedChannels.contains(name))
    {
        ObjectError("Channel with this name ALREADY exists: " << name);
        return -1;
    }

    SkFlowChanID chanID = -1;

    if (freeChans.isEmpty())
    {
        chanID = channels.count();
        channels << ch;
    }

    else
    {
        chanID = freeChans.dequeue();
        channels[chanID] = ch;
    }

    return chanID;
}

bool SkFlowServer::channelsCommonInit(SkAbstractFlowChannel *ch, SkArgsMap *props, SkAbstractFlowConnection *owner)
{
    if (!addPairDatabase(ch->name()))
        return false;

    SkFlowPairsDatabase *db = getPairDatabase(ch->name());

    if (props)
    {
        if (props->isEmpty())
            ObjectWarning("ChannelProps are EMPTY");

        else
        {
            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = props->iterator();

            while(itr->next())
            {
                ObjectMessage("Channel property -> " << ch->name() << "::" << itr->item().key() << " -> " << itr->item().value());
                db->setVariable(itr->item().key().c_str(), itr->item().value());
            }

            delete itr;
        }
    }

    else
        ObjectWarning("ChannelProps are NULL");

    namedChannels[ch->name()] = ch;

    if (owner)
    {
        ch->setOwner(owner);
        db->setOwner(owner);
    }

    ch->setPairDatabase(db);

    if (localWrk)
        ch->setWorker(localWrk);

    SkStringList l;
    namedChannels.keys(l);
    databases["Main"]->setVariable("channels", l);

    SkVariantVector p;
    p << ch->id();
    p << ch->name();
    p << ch->chanType();

    addedChannel(this, p);

    {
        SkVariantList p;
        p << ch->id();
        p << ch->name();

        skApp->propagateEvent(this, "Flow", "StreamingChannelAdded", &p);
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowChanID SkFlowServer::addStreamingChannel(SkFlow_T flowType, SkVariant_T dataType, CStr *name, CStr *mime, SkArgsMap *props, SkAbstractFlowConnection *owner)
{
    SkFlowRedistrChannel *ch = new SkFlowRedistrChannel(this);
    SkFlowChanID chanID = channelsCommonSetup(name, ch);

    if (!ch->setup(chanID, name, flowType, dataType, mime) || !channelsCommonInit(ch, props, owner))
    {
        channels[chanID] = nullptr;
        ch->destroyLater();
        return -1;
    }

    ObjectMessage("ADDED StreamingChannel: "
                  << "[ID: " << ch->id()
                  << "; name: " << ch->name()
                  << "; flow_t: " << SkFlowProto::flowTypeToString(ch->flowType())
                  << "; data_t: " << SkVariant::variantTypeName(ch->dataType())
                  << "; mime_t: " << ch->mime()
                  << "]");

    return ch->id();
}

SkFlowChanID SkFlowServer::addServiceChannel(CStr *name, SkArgsMap *props, SkAbstractFlowConnection *owner)
{
    SkFlowServiceChannel *ch = new SkFlowServiceChannel(this);
    SkFlowChanID chanID = channelsCommonSetup(name, ch);

    if (!ch->setup(chanID, name) || !channelsCommonInit(ch, props, owner))
    {
        channels[chanID] = nullptr;
        ch->destroyLater();
        return -1;
    }

    ObjectMessage("ADDED ServiceChannel: "
                  << "[ID: " << ch->id()
                  << "; name: " << ch->name()
                  << "]");

    return ch->id();
}

SkFlowChanID SkFlowServer::addBlobChannel(CStr *name, SkArgsMap *props, SkAbstractFlowConnection *owner)
{
    SkFlowBlobChannel *ch = new SkFlowBlobChannel(this);
    SkFlowChanID chanID = channelsCommonSetup(name, ch);

    if (!ch->setup(chanID, name, usersPath.c_str()) || !channelsCommonInit(ch, props, owner))
    {
        channels[chanID] = nullptr;
        ch->destroyLater();
        return -1;
    }

    ObjectMessage("ADDED BlobChannel: "
                  << "[ID: " << ch->id()
                  << "; name: " << ch->name()
                  << "]");

    return ch->id();
}

bool SkFlowServer::removeChannel(SkFlowChanID chanID)
{
    CheckForChanID();

    SkAbstractFlowChannel *ch = channels[chanID];
    delPairDatabase(ch->name());

    ch->disable();

    namedChannels.remove(ch->name());
    freeChans.enqueue(chanID);

    SkVariantVector p;
    p << ch->id();
    p << ch->name();
    p << ch->chanType();
    removedChannel(this, p);

    {
        SkVariantList p;
        p << chanID;
        p << ch->name();

        if (ch->chanType() == StreamingChannel)
        {
            ObjectMessage("REMOVED StreamingChannel: "
                          << "[ID: " << ch->id()
                          << "; name: " << ch->name()
                          << "]");

            skApp->propagateEvent(this, "Flow", "StreamingChannelRemoved", &p);
        }

        else if (ch->chanType() == ServiceChannel)
        {
            ObjectMessage("REMOVED ServiceChannel: "
                          << "[ID: " << ch->id()
                          << "; name: " << ch->name()
                          << "]");

            skApp->propagateEvent(this, "Flow", "ServiceChannelRemoved", &p);
        }

        else if (ch->chanType() == BlobChannel)
        {
            ObjectMessage("REMOVED BlobChannel: "
                          << "[ID: " << ch->id()
                          << "; name: " << ch->name()
                          << "]");

            skApp->propagateEvent(this, "Flow", "BlobChannelRemoved", &p);
        }
    }

    SkStringList l;
    namedChannels.keys(l);
    databases["Main"]->setVariable("channels", l);

    SkFlowChanID replicatedChanID = ch->replicatedID();

    if (replicatedChanID > -1)
        replicatedChannels.remove(replicatedChanID);

    channels[ch->id()] = nullptr;
    ch->destroyLater();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowServer::isChannelValid(SkFlowChanID chanID)
{
    return (chanID >= 0 && chanID < static_cast<SkFlowChanID>(channels.count()) && channels[chanID] != nullptr);
}

SkFlowChannel_T SkFlowServer::getChannelType(SkFlowChanID chanID)
{
    if (!isChannelValid(chanID))
        return ChannelNotValid;

    return channels[chanID]->chanType();
}

SkAbstractFlowChannel *SkFlowServer::getChannelByID(SkFlowChanID chanID)
{
    if (!isChannelValid(chanID))
    {
        ObjectError("ChanID is NOT valid: " << chanID);
        return nullptr;
    }

    return channels[chanID];
}

bool SkFlowServer::containsChannel(CStr *name)
{
    return namedChannels.contains(name);
}

SkAbstractFlowChannel *SkFlowServer::getChannelByName(CStr *name)
{
    if (!namedChannels.contains(name))
    {
        ObjectError("Channel with this name NOT found: " << name);
        return nullptr;
    }

    return namedChannels[name];
}

uint64_t SkFlowServer::getSubscribersCount(SkFlowChanID chanID)
{
    if (chanID >= static_cast<SkFlowChanID>(channels.count()) || channels[chanID] == nullptr)
    {
        ObjectError("Wrong ChanID: " << chanID);
        return 0;
    }

    if (channels[chanID]->chanType() == StreamingChannel)
        return dynamic_cast<SkFlowRedistrChannel *>(channels[chanID])->subscribersCount();

    /*else if (channels[chanID]->chanType() == BlobChannel)
        return dynamic_cast<SkFlowBlobChannel *>(channels[chanID])->subscribersCount();*/

    return 0;
}

uint64_t SkFlowServer::channelsCount()
{
    return channels.count();
}

SkAbstractFlowChannel **SkFlowServer::getChannels()
{
    return channels.data();
}
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


CStr *SkFlowServer::serverName()
{
    return svrName.c_str();
}

SkAbstractWorkerObject *SkFlowServer::localWortker()
{
    return localWrk;
}

SkAbstractFlowAuthenticator *SkFlowServer::authenticator()
{
    return auth;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkVariant &SkFlowServer::nv()
{
    return nullValue = SkVariant();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
