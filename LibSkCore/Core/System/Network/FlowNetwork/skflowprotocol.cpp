#include "skflowprotocol.h"
#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skarraycast.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowProto, SkObject)
{
    currentSendCmdTransaction = FCMD_NOCMD;
    currentSendRspTransaction = FRSP_NORSP;
    currentRecvCmdTransaction = FCMD_NOCMD;
    currentRecvRspTransaction = FRSP_NORSP;

    sck = nullptr;
    sendingBufferDevice = nullptr;
}

void SkFlowProto::setup(SkAbstractSocket *sckDevice)
{
    sck = sckDevice;

    sendingBuffer.setObjectName(this, "TxBuffer");
    sendingBufferDevice = new SkBufferDevice(sck);

    sendingBufferDevice->setObjectName(this, "TxBufferDev");
    //it will NOT close, it can bypass closing because it has not an fd
    sendingBufferDevice->open(sendingBuffer);
}

SkAbstractSocket *SkFlowProto::getSocket()
{
    return sck;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// SEND

void SkFlowProto::sendStartOfTransaction(SkFlowCommand cmd)
{
    if (sendingBufferDevice->bytesAvailable())
    {
        ObjectError("CMD transaction ALREADY open: " << commandToString(currentSendCmdTransaction));
        return;
    }

    currentSendCmdTransaction = cmd;
    sendingBufferDevice->writeUInt16(cmd);
}

void SkFlowProto::sendStartOfTransaction(SkFlowResponse rsp)
{
    if (sendingBufferDevice->bytesAvailable())
    {
        ObjectError("RSP transaction ALREADY open: " << responseToString(currentSendRspTransaction));
        return;
    }

    currentSendRspTransaction = rsp;
    sendingBufferDevice->writeUInt16(rsp);
}

void SkFlowProto::sendChanType(SkFlowChannel_T t)
{
    sendingBufferDevice->writeUInt8(t);
}

void SkFlowProto::sendFlowType(SkFlow_T t)
{
    sendingBufferDevice->writeUInt16(t);
}

void SkFlowProto::sendVariantType(SkVariant_T t)
{
    sendingBufferDevice->writeUInt16(t);
}

void SkFlowProto::sendChanID(SkFlowChanID chanID)
{
    sendingBufferDevice->write(SkArrayCast::toChar(&chanID), sizeof(SkFlowChanID));
}

void SkFlowProto::sendSize(uint32_t sz)
{
    sendingBufferDevice->writeUInt32(sz);
}

void SkFlowProto::sendBool(bool val)
{
    sendingBufferDevice->writeUInt8(val);
}

void SkFlowProto::sendInt8(int8_t val)
{
    sendingBufferDevice->writeInt8(val);
}

void SkFlowProto::sendUInt8(uint8_t val)
{
    sendingBufferDevice->writeUInt8(val);
}

void SkFlowProto::sendInt16(Short val)
{
    sendingBufferDevice->writeInt16(val);
}

void SkFlowProto::sendUInt16(UShort val)
{
    sendingBufferDevice->writeUInt16(val);
}

void SkFlowProto::sendInt32(Int val)
{
    sendingBufferDevice->writeInt32(val);
}

void SkFlowProto::sendUInt32(UInt val)
{
    sendingBufferDevice->writeUInt32(val);
}

void SkFlowProto::sendInt64(Long val)
{
    sendingBufferDevice->writeInt64(val);
}

void SkFlowProto::sendUInt64(ULong val)
{
    sendingBufferDevice->writeUInt64(val);
}

void SkFlowProto::sendFloat(float val)
{
    sendingBufferDevice->writeFloat(val);
}

void SkFlowProto::sendDouble(double val)
{
    sendingBufferDevice->writeDouble(val);
}

void SkFlowProto::sendCStr(CStr *str)
{
    uint64_t sz = SkString::size(str);
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(str, sz);
}

void SkFlowProto::sendString(SkString &str)
{
    uint64_t sz = str.size();
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(str.c_str(), sz);
}

void SkFlowProto::sendJSON(SkVariant &val)
{
    SkString str;
    val.toJson(str);
    sendString(str);
}

void SkFlowProto::sendVariant(SkVariant &val)
{
    uint64_t sz = val.size();
    sendSize(sz);
    sendVariantType(val.variantType());

    if (!sz)
        return;

    sendingBufferDevice->write(val.data(), sz);
}

void SkFlowProto::sendBuffer(SkDataBuffer &b)
{
    uint64_t sz = b.size();
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(b.data(), sz);
}

void SkFlowProto::sendBuffer(CVoid *data, uint64_t tempSize)
{
    sendSize(tempSize);

    if (!tempSize)
        return;

    sendingBufferDevice->write(static_cast<CStr *>(data), tempSize);
}

void SkFlowProto::sendEndOfList()
{
    sendingBufferDevice->writeUInt32(0);
}

bool SkFlowProto::sendEndOfTransaction()
{
    if (!sck->isConnected())
    {
        sendingBuffer.clear();
        ObjectError("CANNOT send protocol-transaction [isOpen: false]");
        return false;
    }

    sendingBufferDevice->writeUInt32(0);
    bool ok = sck->write(&sendingBuffer);
    sendingBufferDevice->rewind();
    sendingBuffer.clear();
    currentSendCmdTransaction = FCMD_NOCMD;
    currentSendRspTransaction = FRSP_NORSP;
    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// RECV

uint32_t SkFlowProto::recvStartOfTransaction(SkFlowCommand &cmd)
{
    if (currentRecvCmdTransaction != FCMD_NOCMD)
        ObjectError("Another Cmd transaction is open: " << commandToString(currentRecvCmdTransaction));

    uint32_t transactionSz = 0;//sck->readUInt32();
    cmd = static_cast<SkFlowCommand>(sck->readUInt16());
    currentRecvCmdTransaction = cmd;
    return transactionSz-sizeof(uint16_t);
}

uint32_t SkFlowProto::recvStartOfTransaction(SkFlowResponse &rsp)
{
    if (currentRecvRspTransaction != FRSP_NORSP)
        ObjectError("Another Rsp transaction is open: " << responseToString(currentRecvRspTransaction));

    uint32_t transactionSz = 0;//sck->readUInt32();
    rsp = static_cast<SkFlowResponse>(sck->readUInt16());
    currentRecvRspTransaction = rsp;
    return transactionSz-sizeof(uint16_t);
}

void SkFlowProto::recvChanType(SkFlowChannel_T &t)
{
    t = static_cast<SkFlowChannel_T>(sck->readUInt8());
}

void SkFlowProto::recvFlowType(SkFlow_T &t)
{
    t = static_cast<SkFlow_T>(sck->readUInt16());
}

void SkFlowProto::recvVariantType(SkVariant_T &t)
{
    t = static_cast<SkVariant_T>(sck->readUInt16());
}

void SkFlowProto::recvChanID(SkFlowChanID &chanID)
{
    uint64_t sz = sizeof(SkFlowChanID);
    sck->read(SkArrayCast::toChar(&chanID), sz);
}

void SkFlowProto::recvSize(uint64_t &sz)
{
    //IT IS RIGHT TO BE 32
    sz = sck->readUInt32();
}

bool SkFlowProto::recvBool()
{
    return sck->readUInt8();
}

void SkFlowProto::recvInt8(int8_t &val)
{
    val = sck->readInt8();
}

void SkFlowProto::recvUInt8(uint8_t &val)
{
    val = sck->readUInt8();
}

void SkFlowProto::recvInt16(Short &val)
{
    val = sck->readInt16();
}

void SkFlowProto::recvUInt16(UShort &val)
{
    val = sck->readUInt16();
}

void SkFlowProto::recvInt32(Int &val)
{
    val = sck->readInt32();
}

void SkFlowProto::recvUInt32(UInt &val)
{
    val = sck->readUInt32();
}

void SkFlowProto::recvInt64(Long &val)
{
    val = sck->readInt64();
}

void SkFlowProto::recvUInt64(ULong &val)
{
    val = sck->readUInt64();
}

void SkFlowProto::recvFloat(float &val)
{
    val = sck->readFloat();
}

void SkFlowProto::recvDouble(double &val)
{
    val = sck->readDouble();
}

bool SkFlowProto::recvString(SkString &s, uint64_t &tempSize)
{
    if (tempSize)
    {
        sck->read(s, tempSize);
        return true;
    }

    return false;
}

bool SkFlowProto::recvJSON(SkVariant &val, uint64_t &tempSize)
{
    SkString s;

    if (!recvString(s, tempSize))
        return false;

    //val = s;
    return val.fromJson(s.c_str());
}

bool SkFlowProto::recvVariant(SkVariant &val, uint64_t &tempSize)
{
    SkVariant_T t;
    recvVariantType(t);

    if (tempSize)
    {
        SkDataBuffer b;

        if (!recvBuffer(b, tempSize))
            return false;

        val.setVal(b.toVoid(), tempSize, t);
    }

    return true;
}

bool SkFlowProto::recvBuffer(SkDataBuffer &b, uint64_t &tempSize)
{
    if (tempSize)
    {
        sck->read(&b, tempSize);
        return true;
    }

    return false;
}

bool SkFlowProto::recvList(SkStringList &l)
{
    uint64_t sz=0;

    do
    {
        recvSize(sz);

        if (sz)
        {
            SkString item;

            if (!recvString(item, sz))
                return false;

            l << item;
        }

    } while(sz>0);

    return true;
}

bool SkFlowProto::recvEndOfTransaction()
{
    uint32_t ret = sck->readUInt32();

    //MUST BE 0 TO BE OK
    if (ret)
    {
        ObjectError("FAILED to RECV EndOfTransaction [CurrentRsp: " << responseToString(currentRecvRspTransaction) << ", ret: "<< ret << "!=0]");
        return false;
    }

    currentRecvCmdTransaction = FCMD_NOCMD;
    currentRecvRspTransaction = FRSP_NORSP;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *SkFlowProto::commandToString(SkFlowCommand cmd)
{
    if (cmd > SkFlowCommand::FCMD_QUIT)
        return nullptr;

    //return txtCommands[cmd];
    return getTxtCommands()[cmd];
}

SkFlowCommand SkFlowProto::commandToBin(CStr *cmd)
{
    if (getCmdTxtToBin().contains(cmd))
        return FCMD_NOCMD;

    //return cmdTxtToBin[cmd];
    return getCmdTxtToBin()[cmd];
}

CStr *SkFlowProto::responseToString(SkFlowResponse rsp)
{
    if (rsp > SkFlowResponse::FRSP_KO)
        return nullptr;

    //return txtResponses[rsp];
    return getTxtResponses()[rsp];
}

SkFlowResponse SkFlowProto::responseToBin(CStr *rsp)
{
    //return rspTxtToBin[rsp];
    return getRspTxtToBin()[rsp];
}

CStr *SkFlowProto::flowTypeToString(SkFlow_T flow_t)
{
    //return txtFlow_t[flow_t];
    return getTxtFlow_t()[flow_t];
}

SkFlow_T SkFlowProto::flowTypeToBin(CStr *flow_t)
{
    //return flowTxtToBin[flow_t];
    return getFlowTxtToBin()[flow_t];
}
