#ifndef SKFLOWREDISTRCHANNEL_H
#define SKFLOWREDISTRCHANNEL_H

#include "skabstractflowchannel.h"

class SkDeviceRedistr;

#if defined(ENABLE_HTTP)
    class SkWsRedistr;
#endif

class SkFlowRedistrChannel extends SkAbstractFlowChannel
{
    //SkString u;
    SkVariant_T data_t;
    SkFlow_T flow_t;
    SkVariant min;
    SkVariant max;
    SkDataBuffer hdr;
    SkDataBuffer lastData;
    SkString mm;

    ULong attachedChannelsCount;

    bool publishingEnabled;
    bool publishingAsReplicant;

    SkDeviceRedistr *r;

#if defined(ENABLE_HTTP)
    SkWsRedistr *rWs;
#endif

    SkSet<SkAbstractFlowConnection *> grbs;

    SkSet<SkFlowRedistrChannel *> targets;
    SkFlowRedistrChannel *source;

    public:
        Constructor(SkFlowRedistrChannel, SkAbstractFlowChannel);

        bool setup(SkFlowChanID id, CStr *name, SkFlow_T flowType, SkVariant_T dataType, CStr *mime=nullptr);

        bool replicate(SkFlowAsync *node)               override;

        void setChannelValueLimits(const SkVariant &minimum, const SkVariant &maximum);
        void setReplicantPublishingEnabled(bool enable);

        bool attach(SkFlowRedistrChannel *ch);
        bool detach(SkFlowRedistrChannel *ch);

        void checkPublishState();

        bool addSubscriber(SkAbstractFlowConnection *subscriber);
        bool removeSubscriber(SkAbstractFlowConnection *subscriber);

        bool addGrabber(SkAbstractFlowConnection *grabber);
        bool removeGrabber(SkAbstractFlowConnection *grabber);

        bool publish();
        bool publish(int8_t v);
        bool publish(uint8_t v);
        bool publish(Short v);
        bool publish(UShort v);
        bool publish(Int v);
        bool publish(UInt v);
        bool publish(Long v);
        bool publish(ULong v);
        bool publish(float v);
        bool publish(double v);
        bool publish(CStr *str);
        bool publish(SkString &str);
        bool publish(SkArgsMap &m);

        //IT IS GENERIC PUBLISH
        void publish(CVoid *data, uint32_t sz);

        bool isReplicantPublishingEnabled();
        bool isPublishingEnabled();
        bool hasSubscribers();
        ULong subscribersCount();
        bool hasGrabbers();
        ULong grabbersCount();
        SkFlow_T flowType();
        SkVariant_T dataType();
        CStr *mime();
        SkDataBuffer &header();
        SkDataBuffer &data();
        SkVariant &minimum();
        SkVariant &maximum();

        Signal(dataPublished);
        Slot(onSourcePublish);

        Slot(onFastTick);

    private:
        void onSetPairDatabase()                            override;
        void onDisable()                                    override;
};

#endif // SKFLOWREDISTRCHANNEL_H
