#ifndef SKABSTRACTFLOWSAT_H
#define SKABSTRACTFLOWSAT_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/App/skapp.h>
#include <Core/Object/skabstractworkerobject.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>
#include "Core/System/Network/FlowNetwork/skabstractflowpublisher.h"
#include "Core/System/Network/FlowNetwork/skabstractflowsubscriber.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)
    #include <Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.h>

    struct SkFlowHttpServiceConfig
    {
        bool serviceEnabled;

        SkString listenAddr;
        UShort listenPort;

        bool wwwDirEnabled;
        SkString wwwDir;

        bool sslEnabled;
        SkString sslCertFile;
        SkString sslKeyFile;

        UShort idleTimeoutInterval;
        UShort maxConnQueued;
        ULong maxHeaderSize;

        SkFlowHttpServiceConfig()
        {
            serviceEnabled = false;

            listenAddr = "0.0.0.0";
            listenPort = 0;

            wwwDirEnabled = false;
            sslEnabled = false;

            idleTimeoutInterval = 20;
            maxConnQueued = 10;
            maxHeaderSize = 1000000;
        }
    };
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractFlowSat extends SkAbstractWorkerObject
{
    SkFlowAsync *async;

    SkString userName;
    SkString userPasswd;

#if defined(ENABLE_HTTP)
    SkFlowHttpServiceConfig httpCfg;
    SkFlowAsyncHttpService *http;
#endif

    SkList<SkAbstractFlowSubscriber *> subscribersHolder_ASYNC;
    SkList<SkAbstractFlowPublisher *> publishersHolder_ASYNC;

#if defined(ENABLE_HTTP)
    SkList<SkAbstractFlowPublisher *> publishersHolder_HTTP;
#endif

    SkString workingDirectory;

    SkFlowChanID tickChan;
    bool tickChanEnabled;

    SkElapsedTime checkChrono;

    public:
        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

        Slot(tickParamsChanged);

        Slot(channelAdded);
        Slot(channelRemoved);
        Slot(channelHeaderSetup);

        Slot(channelPublishStartRequest);
        Slot(channelPublishStopRequest);

#if defined(ENABLE_HTTP)
        Slot(channelRedistrStartRequest);
        Slot(channelRedistrStopRequest);
#endif

        Slot(onFlowDisconnection);
        Signal(flowDisconnected);

    protected:
        AbstractConstructor(SkAbstractFlowSat, SkAbstractWorkerObject);

        void setWorkingDir(CStr *dirPath);

        void setLogin(CStr *user, CStr *password);

        bool buildASync(CStr *url=nullptr);
        void closeASync();

#if defined(ENABLE_HTTP)
        void setupHttpService(SkFlowHttpServiceConfig &cfg);
        void buildHttpService();
        void closeHttp();
#endif
        SkFlowSync *buildSyncClient();
        SkFlowAsync *buildOptAsyncClient();

        bool isConnected();

        bool setCurrentDbName(CStr *dbName);
        bool setVariable(CStr *key, const SkVariant &value, CStr *temporaryCurrentDbName="");
        bool delVariable(CStr *key, CStr *temporaryCurrentDbName="");
        bool flushall(CStr *temporaryCurrentDbName="");

        bool addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime=nullptr, SkArgsMap *props=nullptr);
        bool removeChannel(SkFlowChanID chanID);
        bool setChannelHeader(SkFlowChanID chanID, SkDataBuffer &data);
        bool setChannelProperties(SkFlowChanID chanID, SkArgsMap &props);

        bool publish(SkFlowChanID chanID, int8_t v);
        bool publish(SkFlowChanID chanID, uint8_t v);
        bool publish(SkFlowChanID chanID, int16_t v);
        bool publish(SkFlowChanID chanID, uint16_t v);
        bool publish(SkFlowChanID chanID, int32_t v);
        bool publish(SkFlowChanID chanID, uint32_t v);
        bool publish(SkFlowChanID chanID, int64_t v);
        bool publish(SkFlowChanID chanID, uint64_t v);
        bool publish(SkFlowChanID chanID, float v);
        bool publish(SkFlowChanID chanID, double v);
        bool publish(SkFlowChanID chanID, CStr *str);
        bool publish(SkFlowChanID chanID, SkString &str);
        bool publish(SkFlowChanID chanID, SkVariantList &l);
        bool publish(SkFlowChanID chanID, SkArgsMap &m);
        bool publish(SkFlowChanID chanID, CVoid *data, uint64_t sz);

#if defined(ENABLE_HTTP)
        bool httpRedistr(SkFlowChanID chanID, SkDataBuffer &data);
        bool httpRedistr(SkFlowChanID chanID, CVoid *data, uint64_t sz);
#endif

        bool isSubscribed(SkFlowChanID chanID);
        bool subscribeChannel(SkFlowChannel *ch);
        bool subscribeChannel(CStr *name, SkFlowChanID &chanID);
        bool unsubscribeChannel(SkFlowChanID chanID);

        void setupSubscriber(SkAbstractFlowSubscriber *subscriber);
        void setupPublisher(SkAbstractFlowPublisher *publisher);

        bool containsChannel(CStr *name);
        bool containsChannel(SkFlowChanID chanID);
        void channels(SkStringList &names);
        SkFlowChannel *channel(CStr *name);
        SkFlowChannel *channel(SkFlowChanID chanID);
        int64_t channelID(CStr *name);
        uint64_t channelsCount();
        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *channelsIterator();

        CStr *workingDir();

        virtual bool onSetup()                                      {return true;}

        virtual void onInit()                                       {}
        virtual void onQuit()                                       {}

        virtual void onFastTick()                                   {}
        virtual void onSlowTick()                                   {}
        virtual void onOneSecTick()                                 {}
        virtual void onCheckInternals()                             {}
        virtual void onTickParamsChanged()                          {}

        virtual void onFlowDataCome(SkFlowChannelData &)            {}

        virtual void onChannelAdded(SkFlowChanID)                   {}
        virtual void onChannelRemoved(SkFlowChanID)                 {}
        virtual void onChannelHeaderSetup(SkFlowChanID)             {}

        virtual void onChannelPublishStartRequest(SkFlowChanID)     {}
        virtual void onChannelPublishStopRequest(SkFlowChanID)      {}

#if defined(ENABLE_HTTP)
        virtual void onChannelRedistrStartRequest(SkFlowChanID)     {}
        virtual void onChannelRedistrStopRequest(SkFlowChanID)      {}
#endif

        void updateCommand(SkWorkerCommand *cmd)                    override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKABSTRACTFLOWSAT_H
