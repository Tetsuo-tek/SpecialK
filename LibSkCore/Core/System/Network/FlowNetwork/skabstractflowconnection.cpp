#include "skabstractflowconnection.h"
#include "skabstractflowauthenticator.h"
#include "skflowserver.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractFlowConnection, SkObject)
{
    auth = nullptr;
    flowAccount = nullptr;
    svr = nullptr;
    currentDatabase = nullptr;
    authorized = false;

    SignalSet(disabled);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowConnection::setAuthenticator(SkAbstractFlowAuthenticator *authenticator)
{
    auth = authenticator;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*void SkAbstractFlowConnection::abortPendingRequest(CStr *hashStr)
{
    if (!requestsToChannels.contains(hashStr))
    {
        ObjectError("Request NOT found: " << hashStr);
        return;
    }

    requestsToChannels.remove(hashStr);
    ObjectError("Request ABORTED: " << hashStr);
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*SkSet<SkFlowChanID> &SkAbstractFlowConnection::getOwnedChannels()
{
    return ownedChannels;
}

SkSet<SkFlowChanID> &SkAbstractFlowConnection::getRegisteredGrabChannels()
{
    return registeredGrabChannels;
}*/

CStr *SkAbstractFlowConnection::seed()
{
    return lastSeed.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *SkAbstractFlowConnection::name()
{
    return loginName.c_str();
}

CStr *SkAbstractFlowConnection::token()
{
    return loginToken.c_str();
}

bool SkAbstractFlowConnection::authorize()
{
    authorized = false;

    if (!auth)
    {
        authorized = true;
        ObjectWarning("NOT using Authentication; ALL IS PERMITTED!");
        onLogin();
    }

    else if (auth->authorize(this))
    {
        flowAccount = auth->account(loginName.c_str());
        authorized = true;
        lastSeed.clear();
        onLogin();
    }

    return authorized;
}

void SkAbstractFlowConnection::disable()
{
    auth->remove(this);

    if (svr->existsOptionalPairDb(name()))
        svr->delPairDatabase(name());

    if (!ownedDatabases.isEmpty())
    {
        ObjectWarning("CLEARING owned pair-databases [" << ownedDatabases.count() << " elements]");
        SkBinaryTreeVisit<SkString, nullptr_t> *itr = ownedDatabases.iterator();

        while(itr->next())
        {
            SkString &dbName = itr->item().key();
            svr->delPairDatabase(dbName.c_str());
        }

        delete itr;
        ownedDatabases.clear();
    }

    if (!subscribedChannels.isEmpty())
    {
        ObjectWarning("CLEARING streaming-channel subscribes [" << subscribedChannels.count() << " elements]");
        SkBinaryTreeVisit<SkFlowChanID, nullptr_t> *itr = subscribedChannels.iterator();

        while(itr->next())
        {
            SkFlowChanID chanID = itr->item().key();

            if (!svr->isChannelValid(chanID))
                continue;

            SkAbstractFlowChannel *absCh = svr->getChannels()[chanID];

            if (absCh->chanType() == StreamingChannel)
            {
                SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
                ch->removeSubscriber(this);
            }

            else if (absCh->chanType() == BlobChannel)
            {
                /*SkFlowBlobChannel *ch = dynamic_cast<SkFlowBlobChannel *>(absCh);
                ch->removeSubscriber(this);*/
            }
        }

        delete itr;
        subscribedChannels.clear();
    }

    if (!registeredGrabChannels.isEmpty())
    {
        ObjectWarning("CLEARING streaming-channel grabber-registrations [" << registeredGrabChannels.count() << " elements]");
        SkBinaryTreeVisit<SkFlowChanID, nullptr_t> *itr = registeredGrabChannels.iterator();

        while(itr->next())
        {
            SkFlowChanID chanID = itr->item().key();

            if (!svr->isChannelValid(chanID))
                continue;

            SkAbstractFlowChannel *absCh = svr->getChannels()[chanID];

            if (absCh)
            {
                if (absCh->chanType() == StreamingChannel)
                {
                    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
                    ch->removeGrabber(this);
                }
            }
        }

        delete itr;
        registeredGrabChannels.clear();
    }

    if (!ownedChannels.isEmpty())
    {
        ObjectWarning("CLEARING owned channels [" << ownedChannels.count() << " elements]");
        SkBinaryTreeVisit<SkFlowChanID, nullptr_t> *itr = ownedChannels.iterator();

        while(itr->next())
        {
            SkFlowChanID chanID = itr->item().key();

            if (!svr->isChannelValid(chanID))
                continue;

            svr->removeChannel(chanID);
        }

        delete itr;
        ownedChannels.clear();
    }

    onDisable();
    disabled();
}

bool SkAbstractFlowConnection::isAuthorized()
{
    return authorized;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
