#ifndef SKFLOWRAWSVRCONNECTION_H
#define SKFLOWRAWSVRCONNECTION_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skabstractflowconnection.h"
#include "Core/System/Time/skelapsedtime.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowRawSvrConnection extends SkAbstractFlowConnection
{
    SkFlowProto *p;

    SkString connID;
    SkAbstractSocket *sck;
    bool asynchrounous;

    SkFlowCommand prevBinCmd;
    SkFlowCommand lastBinCmd;
    uint32_t lastTransactionSize;
    SkFlowChanID lastChanID;
    uint64_t lastSize;
    SkString lastString;
    SkDataBuffer lastBuffer;
    SkVariant_T lastVariantType;

    //SkTreeMap<SkString, SkFlowChanID> requestsToChannels;

    SkElapsedTime checkChrono;

    public:
        Constructor(SkFlowRawSvrConnection, SkAbstractFlowConnection);

        void setup(SkFlowServer *server, SkAbstractSocket *socket);

        SkAbstractDevice *getSck()                              override;

        bool isAsynchronous();
        SkFlowProto *proto();

        Slot(onSocketReadyRead);
        Slot(onChannelAdded);
        Slot(onChannelRemoved);
        Slot(onChannelHeaderSetup);
        Slot(onFastTick);
        Slot(onOneSecTick);

        void notifyChannelReqPublishStart(SkFlowChanID chanID)  override;//ASYNC
        void notifyChannelReqPublishStop(SkFlowChanID chanID)   override;//ASYNC

        CStr *connectionID();  //name(sck-fd) - ONLY IF AUTHORIZED, OTHERWISE IS EMPTY

    private:
        void notifyAddedChannel(SkFlowChanID chanID)            override;//ASYNC
        void notifyRemovedChannel(SkFlowChanID chanID)          override;//ASYNC
        void notifyChannelHeader(SkFlowChanID chanID)           override;//ASYNC

        void onLogin()                                          override;
        void onDisable()                                        override;

        void analyze();
        void resetCommand();

        void sendLoginUUID();
        void recvLogin();

        void setASync();
        void recvCheckService();

        void sendChannelsList();//SYNC
        void sendChannelProperties();//SYNC

        void sendChannelHeader();//SYNC
        void recvChannelHeader();

        void existsDatabase();
        void addDatabase();
        void removeDatabase();
        void loadDatabase();
        void saveDatabase();
        void existsDatabaseProperty();
        void recvDatabaseProperty();
        void recvDatabasePropertyJson();
        void sendDatabaseProperty();
        void sendDatabasePropertyJson();
        void removeDatabaseProperty();
        void setCurrentDatabase();
        void sendCurrentDatabase();
        void sendVariablesKeys();
        void sendAllVariables();
        void sendAllVariablesJson();
        void existsVariable();
        void recvVariable();
        void recvVariableJson();
        void sendVariable();
        void sendVariableJson();
        void removeVariable();
        void flushall();

        void addStreamingChannel();
        void addServiceChannel();
        void addBlobChannel();

        void removeChannel();

        void attachChannel();
        void detachChannel();

        void acceptRequestForService();
        void grabResponseFromService();

        void publish();

        void registerOnGrabChanData();
        void unregisterOnGrabChanData();
        void sendLastChanData();

        void subscribe();
        void unsubscribe();

        void addUser();
        void removeUser();
        void setUserPermission();
        void killUser();
        void killConnection();

        void quitConnection();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLOWRAWSVRCONNECTION_H
