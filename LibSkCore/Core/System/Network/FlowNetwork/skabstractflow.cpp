#include "skabstractflow.h"
#include "skflowsync.h"
#include <Core/Containers/skarraycast.h>
#include "Core/System/Network/LOCAL/sklocalsocket.h"
#include "Core/System/Network/TCP/sktcpsocket.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowClose(SkObject *obj)
{
    SkAbstractFlow *f = static_cast<SkAbstractFlow *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkAbstractFlowClose()");

    if (f->channelsCount() > 0)
        f->resetChannels();

    if (f->isConnected())
        f->close();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractFlow, SkObject)
{
    p = nullptr;
    socket = nullptr;
    tcpPort = 0;
    auth = false;

    SignalSet(connected);
    SignalSet(disconnected);
    SlotSet(onDisconnect);

    addDtorCompanion(SkAbstractFlowClose);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlow::setNodeName(CStr *name)
{
    nodeName = name;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::localConnect(CStr *path)
{
    if (socket && socket->isConnected())
    {
        ObjectError("Socket is ALREADY connected: " << socket);
        return false;
    }

    SkLocalSocket *sck = new SkLocalSocket(this);
    sck->setObjectName(this, "LocalSocket");

    if (!sck->connect(path))
        return false;

    socket = sck;
    unixPath = path;
    tcpAddress.clear();
    tcpPort = 0;

    onOpenUnixSocket();
    return open();
}

bool SkAbstractFlow::tcpConnect(CStr *address, UShort port)
{
    if (socket && socket->isConnected())
    {
        ObjectError("Socket is ALREADY connected");
        return false;
    }

    SkTcpSocket *sck = new SkTcpSocket(this);
    sck->setObjectName(this, "TcpSocket");

    if (!sck->connect(address, port))
        return false;

    socket = sck;
    unixPath.clear();
    tcpAddress = address;
    tcpPort = port;

    //getLoginSeed();
    onOpenTcpSocket();
    return open();
}

bool SkAbstractFlow::connect(CStr *flowUrl)
{
    SkString addr(flowUrl);

    if (addr.startsWith("local:"))
    {
        addr = &addr.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address: " << addr);
        return localConnect(addr.c_str());
    }

    else if (addr.startsWith("tcp:"))
    {
        addr = &addr.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address: " << addr);

        SkStringList urlParsed;
        addr.split(":", urlParsed);
        AssertKiller(urlParsed.count() < 1 || urlParsed.count() > 2);

        int port = 9000;

        if (urlParsed.count() == 2)
            port = urlParsed.last().toInt();

        return tcpConnect(urlParsed.first().c_str(), port);
    }

    ObjectError("Robot address is MALFORMED (syntax MUST BE <local|tcp>:<address[:tcpPort]> -> " << addr);
    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::open(SkAbstractSocket *sck)
{
    if (socket)
    {
        socket->destroyLater();
        socket = nullptr;
        auth = false;
    }

    socket = sck;
    return open();
}

bool SkAbstractFlow::open()
{
    if (channelsCount() > 0)
        resetChannels();

    p = new SkFlowProto(this);
    p->setObjectName(this, "Proto");
    p->setup(socket);

    onOpen();
    connected();

    Attach(socket, disconnected, this, onDisconnect, SkAttachMode::SkDirect);
    ObjectWarning("Connected");
    return true;
}

void SkAbstractFlow::close()
{
    if (!socket)
    {
        ObjectError("Socket is NOT connected yet");
        return;
    }

    if (!socket->isConnected())
        return;

    ObjectWarning("Closing ..");

    if (socket->isConnected())
        socket->close();

    onClose();

    socket->destroyLater();
    socket = nullptr;

    p->destroyLater();
    p = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractFlow, onDisconnect)
{
    SilentSlotArgsWarning();
    ObjectMessage("Disconnected");
    onDisconnected();
    disconnected();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::getLoginSeed()
{
    p->sendStartOfTransaction(FCMD_GET_LOGIN_SEED);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz;
    p->recvSize(sz);

    if (sz && !p->recvString(lastSeed, sz))
    {
        ProtocolRecvError("CANNOT RECV channel->name");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkAbstractFlow::login(CStr *userName, CStr *userToken)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_LOGIN);
    p->sendCStr(userName);

    if (lastSeed.isEmpty())
        p->sendCStr(userToken);

    else
    {
        SkString hashedToken = stringHash(lastSeed, HM_SHA512);
        lastSeed.clear();
        //cout << hashedToken << "\n";

        SkString requestedToken(userToken);
        hashedToken.append(stringHash(requestedToken, HM_SHA512));
        //cout << stringHash(requestedToken, HM_SHA512) << "\n";

        hashedToken = stringHash(hashedToken, HM_SHA512);
        //cout << hashedToken << "\n";

        p->sendString(hashedToken);

    }

    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    name = userName;
    token = userToken;
    auth = true;

    setVariable("@nodeName", nodeName.c_str());

    return onLogin();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::setCurrentDbName(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }
    
    p->sendStartOfTransaction(FCMD_SET_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool SkAbstractFlow::setVariable(CStr *key, const SkVariant &value)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    SkVariant v(value);
    p->sendStartOfTransaction(FCMD_SET_VARIABLE);
    p->sendCStr(key);
    p->sendVariant(v);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool SkAbstractFlow::delVariable(CStr *key)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_DEL_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool SkAbstractFlow::flushall()
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_FLUSHALL);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::containsChannel(CStr *name)
{
    return channelsNames.contains(name);
}

bool SkAbstractFlow::containsChannel(SkFlowChanID chanID)
{
    return channelsIndexes.contains(chanID);
}

void SkAbstractFlow::channels(SkStringList &names)
{
    channelsNames.keys(names);
}

SkFlowChannel *SkAbstractFlow::channel(CStr *name)
{
    if (!channelsNames.contains(name))
    {
        ObjectError("ChanName UNKNOWN: " << name);
        return nullptr;
    }

    return channelsNames[name];
}

SkFlowChannel *SkAbstractFlow::channel(SkFlowChanID chanID)
{
    if (!channelsIndexes.contains(chanID))
    {
        ObjectError("ChanID UNKNOWN: " << chanID);
        return nullptr;
    }

    return channelsIndexes[chanID];
}

SkFlowChanID SkAbstractFlow::channelID(CStr *name)
{
    if (!channelsNames.contains(name))
    {
        ObjectError("ChanName UNKNOWN: " << name);
        return -1;
    }

    return channelsNames[name]->chanID;
}

uint64_t SkAbstractFlow::channelsCount()
{
    return channelsNames.count();
}

SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *SkAbstractFlow::channelsIterator()
{
    return channelsIndexes.iterator();
}

void SkAbstractFlow::resetChannels()
{
    SkBinaryTreeVisit<SkString , SkFlowChannel*> *itr = channelsNames.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    channelsNames.clear();
    channelsIndexes.clear();
}

CStr *SkAbstractFlow::userName()
{
    return name.c_str();
}

CStr *SkAbstractFlow::userToken()
{
    return token.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlow::isUnixSocket()
{
    return SkString::compare(socket->typeName(), "SkLocalSocket");
}

bool SkAbstractFlow::isTcpSocket()
{
    return SkString::compare(socket->typeName(), "SkTcpSocket");
}

CStr *SkAbstractFlow::socketTypeName()
{
    return socket->typeName();
}

CStr  *SkAbstractFlow::getUnixPath()
{
    return unixPath.c_str();
}

CStr  *SkAbstractFlow::getTcpAddress()
{
    return tcpAddress.c_str();
}

int16_t SkAbstractFlow::getTcpPort()
{
    return tcpPort;
}

SkAbstractSocket *SkAbstractFlow::sck()
{
    return socket;
}

bool SkAbstractFlow::isConnected()
{
    if (!socket)
        return false;

    return socket->isConnected();
}

bool SkAbstractFlow::isAuthorized()
{
    return auth;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

