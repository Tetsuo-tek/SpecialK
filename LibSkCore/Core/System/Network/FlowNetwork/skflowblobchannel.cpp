#include "skflowblobchannel.h"
#include "skflowrawsvrconnection.h"
#include "skflowpairdatabase.h"

#include "Core/App/skeventloop.h"
#include "Core/System/skosenv.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Filesystem/sktempfile.h"
#include "Core/System/Filesystem/skfileinfoslist.h"

#define BLOB_HASHID_LEN     32

ConstructorImpl(SkFlowBlobChannel, SkAbstractFlowChannel)
{
    chan_t = BlobChannel;

    temporaryFiles = new SkTempFile(this);
    temporaryFiles->open(SkFileMode::FLM_ONLYWRITE);

    coreCommands << "PWD"
                 << "LS"
                 << "CD"
                 << "MV"
                 << "CP"
                 << "RM"
                 << "MKDIR"
                 /*<< "MVDIR"
                 << "CPDIR"*/
                 << "PUT_ALLOC"
                 << "PUT"
                 << "GET";

    SlotSet(onUploadProgress);
    SlotSet(onUploadTerminated);

    SlotSet(onDownloadProgress);
    SlotSet(onDownloadTerminated);
}

bool SkFlowBlobChannel::setup(SkFlowChanID id, CStr *name, CStr *usersHomePath)
{
    if (chanID > -1)
    {
        ObjectError("Channel ALREADY inititialized");
        return false;
    }

    init(id, name);

    usersPath = usersHomePath;
    currentRoot = usersPath;

    return true;
}

void SkFlowBlobChannel::onSetPairDatabase()
{
    db->setVariable("chan_t", "BlobChannel");

    SkStringList l;
    db->setVariable("downloaders", l);
    db->setVariable("downloadersCount", 0);

    db->setVariable("uploaders", l);
    db->setVariable("downloadersCount", 0);
}

bool SkFlowBlobChannel::replicate(SkFlowAsync */*node*/)
{
    /*nodeConnection = node;

    SkString chName(node->userName());
    chName.append(".");
    chName.append(n);

    return node->addBlobChannel(replicatedChanID, ..);*/

    return false;
}

void SkFlowBlobChannel::onDisable()
{
    temporaryFiles->close();
    temporaryFiles->open(SkFileMode::FLM_ONLYREAD);
    ULong rawLen = 0;

    while(temporaryFiles->canReadLine(rawLen))
    {
        SkString filePath;

        temporaryFiles->read(filePath, rawLen);
        filePath.trim();

        if (!SkFsUtils::exists(filePath.c_str()))
        {
            ObjectWarning("Blob temporary item NOT found");
            continue;
        }

        SkFsUtils::rm(filePath.c_str());
        ObjectWarning("Temporary file REMOVED: " << filePath);
    }

    temporaryFiles->close();
}

bool SkFlowBlobChannel::isCoreCommand(CStr *cmd)
{
    return coreCommands.contains(cmd);
}

bool SkFlowBlobChannel::onCoreCommand(CStr *cmd, SkVariant &value, SkFlowServiceRequestStatus &st)
{
    bool isOwner = (st.requester() == owner());

    SkArgsMap cmdMap;
    value.copyToMap(cmdMap);

    SkVariantVector args;
    cmdMap["args"].copyToList(args);

    // Permission are going into EXPERIMENTAL-STEPs
    if (isOwner)
    {
        if (SkString::compare(cmd, "CD"))
            return changeRootPath(st, args);

        else if (SkString::compare(cmd, "PWD"))
            return currentRootPath(st, args);

        else if (SkString::compare(cmd, "LS"))
            return listDirContent(st, args);

        else if (SkString::compare(cmd, "CP"))
            return copyItem(st, args);

        else if (SkString::compare(cmd, "MV"))
            return copyItem(st, args);

        else if (SkString::compare(cmd, "RM"))
            return removeItem(st, args);

        else if (SkString::compare(cmd, "MKDIR"))
            return makeDir(st, args);

        /*else if (SkString::compare(cmd, "MVDIR"))
        {}

        else if (SkString::compare(cmd, "CPDIR"))
        {}*/

        else if (SkString::compare(cmd, "RMDIR"))
            return removeItem(st, args);

        else if (SkString::compare(cmd, "PUT_ALLOC"))
            return putAlloc(st, args);

        else if (SkString::compare(cmd, "PUT"))
            return put(st, args);
    }

    //
    // Permissions related to following operations will be checked by backend,
    // BUT THESE ARE NOT IMPLEMENTED YET
    //

    if (SkString::compare(cmd, "GET"))
        return get(st, args);

    closeCoreRequest(st, false, "Command NOT recognized");
    return false;
}

SkString SkFlowBlobChannel::buildAbsRealPath(CStr *userPath)
{
    SkString tempPath = userPath;

    if (tempPath.startsWith("./"))
        tempPath = &tempPath.c_str()[2];

    SkString p;

    if (tempPath.startsWith("$TEMP/"))
        p = tempPath.replace("$TEMP/", osEnv()->getSystemTempPath().c_str(), 0, true);

    else if (tempPath.startsWith("/"))
    {
        p = usersPath;
        p.append(&tempPath.c_str()[1]);
    }

    else
    {
        p = currentRoot;
        p.append(tempPath);
    }

    return p;
}

SkString SkFlowBlobChannel::buildAbsUserPath(CStr *realPath)
{
    SkString tempPath = realPath;
    SkString p;

    if (tempPath.startsWith(osEnv()->getSystemTempPath().c_str()))
    {
        p = "$TEMP/";
        p.append(SkFsUtils::relativePath(osEnv()->getSystemTempPath().c_str(), realPath));
    }

    else
    {
        p = "/";
        p.append(SkFsUtils::relativePath(usersPath.c_str(), realPath));
    }

    return p;
}

bool SkFlowBlobChannel::currentRootPath(SkFlowServiceRequestStatus &st, SkVariantVector &)
{
    SkString pwd = buildAbsUserPath(currentRoot.c_str());
    closeCoreRequest(st, true, "", pwd);
    return true;
}

bool SkFlowBlobChannel::changeRootPath(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() == 0)
    {
        currentRoot = usersPath;
        closeCoreRequest(st, true, "OK", "/");
        return true;
    }

    SkString tempPath = args.first().toString();
    SkString path = buildAbsRealPath(tempPath.c_str());

    ObjectMessage("CHECKING new Blob root-path: " << path);

    SkFileInfo info;
    SkFsUtils::fillFileInfo(path.c_str(), info);

    if (!info.exists || !info.isDir)
    {
        ObjectMessage("Blob root-path is NOT valid: " << path);
        closeCoreRequest(st, false, "New root-path is NOT valid", path);
        return false;
    }

    currentRoot = SkFsUtils::adjustPathEndSeparator(info.completeAbsolutePath.c_str());

    ObjectMessage("Blob root-path is CHANGED: " << currentRoot);
    SkString p = buildAbsUserPath(currentRoot.c_str());
    closeCoreRequest(st, true, "OK", p);
    return true;
}

bool SkFlowBlobChannel::listDirContent(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    SkString path;

    if (args.count() == 0)
        path = currentRoot;

    else
    {
        SkString tempPath = args[0].toString();
        path = buildAbsRealPath(tempPath.c_str());

        SkFileInfo info;
        SkFsUtils::fillFileInfo(path.c_str(), info);

        if (!info.exists)
        {
            closeCoreRequest(st, false, "Path is NOT valid", tempPath);
            return false;
        }

        if (!info.exists)
        {
            closeCoreRequest(st, false, "Path NOT found", tempPath);
            return false;
        }

        if (info.isDir)
            path = info.completeAbsolutePath;

        else
        {
            closeCoreRequest(st, false, "Path is NOT a directory", tempPath);
            return false;
        }
    }

    SkFileInfosList *infoList = new SkFileInfosList(this);

    if (!SkFsUtils::ls(path.c_str(), infoList, false) || !infoList->open())
    {
        closeCoreRequest(st, false, "Internal ERROR");

        if (infoList->isOpen())
            infoList->close();

        infoList->destroyLater();
        return false;
    }

    if (infoList->isEmpty())
    {
        SkString p = buildAbsUserPath(path.c_str());
        closeCoreRequest(st, false, "Directory has NOT contents", p);
        infoList->close();
        infoList->destroyLater();
        return true;
    }

    SkVariantList l;

    while(infoList->next())
    {
        SkString p = buildAbsUserPath(infoList->currentPath().c_str());
        l << p;
    }

    closeCoreRequest(st, true, "OK", l);
    infoList->close();
    infoList->destroyLater();
    return true;
}

bool SkFlowBlobChannel::makeDir(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() == 0)
    {
        closeCoreRequest(st, false, "Directory name is required");
        return false;
    }

    SkString path = currentRoot;
    path.append(args.first().toString());

    SkFileInfo info;
    SkFsUtils::fillFileInfo(path.c_str(), info);

    if (info.exists)
    {
        closeCoreRequest(st, false, "Path ALREADY exists", path);
        return false;
    }

    if (!SkFsUtils::mkdir(info.completeAbsolutePath.c_str()))
    {
        closeCoreRequest(st, false, "Internal ERROR");
        return false;
    }

    closeCoreRequest(st, true, "OK");
    return true;
}

bool SkFlowBlobChannel::copyItem(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() < 2)
    {
        closeCoreRequest(st, false, "Source and target files are required");
        return false;
    }

    SkString tempSource = args[0].toString();
    SkString source = buildAbsRealPath(tempSource.c_str());//currentRoot;

    SkFileInfo info;
    SkFsUtils::fillFileInfo(source.c_str(), info);

    if (!info.exists)
    {
        closeCoreRequest(st, false, "Source NOT found", tempSource);
        return false;
    }

    if (info.isDir)
    {
        closeCoreRequest(st, false, "Source is a directory", tempSource);
        return false;
    }

    source = info.completeAbsolutePath;
    SkString tempSourceName = info.completeName;

    SkString tempTarget = args[1].toString();
    SkString target  = buildAbsRealPath(tempTarget.c_str());;

    SkFsUtils::fillFileInfo(target.c_str(), info);

    if (info.isDir)
    {
        closeCoreRequest(st, false, "Target is a directory", tempTarget);
        info.completeAbsolutePath = SkFsUtils::adjustPathEndSeparator(info.completeAbsolutePath.c_str());
        info.completeAbsolutePath.append(tempSourceName);
        SkFsUtils::fillFileInfo(info.completeAbsolutePath.c_str(), info);
    }

    if (info.exists)
    {
        closeCoreRequest(st, false, "Target ALREADY exists", tempTarget);
        return false;
    }

    target = info.completeAbsolutePath;

    bool ok = false;

    if (SkString::compare(st.command(), "MV"))
        ok = SkFsUtils::mv(source.c_str(), target.c_str());

    else if (SkString::compare(st.command(), "CP"))
        ok = SkFsUtils::cp(source.c_str(), target.c_str());

    if (!ok)
    {
        closeCoreRequest(st, false, "Internal ERROR");
        return false;
    }

    SkString p1 = buildAbsUserPath(source.c_str());
    SkString p2 = buildAbsUserPath(target.c_str());

    SkVariantList l;
    l << p1 << p2;

    closeCoreRequest(st, true, "OK", l);
    return true;
}

bool SkFlowBlobChannel::removeItem(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() == 0)
    {
        closeCoreRequest(st, false, "File name is required");
        return false;
    }

    SkString path = currentRoot;
    path.append(args.first().toString());

    SkFileInfo info;
    SkFsUtils::fillFileInfo(path.c_str(), info);

    if (!info.exists)
    {
        closeCoreRequest(st, false, "Path NOT exists", path);
        return false;
    }

    bool isRmDir = (SkString::compare(st.command(), "RMDIR"));

    if (!isRmDir && info.isDir)
    {
        closeCoreRequest(st, false, "Path is a directory", path);
        return false;
    }

    if (isRmDir && !info.isDir)
    {
        closeCoreRequest(st, false, "Path is NOT a directory", path);
        return false;
    }

    bool ok = false;

    if (isRmDir)
        ok = SkFsUtils::rmdir(info.completeAbsolutePath.c_str(), true);

    else
        ok = SkFsUtils::rm(info.completeAbsolutePath.c_str());

    if (!ok)
    {
        closeCoreRequest(st, false, "Internal ERROR");
        return false;
    }

    SkString p = buildAbsUserPath(info.completeAbsolutePath.c_str());
    closeCoreRequest(st, true, "OK", p);
    return true;
}

bool SkFlowBlobChannel::putAlloc(SkFlowServiceRequestStatus &st, SkVariantVector &)
{
    SkArgsMap m;
    m["uploadHashID"] = st.hashID();

    if (uploadersAllocated.contains(st.hashID()))
    {
        closeCoreRequest(st, false, "Your UploaderHashID is ALREADY allocated", st.hashID());
        return false;
    }

    uploadersAllocated[st.hashID()] = st.requester();
    closeCoreRequest(st, true, "Temporary upload STARTED", m);

    ObjectMessage("Uploader-hashID ALLOCATED: " << st.hashID());
    return true;
}

bool SkFlowBlobChannel::put(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() < 4)
    {
        closeCoreRequest(st, false, "Arguments allocated-hashID, file-name, file-size and storing-path are REQUIRED");
        return false;
    }

    SkString hashID = args[0].toString();

    if (!uploadersAllocated.contains(hashID.c_str()))
    {
        closeCoreRequest(st, false, "Your UploaderHashID is NOT allocated", hashID);
        return false;
    }

    uploadersAllocated.remove(hashID);

    SkString fileName = args[1].toString();
    Long sz = args[2].toInt64();
    SkString targetParentPath = args[3].toString();

    SkString filePath;
    bool isPersistent = !targetParentPath.startsWith("$TEMP");

    if (isPersistent)
    {
        filePath = SkFsUtils::adjustPathEndSeparator(targetParentPath.c_str());
        filePath.append(fileName);

        filePath = buildAbsRealPath(filePath.c_str());
        SkFileInfo info;
        SkFsUtils::fillFileInfo(filePath.c_str(), info);

        if (info.exists)
        {
            SkString p = buildAbsUserPath(info.completeAbsolutePath.c_str());
            closeCoreRequest(st, false, "File ALREADY exists", p);
            return false;
        }

        ObjectWarning("SAVING file WITH-PERSISTENCE [" << sz << " B]: " << filePath);
    }

    else
    {
        filePath = SkFsUtils::adjustPathEndSeparator(osEnv()->getSystemTempPath().c_str());
        filePath.append(fileName);

        ObjectMessage("SAVING file WITHOUT-PERSISTENCE [" << sz << " B]: " << filePath);
    }

    SkString n = "Receiver.";
    n.append(st.requester()->connectionID());

    SkFlowBlobReceiver *recv = new SkFlowBlobReceiver(this);
    recv->setObjectName(this, n.c_str());
    recv->setProperty("persistent", isPersistent);

    if (!recv->start(filePath.c_str(), sz, hashID.c_str(), &st))
    {
        closeCoreRequest(st, false, "CANNOT start the receiver", filePath);
        recv->destroyLater();
        return false;
    }

    uploaders[st.requester()] = recv;
    uploadersByHashID[recv->uploadHashID()] = recv;

    Attach(recv, progress, this, onUploadProgress, SkQueued);
    Attach(recv, terminated, this, onUploadTerminated, SkOneShotQueued);

    ObjectMessage("Blob uploader ADDED [" << st.hashID() << "]: " << filePath);

    return true;
}

bool SkFlowBlobChannel::get(SkFlowServiceRequestStatus &st, SkVariantVector &args)
{
    if (args.count() < 1)
    {
        closeCoreRequest(st, false, "Arguments file-path ro get is REQUIRED");
        return false;
    }

    SkString filePath = args[0].toString();
    filePath = buildAbsRealPath(filePath.c_str());

    SkFileInfo info;
    SkFsUtils::fillFileInfo(filePath.c_str(), info);

    if (!info.exists)
    {
        closeCoreRequest(st, false, "File NOT found", args[0]);
        return false;
    }

    if (!info.isFile)
    {
        closeCoreRequest(st, false, "Item is NOT a file", args[0]);
        return false;
    }

    SkString n = "Sender.";
    n.append(st.requester()->connectionID());

    SkFlowBlobSender *sndr = new SkFlowBlobSender(this);
    sndr->setObjectName(this, n.c_str());

    SkString p = buildAbsUserPath(info.completeAbsolutePath.c_str());

    if (!sndr->start(filePath.c_str(),
                     chanID,
                     st.hashID(),
                     db,
                     st.requester()))
    {
        closeCoreRequest(st, false, "CANNOT start the sender", p);
        sndr->destroyLater();
        return false;
    }

    sndr->setProperty("reqHashID", st.hashID());

    downloaders[st.requester()] = sndr;
    downloadersByHashID[st.hashID()] = sndr;

    Attach(sndr, progress, this, onDownloadProgress, SkQueued);
    Attach(sndr, terminated, this, onDownloadTerminated, SkOneShotQueued);

    ObjectMessage("Blob downloader ADDED [" << st.hashID() << "]: " << filePath);

    SkArgsMap m;
    m["reqHashID"] = st.hashID();
    m["size"] = info.size;
    m["path"] = p;

    closeCoreRequest(st, true, "Download STARTED", m);
    return true;
}

bool SkFlowBlobChannel::publish(CStr *data, UInt sz)
{
    if (sz <= 32)
    {
        ObjectError("CANNOT grab uploading Blob data buffer [size <= 32]");
        return false;
    }

    SkString hashID;
    hashID.append(data, BLOB_HASHID_LEN);

    if (!uploadersByHashID.contains(hashID))
    {
        ObjectError("Blob uploading hashID is NOT valid: " << hashID);
        return false;
    }

    sz -= 32;

    SkFlowBlobReceiver *recv = uploadersByHashID[hashID];
    return recv->write(&data[BLOB_HASHID_LEN], sz);
}

bool SkFlowBlobChannel::hasUploaders()
{
    return !uploaders.isEmpty();
}

ULong SkFlowBlobChannel::uploadersCount()
{
    return uploaders.count();
}

bool SkFlowBlobChannel::hasdownloaders()
{
    return !downloaders.isEmpty();
}

ULong SkFlowBlobChannel::downloadersCount()
{
    return downloaders.count();
}

SlotImpl(SkFlowBlobChannel, onUploadProgress)
{
    SilentSlotArgsWarning();

    ULong sz = Arg_UInt64;
    inputSpeed += sz;
}

SlotImpl(SkFlowBlobChannel, onUploadTerminated)
{
    SilentSlotArgsWarning();

    SkFlowBlobReceiver *recv = DynCast(SkFlowBlobReceiver, referer);
    AssertKiller(!recv);

    bool isPersistent = recv->property("persistent").toBool();

    SkString reqHashID;
    SkFlowRawSvrConnection *uploader = nullptr;

    {
        SkFlowServiceRequestStatus &st = *recv->request();
        reqHashID = st.hashID();
        uploader = st.requester();

        SkString p = buildAbsUserPath(recv->filePath());

        SkArgsMap m;
        m["reqHashID"] = reqHashID;
        m["uploadHashID"] = recv->uploadHashID();
        m["persistent"] = isPersistent;
        m["filePath"] = p;

        if (isPersistent)
            closeCoreRequest(st, true, "Persistent upload CLOSED", m);

        else
        {
            SkString raw = recv->filePath();
            raw.append("\n");
            temporaryFiles->write(raw);
            ObjectWarning("Temporary file INSERTED in the remove list: " << recv->filePath());

            closeCoreRequest(st, true, "Temporary upload CLOSED", m);
        }
    }

    uploaders.remove(uploader);
    uploadersByHashID.remove(recv->uploadHashID());

    ObjectWarning("Uploader REMOVED ["<< reqHashID << "]: " << uploader->connectionID());
    recv->destroyLater();
}

SlotImpl(SkFlowBlobChannel, onDownloadProgress)
{
    SilentSlotArgsWarning();

    ULong sz = Arg_UInt64;
    outputSpeed += sz;
}

SlotImpl(SkFlowBlobChannel, onDownloadTerminated)
{
    SilentSlotArgsWarning();

    SkFlowBlobSender *sndr = DynCast(SkFlowBlobSender, referer);
    AssertKiller(!sndr);

    SkString hashID = sndr->property("reqHashID").toString();

    downloaders.remove(sndr->downloader());
    downloadersByHashID.remove(hashID);

    ObjectWarning("Downloader REMOVED ["<< hashID << "]: " << sndr->downloader()->connectionID());
    sndr->destroyLater();
}

