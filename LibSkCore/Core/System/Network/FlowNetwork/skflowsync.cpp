#include "skflowsync.h"
#include <Core/Containers/skarraycast.h>
#include "Core/System/Network/LOCAL/sklocalsocket.h"
#include "Core/System/Network/TCP/sktcpsocket.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowSync, SkAbstractFlow)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowSync::updateChannels()
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsNames.isEmpty())
        resetChannels();

    SkStringList chans;
    getChannelsList(chans);

    for(uint16_t i=0; i<chans.count(); i++)
    {
        SkFlowChannel *channel = new SkFlowChannel;
        channel->name = chans[i];
        getChannelProperties(channel);
        channelsNames[channel->name] = channel;
        channelsIndexes[channel->chanID] = channel;
    }

    onChannelsChanged();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowSync::getChannelsList(SkStringList &channels)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_CHANS_LIST);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvList(channels))
    {
        ProtocolRecvError("CANNOT RECV channels");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkFlowSync::getChannelProperties(SkFlowChannel *channel)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    channel->isPublishingEnabled = false;

    p->sendStartOfTransaction(FCMD_GET_CHAN_PROPS);
    p->sendString(channel->name);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    p->recvChanType(channel->chan_t);
    p->recvChanID(channel->chanID);

    uint64_t sz;

    p->recvSize(sz);

    if (sz && !p->recvString(channel->hashID, sz))
    {
        ProtocolRecvError("CANNOT RECV channel hashID");
        return false;
    }

    if (channel->chan_t == StreamingChannel)
    {
        p->recvFlowType(channel->flow_t);
        p->recvVariantType(channel->data_t);

        p->recvSize(sz);

        if (sz && !p->recvString(channel->mime, sz))
        {
            ProtocolRecvError("CANNOT RECV props->mime");
            delete channel;
            return false;
        }
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        delete channel;
        return false;
    }

    return true;
}

/*bool SkFlowSync::getChannelHeader(SkFlowChanID chanID, SkDataBuffer &buf)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    return false;
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowSync::existsOptionalPairDb(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_EXISTS_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool exists = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return exists;
}

bool SkFlowSync::addDatabase(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_ADD_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool ok = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return ok;
}

bool SkFlowSync::getCurrentDbName(SkString &dbName)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_DATABASE);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz=0;
    p->recvSize(sz);

    if (!p->recvString(dbName, sz))
    {
        ProtocolRecvError("CANNOT RECV currentDbName");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkFlowSync::variablesKeys(SkStringList &keys)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLES_KEYS);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvList(keys))
    {
        ProtocolRecvError("CANNOT RECV keys");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkFlowSync::getAllVariables(SkArgsMap &db)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLES);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz = 0;
    p->recvSize(sz);

    db.clear();

    SkVariant v;

    if (!p->recvVariant(v, sz))
    {
        ProtocolRecvError("CANNOT RECV recvVariant");
        return false;
    }

    v.copyToMap(db);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkFlowSync::existsVariable(CStr *key)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_EXISTS_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool exists = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return exists;
}

bool SkFlowSync::getVariable(CStr *key, SkVariant &value)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz = 0;
    p->recvSize(sz);

    if (sz && !p->recvVariant(value, sz))
    {
        ProtocolRecvError("CANNOT RECV value data");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowSync::dataGrabbingRegister(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_REGISTER_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool SkFlowSync::dataGrabbingUnRegister(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_UNREGISTER_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool SkFlowSync::grabLastChannelData(SkFlowChanID chanID, SkDataBuffer &data)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_LASTDATA_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    ULong sz;
    p->recvSize(sz);

    if (sz && !p->recvBuffer(data, sz))
    {
        ProtocolRecvError("CANNOT RECV recvBuffer");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool SkFlowSync::sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val)
{
    p->sendStartOfTransaction(FCMD_EXEC_SERVICE_REQUEST);
    p->sendChanID(chanID);
    p->sendCStr(cmd);
    p->sendJSON(val);
    p->sendEndOfTransaction();

    SkFlowCommand fwCmd;
    p->recvStartOfTransaction(fwCmd);

    ULong sz;
    p->recvSize(sz);
    p->recvJSON(val, sz);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    ObjectMessage("ServiceChannel REQUEST sent: " << chanID);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
