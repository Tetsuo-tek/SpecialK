#ifndef SKFLOWSAT_H
#define SKFLOWSAT_H

#include <Core/System/Network/FlowNetwork/skabstractflowsat.h>

class SkFlowSat extends SkAbstractFlowSat
{
    bool connectOnStart;
    bool automaticQuit;
    bool quitting;

    public:
        //Constructor(SkFlowSat, SkAbstractFlowSat);

        Slot(init);
        Slot(quit);

        Slot(onDisconnection);
        Slot(onAppExitRequest);

#if defined(ENABLE_HTTP)
        static void addHttpCLI();
#endif

    protected:
        AbstractConstructor(SkFlowSat, SkAbstractFlowSat);

        //Call it from Costructor of derivative classes
        //default is True
        void disableConnectionOnStart();
        void disableAutomaticQuit();

        bool tryToConnect();
        void closeSatService();

        bool onSetup()                          override;

        virtual void onCustomQuit()             {}
};

#endif // SKFLOWSAT_H
