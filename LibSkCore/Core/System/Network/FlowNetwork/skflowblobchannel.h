#ifndef SKFLOWBLOBCHANNEL_H
#define SKFLOWBLOBCHANNEL_H

#include "skabstractflowchannel.h"
#include "skflowblobreceiver.h"
#include "skflowblobsender.h"

class SkTempFile;

class SkFlowBlobChannel extends SkAbstractFlowChannel
{
    SkStringList coreCommands;
    SkString usersPath;
    SkString currentRoot;

    SkTempFile *temporaryFiles;

    SkTreeMap<SkString, SkAbstractFlowConnection *> uploadersAllocated;
    SkTreeMap<SkAbstractFlowConnection *, SkFlowBlobReceiver *> uploaders;
    SkTreeMap<SkString, SkFlowBlobReceiver *> uploadersByHashID;

    SkTreeMap<SkAbstractFlowConnection *, SkFlowBlobSender *> downloaders;
    SkTreeMap<SkString, SkFlowBlobSender *> downloadersByHashID;

    SkList<SkString> temporaryPaths;

    public:
        Constructor(SkFlowBlobChannel, SkAbstractFlowChannel);

        bool setup(SkFlowChanID id, CStr *name, CStr *usersHomePath);

        bool replicate(SkFlowAsync *node)                   override;

        bool publish(CStr *data, UInt sz);

        bool hasUploaders();
        ULong uploadersCount();

        bool hasdownloaders();
        ULong downloadersCount();

        Slot(onUploadProgress);
        Slot(onUploadTerminated);

        Slot(onDownloadProgress);
        Slot(onDownloadTerminated);

    private:
        void onSetPairDatabase()                            override;
        void onDisable()                                    override;

        bool isCoreCommand(CStr *cmd)                                                   override;
        bool onCoreCommand(CStr *cmd, SkVariant &value, SkFlowServiceRequestStatus &st) override;

        SkString buildAbsRealPath(CStr *itemPath);
        SkString buildAbsUserPath(CStr *itemPath);

        bool currentRootPath(SkFlowServiceRequestStatus &st, SkVariantVector &);
        bool changeRootPath(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool listDirContent(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool makeDir(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool copyItem(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool removeItem(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool putAlloc(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool put(SkFlowServiceRequestStatus &st, SkVariantVector &args);
        bool get(SkFlowServiceRequestStatus &st, SkVariantVector &args);
};

#endif // SKFLOWBLOBCHANNEL_H
