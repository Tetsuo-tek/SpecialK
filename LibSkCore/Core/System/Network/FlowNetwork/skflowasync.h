#ifndef SKASYNCFLOW_H
#define SKASYNCFLOW_H

#include "skflowsync.h"
#include "Core/Containers/skset.h"

class SkAbstractWorkerObject;

struct SkFlowChannelData
{
    SkFlowChanID chanID;
    SkDataBuffer data;
};

class SkFlowAsync extends SkAbstractFlow
{
    SkAbstractWorkerObject *wrk;

    uint64_t maxDataQueueCount;
    SkQueue<SkFlowChannelData *> channelsData;
    SkFlowChannelData currentData;
    SkString currentDbName;
    SkFlowResponse lastResponse;
    uint32_t lastTransactionSize;

    SkFlowChannelData *currentSubscribedGrabbing;
    ULong currentGrabbingDataSize;

    SkTreeMap<SkString, SkFlowChanID *> openingChannels;
    SkSet<SkFlowChanID *> openChannels;

    SkTreeMap<SkString, SkFlowChanID *> subscribingChannels;
    SkSet<SkFlowChanID> subscribedChannels;

    public:
        Constructor(SkFlowAsync, SkAbstractFlow);

        void setWorker(SkAbstractWorkerObject *worker);

        SkFlowSync *buildSyncClient();

        bool checkService();

        bool addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime=nullptr, SkArgsMap *props=nullptr);
        bool addServiceChannel(SkFlowChanID &chanID, CStr *name);
        bool removeChannel(SkFlowChanID chanID);

        //bool getChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b);
        bool setChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b);

        bool setChannelProperties(SkFlowChanID chanID, SkArgsMap &props);

        bool attach(SkFlowChanID sourceID, SkFlowChanID targetID);
        bool detach(SkFlowChanID sourceID, SkFlowChanID targetID);

        bool sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val);

        bool publish(SkFlowChanID chanID);
        bool publish(SkFlowChanID chanID, int8_t v);
        bool publish(SkFlowChanID chanID, uint8_t v);
        bool publish(SkFlowChanID chanID, int16_t v);
        bool publish(SkFlowChanID chanID, uint16_t v);
        bool publish(SkFlowChanID chanID, int32_t v);
        bool publish(SkFlowChanID chanID, uint32_t v);
        bool publish(SkFlowChanID chanID, int64_t v);
        bool publish(SkFlowChanID chanID, uint64_t v);
        bool publish(SkFlowChanID chanID, float v);
        bool publish(SkFlowChanID chanID, double v);
        bool publish(SkFlowChanID chanID, CStr *str);
        bool publish(SkFlowChanID chanID, SkString &str);
        bool publish(SkFlowChanID chanID, SkVariantList &l);
        bool publish(SkFlowChanID chanID, SkAbstractMap<SkString, SkVariant> &m);
        bool publish(SkFlowChanID chanID, CVoid *data, uint64_t sz);

        bool isSubscribed(SkFlowChanID chanID);
        bool subscribeChannel(CStr *name, SkFlowChanID &chanID);
        bool unsubscribeChannel(SkFlowChanID chanID);
        bool subscribeChannel(SkFlowChannel *ch);
        bool unsubscribeChannel(SkFlowChannel *ch);

        ULong getDataCount();
        bool hasNextData();
        bool nextData();
        SkFlowChannelData &getCurrentData();

        CStr *getCurrentDbName();

        Signal(channelAdded);
        Signal(channelRemoved);
        Signal(channelHeaderSetup);

        Signal(publisherAdded);

        Signal(channelPublishStartRequest);
        Signal(channelPublishStopRequest);

        Signal(channelDataPublished);
        Signal(channelDataAvailable);

        Signal(serviceResponseReceived);

        Slot(onWrkEvaluatedCmd);

        Slot(onReadyRead);

    protected:
        virtual void onChannelAdded(SkFlowChanID)                           {}
        virtual void onChannelRemoved(SkFlowChanID)                         {}
        virtual void onChannelHeaderSetup(SkFlowChanID)                     {}

        virtual void onPublisherAdded(SkFlowChanID)                         {}

        virtual void onChannelPublishStartRequest(SkFlowChanID)             {}
        virtual void onChannelPublishStopRequest(SkFlowChanID)              {}

        virtual void onServiceResponseReceived(SkFlowChanID, SkVariant)     {}

    private:
        bool publishPacketizedData(SkFlowChanID chanID, CVoid *val, uint64_t sz);

        bool analyze();

        bool onLogin()              override;
        void onDisconnected()       override;
};

#endif // SKASYNCFLOW_H
