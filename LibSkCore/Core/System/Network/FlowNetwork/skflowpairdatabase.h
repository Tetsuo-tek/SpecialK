#ifndef SKFLOWPAIRDATABASE_H
#define SKFLOWPAIRDATABASE_H

#include "Core/Object/skobject.h"

class SkFlowRedisExport;
class SkFlowRedistrChannel;
class SkAbstractFlowConnection;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowPairsDatabase extends SkObject
{
    SkString dbPath;

    SkString containerName;
    SkTreeMap<SkString, SkVariant> database;
    SkFlowRedistrChannel *pairsCh;
    SkAbstractFlowConnection *o;

    SkTreeMap<SkString, SkVariant> dbProps;

#if defined(ENABLE_REDIS)
    SkFlowRedisExport *r;
#endif

    public:
        Constructor(SkFlowPairsDatabase, SkObject);

        //OWNER MUST BE THE ACCOUNT, NOT THE CONNECTION
        void setOwner(SkAbstractFlowConnection *conn);
        void setup(CStr *name, SkFlowRedistrChannel *pairsChannel, CStr *databasesPath);

#if defined(ENABLE_REDIS)
        void setRedisExport(SkFlowRedisExport *redisClient);
        void synchrononizePairsOnRedis();
        bool isRedisEnabled();
#endif

        bool load(CStr *filePath="");
        bool save(CStr *filePath="");

        void dump(SkVariant &value);

        void variablesKeys(SkStringList &keys);
        bool existsVariable(CStr *key);
        bool setVariable(CStr *key, const SkVariant &value);
        SkVariant &getVariable(CStr *key);
        bool delVariable(CStr *key);
        void flushall();

        void databasePropertiesKeys(SkStringList &keys);
        bool existsDatabaseProperty(CStr *key);
        bool setDatabaseProperty(CStr *key, const SkVariant &value);
        SkVariant &getDatabaseProperty(CStr *key);
        bool delDatabaseProperty(CStr *key);
        void clearDatabaseProperties();

        CStr *name();
        SkBinaryTreeVisit<SkString, SkVariant> *iterator();
        //SkTreeMap<SkString, SkVariant> &db();
        SkVariant &nv();

        SkAbstractFlowConnection *owner();

        Signal(loaded);
        Signal(saved);
        Signal(pairChanged);
        Signal(flushed);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLOWPAIRDATABASE_H
