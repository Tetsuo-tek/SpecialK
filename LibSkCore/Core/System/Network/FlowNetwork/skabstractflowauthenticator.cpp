#include "skabstractflowauthenticator.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowAuthorizedAccount::SkFlowAuthorizedAccount()
{
}

void SkFlowAuthorizedAccount::setup(CStr *name, CStr *token, CStr *sid, SkFlowServicePermission *permissions)
{
    n = name;
    tk = token;
    sessionID = sid;
    perm = *permissions;

    SkString str;
    Stringify(str, n << ".Account");
    setObjectName(str.c_str());

    FlatMessage("READY");
}

bool SkFlowAuthorizedAccount::addConnection(SkAbstractFlowConnection *conn)
{
    if (connections.contains(conn->getSck()))
        return false;

    connections[conn->getSck()] = conn;
    FlatMessage("ADDED connection [t: " << conn->typeName() << "]: " << conn->name());
    return true;
}

bool SkFlowAuthorizedAccount::removeConnection(SkAbstractFlowConnection *conn)
{
    if (!connections.contains(conn->getSck()))
        return false;

    connections.remove(conn->getSck());
    FlatMessage("REMOVED connection [t: " << conn->typeName() << "]: " << conn->name());
    return true;
}

bool SkFlowAuthorizedAccount::isVirtual()
{
    return perm.isVirtualAccount;
}

bool SkFlowAuthorizedAccount::isOsUser()
{
    return perm.isOsUser;
}

bool SkFlowAuthorizedAccount::canSaveDatabases()
{
    return perm.canSaveDatabases;
}

CStr *SkFlowAuthorizedAccount::userName()
{
    return n.c_str();
}

CStr *SkFlowAuthorizedAccount::token()
{
    return tk.c_str();
}

CStr *SkFlowAuthorizedAccount::sid()
{
    return sessionID.c_str();
}

bool SkFlowAuthorizedAccount::hasConnections()
{
    return connections.count();
}

bool SkFlowAuthorizedAccount::hasConnection(SkAbstractDevice *sck)
{
    return connections.contains(sck);
}

SkAbstractFlowConnection *SkFlowAuthorizedAccount::connection(SkAbstractDevice *sck)
{
    if (connections.contains(sck))
        return nullptr;

    return connections[sck];
}

SkFlowServicePermission &SkFlowAuthorizedAccount::permissions()
{
    return perm;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractFlowAuthenticator, SkObject)
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowAuthenticator::addVirtualAccount(CStr *username, CStr *token)
{
    virtualAccounts[username] = token;
    ObjectWarning("ADDED virtual account: " << username);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowAuthenticator::authorize(SkAbstractFlowConnection *conn)
{
    if (SkString::isEmpty(conn->name()))
    {
        ObjectError("FsConnection has empty NAME");
        return false;
    }

    SkFlowAuthorizedAccount *account = nullptr;
    SkString requestedToken = conn->token();

    // ACCOUNT INSTANCE ALREADY EXISTs
    if (isLogged(conn->name()))
    {
        account = auths[conn->name()];

        if (account->hasConnection(conn->getSck()))
        {
            ObjectError("Socket ALREADY stored [SYSTEM_ERROR]: " << conn->name());
            return false;
        }

        if (!onAuthorization(conn->name(), conn->token(), conn->seed(), &account->permissions()))
            return false;
    }

    // FIRST CONNECTION
    else
    {
        SkFlowServicePermission permissions;

        if (!onAuthorization(conn->name(), conn->token(), conn->seed(), &permissions))
            return false;

        SkString sid = uniqueID();

        account = new SkFlowAuthorizedAccount;
        account->setup(conn->name(), requestedToken.c_str(), sid.c_str(), &permissions);
        auths[account->userName()] = account;
    }

    if (!account)
        return false;

    ObjectMessage("FsConnection has LOGGED IN: " << account->userName()
                  << " [isOsUser: " << SkVariant::boolToString(account->isOsUser())
                  << "; isVirtual: " << SkVariant::boolToString(account->isVirtual())
                  << "; dev_T: " << conn->getSck()->typeName()
                  << "; dev: " << conn->getSck() << "@" << conn->objectName()<< "]");

    account->addConnection(conn);
    return true;
}

bool SkAbstractFlowAuthenticator::isVirtualAccount(CStr *userName)
{
    return virtualAccounts.contains(userName);
}

bool SkAbstractFlowAuthenticator::authorizeVirtualLogin(CStr *userName, CStr *token, SkFlowServicePermission *permissions)
{
    ObjectWarning("Checking virtual login: " << userName);

    if (token != virtualAccounts[userName])
    {
        ObjectError("FsConnection token is NOT a valid virtual account: " << userName);
        return false;
    }

    permissions->isVirtualAccount = true;
    permissions->isOsUser = false;
    permissions->isAdmin = false;
    permissions->canSaveDatabases = true;
    return true;
}

CStr *SkAbstractFlowAuthenticator::authorizeSession(CStr *username, CStr *token)
{
    if (virtualAccounts.contains(username))
        return nullptr;

    SkFlowAuthorizedAccount *account = nullptr;

    if (isLogged(username))
    {
        account = auths[username];

        if (!SkString::compare(token, account->token()))
            return nullptr;
    }

    else
    {
        SkFlowServicePermission permission;

        if (!onAuthorization(username, token, "", &permission))
            return nullptr;

        SkString sid = uniqueID();
        account = new SkFlowAuthorizedAccount;

        // THIS AUTH HAS NOT CONNECTIONs (CUL_DE_SAC)
        // IT WILL REMAIN UNTIL A CONNECTION IS CREATED AND DESTROYED
        // SOLUTION: IT COULD BE SUBJECT TO AN ACCOUNT TIMEOUT
        account->setup(username, token, sid.c_str(), &permission);
        auths[account->userName()] = account;
    }

    ObjectMessage("Session instance has LOGGED IN: " << username);
    return account->sid();
}

void SkAbstractFlowAuthenticator::remove(SkAbstractFlowConnection *conn)
{
    if (!conn->isAuthorized() || auths.isEmpty())
        return;

    SkFlowAuthorizedAccount *account = auths[conn->name()];
    account->removeConnection(conn);

    if (!account->hasConnections())
        delete auths.remove(conn->name()).value();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowAuthenticator::isLogged(CStr *userName)
{
    return auths.contains(userName);
}

SkFlowAuthorizedAccount *SkAbstractFlowAuthenticator::account(CStr *userName)
{
    if (!isLogged(userName))
        return nullptr;

    return auths[userName];
}

void SkAbstractFlowAuthenticator::clear()
{
    SkBinaryTreeVisit<SkString, SkFlowAuthorizedAccount *> *itr = auths.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    auths.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
