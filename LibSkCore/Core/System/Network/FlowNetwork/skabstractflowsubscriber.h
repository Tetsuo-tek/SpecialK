#ifndef SKABSTRACTFLOWSUBSCRIBER_H
#define SKABSTRACTFLOWSUBSCRIBER_H

#include "Core/System/Network/FlowNetwork/skflowasync.h"

struct SkFlowSubscriberOptChans
{
    bool enabled;
    SkString chanName;
    SkFlowChannel *ch;

    SkFlowSubscriberOptChans()
    {
        enabled = false;
        ch = nullptr;
    }
};

class SkAbstractFlowSubscriber extends SkObject
{
    SkFlowChannel *src;
    SkString srcSatName;
    SkString srcChanName;

    public:
        void setClient(SkFlowAsync *flowAsync);
        void unSetClient();

        virtual void subscribe(CStr *)                                  =0;
        virtual void unsubscribe()                                      =0;

        virtual void tick(SkFlowChannelData &)                          =0;

        virtual void close()                                            =0;

        SkFlowChannel *source();

        CStr *ownerName();
        CStr *chanName();

        bool isReady();

        Slot(channelAdded);
        Slot(channelRemoved);

        Signal(ready);

    protected:
        AbstractConstructor(SkAbstractFlowSubscriber, SkObject);

        SkFlowAsync *async;

        void setChannelName(CStr *chanName);
        void setSource(SkFlowChannel *source);
        void reset();

        virtual void onChannelAdded(SkFlowChanID)                       =0;
        virtual void onChannelRemoved(SkFlowChanID)                     =0;

};

#endif // SKABSTRACTFLOWSUBSCRIBER_H
