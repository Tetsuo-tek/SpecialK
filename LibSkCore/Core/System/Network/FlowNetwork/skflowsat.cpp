#include "skflowsat.h"

AbstractConstructorImpl(SkFlowSat, SkAbstractFlowSat)
{
    connectOnStart = true;
    automaticQuit = true;
    quitting = false;

    SkCli *cli = skApp->appCli();

    cli->add("--flow-url",                  "", "",         "Setup the FlowHUB url (tcp: or local:)");
    cli->add("--user",                      "", "guest",    "Setup the login username");
    cli->add("--password",                  "", "password", "Setup the login password");
    cli->add("--shared-address",            "", "",         "Setup Satellite shared networking address");

    SlotSet(init);
    SlotSet(quit);
    SlotSet(onDisconnection);
    SlotSet(onAppExitRequest);

    Attach(skApp->started_SIG, pulse, this, init, SkOneShotDirect);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowSat::disableConnectionOnStart()
{
    connectOnStart = false;
}

void SkFlowSat::disableAutomaticQuit()
{
    automaticQuit = false;
}

#if defined(ENABLE_HTTP)
void SkFlowSat::addHttpCLI()
{
    SkCli *cli = skApp->appCli();

    cli->add("--http",                      "", "",         "Enable HTTP service");
    cli->add("--http-www",                  "", "",         "Enable the static HTTP service");
    cli->add("--http-www-path",             "", "www",      "Setup the static www directory");
    cli->add("--http-addr",                 "", "0.0.0.0",  "Setup the listen address for HTTP service");
    cli->add("--http-port",                 "", "20000",    "Setup the listen port for HTTP service");
    cli->add("--http-idle-timeout",         "", "20",       "Setup the HTTP idle-timeout interval in seconds");
    cli->add("--http-queued-connection",    "", "50",       "Setup the HTTP max count for queued sockets");
    cli->add("--http-max-header",           "", "1000000",  "Setup the max allowed header size");
    cli->add("--http-ssl",                  "", "",         "Enable HTTP on SSL");
    cli->add("--http-ssl-cert",             "", "",         "Setup the SSL certificate file");
    cli->add("--http-ssl-key",              "", "",         "Setup the SSL key file");
}
#endif
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowSat::onSetup()
{
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowSat, init)
{
    SilentSlotArgsWarning();

    SkCli *cli = skApp->appCli();
    AssertKiller(!cli->check() || !onSetup());

    if (cli->isUsed("--user"))
    {
        SkString user =  cli->value("--user").toString();
        SkString passwd;

        if (cli->isUsed("--password"))
            passwd =  cli->value("--password").toString();

        else
        {
            cout << "Password: "; cout.flush();
            cin >> passwd;
        }

        setLogin(user.c_str(), passwd.c_str());
    }

    if (connectOnStart)
        AssertKiller(!tryToConnect());

    Attach(this, flowDisconnected, this, onDisconnection, SkQueued);

    Attach(skApp->changed_SIG, pulse, this, tickParamsChanged, SkQueued);
    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
    Attach(skApp->kernel_SIG, pulse, this, onAppExitRequest, SkOneShotQueued);

    onInit();
}

bool SkFlowSat::tryToConnect()
{
    SkCli *cli = skApp->appCli();
    bool ok = false;

    if (cli->isUsed("--flow-url"))
    {
        SkString url =  cli->value("--flow-url").toString();
        ok = buildASync(url.c_str());
    }

    else
        ok = buildASync();

    if (!ok)
        return false;

    SkString sharedAddress = cli->value("--shared-address").toString();

    if (sharedAddress.isEmpty())
        ObjectWarning("Satellite setup has NOT public SHARED_ADDRESS");

    else
    {
        ObjectMessage("Satellite setup has public SHARED_ADDRESS: " << sharedAddress);
        skApp->setGlobalParameter("SHARED_ADDRESS", sharedAddress);
        setVariable("sharedAddress", sharedAddress);
    }

#if defined(ENABLE_HTTP)
    SkFlowHttpServiceConfig httpCfg;

    httpCfg.serviceEnabled = cli->isUsed("--http");

    ObjectMessage("Setting up FsHttpService listen address ..");

    if (httpCfg.serviceEnabled)
    {
        /*if (skApp->existsGlobalParameter("SHARED_ADDRESS"))
        {
            httpCfg.listenAddr = skApp->getGlobalParameter("SHARED_ADDRESS").toString();
            ObjectMessage("Satellite setup has SHARED_ADDRESS configured, using: " << httpCfg.listenAddr);
        }

        else
        {
            httpCfg.listenAddr = cli->value("--http-addr").toString();
            ObjectWarning("Satellite setup has NOT public SHARED_ADDRESS, using: " << httpCfg.listenAddr);
        }*/

        httpCfg.listenAddr = cli->value("--http-addr").toString();
        httpCfg.listenPort = cli->value("--http-port").toUInt16();

        httpCfg.wwwDirEnabled = cli->isUsed("--http-www");
        httpCfg.wwwDir = cli->value("--http-www-path").toString();

        httpCfg.sslEnabled = cli->value("--http-ssl").toBool();
        httpCfg.sslCertFile = workingDir();
        httpCfg.sslCertFile.append(cli->value("--http-ssl-cert").toString());
        httpCfg.sslKeyFile = workingDir();
        httpCfg.sslKeyFile.append(cli->value("--http-ssl-key").toString());
        httpCfg.maxConnQueued = cli->value("--http-queued-connection").toUInt16();
        httpCfg.idleTimeoutInterval = cli->value("--http-idle-timeout").toUInt16();
        httpCfg.maxHeaderSize = cli->value("--http-max-header").toUInt64();

        setupHttpService(httpCfg);
        buildHttpService();
    }
#endif

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowSat, quit)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting ..");
    closeSatService();
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowSat, onAppExitRequest)
{
    SilentSlotArgsWarning();

    int32_t sig = skApp->kernel_SIG->pulse_SIGNAL.getParameters()[0].toInt32();

    if (sig != SIGINT
        && sig != SIGINT
        && sig != SIGTERM
        && sig != SIGQUIT)
    {
        return;
    }

    if (!automaticQuit)
    {
        ObjectWarning("Automatic application shutdown is DISABLED; triggering custom-quit ..");
        onCustomQuit();
        return;
    }

    ObjectWarning("Forcing application shutdown ..");
    eventLoop()->invokeSlot(quit_SLOT, this, this);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowSat, onDisconnection)
{
    SilentSlotArgsWarning();

    if (quitting)
        return;

    ObjectMessage("Quitting due to disconnection..");
    closeSatService();

    if (automaticQuit)
        skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowSat::closeSatService()
{
    quitting = true;
    onQuit();

    closeASync();

#if defined(ENABLE_HTTP)
    closeHttp();
#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

