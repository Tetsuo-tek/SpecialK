#include "skflowservicerequeststatus.h"

SkFlowServiceRequestStatus::SkFlowServiceRequestStatus()
{
    r = nullptr;
    coreCommand = false;
    e = false;
}

void SkFlowServiceRequestStatus::setup(CStr *command, CStr *hashID, bool isCoreCommand, SkFlowRawSvrConnection *requester)
{
    hashStr = hashID;
    r = requester;
    coreCommand = isCoreCommand;
    cmd = command;
}

void SkFlowServiceRequestStatus::setCoreResponse(bool hasError, CStr *msg)
{
    e = hasError;
    resp["status"] = e;
    resp["msg"] = msg;
}

void SkFlowServiceRequestStatus::setCoreResponse(bool hasError, CStr *msg, const SkVariant &optData)
{
    resp["data"] = optData;
    setCoreResponse(hasError, msg);
}

CStr *SkFlowServiceRequestStatus::hashID()
{
    return hashStr.c_str();
}

bool SkFlowServiceRequestStatus::isCoreCommand()
{
    return coreCommand;
}

CStr *SkFlowServiceRequestStatus::command()
{
    return cmd.c_str();
}

bool SkFlowServiceRequestStatus::hasError()
{
    return e;
}

SkArgsMap &SkFlowServiceRequestStatus::response()
{
    return resp;
}

SkFlowRawSvrConnection *SkFlowServiceRequestStatus::requester()
{
    return r;
}
