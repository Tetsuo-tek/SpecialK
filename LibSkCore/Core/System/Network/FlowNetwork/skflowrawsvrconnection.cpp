#include "skflowrawsvrconnection.h"
#include "skflowserver.h"

#include "Core/App/skapp.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Network/TCP/HTTP/skwebsocket.h"
#include "Core/System/skdeviceredistr.h"
#include "Core/Object/skabstractworkerobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define KillingSpuriousClient() \
    do \
    { \
        ObjectError("Killing spurious client [lastCmd: " << SkFlowProto::commandToString(lastBinCmd) << "; prevCmd: " << SkFlowProto::commandToString(prevBinCmd) << "]"); \
        sck->close(); \
    } while(false)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowRawSvrConnection, SkAbstractFlowConnection)
{
    p = nullptr;
    sck = nullptr;

    asynchrounous = false;

    prevBinCmd = FCMD_NOCMD;
    lastBinCmd = FCMD_NOCMD;

    lastTransactionSize = 0;
    lastChanID = -1;
    lastSize = 0;
    lastVariantType = SkVariant_T::T_NULL;

    SlotSet(onSocketReadyRead);
    SlotSet(onChannelAdded);
    SlotSet(onChannelRemoved);
    SlotSet(onChannelHeaderSetup);
    SlotSet(onFastTick);
    SlotSet(onOneSecTick);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowRawSvrConnection::setup(SkFlowServer *server, SkAbstractSocket *socket)
{
    svr = server;
    sck = socket;

    sck->setParent(this);
    sck->setProperty("fwSvrConn", static_cast<void *>(this));

    Attach(sck, readyRead, this, onSocketReadyRead, SkDirect);
    Attach(svr, addedChannel, this, onChannelAdded, SkQueued);
    Attach(svr, removedChannel, this, onChannelRemoved, SkQueued);
    Attach(svr, setChannelHeader, this, onChannelHeaderSetup, SkQueued);
    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkDirect);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick, SkQueued);

    p = new SkFlowProto(this);
    p->setObjectName(this, "Proto");
    p->setup(sck);

    ObjectWarning("Connection ENABLED");
}

CStr *SkFlowRawSvrConnection::connectionID()
{
    return connID.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractDevice *SkFlowRawSvrConnection::getSck()
{
    return sck;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowRawSvrConnection, onFastTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(SkFlowRawSvrConnection, onOneSecTick)
{
    SilentSlotArgsWarning();

    if (checkChrono.stop() <= 5.)
        return;

    checkChrono.start();

    if (sck && sck->isConnected())
    {
        if (asynchrounous)
        {
            p->sendStartOfTransaction(FRSP_CHK_FLOW);
            p->sendEndOfTransaction();
        }

        else
        {
            if (!sck->getRxDataCounter() && !sck->getTxDataCounter())
                sck->disconnect();

            else
                sck->resetAllDataCounters();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowRawSvrConnection, onChannelAdded)
{
    SilentSlotArgsWarning();

    if (!asynchrounous || !sck->isConnected())
        return;

    SkFlowChanID chanID = Arg_Int16;
    SkString name = Arg_String;
    SkFlowChannel_T chan_t = Arg_Enum(SkFlowChannel_T);

    if (chan_t == StreamingChannel)
        ObjectMessage("ADDED StreamingChannel NOTIFY: " << chanID << " [" << name << "]");

    else if (chan_t == BlobChannel)
        ObjectMessage("ADDED BlobChannel NOTIFY: " << chanID << " [" << name << "]");

    else
        ObjectMessage("ADDED ServiceChannel NOTIFY: " << chanID << " [" << name << "]");

    notifyAddedChannel(chanID);
}

SlotImpl(SkFlowRawSvrConnection, onChannelRemoved)
{
    SilentSlotArgsWarning();

    if (!asynchrounous || !sck->isConnected())
        return;

    SkFlowChanID chanID = Arg_Int16;
    SkString name = Arg_String;
    SkFlowChannel_T chan_t = Arg_Enum(SkFlowChannel_T);

    if (chan_t == StreamingChannel)
        ObjectMessage("REMOVED StreamingChannel NOTIFY: " << chanID << " [" << name << "]");

    else if (chan_t == BlobChannel)
        ObjectMessage("REMOVED BlobChannel NOTIFY: " << chanID << " [" << name << "]");

    else
        ObjectMessage("REMOVED ServiceChannel NOTIFY: " << chanID << " [" << name << "]");

    notifyRemovedChannel(chanID);
}

SlotImpl(SkFlowRawSvrConnection, onChannelHeaderSetup)
{
    SilentSlotArgsWarning();

    if (!asynchrounous || !sck->isConnected())
        return;

    SkFlowChanID chanID = Arg_Int16;
    SkString name = Arg_String;
    ULong sz = Arg_UInt64;

    ObjectMessage("StreamingChannel header [" << sz << " B] NOTIFY: " << chanID << " [" << name << "]");
    notifyChannelHeader(chanID);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowRawSvrConnection::notifyAddedChannel(SkFlowChanID chanID)
{
    SkAbstractFlowChannel *absCh = svr->getChannelByID(chanID);

    p->sendStartOfTransaction(FRSP_CHANNEL_ADDED);
    p->sendChanType(absCh->chanType());
    p->sendChanID(absCh->id());
    p->sendCStr(absCh->name());
    p->sendCStr(absCh->hashID());

    if (absCh->chanType() == StreamingChannel)
    {
        SkFlowRedistrChannel *ch = DynCast(SkFlowRedistrChannel, absCh);

        if (!ch)
        {
            ObjectError("New channel to send is NOT valid [chanID: " << chanID << "]");
            return;
        }

        p->sendFlowType(ch->flowType());
        p->sendVariantType(ch->dataType());
        p->sendCStr(ch->mime());

        //p->sendCStr(ch->udm());

        /*p->sendCStr(SkVariant::boolToString(!ch->header().isEmpty()));

        if (!ch->header().isEmpty())
            p->sendBuffer(ch->header());

        //p->sendCStr(ch->mpPath());
        p->sendCStr("");*/
    }

    else if (absCh->chanType() == BlobChannel)
    {
    }

    else if (absCh->chanType() == ServiceChannel)
    {
    }

    p->sendEndOfTransaction();
}

void SkFlowRawSvrConnection::notifyRemovedChannel(SkFlowChanID chanID)
{
    if (ownedChannels.contains(chanID))
        ownedChannels.remove(chanID);

    if (subscribedChannels.contains(chanID))
        subscribedChannels.remove(chanID);

    if (registeredGrabChannels.contains(chanID))
        registeredGrabChannels.remove(chanID);

    p->sendStartOfTransaction(FRSP_CHANNEL_REMOVED);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();
}

void SkFlowRawSvrConnection::notifyChannelHeader(SkFlowChanID chanID)
{
    SkAbstractFlowChannel *absCh = svr->getChannelByID(chanID);
    SkFlowRedistrChannel *ch = DynCast(SkFlowRedistrChannel, absCh);

    p->sendStartOfTransaction(FRSP_CHANNEL_HEADER);
    p->sendChanID(chanID);
    p->sendBuffer(ch->header());
    p->sendEndOfTransaction();
}

void SkFlowRawSvrConnection::notifyChannelReqPublishStart(SkFlowChanID chanID)
{
    p->sendStartOfTransaction(FRSP_CHANNEL_PUBLISH_START);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();
}

void SkFlowRawSvrConnection::notifyChannelReqPublishStop(SkFlowChanID chanID)
{
    p->sendStartOfTransaction(FRSP_CHANNEL_PUBLISH_STOP);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowRawSvrConnection::onLogin()
{
    connID = loginName;
    connID.append("@");
    connID.append(sck->typeName());
    connID.append("(");
    connID.concat(sck->getFileDescriptor());
    connID.append(")");

    SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");

    SkArgsMap props;
    props["id"] = connID;
    props["sck_t"] = sck->typeName();
    props["fd"] = sck->getFileDescriptor();
    props["user"] = name();
    props["isAsynchronous"] = false;

    connDB->setVariable(connID.c_str(), props);
}

void SkFlowRawSvrConnection::onDisable()
{
    Detach(sck, readyRead, this, onSocketReadyRead);
    Detach(svr, addedChannel, this, onChannelAdded);
    Detach(svr, removedChannel, this, onChannelRemoved);
    Detach(svr, setChannelHeader, this, onChannelHeaderSetup);
    Detach(eventLoop()->fastZone_SIG, pulse, this, onFastTick);
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick);

    if (isAuthorized())
    {
        if (isAsynchronous() && svr->existsOptionalPairDb(name()))
        {
            SkFlowPairsDatabase *userDB = svr->getPairDatabase(name());

            SkStringList l;
            SkVariant &v = userDB->getVariable("asynchs");
            v.copyToStringList(l);
            l.remove(connID);
            userDB->setVariable("asynchs", l);
            userDB->setVariable("asynchsCount", l.count());
        }

        if (svr->existsOptionalPairDb("Connections"))
        {
            SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");
            connDB->delVariable(connID.c_str());
        }
    }

    ObjectWarning("Connection DISABLED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowRawSvrConnection, onSocketReadyRead)
{
    SilentSlotArgsWarning();

    while (sck->isConnected() && sck->bytesAvailable()>0)
    {
        ULong sz = sck->bytesAvailable();
        analyze();

        if (sck->bytesAvailable() == sz)
        {
            eventLoop()->invokeSlot(onSocketReadyRead_SLOT, this, this);
            break;
        }

        //if (lastBinCmd == FCMD_NOCMD)
            //break;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowRawSvrConnection::analyze()
{
    //HERE WE GRAB THE COMMAND
    if (lastBinCmd == FCMD_NOCMD && sck->bytesAvailable() >= /*sizeof(uint32_t) + */sizeof(uint16_t))
    {
        lastTransactionSize = p->recvStartOfTransaction(lastBinCmd);
        //cout << "!!!!!!!!!!!!!!!!!" << SkFlowProto::commandToString(lastBinCmd) << "\n";
    }

    /*if (lastBinCmd != FCMD_NOCMD && lastTransactionSize > 0 && sck->bytesAvailable() < lastTransactionSize)
        return;*/

    else if (!authorized)
    {
        if (lastBinCmd == FCMD_GET_LOGIN_SEED)
            sendLoginUUID();

        else if (lastBinCmd == FCMD_LOGIN)
            recvLogin();

        else if (lastBinCmd == FCMD_QUIT)
            quitConnection();

        else
            KillingSpuriousClient();
    }

    else
    {
        if (lastBinCmd == FCMD_SET_ASYNC)
            setASync();

        else if (lastBinCmd == FCMD_CHK_SERVICE)
            recvCheckService();

        else if (lastBinCmd == FCMD_GET_CHANS_LIST)
            sendChannelsList();

        else if (lastBinCmd == FCMD_GET_CHAN_PROPS)
            sendChannelProperties();

        else if (lastBinCmd == FCMD_GET_CHAN_HEADER)
            sendChannelHeader();

        else if (lastBinCmd == FCMD_SET_CHAN_HEADER)
            recvChannelHeader();

        else if (lastBinCmd == FCMD_ADD_STREAMING_CHAN)
            addStreamingChannel();

        else if (lastBinCmd == FCMD_ADD_SERVICE_CHAN)
            addServiceChannel();

        else if (lastBinCmd == FCMD_ADD_BLOB_CHAN)
            addBlobChannel();

        else if (lastBinCmd == FCMD_DEL_CHAN)
            removeChannel();

        else if (lastBinCmd == FCMD_ATTACH_CHAN)
            attachChannel();

        else if (lastBinCmd == FCMD_DETACH_CHAN)
            detachChannel();

        else if (lastBinCmd == FCMD_EXEC_SERVICE_REQUEST)
            acceptRequestForService();

        else if (lastBinCmd == FCMD_RETURN_SERVICE_RESPONSE)
            grabResponseFromService();

        else if (lastBinCmd == FCMD_PUBLISH)
            publish();

        else if (lastBinCmd == FCMD_SUBSCRIBE_CHAN)
            subscribe();

        else if (lastBinCmd == FCMD_UNSUBSCRIBE_CHAN)
            unsubscribe();

        else if (lastBinCmd == FCMD_EXISTS_DATABASE)
            existsDatabase();

        else if (lastBinCmd == FCMD_ADD_DATABASE)
            addDatabase();

        else if (lastBinCmd == FCMD_DEL_DATABASE)
            removeDatabase();

        else if (lastBinCmd == FCMD_LOAD_DATABASE)
            loadDatabase();

        else if (lastBinCmd == FCMD_SAVE_DATABASE)
            saveDatabase();

        else if (lastBinCmd == FCMD_EXISTS_DATABASE_PROPERTY)
            existsDatabaseProperty();

        else if (lastBinCmd == FCMD_SET_DATABASE_PROPERTY)
            recvDatabaseProperty();

        else if (lastBinCmd == FCMD_SET_DATABASE_PROPERTY_JSON)
            recvDatabasePropertyJson();

        else if (lastBinCmd == FCMD_GET_DATABASE_PROPERTY)
            sendDatabaseProperty();

        else if (lastBinCmd == FCMD_GET_DATABASE_PROPERTY_JSON)
            sendDatabasePropertyJson();

        else if (lastBinCmd == FCMD_DEL_DATABASE_PROPERTY)
            removeDatabaseProperty();

        else if (lastBinCmd == FCMD_SET_DATABASE)
            setCurrentDatabase();

        else if (lastBinCmd == FCMD_GET_DATABASE)
            sendCurrentDatabase();

        else if (lastBinCmd == FCMD_GET_VARIABLES_KEYS)
            sendVariablesKeys();

        else if (lastBinCmd == FCMD_GET_VARIABLES)
            sendAllVariables();

        else if (lastBinCmd == FCMD_GET_VARIABLES_JSON)
            sendAllVariablesJson();

        else if (lastBinCmd == FCMD_EXISTS_VARIABLE)
            existsVariable();

        else if (lastBinCmd == FCMD_SET_VARIABLE)
            recvVariable();

        else if (lastBinCmd == FCMD_SET_VARIABLE_JSON)
            recvVariableJson();

        else if (lastBinCmd == FCMD_GET_VARIABLE)
            sendVariable();

        else if (lastBinCmd == FCMD_GET_VARIABLE_JSON)
            sendVariableJson();

        else if (lastBinCmd == FCMD_DEL_VARIABLE)
            removeVariable();

        else if (lastBinCmd == FCMD_FLUSHALL)
            flushall();

        else if (lastBinCmd == FCMD_GRAB_REGISTER_CHAN)
            registerOnGrabChanData();

        else if (lastBinCmd == FCMD_GRAB_UNREGISTER_CHAN)
            unregisterOnGrabChanData();

        else if (lastBinCmd == FCMD_GRAB_LASTDATA_CHAN)
            sendLastChanData();

        //ADMIN CMDs

        else if (lastBinCmd == FCMD_ADD_USER)
            addUser();

        else if (lastBinCmd == FCMD_DEL_USER)
            removeUser();

        else if (lastBinCmd == FCMD_SET_USER_PERMISSION)
            setUserPermission();

        else if (lastBinCmd == FCMD_KILL_USER)
            killUser();

        else if (lastBinCmd == FCMD_KILL_CONNECTION)
            killConnection();

        else
            KillingSpuriousClient();
    }
}

void SkFlowRawSvrConnection::resetCommand()
{
    prevBinCmd = lastBinCmd;
    lastBinCmd = FCMD_NOCMD;
    lastTransactionSize = 0;
    lastChanID = -1;
    lastString.clear();
    lastBuffer.clear();
    lastVariantType = SkVariant_T::T_NULL;
    lastSize = 0;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowRawSvrConnection::isAsynchronous()
{
    return asynchrounous;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowProto *SkFlowRawSvrConnection::proto()
{
    return p;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowRawSvrConnection::sendLoginUUID()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);
    lastSeed = uuid();
    p->sendString(lastSeed);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::recvLogin()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(loginName, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(loginToken, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);
    p->sendEndOfTransaction();

    resetCommand();

    if (!authorize())
    {
        ObjectError("Login FAILED [" << loginName << "]; killing ..");
        sck->disconnect();
        return;
    }

    setObjectName(connID.c_str());

    SkString tempName = connID;//connID built by onLogin()
    tempName.append(".sck");
    sck->setObjectName(tempName.c_str());
}

void SkFlowRawSvrConnection::setASync()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    asynchrounous = true;

    SkStringList l;
    SkFlowPairsDatabase *userDB = nullptr;

    if (svr->existsOptionalPairDb(name()))
    {
        userDB = svr->getPairDatabase(name());
        SkVariant &v = userDB->getVariable("asynchs");
        v.copyToStringList(l);
    }

    else
    {
        if (!svr->addPairDatabase(name()))
        {
            KillingSpuriousClient();
            return;
        }

        userDB = svr->getPairDatabase(name());
        userDB->setOwner(this);
    }

    l << connID;
    userDB->setVariable("asynchs", l);
    userDB->setVariable("asynchsCount", l.count());

    SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");

    SkArgsMap props;
    connDB->getVariable(connID.c_str()).copyToMap(props);
    props["isAsynchronous"] = true;
    connDB->setVariable(connID.c_str(), props);

    for(uint64_t i=0; i<svr->channelsCount(); i++)
        if (svr->isChannelValid(i))
            notifyAddedChannel(i);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::recvCheckService()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    resetCommand();
}

void SkFlowRawSvrConnection::sendChannelsList()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    SkAbstractFlowChannel **channels = svr->getChannels();

    for(uint64_t i=0; i<svr->channelsCount(); i++)
        if (channels[i])
            p->sendCStr(channels[i]->name());

    p->sendEndOfList();
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendChannelProperties()
{
    uint64_t sz=0;
    p->recvSize(sz);

    SkString chanName;

    if (sz && !p->recvString(chanName, sz))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByName(chanName.c_str());

    if (!absCh)
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);
    p->sendChanType(absCh->chanType());
    p->sendChanID(absCh->id());
    p->sendCStr(absCh->hashID());

    if (absCh->chanType() == StreamingChannel)
    {
        SkFlowRedistrChannel *ch = DynCast(SkFlowRedistrChannel, absCh);
        p->sendFlowType(ch->flowType());
        p->sendVariantType(ch->dataType());
        p->sendCStr(ch->mime());
        //p->sendCStr(ch->udm());
        //p->sendCStr(SkVariant::boolToString(!ch->header().isEmpty()));
    }
    //else

    p->sendEndOfTransaction();
    resetCommand();
}

void SkFlowRawSvrConnection::sendChannelHeader()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    SkFlowRedistrChannel *ch = DynCast(SkFlowRedistrChannel, absCh);
    p->sendStartOfTransaction(lastBinCmd);
    p->sendBuffer(ch->header());
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::recvChannelHeader()
{
    p->recvChanID(lastChanID);

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);

    if (!lastSize)
    {
        KillingSpuriousClient();
        return;
    }

    ch->header().clear();

    if (!p->recvBuffer(ch->header(), lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkVariantVector p;
    p << ch->id();
    p << ch->name();
    p << ch->header().size();
    svr->setChannelHeader(this, p);

    {
        SkVariantList p;
        p << ch->id();
        p << ch->name();
        p << ch->header().size();

        skApp->propagateEvent(this, "Flow", "ChannelHeaderSetup", &p);
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::existsDatabase()
{
    p->recvSize(lastSize);

    SkString s;

    if (lastSize && !p->recvString(s, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);
    p->sendBool(svr->existsOptionalPairDb(s.c_str()));
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::addDatabase()
{
    p->recvSize(lastSize);

    SkString s;

    if (lastSize && !p->recvString(s, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    bool ok = svr->addPairDatabase(s.c_str());

    p->sendStartOfTransaction(lastBinCmd);
    p->sendBool(ok);
    p->sendEndOfTransaction();

    if (ok)
    {
        svr->getPairDatabase(s.c_str())->setOwner(this);
        ownedDatabases.add(s.c_str());
    }

    resetCommand();
}

void SkFlowRawSvrConnection::removeDatabase()
{
    p->recvSize(lastSize);

    SkString s;

    if (lastSize && !p->recvString(s, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    bool ok = svr->delPairDatabase(s.c_str());

    p->sendStartOfTransaction(lastBinCmd);
    p->sendBool(ok);
    p->sendEndOfTransaction();

    if (ok)
    {
        svr->getPairDatabase(s.c_str())->setOwner(this);
        ownedDatabases.remove(s.c_str());
    }

    resetCommand();
}

void SkFlowRawSvrConnection::loadDatabase()
{
    p->recvSize(lastSize);

    SkString s;

    if (lastSize && !p->recvString(s, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    bool ok = svr->getPairDatabase(s.c_str())->load();

    p->sendStartOfTransaction(lastBinCmd);
    p->sendBool(ok);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::saveDatabase()
{
    p->recvSize(lastSize);

    SkString s;

    if (lastSize && !p->recvString(s, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    bool ok = svr->getPairDatabase(s.c_str())->save();

    p->sendStartOfTransaction(lastBinCmd);
    p->sendBool(ok);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::existsDatabaseProperty()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    bool exists = false;

    if (currentDatabase)
        exists = currentDatabase->existsDatabaseProperty(lastString.c_str());

    p->sendBool(exists);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::recvDatabaseProperty()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    SkVariant v;

    if (lastSize && !p->recvVariant(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (currentDatabase)
        currentDatabase->setDatabaseProperty(lastString.c_str(), v);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::recvDatabasePropertyJson()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    SkVariant v;

    if (lastSize && !p->recvJSON(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (currentDatabase)
        currentDatabase->setDatabaseProperty(lastString.c_str(), v);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::sendDatabaseProperty()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    if (currentDatabase)
    {
        SkVariant &v = currentDatabase->getDatabaseProperty(lastString.c_str());
        p->sendVariant(v);
    }

    else
    {
        SkVariant nv;
        p->sendVariant(nv);
    }

    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendDatabasePropertyJson()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    if (currentDatabase)
    {
        SkVariant &v = currentDatabase->getDatabaseProperty(lastString.c_str());
        p->sendJSON(v);
    }

    else
        p->sendSize(0);

    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::removeDatabaseProperty()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (currentDatabase)
        currentDatabase->delDatabaseProperty(lastString.c_str());

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::setCurrentDatabase()
{
    p->recvSize(lastSize);
    currentDatabase = nullptr;

    SkString dbName;

    if (lastSize && !p->recvString(dbName, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (svr->existsOptionalPairDb(dbName.c_str()))
        currentDatabase = svr->getPairDatabase(dbName.c_str());

    else
        ObjectError("Database NOT found: " << dbName);

    if (asynchrounous)
    {
        p->sendStartOfTransaction(FRSP_CURRENTDB_CHANGED);
        p->sendString(dbName);
        p->sendEndOfTransaction();
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::sendCurrentDatabase()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    SkString currentDbName;

    if (currentDatabase)
        currentDbName = currentDatabase->name();

    p->sendString(currentDbName);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendVariablesKeys()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkStringList keys;

    if (currentDatabase)
        currentDatabase->variablesKeys(keys);

    p->sendStartOfTransaction(lastBinCmd);

    for(uint64_t i=0; i<keys.count(); i++)
        p->sendString(keys[i]);

    p->sendEndOfList();
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendAllVariables()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    SkVariant v;
    currentDatabase->dump(v);
    p->sendVariant(v);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendAllVariablesJson()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    SkVariant v;
    currentDatabase->dump(v);
    p->sendJSON(v);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::existsVariable()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    bool exists = false;

    if (lastString.startsWith("@"))
        exists = existsProperty(lastString.c_str());

    else if (currentDatabase)
        exists = currentDatabase->existsVariable(lastString.c_str());

    p->sendBool(exists);
    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::recvVariable()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    SkVariant v;

    if (lastSize && !p->recvVariant(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (lastString.startsWith("@"))
    {
        setProperty(lastString.c_str(), v);
        SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");

        SkArgsMap props;
        connDB->getVariable(connID.c_str()).copyToMap(props);
        props[&lastString.c_str()[1]] = v;
        connDB->setVariable(connID.c_str(), props);
    }

    else if (currentDatabase)
        currentDatabase->setVariable(lastString.c_str(), v);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::recvVariableJson()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    p->recvSize(lastSize);

    SkVariant v;

    if (lastSize && !p->recvJSON(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (lastString.startsWith("@"))
    {
        setProperty(lastString.c_str(), v);
        SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");

        SkArgsMap props;
        connDB->getVariable(connID.c_str()).copyToMap(props);
        props[&lastString.c_str()[1]] = v;
        connDB->setVariable(connID.c_str(), props);
    }

    else if (currentDatabase)
        currentDatabase->setVariable(lastString.c_str(), v);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::sendVariable()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);


    if (lastString.startsWith("@"))
    {
        SkVariant &v = property(lastString.c_str());
        p->sendVariant(v);
    }

    else if (currentDatabase)
    {
        SkVariant &v = currentDatabase->getVariable(lastString.c_str());
        p->sendVariant(v);
    }

    else
    {
        SkVariant nv;
        p->sendVariant(nv);
    }

    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::sendVariableJson()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    if (lastString.startsWith("@"))
    {
        SkVariant &v = property(lastString.c_str());
        p->sendJSON(v);
    }

    else if (currentDatabase)
    {
        SkVariant &v = currentDatabase->getVariable(lastString.c_str());
        p->sendJSON(v);
    }

    else
        p->sendSize(0);

    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::removeVariable()
{
    p->recvSize(lastSize);

    if (!lastSize || !p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (lastString.startsWith("@"))
    {
        removeProperty(lastString.c_str());
        SkFlowPairsDatabase *connDB = svr->getPairDatabase("Connections");

        SkArgsMap props;
        connDB->getVariable(connID.c_str()).copyToMap(props);
        props.remove(&lastString.c_str()[1]);
        connDB->setVariable(connID.c_str(), props);
    }

    else if (currentDatabase)
        currentDatabase->delVariable(lastString.c_str());

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::flushall()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (currentDatabase)
        currentDatabase->flushall();

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::addStreamingChannel()
{
    // MUST UNIFY WITH addBlobChannel() - THEY ARE THE SAME

    SkFlow_T flow_t;//DEPRECATED
    p->recvFlowType(flow_t);

    SkVariant_T data_t;//DEPRECATED
    p->recvVariantType(data_t);

    SkString name;
    p->recvSize(lastSize);

    if (!p->recvString(name, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    SkString mime;//DEPRECATED
    p->recvSize(lastSize);//COULD BE 0
    p->recvString(mime, lastSize);//COULD BE FALSE

    SkVariant v;
    p->recvSize(lastSize);//COULD BE 0
    p->recvJSON(v, lastSize);//COULD BE FALSE

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkArgsMap props;

    if (v.variantType() == T_MAP)
        v.copyToMap(props);

    if (existsProperty("@nodeName"))
    {
        SkString str = property("@nodeName").toString();
        props["publisher"] = str;
    }

    SkString chName;
    Stringify(chName, loginName << "." << name);

    SkFlowChanID chanID = -1;
    SkArgsMap *propsPtr = nullptr;

    if (!props.isEmpty())
        propsPtr = &props;

    chanID = svr->addStreamingChannel(flow_t, data_t, chName.c_str(), mime.c_str(), propsPtr, this);

    if (chanID > -1)
    {
        SkAbstractFlowChannel *ch = svr->getChannels()[chanID];
        ownedChannels.add(ch->id());
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::addServiceChannel()
{
    p->recvSize(lastSize);

    SkVariant v;

    if (!p->recvJSON(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkVariantVector props;//DEPRECATED
    v.copyToList(props);//DEPRECATED
    SkString name = props[0].toString();//DEPRECATED

    SkString chName;
    Stringify(chName, loginName << "." << name);

    SkFlowChanID chanID = -1;

    if (existsProperty("@nodeName"))
    {
        SkArgsMap props;
        SkString str = property("@nodeName").toString();
        props["manager"] = str;
        chanID = svr->addServiceChannel(chName.c_str(), &props, this);
    }

    else
        chanID = svr->addServiceChannel(chName.c_str(), nullptr, this);

    if (chanID > -1)
    {
        SkAbstractFlowChannel *ch = svr->getChannels()[chanID];
        ownedChannels.add(ch->id());
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::addBlobChannel()
{
    // MUST UNIFY WITH addStreamingChannel() - THEY ARE THE SAME

    SkString name;
    p->recvSize(lastSize);

    if (!p->recvString(name, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    SkVariant v;
    p->recvSize(lastSize);//COULD BE 0
    p->recvJSON(v, lastSize);//COULD BE FALSE

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkArgsMap props;

    if (v.variantType() == T_MAP)
        v.copyToMap(props);

    if (existsProperty("@nodeName"))
    {
        SkString str = property("@nodeName").toString();
        props["publisher"] = str;
    }

    SkString chName;
    Stringify(chName, loginName << "." << name);

    SkFlowChanID chanID = -1;
    SkArgsMap *propsPtr = nullptr;

    if (!props.isEmpty())
        propsPtr = &props;

    chanID = svr->addBlobChannel(chName.c_str(), propsPtr, this);

    if (chanID > -1)
    {
        SkAbstractFlowChannel *ch = svr->getChannels()[chanID];
        ownedChannels.add(ch->id());
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::removeChannel()
{
    p->recvChanID(lastChanID);

    SkAbstractFlowChannel *ch = svr->getChannelByID(lastChanID);

    if (!ch)
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (flowAccount->permissions().isAdmin || ch->owner() == this)
    {
        if (flowAccount->permissions().isAdmin && ch->owner() != this)
            ObjectWarning("ADMIN is removing a NOT-OWNED channel: " << ch->name());

        if (!svr->removeChannel(ch->id()))
        {
            KillingSpuriousClient();
            return;
        }
    }

    else
        ObjectWarning("Trying to remove a NOT-OWNED channel: " << ch->name());

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::attachChannel()
{
    SkFlowChanID sourceID;
    p->recvChanID(sourceID);

    SkFlowChanID targetID;
    p->recvChanID(targetID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (svr->getChannelType(sourceID) != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    SkFlowRedistrChannel *src = DynCast(SkFlowRedistrChannel, svr->getChannels()[sourceID]);
    SkFlowRedistrChannel *tgt = DynCast(SkFlowRedistrChannel, svr->getChannels()[targetID]);

    src->attach(tgt);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::detachChannel()
{
    SkFlowChanID sourceID;
    p->recvChanID(sourceID);

    SkFlowChanID targetID;
    p->recvChanID(targetID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (svr->getChannelType(sourceID) != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    SkFlowRedistrChannel *src = DynCast(SkFlowRedistrChannel, svr->getChannels()[sourceID]);
    SkFlowRedistrChannel *tgt = DynCast(SkFlowRedistrChannel, svr->getChannels()[targetID]);

    src->detach(tgt);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::acceptRequestForService()
{
    p->recvChanID(lastChanID);
    p->recvSize(lastSize);

    if (!p->recvString(lastString, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    SkVariant v;
    p->recvSize(lastSize);

    if (!p->recvJSON(v, lastSize))
    {
        KillingSpuriousClient();
        return;
    }

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    if (svr->isChannelValid(lastChanID))
    {
        SkAbstractFlowChannel *service = svr->getChannels()[lastChanID];
        SkFlowServiceRequestStatus *st = service->openRequest(this, lastString.c_str(), v);

        if (!st)
            ObjectError("Service request was ABORTED: " << service->name());
    }

    else
        ObjectError("CANNOT send a request to a NOT valid channel: " << lastChanID);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::grabResponseFromService()
{
    p->recvChanID(lastChanID);

    uint64_t sz;
    p->recvSize(sz);

    SkString hashStr;
    p->recvString(hashStr, sz);

    p->recvSize(sz);

    SkVariant response;
    p->recvJSON(response, sz);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *service = svr->getChannels()[lastChanID];
    service->closeRequest(hashStr.c_str(), response);

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::publish()
{
    if (!lastSize)
    {
        if (sck->bytesAvailable() < sizeof(SkFlowChanID) + sizeof(uint32_t))
            return;

        p->recvChanID(lastChanID);
        p->recvSize(lastSize);

        //CAN PULSE NOW
        if (!lastSize)
        {
            ObjectError("Payload size MUST be > 0");
            //MUST USE ALSO WITH size=0 WHEN IT IS AS A PULSE()
            KillingSpuriousClient();
            return;
        }
    }

    if (lastSize && lastBuffer.size() < lastSize)
    {
        uint64_t remains = lastSize-lastBuffer.size();
        uint64_t available = sck->bytesAvailable();

        if (available < remains)
        {
            //cout << "# " << remains << " " <<  available << "\n";
            remains = available;
        }

        //cout  << remains << "\n";

        if (remains && !p->recvBuffer(lastBuffer, remains))
        {
            KillingSpuriousClient();
            return;
        }

        /*if (lastBuffer.size() > lastSize)
            cout << " " << remains <<" "<< lastSize << " " << lastBuffer.size() << " " << sck->bytesAvailable() << "\n";*/
    }

    if (lastBuffer.size() < lastSize)
        return;

    if (sck->bytesAvailable() < sizeof(uint32_t))
        return;

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || (absCh->chanType() != StreamingChannel && absCh->chanType() != BlobChannel))
    {
        KillingSpuriousClient();
        return;
    }

    if (absCh->owner() == this)
    {
        ULong sz = lastBuffer.size();

        if (absCh->chanType() == StreamingChannel)
        {
            SkFlowRedistrChannel *ch = DynCast(SkFlowRedistrChannel, absCh);
            ch->publish(lastBuffer.toVoid(), sz);
        }

        else if (absCh->chanType() == BlobChannel)
        {
            SkFlowBlobChannel *ch = DynCast(SkFlowBlobChannel, absCh);
            ch->publish(lastBuffer.data(), sz);
        }
    }

    else
        ObjectWarning("Trying to publish on NOT-OWNED channel: " << absCh->name());

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::registerOnGrabChanData()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    registeredGrabChannels.add(absCh->id());

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
    ch->addGrabber(this);

    resetCommand();
}

void SkFlowRawSvrConnection::unregisterOnGrabChanData()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    registeredGrabChannels.remove(absCh->id());

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
    ch->removeGrabber(this);

    resetCommand();
}

void SkFlowRawSvrConnection::sendLastChanData()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        KillingSpuriousClient();
        return;
    }

    p->sendStartOfTransaction(lastBinCmd);

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
    p->sendBuffer(ch->data());

    p->sendEndOfTransaction();

    resetCommand();
}

void SkFlowRawSvrConnection::subscribe()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || (absCh->chanType() != StreamingChannel && absCh->chanType() != BlobChannel))
    {
        if (!absCh)
            ObjectError("Channel is NOT valid: " << lastChanID);

        else
            ObjectError("Channel CANNOT be subscribed: " << absCh->name());
    }

    else
    {
        subscribedChannels.add(absCh->id());

        if (absCh->chanType() == StreamingChannel)
        {
            SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
            ch->addSubscriber(this);
        }

        else if (absCh->chanType() == BlobChannel)
        {
            /*SkFlowBlobChannel *ch = dynamic_cast<SkFlowBlobChannel *>(absCh);
            ch->addDownloader(this);*/
        }
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::unsubscribe()
{
    p->recvChanID(lastChanID);

    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    SkAbstractFlowChannel *absCh = svr->getChannelByID(lastChanID);

    if (!absCh || (absCh->chanType() != StreamingChannel && absCh->chanType() != BlobChannel))
    {
        if (!absCh)
            ObjectError("Channel is NOT valid: " << lastChanID);

        else
            ObjectError("Channel CANNOT be subscribed: " << absCh->name());
    }

    else
    {
        subscribedChannels.remove(absCh->id());

        if (absCh->chanType() == StreamingChannel)
        {
            SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
            ch->removeSubscriber(this);
        }

        else if (absCh->chanType() == BlobChannel)
        {
            /*SkFlowBlobChannel *ch = dynamic_cast<SkFlowBlobChannel *>(absCh);
            ch->removeDownloader(this);*/
        }
    }

    //NO RESPONSE
    resetCommand();
}

void SkFlowRawSvrConnection::addUser()
{

}

void SkFlowRawSvrConnection::removeUser()
{

}

void SkFlowRawSvrConnection::setUserPermission()
{

}

void SkFlowRawSvrConnection::killUser()
{

}

void SkFlowRawSvrConnection::killConnection()
{

}

void SkFlowRawSvrConnection::quitConnection()
{
    if (!p->recvEndOfTransaction())
    {
        KillingSpuriousClient();
        return;
    }

    //NO RESPONSE
    resetCommand();

    sck->close();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
