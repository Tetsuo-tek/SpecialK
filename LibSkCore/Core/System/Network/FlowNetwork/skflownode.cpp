#include "skflownode.h"
#include "skflowserver.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkFlowNode, SkFlowAsync)
{
    owner = dynamic_cast<SkFlowServer *>(parent());

    SlotSet(onReplicatedChannelAdded);
    SlotSet(onReplicatedChannelRemoved);
    SlotSet(onReplicatedChannelPublishStartRequest);
    SlotSet(onReplicatedChannelPublishStopRequest);

    SlotSet(onFlowDisconnected);//not using onDisconnected virtual because it is used from SkFlowAsync

    SlotSet(onOneSecCheck);

    setObjectName(owner, "NodeClient");
}

void SkFlowNode::init(CStr *address, Short port, CStr *name, CStr *password)
{
    flowNodeAddress = address;
    flowNodePort = port;
    flowNodeName = name;
    flowNodePassword = password;

    Attach(this, channelAdded, this, onReplicatedChannelAdded, SkDirect);
    Attach(this, channelRemoved, this, onReplicatedChannelRemoved, SkDirect);
    Attach(this, channelPublishStartRequest, this, onReplicatedChannelPublishStartRequest, SkDirect);
    Attach(this, channelPublishStopRequest, this, onReplicatedChannelPublishStopRequest, SkDirect);

    Attach(this, disconnected, this, onFlowDisconnected, SkDirect);

    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecCheck, SkQueued);
}

bool SkFlowNode::replicate()
{
    if (!tcpConnect(flowNodeAddress.c_str(), flowNodePort) || !login(flowNodeName.c_str(), flowNodePassword.c_str()))
        return false;

    for(uint64_t i=0; i<owner->channelsCount(); i++)
    {
        SkAbstractFlowChannel *ch = owner->getChannels()[i];

        if (!ch)
            continue;

        ch->replicate(this);
    }

    return true;
}

SlotImpl(SkFlowNode, onReplicatedChannelAdded)
{
    SilentSlotArgsWarning();

    SkFlowChanID replicatedChanID = Arg_UInt16;

    for(uint64_t i=0; i<owner->channelsCount(); i++)
    {
        SkAbstractFlowChannel *ch = owner->getChannels()[i];

        if (!ch || ch->replicatedID() != replicatedChanID)
            continue;

        replicatedChannels[replicatedChanID] = ch->id();
        ObjectMessage("Channel has a REPLICANT now  [chanID: " << ch->id() << "; replicatedChanID: " << replicatedChanID << "]: " << ch->name() << " -> " << Arg_CStr);
        return;
    }
}

SlotImpl(SkFlowNode, onReplicatedChannelRemoved)
{
    SilentSlotArgsWarning();

    SkFlowChanID replicatedChanID = Arg_UInt16;

    if (replicatedChannels.contains(replicatedChanID))
        return;

    SkAbstractFlowChannel *absCh = owner->getChannelByID(replicatedChannels[replicatedChanID]);

    if (!absCh)
    {
        ObjectError("REPLICATED Channel is NOT valid: " << replicatedChanID);
        return;
    }

    replicatedChannels.remove(replicatedChanID);

    if (absCh->chanType() == StreamingChannel)
    {
        SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
        ch->setReplicantPublishingEnabled(false);
        ch->unsetReplicantID();
        ch->checkPublishState();
    }

    else
        absCh->unsetReplicantID();
}

SlotImpl(SkFlowNode, onReplicatedChannelPublishStartRequest)
{
    SilentSlotArgsWarning();

    SkFlowChanID replicatedChanID = Arg_UInt16;
    SkAbstractFlowChannel *absCh = owner->getChannelByID(replicatedChannels[replicatedChanID]);

    if (!absCh)
    {
        ObjectError("REPLICATED Channel is NOT valid: " << replicatedChanID);
        return;
    }

    if (absCh->chanType() == StreamingChannel)
    {
        SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
        ch->setReplicantPublishingEnabled(true);
        ObjectMessage("Channel REPLICANT requesting to START to publish [chanID: " << ch->id() << "; replicatedChanID: " << replicatedChanID << "]: " << ch->name() << " -> " << Arg_CStr);
        ch->checkPublishState();
    }
}

SlotImpl(SkFlowNode, onReplicatedChannelPublishStopRequest)
{
    SilentSlotArgsWarning();

    SkFlowChanID replicatedChanID = Arg_UInt16;
    SkAbstractFlowChannel *absCh = owner->getChannelByID(replicatedChannels[replicatedChanID]);

    if (!absCh)
    {
        ObjectError("REPLICATED Channel is NOT valid: " << replicatedChanID);
        return;
    }

    if (absCh->chanType() == StreamingChannel)
    {
        SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
        ch->setReplicantPublishingEnabled(false);
        ObjectMessage("Channel REPLICANT requesting to STOP to publish [chanID: " << ch->id() << "; replicatedChanID: " << replicatedChanID << "]: " << ch->name() << " -> " << Arg_CStr);
        ch->checkPublishState();
    }
}

SlotImpl(SkFlowNode, onFlowDisconnected)
{
    SilentSlotArgsWarning();

    ObjectWarning("Clear replicated channels");

    replicatedChannels.clear();

    for(uint64_t i=0; i<owner->channelsCount(); i++)
    {
        SkAbstractFlowChannel *absCh = owner->getChannels()[i];

        if (!absCh)
            continue;

        if (absCh->chanType() == StreamingChannel)
        {
            SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
            ch->setReplicantPublishingEnabled(false);
            ch->unsetReplicantID();
            ch->checkPublishState();
        }

        else
            absCh->unsetReplicantID();
    }

    //internal client resetChannels() will be automatically executed on re-open if there are channels
}

SlotImpl(SkFlowNode, onOneSecCheck)
{
    SilentSlotArgsWarning();

    if (isConnected())
        checkService();

    if (!isConnected() && !flowNodeAddress.isEmpty())
        replicate();
}
