#include "skflowblobsender.h"
#include "skflowrawsvrconnection.h"
#include "skflowpairdatabase.h"
#include "Core/App/skeventloop.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfile.h"

ConstructorImpl(SkFlowBlobSender, SkObject)
{
    db = nullptr;
    flow = nullptr;

    blob = new SkFile(this);

    chanID = -1;

    sendingBufSz = 8192;
    sendingTickInterval = 0.f;

    SignalSet(progress);
    SlotSet(onFastTick);
    SlotSet(onDownloaderDisabled);
    SignalSet(terminated);
}

bool SkFlowBlobSender::start(CStr *filePath,
                             SkFlowChanID channelID,
                             CStr *reqHashID,
                             SkFlowPairsDatabase *channelDB,
                             SkFlowRawSvrConnection *downloader)
{
    if (blob->isOpen())
    {
        ObjectError("Blob download is ALREADY started");
        return false;
    }

    blob->setFilePath(filePath);

    if (!blob->open(SkFileMode::FLM_ONLYREAD))
        return false;

    chanID = channelID;
    hashID = reqHashID;
    db = channelDB;
    flow = downloader;

    if (db->existsProperty("sendingBufSz"))
        sendingBufSz = db->getVariable("sendingBufSz").toFloat();

    if (db->existsProperty("sendingTickInterval"))
        sendingTickInterval = db->getVariable("sendingTickInterval").toFloat();

    Attach(flow, disabled, this, onDownloaderDisabled, SkOneShotQueued);
    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);

    ObjectMessage("Blob download STARTED: " << blob->getFilePath());
    return true;
}

bool SkFlowBlobSender::stop()
{
    if (!blob->isOpen())
    {
        ObjectError("Blob download NOT started yet");
        return false;
    }

    Detach(eventLoop()->fastZone_SIG, pulse, this, onFastTick);

    blob->close();
    terminated();

    ObjectMessage("Blob download TERMINATED");
    return true;
}

SlotImpl(SkFlowBlobSender, onDownloaderDisabled)
{
    SilentSlotArgsWarning();
    stop();
}

SlotImpl(SkFlowBlobSender, onFastTick)
{
    SilentSlotArgsWarning();

    if (!isRunning())
        return;

    if (blob->atEof())
    {
        ObjectMessage("Blob download COMPLETED");
        stop();
        return;
    }

    if ((sendingTickInterval > 0.f) && (sendingTickChrono.stop() < sendingTickInterval))
        return;

    SkDataBuffer b;
    b.append(hashID);

    if (!blob->read(&b, sendingBufSz))
    {
        ObjectMessage("Reading internal ERROR");
        stop();
        return;
    }

    SkDataBuffer pck;
    uint16_t rsp = FRSP_SUBSCRIBED_DATA;
    uint32_t endOfTr = 0;

    pck.setData(&rsp, sizeof(uint16_t));
    pck.append(SkArrayCast::toChar(&chanID), sizeof(SkFlowChanID));

    ULong sz = b.size();
    pck.append(SkArrayCast::toChar(&sz), sizeof(uint32_t));
    pck.append(b.data(), b.size());

    pck.append(SkArrayCast::toChar(&endOfTr), sizeof(uint32_t));

    flow->getSck()->write(&pck);

    SkVariantVector p;
    p << b.size();
    progress(p);

    if (sendingTickInterval > 0)
        sendingTickChrono.start();
}

bool SkFlowBlobSender::isRunning()
{
    return blob->isOpen();
}

CStr *SkFlowBlobSender::filePath()
{
    return blob->getFilePath();
}

SkFlowRawSvrConnection *SkFlowBlobSender::downloader()
{
    return flow;
}

