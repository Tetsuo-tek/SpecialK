#include "skflowservicechannel.h"
#include "skflowasync.h"
#include "skflowpairdatabase.h"
#include "skflowrawsvrconnection.h"

ConstructorImpl(SkFlowServiceChannel, SkAbstractFlowChannel)
{
    chan_t = ServiceChannel;
}

bool SkFlowServiceChannel::setup(SkFlowChanID id, CStr *name)
{
    if (chanID > -1)
    {
        ObjectError("Channel ALREADY inititialized");
        return false;
    }

    init(id, name);
    return true;
}

void SkFlowServiceChannel::onSetPairDatabase()
{
    db->setVariable("chan_t", "ServiceChannel");
}

bool SkFlowServiceChannel::replicate(SkFlowAsync *node)
{
    nodeConnection = node;

    SkString chName(node->userName());
    chName.append(".");
    chName.append(n);

    return false;
}

void SkFlowServiceChannel::onDisable()
{}

