#ifndef SKABSTRACTFLOW_H
#define SKABSTRACTFLOW_H

#include "skflowprotocol.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkDeviceRedistr;
class SkFile;

struct SkFlowChannel
{
    SkFlowChanID chanID;
    SkFlow_T flow_t;
    SkFlowChannel_T chan_t;
    SkVariant_T data_t;
    SkString name;
    SkString hashID;
    //SkString udm;
    SkString mime;
    SkVariant min;
    SkVariant max;
    bool hasHeader;
    bool isPublishingEnabled;
    SkDataBuffer header;
    SkString mpPath;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define ProtocolRecvError(MSG) \
    do \
    { \
        ObjectError("{FATAL} => " << MSG); \
        if (socket->isConnected()) \
            socket->disconnect(); \
    } while(false)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractFlow extends SkObject
{
    SkString lastSeed;

    public:
        void setNodeName(CStr *name);

        bool localConnect(CStr *path);
        bool tcpConnect(CStr *address, UShort port);

        bool connect(CStr *flowUrl);

        //The socket must be connected first
        bool open(SkAbstractSocket *sck);
        void close();

        //THESE ARE SYNC-OPERATIONs (ASync born as Sync)
        bool getLoginSeed();
        bool login(CStr *userName, CStr *userToken);

        bool setCurrentDbName(CStr *dbName);
        bool setVariable(CStr *key, const SkVariant &value);
        bool delVariable(CStr *key);
        bool flushall();

        bool isUnixSocket();
        bool isTcpSocket();
        CStr *socketTypeName();
        CStr  *getUnixPath();
        CStr  *getTcpAddress();
        int16_t getTcpPort();

        SkAbstractSocket *sck();

        bool isConnected();
        bool isAuthorized();

        bool containsChannel(CStr *name);
        bool containsChannel(SkFlowChanID chanID);
        void channels(SkStringList &names);
        SkFlowChannel *channel(CStr *name);
        SkFlowChannel *channel(SkFlowChanID chanID);
        SkFlowChanID channelID(CStr *name);
        uint64_t channelsCount();

        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *channelsIterator();

        CStr *userName();
        CStr *userToken();

        Signal(connected);
        Signal(disconnected);

        Slot(onDisconnect);

    protected:
        AbstractConstructor(SkAbstractFlow, SkObject);
        friend void SkAbstractFlowClose(SkObject *);

        SkFlowProto *p;
        SkAbstractSocket *socket;
        SkString unixPath;
        SkString tcpAddress;
        int16_t tcpPort;
        SkString name;
        SkString token;
        bool auth;

        SkString nodeName;

        SkTreeMap<SkString, SkFlowChannel *> channelsNames;
        SkTreeMap<SkFlowChanID, SkFlowChannel *> channelsIndexes;

        virtual bool onOpenUnixSocket() {return true;}
        virtual bool onOpenTcpSocket()  {return true;}

        virtual bool onLogin()          {return true;}

        virtual void onOpen()           {}
        virtual void onClose()          {}
        virtual void onDisconnected()   {}

        bool open();
        void resetChannels();//automatically executed on open if there are channels
};

#endif // SKABSTRACTFLOW_H
