#include "skabstractflowsat.h"

#include <Core/Containers/skarraycast.h>
#include <Core/System/skosenv.h>
#include <Core/System/Filesystem/skfsutils.h>

AbstractConstructorImpl(SkAbstractFlowSat, SkAbstractWorkerObject)
{
    async = nullptr;

#if defined(ENABLE_HTTP)
    http = nullptr;
    httpCfg.serviceEnabled = false;
#endif

    tickChan = -1;
    tickChanEnabled = false;

    userName = "guest";
    userPasswd = "password";

    workingDirectory = "./";

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
    SlotSet(tickParamsChanged);

    SlotSet(channelAdded);
    SlotSet(channelRemoved);
    SlotSet(channelHeaderSetup);

    SlotSet(channelPublishStartRequest);
    SlotSet(channelPublishStopRequest);

#if defined(ENABLE_HTTP)
    SlotSet(channelRedistrStartRequest);
    SlotSet(channelRedistrStopRequest);
#endif

    SlotSet(onFlowDisconnection);
    SignalSet(flowDisconnected);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowSat::setWorkingDir(CStr *dirPath)
{
    workingDirectory = SkFsUtils::adjustPathEndSeparator(dirPath);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowSat::setLogin(CStr *user, CStr *password)
{
    userName = user;
    userPasswd = password;

    setObjectName(user);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::buildASync(CStr *url)
{
    closeASync();

    async = new SkFlowAsync(this);
    async->setObjectName(this, "Async");
    async->setNodeName(typeName());

    SkString robotAddress;

    if (url)
    {
        robotAddress = url;
        ObjectWarning("Found CUSTOM robot-address -> " << robotAddress);
    }

    else
    {
        AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
        robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");
        ObjectWarning("Found environment variable '$ROBOT_ADDRESS' -> " << robotAddress);
    }

    bool ok = false;

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! SUBSTITUTED WITH connect(..)
    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address: " << robotAddress);
        ok = async->localConnect(robotAddress.c_str());
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        int port = 9000;

        if (robotAddressParsed.count() == 2)
            port = robotAddressParsed.last().toInt();

        ok = async->tcpConnect(robotAddressParsed.first().c_str(), port);

        /*if (ok)
            async->getLoginSeed();*/
    }

    else
    {
        async->destroyLater();
        async = nullptr;
        ObjectError("Robot address is MALFORMED (syntax MUST BE <local|tcp>:<address[:tcpPort]> -> " << robotAddress);
        return false;
    }
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if (!ok)
    {
        async->destroyLater();
        async = nullptr;
        return false;
    }

    skApp->setupSharedComponent(objectName(), this);
    async->setWorker(this);

    {
        SkAbstractListIterator<SkAbstractFlowSubscriber *> *itr = subscribersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->setClient(async);

        delete itr;
    }

    {
        SkAbstractListIterator<SkAbstractFlowPublisher *> *itr = publishersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->setClient(async);

        delete itr;
    }

    AssertKiller(!async->login(userName.c_str(), userPasswd.c_str()));
    AssertKiller(!addStreamingChannel(tickChan, FT_TICK, T_NULL, "Tick"));

    tickParamsChanged();

    Attach(async, disconnected, this, onFlowDisconnection, SkOneShotDirect);

    Attach(async, channelAdded, this, channelAdded, SkDirect);
    Attach(async, channelRemoved, this, channelRemoved, SkDirect);
    Attach(async, channelHeaderSetup, this, channelHeaderSetup, SkDirect);
    Attach(async, channelPublishStartRequest, this, channelPublishStartRequest, SkQueued);
    Attach(async, channelPublishStopRequest, this, channelPublishStopRequest, SkQueued);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)

void SkAbstractFlowSat::setupHttpService(SkFlowHttpServiceConfig &cfg)
{
    httpCfg = cfg;
}

void SkAbstractFlowSat::buildHttpService()
{
    closeHttp();

    if (httpCfg.serviceEnabled)
    {
        http = new SkFlowAsyncHttpService(this);
        http->setClient(async);

        http->listenTo(httpCfg.listenPort, httpCfg.listenAddr.c_str());

        if (httpCfg.wwwDirEnabled && !httpCfg.wwwDir.isEmpty())
        {
            SkString wwwDir = SkFsUtils::adjustPathEndSeparator(httpCfg.wwwDir.c_str());
            http->enableStaticWWW(wwwDir.c_str());
        }

        if (httpCfg.sslEnabled)
            http->enableSsl(httpCfg.sslCertFile.c_str(), httpCfg.sslKeyFile.c_str());

        Attach(http, channelRedistrStartRequest, this, channelRedistrStartRequest, SkQueued);
        Attach(http, channelRedistrStopRequest, this, channelRedistrStopRequest, SkQueued);

        AssertKiller(!http->init(httpCfg.maxConnQueued, httpCfg.idleTimeoutInterval, httpCfg.maxHeaderSize));

        {
            SkAbstractListIterator<SkAbstractFlowPublisher *> *itr = publishersHolder_HTTP.iterator();

            while(itr->next())
                itr->item()->setHttpService(http);

            delete itr;
        }
    }
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowSync *SkAbstractFlowSat::buildSyncClient()
{
    return async->buildSyncClient();
}

SkFlowAsync *SkAbstractFlowSat::buildOptAsyncClient()
{
    if (!async || !async->isConnected())
        return nullptr;

    SkFlowAsync *optAsync = new SkFlowAsync(this);
    optAsync->setObjectName(this, "OptAsync");

    if (async->isTcpSocket())
        AssertKiller(!optAsync->tcpConnect(async->getTcpAddress(), async->getTcpPort()));

    else
        AssertKiller(!optAsync->localConnect(async->getUnixPath()));

    AssertKiller(!optAsync->login(async->userName(), async->userToken()));

    optAsync->setWorker(this);
    optAsync->setCurrentDbName(async->userName());

    return optAsync;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowSat::closeASync()
{
    if (!async)
        return;

    {
        SkAbstractListIterator<SkAbstractFlowSubscriber *> *itr = subscribersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->unSetClient();

        delete itr;
    }

    {
        SkAbstractListIterator<SkAbstractFlowPublisher *> *itr = publishersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->unSetClient();

        delete itr;
    }

    skApp->removeSharedComponent(objectName());

    if (async->isConnected())
        async->close();

    async->destroyLater();
    async = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)
void SkAbstractFlowSat::closeHttp()
{
    if (!http)
        return;

    http->quit();
    http->destroyLater();
    http = nullptr;
}
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowSat::updateCommand(SkWorkerCommand *cmd)
{
    SkString k = cmd->command();
    k.append("_TRANS");

    SkArgsMap m;
    m["owner"] = objectName();
    m["ownerType"] = typeName();

    cmd->toMap(m);

    async->setCurrentDbName(async->userName());
    async->setVariable(k.c_str(), m);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractFlowSat, fastTick)
{
    SilentSlotArgsWarning();

    if (!async || !async->isConnected())
        return;

    while(async->nextData())
    {
        SkFlowChannelData &chData = async->getCurrentData();
        SkAbstractListIterator<SkAbstractFlowSubscriber *> *itr = subscribersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->tick(chData);

        delete itr;

        onFlowDataCome(chData);
    }

    if (tickChanEnabled)
        async->publish(tickChan);

    onFastTick();
}

SlotImpl(SkAbstractFlowSat, slowTick)
{
    SilentSlotArgsWarning();

    if (!async || !async->isConnected())
        return;

    onSlowTick();
}

SlotImpl(SkAbstractFlowSat, oneSecTick)
{
    SilentSlotArgsWarning();

    if (checkChrono.stop() > 2.)
    {
        if (async && async->isConnected())
            async->checkService();

        onCheckInternals();
        checkChrono.start();
    }

    if (!async || !async->isConnected())
        return;

    onOneSecTick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractFlowSat, tickParamsChanged)
{
    SilentSlotArgsWarning();

    if (!async || !async->isConnected())
        return;

    async->setCurrentDbName(async->userName());
    async->setVariable("fastTickTime", eventLoop()->getFastInterval());
    async->setVariable("slowTickTime", eventLoop()->getSlowInterval());
    async->setVariable("tickMode", eventLoop()->getTimerMode());

    onTickParamsChanged();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractFlowSat, channelAdded)
{
    SilentSlotArgsWarning();
    onChannelAdded(Arg_Int16);
}

SlotImpl(SkAbstractFlowSat, channelRemoved)
{
    SilentSlotArgsWarning();
    onChannelRemoved(Arg_Int16);
}

SlotImpl(SkAbstractFlowSat, channelHeaderSetup)
{
    SilentSlotArgsWarning();
    onChannelHeaderSetup(Arg_Int16);
}

SlotImpl(SkAbstractFlowSat, channelPublishStartRequest)
{
    SilentSlotArgsWarning();

    SkFlowChanID chanID = Arg_Int16;

    if (chanID == tickChan)
        tickChanEnabled = true;

    else
        onChannelPublishStartRequest(chanID);
}

SlotImpl(SkAbstractFlowSat, channelPublishStopRequest)
{
    SilentSlotArgsWarning();
    SkFlowChanID chanID = Arg_Int16;

    if (chanID == tickChan)
        tickChanEnabled = false;

    else
        onChannelPublishStopRequest(chanID);
}

#if defined(ENABLE_HTTP)

SlotImpl(SkAbstractFlowSat, channelRedistrStartRequest)
{
    SilentSlotArgsWarning();
    SkFlowChanID chanID = Arg_Int16;

    if (chanID == tickChan)
        tickChanEnabled = true;

    else
        onChannelRedistrStartRequest(chanID);
}

SlotImpl(SkAbstractFlowSat, channelRedistrStopRequest)
{
    SilentSlotArgsWarning();
    SkFlowChanID chanID = Arg_Int16;

    if (chanID == tickChan)
        tickChanEnabled = false;

    else
        onChannelRedistrStopRequest(chanID);
}

#endif


SlotImpl(SkAbstractFlowSat, onFlowDisconnection)
{
    SilentSlotArgsWarning();

    ObjectWarning("Flow disconnected");
    flowDisconnected();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*SlotImpl(SkAbstractFlowSat, onDisconnection)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting on disconnection..");
    closeSatService();
    skApp->quit();
}

SlotImpl(SkAbstractFlowSat, quitSatApplication)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting ..");
    closeSatService();
    skApp->quit();
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::isConnected()
{
    return async && async->isConnected();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::setCurrentDbName(CStr *dbName)
{
    return async->setCurrentDbName(dbName);
}

bool SkAbstractFlowSat::setVariable(CStr *key, const SkVariant &value, CStr *temporaryCurrentDbName)
{
    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->setVariable(key, value);

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->setVariable(key, value);

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

bool SkAbstractFlowSat::delVariable(CStr *key, CStr *temporaryCurrentDbName)
{
    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->delVariable(key);

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->delVariable(key);

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

bool SkAbstractFlowSat::flushall(CStr *temporaryCurrentDbName)
{
    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->flushall();

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->flushall();

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime, SkArgsMap *props)
{
    return async->addStreamingChannel(chanID, flow_t, data_t, name, mime, props);
}

bool SkAbstractFlowSat::removeChannel(SkFlowChanID chanID)
{
    return async->removeChannel(chanID);
}

bool SkAbstractFlowSat::setChannelHeader(SkFlowChanID chanID, SkDataBuffer &data)
{
    return async->setChannelHeader(chanID, data);
}

bool SkAbstractFlowSat::setChannelProperties(SkFlowChanID chanID, SkArgsMap &props)
{
    return async->setChannelProperties(chanID, props);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, int8_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, uint8_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, int16_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, uint16_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, int32_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, uint32_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, int64_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, uint64_t v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, float v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, double v)
{
    return async->publish(chanID, v);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, CStr *str)
{
    return async->publish(chanID, str);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, SkString &str)
{
    return async->publish(chanID, str);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, SkVariantList &l)
{
    return async->publish(chanID, l);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, SkArgsMap &m)
{
    return async->publish(chanID, m);
}

bool SkAbstractFlowSat::publish(SkFlowChanID chanID, CVoid *data, uint64_t sz)
{
    return async->publish(chanID, data, sz);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)

bool SkAbstractFlowSat::httpRedistr(SkFlowChanID chanID, SkDataBuffer &data)
{
    if (!http)
        return false;

    return http->redistr(chanID, data);
}

bool SkAbstractFlowSat::httpRedistr(SkFlowChanID chanID, CVoid *data, uint64_t sz)
{
    if (!http)
        return false;

    return http->redistr(chanID, data, sz);
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::isSubscribed(SkFlowChanID chanID)
{
    return async->isSubscribed(chanID);
}

bool SkAbstractFlowSat::subscribeChannel(SkFlowChannel *ch)
{
    return async->subscribeChannel(ch);
}

bool SkAbstractFlowSat::subscribeChannel(CStr *name, SkFlowChanID &chanID)
{
    return async->subscribeChannel(name, chanID);
}

bool SkAbstractFlowSat::unsubscribeChannel(SkFlowChanID chanID)
{
    return async->unsubscribeChannel(chanID);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlowSat::setupSubscriber(SkAbstractFlowSubscriber *subscriber)
{
    if (async)
        subscriber->setClient(async);

    subscribersHolder_ASYNC << subscriber;
}

void SkAbstractFlowSat::setupPublisher(SkAbstractFlowPublisher *publisher)
{
    if (async)
        publisher->setClient(async);

    publishersHolder_ASYNC << publisher;

#if defined(ENABLE_HTTP)

    if (http)
        publisher->setHttpService(http);

    publishersHolder_HTTP << publisher;

#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlowSat::containsChannel(CStr *name)
{
    return async->containsChannel(name);
}

bool SkAbstractFlowSat::containsChannel(SkFlowChanID chanID)
{
    return async->containsChannel(chanID);
}

void SkAbstractFlowSat::channels(SkStringList &names)
{
    return async->channels(names);
}

SkFlowChannel *SkAbstractFlowSat::channel(CStr *name)
{
    return async->channel(name);
}

SkFlowChannel *SkAbstractFlowSat::channel(SkFlowChanID chanID)
{
    return async->channel(chanID);
}

int64_t SkAbstractFlowSat::channelID(CStr *name)
{
    return async->channelID(name);
}

uint64_t SkAbstractFlowSat::channelsCount()
{
    return async->channelsCount();
}

SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *SkAbstractFlowSat::channelsIterator()
{
    return async->channelsIterator();
}

CStr *SkAbstractFlowSat::workingDir()
{
    return workingDirectory.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

