#ifndef SKABSTRACTFLOWCHANNEL_H
#define SKABSTRACTFLOWCHANNEL_H

#include "Core/Containers/skset.h"
#include "skflowservicerequeststatus.h"

class SkAbstractFlowConnection;
class SkFlowAsync;
class SkFlowPairsDatabase;
class SkAbstractWorkerObject;

/*INHERITED SERVICE

    SERVICE             FS                          CLIENT
    open service
                                                    send request
                        make request hash
                        send request to service
    receive request
    send response
                        send response to client
                                                    receive the response
    [..]
    close service
*/

class SkAbstractFlowChannel extends SkObject
{
    ULong totalInputBytes;
    ULong totalOutputBytes;

    public:
        void setOwner(SkAbstractFlowConnection *conn);
        void setWorker(SkAbstractWorkerObject *localWorker);
        void setPairDatabase(SkFlowPairsDatabase *pairDatabase);

        virtual bool replicate(SkFlowAsync *node)           = 0;

        void disable();

        bool isReplicated();
        void unsetReplicantID();

        SkFlowChannel_T chanType();
        SkFlowChanID id();
        SkFlowChanID replicatedID();
        CStr *name();
        CStr *hashID();

        SkAbstractFlowConnection *owner();
        SkFlowPairsDatabase *pairDatabase();
        CStr *serverName();

        //INHERITED SERVICE ON EACH CHANNEL TYPE
        SkFlowServiceRequestStatus *openRequest(SkFlowRawSvrConnection *requester, CStr *cmd, SkVariant &value);
        bool closeRequest(CStr *hashStr, SkVariant &response);
        void destroyRequest(CStr *hashStr);
        bool isPendingRequest(CStr *hashStr);

        Slot(onOneSecTick);

        Slot(asyncCloseCoreRequest);
        Slot(onLocalWrkEvaluatedCmd);
        //

    protected:
        AbstractConstructor(SkAbstractFlowChannel, SkObject);

        SkFlowChannel_T chan_t;
        SkString svrName;
        SkFlowChanID chanID;
        SkString hashChanID;
        SkString n;
        SkFlowChanID replicatedChanID;
        SkAbstractFlowConnection *o;
        SkAbstractWorkerObject *localWrk;
        SkFlowAsync *nodeConnection;

        ULong inputSpeed;
        ULong outputSpeed;

        SkFlowPairsDatabase *db;

        void init(SkFlowChanID id, CStr *name);

        //INHERITED SERVICE ON EACH CHANNEL TYPE
        SkTreeMap<SkString, SkFlowServiceRequestStatus *> requesters;//hash, requester

        //TO channel-owner SAT
        void sendServiceRequest(CStr *hash, CStr *cmd, SkVariant &val);//ASYNC

        //FROM CoreChannel (asynchronous)
        void closeCoreRequest(SkFlowServiceRequestStatus &st, bool hasError, CStr *msg);
        void closeCoreRequest(SkFlowServiceRequestStatus &st, bool hasError, CStr *msg, const SkVariant &optData);
        //

        virtual void onSetPairDatabase()                   = 0;
        virtual void onDisable()                           = 0;

        virtual bool isCoreCommand(CStr *)                                              {return false;}
        virtual bool onCoreCommand(CStr *, SkVariant &, SkFlowServiceRequestStatus &)   {return false;}
};

#endif // SKABSTRACTFLOWCHANNEL_H
