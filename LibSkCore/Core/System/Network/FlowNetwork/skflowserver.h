#ifndef SKFLOWSERVER_H
#define SKFLOWSERVER_H

#include "Core/System/Time/skdatetime.h"

#include "skabstractflowauthenticator.h"

#if defined(ENABLE_REDIS)
    #include "skflowredisexport.h"
#endif

#include "skflowpairdatabase.h"
#include "skflowrawsvrconnection.h"
#include "skflowredistrchannel.h"
#include "skflowservicechannel.h"
#include "skflowblobchannel.h"
#include "skflownode.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkLocalServer;
class SkTcpServer;
class SkAbstractServer;
class SkFlowAsync;
class SkRedisSync;
class SkRedisSubscriber;

class SkFlowServer extends SkObject
{
    SkString svrName;

    SkAbstractWorkerObject *localWrk;

    SkAbstractFlowAuthenticator *auth;
    SkLocalServer *localSvr;
    SkTcpServer *tcpSvr;

#if defined(ENABLE_REDIS)
    SkFlowRedisExport *r;
#endif

    SkFlowChanID pairsChan;

    SkString dbPath;
    SkString usersPath;

    SkTreeMap<SkString, SkFlowPairsDatabase *> databases;
    SkVariant nullValue;

    SkList<SkFlowRawSvrConnection *> connections;
    SkList<SkFlowRawSvrConnection *> deathReign;

    SkVector<SkAbstractFlowChannel *> channels;
    SkQueue<SkFlowChanID> freeChans;
    SkTreeMap<SkString, SkAbstractFlowChannel *> namedChannels;
    SkTreeMap<SkFlowChanID, SkFlowChanID> replicatedChannels;//replicated, local

    SkFlowNode *node;

    public:
        Constructor(SkFlowServer, SkObject);

        bool init(CStr *serverName, SkAbstractWorkerObject *localWorker, CStr *databasesPath, CStr *usersHomePath);
        void setAuthenticator(SkAbstractFlowAuthenticator *authenticator);//WHITOUT auth ALL IS PERMITTED
        void close();

        bool initLocalServer(CStr *path, int maxQueuedConnections);
        CStr *getDeliveryLocalPath();

        bool initTcpServer(CStr *address, uint16_t port, int maxQueuedConnections);

        bool replicate(CStr *address, Short port, CStr *name, CStr *password);

#if defined(ENABLE_REDIS)
        bool enableRedis(CStr *serverHostname, uint16_t serverPort);
        bool isRedisEnabled();
#endif

        //bool buildPairDatabase(CStr *name, CStr *pathLocation, SkTreeMap<SkString, SkVariant> &database);
        bool addPairDatabase(CStr *name);
        bool delPairDatabase(CStr *name);
        void pairDbNames(SkStringList &names);
        bool existsOptionalPairDb(CStr *name);
        SkFlowPairsDatabase *getPairDatabase(CStr *name);

        SkFlowChanID addStreamingChannel(SkFlow_T flowType, SkVariant_T dataType, CStr *name, CStr *mime=nullptr, SkArgsMap *props=nullptr, SkAbstractFlowConnection *owner=nullptr);
        SkFlowChanID addServiceChannel(CStr *name, SkArgsMap *props=nullptr, SkAbstractFlowConnection *owner=nullptr);
        SkFlowChanID addBlobChannel(CStr *name, SkArgsMap *props=nullptr, SkAbstractFlowConnection *owner=nullptr);

        bool removeChannel(SkFlowChanID chanID);

        bool isChannelValid(SkFlowChanID chanID);
        SkFlowChannel_T getChannelType(SkFlowChanID chanID);
        SkAbstractFlowChannel *getChannelByID(SkFlowChanID chanID);
        bool containsChannel(CStr *name);
        SkAbstractFlowChannel *getChannelByName(CStr *name);
        uint64_t getSubscribersCount(SkFlowChanID chanID);
        uint64_t channelsCount();//contains also channels removed
        SkAbstractFlowChannel **getChannels();//contains also channels removed, having its position as nullptr

        CStr *serverName();
        SkAbstractWorkerObject *localWortker();
        SkAbstractFlowAuthenticator *authenticator();

        SkVariant &nv();

        Slot(onFlowConnection);
        Slot(onFlowDisconnection);
        Signal(addedFlowConnection);
        Signal(removedFlowConnection);
        Slot(onPairChange);
        Signal(pairChanged);
        Signal(addedChannel);
        Signal(removedChannel);
        Signal(setChannelHeader);
        Slot(onSlowTick);
        Slot(onOneSecTick);

    private:
        void addSockets(SkAbstractServer *svr);
        void removeSocket(SkAbstractSocket *socket);

        SkFlowChanID channelsCommonSetup(CStr *name, SkAbstractFlowChannel *ch);
        bool channelsCommonInit(SkAbstractFlowChannel *ch, SkArgsMap *props, SkAbstractFlowConnection *owner);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif // SKFLOWSERVER_H
