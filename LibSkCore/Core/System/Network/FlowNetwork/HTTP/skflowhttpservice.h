#ifndef SKFLOWHTTPSERVICE_H
#define SKFLOWHTTPSERVICE_H

#include "Core/System/Network/FlowNetwork/skflowserver.h"

#if defined(ENABLE_HTTP)

class SkHttpService;
class SkFlowWebAppMP;
class SkFlowPairsMP;
class SkFsMountPoint;
class SkFlowMP;

class SkFlowHttpService extends SkObject
{
    SkFlowServer *fs;
    SkAbstractFlowAuthenticator *auth;
    SkHttpService *service;
    SkFlowWebAppMP *webAppMp;
    SkFlowPairsMP *pairsMp;
    SkFsMountPoint *wwwMp;
    SkFlowMP *flowMp;

    SkString listenAddress;
    UShort listenPort;

    SkString wwwPath;

    bool ssl;
    SkString sslCert;
    SkString sslKey;

    public:
        Constructor(SkFlowHttpService, SkObject);

        void listenTo(UShort port, CStr *address="0.0.0.0");
        void enableStaticWWW(CStr *realDirPath);
        void enableSsl(CStr *sslCertFilePath, CStr *sslKeyFilePath);

        bool init(SkFlowServer *flowServer, UShort maxQueuedConnections=100, UShort idleTimeoutInterval=30, ULong maxHeaderSize=100000);
        void quit();

    private:
        bool initFsMP();
        bool initWebAppMP();
        bool initPairsMP();
        bool initFlowMP();

        void resetService();
};

#endif

#endif // SKFLOWHTTPSERVICE_H
