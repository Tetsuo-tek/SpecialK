#ifndef SKFLOWASYNCHTTPSERVICE_H
#define SKFLOWASYNCHTTPSERVICE_H

#include "Core/System/Network/FlowNetwork/skflowasync.h"

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHttpService;
class SkFsMountPoint;
class SkRawRedistrMountPoint;
class SkPartRedistrMountPoint;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkFlowRedistrMP
{
    SkFlowChannel *ch;
    SkString path;
    SkRawRedistrMountPoint *r;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowAsyncHttpService extends SkObject
{
    SkFlowAsync *async;

    SkHttpService *service;
    SkFsMountPoint *wwwMp;

    SkString listenAddress;
    UShort listenPort;

    SkString wwwPath;

    bool ssl;
    SkString sslCert;
    SkString sslKey;

    SkTreeMap<SkFlowChanID, SkFlowRedistrMP *> redistrs;

    public:
        Constructor(SkFlowAsyncHttpService, SkObject);

        void setClient(SkFlowAsync *flowAsync);

        void listenTo(UShort port, CStr *address="0.0.0.0");
        void enableStaticWWW(CStr *realDirPath);
        void enableSsl(CStr *sslCertFilePath, CStr *sslKeyFilePath);

        bool init(UShort maxQueuedConnections=100, UShort idleTimeoutInterval=30, ULong maxHeaderSize=100000);
        void quit();

        bool redistr(SkFlowChanID chanID, SkDataBuffer &data);
        bool redistr(SkFlowChanID chanID, CVoid *data, ULong sz);

        SkRawRedistrMountPoint *redistrMountPoint(SkFlowChanID chanID);

        Slot(onPublisherAdded);
        Slot(onChannelRemoved);

        Slot(onChannelHeaderSetup);

        Slot(onStreamTargetAdded);
        Slot(onStreamTargetRemoved);

        Signal(channelRedistrStartRequest);
        Signal(channelRedistrStopRequest);

    private:
        bool initFsMP();
        void resetService();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWASYNCHTTPSERVICE_H
