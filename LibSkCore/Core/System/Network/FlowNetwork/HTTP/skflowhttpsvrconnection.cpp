#include "skflowhttpsvrconnection.h"

#if defined(ENABLE_HTTP)

#include "Core/Containers/skarraycast.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowHttpSvrConnection, SkAbstractFlowConnection)
{
    sck = nullptr;

    SlotSet(onTextReadyRead);
    SlotSet(onDataReadyRead);
    SlotSet(onDisconnected);

    SlotSet(onChannelAdded);
    SlotSet(onChannelRemoved);
    SlotSet(onChannelHeaderSetup);
    SlotSet(onPairChange);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpSvrConnection::setup(SkFlowServer *flowServer, SkWebSocket *socket)
{
    svr = flowServer;
    sck = socket;

    Attach(sck, textReadyRead, this, onTextReadyRead, SkQueued);
    Attach(sck, binaryReadyRead, this, onDataReadyRead, SkQueued);
    Attach(sck, disconnected, this, onDisconnected, SkDirect);

    Attach(svr, addedChannel, this, onChannelAdded, SkQueued);
    Attach(svr, removedChannel, this, onChannelRemoved, SkQueued);
    Attach(svr, setChannelHeader, this, onChannelHeaderSetup, SkQueued);
    Attach(svr, pairChanged, this, onPairChange, SkDirect);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractDevice *SkFlowHttpSvrConnection::getSck()
{
    return sck;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpSvrConnection::sendEvtPck(CStr *evtName, SkArgsMap &evt)
{
    if (!sck || !sck->isConnected())
        return;

    evt["evt"] = evtName;

    SkString json;
    evt.toString(json);

    sck->setSendingMode(WSM_TEXT);
    sck->write(json);
}

void SkFlowHttpSvrConnection::sendData(SkDataBuffer &data)
{
    if (!sck || !sck->isConnected())
        return;

    sck->setSendingMode(WSM_BINARY);
    sck->write(&data);
}

void SkFlowHttpSvrConnection::sendChannelData(SkFlowChanID chanID, SkDataBuffer &data)
{
    if (!sck || !sck->isConnected())
        return;

    data.prepend(SkArrayCast::toChar(&chanID), sizeof(SkFlowChanID));//BIN

    sck->setSendingMode(WSM_BINARY);
    sck->write(&data);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define SendError(LOGITEM) \
    FakeSingleLine(stringstream stream; stream << LOGITEM; sendError(stream.str().data());)

void SkFlowHttpSvrConnection::sendError(CStr *description)
{
    SkArgsMap errPck;
    errPck["description"] = description;
    sendEvtPck("ERR", errPck);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowHttpSvrConnection, onTextReadyRead)
{
    SilentSlotArgsWarning();

    if (!sck || !sck->isConnected())
        return;

    SkString json;
    sck->read(json);

    if (!currentCmdPck.fromString(json.c_str()))
        return;

    analyze();
}

SlotImpl(SkFlowHttpSvrConnection, onDataReadyRead)
{
    SilentSlotArgsWarning();

    if (!sck || !sck->isConnected())
        return;

    SkDataBuffer b;
    sck->read(&b);

    cout << "!!!! " << b.size() << "\n";
    return;

    SkString channel;
    channel.append(b.data(), 5);//TEXT
    SkFlowChanID chanID = channel.toInt();
    SkAbstractFlowChannel *absCh = svr->getChannelByID(chanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        SendError("Cannot publish on chanID: " << chanID);
        return;
    }

    if (absCh->owner() == this)
    {
        SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
        ch->publish(&b.data()[5], b.size()-5);
    }

    else
        SendError("Trying to publish on NOT-OWNED channel: " << absCh->name());

}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowHttpSvrConnection, onDisconnected)
{
    SilentSlotArgsWarning();

    /*Detach(svr, addedChannel, this, onChannelAdded);
    Detach(svr, removedChannel, this, onChannelRemoved);
    Detach(svr, setChannelHeader, this, onChannelHeaderSetup);
    Detach(svr, pairChanged, this, onPairChange);*/

    sck = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowHttpSvrConnection, onChannelAdded)
{
    SilentSlotArgsWarning();

    if (!sck->isConnected())
        return;

    SkFlowChanID chanID = Arg_Int16;
    SkString name = Arg_String;
    SkFlowChannel_T chan_t = Arg_Enum(SkFlowChannel_T);

    if (chan_t == StreamingChannel)
        ObjectDebug("ADDED StreamingChannel NOTIFY: " << chanID << " [" << name << "]");

    else
        ObjectDebug("ADDED ServiceChannel NOTIFY: " << chanID << " [" << name << "]");

    notifyAddedChannel(chanID);
}

SlotImpl(SkFlowHttpSvrConnection, onChannelRemoved)
{
    SilentSlotArgsWarning();

    if (!sck->isConnected())
        return;

    SkFlowChanID chanID = Arg_Int16;
    SkString name = Arg_String;
    SkFlowChannel_T chan_t = Arg_Enum(SkFlowChannel_T);

    if (chan_t == StreamingChannel)
        ObjectDebug("REMOVED StreamingChannel NOTIFY: " << chanID << " [" << name << "]");

    else
        ObjectDebug("REMOVED ServiceChannel NOTIFY: " << chanID << " [" << name << "]");

    notifyRemovedChannel(chanID);
}

SlotImpl(SkFlowHttpSvrConnection, onChannelHeaderSetup)
{
    SilentSlotArgsWarning();
}

SlotImpl(SkFlowHttpSvrConnection, onPairChange)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpSvrConnection::notifyAddedChannel(SkFlowChanID chanID)
{
}

void SkFlowHttpSvrConnection::notifyRemovedChannel(SkFlowChanID chanID)
{
    ownedChannels.remove(chanID);
    registeredGrabChannels.remove(chanID);

    //
}

void SkFlowHttpSvrConnection::notifyChannelHeader(SkFlowChanID chanID)
{
}

void SkFlowHttpSvrConnection::notifyChannelReqPublishStart(SkFlowChanID chanID)
{
}

void SkFlowHttpSvrConnection::notifyChannelReqPublishStop(SkFlowChanID chanID)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpSvrConnection::onLogin()
{
    SkArgsMap evt;
    sendEvtPck("LOGGED_IN", evt);

    /*SkDataBuffer d;
    SkFlowChanID t = 515;
    d.setData(&t, sizeof(SkFlowChanID));
    sendData(d);*/
}

void SkFlowHttpSvrConnection::onDisable()
{
    //
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpSvrConnection::analyze()
{
    currentCmd = currentCmdPck["cmd"].toString();

    if (!authorized)
        recvLogin();

    else
    {
        if (currentCmd == "addDatabase")
            addDatabase();

        else if (currentCmd == "setCurrentDatabase")
            setCurrentDatabase();

        else if (currentCmd == "setVariable")
            recvVariable();

        else if (currentCmd == "delVariable")
            removeVariable();

        else if (currentCmd == "varImport")
            varImport();

        else if (currentCmd == "varDeport")
            varDeport();

        else if (currentCmd == "addStreamingChannel")
            addStreamingChannel();

        else if (currentCmd == "addServiceChannel")
            addServiceChannel();

        else if (currentCmd == "removeChannel")
            removeChannel();

        else if (currentCmd == "subscribe")
            subscribe();

        else if (currentCmd == "unsubscribe")
            unsubscribe();

        else if (currentCmd == "attach")
            attachChannel();

        else if (currentCmd == "detach")
            detachChannel();
    }
}

void SkFlowHttpSvrConnection::recvLogin()
{
    if (currentCmd != "login")
    {
        SkString json;
        currentCmdPck.toString(json, true);
        SendError("You MUST login first!");
        return;
    }

    loginName = currentCmdPck["user"].toString();
    loginToken = currentCmdPck["token"].toString();

    if (!authorize())
        sck->disconnect();

    //svr->tokenAuth(this);
}

void SkFlowHttpSvrConnection::addDatabase()
{
    SkString dbName = currentCmdPck["dbName"].toString();

    if (!svr->addPairDatabase(dbName.c_str()))
        SendError("Cannot create database: " << dbName);
}

void SkFlowHttpSvrConnection::setCurrentDatabase()
{
    SkString dbName = currentCmdPck["dbName"].toString();
    currentDatabase = svr->getPairDatabase(dbName.c_str());

    if (!currentDatabase)
        SendError("Cannot set current-database: " << dbName);
}

void SkFlowHttpSvrConnection::recvVariable()
{
    SkString k = currentCmdPck["key"].toString();
    SkVariant &v = currentCmdPck["val"];

    if (currentDatabase)
        currentDatabase->setVariable(k.c_str(), v);

    else
        SendError("Current-database is NOT set; cannot set new variable: " << k);
}

void SkFlowHttpSvrConnection::removeVariable()
{
    SkString k = currentCmdPck["key"].toString();

    if (currentDatabase)
        currentDatabase->delVariable(k.c_str());

    else
        SendError("Current-database is NOT set; cannot remove variable: " << k);
}

void SkFlowHttpSvrConnection::varImport()
{
    SkString variable = currentCmdPck["dbName"].toString();
    variable.append(".");
    variable.append(currentCmdPck["key"].toString());

    if (!importedPairs.add(variable))
        SendError("Variable ALREADY imported: " << variable);
}

void SkFlowHttpSvrConnection::varDeport()
{
    SkString variable = currentCmdPck["dbName"].toString();
    variable.append(".");
    variable.append(currentCmdPck["key"].toString());

    if (!importedPairs.remove(variable))
        SendError("Variable to deport NOT found: " << variable);
}

void SkFlowHttpSvrConnection::addStreamingChannel()
{
    SkString flowType = currentCmdPck["flow_t"].toString();
    SkFlow_T flow_t = SkFlowProto::flowTypeToBin(flowType.c_str());

    SkString dataType = currentCmdPck["data_t"].toString();
    SkVariant_T data_t = SkVariant::variantTypeFromName(dataType.c_str());

    SkString name = currentCmdPck["name"].toString();
    SkString mime = currentCmdPck["mime"].toString();

    SkString chName(loginName);
    chName.append(".");
    chName.append(name);
    SkFlowChanID chanID = svr->addStreamingChannel(flow_t, data_t, chName.c_str(), mime.c_str());

    if (chanID > -1)
    {
        //svr->getChannels()[chanID]->setOwner(this);
        ownedChannels.add(chanID);
    }

    else
        SendError("Cannot add streaming-channel: " << chName);
}

void SkFlowHttpSvrConnection::addServiceChannel()
{

}

void SkFlowHttpSvrConnection::removeChannel()
{
    SkFlowChanID chanID = currentCmdPck["chanID"].toInt();

    if (!svr->removeChannel(chanID))
        SendError("Cannot remove chanID: " << chanID);
}

void SkFlowHttpSvrConnection::subscribe()
{
    SkFlowChanID chanID = currentCmdPck["chanID"].toInt();
    SkAbstractFlowChannel *absCh = svr->getChannelByID(chanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        SendError("Cannot subscribe chanID: " << chanID);
        return;
    }

    subscribedChannels.add(absCh->id());

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
    //ch->addSubscriber(this);
}

void SkFlowHttpSvrConnection::unsubscribe()
{
    SkFlowChanID chanID = currentCmdPck["chanID"].toInt();
    SkAbstractFlowChannel *absCh = svr->getChannelByID(chanID);

    if (!absCh || absCh->chanType() != StreamingChannel)
    {
        SendError("Cannot unsubscribe chanID: " << chanID);
        return;
    }

    subscribedChannels.remove(absCh->id());

    SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(absCh);
    //ch->removeSubscriber(this);
}

void SkFlowHttpSvrConnection::attachChannel()
{
    SkFlowChanID sourceID = currentCmdPck["sourceChanID"].toInt();
    SkFlowRedistrChannel *src = dynamic_cast<SkFlowRedistrChannel *>(svr->getChannelByID(sourceID));

    SkFlowChanID targetID = currentCmdPck["targetChanID"].toInt();
    SkFlowRedistrChannel *tgt = dynamic_cast<SkFlowRedistrChannel *>(svr->getChannelByID(sourceID));

    if (!src || !tgt)
    {
        SendError("Attaching channels MUST be valid for streaming: " << sourceID << ", " << targetID);
        return;
    }

    if (!src->attach(tgt))
    {
        SendError("Cannot ATTACH channels: " << sourceID << ", " << targetID);
        return;
    }
}

void SkFlowHttpSvrConnection::detachChannel()
{
    SkFlowChanID sourceID = currentCmdPck["sourceChanID"].toInt();
    SkFlowRedistrChannel *src = dynamic_cast<SkFlowRedistrChannel *>(svr->getChannelByID(sourceID));

    SkFlowChanID targetID = currentCmdPck["targetChanID"].toInt();
    SkFlowRedistrChannel *tgt = dynamic_cast<SkFlowRedistrChannel *>(svr->getChannelByID(sourceID));

    if (!src || !tgt)
    {
        SendError("Detaching channels MUST be valid streaming: " << sourceID << ", " << targetID);
        return;
    }

    if (!src->detach(tgt))
    {
        SendError("Cannot DETACH channels: " << sourceID << ", " << targetID);
        return;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
