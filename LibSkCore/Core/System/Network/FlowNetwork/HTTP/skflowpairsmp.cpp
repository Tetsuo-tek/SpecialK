#include "skflowpairsmp.h"

#if defined(ENABLE_HTTP)

#include "Core/System/Network/FlowNetwork/skflowpairdatabase.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

ConstructorImpl(SkFlowPairsMP, SkGenericMountPoint)
{
    fs = nullptr;
    service = nullptr;

    SlotSet(onPageRequest);

    Attach(this, accepted, this, onPageRequest, SkQueued);
}

void SkFlowPairsMP::init(SkFlowServer *flowServer, SkHttpService *httpService)
{
    fs = flowServer;
    service = httpService;
}

SlotImpl(SkFlowPairsMP, onPageRequest)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);

    if (!httpSck->url().getQueryMap().contains("db"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkString dbName = httpSck->url().getQueryMap()["db"].toString();

    if (dbName != "Main" && !fs->existsOptionalPairDb(dbName.c_str()))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
        return;
    }

    if (!httpSck->url().getQueryMap().contains("cmd"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkFlowPairsDatabase *db = fs->getPairDatabase(dbName.c_str());
    SkString cmd = httpSck->url().getQueryMap()["cmd"].toString();

    SkString json;

    if (cmd == "keys")
    {
        SkStringList keys;
        db->variablesKeys(keys);

        SkVariant v = keys;

        if (!v.toJson(json, true))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
            return;
        }
    }

    else if (cmd == "dump")
    {
        SkVariant v;
        db->dump(v);

        if (!v.toJson(json, true))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
            return;
        }
    }

    else
    {
        if (!httpSck->url().getQueryMap().contains("key"))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
            return;
        }

        SkString key = httpSck->url().getQueryMap()["key"].toString();

        if (!db->existsVariable(key.c_str()))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
            return;
        }

        if (cmd == "get")
        {
            SkVariant &v = db->getVariable(key.c_str());

            if (!v.toJson(json, true))
            {
                service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
                return;
            }
        }

        else if (cmd == "del")
        {
            if (!db->delVariable(key.c_str()))
            {
                service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
                return;
            }

            httpSck->send(SkHttpStatusCode::Ok);
            return;
        }
    }

    httpSck->send(json, "application/json", SkHttpStatusCode::Ok);
}

#endif
