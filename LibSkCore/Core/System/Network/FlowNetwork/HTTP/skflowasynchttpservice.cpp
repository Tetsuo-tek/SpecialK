#include "skflowasynchttpservice.h"

#if defined(ENABLE_HTTP)

#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/skdeviceredistr.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpservice.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowAsyncHttpService, SkObject)
{
    async = nullptr;

    service = nullptr;
    wwwMp = nullptr;
    listenPort = 0;
    listenAddress = "0.0.0.0";
    ssl = false;

    SlotSet(onPublisherAdded);
    SlotSet(onChannelRemoved);

    SlotSet(onChannelHeaderSetup);

    SlotSet(onStreamTargetAdded);
    SlotSet(onStreamTargetRemoved);

    SignalSet(channelRedistrStartRequest);
    SignalSet(channelRedistrStopRequest);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAsyncHttpService::setClient(SkFlowAsync *flowAsync)
{
    async = flowAsync;
    setObjectName(async, "Http");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAsyncHttpService::listenTo(UShort port, CStr *address)
{
    listenPort = port;
    listenAddress = address;
}

void SkFlowAsyncHttpService::enableStaticWWW(CStr *realDirPath)
{
    wwwPath = SkFsUtils::adjustPathEndSeparator(realDirPath);
}

void SkFlowAsyncHttpService::enableSsl(CStr *sslCertFilePath, CStr *sslKeyFilePath)
{
    ssl = true;
    sslCert = sslCertFilePath;
    sslKey = sslKeyFilePath;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/App/skeventloop.h"

bool SkFlowAsyncHttpService::init(UShort maxQueuedConnections, UShort idleTimeoutInterval, ULong maxHeaderSize)
{
    service = new SkHttpService(this);
    service->setObjectName(this, "Service");
    service->setup(async->userName(), ssl, maxHeaderSize, idleTimeoutInterval);

    if (ssl)
        service->sslSvr()->initSslCtx(sslCert.c_str(), sslKey.c_str());

    if (!initFsMP())
    {
        resetService();
        return false;
    }

    if (!service->start(listenAddress.c_str(), listenPort, maxQueuedConnections))
    {
        resetService();
        return false;
    }

    Attach(async, publisherAdded, this, onPublisherAdded, SkDirect);
    Attach(async, channelRemoved, this, onChannelRemoved, SkDirect);
    Attach(async, channelHeaderSetup, this, onChannelHeaderSetup, SkDirect);

    return true;
}

void SkFlowAsyncHttpService::quit()
{
    if (!service)
        return;

    //async->delVariable("httpService");

    Detach(async, publisherAdded, this, onPublisherAdded);
    Detach(async, channelRemoved, this, onChannelRemoved);
    Detach(async, channelHeaderSetup, this, onChannelHeaderSetup);

    resetService();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsyncHttpService::initFsMP()
{
    if (wwwPath.isEmpty())
        return true;

    wwwMp = new SkFsMountPoint(service);
    return (wwwMp->setup("/", true) && wwwMp->init(wwwPath.c_str()) && service->addMountPoint(wwwMp));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAsyncHttpService::resetService()
{
    if (service->isListening())
        service->stop();

    service->destroyLater();

    service = nullptr;
    wwwMp = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowAsyncHttpService, onPublisherAdded)
{
    SilentSlotArgsWarning();

    SkFlowChanID chanID = Arg_Int16;
    SkFlowChannel *ch = async->channel(chanID);

    AssertKiller(!ch);

    SkFlowRedistrMP *mp = new SkFlowRedistrMP;
    mp->ch = ch;
    mp->path = "/";
    mp->path.append(ch->name);

    if (mp->ch->mime == "image/jpeg")
    {
        SkPartRedistrMountPoint *r = new SkPartRedistrMountPoint;
        AssertKiller(!r->setup(mp->path.c_str(), true) || !r->init(async->userName(), "image/jpeg"));
        mp->r = r;
    }

    else
    {
        mp->r = new SkRawRedistrMountPoint;
        AssertKiller(!mp->r->setup(mp->path.c_str(), true) || !mp->r->init(async->userName(), ch->mime.c_str()));
    }

    if (!ch->header.isEmpty() && redistrs.contains(ch->chanID))
        mp->r->setHeader(&ch->header);

    AssertKiller(!service->addMountPoint(mp->r));

    SkDeviceRedistr *redistr = mp->r->getRedistr();
    redistr->setProperty("ChanID", chanID);

    Attach(redistr, targetAdded, this, onStreamTargetAdded, SkQueued);
    Attach(redistr, targetDeleted, this, onStreamTargetRemoved, SkQueued);

    redistrs[ch->chanID] = mp;
}

SlotImpl(SkFlowAsyncHttpService, onChannelRemoved)
{
    SilentSlotArgsWarning();

    SkFlowChanID chanID = Arg_Int16;
    SkFlowChannel *ch = async->channel(chanID);

    AssertKiller(!ch);

    if (ch->chan_t == StreamingChannel)
    {
        if (redistrs.contains(ch->chanID))
        {
            SkFlowRedistrMP *mp = redistrs[ch->chanID];

            SkString msPath = mp->r->getPath();
            service->delMountPoint(msPath.c_str());
            redistrs.remove(ch->chanID);
            delete mp;
        }
    }
}

SlotImpl(SkFlowAsyncHttpService, onChannelHeaderSetup)
{
    SilentSlotArgsWarning();

    SkFlowChanID chanID = Arg_Int16;
    SkFlowChannel *ch = async->channel(chanID);

    AssertKiller(!ch);

    if (ch->chan_t == StreamingChannel)
    {
        if (!ch->header.isEmpty() && redistrs.contains(ch->chanID))
        {
            SkFlowRedistrMP *mp = redistrs[ch->chanID];
            mp->r->setHeader(&ch->header);
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowAsyncHttpService, onStreamTargetAdded)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);
    SkFlowChanID chanID = r->property("ChanID").toInt16();

    if (r->count() == 1)
    {
        ObjectWarning("Requesting to start redistr: " << chanID);
        SkVariantVector p;
        p << chanID;
        channelRedistrStartRequest(this, p);
    }
}

SlotImpl(SkFlowAsyncHttpService, onStreamTargetRemoved)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);
    SkFlowChanID chanID = r->property("ChanID").toInt16();

    if (r->count() == 0)
    {
        ObjectWarning("Requesting to stop redistr: " << chanID);
        SkVariantVector p;
        p << chanID;
        channelRedistrStopRequest(this, p);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsyncHttpService::redistr(SkFlowChanID chanID, SkDataBuffer &data)
{
    return redistr(chanID, data.toVoid(), data.size());
}

bool SkFlowAsyncHttpService::redistr(SkFlowChanID chanID, CVoid *data, ULong sz)
{
    if (!redistrs.contains(chanID))
        return false;

    SkFlowRedistrMP *mp = redistrs[chanID];
    mp->r->getRedistr()->sendToAll(SkArrayCast::toCStr(data), sz);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkRawRedistrMountPoint *SkFlowAsyncHttpService::redistrMountPoint(SkFlowChanID chanID)
{
    if (!redistrs.contains(chanID))
        return nullptr;

    return redistrs[chanID]->r;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
