#ifndef SKFLOWHTTPSVRCONNECTION_H
#define SKFLOWHTTPSVRCONNECTION_H

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/System/Network/TCP/HTTP/skwebsocket.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpservice.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h"

class SkFlowHttpSvrConnection extends SkAbstractFlowConnection
{
    SkWebSocket *sck;

    SkArgsMap currentCmdPck;
    SkString currentCmd;

    SkSet<SkString> importedPairs;

    public:
        Constructor(SkFlowHttpSvrConnection, SkAbstractFlowConnection);

        void setup(SkFlowServer *flowServer, SkWebSocket *socket);

        SkAbstractDevice *getSck();

        void sendEvtPck(CStr *evtName, SkArgsMap &evt);
        void sendData(SkDataBuffer &data);
        void sendChannelData(SkFlowChanID chanID, SkDataBuffer &data);

        void close();

        Slot(onTextReadyRead);
        Slot(onDataReadyRead);
        Slot(onDisconnected);

        Slot(onChannelAdded);
        Slot(onChannelRemoved);
        Slot(onChannelHeaderSetup);
        Slot(onPairChange);

        void notifyChannelReqPublishStart(SkFlowChanID chanID)  override;//ASYNC
        void notifyChannelReqPublishStop(SkFlowChanID chanID)   override;//ASYNC

    private:
        void notifyAddedChannel(SkFlowChanID chanID)            override;//ASYNC
        void notifyRemovedChannel(SkFlowChanID chanID)          override;//ASYNC
        void notifyChannelHeader(SkFlowChanID chanID)           override;//ASYNC

        void onLogin()                                          override;
        void onDisable()                                        override;

        void analyze();

        void recvLogin();

        void addDatabase();
        void setCurrentDatabase();
        void recvVariable();
        void removeVariable();

        void varImport();
        void varDeport();

        void addStreamingChannel();
        void addServiceChannel();
        void removeChannel();

        void subscribe();
        void unsubscribe();

        void attachChannel();
        void detachChannel();

        void sendError(CStr *description);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWHTTPSVRCONNECTION_H
