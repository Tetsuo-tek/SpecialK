#ifndef SKFLOWMP_H
#define SKFLOWMP_H

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skflowhttpsvrconnection.h"

class SkFlowMP extends SkGenericMountPoint
{
    SkAbstractFlowAuthenticator *auth;
    SkFlowServer *svr;
    SkHttpService *service;
    SkTreeMap<SkString, SkFlowHttpSvrConnection *> sockets;//wsKey, sck

    public:
        Constructor(SkFlowMP, SkGenericMountPoint);

        void init(SkFlowServer *flowServer, SkHttpService *httpService);
        void setAuthenticator(SkAbstractFlowAuthenticator *authenticator);//WHITOUT auth ALL IS PERMITTED

        Slot(onUserAccepted);
        Slot(onUserDisconnected);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWMP_H
