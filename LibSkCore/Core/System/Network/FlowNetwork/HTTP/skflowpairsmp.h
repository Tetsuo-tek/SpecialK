#ifndef SKFLOWPAIRSMP_H
#define SKFLOWPAIRSMP_H

#if defined(ENABLE_HTTP)

#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpservice.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h"

class SkFlowPairsMP extends SkGenericMountPoint
{
    SkFlowServer *fs;
    SkHttpService *service;

    public:
        Constructor(SkFlowPairsMP, SkGenericMountPoint);

        void init(SkFlowServer *flowServer, SkHttpService *httpService);

        Slot(onPageRequest);
};

#endif

#endif // SKFLOWPAIRSMP_H
