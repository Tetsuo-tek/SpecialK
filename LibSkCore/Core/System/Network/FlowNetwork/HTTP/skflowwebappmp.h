#ifndef SKFLOWWEBAPPMP_H
#define SKFLOWWEBAPPMP_H

#if defined(ENABLE_HTTP)

#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpservice.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h"

class SkFlowWebAppMP extends SkGenericMountPoint
{
    SkAbstractFlowAuthenticator *auth;
    SkFlowServer *fs;
    SkHttpService *service;

    public:
        Constructor(SkFlowWebAppMP, SkGenericMountPoint);

        void init(SkFlowServer *flowServer, SkHttpService *httpService);
        void setAuthenticator(SkAbstractFlowAuthenticator *authenticator);//WHITOUT auth ALL IS PERMITTED

        Slot(onPageRequest);

    private:
        void login(SkHttpSocket *httpSck);
        void logout(SkHttpSocket *httpSck);
        void home(SkHttpSocket *httpSck);
};

#endif

#endif // SKFLOWWEBAPPMP_H
