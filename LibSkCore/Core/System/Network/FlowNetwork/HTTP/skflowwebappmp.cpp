﻿#include "skflowwebappmp.h"

#if defined(ENABLE_HTTP)

#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

ConstructorImpl(SkFlowWebAppMP, SkGenericMountPoint)
{
    auth = nullptr;
    fs = nullptr;
    service = nullptr;

    SlotSet(onPageRequest);

    Attach(this, accepted, this, onPageRequest, SkQueued);
}

void SkFlowWebAppMP::init(SkFlowServer *flowServer, SkHttpService *httpService)
{
    fs = flowServer;
    service = httpService;
}

void SkFlowWebAppMP::setAuthenticator(SkAbstractFlowAuthenticator *authenticator)
{
    auth = authenticator;
}

SlotImpl(SkFlowWebAppMP, onPageRequest)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);

    if (!auth)
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
        return;
    }

    SkArgsMap &urlQuery = httpSck->url().getQueryMap();

    if (!urlQuery.contains("page"))
    {
        home(httpSck);
        return;
    }

    SkString page = urlQuery["page"].toString();

    if (page == "login")
        login(httpSck);

    else if (page == "logout")
        logout(httpSck);

    else
        home(httpSck);
}

void SkFlowWebAppMP::login(SkHttpSocket *httpSck)
{
    if (!SkString::compare(httpSck->getHttpMethod(), "POST"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::MethodNotAllowed);
        return;
    }

    SkArgsMap &recvCookies = httpSck->getHttpCookies();

    if (recvCookies.contains("sid"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotAcceptable);
        return;
    }

    SkCookies sendCookies;
    SkPostParser *post = httpSck->getPostParser();

    if (!post->existsField("username") || !post->existsField("password"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkString userName = post->getFieldContent("username");
    SkString token = post->getFieldContent("password");

    CStr *sessionID = auth->authorizeSession(userName.c_str(), token.c_str());

    if (!sessionID)
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::Unauthorized);
        return;
    }

    sendCookies.add("sid", sessionID);
    ObjectMessage("User has LOGGED IN: " << userName);

    httpSck->send(SkHttpStatusCode::Ok, &sendCookies);
}

void SkFlowWebAppMP::logout(SkHttpSocket *httpSck)
{
    if (!SkString::compare(httpSck->getHttpMethod(), "GET"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::MethodNotAllowed);
        return;
    }

    SkArgsMap &recvCookies = httpSck->getHttpCookies();

    if (!recvCookies.contains("sid"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotAcceptable);
        return;
    }

    SkCookies sendCookies;
    SkCookieAttribute attr;
    attr.expires = true;
    attr.path = "/";

    sendCookies.add("sid", "", attr);

    httpSck->send(SkHttpStatusCode::Ok, &sendCookies);
}

void SkFlowWebAppMP::home(SkHttpSocket *httpSck)
{
    service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
}

#endif
