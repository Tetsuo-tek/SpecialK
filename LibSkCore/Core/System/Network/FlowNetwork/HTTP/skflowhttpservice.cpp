#include "skflowhttpservice.h"

#if defined(ENABLE_HTTP)

#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpservice.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h"

#include "skflowwebappmp.h"
#include "skflowpairsmp.h"
#include "skflowmp.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowHttpService, SkObject)
{
    fs = nullptr;
    auth = nullptr;

    service = nullptr;

    wwwMp = nullptr;
    webAppMp = nullptr;
    pairsMp = nullptr;
    flowMp = nullptr;

    listenPort = 0;
    listenAddress = "0.0.0.0";

    ssl = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpService::listenTo(UShort port, CStr *address)
{
    listenPort = port;
    listenAddress = address;
}

void SkFlowHttpService::enableStaticWWW(CStr *realDirPath)
{
    wwwPath = SkFsUtils::adjustPathEndSeparator(realDirPath);
}

void SkFlowHttpService::enableSsl(CStr *sslCertFilePath, CStr *sslKeyFilePath)
{
    ssl = true;
    sslCert = sslCertFilePath;
    sslKey = sslKeyFilePath;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowHttpService::init(SkFlowServer *flowServer, UShort maxQueuedConnections, UShort idleTimeoutInterval, ULong maxHeaderSize)
{
    fs = flowServer;
    auth = flowServer->authenticator();
    setObjectName(fs, "Http");

    service = new SkHttpService(this);
    service->setObjectName(this, "Service");
    service->setup(objectName(), ssl, maxHeaderSize, idleTimeoutInterval);

    if (ssl)
        service->sslSvr()->initSslCtx(sslCert.c_str(), sslKey.c_str());

    if (!initFsMP())
    {
        resetService();
        return false;
    }

    if (!initWebAppMP())
    {
        resetService();
        return false;
    }

    if (!initPairsMP())
    {
        resetService();
        return false;
    }

    if (!initFlowMP())
    {
        resetService();
        return false;
    }

    if (!service->start(listenAddress.c_str(), listenPort, maxQueuedConnections))
    {
        resetService();
        return false;
    }

    return true;
}

void SkFlowHttpService::quit()
{
    if (!service)
        return;

    resetService();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowHttpService::initFsMP()
{
    if (wwwPath.isEmpty())
        return true;

    wwwMp = new SkFsMountPoint(service);
    return (wwwMp->setup("/", true) && wwwMp->init(wwwPath.c_str()) && service->addMountPoint(wwwMp));
}

bool SkFlowHttpService::initWebAppMP()
{
    webAppMp = new SkFlowWebAppMP(service);
    webAppMp->setAuthenticator(auth);

    if (!webAppMp->setup("/app", true) || !service->addMountPoint(webAppMp))
        return false;

    webAppMp->init(fs, service);
    return true;
}

bool SkFlowHttpService::initPairsMP()
{
    pairsMp = new SkFlowPairsMP(service);

    if (!pairsMp->setup("/pairs", true) || !service->addMountPoint(pairsMp))
        return false;

    pairsMp->init(fs, service);
    return true;
}

bool SkFlowHttpService::initFlowMP()
{
    flowMp = new SkFlowMP(service);
    flowMp->setAuthenticator(auth);

    if (!flowMp->setup("/flow", true) || !service->addMountPoint(flowMp))
        return false;

    flowMp->init(fs, service);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowHttpService::resetService()
{
    service->destroyLater();

    service = nullptr;
    pairsMp = nullptr;
    wwwMp = nullptr;
    flowMp = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
