﻿#include "skflowmp.h"

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

ConstructorImpl(SkFlowMP, SkGenericMountPoint)
{
    auth = nullptr;
    svr = nullptr;
    service = nullptr;

    SlotSet(onUserAccepted);
    SlotSet(onUserDisconnected);

    Attach(this, accepted, this, onUserAccepted, SkQueued);
}

void SkFlowMP::init(SkFlowServer *flowServer, SkHttpService *httpService)
{
    svr = flowServer;
    service = httpService;
}

void SkFlowMP::setAuthenticator(SkAbstractFlowAuthenticator *authenticator)
{
    auth = authenticator;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowMP, onUserAccepted)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);

    if (!wsSck)
    {
        ObjectError("Connection is NOT WebSocket");
        return;
    }

    SkFlowHttpSvrConnection *c = new SkFlowHttpSvrConnection(this);
    c->setup(svr, wsSck);
    c->setAuthenticator(auth);
    sockets[wsSck->getKey()] = c;

    Attach(wsSck, disconnected, this, onUserDisconnected, SkDirect);
    ObjectMessage("ADDED flow WebSocket: " << wsSck->objectName());
}

SlotImpl(SkFlowMP, onUserDisconnected)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);
    SkFlowHttpSvrConnection *conn = sockets.remove(wsSck->getKey()).value();

    if (auth)
        auth->remove(conn);

    conn->destroyLater();
    ObjectMessage("REMOVED flow WebSocket: " << wsSck->objectName());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
