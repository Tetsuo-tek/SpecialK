#include "skflowredistrchannel.h"
#include "skflowrawsvrconnection.h"
#include "skflowasync.h"

#include "Core/App/skapp.h"
#include "Core/System/skdeviceredistr.h"
#include "Core/System/Filesystem/skmimetype.h"
#include "Core/Containers/skarraycast.h"

#include "Core/System/Network/FlowNetwork/skflowpairdatabase.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/skwebsocket.h"
    #include "HTTP/skflowhttpsvrconnection.h"
#include "Core/System/Network/TCP/HTTP/skwsredistr.h"
#endif

#define CheckForDataType(VARIANT_T) \
    FakeSingleLine( \
        if (data_t != VARIANT_T) \
        { \
            ObjectError("Channel value type is NOT valid: [" \
              << "; requested dataType: " << SkVariant::variantTypeName(data_t) \
              << "; wrong type: " << SkVariant::variantTypeName(VARIANT_T) \
              << "]"); \
            return false; \
        } \
    )

ConstructorImpl(SkFlowRedistrChannel, SkAbstractFlowChannel)
{
    r = nullptr;

#if defined(ENABLE_HTTP)
    rWs = nullptr;
#endif

    source = nullptr;

    flow_t = FT_BLOB;
    chan_t = StreamingChannel;
    data_t = T_NULL;

    publishingEnabled = false;
    publishingAsReplicant = false;

    SignalSet(dataPublished);
    SlotSet(onSourcePublish);

    SlotSet(onFastTick);
}

bool SkFlowRedistrChannel::setup(SkFlowChanID id, CStr *name, SkFlow_T flowType, SkVariant_T dataType, CStr *mime)
{
    if (chanID > -1)
    {
        ObjectError("Channel ALREADY inititialized");
        return false;
    }

    init(id, name);

    if (SkString::isEmpty(mime))
        mm = "application/octet-stream";

    else
        mm = mime;

    flow_t = flowType;
    data_t = dataType;

    r = new SkDeviceRedistr(this);
    r->setObjectName(this, "Redistr");
    r->setProperty("ChanID", chanID);
    r->setProperty("ChanName", n.c_str());

#if defined(ENABLE_HTTP)

    rWs = new SkWsRedistr(this);
    rWs->setObjectName(this, "WsRedistr");

    rWs->setProperty("ChanID", chanID);
    rWs->setProperty("ChanName", n.c_str());
#endif

    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);

    return true;
}

void SkFlowRedistrChannel::onSetPairDatabase()
{
    db->setVariable("chan_t", "StreamingChannel");
    db->setVariable("flow_t", SkFlowProto::flowTypeToString(flow_t));
    db->setVariable("chan_t", SkVariant::variantTypeName(data_t));
    db->setVariable("mime_t", mm);

    SkStringList l;
    db->setVariable("subscribers", l);
    db->setVariable("subscribersCount", 0);

    db->setVariable("targets", l);
    db->setVariable("targetsCount", 0);

    db->setVariable("source", "NULL");
}

SlotImpl(SkFlowRedistrChannel, onFastTick)
{
    SilentSlotArgsWarning();
}

bool SkFlowRedistrChannel::replicate(SkFlowAsync *node)
{
    nodeConnection = node;

    SkString chName(node->userName());
    chName.append(".");
    chName.append(n);

    return node->addStreamingChannel(replicatedChanID, flow_t, data_t, chName.c_str(), mm.c_str());
}

void SkFlowRedistrChannel::onDisable()
{
    Detach(eventLoop()->fastZone_SIG, pulse, this, onFastTick);

    {
        SkBinaryTreeVisit<SkFlowRedistrChannel *, nullptr_t> *itr = targets.iterator();

        while(itr->next())
        {
            SkFlowRedistrChannel *target = itr->item().key();
            detach(target);
        }

        delete itr;
        targets.clear();
    }

    if (source)
    {
        source->detach(this);
        source = nullptr;
    }
}

void SkFlowRedistrChannel::setChannelValueLimits(const SkVariant &minimum, const SkVariant &maximum)
{
    min = minimum;
    max = maximum;

    ObjectMessage("Setup limits: [min: " << min.toString() << "; max: " << max.toString() << "]");
}

void SkFlowRedistrChannel::setReplicantPublishingEnabled(bool enable)
{
    publishingAsReplicant = enable;
}

bool SkFlowRedistrChannel::attach(SkFlowRedistrChannel *ch)
{
    if (ch->source)
    {
        ObjectError("Target is ALREADY attached: " << ch->objectName());
        return false;
    }

    if (!targets.add(ch))
    {
        ObjectError("Target is ALREADY attached: " << ch->objectName());
        return false;
    }

    ch->source = this;

    Attach(this, dataPublished, ch, onSourcePublish, SkDirect);
    checkPublishState();

    SkVariantList l;
    SkBinaryTreeVisit<SkFlowRedistrChannel *, nullptr_t> *itr = targets.iterator();

    while(itr->next())
        l << itr->item().key()->name();

    delete itr;

    db->setVariable("targets", l);
    db->setVariable("targetsCount", l.count());

    ch->db->setVariable("source", n);

    ObjectWarning("Target ATTACHED: " << ch->objectName());
    return true;
}

bool SkFlowRedistrChannel::detach(SkFlowRedistrChannel *ch)
{
    if (!ch->source)
    {
        ObjectError("Target is NOT attached: " << ch->objectName());
        return false;
    }

    if (!targets.remove(ch))
    {
        ObjectError("Target is NOT attached yet: " << ch->objectName());
        return false;
    }

    ch->source = nullptr;
    Detach(this, dataPublished, ch, onSourcePublish);
    checkPublishState();

    /*if (targets.isEmpty())
        db->delVariable("targets");

    else
    {*/
        SkVariantList l;
        SkBinaryTreeVisit<SkFlowRedistrChannel *, nullptr_t> *itr = targets.iterator();

        while(itr->next())
            l << itr->item().key()->name();

        delete itr;

        db->setVariable("targets", l);
    //}

    ch->db->setVariable("source", "NULL");

    ObjectWarning("Target DETACHED: " << ch->objectName());
    return true;
}

SlotImpl(SkFlowRedistrChannel, onSourcePublish)
{
    SilentSlotArgsWarning();

    SkFlowRedistrChannel *source = dynamic_cast<SkFlowRedistrChannel *>(referer);
    publish(source->lastData.toVoid(), source->lastData.size());
}

void SkFlowRedistrChannel::checkPublishState()
{
    SkString evtName;
    //SkFlowResponse rsp = FRSP_NORSP;

    if (publishingEnabled)
    {
        bool conditionToDisable = (!publishingAsReplicant && !hasSubscribers() && !hasGrabbers() && targets.isEmpty());
        ObjectWarning("Checking activity state [enabled now] -> [conditionToDisable: " << SkVariant::boolToString(conditionToDisable) << "]");

        if (conditionToDisable)
        {
            publishingEnabled = false;
            ObjectWarning("Publish disabled");
            evtName = "ChannelDeActivationRequest";
            //rsp = FRSP_CHANNEL_PUBLISH_STOP;
        }

        else
            return;
    }

    else if (!publishingEnabled)//redoundant :)
    {
        bool conditionToEnable = (publishingAsReplicant || hasSubscribers() || hasGrabbers() || !targets.isEmpty());
        ObjectWarning("Checking activity state [disabled now] -> [conditionToEnable: " << SkVariant::boolToString(conditionToEnable) << "]");

        if (conditionToEnable)
        {
            publishingEnabled = true;
            ObjectWarning("Publish enabled");
            evtName = "ChannelActivationRequest";
            //rsp = FRSP_CHANNEL_PUBLISH_START;
        }

        else
            return;
    }

    db->setVariable("isPublishEnabled", publishingEnabled);

    SkVariantList p;
    p << chanID;
    p << n;

    skApp->propagateEvent(this, "Flow", evtName.c_str(), &p);

    if (o)
    {
        if (publishingEnabled)
            o->notifyChannelReqPublishStart(chanID);

        else
            o->notifyChannelReqPublishStop(chanID);
    }
}

bool SkFlowRedistrChannel::addSubscriber(SkAbstractFlowConnection *subscriber)
{
    SkAbstractDevice *dev = subscriber->getSck();
    SkFlowRawSvrConnection *rawConn = dynamic_cast<SkFlowRawSvrConnection*>(subscriber);

    if (rawConn)
    {
        if (!r->existsDevice(dev))
        {
            SkStringList l;

            if (!r->isEmpty())
            {
                SkVariant &v = db->getVariable("subscribers");
                v.copyToStringList(l);
            }

            r->addDevice(dev);

            l << rawConn->connectionID();
            db->setVariable("subscribers", l);
            db->setVariable("subscribersCount", r->count());

            ObjectMessage("ADDED subscriber [" << dev->typeName() << "]: " << dev->objectName() << "[" << subscriber->objectName() << "]");
            checkPublishState();
            return true;
        }
    }

#if defined(ENABLE_HTTP)
    SkFlowHttpSvrConnection *wsConn = dynamic_cast<SkFlowHttpSvrConnection*>(subscriber);

    if (wsConn)
    {
        SkWebSocket *sck = dynamic_cast<SkWebSocket*>(dev);

        if (!rWs->existsDevice(sck))
        {
            rWs->addDevice(sck);
            db->setVariable("httpStreamsCount", rWs->count());
            ObjectMessage("ADDED subscriber [" << sck->typeName() << "]: " << sck->objectName() << "[" << subscriber->objectName() << "]");
            checkPublishState();
            return true;
        }
    }
#endif

    ObjectError("Device is ALREADY a subscriber [" << dev->typeName() << "]: " << dev->objectName() << "[" << subscriber->objectName() << "]");
    return false;
}

bool SkFlowRedistrChannel::removeSubscriber(SkAbstractFlowConnection *subscriber)
{
    SkAbstractDevice *dev = subscriber->getSck();
    SkFlowRawSvrConnection *rawConn = dynamic_cast<SkFlowRawSvrConnection*>(subscriber);

    if (rawConn)
    {
        if (r->existsDevice(dev))
        {
            r->delDevice(dev);

            SkStringList l;
            SkVariant &v = db->getVariable("subscribers");
            v.copyToStringList(l);
            l.remove(rawConn->connectionID());

            db->setVariable("subscribers", l);
            db->setVariable("subscribersCount", r->count());

            ObjectMessage("REMOVED subscriber [" << dev->typeName() << "]: " << dev->objectName() << "[" << subscriber->objectName() << "]");
            checkPublishState();
            return true;
        }
    }

#if defined(ENABLE_HTTP)
    SkFlowHttpSvrConnection *wsConn = dynamic_cast<SkFlowHttpSvrConnection*>(subscriber);

    if (wsConn)
    {
        SkWebSocket *sck = dynamic_cast<SkWebSocket*>(dev);

        if (rWs->existsDevice(sck))
        {
            rWs->delDevice(sck);
            db->setVariable("httpStreamsCount", rWs->count());
            ObjectMessage("REMOVED subscriber [" << sck->typeName() << "]: " << sck->objectName() << "[" << subscriber->objectName() << "]");
            checkPublishState();
            return true;
        }
    }
#endif

    ObjectError("Device is NOT a subscriber [" << dev->typeName() << "]: " << dev->objectName() << "[" << subscriber->objectName() << "]");
    return false;
}

bool SkFlowRedistrChannel::hasSubscribers()
{
    return (subscribersCount() > 0);
}

ULong SkFlowRedistrChannel::subscribersCount()
{
#if defined(ENABLE_HTTP)
    return r->count() + rWs->count();
#else
    return r->count();
#endif
}

bool SkFlowRedistrChannel::hasGrabbers()
{
    return (grabbersCount() > 0);
}

ULong SkFlowRedistrChannel::grabbersCount()
{
    return grbs.count();
}

bool SkFlowRedistrChannel::addGrabber(SkAbstractFlowConnection *grabber)
{
    if (grbs.add(grabber))
    {
        db->setVariable("grabbers", grbs.count());
        ObjectMessage("REGISTERED grabber: " << grabber->objectName());
        checkPublishState();

        SkVariantList l;
        SkBinaryTreeVisit<SkAbstractFlowConnection *, nullptr_t> *itr = grbs.iterator();

        while(itr->next())
            l << itr->item().key()->name();

        delete itr;

        db->setVariable("grabbers", l);
        return true;
    }

    ObjectError("Grabber is ALREADY registered: " << grabber->objectName());
    return false;
}

bool SkFlowRedistrChannel::removeGrabber(SkAbstractFlowConnection *grabber)
{
    if (grbs.remove(grabber))
    {
        db->setVariable("grabbers", grbs.count());
        ObjectMessage("UNREGISTERED grabber: " << grabber->objectName());
        checkPublishState();

        if (grbs.isEmpty())
            db->delVariable("grabbers");

        else
        {
            SkVariantList l;
            SkBinaryTreeVisit<SkAbstractFlowConnection *, nullptr_t> *itr = grbs.iterator();

            while(itr->next())
                l << itr->item().key()->name();

            delete itr;

            db->setVariable("grabbers", l);
        }

        return true;
    }

    ObjectError("Grabber is NOT registered yet: " << grabber->objectName());
    return false;
}

bool SkFlowRedistrChannel::publish()
{
    publish(nullptr, 0);
    return true;
}

bool SkFlowRedistrChannel::publish(int8_t v)
{
    CheckForDataType(SkVariant_T::T_INT8);
    publish(SkArrayCast::toChar(&v), sizeof(int8_t));
    return true;
}

bool SkFlowRedistrChannel::publish(uint8_t v)
{
    CheckForDataType(SkVariant_T::T_UINT8);
    publish(SkArrayCast::toChar(&v), sizeof(uint8_t));
    return true;
}

bool SkFlowRedistrChannel::publish(Short v)
{
    CheckForDataType(SkVariant_T::T_INT16);
    publish(SkArrayCast::toChar(&v), sizeof(int16_t));
    return true;
}

bool SkFlowRedistrChannel::publish(UShort v)
{
    CheckForDataType(SkVariant_T::T_UINT16);
    publish(SkArrayCast::toChar(&v), sizeof(uint16_t));
    return true;
}

bool SkFlowRedistrChannel::publish(Int v)
{
    CheckForDataType(SkVariant_T::T_INT32);
    publish(SkArrayCast::toChar(&v), sizeof(int32_t));
    return true;
}

bool SkFlowRedistrChannel::publish(UInt v)
{
    CheckForDataType(SkVariant_T::T_UINT32);
    publish(SkArrayCast::toChar(&v), sizeof(uint32_t));
    return true;
}

bool SkFlowRedistrChannel::publish(Long v)
{
    CheckForDataType(SkVariant_T::T_INT64);
    publish(SkArrayCast::toChar(&v), sizeof(int64_t));
    return true;
}

bool SkFlowRedistrChannel::publish(ULong v)
{
    CheckForDataType(SkVariant_T::T_UINT64);
    publish(SkArrayCast::toChar(&v), sizeof(uint64_t));
    return true;
}

bool SkFlowRedistrChannel::publish(float v)
{
    CheckForDataType(SkVariant_T::T_FLOAT);
    publish(SkArrayCast::toChar(&v), sizeof(float));
    return true;
}

bool SkFlowRedistrChannel::publish(double v)
{
    CheckForDataType(SkVariant_T::T_DOUBLE);
    publish(SkArrayCast::toChar(&v), sizeof(double));
    return true;
}

bool SkFlowRedistrChannel::publish(CStr *str)
{
    CheckForDataType(SkVariant_T::T_STRING);
    publish(str, strlen(str));
    return true;
}

bool SkFlowRedistrChannel::publish(SkString &str)
{
    CheckForDataType(SkVariant_T::T_STRING);
    publish(str.c_str(), str.size());
    return true;
}

bool SkFlowRedistrChannel::publish(SkArgsMap &m)
{
    CheckForDataType(SkVariant_T::T_MAP);
    SkString json;
    m.toString(json);
    publish(json.c_str(), json.size());
    return true;
}

void SkFlowRedistrChannel::publish(CVoid *val, uint32_t sz)
{
    if (sz)
    {
        lastData.setData(val, sz);

        //totalInputBytes += sz;
        inputSpeed += sz;
    }

    if (!r->isEmpty())
    {
        SkDataBuffer pck;
        uint16_t rsp = FRSP_SUBSCRIBED_DATA;
        pck.setData(&rsp, sizeof(uint16_t));
        pck.append(SkArrayCast::toChar(&chanID), sizeof(SkFlowChanID));
        pck.append(SkArrayCast::toChar(&sz), sizeof(uint32_t));

        if (sz)
        {
            pck.append(lastData.data(), sz);

            //totalOutputBytes += (sz * r->count());
            outputSpeed += (sz * r->count());
        }

        uint32_t endOfTr = 0;
        pck.append(SkArrayCast::toChar(&endOfTr), sizeof(uint32_t));

        r->sendToAll(pck.data(), pck.size());
    }

    if (nodeConnection && nodeConnection->isConnected() && nodeConnection->isAuthorized() && replicatedChanID > -1)
        nodeConnection->publish(replicatedChanID, val, sz);

#if defined(ENABLE_HTTP)
    if (!rWs->isEmpty())
    {
        SkDataBuffer wsData;
        wsData.setData(&chanID, sizeof(SkFlowChanID));

        if (sz)
            wsData.append(lastData);

        rWs->sendBinaryToAll(wsData);
    }
#endif

    dataPublished();
}

bool SkFlowRedistrChannel::isReplicantPublishingEnabled()
{
    return publishingAsReplicant;
}

bool SkFlowRedistrChannel::isPublishingEnabled()
{
    return publishingEnabled;
}

SkFlow_T SkFlowRedistrChannel::flowType()
{
    return flow_t;
}

SkVariant_T SkFlowRedistrChannel::dataType()
{
    return data_t;
}

SkDataBuffer &SkFlowRedistrChannel::header()
{
    return hdr;
}

SkDataBuffer &SkFlowRedistrChannel::data()
{
    return lastData;
}

CStr *SkFlowRedistrChannel::mime()
{
    return mm.c_str();
}

SkVariant &SkFlowRedistrChannel::minimum()
{
    return min;
}

SkVariant &SkFlowRedistrChannel::maximum()
{
    return max;
}
