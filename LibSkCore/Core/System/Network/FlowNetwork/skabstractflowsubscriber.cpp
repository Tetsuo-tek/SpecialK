#include "skabstractflowsubscriber.h"

AbstractConstructorImpl(SkAbstractFlowSubscriber, SkObject)
{
    async = nullptr;
    src = nullptr;

    SignalSet(ready);
}

void SkAbstractFlowSubscriber::setClient(SkFlowAsync *flowAsync)
{
    if (flowAsync == async)
        return;

    async = flowAsync;

    Attach(async, channelAdded, this, channelAdded, SkQueued);//MUST BE QUEUED DUE SOME ASYNC PROBLEM WITH PUBLISHERs
    Attach(async, channelRemoved, this, channelRemoved, SkDirect);
}

void SkAbstractFlowSubscriber::unSetClient()
{
    if (async)
    {
        Detach(async, channelAdded, this, channelAdded);
        Detach(async, channelRemoved, this, channelRemoved);
        async = nullptr;
    }
}

void SkAbstractFlowSubscriber::setChannelName(CStr *chanName)
{
    srcChanName = chanName;

    SkStringList parsedName;
    srcChanName.split(".", parsedName);
    AssertKiller(parsedName.count() < 2);
    srcSatName = parsedName.first();

    ObjectMessage("Selected chan: " << srcChanName);

    if (async->containsChannel(srcChanName.c_str()))
        onChannelAdded(async->channelID(srcChanName.c_str()));
}

void SkAbstractFlowSubscriber::setSource(SkFlowChannel *source)
{
    src = source;
    ready();

    ObjectMessage("Source channel is READY");
    async->subscribeChannel(src);
}

SlotImpl(SkAbstractFlowSubscriber, channelAdded)
{
    SilentSlotArgsWarning();
    onChannelAdded(Arg_Int16);
}

SlotImpl(SkAbstractFlowSubscriber, channelRemoved)
{
    SilentSlotArgsWarning();
    onChannelRemoved(Arg_Int16);
}

void SkAbstractFlowSubscriber::reset()
{
    ObjectWarning("Reset");

    if (src && async->isSubscribed(src->chanID))
        async->unsubscribeChannel(src);

    src = nullptr;
    srcSatName.clear();
    srcChanName.clear();
}

SkFlowChannel *SkAbstractFlowSubscriber::source()
{
    return src;
}

CStr *SkAbstractFlowSubscriber::ownerName()
{
    return srcSatName.c_str();
}

CStr *SkAbstractFlowSubscriber::chanName()
{
    if (src)
        return "";

    return srcChanName.c_str();
}

bool SkAbstractFlowSubscriber::isReady()
{
    return (src != nullptr);
}
