#ifndef SKFLOWSERVICECHANNEL_H
#define SKFLOWSERVICECHANNEL_H

#include "skabstractflowchannel.h"

class SkFlowServiceChannel extends SkAbstractFlowChannel
{
    public:
        Constructor(SkFlowServiceChannel, SkAbstractFlowChannel);

        bool setup(SkFlowChanID id, CStr *name);

        bool replicate(SkFlowAsync *node)                   override;

    private:
        void onSetPairDatabase()                            override;
        void onDisable()                                    override;
};

#endif // SKFLOWSERVICECHANNEL_H
