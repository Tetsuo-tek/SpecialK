#ifndef SKFLOWBLOBRECEIVER_H
#define SKFLOWBLOBRECEIVER_H

#include "Core/Object/skobject.h"
#include "Core/System/Network/FlowNetwork/skflowcommon.h"
#include "Core/System/Time/skelapsedtime.h"

class SkFile;
class SkFlowRawSvrConnection;
class SkFlowServiceRequestStatus;

class SkFlowBlobReceiver extends SkObject
{
    SkFlowServiceRequestStatus *st;
    SkFlowRawSvrConnection *flow;
    SkString realFilePath;
    SkFile *blob;
    SkString blobHashID;
    Long bSize;
    Long totalBytes;

    public:
        Constructor(SkFlowBlobReceiver, SkObject);

        bool start(CStr *filePath, Long sz, CStr *uploadHashID, SkFlowServiceRequestStatus *request);
        bool stop();

        bool write(CVoid *data, ULong sz);

        bool isRunning();
        CStr *filePath();

        CStr *uploadHashID();
        SkFlowServiceRequestStatus *request();
        SkFlowRawSvrConnection *uploader();

        Signal(progress);
        Slot(onUploaderDisabled);
        Signal(terminated);
};

#endif // SKFLOWBLOBRECEIVER_H
