#ifndef SKFLOWREDISEXPORT_H
#define SKFLOWREDISEXPORT_H

#if defined(ENABLE_REDIS)

#include "Core/System/Network/RedisNetwork/skredissync.h"

class SkRedisSubscriber;
class SkFlowPairsDatabase;

class SkFlowRedisExport extends SkRedisSync
{
    public:
        Constructor(SkFlowRedisExport, SkRedisSync);

        bool init(CStr *serverHostname, uint16_t serverPort);
        void close();

        Slot(onRedisSubscriberActivity);

    private:
        SkRedisSubscriber *subscriber;
};
#endif

#endif // SKFLOWREDISEXPORT_H
