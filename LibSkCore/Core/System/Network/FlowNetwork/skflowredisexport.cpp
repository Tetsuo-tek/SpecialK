#include "skflowredisexport.h"
#include "skflowpairdatabase.h"
#include "Core/System/Network/RedisNetwork/skredissubscriber.h"

#if defined(ENABLE_REDIS)

ConstructorImpl(SkFlowRedisExport, SkRedisSync)
{
    subscriber = nullptr;
    SlotSet(onRedisSubscriberActivity);
}

bool SkFlowRedisExport::init(CStr *serverHostname, uint16_t serverPort)
{
    setObjectName(parent(), "RedisSync");

    subscriber = new SkRedisSubscriber(this);
    subscriber->setObjectName(parent(), "RedisSubscriber");

    if (!connect(serverHostname, serverPort))
    {
        subscriber->destroyLater();
        subscriber = nullptr;

        ObjectError("CANNOT open Redis -> " << serverHostname << ":" << serverPort);
        return false;
    }

    subscriber->setLatencyInterval(50);

    if (!subscriber->connect(serverHostname, serverPort))
    {
        subscriber->destroyLater();
        subscriber = nullptr;

        ObjectError("CANNOT open Redis suscriber -> " << serverHostname << ":" << serverPort);
        return false;
    }

    Attach(subscriber, newReply, this, onRedisSubscriberActivity, SkAttachMode::SkQueued);

    ObjectMessage("Redis flow is OPEN now");
    return true;
}

void SkFlowRedisExport::close()
{
    if (isConnected())
        quit();

    if (subscriber->isConnected())
        subscriber->disconnect();

    subscriber->destroyLater();

    ObjectMessage("Redis flow is CLOSED now");
}

SlotImpl(SkFlowRedisExport, onRedisSubscriberActivity)
{
    SilentSlotArgsWarning();

    while(subscriber->repliesCount() > 0)
    {
        SkRedisASyncReply reply;
        subscriber->getReply(reply);

        ObjectMessage_EXT(subscriber, "(" << reply.value.variantTypeName() << "): " << reply.value.toString());
    }
}
#endif
