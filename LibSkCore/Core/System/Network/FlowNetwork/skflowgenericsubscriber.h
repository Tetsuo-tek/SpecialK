#ifndef SKFLOWGENERICSUBSCRIBER_H
#define SKFLOWGENERICSUBSCRIBER_H

#include "Core/System/Network/FlowNetwork/skabstractflowsubscriber.h"

class SkFlowGenericSubscriber extends SkAbstractFlowSubscriber
{
    SkDataBuffer data;

    public:
        Constructor(SkFlowGenericSubscriber, SkAbstractFlowSubscriber);

        void subscribe(CStr *chanName)                                  override;
        void unsubscribe()                                              override;

        void close()                                                    override;

        Signal(dataAvailable);

        SkDataBuffer &lastData();

        //AbstractFlowSat CALLs THIS IN BACKGROUND
        void tick(SkFlowChannelData &chData)                            override;

    private:
        void onChannelAdded(SkFlowChanID chanID)                        override;
        void onChannelRemoved(SkFlowChanID chanID)                      override;

};

#endif // SKFLOWGENERICSUBSCRIBER_H
