#ifndef SKFLOWGENERICPUBLISHER_H
#define SKFLOWGENERICPUBLISHER_H

#include "Core/System/Network/FlowNetwork/skabstractflowpublisher.h"

class SkFlowGenericPublisher extends SkAbstractFlowPublisher
{
    SkString channelName;
    SkFlow_T ft;
    SkVariant_T dt;
    SkString mt;
    SkArgsMap channelProps;

    public:
        Constructor(SkFlowGenericPublisher, SkAbstractFlowPublisher);

        bool setup(CStr *chName, SkFlow_T flow_t, SkVariant_T data_t, CStr *mime_t, SkArgsMap *properties);

        bool start()                                                    override;
        void stop()                                                     override;

        void tick(CVoid *data, ULong sz);

        bool hasTargets()                                               override;

        SkFlowChanID getChannelID();
        SkFlowChannel *getChannel();

        Signal(ready);

    protected:
        SkFlowChanID channelID;
        SkFlowChannel *channel;

#if defined(ENABLE_HTTP)
        SkRawRedistrMountPoint *mp;
#endif

    private:
        void onChannelAdded(SkFlowChanID chanID)                        override;
        void onChannelRemoved(SkFlowChanID chanID)                      override;

        virtual void onStart()                                          {}
        virtual void onStop()                                           {}

        virtual void onChannelReady()                                   {}
        virtual void onChannelExpired()                                 {}
};

#endif // SKFLOWGENERICPUBLISHER_H
