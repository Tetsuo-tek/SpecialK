#include "skflowgenericsubscriber.h"

ConstructorImpl(SkFlowGenericSubscriber, SkAbstractFlowSubscriber)
{
    SignalSet(dataAvailable);
}

void SkFlowGenericSubscriber::subscribe(CStr *chanName)
{
    setChannelName(chanName);
}

void SkFlowGenericSubscriber::unsubscribe()
{
    if (!isReady())
        return;

    async->unsubscribeChannel(source());
}

void SkFlowGenericSubscriber::tick(SkFlowChannelData &chData)
{
    if (!isReady())
        return;

    if (chData.chanID == source()->chanID)
    {
        data = chData.data;
        dataAvailable();
    }
}

void SkFlowGenericSubscriber::close()
{
    reset();
}

void SkFlowGenericSubscriber::onChannelAdded(SkFlowChanID chanID)
{
    if (isReady())
        return;

    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (SkString::compare(ch->name.c_str(), chanName()))
        setSource(ch);
}

void SkFlowGenericSubscriber::onChannelRemoved(SkFlowChanID chanID)
{
    if (!isReady())
        return;

    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (ch->chanID == source()->chanID)
        reset();
}

SkDataBuffer &SkFlowGenericSubscriber::lastData()
{
    return data;
}
