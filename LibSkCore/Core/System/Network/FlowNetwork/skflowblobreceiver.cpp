#include "skflowblobreceiver.h"
#include "skflowrawsvrconnection.h"
#include "skflowservicerequeststatus.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfile.h"
#include "Core/System/Filesystem/skfsutils.h"

ConstructorImpl(SkFlowBlobReceiver, SkObject)
{
    st = nullptr;
    flow = nullptr;
    blob = new SkFile(this);

    bSize = -1;
    totalBytes = 0;

    SignalSet(progress);
    SlotSet(onUploaderDisabled);
    SignalSet(terminated);
}

bool SkFlowBlobReceiver::start(CStr *filePath, Long sz, CStr *uploadHashID, SkFlowServiceRequestStatus *request)
{
    if (blob->isOpen())
    {
        ObjectError("Blob upload is ALREADY started");
        return false;
    }

    realFilePath = filePath;
    SkString tempFilePath = realFilePath;
    tempFilePath.append(".upload");

    blob->setFilePath(tempFilePath.c_str());

    if (!blob->open(SkFileMode::FLM_ONLYWRITE))
        return false;

    blobHashID = uploadHashID;
    totalBytes = 0;
    bSize = sz;
    st = request;
    flow = request->requester();

    Attach(flow, disabled, this, onUploaderDisabled, SkOneShotQueued);

    ObjectMessage("Blob upload STARTED [" << bSize << " B]: " << blob->getFilePath());
    return true;
}

bool SkFlowBlobReceiver::stop()
{
    if (!blob->isOpen())
    {
        ObjectError("Blob upload NOT started yet");
        return false;
    }

    blob->close();

    SkFsUtils::mv(blob->getFilePath(), realFilePath.c_str());
    terminated();

    ObjectMessage("Blob upload TERMINATED");
    return true;
}

SlotImpl(SkFlowBlobReceiver, onUploaderDisabled)
{
    SilentSlotArgsWarning();
    stop();
}

bool SkFlowBlobReceiver::write(CVoid *data, ULong sz)
{
    if (!blob->isOpen())
    {
        ObjectError("Blob upload NOT started yet");
        return false;
    }

    if (!blob->write(SkArrayCast::toCStr(data), sz))
        return false;

    totalBytes += sz;

    SkVariantVector p;
    p << sz;
    progress(p);

    if (totalBytes == bSize)
    {
        ObjectMessage("Blob upload COMPLETED [" << totalBytes << " B]");
        stop();
    }

    return true;
}

bool SkFlowBlobReceiver::isRunning()
{
    return blob->isOpen();
}

CStr *SkFlowBlobReceiver::filePath()
{
    return blob->getFilePath();
}

CStr *SkFlowBlobReceiver::uploadHashID()
{
    return blobHashID.c_str();
}

SkFlowServiceRequestStatus *SkFlowBlobReceiver::request()
{
    return st;
}

SkFlowRawSvrConnection *SkFlowBlobReceiver::uploader()
{
    return flow;
}
