#include "skflowasync.h"
#include "Core/Containers/skarraycast.h"
#include "Core/Object/skabstractworkerobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowAsync, SkAbstractFlow)
{
    wrk = nullptr;

    currentSubscribedGrabbing = nullptr;
    currentGrabbingDataSize = 0;

    lastResponse = FRSP_NORSP;
    lastTransactionSize = 0;

    maxDataQueueCount = 1000;

    SignalSet(channelAdded);
    SignalSet(channelRemoved);
    SignalSet(channelHeaderSetup);

    SignalSet(publisherAdded);

    SignalSet(channelPublishStartRequest);
    SignalSet(channelPublishStopRequest);

    SignalSet(channelDataPublished);
    SignalSet(channelDataAvailable);

    SignalSet(serviceResponseReceived);

    SlotSet(onWrkEvaluatedCmd);

    SlotSet(onReadyRead);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAsync::setWorker(SkAbstractWorkerObject *worker)
{
    wrk = worker;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowSync *SkFlowAsync::buildSyncClient()
{
    SkFlowSync *sync = new SkFlowSync(this);
    sync->setObjectName(this, "OptSync");

    if ((isTcpSocket() && !sync->tcpConnect(getTcpAddress(), getTcpPort()))
        || (isUnixSocket() && !sync->localConnect(getUnixPath()))
        || !sync->login(userName(), userToken()))
    {
        return nullptr;
    }

    return sync;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::checkService()
{
    if (!socket || !socket->isConnected())
        return false;

    p->sendStartOfTransaction(FCMD_CHK_SERVICE);
    p->sendEndOfTransaction();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime, SkArgsMap *props)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    SkString chName(this->name);
    chName.append(".");
    chName.append(name);

    if (openingChannels.contains(chName))
    {
        ObjectWarning("Channel is ALREADY in the opening mode: " << chName);
        return false;
    }

    openingChannels[chName] = &chanID;

    /*SkArgsMap tempProps;

    if (!nodeName.isEmpty())
    {
        if (!props)
            props = &tempProps;

        (*props)["publisher"] = nodeName;
    }*/

    p->sendStartOfTransaction(FCMD_ADD_STREAMING_CHAN);

    p->sendFlowType(flow_t);
    p->sendVariantType(data_t);
    p->sendCStr(name);

    if (!SkString::isEmpty(mime))
        p->sendCStr(mime);

    else
        p->sendSize(0);

    if (props && !props->isEmpty())
    {
        SkVariant v(*props);
        SkString json;
        v.toJson(json);
        p->sendString(json);
    }

    else
        p->sendSize(0);

    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to ADD a new StreamingChannel: " << chName);

    //NO RESPONSE
    return true;
}

bool SkFlowAsync::addServiceChannel(SkFlowChanID &chanID, CStr *name)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    SkString chName(this->name);
    chName.append(".");
    chName.append(name);

    if (openingChannels.contains(chName))
    {
        ObjectWarning("Channel is ALREADY in the opening mode: " << chName);
        return false;
    }

    openingChannels[chName] = &chanID;

    p->sendStartOfTransaction(FCMD_ADD_SERVICE_CHAN);

    SkVariantList l;
    l << name;

    SkVariant v(l);
    SkString json;
    v.toJson(json);
    p->sendString(json);

    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to ADD a new ServiceChannel: " << chName);

    //NO RESPONSE
    return true;
}

bool SkFlowAsync::removeChannel(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        ObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_DEL_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to REMOVE a channel: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}

/*bool SkFlowAsync::getChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        ObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_CHAN_HEADER);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to REMOVE a channel: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}*/

bool SkFlowAsync::setChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b)
{
    if (!socket || !isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        ObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_SET_CHAN_HEADER);
    p->sendChanID(chanID);
    p->sendBuffer(b);
    p->sendEndOfTransaction();

    ObjectMessage("Setup HEADER [" << b.size() << " B] for a channel: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}

bool SkFlowAsync::setChannelProperties(SkFlowChanID chanID, SkArgsMap &props)
{
    if (!containsChannel(chanID))
    {
        ObjectError("ChanID NOT found: " << chanID);
        return false;
    }

    SkFlowChannel *ch = channel(chanID);

    SkString holdDbName;

    if (ch->name != currentDbName)
        holdDbName = currentDbName;

    setCurrentDbName(ch->name.c_str());

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = props.iterator();

    while(itr->next())
        setVariable(itr->item().key().c_str(), itr->item().value());

    delete itr;

    if (!holdDbName.isEmpty())
        setCurrentDbName(holdDbName.c_str());

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::attach(SkFlowChanID sourceID, SkFlowChanID targetID)
{
    p->sendStartOfTransaction(FCMD_ATTACH_CHAN);
    p->sendChanID(sourceID);
    p->sendChanID(targetID);
    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to ATTACH channels: " << channelsIndexes[sourceID]->name << " -> " << channelsIndexes[targetID]->name);
    //NO RESPONSE
    return true;
}

bool SkFlowAsync::detach(SkFlowChanID sourceID, SkFlowChanID targetID)
{
    p->sendStartOfTransaction(FCMD_DETACH_CHAN);
    p->sendChanID(sourceID);
    p->sendChanID(targetID);
    p->sendEndOfTransaction();

    ObjectMessage("REQUESTED to DETACH channels: " << channelsIndexes[sourceID]->name << " -> " << channelsIndexes[targetID]->name);
    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val)
{
    p->sendStartOfTransaction(FCMD_EXEC_SERVICE_REQUEST);
    p->sendChanID(chanID);
    p->sendCStr(cmd);
    p->sendJSON(val);
    p->sendEndOfTransaction();

    ObjectMessage("ServiceChannel REQUEST sent: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::publish(SkFlowChanID chanID)
{return publish(chanID, nullptr, 0);}

bool SkFlowAsync::publish(SkFlowChanID chanID, int8_t v)
{return publish(chanID, &v, sizeof(int8_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, uint8_t v)
{return publish(chanID, &v, sizeof(uint8_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, int16_t v)
{return publish(chanID, &v, sizeof(int16_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, uint16_t v)
{return publish(chanID, &v, sizeof(uint16_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, int32_t v)
{return publish(chanID, &v, sizeof(int32_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, uint32_t v)
{return publish(chanID, &v, sizeof(uint32_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, int64_t v)
{return publish(chanID, &v, sizeof(int64_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, uint64_t v)
{return publish(chanID, &v, sizeof(uint64_t));}

bool SkFlowAsync::publish(SkFlowChanID chanID, float v)
{return publish(chanID, &v, sizeof(float));}

bool SkFlowAsync::publish(SkFlowChanID chanID, double v)
{return publish(chanID, &v, sizeof(double));}

bool SkFlowAsync::publish(SkFlowChanID chanID, CStr *str)
{return publish(chanID, str, SkString::size(str));}

bool SkFlowAsync::publish(SkFlowChanID chanID, SkString &str)
{return publish(chanID, str.c_str(), str.size());}

bool SkFlowAsync::publish(SkFlowChanID chanID, SkVariantList &l)
{
    SkString json;
    SkVariant v(l);
    v.toJson(json);
    return publish(chanID, json.c_str(), json.size());
}

bool SkFlowAsync::publish(SkFlowChanID chanID, SkAbstractMap<SkString, SkVariant> &m)
{
    SkString json;
    SkVariant v(m);
    v.toJson(json);
    return publish(chanID, json.c_str(), json.size());
}

bool SkFlowAsync::publish(SkFlowChanID chanID, CVoid *data, uint64_t sz)
{return publishPacketizedData(chanID, data, sz);}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::publishPacketizedData(SkFlowChanID chanID, CVoid *val, uint64_t sz)
{
    if (chanID < 0)
        return false;

    while(!socket->canWrite())
        if (!socket->isConnected())
            return false;

    if (!channelsIndexes[chanID]->isPublishingEnabled)
        return false;

    p->sendStartOfTransaction(FCMD_PUBLISH);
    p->sendChanID(chanID);
    p->sendBuffer(val, sz);
    bool ok = p->sendEndOfTransaction();

    if (ok)
    {
        SkVariantVector params;
        params << chanID;
        channelDataPublished(this, params);
    }

    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::isSubscribed(SkFlowChanID chanID)
{
    return subscribedChannels.contains(chanID);
}

bool SkFlowAsync::subscribeChannel(CStr *name, SkFlowChanID &chanID)
{
    if (channelsNames.contains(name))
    {
        SkFlowChannel *channel = channelsNames[name];
        chanID = channel->chanID;
        return subscribeChannel(channel);
    }

    if (subscribingChannels.contains(name))
    {
        ObjectWarning("Channel is ALREADY in the subscribing mode: " << name);
        return false;
    }

    subscribingChannels[name] = &chanID;
    ObjectMessage("REQUESTED to subscribe a channel: " << name);

    return true;
}

bool SkFlowAsync::unsubscribeChannel(SkFlowChanID chanID)
{
    if (!subscribedChannels.contains(chanID))
    {
        ProtocolRecvError("Channel is NOT subscribed yet: " << chanID);
        return false;
    }

    return unsubscribeChannel(channelsIndexes[chanID]);
}

bool SkFlowAsync::subscribeChannel(SkFlowChannel *ch)
{
    if (!isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (subscribedChannels.contains(ch->chanID))
    {
        ProtocolRecvError("Channel is ALREADY subscribed: " << ch->name);
        return false;
    }

    p->sendStartOfTransaction(FCMD_SUBSCRIBE_CHAN);
    p->sendChanID(ch->chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE

    subscribedChannels.add(ch->chanID);
    ObjectMessage("Sent SUBSCRIBE for a Channel: " << ch->name);

    return true;
}

bool SkFlowAsync::unsubscribeChannel(SkFlowChannel *ch)
{
    if (!isConnected())
    {
        ObjectError("Socket is NOT connected");
        return false;
    }

    if (!subscribedChannels.contains(ch->chanID))
    {
        ProtocolRecvError("Channel is NOT subscribed yet: " << ch->name);
        return false;
    }

    p->sendStartOfTransaction(FCMD_UNSUBSCRIBE_CHAN);
    p->sendChanID(ch->chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE

    subscribedChannels.remove(ch->chanID);
    ObjectMessage("Sent UNSUBSCRIBE for a channel: " << ch->name);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ULong SkFlowAsync::getDataCount()
{
    return channelsData.count();
}

bool SkFlowAsync::hasNextData()
{
    return !channelsData.isEmpty();
}

bool SkFlowAsync::nextData()
{
    if (!hasNextData())
        return false;

    SkFlowChannelData *d = channelsData.dequeue();
    currentData = *d;
    delete d;

    return true;
}

SkFlowChannelData &SkFlowAsync::getCurrentData()
{
    return currentData;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *SkFlowAsync::getCurrentDbName()
{
    return currentDbName.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFlowAsync, onReadyRead)
{
    SilentSlotArgsWarning();

    while (socket != nullptr && socket->isConnected() && socket->bytesAvailable()>0)
        if (!analyze())
            break;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::analyze()
{
    //HERE WE GRAB THE RESPONSE
    if (lastResponse == FRSP_NORSP && socket->bytesAvailable() >= /*sizeof(uint32_t) + */sizeof(uint16_t))
        lastTransactionSize = p->recvStartOfTransaction(lastResponse);

    /*if (lastResponse != FRSP_NORSP && lastTransactionSize > 0 && socket->bytesAvailable() < lastTransactionSize)
        return;*/

    //cout << "!!!!!!!!!! " << p->responseToString(lastResponse) << " " << socket->bytesAvailable()<< "\n";

    if (lastResponse == FRSP_CHK_FLOW)
    {
        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CURRENTDB_CHANGED)
    {
        uint64_t sz;
        p->recvSize(sz);

        currentDbName.clear();
        p->recvString(currentDbName, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_ADDED)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChannel *channel = new SkFlowChannel;
        channel->isPublishingEnabled = false;

        p->recvChanType(channel->chan_t);
        p->recvChanID(channel->chanID);

        uint64_t sz;
        p->recvSize(sz);

        if (sz && !p->recvString(channel->name, sz))
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            delete channel;
            return false;
        }

        p->recvSize(sz);

        if (sz && !p->recvString(channel->hashID, sz))
        {
            ProtocolRecvError("CANNOT RECV channel hashID");
            return false;
        }

        //
        /*if (channelsIndexes.contains(channel->chanID))
        {
            ProtocolRecvError("Channel is ALREADY stored: " << channel->name << " [ChanID: " << channel->chanID << "]");
            delete channel;
            return false;
        }*/
        //

        if (channel->chan_t == StreamingChannel)
        {
            p->recvFlowType(channel->flow_t);
            p->recvVariantType(channel->data_t);

            p->recvSize(sz);

            if (sz && !p->recvString(channel->mime, sz))
            {
                ProtocolRecvError("CANNOT RECV channel->mime");
                delete channel;
                return false;
            }
        }

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            delete channel;
            return false;
        }

        // !!!!!!!!!!!!!!!
        if (channelsIndexes.contains(channel->chanID))
            ObjectError("Channel is ALREADY stored: " << channel->name << " [ChanID: " << channel->chanID << "]");

        else
        {
            channelsNames[channel->name] = channel;
            channelsIndexes[channel->chanID] = channel;
            bool isOwnerChannel = openingChannels.contains(channel->name);

            if (isOwnerChannel)
            {
                SkFlowChanID *chanID = openingChannels[channel->name];
                *chanID = channel->chanID;
                openingChannels.remove(channel->name);
                openChannels.add(chanID);
            }

            if (channel->chan_t == StreamingChannel || channel->chan_t == BlobChannel)
            {
                // BAH

                if (subscribingChannels.contains(channel->name))
                {
                    *(subscribingChannels[channel->name]) = channel->chanID;
                    subscribingChannels.remove(channel->name);
                    subscribeChannel(channel);
                }

                //
                if (channel->chan_t == StreamingChannel)
                {
                    ObjectDebug("ADDED StreamingChannel: "
                                << "[ID: " << channel->chanID
                                << "; name: " << channel->name
                                << "; flow_T: " << SkFlowProto::flowTypeToString(channel->flow_t)
                                << "; val_T: " << SkVariant::variantTypeName(channel->data_t)
                                << "; mime_T: " << channel->mime
                                //<< "; udm: " << channel->udm
                                << "; header: " << channel->header.size() << " B"
                                << "; http: " << channel->mpPath
                                << "]");
                }

                else if (channel->chan_t == ServiceChannel)
                {
                    ObjectDebug("ADDED ServiceChannel: "
                                << "[ID: " << channel->chanID
                                << "; name: " << channel->name
                                << "]");
                }

                else if (channel->chan_t == BlobChannel)
                {
                    //
                    ObjectDebug("ADDED BlobChannel: "
                                << "[ID: " << channel->chanID
                                << "; name: " << channel->name
                                << "]");
                }
            }

            onChannelAdded(channel->chanID);

            {
                SkVariantVector p;
                p << channel->chanID;
                p << channel->name;
                p << channel->chan_t;

                channelAdded(this, p);

                if ((channel->chan_t == StreamingChannel || channel->chan_t == BlobChannel)&& isOwnerChannel)
                {
                    publisherAdded(this, p);
                    onPublisherAdded(channel->chanID);
                }
            }
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_REMOVED)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];

        //
        SkFlowChanID *chID = nullptr;
        SkBinaryTreeVisit<SkFlowChanID *, nullptr_t> *itr = openChannels.iterator();

        while(itr->next())
            if (*(itr->item().key()) == channel->chanID)
            {
                ObjectWarning("ChanID RESET [" << channel->chanID << " > -1]: " << channel->name);
                chID = itr->item().key();
                *(chID) = -1;
                break;
            }

        delete itr;

        if (chID)
            openChannels.remove(chID);

        //
        if (channel->chan_t == StreamingChannel || channel->chan_t == BlobChannel)
        {
            if (subscribedChannels.contains(channel->chanID))
            {
                subscribedChannels.remove(chanID);
                ObjectWarning("AUTOMATIC UNSUBSCRIBE for a removed channel: " << channel->name);
            }
        }

        ObjectDebug("Channel REMOVED: "
                    << "[ID: " << channel->chanID
                    << "; name: " << channel->name
                    << "]");

        {
            SkVariantVector p;
            p << channel->chanID;
            p << channel->name;
            channelRemoved(this, p);
        }

        onChannelRemoved(channel->chanID);

        channelsIndexes.remove(channel->chanID);
        channelsNames.remove(channel->name);

        delete channel;
        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_HEADER)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];

        uint64_t sz;
        p->recvSize(sz);

        if (!sz)
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            return false;
        }

        channel->header.clear();

        if (sz && !p->recvBuffer(channel->header, sz))
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            return false;
        }

        channel->hasHeader = !channel->header.isEmpty();

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        {
            SkVariantVector p;
            p << channel->chanID;
            p << channel->name;
            p << channel->header.size();
            channelHeaderSetup(this, p);
        }

        onChannelHeaderSetup(channel->chanID);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SEND_RESPONSE_TO_REQUESTER)
    {
        SkFlowChanID chanID;
        p->recvChanID(chanID);

        uint64_t sz;
        p->recvSize(sz);

        SkVariant response;
        p->recvJSON(response, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        //cout << "#### " << response.toString() << "\n";

        ObjectMessage("ServiceChannel response RETURNED to requester: " << response.toString());

        {
            SkVariantVector p;
            p << chanID;
            p << response;

            serviceResponseReceived(this, p);
        }

        onServiceResponseReceived(chanID, response);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SEND_REQUEST_TO_SERVICE)
    {
        SkFlowChanID chanID;
        p->recvChanID(chanID);

        uint64_t sz;
        p->recvSize(sz);

        SkString hashStr;
        p->recvString(hashStr, sz);

        p->recvSize(sz);

        SkString cmdName;
        p->recvString(cmdName, sz);

        p->recvSize(sz);

        SkVariant val;
        p->recvJSON(val, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        //cout << "#### " << response.toString() << "\n";
        ObjectMessage("ServiceChannel request ACCEPTED from requester: " << val.toString());

        if (!wrk)
        {
            ObjectError("Worker is NULL");
            lastResponse = FRSP_NORSP;
            lastTransactionSize = 0;
            return false;
        }

        SkArgsMap m;
        val.copyToMap(m);

        SkWorkerTransaction *t = new SkWorkerTransaction(this);
        t->setProperty("hash", hashStr);
        t->setProperty("chanID", chanID);
        t->setCommand(wrk, nullptr, onWrkEvaluatedCmd_SLOT, cmdName.c_str(), &m);
        t->invoke();

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_PUBLISH_START)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];
        channel->isPublishingEnabled = true;

        {
            SkVariantVector p;
            p << channel->chanID;
            p << channel->name;
            channelPublishStartRequest(this, p);
        }

        onChannelPublishStartRequest(channel->chanID);

        ObjectDebug("Server has requested for channel-publish START: " << channel->name);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_PUBLISH_STOP)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];
        channel->isPublishingEnabled = false;

        {
            SkVariantVector p;
            p << channel->chanID;
            p << channel->name;
            channelPublishStopRequest(this, p);
        }

        onChannelPublishStopRequest(channel->chanID);

        ObjectDebug("Server has requested for channel-publish STOP: " << channel->name);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SUBSCRIBED_DATA)
    {
        if (!currentSubscribedGrabbing)
        {
            if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
                return false;

            SkFlowChanID chanID;
            p->recvChanID(chanID);

            currentSubscribedGrabbing = new SkFlowChannelData;
            currentSubscribedGrabbing->chanID = chanID;

            p->recvSize(currentGrabbingDataSize);

            // // // PULSE DOES NOT PASS
            /*if (!currentGrabbingDataSize)
            {
                ProtocolRecvError("Subscribed arriving-data size CANNOT BE 0");
                delete currentSubscribedGrabbing;
                currentSubscribedGrabbing = nullptr;
                return false;
            }*/
            // // //
        }

        ULong sz = socket->bytesAvailable();

        if (sz == 0)
            return false;

        if (sz < currentGrabbingDataSize)
        {
            if (!p->recvBuffer(currentSubscribedGrabbing->data, sz))
            {
                ProtocolRecvError("CANNOT GRAB channel->data");
                return false;
            }

            currentGrabbingDataSize -= currentSubscribedGrabbing->data.size();
            //cout << "!!!INCOMPLETE " << currentSubscribedGrabbing->data.size() << "\n";

            return false;
        }

        else
        {
            if (!p->recvBuffer(currentSubscribedGrabbing->data, currentGrabbingDataSize))
            {
                ProtocolRecvError("CANNOT GRAB channel->data");
                return false;
            }

            //cout << "!!!COMPLETE " << currentSubscribedGrabbing->data.size() << "\n";
        }

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        channelsData.enqueue(currentSubscribedGrabbing);

        if (channelsData.count() > maxDataQueueCount)
        {
            SkFlowChannelData *d = channelsData.dequeue();
            delete d;

            ObjectWarning("Removing channelData item because count > maxDataQueueCount: " << maxDataQueueCount);
        }

        SkVariantVector params;
        params << currentSubscribedGrabbing->chanID;

        channelDataAvailable(this, params);

        currentSubscribedGrabbing = nullptr;
        currentGrabbingDataSize = 0;
        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    return true;
}

SlotImpl(SkFlowAsync, onWrkEvaluatedCmd)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    SkVariant val(t->response());
    SkString hash = t->property("hash").toString();
    SkFlowChanID chanID = static_cast<SkFlowChanID>(t->property("chanID").toInt());

    p->sendStartOfTransaction(FCMD_RETURN_SERVICE_RESPONSE);
    p->sendChanID(chanID);
    p->sendString(hash);
    p->sendJSON(val);
    p->sendEndOfTransaction();

    t->destroyLater();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAsync::onLogin()
{
    p->sendStartOfTransaction(FCMD_SET_ASYNC);
    p->sendEndOfTransaction();

    setCurrentDbName(userName());

    Attach(socket, readyRead, this, onReadyRead, SkAttachMode::SkQueued);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAsync::onDisconnected()
{
    lastResponse = FRSP_NORSP;

    SkBinaryTreeVisit<SkFlowChanID *, nullptr_t> *itr = openChannels.iterator();

    while(itr->next())
        *(itr->item().key()) = -1;

    delete itr;

    openingChannels.clear();
    openChannels.clear();

    subscribingChannels.clear();
    subscribedChannels.clear();

    while(hasNextData())
    {
        SkFlowChannelData *channelData = channelsData.dequeue();
        delete channelData;
    }

    Detach(socket, readyRead, this, onReadyRead);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
