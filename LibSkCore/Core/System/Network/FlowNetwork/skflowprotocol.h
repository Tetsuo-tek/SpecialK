#ifndef SKFLOWPROTOCOL_H
#define SKFLOWPROTOCOL_H

#include "Core/System/Network/skabstractsocket.h"
#include "Core/System/skbufferdevice.h"
#include "Core/System/Network/FlowNetwork/skflowcommon.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowProto extends SkObject
{
    public:
        Constructor(SkFlowProto, SkObject);

        void setup(SkAbstractSocket *sckDevice);

        SkAbstractSocket *getSocket();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// SEND (buffered)

        void sendStartOfTransaction(SkFlowCommand cmd);
        void sendStartOfTransaction(SkFlowResponse rsp);
        void sendChanType(SkFlowChannel_T t);
        void sendFlowType(SkFlow_T t);
        void sendVariantType(SkVariant_T t);
        void sendChanID(SkFlowChanID chanID);
        void sendSize(uint32_t sz);
        void sendBool(bool val);
        void sendInt8(int8_t val);
        void sendUInt8(uint8_t val);
        void sendInt16(Short val);
        void sendUInt16(UShort val);
        void sendInt32(Int val);
        void sendUInt32(UInt val);
        void sendInt64(Long val);
        void sendUInt64(ULong val);
        void sendFloat(float val);
        void sendDouble(double val);
        void sendCStr(CStr *str);
        void sendString(SkString &str);
        void sendJSON(SkVariant &val);
        void sendVariant(SkVariant &val);
        void sendBuffer(SkDataBuffer &b);
        void sendBuffer(CVoid *data, uint64_t tempSize);
        void sendEndOfList();
        bool sendEndOfTransaction();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// RECV

        uint32_t recvStartOfTransaction(SkFlowCommand &cmd);
        uint32_t recvStartOfTransaction(SkFlowResponse &rsp);
        void recvChanType(SkFlowChannel_T &t);
        void recvFlowType(SkFlow_T &t);
        void recvVariantType(SkVariant_T &t);
        void recvChanID(SkFlowChanID &chanID);
        void recvSize(uint64_t &sz);
        bool recvBool();
        void recvInt8(int8_t &val);
        void recvUInt8(uint8_t &val);
        void recvInt16(Short &val);
        void recvUInt16(UShort &val);
        void recvInt32(Int &val);
        void recvUInt32(UInt &val);
        void recvInt64(Long &val);
        void recvUInt64(ULong &val);
        void recvFloat(float &val);
        void recvDouble(double &val);
        bool recvString(SkString &s, uint64_t &tempSize);
        bool recvJSON(SkVariant &val, uint64_t &tempSize);
        bool recvVariant(SkVariant &val, uint64_t &tempSize);
        bool recvBuffer(SkDataBuffer &b, uint64_t &tempSize);
        bool recvList(SkStringList &l);
        bool recvEndOfTransaction();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// REQ

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// ANSWER

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        static CStr *commandToString(SkFlowCommand cmd);
        static SkFlowCommand commandToBin(CStr *cmd);

        static CStr *responseToString(SkFlowResponse rsp);
        static SkFlowResponse responseToBin(CStr *rsp);

        static CStr *flowTypeToString(SkFlow_T flow_t);
        static SkFlow_T flowTypeToBin(CStr *flow_t);

    protected:
        SkAbstractSocket *sck;
        SkBufferDevice *sendingBufferDevice;
        SkFlowCommand currentSendCmdTransaction;
        SkFlowResponse currentSendRspTransaction;
        SkFlowCommand currentRecvCmdTransaction;
        SkFlowResponse currentRecvRspTransaction;
        SkDataBuffer sendingBuffer;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLOWPROTOCOL_H
