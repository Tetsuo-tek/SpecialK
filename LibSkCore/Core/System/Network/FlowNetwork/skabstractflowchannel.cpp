#include "skabstractflowchannel.h"
#include "skflowserver.h"
#include "skflowasync.h"

#include "Core/Object/skabstractworkerobject.h"
#include "Core/App/skeventloop.h"

AbstractConstructorImpl(SkAbstractFlowChannel, SkObject)
{
    svrName = DynCast(SkFlowServer, parent())->serverName();
    o = nullptr;
    localWrk = nullptr;
    db = nullptr;
    nodeConnection = nullptr;
    chan_t = ChannelNotValid;
    chanID = -1;
    replicatedChanID = -1;

    totalOutputBytes = 0;
    outputSpeed = 0;

    totalInputBytes = 0;
    inputSpeed = 0;

    SlotSet(onOneSecTick);
    SlotSet(asyncCloseCoreRequest);
    SlotSet(onLocalWrkEvaluatedCmd);
}

void SkAbstractFlowChannel::init(SkFlowChanID id, CStr *name)
{
    chanID = id;
    n = name;

    SkString str(svrName);
    str.append("[");
    str.concat(chanID);
    str.append("]");

    hashChanID = stringHash(str);

    setObjectName(n.c_str());

    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick, SkQueued);
}

void SkAbstractFlowChannel::setOwner(SkAbstractFlowConnection *conn)
{
    o = conn;
}

void SkAbstractFlowChannel::setWorker(SkAbstractWorkerObject *localWorker)
{
    localWrk = localWorker;
}

void SkAbstractFlowChannel::setPairDatabase(SkFlowPairsDatabase *pairDatabase)
{
    db = pairDatabase;

    db->setVariable("chanID", chanID);
    db->setVariable("name", n);
    db->setVariable("hashID", hashChanID);
    db->setVariable("svrName", svrName);

    if (o)
        db->setVariable("owner", o->objectName());

    else
        db->setVariable("owner", "SYSTEM");

    if (localWrk)
        db->setVariable("worker", localWrk->objectName());

    else
        db->setVariable("worker", "NULL");

    db->setVariable("totalInputBytes", 0);
    db->setVariable("inputSpeed", 0);
    db->setVariable("totalOutputBytes", 0);
    db->setVariable("outputSpeed", 0);

    onSetPairDatabase();
}

void SkAbstractFlowChannel::disable()
{
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick);

    ObjectWarning("CLEARING channel internal-data before it dies ..");

    /*if (!requesters.isEmpty())
    {
        ObjectWarning("ABORTING " << requesters.count() << " pending requests ..");

        SkBinaryTreeVisit<SkString, SkFlowServiceRequestStatus *> *itr = requesters.iterator();

        while(itr->next())
        {
            //SkString &hashStr = itr->item().key();

            //SkFlowRawSvrConnection *requester = itr->item().value();
            //requester->abortPendingRequest(hashStr.c_str());
            delete itr->item().value();
            ObjectWarning("PendingRequest REMOVED: " << itr->item().key());
        }

        delete itr;
        requesters.clear();
    }*/

    onDisable();
}

SkFlowServiceRequestStatus *SkAbstractFlowChannel::openRequest(SkFlowRawSvrConnection *requester, CStr *cmd, SkVariant &value)
{
    //MUST CHECK PERMISSIONS FROM CHAN-DATABASE

    SkString c = cmd;
    c.toUpperCase();

    SkString str(chanID);
    str.append(c);
    str.append(objectName());
    str.append(SkDateTime::currentDateTimeISOString());
    str.concat(requesters.count());
    str.concat(requesters.size());

    SkString hashStr = stringHash(str);

    SkFlowServiceRequestStatus *stptr = new SkFlowServiceRequestStatus();
    SkFlowServiceRequestStatus &st = *stptr;

    st.setup(c.c_str(), hashStr.c_str(), isCoreCommand(c.c_str()), requester);

    SkArgsMap m;
    m["hash"] = hashStr;
    m["value"] = value;
    m["isCoreCommand"] = st.isCoreCommand();
    m["chanID"] = chanID;
    m["user"] = requester->name();
    m["connID"] = requester->connectionID();
    m["node"] = requester->property("@nodeName");

    requesters[hashStr] = stptr;
    ObjectMessage("Request ADDED: " << st.hashID());

    if (st.isCoreCommand())
    {
        ObjectMessage("ChannelCore request ACCEPTED: " << c << " -> Args; " << value.toString() << " -> Requester; " << requester->connectionID());

        //HERE WE COULD CHECK FOR THIS-LEVEL CORE COMMANDs,
        //BEFORE TO CALL THE VIRTUAL onCoreCommand
        if (!onCoreCommand(c.c_str(), value, st))
            ObjectWarning("ChannelCore request has returned an ERROR: " << c << " -> Args; " << value.toString() << " -> Requester; " << requester->connectionID());

        /*SkVariantVector p;
        p << st.hashID() << st.response();
        eventLoop()->invokeSlot(asyncCloseCoreRequest_SLOT, this, nullptr, p);*/
    }

    else
    {
        if (o)
        {
            value = m;

            ObjectMessage("Normal request ACCEPTED: " << c << " -> Args; " << value.toString() << " -> Requester; " << requester->connectionID());
            sendServiceRequest(hashStr.c_str(), c.c_str(), value);
        }

        else
        {
            ObjectError("LocalWorkerObject is OUT of SERVICE!");
            delete requesters.remove(st.hashID()).value();
            return nullptr;

            /*if (!localWrk)
            {
                ObjectError("Local Worker is NOT valid; cannot accept Request coming from: " << requester->objectName());
                requesters.remove(hashStr);
                st.hasError = true;
                return st;
            }

            ObjectMessage("Request ACCEPTED on WorkerObject: " << n << "." << c << " -> Args; " << value.toString());

            SkWorkerTransaction *t = new SkWorkerTransaction(this);
            t->setProperty("hash", hashStr);
            t->setCommand(localWrk, nullptr, onLocalWrkEvaluatedCmd_SLOT, c.c_str(), &m);
            t->invoke();*/
        }
    }

    SkStringList requests;
    requesters.keys(requests);
    db->setVariable("pendingRequests", requests);

    return stptr;
}

bool SkAbstractFlowChannel::closeRequest(CStr *hashStr, SkVariant &response)
{
    if (!requesters.contains(hashStr))
    {
        ObjectError("CANNOT close REQUEST; request NOT found: " << hashStr);
        return false;
    }

    SkFlowRawSvrConnection *requester = nullptr;

    {
        SkFlowServiceRequestStatus *st = requesters.remove(hashStr).value();
        requester = st->requester();
        delete st;
    }

    ObjectMessage("Request REMOVED: " << hashStr << " -> Requester; " << requester->connectionID());
    SkFlowProto *p = requester->proto();

    if (requester->isAsynchronous())
    {
        p->sendStartOfTransaction(FRSP_SEND_RESPONSE_TO_REQUESTER);
        p->sendChanID(chanID);
    }

    else
        p->sendStartOfTransaction(FCMD_EXEC_SERVICE_REQUEST);

    p->sendJSON(response);
    p->sendEndOfTransaction();

    SkStringList requests;
    requesters.keys(requests);
    db->setVariable("pendingRequests", requests);

    ObjectMessage("Request CLOSED [" << hashStr << "] :" << response);
    return true;
}

void SkAbstractFlowChannel::destroyRequest(CStr *hashStr)
{
    if (!requesters.contains(hashStr))
    {
        ObjectError("CANNOT destroy REQUEST; request NOT found: " << hashStr);
        return;
    }

    delete requesters.remove(hashStr).value();

    SkStringList requests;
    requesters.keys(requests);

    db->setVariable("pendingRequests", requests);
    ObjectWarning("Request DESTROYED: " << hashStr);
}

SlotImpl(SkAbstractFlowChannel, asyncCloseCoreRequest)
{
    SilentSlotArgsWarning();

    SkString hashStr = Arg_String;
    SkVariant &response = Arg_AbstractVariadic;

    ObjectMessage("Making ASYNCHRONOUS closing related to the ChannelCore request: " << hashStr);
    closeRequest(hashStr.c_str(), response);
}

void SkAbstractFlowChannel::closeCoreRequest(SkFlowServiceRequestStatus &st, bool hasError, CStr *msg)
{
    st.setCoreResponse(hasError, msg);

    SkVariantVector p;
    p << st.hashID() << st.response();
    eventLoop()->invokeSlot(asyncCloseCoreRequest_SLOT, this, nullptr, p);
}

void SkAbstractFlowChannel::closeCoreRequest(SkFlowServiceRequestStatus &st, bool hasError, CStr *msg, const SkVariant &optData)
{
    st.setCoreResponse(hasError, msg, optData);

    SkVariantVector p;
    p << st.hashID() << st.response();
    eventLoop()->invokeSlot(asyncCloseCoreRequest_SLOT, this, nullptr, p);
}

SlotImpl(SkAbstractFlowChannel, onLocalWrkEvaluatedCmd)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);
    SkString hashStr = t->property("hash").toString();

    SkVariant response(t->response());
    closeRequest(hashStr.c_str(), response);

    t->destroyLater();
}

SlotImpl(SkAbstractFlowChannel, onOneSecTick)
{
    SilentSlotArgsWarning();

    if (inputSpeed)
    {
        totalInputBytes += inputSpeed;
        db->setVariable("totalInputBytes", totalInputBytes);
        db->setVariable("inputSpeed", inputSpeed);
        inputSpeed = 0;
    }

    if (outputSpeed)
    {
        totalOutputBytes += outputSpeed;
        db->setVariable("totalOutputBytes", totalOutputBytes);
        db->setVariable("outputSpeed", outputSpeed);
        outputSpeed = 0;
    }
}

void SkAbstractFlowChannel::sendServiceRequest(CStr *hashStr, CStr *cmd, SkVariant &val)
{
    SkFlowProto *p = dynamic_cast<SkFlowRawSvrConnection *>(o)->proto();

    p->sendStartOfTransaction(FRSP_SEND_REQUEST_TO_SERVICE);
    p->sendChanID(chanID);
    p->sendCStr(hashStr);
    p->sendCStr(cmd);
    p->sendJSON(val);
    p->sendEndOfTransaction();
}

bool SkAbstractFlowChannel::isReplicated()
{
    return (replicatedChanID > -1);
}

void SkAbstractFlowChannel::unsetReplicantID()
{
    nodeConnection = nullptr;
    //Controlled by addChannel arg pointer
    //replicatedChanID = -1;
}

SkFlowChannel_T SkAbstractFlowChannel::chanType()
{
    return chan_t;
}

SkFlowChanID SkAbstractFlowChannel::id()
{
    return chanID;
}

SkFlowChanID SkAbstractFlowChannel::replicatedID()
{
    return replicatedChanID;
}

CStr *SkAbstractFlowChannel::name()
{
    return n.c_str();
}

CStr *SkAbstractFlowChannel::hashID()
{
    return hashChanID.c_str();
}

SkAbstractFlowConnection *SkAbstractFlowChannel::owner()
{
    return o;
}

SkFlowPairsDatabase *SkAbstractFlowChannel::pairDatabase()
{
    return db;
}

CStr *SkAbstractFlowChannel::serverName()
{
    return svrName.c_str();
}
