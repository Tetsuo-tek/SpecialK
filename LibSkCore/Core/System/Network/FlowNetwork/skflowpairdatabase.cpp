#include "skflowpairdatabase.h"
#include "skflowredistrchannel.h"
#include "skflowredisexport.h"

#include "Core/Containers/skringbuffer.h"
#include "Core/System/Filesystem/skfsutils.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowPairsDatabase, SkObject)
{
    o = nullptr;
    pairsCh = nullptr;

#if defined(ENABLE_REDIS)
    r = nullptr;
#endif

    SignalSet(loaded);
    SignalSet(saved);
    SignalSet(pairChanged);
    SignalSet(flushed);
}

void SkFlowPairsDatabase::setOwner(SkAbstractFlowConnection *conn)
{
    o = conn;
}

void SkFlowPairsDatabase::setup(CStr *name, SkFlowRedistrChannel *pairsChannel, CStr *databasesPath)
{
    containerName = name;
    pairsCh = pairsChannel;
    dbPath = SkFsUtils::adjustPathEndSeparator(databasesPath);
}

#if defined(ENABLE_REDIS)
void SkFlowPairsDatabase::setRedisExport(SkFlowRedisExport *redisClient)
{
    r = redisClient;
}

void SkFlowPairsDatabase::synchrononizePairsOnRedis()
{
    if (!isRedisEnabled())
        return;

    SkBinaryTreeVisit<SkString, SkVariant> *itr = database.iterator();

    while(itr->next())
    {
        SkVariant &v = itr->item().value();

        SkString k(containerName);
        k.append(".");
        k.append(itr->item().key());

        SkString json;
        v.toJson(json);

        r->setString(k.c_str(), json.c_str());
    }

    delete itr;

    ObjectMessage("Redis synchronized");
}

bool SkFlowPairsDatabase::isRedisEnabled()
{
    return (r!=nullptr);
}
#endif

bool SkFlowPairsDatabase::load(CStr *filePath)
{
    SkString path;

    if (SkString::isEmpty(filePath))
        Stringify(path, dbPath << containerName << ".variant");

    else
        path = filePath;

    SkPathInfo info;
    SkFsUtils::fillPathInfo(path.c_str(), info);

    SkVariant v;

    SkString propsFilePath;
    Stringify(propsFilePath, SkFsUtils::adjustPathEndSeparator(info.absoluteParentPath.c_str()) << info.name << "-dbprops.json");

    if (SkFsUtils::exists(propsFilePath.c_str()))
    {
        if (!SkFsUtils::readJSON(propsFilePath.c_str(), v))
        {
            ObjectError("CANNOT load database-properties from file: " << propsFilePath);
            return false;
        }

        v.copyToMap(dbProps);

        ObjectMessage("LOADED database-properties from file: " << propsFilePath);
    }

    SkDataBuffer fileBuff;

    if (!SkFsUtils::readDATA(path.c_str(), fileBuff))
    {
        ObjectError("CANNOT load database from file: " << path);
        return false;
    }

    SkRingBuffer valBuff;
    valBuff.addData(fileBuff.data(), fileBuff.size());
    v.fromData(&valBuff, v);
    v.copyToMap(database);

    ObjectMessage("LOADED database from file: " << path);

#if defined(ENABLE_REDIS)
    synchrononizePairsOnRedis();
#endif

    loaded();
    return true;
}

bool SkFlowPairsDatabase::save(CStr *filePath)
{
    SkString path;

    if (SkString::isEmpty(filePath))
        Stringify(path, dbPath << containerName << ".variant");

    else
        path = filePath;

    SkPathInfo info;
    SkFsUtils::fillPathInfo(path.c_str(), info);

    SkVariant v;

    if (!dbProps.isEmpty())
    {
        v.setVal(dbProps);

        SkString propsFilePath;
        Stringify(propsFilePath, SkFsUtils::adjustPathEndSeparator(info.absoluteParentPath.c_str()) << info.name << "-dbprops.json");

        if (!SkFsUtils::writeJSON(propsFilePath.c_str(), v, true))
        {
            ObjectError("CANNOT save database-properties to file: " << propsFilePath);
            return false;
        }

        ObjectMessage("SAVED database-properties to file: " << path);
    }

    v.setVal(database);
    SkRingBuffer valBuff;
    v.toData(&valBuff);
    SkDataBuffer fileBuff;
    valBuff.copyTo(fileBuff);

    if (!SkFsUtils::writeDATA(info.completeAbsolutePath.c_str(), fileBuff))
    {
        ObjectError("CANNOT save database to file: " << path);
        return false;
    }

    ObjectMessage("SAVED database to file: " << path);

    saved();
    return true;
}

void SkFlowPairsDatabase::dump(SkVariant &value)
{
    value.setVal(database);
}

void SkFlowPairsDatabase::variablesKeys(SkStringList &keys)
{
    database.keys(keys);
}

bool SkFlowPairsDatabase::existsVariable(CStr *key)
{
    return database.contains(key);
}

bool SkFlowPairsDatabase::setVariable(CStr *key, const SkVariant &value)
{
    SkVariant &v = database[key];

    if (v != value)
    {
        v = value;
#if defined(ENABLE_REDIS)
        if (isRedisEnabled())
        {
            SkString json;
            SkVariant v(value);
            v.toJson(json);

            SkString k(containerName);
            k.append(".");
            k.append(key);

            r->setString(k.c_str(), json.c_str());
        }
#endif

        SkVariantVector p;
        p << "SET" << key << value;
        pairChanged(this, p);

        if (pairsCh)
        {
            p << containerName;

            SkVariant v(p);

            SkString json;
            v.toJson(json);

            pairsCh->publish(json);
        }

        return true;
    }

    return false;
}

SkVariant &SkFlowPairsDatabase::getVariable(CStr *key)
{
    if (!database.contains(key))
    {
        ObjectError("Requested key does NOT exist inside this database: " << key);
        return database.nv();
    }

    return database[key];
}

bool SkFlowPairsDatabase::delVariable(CStr *key)
{
    if (!database.contains(key))
    {
        ObjectError("Requested key does NOT exist inside this database: " << key);
        return false;
    }

    SkVariant value = database.remove(key).value();

#if defined(ENABLE_REDIS)
    if (isRedisEnabled())
    {
        SkString k(containerName);
        k.append(".");
        k.append(key);

        r->deleteKey(k.c_str());
    }
#endif
    SkVariantVector p;
    p << "DEL" << key << value;
    pairChanged(this, p);

    if (pairsCh)
    {
        p << containerName;

        SkVariant v(p);

        SkString json;
        v.toJson(json);

        pairsCh->publish(json);
    }

    return true;
}

void SkFlowPairsDatabase::flushall()
{
    database.clear();
    flushed();
}

void SkFlowPairsDatabase::databasePropertiesKeys(SkStringList &keys)
{
    dbProps.keys(keys);
}

bool SkFlowPairsDatabase::existsDatabaseProperty(CStr *key)
{
    return dbProps.contains(key);
}

bool SkFlowPairsDatabase::setDatabaseProperty(CStr *key, const SkVariant &value)
{
    SkVariant &v = dbProps[key];

    if (v != value)
    {
        v = value;
        return true;
    }

    return false;
}

SkVariant &SkFlowPairsDatabase::getDatabaseProperty(CStr *key)
{
    if (!dbProps.contains(key))
    {
        ObjectError("Requested database property-key does NOT exist: " << key);
        return dbProps.nv();
    }

    return dbProps[key];
}

bool SkFlowPairsDatabase::delDatabaseProperty(CStr *key)
{
    if (!dbProps.contains(key))
    {
        ObjectError("Requested database property-key does NOT exist: " << key);
        return false;
    }

    dbProps.remove(key);
    return true;
}

void SkFlowPairsDatabase::clearDatabaseProperties()
{
    dbProps.clear();
}

SkAbstractFlowConnection *SkFlowPairsDatabase::owner()
{
    return o;
}

CStr *SkFlowPairsDatabase::name()
{
    return containerName.c_str();
}

SkBinaryTreeVisit<SkString, SkVariant> *SkFlowPairsDatabase::iterator()
{
    return database.iterator();
}

/*SkTreeMap<SkString, SkVariant> &SkFlowPairsDatabase::db()
{
    return database;
}*/

SkVariant &SkFlowPairsDatabase::nv()
{
    return database.nv();
}
