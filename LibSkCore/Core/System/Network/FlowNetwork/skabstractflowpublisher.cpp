#include "skabstractflowpublisher.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"
#endif

AbstractConstructorImpl(SkAbstractFlowPublisher, SkObject)
{
    async = nullptr;

#if defined(ENABLE_HTTP)
    http = nullptr;
#endif

    SlotSet(channelAdded);
    SlotSet(channelRemoved);
}

void SkAbstractFlowPublisher::setClient(SkFlowAsync *flowAsync)
{
    if (flowAsync == async)
        return;

    async = flowAsync;

    Attach(async, channelAdded, this, channelAdded, SkQueued);//MUST BE QUEUED DUE SOME ASYNC PROBLEM WITH PUBLISHERs
    Attach(async, channelRemoved, this, channelRemoved, SkDirect);

    ObjectWarning("SET async client");
}

void SkAbstractFlowPublisher::unSetClient()
{
    if (async)
    {
        Detach(async, channelAdded, this, channelAdded);
        Detach(async, channelRemoved, this, channelRemoved);

        async = nullptr;

#if defined(ENABLE_HTTP)
        http = nullptr;
#endif

        ObjectWarning("UNSET async client");
    }
}

#if defined(ENABLE_HTTP)

void SkAbstractFlowPublisher::setHttpService(SkFlowAsyncHttpService *service)
{
    http = service;
    ObjectWarning("Setup HTTP service");
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractFlowPublisher *SkAbstractFlowPublisher::createPublisher(SkAbstractFlowPublisher *publisher)
{
    if (!async)
        return nullptr;

    publisher->setClient(async);

#if defined(ENABLE_HTTP)
    if (http)
        publisher->setHttpService(http);
#endif

    return publisher;
}

SlotImpl(SkAbstractFlowPublisher, channelAdded)
{
    SilentSlotArgsWarning();
    onChannelAdded(Arg_UInt16);
}

SlotImpl(SkAbstractFlowPublisher, channelRemoved)
{
    SilentSlotArgsWarning();
    onChannelRemoved(Arg_UInt16);
}

bool SkAbstractFlowPublisher::hasTargets(SkFlowChannel *ch)
{
    if (ch && ch->isPublishingEnabled)
        return true;

    return false;
}

#if defined(ENABLE_HTTP)

bool SkAbstractFlowPublisher::hasTargets(SkRawRedistrMountPoint *mp)
{
    if (/*http && */mp && !mp->isEmpty())
        return true;

    return false;
}

#endif

