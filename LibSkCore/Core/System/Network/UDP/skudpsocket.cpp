#include "skudpsocket.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skringbuffer.h"
#include <unistd.h>
#include <sys/ioctl.h>

void SkUdpSocketClose(SkObject *obj)
{
    SkUdpSocket *f = static_cast<SkUdpSocket *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkUdpSocketClose()");

    if (f->isOpen())
        f->close();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkUdpSocketMode)
{
    SetupEnumWrapper(SkUdpSocketMode);

    SetEnumItem(SkUdpSocketMode::USM_NOMODE);
    SetEnumItem(SkUdpSocketMode::USM_ONLYREAD);
    SetEnumItem(SkUdpSocketMode::USM_ONLYWRITE);
    SetEnumItem(SkUdpSocketMode::USM_READWRITE);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkUdpSocket, open, bool, Arg_Enum(SkUdpSocketMode), Arg_CStr, Arg_UInt16, Arg_Custom(SkIPAddress), Arg_UInt16)
DeclareMeth_INSTANCE_RET(SkUdpSocket, openForWrite, bool, Arg_CStr, Arg_UInt16)
DeclareMeth_INSTANCE_RET(SkUdpSocket, openForRead, bool, Arg_Custom(SkIPAddress), Arg_UInt16)
DeclareMeth_INSTANCE_RET(SkUdpSocket, isSequencial, bool)
DeclareMeth_INSTANCE_RET(SkUdpSocket, size, int64_t)
DeclareMeth_INSTANCE_RET(SkUdpSocket, bytesAvailable, uint64_t)
DeclareMeth_INSTANCE_VOID(SkUdpSocket, close)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkUdpSocket, SkAbstractDevice,
                {
                    AddEnumType(SkUdpSocketMode);

                    AddMeth_INSTANCE_RET(SkUdpSocket, open);
                    AddMeth_INSTANCE_RET(SkUdpSocket, openForWrite);
                    AddMeth_INSTANCE_RET(SkUdpSocket, openForRead);
                    AddMeth_INSTANCE_RET(SkUdpSocket, isSequencial);
                    AddMeth_INSTANCE_RET(SkUdpSocket, size);
                    AddMeth_INSTANCE_RET(SkUdpSocket, bytesAvailable);
                    AddMeth_INSTANCE_VOID(SkUdpSocket, close);
                })
{
    rx = nullptr;

    openMode = SkUdpSocketMode::USM_NOMODE;
    baseUpdateEnabled = false;
    sckReadValid = false;
    socketFD_READ = -1;
    socketFD_WRITE = -1;

    SlotSet(updateBase);

    addDtorCompanion(SkUdpSocketClose);
}

bool SkUdpSocket::open(SkUdpSocketMode mode,
                       CStr *hostName,
                       uint16_t hostPort,
                       SkIPAddress *listenAddress,
                       uint16_t listenPort)
{
    if ((mode == SkUdpSocketMode::USM_READWRITE || mode == SkUdpSocketMode::USM_ONLYREAD)
            && listenAddress && listenPort)
    {
        if (socketFD_READ > -1)
        {
            notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
            return false;
        }

        rx = new SkRingBuffer;
        socketFD_READ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        if (socketFD_READ < 0)
        {
            ObjectError("Cannot open UDP reader");
            return false;
        }

        memset((char *) &addrSource, 0, sizeof(addrSource));

        addrSource.sin_family = AF_INET;
        addrSource.sin_port = htons(listenPort);
        addrSource.sin_addr.s_addr = htonl(INADDR_ANY);

        if (::bind(socketFD_READ, (const struct sockaddr*) &addrSource, sizeof(addrSource)) != 0)
        {
            close();
            ObjectError("Cannot bind UDP reader");
            return false;
        }

        sckReadValid = true;

        notifyState(SkDeviceState::Open);
        enableBaseUpdate(true);
    }

    if ((mode == SkUdpSocketMode::USM_READWRITE || mode == SkUdpSocketMode::USM_ONLYWRITE)
            && !SkString::isEmpty(hostName) && hostPort)
    {
        memset((char *) &addrTarget, 0, sizeof(addrTarget));
        addrTarget.sin_family = AF_INET;
        addrTarget.sin_port = htons(hostPort);

        if (inet_aton(hostName, &addrTarget.sin_addr) == 0)
            return false;
    }

    if (mode == SkUdpSocketMode::USM_NOMODE)
    {
        ObjectError("Open mode is NOT valid");
        return false;
    }

    openMode = mode;
    return true;
}

bool SkUdpSocket::openForWrite(CStr *hostName, uint16_t hostPort)
{
    return open(SkUdpSocketMode::USM_ONLYWRITE, hostName, hostPort, nullptr, 0);
}

bool SkUdpSocket::openForRead(SkIPAddress *listenAddress, uint16_t listenPort)
{
    return open(SkUdpSocketMode::USM_ONLYREAD, nullptr, 0, listenAddress, listenPort);
}

uint64_t SkUdpSocket::bytesAvailable()
{
    if (!isOpen())
        return 0;

    return rx->size();
}

void SkUdpSocket::close()
{
    if (socketFD_READ == -1 && socketFD_WRITE == -1)
        return;

    if (socketFD_READ > -1)
    {
        enableBaseUpdate(false);
        delete rx;
        rx = nullptr;
        ::close(socketFD_READ);
        socketFD_READ = -1;
        notifyState(SkDeviceState::Closed);
    }

    if (socketFD_WRITE > -1)
    {
        ::close(socketFD_WRITE);
        socketFD_WRITE = -1;
    }

    openMode = SkUdpSocketMode::USM_NOMODE;
}

bool SkUdpSocket::enableBaseUpdate(bool value)
{
    if (value)
    {
        if (baseUpdateEnabled)
        {
            ObjectError("UDP-READSCK-UPDATE is ALREADY enabled");
            return false;
        }

        ObjectDebug("UDP-READSCK-UPDATE ENABLED");

        baseUpdateEnabled = true;
        Attach(eventLoop()->fastZone_SIG, pulse, this, updateBase, SkAttachMode::SkDirect);
        Attach(eventLoop()->oneSecZone_SIG, pulse, this, checkConnection, SkAttachMode::SkQueued);

        eventLoop()->addDescriptor(socketFD_READ);
    }

    else
    {
        if (!baseUpdateEnabled)
        {
            ObjectError("UDP-READSCK-UPDATE is ALREADY disabled");
            return false;
        }

        eventLoop()->delDescriptor(socketFD_READ);

        ObjectDebug("UDP-READSCK-UPDATE DISABLED");

        baseUpdateEnabled = false;
        Detach(eventLoop()->fastZone_SIG, pulse, this, updateBase);
        Detach(eventLoop()->oneSecZone_SIG, pulse, this, checkConnection);

        //if (isConnected())
        {
            sckReadValid = false;
            updateBase();
        }
    }

    onBaseUpdateChangeState(value);
    return true;
}

SlotImpl(SkUdpSocket, updateBase)
{
    SilentSlotArgsWarning();

    int64_t recvSize = 0;
    socklen_t slen = sizeof(addrSource);

    int count = 0;

    //MUST TEST IF HERE IT IS WREQUIRED A NON_BLOCKING READ O IS FIONREAD USABLE

    if (ioctl(socketFD_READ, FIONREAD, &count) < 0)
    {
        close();
        return;
    }

    if (count == 0)
        return;

    char *datagram = new char[count];

    if ((recvSize = recvfrom(socketFD_READ, datagram, static_cast<uint64_t>(count), 0, (struct sockaddr *) &addrSource, &slen)) != -1)
    {
        rx->addData(datagram, static_cast<uint64_t>(recvSize));
        readyRead(this);
    }

    delete [] datagram;
}

bool SkUdpSocket::readInternal(char *data, uint64_t &len, bool onlyPeek)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = false;

    if (rx)
        ok = rx->getCustomBuffer(data, len, onlyPeek);

    if (ok)
        notifyRead(len);

    return ok;
}

bool SkUdpSocket::writeInternal(CStr *data, uint64_t &len)
{
    if (socketFD_WRITE == -1)
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    //char tempData[512];
    socklen_t slen = sizeof(addrTarget);
    int64_t sent = sendto(socketFD_WRITE, data, len, 0, (struct sockaddr *) &addrTarget, slen);

    if (sent > 0)
    {
        len = static_cast<uint64_t>(sent);
        notifyWrite(len);
        return true;
    }

    return false;
}
