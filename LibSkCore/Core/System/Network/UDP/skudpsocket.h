/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKUDPSOCKET_H
#define SKUDPSOCKET_H

#include "Core/System/skabstractdevice.h"
#include <arpa/inet.h>

class SkIPAddress;
class SkRingBuffer;

enum SkUdpSocketMode
{
    USM_NOMODE,
    USM_ONLYREAD,
    USM_ONLYWRITE,
    USM_READWRITE
};

class SPECIALK SkUdpSocket extends SkAbstractDevice
{
    public:
        Constructor(SkUdpSocket, SkAbstractDevice);

        bool open(SkUdpSocketMode mode,
                  CStr *hostName,
                  uint16_t hostPort,
                  SkIPAddress *listenAddress,
                  uint16_t listenPort);

        //FACILITIES
        bool openForWrite(CStr *hostName, uint16_t hostPort);
        bool openForRead(SkIPAddress *listenAddress, uint16_t listenPort);

        bool isSequencial()         {return true;}
        int64_t size()              {return -1;}
        uint64_t bytesAvailable();
        void close();

        Slot(updateBase);

    private:
        SkUdpSocketMode openMode;
        bool baseUpdateEnabled;
        bool sckReadValid;

        int socketFD_READ;
        struct sockaddr_in addrSource;

        int socketFD_WRITE;
        struct sockaddr_in addrTarget;

        SkRingBuffer *rx;

        bool readInternal(char *data, uint64_t &len, bool onlyPeek=false);
        bool writeInternal(CStr *data, uint64_t &len);

        bool enableBaseUpdate(bool value);
        void onBaseUpdateChangeState(bool){}
};

#endif // SKUDPSOCKET_H
