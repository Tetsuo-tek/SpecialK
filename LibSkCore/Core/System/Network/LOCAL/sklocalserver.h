/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKLOCALSERVER_H
#define SKLOCALSERVER_H

#include "Core/System/Network/skabstractserver.h"
#include "Core/System/Network/LOCAL/skflatlocalserver.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "sklocalsocket.h"

class SPECIALK SkLocalServer extends SkAbstractServer
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkLocalServer, SkAbstractServer);

        /**
         * @brief Sets the local server permission
         * @param perm the permission as SkFilePerm reference
         */
        void setSocketPermission(SkFilePerm &perm);

        /**
         * @brief Opens the server, starting to listen for clients
         * @param path the local path name to use as server
         * @param maxQueuedConnections max number of connection that this server
         * can store as pending file-descriptors
         * @return true if there are not errors, otherwise false
         */
        bool start(CStr *path, uint64_t maxQueuedConnections);

        /**
         * @brief Closes the server listening
         */
        void stop();

        /**
         * @brief Gets the local server path name
         * @return the local path name
         */
        CStr *getServerPath();

    private:
        struct sockaddr_un svr;
        SkString svrPath;
        SkFilePerm sckPerm;

    protected:
        bool tryToAcceptSD(int &fdClient);
        void onAcceptConnection(SkAbstractSocket *);
};

#endif // SKLOCALSERVER_H
