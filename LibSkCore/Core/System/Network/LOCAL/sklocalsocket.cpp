#include "sklocalsocket.h"
#include <unistd.h>
#include <netdb.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkLocalSocket, connect, bool, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkLocalSocket, setAsServer)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkLocalSocket, SkAbstractSocket,
                {
                    AddMeth_INSTANCE_RET(SkLocalSocket, connect);
                    AddMeth_INSTANCE_VOID(SkLocalSocket, setAsServer);
                })
{}

bool SkLocalSocket::connect(CStr *path)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (SkString::isEmpty(path))
    {
        ObjectError("Socket-path is NOT valid");
        return false;
    }

    ObjectDebug("Connecting to TCP target: " << path);
    int socketFD = socket(AF_UNIX, SOCK_STREAM, 0);

    if (socketFD == -1)
    {
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, path);
        return false;
    }

    clnt.sun_family = AF_UNIX;
    strcpy(clnt.sun_path, path);

    bool ok = (::connect(socketFD, (const struct sockaddr *) &clnt, sizeof(sockaddr_un)) == 0);

    if (ok)
        setFileDescriptor(socketFD);

    else
    {
        ::close(socketFD);
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, path);
        notifyState(SkDeviceState::Closed);
    }

    return ok;
}
