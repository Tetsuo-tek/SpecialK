/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKLOCALSOCKET_H
#define SKLOCALSOCKET_H

#include "Core/System/Network/skabstractsocket.h"
#include "Core/System/Network/LOCAL/skflatlocalsocket.h"
#include <sys/un.h>

class SPECIALK SkLocalSocket extends SkAbstractSocket
{
    public:
        Constructor(SkLocalSocket, SkAbstractSocket);

        /**
         * @brief Connects as client to a local server path name
         * @param path the server path name
         * @return true if the connection is established, otherwise false
         */
        bool connect(CStr *path);

        /**
         * @brief Does nothing
         */
        void setAsServer(){}

    protected:
        struct sockaddr_un clnt;
};

#endif // SKLOCALSOCKET_H
