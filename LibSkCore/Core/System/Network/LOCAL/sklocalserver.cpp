#include "sklocalserver.h"
#include "Core/App/skapp.h"
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkLocalServer, setSocketPermission, *Arg_Custom(SkFilePerm))
DeclareMeth_INSTANCE_RET(SkLocalServer, start, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkLocalServer, stop)
DeclareMeth_INSTANCE_RET(SkLocalServer, getServerPath, CStr*)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkLocalServer, SkAbstractServer,
                {
                    AddMeth_INSTANCE_VOID(SkLocalServer, setSocketPermission);
                    AddMeth_INSTANCE_RET(SkLocalServer, start);
                    AddMeth_INSTANCE_VOID(SkLocalServer, stop);
                    AddMeth_INSTANCE_RET(SkLocalServer, getServerPath);
                })
{
    sckPerm.owner = SkFilePermItem::PM_RW_;
    sckPerm.group = SkFilePermItem::PM_NOPERM;
    sckPerm.other = SkFilePermItem::PM_NOPERM;
}

void SkLocalServer::setSocketPermission(SkFilePerm &perm)
{
    sckPerm = perm;
}

bool SkLocalServer::start(CStr *path, uint64_t maxQueuedConnections)
{
    if (svrSckFD != -1)
    {
        notifyError(SkServerError::ServerAlreadyOpen);
        return false;
    }

    if ((svrSckFD = ::socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    {
        notifyError(SkServerError::CannotCreateServer);
        return false;
    }

    ::unlink(path);

    svr.sun_family = AF_UNIX;
    ::strcpy(svr.sun_path, path);

    socklen_t t = sizeof(sockaddr_un);

    /*************************************************************/
    /* Set socket to be nonblocking. All of the sockets for      */
    /* the incoming connections will also be nonblocking since   */
    /* they will inherit that state from the listening socket.   */
    /*************************************************************/

    int rc = 0; //ioctl(socketFD, FIONBIO, (char *)&on);

    if (rc < 0 || bind(svrSckFD, (const struct sockaddr *) &svr, t) == -1)
    {
        stop();
        notifyError(SkServerError::CannotBind);
        return false;
    }

    svrPath = path;
    SkFsUtils::chmod(svrPath.c_str(), sckPerm);

    bool ok = enableLinistening(maxQueuedConnections);

    if (!ok)
        stop();

    return ok;
}

bool SkLocalServer::tryToAcceptSD(int &fdClient)
{
    sockaddr_un remote;
    socklen_t t = sizeof(remote);
    return ((fdClient = ::accept(svrSckFD, (sockaddr *) &remote, &t)) > -1);
}

void SkLocalServer::onAcceptConnection(SkAbstractSocket *)
{

}

void SkLocalServer::stop()
{
    if (svrSckFD != -1)
    {
        if (isListening())
            enableBaseUpdate(false);

        //eventLoop()->delDescriptor(socketFD);

        ::close(svrSckFD);
        svrSckFD = -1;

        ::unlink(svrPath.c_str());
        svrPath.clear();
    }
}

CStr *SkLocalServer::getServerPath()
{
    return svrPath.c_str();
}
