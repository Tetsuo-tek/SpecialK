#include "skabstractsocket.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skringbuffer.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include <netdb.h>

void SkAbstractSocketClose(SkObject *obj)
{
    SkAbstractSocket *f = static_cast<SkAbstractSocket *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkAbstractSocketClose()");

    if (f->isConnected())
        f->disconnect();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkAbstractSocket, setFileDescriptor, bool, Arg_Int)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, getFileDescriptor, int)
DeclareMeth_INSTANCE_VOID(SkAbstractSocket, setAsServer)
DeclareMeth_INSTANCE_VOID(SkAbstractSocket, setWriteBuffer, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, getWriteBuffer, uint64_t)
DeclareMeth_INSTANCE_VOID(SkAbstractSocket, setReadBuffer, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, getReadBuffer, uint64_t)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, isSequencial, bool)
//DeclareMeth_INSTANCE_RET(SkAbstractSocket, canReadLine, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, canWrite, bool)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, flush, bool)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, flushReadBuffer, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, flushWriteBuffer, bool)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, isConnected, bool)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, size, int64_t)
DeclareMeth_INSTANCE_RET(SkAbstractSocket, bytesAvailable, int64_t)
DeclareMeth_INSTANCE_VOID(SkAbstractSocket, close)

// // // // // // // // // // // // // // // // // // // // //

AbstractConstructorImpl(SkAbstractSocket, SkAbstractDevice,
                        {
                            AddMeth_INSTANCE_RET(SkAbstractSocket, setFileDescriptor);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, getFileDescriptor);
                            AddMeth_INSTANCE_VOID(SkAbstractSocket, setAsServer);
                            AddMeth_INSTANCE_VOID(SkAbstractSocket, setWriteBuffer);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, getWriteBuffer);
                            AddMeth_INSTANCE_VOID(SkAbstractSocket, setReadBuffer);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, getReadBuffer);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, isSequencial);
                            //AddMeth_INSTANCE_RET(SkAbstractSocket, canReadLine);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, canWrite);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, flush);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, flushReadBuffer);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, flushWriteBuffer);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, isConnected);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, size);
                            AddMeth_INSTANCE_RET(SkAbstractSocket, bytesAvailable);
                            AddMeth_INSTANCE_VOID(SkAbstractSocket, close);
                        })
{
    isServerSck = false;

    socketFD = -1;
    //closeFileDescriptorWhenFinished = true;
    sckValid = false;
    baseUpdateEnabled = false;

    SignalSet(connected);
    SignalSet(disconnected);
    SlotSet(disconnect);
    SlotSet(updateBase);
    SlotSet(checkConnection);

    addDtorCompanion(SkAbstractSocketClose);
}

bool SkAbstractSocket::setFileDescriptor(int fd)
{
    if (socketFD == -1)
    {
        socketFD = fd;

        notifyState(SkDeviceState::Open);
        enableBaseUpdate(true);
        ObjectDebug("Socket activated: " << fd);
        return true;
    }

    notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
    return false;
}

int SkAbstractSocket::getFileDescriptor()
{
    return socketFD;
}

void SkAbstractSocket::setWriteBuffer(uint64_t size)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    if (setsockopt(socketFD, SOL_SOCKET, SO_SNDBUF, &size, sizeof(uint64_t)) == -1)
        ObjectError("Cannot SET socket write-buffer size");
}

uint64_t SkAbstractSocket::getWriteBuffer()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    int buffSize;
    socklen_t optlen = sizeof(buffSize);

    if (getsockopt(socketFD, SOL_SOCKET, SO_SNDBUF, &buffSize, &optlen) == -1)
    {
        ObjectError("Cannot GET socket write-buffer size");
        return 0;
    }

    return static_cast<uint64_t>(buffSize);
}

void SkAbstractSocket::setReadBuffer(uint64_t size)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    if (setsockopt(socketFD, SOL_SOCKET, SO_RCVBUF, &size, sizeof(uint64_t)) == -1)
        ObjectError("Cannot SET socket read-buffer size");
}

bool SkAbstractSocket::waitForData(uint64_t bytes)
{
    if (bytes < 1)
    {
        ObjectError("CANNOT wait for 0 bytes");
        return false;
    }

    char c;
    uint64_t sz = bytes;
    return read(&c, sz, true);
}

uint64_t SkAbstractSocket::getReadBuffer()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    int buffSize;
    socklen_t optlen = sizeof(buffSize);

    if (getsockopt(socketFD, SOL_SOCKET, SO_RCVBUF, &buffSize, &optlen) == -1)
    {
        ObjectError("Cannot GET socket read-buffer size");
        return 0;
    }

    return static_cast<uint64_t>(buffSize);
}

bool SkAbstractSocket::canReadLine(uint64_t &lineLength)
{
    if (!isConnected())
    {
        lineLength = 0;
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t sz = bytesAvailable();

    if (sz)
    {
        char *d = new char [sz];

        if (read(d, sz, true))
        {
            for(uint64_t i=0; i<sz; i++)
                if (d[i] == '\n')
                {
                    lineLength = i+1;
                    delete [] d;
                    return true;
                }
        }

        delete [] d;
    }

    return false;
}

bool SkAbstractSocket::canWrite()
{
    if (!isConnected())
        return false;

    fd_set writeSet;
    FD_ZERO(&writeSet);

    struct timeval selectTimeout;
    selectTimeout.tv_sec = 0;
    selectTimeout.tv_usec = eventLoop()->getFastInterval()/*/10*/;

    FD_SET(socketFD, &writeSet);

    int activity = select(socketFD+1, nullptr, &writeSet, nullptr, &selectTimeout);

    if (activity < 0)
    {
        ObjectError("Select error testing write");
        return false;
    }

    return FD_ISSET(socketFD , &writeSet);
}

bool SkAbstractSocket::readInternal(char *data, uint64_t &len, bool onlyPeek)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = readFromSocket(data, len, onlyPeek);

    if (ok)
        notifyRead(len);

    sckValid = ok;
    return ok;
}

bool SkAbstractSocket::readFromSocket(char *data, uint64_t &len, bool onlyPeek)
{
    if (len == 0)
        len = bytesAvailable();

    /*if (len == 0)
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Device has NOT data");
        return false;
    }*/

    int64_t readSize = -1;
    errno = 0;

    int flags = 0;

    if (onlyPeek)
        flags = MSG_PEEK | MSG_NOSIGNAL;

    else
        flags = MSG_NOSIGNAL;

    readSize = recv(socketFD, data, len, flags);

    if (readSize > 0)
        len = static_cast<uint64_t>(readSize);

    else
    {
        len = 0;
        sckValid = false;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "recv error");
    }

    return (len > 0);
}

bool SkAbstractSocket::writeInternal(CStr *data, uint64_t &len)
{
    if (!isConnected())
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = writeToSocket(data, len);

    if (ok)
        notifyWrite(len);

    sckValid = ok;
    return ok;
}

bool SkAbstractSocket::writeToSocket(CStr *data, uint64_t &len)
{
    int64_t tempSentSize = send(socketFD, data, len, MSG_NOSIGNAL);

    if (tempSentSize < 0)
    {
        len = 0;

        //notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);
        sckValid = false;

        return false;
    }

    len = static_cast<uint64_t>(tempSentSize);
    return true;
}

uint64_t SkAbstractSocket::bytesAvailable()
{
    if (!isConnected())
        return 0;

    int count;

    if (ioctl(socketFD, FIONREAD, &count) < 0)
        return 0;

    return static_cast<uint64_t>(count);
}

bool SkAbstractSocket::isConnected()
{
    return isOpen() && sckValid;
}

bool SkAbstractSocket::flush()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = flushWriteBuffer();

    if (ok)
        ok = flushReadBuffer(0);

    return ok;
}

bool SkAbstractSocket::flushReadBuffer(uint64_t size)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t sz = bytesAvailable();

    if (sz == 0)
        return false;

    if (size == 0 || sz < size)
        size = sz;

    char *fakeBuffer = new char [size];
    bool ok = readFromSocket(fakeBuffer, size);
    delete [] fakeBuffer;
    return ok;
}

bool SkAbstractSocket::flushWriteBuffer()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return (fsync(socketFD) != 0);
}

void SkAbstractSocket::close()
{
    disconnect();
}

SlotImpl(SkAbstractSocket, disconnect)
{
    SilentSlotArgsWarning();

    if (!sckValid)
    {
        ObjectWarning("Socket ALREADY disconnected");
        return;
    }

    if (baseUpdateEnabled)
    {
        ObjectDebug("Disconnecting .. ");
        enableBaseUpdate(false);
    }
}

bool SkAbstractSocket::enableBaseUpdate(bool value)
{
    if (value)
    {
        if (baseUpdateEnabled)
        {
            ObjectWarning("SCK-UPDATE is ALREADY enabled");
            return false;
        }

        baseUpdateEnabled = true;
        sckValid = true;

        Attach(eventLoop()->fastZone_SIG, pulse, this, updateBase, SkAttachMode::SkDirect);
        Attach(eventLoop()->oneSecZone_SIG, pulse, this, checkConnection, SkAttachMode::SkDirect);
        onBaseUpdateChangeState(true);

        eventLoop()->addDescriptor(socketFD);

        ObjectDebug("SCK-UPDATE ENABLED: " << socketFD);
        connected();
    }

    else
    {
        if (!baseUpdateEnabled)
        {
            ObjectWarning("SCK-UPDATE is ALREADY disabled");
            return false;
        }

        eventLoop()->delDescriptor(socketFD);

        baseUpdateEnabled = false;
        sckValid = false;

        if (currentState() != SkDeviceState::Closed)
            notifyState(SkDeviceState::Closed);

        Detach(eventLoop()->fastZone_SIG, pulse, this, updateBase);
        Detach(eventLoop()->oneSecZone_SIG, pulse, this, checkConnection);

        ObjectDebug("SCK-UPDATE DISABLED: " << socketFD);
        onBaseUpdateChangeState(false);

        if (socketFD > 2)
        {
            ::close(socketFD);
            ObjectWarning("Socket closed: " << socketFD);
            socketFD = -1;
        }

        disconnected();
    }

    return true;
}

SlotImpl(SkAbstractSocket, updateBase)
{
    SilentSlotArgsWarning();

    if (socketFD == -1 || isPreparedToDie())
        return;

    if (sckValid)
        onUpdateBase();
}

void SkAbstractSocket::onUpdateBase()
{
    if (bytesAvailable())
        readyRead(this);
}

SlotImpl(SkAbstractSocket, checkConnection)
{
    SilentSlotArgsWarning();

    if (isPreparedToDie())
        return;

    if (!isConnectionValid()/* && baseUpdateEnabled*/)
    {
        ObjectDebug("Socket appears to be NOT valid; closing ..");
        enableBaseUpdate(false);
    }
}

bool SkAbstractSocket::isConnectionValid()
{
    if (socketFD == -1 || !sckValid)
        return false;

    char data;
    int64_t ret;

    //if ((ret = ::write(socketFD, &data, 0)) < 0) //it will disconnect too easly
    if ((ret = ::recv(socketFD, &data, 1, MSG_PEEK | MSG_DONTWAIT)) == 0 && (errno != EAGAIN ))//&& errno != EWOULDBLOCK)))
    {
        ObjectDebug("Connection check FAILED [ret==" << ret << "]");
        return false;
    }

    return true;
}

void SkAbstractSocket::onPreparingToDie()
{
    sckValid = false;
}
