/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTSERVER_H
#define SKABSTRACTSERVER_H

#include "Core/Object/skobject.h"
#include "Core/Containers/skqueue.h"
#include "Core/System/Network/skabstractflatserver.h"

enum SkAbstractServerState
{
    ServerListening,
    ServerClosed
};

class SkAbstractSocket;

class SPECIALK SkAbstractServer extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkAbstractServer, SkObject);

        /**
         * @brief Checks if this server is listening
         * @return true if it is listening, otherwise false
         */
        bool isListening();

        /**
         * @brief Gets the server current state
         * @return the current server state value
         */
        SkAbstractServerState getCurrentState();

        /**
         * @brief Gets the server current state as cstring
         * @return the current server state value, as cstring representation
         */
        CStr *getCurrentStateString();

        /**
         * @brief Gets the server current error
         * @return the current server error value
         */
        SkServerError getCurrentError();

        /**
         * @brief Gets the server current error as cstring
         * @return the current server error value, as cstring representation
         */
        CStr *getCurrentErrorString();

        /**
         * @brief Accepts the pending connection
         * @param sck the socket to initialized as accepted connection
         * @return true if there are not errors, otherwise false
         */
        bool acceptConnection(SkAbstractSocket *sck);

        /**
         * @brief Dequeue the pending file-descriptor to permits manual acception
         * on a socket through its method 'setFileDescriptor'
         * @param sd the file-descriptor to dequeue
         * @return true if there is a pending fd at least, otherwise false
         */
        bool dequeueSD(int &sd);

        /**
         * @brief Checks if there are pending file-descriptors
         * @return true if this server has pending file-descriptors
         */
        bool hasQueuedSockets();

        /**
         * @brief Gets the file-descriptors count value
         * @return the count number of the pending file-descriptors
         */
        uint64_t queuedSocketsCount();

        /**
         * @brief Closes the server listenbing, but here do nothing because it
         * depends on derived
         * server class
         */
        virtual void stop(){}

        /**
         * @brief Triggered when the server is open
         */
        Signal(svrOpen);

        /**
         * @brief Triggered when the server is closed
         */
        Signal(svrClosed);

        /**
         * @brief Triggered when the server store a new pending file-descriptor
         */
        Signal(newSvrConnection);

        /**
         * @brief Triggered when the server state is changed
         */
        Signal(stateChanged);

        /**
         * @brief Triggered when the server has met an error
         */
        Signal(errorOccurred);

        //THIS METH IS INTENDED ONLY FOR INTERNAL USE
        /**
         * @brief Internally used; invoked to perform tick on server
         */
        Slot(updateBase);

    protected:
        int svrSckFD;
        uint64_t maxQueuedSD;
        SkMutex sdQueueEx;
        //SkQueue<int *> sdQueue;
        SkQueue<int> sdQueue;

        SkAbstractServerState state;
        SkServerError error;

        bool enableLinistening(uint64_t sdQueueMax);
        bool enableBaseUpdate(bool value);

        virtual bool tryToAcceptSD(int &){return false;}

        void notifyState(SkAbstractServerState st);
        void notifyError(SkServerError err);

        virtual void onAcceptConnection(SkAbstractSocket *){}
        void onPreparingToDie();

    private:
        bool baseUpdateEnabled;
        void checkSdQueue();
};

#endif // SKABSTRACTSERVER_H
