#include "skabstractbound.h"

AbstractConstructorImpl(SkAbstractBound, SkDipoleProto)
{
    lastMsgSize = 0;

    SlotSet(onReadyRead);
    SlotSet(onDisconnect);

    SignalSet(newMessage);
    SignalSet(disconnected);
}

void SkAbstractBound::init()
{
    Attach(sck, readyRead, this, onReadyRead, SkQueued);
    Attach(sck, disconnected, this, onDisconnect, SkQueued);
}

void SkAbstractBound::close()
{
    if (sck->isOpen())
        sck->close();
}

bool SkAbstractBound::sendMessage(SkDipoleMessage *msg)
{
    sendStartOfTransaction(msg->f);
    sendString(msg->sourceKey);
    sendString(msg->targetKey);
    sendVariant(msg->data);

    return true;
}

SlotImpl(SkAbstractBound, onReadyRead)
{
    SilentSlotArgsWarning();

    if (!lastMsgSize)
    {
        if (!recvSize(lastMsgSize))
            return;
    }

    if (lastMsgSize && sck->bytesAvailable() >= lastMsgSize)
    {
        ULong sz;

        recvSize(sz);
        sck->read(lastReceivedMsg.sourceKey, sz);

        recvSize(sz);

        if (sz)
            sck->read(lastReceivedMsg.targetKey, sz);

        recvSize(sz);

        if (sz)
            recvVariant(lastReceivedMsg.data, sz);

        newMessage();
    }
}

SlotImpl(SkAbstractBound, onDisconnect)
{
    SilentSlotArgsWarning();
    disconnected();
}

SkDipoleMessage &SkAbstractBound::getLastReceivedMsg()
{
    return lastReceivedMsg;
}
