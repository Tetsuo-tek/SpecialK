#ifndef SKDIPOLENODE_H
#define SKDIPOLENODE_H

#include "sknorthbound.h"
#include "sksouthbound.h"

#include "Core/System/Network/TCP/sktcpserver.h"

class SkDipoleNode extends SkTcpServer
{
    public:
        Constructor(SkDipoleNode, SkTcpServer);

        void setNorthPole(CStr *key, CStr *masterAddress, UShort port);
        void setSouthNegativity(uint8_t max);
        void setKnownDipoles(SkStringList &keys);

        bool init();
        void close();

        Slot(onNewSouthConnection);

    private:
        SkString northKey;
        SkStringList knownKeys;

        uint8_t negativity;

        SkNorthBound *north;
        SkList<SkSouthBound *> south;
};

#endif // SKDIPOLENODE_H
