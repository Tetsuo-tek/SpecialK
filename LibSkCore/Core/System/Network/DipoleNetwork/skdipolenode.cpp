#include "skdipolenode.h"
#include <unistd.h>

ConstructorImpl(SkDipoleNode, SkTcpServer)
{
    north = nullptr;
    negativity = 1;

    SlotSet(onNewSouthConnection);
}

void SkDipoleNode::setNorthPole(CStr *key, CStr *masterAddress, UShort port)
{
    northKey = key;
}

void SkDipoleNode::setSouthNegativity(uint8_t max)
{
    negativity = max;
}

void SkDipoleNode::setKnownDipoles(SkStringList &keys)
{
    knownKeys = keys;
}

bool SkDipoleNode::init()
{
    Attach(this, newSvrConnection, this, onNewDeliveryClient, SkAttachMode::SkDirect);

    if (!start("0.0.0.0", 9998, 1))
        return false;

    ObjectMessage("SouthBound server initialized: " << "0.0.0.0" << ":" << 9998);
    return true;
}

void SkDipoleNode::close()
{
    SkAbstractListIterator<SkSouthBound *> *itr = south.iterator();

    while(itr->next())
    {
        SkSouthBound *sb = itr->item();
        sb->close();
        sb->destroyLater();
    }

    delete itr;

    stop();

    ObjectMessage("SouthBound server closed");
}

SlotImpl(SkDipoleNode, onNewSouthConnection)
{
    SilentSlotArgsWarning();

    SkAbstractServer *svr = dynamic_cast<SkAbstractServer *>(referer);

    int sd;
    svr->dequeueSD(sd);

    if (south.count() == negativity)
    {
        ::close(sd);
        return;
    }

    SkSouthBound *sb = new SkSouthBound(this);

    if (!sb->setSocketDescriptor(sd))
    {
        ::close(sd);
        sb->destroyLater();
        return;
    }

    south << sb;
}
