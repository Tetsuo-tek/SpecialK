#include "skdipoleprotocol.h"

ConstructorImpl(SkDipoleProto, SkObject)
{
    sck = nullptr;
    sendingBufferDevice = new SkBufferDevice(this);
}

void SkDipoleProto::setup(SkAbstractDevice *device)
{
    sck = device;

    sendingBuffer.setObjectName(this, "TxBuffer");

    sendingBufferDevice->setObjectName(this, "TxBufferDev");
    //it will NOT close, it can bypass closing because it has not an fd
    sendingBufferDevice->open(sendingBuffer);
}

bool SkDipoleProto::recvFlag(SkDipoleMessageFlag &flag)
{
    if (sck->bytesAvailable() < sizeof(UShort))
        return false;

    flag = static_cast<SkDipoleMessageFlag>(sck->readUInt16());
    return true;
}

bool SkDipoleProto::recvSize(ULong &sz)
{
    if (sck->bytesAvailable() < sizeof(UInt))
        return false;

    //IT IS RIGHT TO BE 32
    sz = sck->readUInt32();
    return true;
}

bool SkDipoleProto::recvVariantType(SkVariant_T &t)
{
    if (sck->bytesAvailable() < sizeof(UShort))
        return false;

    t = static_cast<SkVariant_T>(sck->readUInt16());
    return true;
}

bool SkDipoleProto::recvVariant(SkVariant &value, ULong sz)
{
    if (sz)
    {
        SkVariant_T t;
        recvVariantType(t);

        SkDataBuffer b;

        if (sck->read(&b, sz))
        {
            value.setVal(b.toVoid(), sz, t);
            return true;
        }
    }

    return false;
}

void SkDipoleProto::sendStartOfTransaction(SkDipoleMessageFlag flag)
{
    sendingBufferDevice->writeUInt16(flag);
}

void SkDipoleProto::sendSize(ULong sz)
{
    sendingBufferDevice->writeUInt32(sz);
}

void SkDipoleProto::sendString(SkString &str)
{
    ULong sz = str.size();
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(str.c_str(), sz);
}

void SkDipoleProto::sendVariantType(SkVariant_T t)
{
    sendingBufferDevice->writeUInt16(t);
}

void SkDipoleProto::sendVariant(SkVariant &val)
{
    ULong sz = val.size();
    sendSize(sz);

    sendVariantType(val.variantType());

    if (!sz)
        return;

    sendingBufferDevice->write(val.data(), sz);
}

bool SkDipoleProto::sendEndOfTransaction()
{
    bool ok = (sendingBufferDevice->writeUInt32(sendingBuffer.size()) && sck->write(&sendingBuffer));
    sendingBufferDevice->rewind();
    sendingBuffer.clear();
    return ok;
}
