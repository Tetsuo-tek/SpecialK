#ifndef SKNORTHBOUND_H
#define SKNORTHBOUND_H

#include "skabstractbound.h"
#include "Core/System/Network/TCP/sktcpsocket.h"

/*
    - North is a client-connection-server
    - It connects ONLY to a south-connection-server
    - If Dipole is a fork, negativity can be > 1 (it is max clnt number)
*/

class SkNorthBound extends SkAbstractBound
{
    public:
        Constructor(SkNorthBound, SkAbstractBound);

        bool connect();

    private:
        SkTcpSocket *clnt;
};

#endif // SKNORTHBOUND_H
