#include "sksouthbound.h"

ConstructorImpl(SkSouthBound, SkAbstractBound)
{
    clnt = new SkTcpSocket(this);
}

bool SkSouthBound::setSocketDescriptor(int sd)
{
    if (!clnt->setFileDescriptor(sd))
        return false;

    setup(clnt);
    init();
    return true;
}
