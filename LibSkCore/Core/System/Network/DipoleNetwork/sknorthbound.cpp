#include "sknorthbound.h"

ConstructorImpl(SkNorthBound, SkAbstractBound)
{
    clnt = new SkTcpSocket(this);
    setup(clnt);
}

bool SkNorthBound::connect()
{
    SkString address;

    if (!clnt->connect(address.c_str(), 9998))
        return false;

    setup(clnt);
    init();
    return true;
}

