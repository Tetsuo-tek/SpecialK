#ifndef SKSOUTHBOUND_H
#define SKSOUTHBOUND_H

#include "skabstractbound.h"
#include "Core/System/Network/TCP/sktcpsocket.h"

/*
    - South is a one-connection-server
    - It accepts ONLY north-client-connection
*/

class SkSouthBound extends SkAbstractBound
{
    public:
        Constructor(SkSouthBound, SkAbstractBound);

        bool setSocketDescriptor(int sd);

    private:
        SkTcpSocket *clnt;
};

#endif // SKSOUTHBOUND_H
