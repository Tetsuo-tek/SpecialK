#ifndef SKDIPOLEPROTOCOL_H
#define SKDIPOLEPROTOCOL_H

#include <Core/System/Network/skabstractsocket.h>
#include <Core/System/skbufferdevice.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkDipoleInfo
{
    SkString name;
    SkString address;
    SkString key;
    SkVariant data;
};

enum SkDipoleMessageFlag
{
    DipoleAnnouncement,
    DipolePresentation,
    DipoleData
};

struct SkDipoleMessage
{
    SkDipoleMessageFlag f;
    SkString sourceKey;
    SkString targetKey;//COULD BE EMPTY IF IT IS A MSG FOR ALL NODEs
    SkVariant data;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkDipoleProto extends SkObject
{
    public:
        Constructor(SkDipoleProto, SkObject);

        void setup(SkAbstractDevice *device);

    protected:
        SkAbstractDevice *sck;

        bool recvFlag(SkDipoleMessageFlag &flag);
        bool recvSize(ULong &sz);
        bool recvVariantType(SkVariant_T &t);
        bool recvVariant(SkVariant &value, ULong sz);

        void sendStartOfTransaction(SkDipoleMessageFlag flag);
        void sendSize(ULong sz);
        void sendString(SkString &str);
        void sendVariantType(SkVariant_T t);
        void sendVariant(SkVariant &val);
        bool sendEndOfTransaction();

    private:
        SkBufferDevice *sendingBufferDevice;
        SkDataBuffer sendingBuffer;
};

#endif // SKDIPOLEPROTOCOL_H
