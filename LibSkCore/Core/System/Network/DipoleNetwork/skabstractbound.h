#ifndef SKABSTRACTBOUND_H
#define SKABSTRACTBOUND_H

#include "skdipoleprotocol.h"

class SkAbstractBound extends SkDipoleProto
{
    public:
        void init();
        void close();

        bool sendMessage(SkDipoleMessage *msg);
        SkDipoleMessage &getLastReceivedMsg();

        Slot(onReadyRead);
        Slot(onDisconnect);

        Signal(newMessage);
        Signal(disconnected);

    protected:
        AbstractConstructor(SkAbstractBound, SkDipoleProto);

    private:
        SkDipoleMessage lastReceivedMsg;
        ULong lastMsgSize;
};

#endif // SKABSTRACTBOUND_H
