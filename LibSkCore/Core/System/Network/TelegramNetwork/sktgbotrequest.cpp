#include "sktgbotrequest.h"
#include "Core/System/Filesystem/skfile.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)

#define TG_API_URL  "https://api.telegram.org/bot"
#define TG_FILE_URL "https://api.telegram.org/file/bot"

ConstructorImpl(SkTgBotRequest, SkHttpRequest)
{
    downloadingFile = nullptr;
    currentGettingFile = nullptr;

    hasResultError = false;
    response = nullptr;

    SlotSet(onResponseGrabbed);
    SlotSet(onDataAdded);
    SlotSet(onFinished);

    SignalSet(executed);

    Attach(this, responsed, this, onResponseGrabbed, SkDirect);
    Attach(this, dataAdded, this, onDataAdded, SkDirect);
    Attach(this, completed, this, onFinished, SkQueued);
}

void SkTgBotRequest::prepareCommand(CStr *botToken, CStr *apiCommand, CStr *query, SkArgsMap *jsonPost)
{
    currentGettingFile = nullptr;
    response = new SkBufferDevice(this);

    token = botToken;
    command = apiCommand;

    if (query)
        cmdQuery = query;

    if (jsonPost)
        cmdJsonPost = *jsonPost;

    request = TG_API_URL;
    request.append(token);
    request.append("/");
    request.append(command);

    if (!cmdQuery.isEmpty())
    {
        request.append("?");
        request.append(cmdQuery);
    }

    url.set(request.c_str());

    response->open(data, SkBufferDeviceMode::BVM_READWRITE);
}

bool SkTgBotRequest::prepareDownload(CStr *botToken, SkTgBotGettingFile *file, CStr *localPath)
{
    currentGettingFile = file;
    downloadingFile = new SkFile(this);

    token = botToken;
    command = "download";// IT IS NOT THE REAL API COMMAND; REQUEST IS NOT STANDARD


    request = TG_FILE_URL;
    request.append(token);
    request.append("/");
    request.append(currentGettingFile->remoteFilePath);

    url.set(request.c_str());

    file->localFilePath = SkFsUtils::adjustPathEndSeparator(localPath);

    if (currentGettingFile->fileName.isEmpty())
        file->localFilePath.append(currentGettingFile->fileID);

    else
        file->localFilePath.append(currentGettingFile->fileName);

    downloadingFile->setFilePath(file->localFilePath.c_str());

    if (!downloadingFile->open(SkFileMode::FLM_ONLYWRITE))
    {
        downloadingFile->destroyLater();
        return false;
    }

    return true;
}

bool SkTgBotRequest::exec()
{
    hasResultError = false;

    SkArgsMap headers;
    headers["Host"] = "api.telegram.org";

    SkAbstractDevice *output = nullptr;

    if (response)
        output = response;
    else
        output = downloadingFile;

    if (!cmdJsonPost.isEmpty())
    {
        if (!post(url, output, cmdJsonPost, headers))
        {
            output->close();
            return false;
        }
    }

    else
    {
        if (!get(url, output, headers))
        {
            output->close();
            return false;
        }
    }

    SkArgsMap &requestedHeaders = getRequestedHeaders();

    SkStringList l;
    requestedHeaders.keys(l);

    ObjectMessage("Requested headers:");

    for(size_t i=0; i<l.count(); i++)
    {ObjectMessage("=>> " << l[i] << " = " << requestedHeaders[l[i]].data());}

    return true;
}

SlotImpl(SkTgBotRequest, onResponseGrabbed)
{
    SilentSlotArgsWarning();

    SkArgsMap &responsedHeaders = getResponsedHeaders();
    ObjectMessage("Responsed: " << httpCodeString(getResponsedCode()));

    SkStringList l;
    responsedHeaders.keys(l);

    for(size_t i=0; i<l.count(); i++)
        ObjectMessage("<<= " << l[i] << " = "  << responsedHeaders[l[i]].data());
}

SlotImpl(SkTgBotRequest, onDataAdded)
{
    SilentSlotArgsWarning();

    /*cout << ".";
    cout.flush();*/
}

SlotImpl(SkTgBotRequest, onFinished)
{
    SilentSlotArgsWarning();

    Detach(this, responsed, this, onResponseGrabbed);
    Detach(this, dataAdded, this, onDataAdded);
    Detach(this, completed, this, onFinished);

    if (response)
    {
        response->close();

        SkDataBuffer b;

        SkArgsMap &responsedHeaders = getResponsedHeaders();

        if (responsedHeaders.contains("Content-Encoding") && responsedHeaders["Content-Encoding"].toString() == "gzip")
        {
            if (!data.decompress(b))
                return;
        }

        else
            b = data;

        if (responsedHeaders.contains("Content-Type") && responsedHeaders["Content-Type"].toString() == "application/json")
        {
            SkArgsMap m;
            SkString json = b.toString();
            AssertKiller(!m.fromString(json.c_str()));

            ObjectMessage("Response: " << json);

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

            while(itr->next())
            {
                SkString &k = itr->item().key();
                SkVariant &v = itr->item().value();

                if (k == "ok")
                {
                    hasResultError = !v.toBool();

                    if (hasResultError)
                    {
                        reqResult = m;
                        resultErrorDescription = m["description"].toString();
                        break;
                    }
                }

                else if (k == "result")
                    reqResult = v;
            }

            delete itr;

            if (hasResultError)
            {ObjectError("Request has reported errors: " << resultErrorDescription);}
        }

        response->destroyLater();
        response = nullptr;
    }

    else
    {
        downloadingFile->close();

        if (getResponsedCode() != SkHttpStatusCode::Ok)
        {
            hasResultError = true;
            SkString json;
            SkFsUtils::readTEXT(downloadingFile->getFilePath(), json);
            ObjectError("Donwload has reported errors [HTTP code: " << getResponsedCode() << "]: " << json);
            SkFsUtils::rm(downloadingFile->getFilePath());
            KillApp();
        }

        downloadingFile->destroyLater();
        downloadingFile = nullptr;
    }

    executed();
}

CStr *SkTgBotRequest::botToken()
{
    return token.c_str();
}

CStr *SkTgBotRequest::apiCommand()
{
    return command.c_str();
}

CStr *SkTgBotRequest::query()
{
    return cmdQuery.c_str();
}

SkArgsMap &SkTgBotRequest::jsonPost()
{
    return cmdJsonPost;
}

SkVariant &SkTgBotRequest::result()
{
    return reqResult;
}

SkTgBotGettingFile *SkTgBotRequest::getCurrentGettingFile()
{
    return currentGettingFile;
}

bool SkTgBotRequest::hasError()
{
    return hasResultError;
}

CStr *SkTgBotRequest::error()
{
    return resultErrorDescription.c_str();
}

#endif
