#include "sktgbot.h"
#include "Core/App/skeventloop.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)

ConstructorImpl(SkTgBot, SkObject)
{
    lastReceivedUpdateID = 0;
    currentRequest = nullptr;

    SignalSet(newResponse);
    SlotSet(onRequestExecuted);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTgBot::setup(CStr *botToken, CStr *botName, CStr *botUserName)
{
    token = botToken;
    name = botName;
    userName = botUserName;

    SkTgBotRequest *req = getMe();
    requests.enqueue(req);

    eventLoop()->invokeSlot(onRequestExecuted_SLOT, this, this);

    ObjectMessage("Initialized: " << userName);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkTgBot, onRequestExecuted)
{
    SilentSlotArgsWarning();

    if (currentRequest)
    {
        if (SkString::compare(currentRequest->apiCommand(), "getUpdates"))
        {
            SkVariantList l;
            currentRequest->result().copyToList(l);

            if (!l.isEmpty())
                parseMessagesList(l);
        }

        else if (SkString::compare(currentRequest->apiCommand(), "getFile"))
        {
            if (!currentRequest->hasError())
            {
                SkTgBotGettingFile *f = gettingTelegramFilesInfos.dequeue();
                SkArgsMap m;

                currentRequest->result().copyToMap(m);
                f->remoteFilePath = m["file_path"].toString();

                onFileInfoGotten(f);

                if (f == f->msg->files.last())
                    onNewTelegram(f->msg);
            }
        }

        else if (SkString::compare(currentRequest->apiCommand(), "download"))
        {
            SkTgBotGettingFile *f = currentRequest->getCurrentGettingFile();
            onFileGotten(f);
        }

        else
        {
            SkVariantVector p;
            p << currentRequest->result();
            newResponse(this, p);
        }

        currentRequest->destroyLater();
        currentRequest = nullptr;
    }

    if (!receivedTelegrams.isEmpty())
    {
        while(!receivedTelegrams.isEmpty())
        {
            SkTgBotReceivedTelegram *msg = receivedTelegrams.dequeue();

            if (msg->files.isEmpty())
                onNewTelegram(msg);

            else
            {
                SkAbstractListIterator<SkTgBotGettingFile *> *itr = msg->files.iterator();

                while(itr->next())
                {
                    SkTgBotGettingFile *f = itr->item();
                    SkTgBotRequest *req = getFileInfo(f->fileID.c_str());
                    requests.enqueue(req);
                    gettingTelegramFilesInfos.enqueue(f);
                }

                delete itr;
                break;
            }
        }
    }

    if (requests.isEmpty())
        currentRequest = update();

    else
        currentRequest = requests.dequeue();

    currentRequest->exec();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkTgBotRequest *SkTgBot::getMe()
{
    SkTgBotRequest *req = new SkTgBotRequest(this);
    req->setObjectName(this, "GetMe");

    req->prepareCommand(token.c_str(), "getMe", nullptr, nullptr);
    Attach(req, executed, this, onRequestExecuted, SkQueued);

    return req;
}

SkTgBotRequest *SkTgBot::getFileInfo(CStr *fileID)
{
    SkTgBotRequest *req = new SkTgBotRequest(this);
    req->setObjectName(this, "GetFile");

    SkString query("file_id=");
    query.append(fileID);

    req->prepareCommand(token.c_str(), "getFile", query.c_str(), nullptr);
    Attach(req, executed, this, onRequestExecuted, SkQueued);

    return req;
}

SkTgBotRequest *SkTgBot::update()
{
    SkTgBotRequest *req = new SkTgBotRequest(this);
    req->setObjectName(this, "GetUpdates");

    SkString query("offset=");
    query.concat(lastReceivedUpdateID);

    req->prepareCommand(token.c_str(), "getUpdates", query.c_str(), nullptr);
    Attach(req, executed, this, onRequestExecuted, SkQueued);

    return req;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTgBot::sendMessage(Long idChat, CStr *text)
{
    SkTgBotRequest *req = new SkTgBotRequest(this);
    req->setObjectName(this, "SendMessage");

    SkArgsMap msg;
    msg["chat_id"] = idChat;
    msg["text"] = text;

    req->prepareCommand(token.c_str(), "sendMessage", nullptr, &msg);
    Attach(req, executed, this, onRequestExecuted, SkQueued);

    requests.enqueue(req);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTgBot::download(SkTgBotGettingFile *file, CStr *localFilePath)
{
    SkTgBotRequest *req = new SkTgBotRequest(this);
    req->setObjectName(this, "Download");

    if (!req->prepareDownload(token.c_str(), file, localFilePath))
    {
        req->destroyLater();
        ObjectError("Cannot prepare download: " << file->fileID);
        return false;
    }

    Attach(req, executed, this, onRequestExecuted, SkQueued);

    requests.enqueue(req);
    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTgBot::parseMessagesList(SkVariantList &l)
{
    SkAbstractListIterator<SkVariant> *itr = l.iterator();

    while(itr->next())
    {
        SkArgsMap m;
        itr->item().copyToMap(m);

        SkTgBotReceivedTelegram *msg = parseMessage(m);

        if (!itr->hasNext())
            lastReceivedUpdateID = msg->idUpdate + 1;

        if (msg->type == BotCommand)
        {
            msg->text = &msg->text.c_str()[1];
            msg->text.toLowerCase();

            //MUST CHECK IF space IS THE LAST CHAR
            Long pos = msg->text.find(" ");

            if (pos == -1)
                msg->cmd.command = msg->text.c_str();

            else
            {
                msg->cmd.argument = &msg->text.c_str()[pos+1];
                msg->cmd.command =  msg->text.c_str();

                Long sz = msg->cmd.command.size();
                msg->cmd.command.chop(sz - pos);
            }

            ObjectMessage("GOTTEN new COMMAND [from: " << msg->from.firstName << "] -> "
                      << msg->cmd.command  << " [arg: " << msg->cmd.argument << "]");
        }

        else
        {ObjectMessage("GOTTEN new MESSAGE [from: " << msg->from.firstName << "; type: " << msg->type<< "]");}

        receivedTelegrams.enqueue(msg);
    }

    delete itr;
}

SkTgBotReceivedTelegram *SkTgBot::parseMessage(SkArgsMap &m)
{
    SkArgsMap message;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    SkTgBotReceivedTelegram *msg = new SkTgBotReceivedTelegram;

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "update_id")
            msg->idUpdate = v.toUInt64();

        else if (k == "message")
            v.copyToMap(message);
    }

    delete itr;

    msg->type = TextMsg;
    itr = message.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "message_id")
            msg->id = v.toUInt64();

        else if (k == "from")
        {
            SkArgsMap m;
            v.copyToMap(m);
            parseMessageFrom(msg, m);
        }

        else if (k == "chat")
        {
            SkArgsMap m;
            v.copyToMap(m);
            parseMessageChat(msg, m);
        }

        else if (k == "date")
            msg->unixDateTime = v.toUInt64();

        else if (k == "document")
        {
            msg->type = DocumentMsg;

            SkArgsMap m;
            v.copyToMap(m);
            parseDocument(msg, m);
        }

        else if (k == "text" || k == "caption")
            msg->text = v.toString();

        else if (k == "entities")
        {
            SkVariantList l;
            v.copyToList(l);
            parseMessageEntitiesList(msg, l);
        }
    }

    delete itr;
    return msg;
}

void SkTgBot::parseMessageFrom(SkTgBotReceivedTelegram *telegram, SkArgsMap &m)
{
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "id")
            telegram->from.id = v.toInt64();

        else if (k == "is_bot")
            telegram->from.isBot = v.toBool();

        else if (k == "first_name")
            telegram->from.firstName = v.toString();

        else if (k == "language_code")
            telegram->from.lang = v.toString();
    }

    delete itr;
}

void SkTgBot::parseMessageChat(SkTgBotReceivedTelegram *telegram, SkArgsMap &m)
{
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "id")
            telegram->chat.id = v.toInt64();

        else if (k == "first_name")
            telegram->chat.name = v.toString();

        else if (k == "type")
        {
            SkString mode = v.toString();
            telegram->isPrivate = (mode == "private");
        }
    }

    delete itr;
}

void SkTgBot::parseDocument(SkTgBotReceivedTelegram *telegram, SkArgsMap &m)
{
    SkTgBotGettingFile *currentGettingFile = new SkTgBotGettingFile;
    currentGettingFile->msg = telegram;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "file_id")
            currentGettingFile->fileID = v.toString();

        else if (k == "file_unique_id")
            currentGettingFile->fileUniqueID = v.toString();

        else if (k == "file_name")
            currentGettingFile->fileName = v.toString();

        else if (k == "mime_type")
            currentGettingFile->mimeType = v.toString();

        else if (k == "file_size")
            currentGettingFile->size = v.toUInt64();

        else if (k == "thumb")
        {}
    }

    delete itr;

    telegram->files << currentGettingFile;
}

void SkTgBot::parseMessageEntitiesList(SkTgBotReceivedTelegram *telegram, SkVariantList &l)
{
    SkAbstractListIterator<SkVariant> *itr = l.iterator();

    while(itr->next())
    {
        SkVariant &v = itr->item();

        SkArgsMap m;
        v.copyToMap(m);
        parseMessageEntity(telegram, m);
    }

    delete itr;
}

void SkTgBot::parseMessageEntity(SkTgBotReceivedTelegram *telegram, SkArgsMap &m)
{
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "type")
        {
            SkString mode = v.toString();

            if (mode == "bot_command")
                telegram->type = BotCommand;
        }
    }

    delete itr;
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
