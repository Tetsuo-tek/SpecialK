#ifndef SKTGBOT_H
#define SKTGBOT_H

#include "sktgbotrequest.h"

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkTgBotMsgType
{
    TextMsg,
    DocumentMsg,
    PhotoMsg,
    LinkMsg,
    BotCommand
};

struct SkTgBotFrom
{
    bool isBot;
    Long id;
    SkString firstName;
    SkString lang;
};

struct SkTgBotCommand
{
    SkString command;
    SkString argument;
};

struct SkTgBotChat
{
    Long id;
    SkString name;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkTgBotReceivedTelegram
{
    ULong id;
    ULong idUpdate;
    bool isPrivate;
    SkTgBotMsgType type;

    SkTgBotFrom from;
    SkTgBotChat chat;

    SkTgBotCommand cmd;

    SkList<SkTgBotGettingFile *> files;

    SkString text;//it supports also 'caption' for documents and affines

    ULong unixDateTime;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkTgBot extends SkObject
{
    public:
        Constructor(SkTgBot, SkObject);

        void setup(CStr *botToken, CStr *botName, CStr *botUserName);

        void sendMessage(Long idChat, CStr *text);
        bool download(SkTgBotGettingFile *file, CStr *localFilePath);

        Signal(newResponse);
        Slot(onRequestExecuted);

    private:
        SkTgBotRequest *currentRequest;

        SkString token;
        SkString name;
        SkString userName;
        ULong lastReceivedUpdateID;

        SkQueue<SkTgBotRequest *> requests;
        SkVariantQueue results;

        SkTgBotRequest *getMe();
        SkTgBotRequest *getFileInfo(CStr *fileID);
        SkTgBotRequest *update();

        SkQueue<SkTgBotReceivedTelegram *> receivedTelegrams;
        SkQueue<SkTgBotGettingFile *> gettingTelegramFilesInfos;

        virtual void onFileInfoGotten(SkTgBotGettingFile *)     {}
        virtual void onFileGotten(SkTgBotGettingFile *)         {}
        virtual void onNewTelegram(SkTgBotReceivedTelegram *)   {}

        void parseMessagesList(SkVariantList &l);
        SkTgBotReceivedTelegram *parseMessage(SkArgsMap &m);
        void parseMessageFrom(SkTgBotReceivedTelegram *telegram, SkArgsMap &m);
        void parseMessageChat(SkTgBotReceivedTelegram *telegram, SkArgsMap &m);
        void parseDocument(SkTgBotReceivedTelegram *telegram, SkArgsMap &m);
        void parseMessageEntitiesList(SkTgBotReceivedTelegram *telegram, SkVariantList &l);
        void parseMessageEntity(SkTgBotReceivedTelegram *telegram, SkArgsMap &m);
};

#endif

//Bot command
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421759,
            "message" : {
                "message_id" : 122,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668424101,
                "text" : "/hello",
                "entities" : [
                    {
                        "offset" : 0,
                        "length" : 6,
                        "type" : "bot_command"
                    }
                ]
            }
        }
    ]
}*/

//Invite to channel
/*[
    {
        "update_id" : 177421803,
        "message" : {
            "message_id" : 237,
            "from" : {
                "id" : 1301960589,
                "is_bot" : false,
                "first_name" : "Tetsuo",
                "language_code" : "it"
            },
            "chat" : {
                "id" : 1301960589,
                "first_name" : "Tetsuo",
                "type" : "private"
            },
            "date" : 1669550765,
            "text" : "https://t.me/+kBiND2rPLdgzOWZk",
            "entities" : [
                {
                    "offset" : 0,
                    "length" : 30,
                    "type" : "url"
                }
            ]
        }
    }
]
*/

//new chat
/*[
    {
        "update_id" : 177421804,
        "my_chat_member" : {
            "chat" : {
                "id" : -1001825935644,
                "title" : "SkDipoleNetwork",
                "type" : "channel"
            },
            "from" : {
                "id" : 1301960589,
                "is_bot" : false,
                "first_name" : "Tetsuo",
                "language_code" : "it"
            },
            "date" : 1669550840,
            "old_chat_member" : {
                "user" : {
                    "id" : 5697613608,
                    "is_bot" : true,
                    "first_name" : "SkTgBot",
                    "username" : "skRobotBot"
                },
                "status" : "left"
            },
            "new_chat_member" : {
                "user" : {
                    "id" : 5697613608,
                    "is_bot" : true,
                    "first_name" : "SkTgBot",
                    "username" : "skRobotBot"
                },
                "status" : "administrator",
                "can_be_edited" : false,
                "can_manage_chat" : true,
                "can_change_info" : true,
                "can_post_messages" : true,
                "can_edit_messages" : true,
                "can_delete_messages" : true,
                "can_invite_users" : true,
                "can_restrict_members" : true,
                "can_promote_members" : false,
                "can_manage_video_chats" : true,
                "is_anonymous" : false,
                "can_manage_voice_chats" : true
            }
        }
    }
]*/

//Channel post
/*[
    {
        "update_id" : 177421805,
        "channel_post" : {
            "message_id" : 2,
            "sender_chat" : {
                "id" : -1001825935644,
                "title" : "SkDipoleNetwork",
                "type" : "channel"
            },
            "chat" : {
                "id" : -1001825935644,
                "title" : "SkDipoleNetwork",
                "type" : "channel"
            },
            "date" : 1669552288,
            "text" : "Hi guys, I'm ready!"
        }
    }
]*/

//channel changed name
/*[
    {
        "update_id" : 177421810,
        "channel_post" : {
            "message_id" : 6,
            "sender_chat" : {
                "id" : -1001825935644,
                "title" : "SkDipoleChannel",
                "type" : "channel"
            },
            "chat" : {
                "id" : -1001825935644,
                "title" : "SkDipoleChannel",
                "type" : "channel"
            },
            "date" : 1669553450,
            "new_chat_title" : "SkDipoleChannel"
        }
    }
]*/

//new group
/*[
    {
        "update_id" : 177421808,
        "my_chat_member" : {
            "chat" : {
                "id" : -876564630,
                "title" : "SkDipoleGroup",
                "type" : "group",
                "all_members_are_administrators" : true
            },
            "from" : {
                "id" : 1301960589,
                "is_bot" : false,
                "first_name" : "Tetsuo",
                "language_code" : "it"
            },
            "date" : 1669553159,
            "old_chat_member" : {
                "user" : {
                    "id" : 5697613608,
                    "is_bot" : true,
                    "first_name" : "SkTgBot",
                    "username" : "skRobotBot"
                },
                "status" : "left"
            },
            "new_chat_member" : {
                "user" : {
                    "id" : 5697613608,
                    "is_bot" : true,
                    "first_name" : "SkTgBot",
                    "username" : "skRobotBot"
                },
                "status" : "member"
            }
        }
    },
    {
        "update_id" : 177421809,
        "message" : {
            "message_id" : 239,
            "from" : {
                "id" : 1301960589,
                "is_bot" : false,
                "first_name" : "Tetsuo",
                "language_code" : "it"
            },
            "chat" : {
                "id" : -876564630,
                "title" : "SkDipoleGroup",
                "type" : "group",
                "all_members_are_administrators" : true
            },
            "date" : 1669553159,
            "group_chat_created" : true
        }
    }
]*/

//new member
/*[
    {
        "update_id" : 346048789,
        "message" : {
            "message_id" : 15,
            "from" : {
                "id" : 1301960589,
                "is_bot" : false,
                "first_name" : "Tetsuo",
                "language_code" : "it"
            },
            "chat" : {
                "id" : -876564630,
                "title" : "SkDipoleGroup",
                "type" : "group",
                "all_members_are_administrators" : true
            },
            "date" : 1669553612,
            "new_chat_participant" : {
                "id" : 5680976178,
                "is_bot" : true,
                "first_name" : "RadiotronikBot",
                "username" : "skRadiotronikBot"
            },
            "new_chat_member" : {
                "id" : 5680976178,
                "is_bot" : true,
                "first_name" : "RadiotronikBot",
                "username" : "skRadiotronikBot"
            },
            "new_chat_members" : [
                {
                    "id" : 5680976178,
                    "is_bot" : true,
                    "first_name" : "RadiotronikBot",
                    "username" : "skRadiotronikBot"
                }
            ]
        }
    }
]*/

//Bot message
/*{
    "ok" : true,
    "result" : {
        "message_id" : 123,
        "from" : {
            "id" : 5697613608,
            "is_bot" : true,
            "first_name" : "SpecialK BOT",
            "username" : "skRobotBot"
        },
        "chat" : {
            "id" : 1301960589,
            "first_name" : "Tetsuo",
            "type" : "private"
        },
        "date" : 1668424115,
        "text" : "Hi Father! I'm a good son"
    }
}*/


//Text
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421753,
            "message" : {
                "message_id" : 116,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668423473,

                "text" : "ciao"
            }
        }
    ]
}*/

//Document .pdf
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421758,
            "message" : {
                "message_id" : 121,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668424014,
                "document" : {
                    "file_name" : "telegram-api_tesi.pdf",
                    "mime_type" : "application/pdf",
                    "thumb" : {
                        "file_id" : "AAMCBAADGQEAA3ljciFNLFosvjqDttZv_3nnP_2Z4gACYA0AArCRkVM2XIq4XMn7XAEAB20AAysE",
                        "file_unique_id" : "AQADYA0AArCRkVNy",
                        "file_size" : 5565,
                        "width" : 226,
                        "height" : 320
                    },
                    "file_id" : "BQACAgQAAxkBAAN5Y3IhTSxaLL46g7bWb_955z_9meIAAmANAAKwkZFTNlyKuFzJ-1wrBA",
                    "file_unique_id" : "AgADYA0AArCRkVM",
                    "file_size" : 6833272
                },
                "caption" : "sss"
            }
        }
    ]
}*/

//Document .m3u
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421754,
            "message" : {
                "message_id" : 117,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668423478,

                "document" : {
                    "file_name" : "radiotronik.m3u",
                    "mime_type" : "audio/x-mpegurl",
                    "file_id" : "BQACAgQAAxkBAAN1Y3IfNeWE23sZjyzqmthnDo_FrkUAAl4NAAKwkZFTBEi41YN9v9MrBA",
                    "file_unique_id" : "AgADXg0AArCRkVM",
                    "file_size" : 110
                },
                "caption" : "ss"
            }
        }
    ]
}*/

//Photo
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421755,
            "message" : {
                "message_id" : 118,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668423699,
                "photo" : [
                    {
                        "file_id" : "AgACAgQAAxkBAAN2Y3IgE7jBqU6sm3m9uzxmsa5mfgsAAsq4MRuwkZFThOvMlyogUmUBAAMCAANzAAMrBA",
                        "file_unique_id" : "AQADyrgxG7CRkVN4",
                        "file_size" : 1124,
                        "width" : 90,
                        "height" : 51
                    },
                    {
                        "file_id" : "AgACAgQAAxkBAAN2Y3IgE7jBqU6sm3m9uzxmsa5mfgsAAsq4MRuwkZFThOvMlyogUmUBAAMCAANtAAMrBA",
                        "file_unique_id" : "AQADyrgxG7CRkVNy",
                        "file_size" : 10836,
                        "width" : 320,
                        "height" : 180
                    },
                    {
                        "file_id" : "AgACAgQAAxkBAAN2Y3IgE7jBqU6sm3m9uzxmsa5mfgsAAsq4MRuwkZFThOvMlyogUmUBAAMCAAN4AAMrBA",
                        "file_unique_id" : "AQADyrgxG7CRkVN9",
                        "file_size" : 25835,
                        "width" : 640,
                        "height" : 360
                    }
                ],
                "caption" : "photo"
            }
        }
    ]
}*/

//Url
/*{
    "ok" : true,
    "result" : [
        {
            "update_id" : 177421757,
            "message" : {
                "message_id" : 120,
                "from" : {
                    "id" : 1301960589,
                    "is_bot" : false,
                    "first_name" : "Tetsuo",
                    "language_code" : "it"
                },
                "chat" : {
                    "id" : 1301960589,
                    "first_name" : "Tetsuo",
                    "type" : "private"
                },
                "date" : 1668423753,
                "text" : "https://gitlab.com/Tetsuo-tek/SpecialK",
                "entities" : [
                    {
                        "offset" : 0,
                        "length" : 38,
                        "type" : "url"
                    }
                ]
            }
        }
    ]
}*/

#endif //SKTGBOT_H
