#ifndef SKTGBOTREQUEST_H
#define SKTGBOTREQUEST_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_HTTP)

#include "Core/System/Network/TCP/HTTP/skhttprequest.h"
#include "Core/System/skbufferdevice.h"
#include <Core/System/Filesystem/skfsutils.h>
#include "Core/System/Time/skdatetime.h"

class SkFile;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkTgBotReceivedTelegram;

struct SkTgBotGettingFile
{
    SkTgBotReceivedTelegram *msg;

    SkString fileID;
    SkString fileUniqueID;
    SkString fileName;
    SkString mimeType;
    ULong size;

    SkString remoteFilePath;
    SkString localFilePath;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkTgBotRequest extends SkHttpRequest
{
    public:
        Constructor(SkTgBotRequest, SkHttpRequest);

        void prepareCommand(CStr *botToken, CStr *apiCommand, CStr *query, SkArgsMap *jsonPost);
        bool prepareDownload(CStr *botToken, SkTgBotGettingFile *file, CStr *localPath);

        bool exec();

        CStr *botToken();
        CStr *apiCommand();
        CStr *query();
        SkArgsMap &jsonPost();
        SkVariant &result();
        SkTgBotGettingFile *getCurrentGettingFile();
        bool hasError();
        CStr *error();

        Slot(onResponseGrabbed);
        Slot(onDataAdded);
        Slot(onFinished);

        Signal(executed);

    private:
        SkString command;
        SkString cmdQuery;
        SkString token;
        SkArgsMap cmdJsonPost;

        SkUrl url;

        SkString request;

        SkVariant reqResult;
        bool hasResultError;
        SkString resultErrorDescription;
        SkBufferDevice *response;
        SkDataBuffer data;

        SkTgBotGettingFile *currentGettingFile;
        SkFile *downloadingFile;
};

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKTGBOTREQUEST_H
