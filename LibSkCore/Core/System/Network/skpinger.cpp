#include "skpinger.h"
#include "Core/System/Network/sknetutils.h"
#include "Core/Containers/skarraycast.h"
#include <unistd.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkPinger, setCustomData, Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkPinger, setCustomData, 1, Arg_UInt64, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkPinger, setTTL, Arg_Int)
DeclareMeth_INSTANCE_RET(SkPinger, start, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkPinger, pckSize, uint64_t)
DeclareMeth_INSTANCE_RET(SkPinger, hostTarget, CStr*)
DeclareMeth_INSTANCE_RET(SkPinger, hostIP, CStr*)
DeclareMeth_INSTANCE_RET(SkPinger, hostTargetReversed, CStr*)
DeclareMeth_INSTANCE_RET(SkPinger, hasReverseLookup, bool)
DeclareMeth_INSTANCE_RET(SkPinger, sent, uint)
DeclareMeth_INSTANCE_RET(SkPinger, received, uint)
DeclareMeth_INSTANCE_RET(SkPinger, ttl, int)
DeclareMeth_INSTANCE_RET(SkPinger, rtt, double)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkPinger, SkFlatTimer,
                {
                    AddMeth_INSTANCE_VOID(SkPinger, setCustomData);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkPinger, setCustomData, 1);
                    AddMeth_INSTANCE_VOID(SkPinger, setTTL);
                    AddMeth_INSTANCE_RET(SkPinger, start);
                    AddMeth_INSTANCE_RET(SkPinger, pckSize);
                    AddMeth_INSTANCE_RET(SkPinger, hostTarget);
                    AddMeth_INSTANCE_RET(SkPinger, hostIP);
                    AddMeth_INSTANCE_RET(SkPinger, hostTargetReversed);
                    AddMeth_INSTANCE_RET(SkPinger, hasReverseLookup);
                    AddMeth_INSTANCE_RET(SkPinger, sent);
                    AddMeth_INSTANCE_RET(SkPinger, received);
                    AddMeth_INSTANCE_RET(SkPinger, ttl);
                    AddMeth_INSTANCE_RET(SkPinger, rtt);
                })
{
    hasReverse = false;
    lastElapsedTime = 0;
    sentCount = 0;
    receivedCount = 0;
    socketFD = -1;
    ttlVal = 64;

    header.type = ICMP_ECHO;
    header.un.echo.id = static_cast<uint16_t>(getpid());
}

void SkPinger::setCustomData(uint64_t size, CStr *data)
{
    if (size == 0)
    {
        ObjectError("Custom size is 0");
        return;
    }

    uint64_t hdrSz = sizeof(struct icmphdr);

    if (size <= hdrSz)
    {
        ObjectError("Custom size must be >= " << hdrSz);
        return;
    }

    if (data)
        pingDataBuffer.setData(data, size);

    else
        pingDataBuffer.fill(1+'0', size-hdrSz);

    ObjectDebug("Ping CUSTOM-BUFFER size is: " << pingDataBuffer.size());
}

void SkPinger::setTTL(int ttl)
{
    ttlVal = ttl;
}

bool SkPinger::start(CStr *target)
{
    pingTargetHost = target;
    ObjectDebug("Starting ICMP-PING session on target: " << pingTargetHost << " ..");

    if (!checkDnsLookups(target))
        return false;

    if (!open())
        return false;

    if (pingDataBuffer.isEmpty())
        pingDataBuffer.fill(1+'0', 64-sizeof(struct icmphdr));

    SkFlatTimer::start();
    return true;
}

bool SkPinger::checkDnsLookups(CStr *target)
{
    SkString ip;

    if (!SkNetUtils::dnsDirectLookup(target, ip, &addr))
    {
        ObjectError("Cannot start ICMP-PING session on target: " << target);
        return false;
    }

    lookupedIpAddress = ip;
    ObjectDebug("PingTarget DNS-DIRECT-LOOKUP: " << target << " -> " << lookupedIpAddress);

    hasReverse = false;

    if (SkNetUtils::dnsReverseLookup(ip.c_str(), reversedHostName))
    {
        hasReverse = true;
        ObjectDebug("PingTarget has DNS-REVERSE-LOOKUP: " << ip << " -> " << reversedHostName);
    }

    else
        reversedHostName = ip;

    return true;
}

bool SkPinger::open()
{
    socketFD = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

    if(socketFD < 0)
    {
        socketFD = -1;
        ObjectError("Cannot open ICMP-SOCKET");
        return false;
    }

    //set socket options at ip to TTL and value
    if (setsockopt(socketFD, SOL_IP, IP_TTL, &ttlVal, sizeof(ttlVal)) != 0)
    {
        close();

        ObjectError("Setting socket options to TTL failed");
        return false;
    }

    ObjectDebug("ICMP-PING session ttl will be: " << ttlVal);

    struct timeval sckTimeOutValue;
    sckTimeOutValue.tv_sec = 1;
    sckTimeOutValue.tv_usec = 0;

    // setting timeout of recv setting
    setsockopt(socketFD, SOL_SOCKET, SO_RCVTIMEO, SkArrayCast::toChar(&sckTimeOutValue), sizeof(sckTimeOutValue));
    return true;
}

uint16_t SkPinger::pckChkSum()
{
    uint16_t *buf = SkArrayCast::toUInt16(pingPckBuffer.toVoid());
    unsigned int sum =0;
    uint16_t result;

    uint64_t sz = pingPckBuffer.size();

    for (sum=0; sz>1; sz-=2)
        sum += *buf++;

    if (sz == 1)
        sum += *(unsigned char*)buf;

    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    result = ~sum;

    return result;
}

void SkPinger::pckPrepare()
{
    header.un.echo.sequence = sentCount++;
    pingPckBuffer.setData(SkArrayCast::toChar(&header), sizeof(struct icmphdr));
    pingDataBuffer.copyTo(pingPckBuffer);

    struct icmphdr *hdr = static_cast<struct icmphdr *>(pingPckBuffer.toVoid());
    hdr->checksum = pckChkSum();
}

void SkPinger::onStart()
{
    if (socketFD == -1)
    {
        ObjectError("RAW-SCK is NOT open; stopping ..");
        stop();
        return;
    }

    pongElapsedTime.start();
}

#include "Core/Containers/skvariant.h"

void SkPinger::timeout()
{
    pckPrepare();

    if (sendto(socketFD, pingPckBuffer.data(), pingPckBuffer.size(), 0, (struct sockaddr*) &addr, sizeof(addr)) <= 0)
    {
        ObjectError("Packet Sending Failed");
        stop();
        return;
    }

    pongElapsedTime.start();

    struct sockaddr_in r_addr;
    socklen_t addr_len = sizeof(r_addr);

    char pkg[pingPckBuffer.size()];

    if (recvfrom(socketFD,
                 pkg,
                 pingPckBuffer.size(),
                 0,
                 (struct sockaddr*) &r_addr,
                 &addr_len) <= 0)
    {
        lastElapsedTime = -1;
        ObjectError("Packet receive failed");
    }

    else
    {
        receivedCount++;
        lastElapsedTime = pongElapsedTime.stop();
    }

    SkVariantVector p;
    p << static_cast<int>(lastElapsedTime*1000);
    pulse(this, p);
}

void SkPinger::onStop()
{
    lastElapsedTime = 0;
    sentCount = 0;
    receivedCount = 0;

    if (socketFD == -1)
        return;

    close();
    pingPckBuffer.clear();
}

void SkPinger::close()
{
    ::close(socketFD);
    socketFD = -1;
}

uint64_t SkPinger::pckSize()
{
    return pingPckBuffer.size();
}

CStr *SkPinger::hostTarget()
{
    return pingTargetHost.c_str();
}

CStr *SkPinger::hostIP()
{
    return lookupedIpAddress.c_str();
}

CStr *SkPinger::hostTargetReversed()
{
    return reversedHostName.c_str();
}

bool SkPinger::hasReverseLookup()
{
    return hasReverse;
}

uint SkPinger::sent()
{
    return sentCount;
}

uint SkPinger::received()
{
    return receivedCount;
}

int SkPinger::ttl()
{
    return ttlVal;
}

double SkPinger::rtt()
{
    return lastElapsedTime*1000.;
}


