#if defined(ENABLE_REDIS)

#include "skredisasync.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkRedisASync, SkAbstractRedisClient)
{
    setObjectName("SkASyncRedis");

    maxStoredReplies = 1000;

    latencyMS = 1;

    SlotSet(fastTick);
    SignalSet(newReply);
}

void SkRedisASync::onConnection()
{
    Int l = 100;//eventLoop()->getFastInterval();
    struct timeval tv = {0, l};
    redisSetTimeout(ctx, tv);

    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkDirect);
    ObjectDebug("ASync-Connection ENABLED");
}

void SkRedisASync::setLatencyInterval(ulong millis)
{
    latencyMS = millis;
}

void SkRedisASync::onDisconnection()
{
    Detach(eventLoop()->fastZone_SIG, pulse, this, fastTick);
    ObjectWarning("ASync-Connection DISABLED");
}

bool SkRedisASync::sendCommand(CStr *commandLine)
{
    if (!isConnected())
    {
        ObjectError("Client is NOT connected to Redis yet");
        return false;
    }

    if (SkString::isEmpty(commandLine))
        return false;

    SkString tempCmd;
    SkStringList tempCmdArgs;

    if (!parseCommand(commandLine, tempCmd, tempCmdArgs))
        return false;

    return sendCommand(tempCmd.c_str(), tempCmdArgs);
}

bool SkRedisASync::sendCommand(CStr *command, SkStringList &args)
{
    SkString cmd(command);
    cmd.toUpperCase();

    lastCmd = cmd;
    lastCmdArgs = args;

    cmd.append(" ");
    cmd.append(args.join(" "));

    lastCmdLine = cmd;

    ObjectDebug("Sending commandLine: " << lastCmdLine);

    redisAppendCommand(ctx, lastCmdLine.c_str());
    return true;
}

uint64_t SkRedisASync::repliesCount()
{
    return replies.count();
}

bool SkRedisASync::getReply(SkRedisASyncReply &reply)
{
    if (replies.isEmpty())
        return false;

    SkRedisASyncReply *storedReply = replies.dequeue();
    reply = *storedReply;
    delete storedReply;
    return true;
}

CStr *SkRedisASync::getLastCommand()
{
    return lastCmd.c_str();
}

SkStringList &SkRedisASync::getLastCommandArgs()
{
    return lastCmdArgs;
}

CStr *SkRedisASync::getLastCommandLine()
{
    return lastCmdLine.c_str();
}

SlotImpl(SkRedisASync, fastTick)
{
    SilentSlotArgsWarning();

    if (latencyChrono.stop() >= latencyMS)
    {
        if (lastCmd.isEmpty())
        {
            latencyChrono.start();
            return;
        }

        redisReply *reply = nullptr;
        int r = redisGetReply(ctx, reinterpret_cast<void **>(&reply));

        if (r == REDIS_ERR)
        {
            if (ctx->err == REDIS_ERR_IO && errno == EAGAIN)
            {
                ctx->err = 0; // stop redisBufferRead from returning early
                latencyChrono.start();
                return;
            }
        }

        if (r != REDIS_OK || ctx->err)
        {
            ObjectError("Gotten an error from Redis-reply: " << ctx->errstr);
            latencyChrono.start();
            return;
        }

        SkRedisASyncReply *ar = new SkRedisASyncReply;

        if (checkReply(ctx, reply, &ar->value))
        {
            replies.enqueue(ar);

            ObjectDebug("Added new Async-reply [" << replyTypeName(reply->type) << "]");

            if (replies.count() > maxStoredReplies)
                delete replies.dequeue();

            newReply();
        }

        else
            delete ar;

        freeReplyObject(reply);

        latencyChrono.start();
    }
}

#endif
