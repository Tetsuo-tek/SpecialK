#if defined(ENABLE_REDIS)

#include "skredismonitor.h"

ConstructorImpl(SkRedisMonitor, SkRedisASync)
{

}

bool SkRedisMonitor::monitor(CStr *serverHostname, uint16_t serverPort)
{
    if (!connect(serverHostname, serverPort))
        return false;

    return sendCommand("MONITOR");
}

#endif
