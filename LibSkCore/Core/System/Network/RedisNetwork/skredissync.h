#ifndef SKREDISSYNC_H
#define SKREDISSYNC_H

#if defined(ENABLE_REDIS)

#include "skabstractredisclient.h"

class SkRedisSync : public SkAbstractRedisClient
{
    public:
        Constructor(SkRedisSync, SkAbstractRedisClient);

        bool sendCommand(CStr *commandLine, SkVariant *replyContent=nullptr);
        bool sendCommand(CStr *command, SkStringList &args, SkVariant *replyContent=nullptr);

        SkString info();
        bool ping(CStr *strVal=nullptr);
        bool flushAll();
        bool quit();

        bool setString(CStr *key, CStr *strVal);
        bool setInteger(CStr *key, int64_t intVal);
        bool setDouble(CStr *key, double dblVal);
        bool setBinary(CStr *key, void *data, int len);

        bool setStringOnlyIfExists(CStr *key, CStr *strVal);
        bool setIntegerOnlyIfExists(CStr *key, int64_t intVal);
        bool setDoubleOnlyIfExists(CStr *key, double intVal);
        bool setBinaryOnlyIfExists(CStr *key, void *data, int len);

        bool setStringOnlyIfNotExists(CStr *key, CStr *strVal);
        bool setIntegerOnlyIfNotExists(CStr *key, int64_t intVal);
        bool setDoubleOnlyIfNotExists(CStr *key, double intVal);
        bool setBinaryOnlyIfNotExists(CStr *key, void *data, int len);

        bool setStringWithExpireSEC(CStr *key, CStr *strVal, int seconds);
        bool setIntegerWithExpireSEC(CStr *key, int64_t intVal, int seconds);
        bool setDoubleWithExpireSEC(CStr *key, double dblVal, int seconds);
        bool setBinaryWithExpireSEC(CStr *key, void *data, int len, int seconds);

        bool setStringWithExpireMS(CStr *key, CStr *strVal, int milliseconds);
        bool setIntegerWithExpireMS(CStr *key, int64_t intVal, int milliseconds);
        bool setDoubleWithExpireMS(CStr *key, double dblVal, int milliseconds);
        bool setBinaryWithExpireMS(CStr *key, void *data, int len, int milliseconds);

        bool multipleSet(SkArgsMap &settingData);
        bool multipleSetIfExists(SkArgsMap &settingData);
        bool multipleSetIfNotExists(SkArgsMap &settingData);
        bool multipleGet(SkStringList &keys, SkArgsMap &dataGotten);

        bool append(CStr *key, CStr *strVal);
        bool append(CStr *key, void *data, int len);

        bool replace(CStr *key, int from, CStr *replaceStrVal);
        bool replace(CStr *key, int from, void *data, int len);

        bool setMap(CStr *key, SkArgsMap &m);
        bool delMapField(CStr *key, CStr *fieldName);
        bool delMapFields(CStr *key, SkStringList &fieldNames);
        bool existsMapField(CStr *key, CStr *fieldName);
        bool getMap(CStr *key, SkArgsMap &m);
        bool getMapFieldNames(CStr *key, SkStringList &fieldNames);
        bool getMapFieldValue(CStr *key, CStr *fieldName, SkVariant &value);
        bool getMapFieldValues(CStr *key, SkStringList &fieldNames, SkVariantList &values);
        bool incrementMapFieldBy(CStr *key, CStr *fieldName, int64_t incVal);
        bool incrementMapFieldBy(CStr *key, CStr *fieldName, double incVal);
        int64_t mapCount(CStr *key);

        SkString getString(CStr *key);
        int64_t getStringLength(CStr *key);

        /* - The offset value for the 1st character in the string is 0 (and not 1).
         * - The last value in the GETRANGE is not the total number of characters to extract.
         *   It is the offset value (the location) of the last character (until which) that you want extract.
         * - You can also give negative value for the offset, which will be calculated from
         *   the last position. For example, offset of -1 indicates the last character,
         *   -2 indicates the 2nd character from last, etc.
         * - The 1st character position is offset 0, and the last character position represented
         *   by negative offset value is -1. So, the following example will start from 1st position
         *   until the last position, giving us the whole string back.
         * - If the last offset (end offset) is way out of range
         *   (i.e greater than the total number of characters in the string),
         *   it will get until the end of the string. In the following example,
         *   offset value of 25 is greater than the total number of characters in the string.
         *   So, it started from offset 7 and extracted the values until the end of the string.*/
        SkString getSubString(CStr *key, int from, int offset);

        int64_t getInteger(CStr *key);
        double getDouble(CStr *key);
        CVoid *getBinary(CStr *key, int &len);

        SkString getSetString(CStr *key, CStr *newStrVal);
        int64_t getSetInteger(CStr *key, int newIntVal);
        double getSetDouble(CStr *key, double newDblVal);
        CVoid *getSetBinary(CStr *key, int &len, void *newData, int newLen);

        bool increment(CStr *key);
        bool incrementBy(CStr *key, int64_t incVal);
        bool incrementBy(CStr *key, double incVal);

        bool decrement(CStr *key);
        bool decrementBy(CStr *key, int64_t decVal);
        bool decrementBy(CStr *key, double decVal);

        bool keys(CStr *text, SkStringList &keysList);
        bool existsKey(CStr *key);
        bool deleteKey(CStr *key);
        bool deleteKeys(SkStringList &keys);

        bool publishString(CStr *topic, CStr *strVal);
        bool publishInteger(CStr *topic, int64_t intVal);
        bool publishDouble(CStr *topic, double dblVal);
        bool publishBinary(CStr *topic, void *data, int len);

        CStr *getLastCommand();
        SkStringList &getLastCommandArgs();
        CStr *getLastCommandLine();

    private:
        redisReply *reply;

        SkString lastCmd;
        SkStringList lastCmdArgs;
        SkString lastCmdLine;

        void onConnection();
        void onDisconnection();
};

#endif

#endif // SKREDISSYNC_H
