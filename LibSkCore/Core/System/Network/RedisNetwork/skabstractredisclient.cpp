#if defined(ENABLE_REDIS)

#include "skabstractredisclient.h"

AbstractConstructorImpl(SkAbstractRedisClient, SkObject)
{
    ctx = nullptr;
    port = 0;
}

bool SkAbstractRedisClient::connect(CStr *serverHostname, uint16_t serverPort)
{
    if (isConnected())
    {
        ObjectError("Already connected to Redis");
        return false;
    }

    hostname = serverHostname;
    port = serverPort;

    ObjectDebug("Using libhiredis-" << HIREDIS_MAJOR << "." << HIREDIS_MINOR << "." << HIREDIS_PATCH);

    struct timeval tv = {10, 0};
    ctx = redisConnectWithTimeout(hostname.c_str(), port, tv);

    if (ctx)
        ObjectMessage("Connection to Redis-server is ENABLED");

    else
    {
        ObjectError("Cannot connect to Redis");
        return false;
    }

    onConnection();
    return true;
}

void SkAbstractRedisClient::disconnect()
{
    if (!isConnected())
    {
        ObjectError("Client is NOT connected to Redis yet");
        return;
    }

    if (ctx)
    {
        redisFree(ctx);
        ctx = nullptr;
    }

    ObjectMessage("Connection to Redis-server is DISABLED");

    onDisconnection();
}

bool SkAbstractRedisClient::parseCommand(CStr *commandLine, SkString &cmd, SkStringList &args)
{
    cmd = commandLine;

    if (cmd.isEmpty())
        return false;

    args.clear();
    cmd.split(' ', args);
    args[0].toUpperCase();
    cmd = args[0];
    args.removeFirst();

    return true;
}

CStr *SkAbstractRedisClient::replyTypeName(int type)
{
    if (type == REDIS_REPLY_STRING)
        return "STRING";

    else if (type == REDIS_REPLY_ARRAY)
        return "STRING";

    else if (type == REDIS_REPLY_INTEGER)
        return "STRING";

    else if (type == REDIS_REPLY_NIL)
        return "NIL";

    else if (type == REDIS_REPLY_STATUS)
        return "STATUS";

    else if (type == REDIS_REPLY_ERROR)
        return "ERROR";

    else if (type == REDIS_REPLY_DOUBLE)
        return "DOUBLE";

    else if (type == REDIS_REPLY_BOOL)
        return "BOOL";

    else if (type == REDIS_REPLY_MAP)
        return "MAP";

    else if (type == REDIS_REPLY_SET)
        return "SET";

    else if (type == REDIS_REPLY_ATTR)
        return "ATTR";

    else if (type == REDIS_REPLY_PUSH)
        return "PUSH";

    else if (type == REDIS_REPLY_BIGNUM)
        return "BIGNUM";

    else if (type == REDIS_REPLY_VERB)
        return "VERB";

    return "UNKNOWN";
}

bool SkAbstractRedisClient::checkReply(redisContext *ctx,
                                       redisReply *reply,
                                       SkVariant *replyContent,
                                       uint recursion,
                                       uint64_t id)
{

    if (ctx->err)
    {
        ObjectError(ctx->errstr);

        if (replyContent)
            replyContent->nullify();

        return false;
    }

    if (reply)
    {
        /* A bulk (string) reply. The value of the reply can be accessed using reply->str.
         * The length of this string can be accessed using reply->len.*/

        if (reply->type == REDIS_REPLY_STRING)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << reply->str);

            if (replyContent)
                replyContent->setVal(reply->str);
        }

        /* The command replied with an integer. The integer value can be accessed
         * using the reply->integer field of type long long.*/

        else if (reply->type == REDIS_REPLY_INTEGER)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << reply->integer);

            if (replyContent)
                replyContent->setVal(static_cast<int64_t>(reply->integer));
        }

        /* The command replied with a double-precision floating point number.
         * The value is stored as a string in the str member, and can be converted
         * with strtod or similar.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_DOUBLE)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << reply->dval);

            if (replyContent)
                replyContent->setVal(reply->dval);
        }

        /* A boolean true/false reply. The value is stored in the integer member
         * and will be either 0 or 1.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_BOOL)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << SkVariant::boolToString(reply->integer));

            if (replyContent)
                replyContent->setVal(static_cast<bool>(reply->integer));
        }

        /* A multi bulk reply. The number of elements in the multi bulk reply is
         * stored in reply->elements. Every element in the multi bulk reply is
         * a redisReply object as well and can be accessed via reply->element[..index..].
         * Redis may reply with nested arrays but this is fully supported.*/

        else if (reply->type == REDIS_REPLY_ARRAY)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << "[" << reply->elements << "])");

            if (replyContent)
            {
                SkList<SkVariant> l;

                /*if (strcmp(reply->element[0]->str, "subscribe") != 0)
                    ObjectMessage("Message received -> " << reply->element[2]->str << "(on channel : " << reply->element[1]->str << ")")

                else*/
                {
                    for(uint64_t i=0; i<reply->elements; i++)
                    {
                        SkVariant elem;

                        if (!checkReply(ctx, reply->element[i], &elem, recursion+1, i))
                            return false;

                        l << elem;
                    }
                }

                replyContent->setVal(l);
            }

            else
            {
                for(uint64_t i=0; i<reply->elements; i++)
                    if (!checkReply(ctx, reply->element[i], nullptr, recursion+1, i))
                        return false;
            }
        }

        /* An array with the added invariant that there will always be an even
         * number of elements. The MAP is functionally equivalent to REDIS_REPLY_ARRAY
         * except for the previously mentioned invariant.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_MAP)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") "/* << reply->str*/);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /* An array response where each entry is unique. Like the MAP type,
         * the data is identical to an array response except there are no duplicate values.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_SET)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") "/* << reply->str*/);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /* An array structurally identical to a MAP but intended as meta-data
         * about a reply. As of Redis 6.0.6 this reply type is not used in Redis.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_ATTR)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") "/* << reply->str*/);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /* An array that can be generated spontaneously by Redis.
         * This array response will always contain at least two subelements.
         * The first contains the type of PUSH message (e.g. message, or invalidate),
         * and the second being a sub-array with the PUSH payload itself.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_PUSH)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") "/* << reply->str*/);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /* A string representing an arbitrarily large signed or unsigned integer value.
         * The number will be encoded as a string in the str member of redisReply.
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_BIGNUM)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << reply->str);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /*A verbatim string, intended to be presented to the user without modification.
         * The string payload is stored in the str member, and type data is stored
         * in the vtype member (e.g. txt for raw text or md for markdown).
         * Is RESP3 */

        else if (reply->type == REDIS_REPLY_VERB)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << "[" << reply->vtype <<"]) " << reply->str);

            /*if (replyContent)
                replyContent->setVal(reply->str);*/
        }

        /* The command replied with a nil object. There is no data to access.*/

        else if (reply->type == REDIS_REPLY_NIL)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ")");

            if (replyContent)
                replyContent->nullify();
        }

        /* The command replied with a status reply. The status string can be accessed using reply->str.
         * The length of this string can be accessed using reply->len.*/

        else if (reply->type == REDIS_REPLY_STATUS)
        {
            ObjectPlusDebug("Recursion: " << recursion << "; id: " << id << " -> (" << replyTypeName(reply->type) << ") " << reply->str);

            if (replyContent)
                replyContent->setVal(reply->str);
        }

        /* The command replied with an error. The error string can be accessed
         * identical to REDIS_REPLY_STATUS.*/

        else if (reply->type == REDIS_REPLY_ERROR)
        {
            ObjectError(reply->str);
            return false;
        }
    }

    else
    {
        ObjectError("No response");
        return false;
    }

   return true;
}

bool SkAbstractRedisClient::isConnected()
{
    return (ctx != nullptr);
}

CStr *SkAbstractRedisClient::getHostname()
{
    return hostname.c_str();
}

int SkAbstractRedisClient::getPort()
{
    return port;
}

#endif
