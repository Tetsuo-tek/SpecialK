#ifndef SKREDISSUBSCRIBER_H
#define SKREDISSUBSCRIBER_H

#if defined(ENABLE_REDIS)

#include "skredisasync.h"

class SkRedisSubscriber extends SkRedisASync
{
    public:
        Constructor(SkRedisSubscriber, SkRedisASync);

        //AFTER MONITOR COMMAND THE CONTEXT WILL ALLOW ONLY FOLLOWING COMMANDs:
        // * SUBSCRIBE/UNSUBSCRIBE, PING, QUIT

        bool subscribe(CStr *topic);
        bool unsubscribe(CStr *topic);
};

#endif

#endif // SKREDISSUBSCRIBER_H
