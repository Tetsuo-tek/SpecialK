#if defined(ENABLE_REDIS)

#include "skredissync.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkRedisSync, SkAbstractRedisClient)
{
    setObjectName("SkSyncRedis");

    ctx = nullptr;
    reply = nullptr;
}

void SkRedisSync::onConnection()
{
    struct timeval tv = {1, 0};
    redisSetTimeout(ctx, tv);

    ObjectDebug("Sync-Connection ENABLED");
}

void SkRedisSync::onDisconnection()
{
    ObjectWarning("Sync-Connection DISABLED");
}

bool SkRedisSync::sendCommand(CStr *commandLine, SkVariant *replyContent)
{
    if (!isConnected())
    {
        ObjectError("Client is NOT connected to Redis yet");
        return false;
    }

    if (SkString::isEmpty(commandLine))
        return false;

    SkString tempCmd;
    SkStringList tempCmdArgs;

    if (!parseCommand(commandLine, tempCmd, tempCmdArgs))
        return false;

    return sendCommand(tempCmd.c_str(), tempCmdArgs, replyContent);
}

bool SkRedisSync::sendCommand(CStr *command, SkStringList &args, SkVariant *replyContent)
{
    if (!isConnected())
    {
;        return false;
    }

    SkString cmd(command);
    cmd.toUpperCase();

    if (cmd == "MONITOR" || cmd == "SUBSCRIBE")
    {
        ObjectError("Use SkASyncRedis to perform aynchronous commands");
        return false;
    }

    lastCmd = cmd;
    lastCmdArgs = args;

    cmd.append(" ");
    cmd.append(args.join(" "));

    lastCmdLine = cmd;

    ObjectDebug("Sending commandLine: " << lastCmdLine);

    reply = static_cast<redisReply *>(redisCommand(ctx, cmd.c_str()));
    bool ok = checkReply(ctx, reply, replyContent);
    freeReplyObject(reply);
    reply = nullptr;
    return ok;
}

SkString SkRedisSync::info()
{
    if (!isConnected())
    {
        ObjectError("Client is NOT connected to Redis yet");
        return "";
    }

    SkVariant info;

    if (sendCommand("INFO", &info))
        return info.data();

    return "";
}

#define rediscmd(VALPOINTER, ...) \
    bool ok = isConnected(); \
    if (!ok) \
    {ObjectError("Client is NOT connected to Redis yet");} \
    else \
    { \
        reply = static_cast<redisReply *>(redisCommand(ctx, __VA_ARGS__)); \
        ok = checkReply(ctx, reply, VALPOINTER); \
        if (reply) \
        { \
            freeReplyObject(reply); \
            reply = nullptr; \
        } \
    } \
    (void) ok

bool SkRedisSync::ping(CStr *strVal)
{
    if (strVal)
    {
        rediscmd(nullptr, "PING %s", strVal);
        return ok;
    }

    rediscmd(nullptr, "PING");
    return ok;
}

bool SkRedisSync::flushAll()
{
    rediscmd(nullptr, "FLUSHALL");
    return ok;
}

bool SkRedisSync::quit()
{
    rediscmd(nullptr, "QUIT");
    return ok;
}

bool SkRedisSync::setString(CStr *key, CStr *strVal)
{
    rediscmd(nullptr, "SET %s %s", key, strVal);
    return ok;
}

bool SkRedisSync::setInteger(CStr *key, int64_t intVal)
{
    rediscmd(nullptr, "SET %s %lld", key, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::setDouble(CStr *key, double dblVal)
{
    rediscmd(nullptr, "SET %s %f", key, dblVal);
    return ok;
}

bool SkRedisSync::setBinary(CStr *key, void *data, int len)
{
    rediscmd(nullptr, "SET %s %b", key, data, len);
    return ok;
}

bool SkRedisSync::setStringOnlyIfExists(CStr *key, CStr *strVal)
{
    rediscmd(nullptr, "SETXX %s %s", key, strVal);
    return ok;
}

bool SkRedisSync::setIntegerOnlyIfExists(CStr *key, int64_t intVal)
{
    rediscmd(nullptr, "SETXX %s %lld", key, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::setDoubleOnlyIfExists(CStr *key, double dblVal)
{
    rediscmd(nullptr, "SETXX %s %f", key, dblVal);
    return ok;
}

bool SkRedisSync::setBinaryOnlyIfExists(CStr *key, void *data, int len)
{
    rediscmd(nullptr, "SETXX %s %b", key, data, len);
    return ok;
}

bool SkRedisSync::setStringOnlyIfNotExists(CStr *key, CStr *strVal)
{
    rediscmd(nullptr, "SETNX %s %s", key, strVal);
    return ok;
}

bool SkRedisSync::setIntegerOnlyIfNotExists(CStr *key, int64_t intVal)
{
    rediscmd(nullptr, "SETNX %s %lld", key, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::setDoubleOnlyIfNotExists(CStr *key, double dblVal)
{
    rediscmd(nullptr, "SETNX %s %f", key, dblVal);
    return ok;
}

bool SkRedisSync::setBinaryOnlyIfNotExists(CStr *key, void *data, int len)
{
    rediscmd(nullptr, "SETNX %s %b", key, data, len);
    return ok;
}

bool SkRedisSync::setStringWithExpireSEC(CStr *key, CStr *strVal, int seconds)
{
    rediscmd(nullptr, "SETEX %s %d %s", key, seconds, strVal);
    return ok;
}

bool SkRedisSync::setIntegerWithExpireSEC(CStr *key, int64_t intVal, int seconds)
{
    rediscmd(nullptr, "SETEX %s %d %lld", key, seconds, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::setDoubleWithExpireSEC(CStr *key, double dblVal, int seconds)
{
    rediscmd(nullptr, "SETEX %s %d %f", key, seconds, dblVal);
    return ok;
}

bool SkRedisSync::setBinaryWithExpireSEC(CStr *key, void *data, int len, int seconds)
{
    rediscmd(nullptr, "SETEX %s %d %b", key, seconds, data, len);
    return ok;
}

bool SkRedisSync::setStringWithExpireMS(CStr *key, CStr *strVal, int milliseconds)
{
    rediscmd(nullptr, "PSETEX %s %d %s", key, milliseconds, strVal);
    return ok;
}

bool SkRedisSync::setIntegerWithExpireMS(CStr *key, int64_t intVal, int milliseconds)
{
    rediscmd(nullptr, "PSETEX %s %d %lld", key, milliseconds, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::setDoubleWithExpireMS(CStr *key, double dblVal, int milliseconds)
{
    rediscmd(nullptr, "PSETEX %s %d %f", key, milliseconds, dblVal);
    return ok;
}

bool SkRedisSync::setBinaryWithExpireMS(CStr *key, void *data, int len, int milliseconds)
{
    rediscmd(nullptr, "PSETEX %s %d %b", key, milliseconds, data, len);
    return ok;
}

bool SkRedisSync::multipleSet(SkArgsMap &settingData)
{
    SkString str;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = settingData.iterator();

    while(itr->next())
    {
        str.append(itr->item().key());
        str.append(" ");

        if (itr->item().value().isString())
            str.append(itr->item().value().data());

        else if (itr->item().value().isNumber())
        {
            if (itr->item().value().isBoolean() || itr->item().value().isInteger())
                str.concat(itr->item().value().toInt());

            else if (itr->item().value().isReal())
                str.concat(itr->item().value().toDouble());
        }

        if (itr->hasNext())
            str.append(" ");
    }

    delete itr;

    rediscmd(nullptr, "MSET %s", str.c_str());
    return ok;
}

bool SkRedisSync::multipleSetIfExists(SkArgsMap &settingData)
{
    //rediscmd(nullptr, "MGET %s", str);
    return false;
}

bool SkRedisSync::multipleSetIfNotExists(SkArgsMap &settingData)
{
    //rediscmd(nullptr, "MGET %s", str);
    return false;
}

bool SkRedisSync::multipleGet(SkStringList &keys, SkArgsMap &dataGotten)
{
    //rediscmd(nullptr, "MGET %s", str);
    return false;
}

bool SkRedisSync::append(CStr *key, CStr *strVal)
{
    rediscmd(nullptr, "APPEND %s %s", key, strVal);
    return ok;
}

bool SkRedisSync::append(CStr *key, void *data, int len)
{
    rediscmd(nullptr, "APPEND %s %b", key, data, len);
    return ok;
}

bool SkRedisSync::replace(CStr *key, int from, CStr *replaceStrVal)
{
    rediscmd(nullptr, "SETRANGE %s %d %s", key, from, replaceStrVal);
    return ok;
}

bool SkRedisSync::replace(CStr *key, int from, void *data, int len)
{
    rediscmd(nullptr, "SETRANGE %s %d %b", key, from, data, len);
    return ok;
}

bool SkRedisSync::setMap(CStr *key, SkArgsMap &m)
{
    if (!isConnected())
        return false;

    if (m.isEmpty())
        return false;

    SkString cmd("HSET ");
    cmd.append(key);
    cmd.append(" ");

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        cmd.append(itr->item().key());
        cmd.append(" ");

        if (itr->item().value().isString())
            cmd.append(itr->item().value().data());

        else
            cmd.append(itr->item().value().toString());

        if (itr->hasNext())
            cmd.append(" ");
    }

    delete itr;

    rediscmd(nullptr, cmd.c_str());
    return ok;
}

bool SkRedisSync::delMapField(CStr *key, CStr *fieldName)
{
    rediscmd(nullptr, "HDEL %s %s", key, fieldName);
    return ok;
}

bool SkRedisSync::delMapFields(CStr *key, SkStringList &fieldNames)
{
    SkString args = fieldNames.join(" ");
    rediscmd(nullptr, "HDEL %s %s", key, args.c_str());
    return ok;
}

bool SkRedisSync::existsMapField(CStr *key, CStr *fieldName)
{
    SkVariant v;
    rediscmd(&v, "HEXISTS %s %s", key, fieldName);

    if (!ok)
        return false;

    return v.toBool();
}

bool SkRedisSync::getMap(CStr *key, SkArgsMap &m)
{
    SkVariant v;
    rediscmd(&v, "HGETALL %s", key);

    if (!ok)
        return false;

    SkVariantList l;
    v.copyToList(l);

    SkAbstractListIterator<SkVariant> *itr = l.iterator();

    while(itr->next())
    {
        SkString k(itr->item().data());
        itr->next();
        m[k] = itr->item();
    }

    delete itr;
    return true;
}

bool SkRedisSync::getMapFieldNames(CStr *key, SkStringList &fieldNames)
{
    SkVariant v;
    rediscmd(&v, "HKEYS %s", key);
    v.copyToStringList(fieldNames);
    return ok;
}

bool SkRedisSync::getMapFieldValue(CStr *key, CStr *fieldName, SkVariant &value)
{
    rediscmd(&value, "HGET %s %s", key, fieldName);
    return ok;
}

bool SkRedisSync::getMapFieldValues(CStr *key, SkStringList &fieldNames, SkVariantList &values)
{
    SkVariant v;

    if (fieldNames.isEmpty())
    {
        rediscmd(&v, "HVALS %s", key);
        v.copyToList(values);
        return ok;
    }

    SkString args = fieldNames.join(" ");
    rediscmd(&v, "HMGET %s %s", key, args.c_str());
    v.copyToList(values);
    return ok;
}

bool SkRedisSync::incrementMapFieldBy(CStr *key, CStr *fieldName, int64_t incVal)
{
    rediscmd(nullptr, "HINCRBY %s %s %lld", key, fieldName, incVal);
    return ok;
}

bool SkRedisSync::incrementMapFieldBy(CStr *key, CStr *fieldName, double incVal)
{
    rediscmd(nullptr, "HINCRBYFLOAT %s %s %f", key, fieldName, incVal);
    return ok;
}

int64_t SkRedisSync::mapCount(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "HLEN %s", key);
    return v.toInt64();
}

SkString SkRedisSync::getString(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "GET %s", key);
    return v.toString();
}

int64_t SkRedisSync::getStringLength(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "STRLEN %s", key);
    return v.toInt64();
}

SkString SkRedisSync::getSubString(CStr *key, int from, int offset)
{
    SkVariant v;
    rediscmd(&v, "GETRANGE %s %d %d", key, from, offset);
    return v.toString();
}

int64_t SkRedisSync::getInteger(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "GET %s", key);
    return v.toInt();
}

double SkRedisSync::getDouble(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "GET %s", key);
    return v.toDouble();
}

CVoid *SkRedisSync::getBinary(CStr *key, int &len)
{
    SkVariant v;
    rediscmd(&v, "GET %s", key);
    len = v.size();
    return v.data();
}

SkString SkRedisSync::getSetString(CStr *key, CStr *newStrVal)
{
    SkVariant v;
    rediscmd(&v, "GETSET %s %s", key, newStrVal);
    return v.toString();
}

int64_t SkRedisSync::getSetInteger(CStr *key, int newIntVal)
{
    SkVariant v;
    rediscmd(&v, "GETSET %s %lld", key, static_cast<long long>(newIntVal));
    return v.toInt();
}

double SkRedisSync::getSetDouble(CStr *key, double newDblVal)
{
    SkVariant v;
    rediscmd(&v, "GETSET %s %f", key, newDblVal);
    return v.toDouble();
}

CVoid *SkRedisSync::getSetBinary(CStr *key, int &len, void *newData, int newLen)
{
    SkVariant v;
    rediscmd(&v, "GETSET %s %b", key, newData, newLen);
    len = v.size();
    return v.data();
}

bool SkRedisSync::increment(CStr *key)
{
    rediscmd(nullptr, "INCR %s", key);
    return ok;
}

bool SkRedisSync::incrementBy(CStr *key, int64_t incVal)
{
    rediscmd(nullptr, "INCRBY %s %lld", key, static_cast<long long>(incVal));
    return ok;
}

bool SkRedisSync::incrementBy(CStr *key, double incVal)
{
    rediscmd(nullptr, "INCRBYFLOAT %s %f", key, incVal);
    return ok;
}

bool SkRedisSync::decrement(CStr *key)
{
    rediscmd(nullptr, "DECR %s", key);
    return ok;
}

bool SkRedisSync::decrementBy(CStr *key, int64_t decVal)
{
    rediscmd(nullptr, "DECRBY %s %lld", key, static_cast<long long>(decVal));
    return ok;
}

bool SkRedisSync::decrementBy(CStr *key, double decVal)
{
    rediscmd(nullptr, "DECRBYFLOAT %s %f", key, decVal);
    return ok;
}

bool SkRedisSync::keys(CStr *text, SkStringList &keysList)
{
    SkVariant v;
    rediscmd(&v, "KEYS %s", text);
    v.copyToStringList(keysList);
    return ok;
}

bool SkRedisSync::existsKey(CStr *key)
{
    SkVariant v;
    rediscmd(&v, "EXISTS %s", key);
    return v.toBool();
}

bool SkRedisSync::deleteKey(CStr *key)
{
    rediscmd(nullptr, "DEL %s", key);
    return ok;
}

bool SkRedisSync::deleteKeys(SkStringList &keys)
{
    SkString str = keys.join(" ");
    rediscmd(nullptr, "DEL %s", str.c_str());
    return ok;
}

bool SkRedisSync::publishString(CStr *topic, CStr *strVal)
{
    rediscmd(nullptr, "PUBLISH %s %s", topic, strVal);
    return ok;
}

bool SkRedisSync::publishInteger(CStr *topic, int64_t intVal)
{
    rediscmd(nullptr, "PUBLISH %s %lld", topic, static_cast<long long>(intVal));
    return ok;
}

bool SkRedisSync::publishDouble(CStr *topic, double dblVal)
{
    rediscmd(nullptr, "PUBLISH %s %f", topic, dblVal);
    return ok;
}

bool SkRedisSync::publishBinary(CStr *topic, void *data, int len)
{
    rediscmd(nullptr, "PUBLISH %s %b", topic, data, len);
    return ok;
}

CStr *SkRedisSync::getLastCommand()
{
    return lastCmd.c_str();
}

SkStringList &SkRedisSync::getLastCommandArgs()
{
    return lastCmdArgs;
}

CStr *SkRedisSync::getLastCommandLine()
{
    return lastCmdLine.c_str();
}

#endif
