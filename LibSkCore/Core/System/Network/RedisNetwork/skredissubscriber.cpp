#if defined(ENABLE_REDIS)

#include "skredissubscriber.h"

ConstructorImpl(SkRedisSubscriber, SkRedisASync)
{

}

bool SkRedisSubscriber::subscribe(CStr *topic)
{
    SkString str("SUBSCRIBE ");
    str.append(topic);
    return sendCommand(str.c_str());
}

bool SkRedisSubscriber::unsubscribe(CStr *topic)
{
    SkString str("UNSUBSCRIBE ");
    str.append(topic);
    return sendCommand(str.c_str());
}

#endif
