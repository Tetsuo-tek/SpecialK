#ifndef SKABSTRACTREDISCLIENT_H
#define SKABSTRACTREDISCLIENT_H

#if defined(ENABLE_REDIS)

#include <hiredis/hiredis.h>
#include <Core/Object/skobject.h>

class SkAbstractRedisClient extends SkObject
{
    public:
        bool connect(CStr *serverHostname="127.0.0.1", uint16_t serverPort=6379);
        void disconnect();
        bool isConnected();

        CStr *getHostname();
        int getPort();

        static bool parseCommand(CStr *commandLine, SkString &cmd, SkStringList &args);

    protected:
        AbstractConstructor(SkAbstractRedisClient, SkObject);

        virtual void onConnection(){}
        virtual void onDisconnection(){}

        redisContext *ctx;

        CStr *replyTypeName(int type);
        bool checkReply(redisContext *ctx,
                        redisReply *reply,
                        SkVariant *content,
                        uint recursion=0,
                        uint64_t id=0);

    private:
        SkString hostname;
        int port;
};

#endif

#endif // SKABSTRACTREDISCLIENT_H
