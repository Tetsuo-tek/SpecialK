#ifndef SKREDISMONITOR_H
#define SKREDISMONITOR_H

#if defined(ENABLE_REDIS)

#include "skredisasync.h"

class SkRedisMonitor extends SkRedisASync
{
    public:
        Constructor(SkRedisMonitor, SkRedisASync);

        //AFTER MONITOR COMMAND THE CONTEXT WILL NOT ALLOW OTHER COMMANDs

        bool monitor(CStr *serverHostname="127.0.0.1", uint16_t serverPort=6379);
};

#endif

#endif // SKREDISMONITOR_H
