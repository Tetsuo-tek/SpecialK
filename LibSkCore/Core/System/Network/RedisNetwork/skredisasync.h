#ifndef SKREDISASYNC_H
#define SKREDISASYNC_H

#if defined(ENABLE_REDIS)

#include "skabstractredisclient.h"
#include "Core/System/Time/skelapsedtime.h"

typedef struct
{
    /*SkString lastCmd;
    SkString lastCmdLine;*/
    SkVariant value;
} SkRedisASyncReply;

class SkRedisASync extends SkAbstractRedisClient
{
    public:
        Constructor(SkRedisASync, SkAbstractRedisClient);

        void setLatencyInterval(ulong millis);

        bool sendCommand(CStr *commandLine);
        bool sendCommand(CStr *command, SkStringList &args);

        CStr *getLastCommand();
        SkStringList &getLastCommandArgs();
        CStr *getLastCommandLine();

        uint64_t repliesCount();
        bool getReply(SkRedisASyncReply &reply);

        Slot(fastTick);
        Signal(newReply);

    protected:

    private:
        SkString lastCmd;
        SkStringList lastCmdArgs;
        SkString lastCmdLine;

        uint64_t maxStoredReplies;

        SkQueue<SkRedisASyncReply *> replies;

        ulong latencyMS;
        SkElapsedTimeMillis latencyChrono;

        void onConnection();
        void onDisconnection();
};

#endif

#endif // SKREDISASYNC_H
