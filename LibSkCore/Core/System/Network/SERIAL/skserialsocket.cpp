#include "skserialsocket.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <termios.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkSerialMode)
{
    SetupEnumWrapper(SkSerialMode);

    SetEnumItem(SkSerialMode::Mode5N1);
    SetEnumItem(SkSerialMode::Mode6N1);
    SetEnumItem(SkSerialMode::Mode7N1);
    SetEnumItem(SkSerialMode::Mode8N1);
    SetEnumItem(SkSerialMode::Mode5N2);
    SetEnumItem(SkSerialMode::Mode6N2);
    SetEnumItem(SkSerialMode::Mode7N2);
    SetEnumItem(SkSerialMode::Mode8N2);
    SetEnumItem(SkSerialMode::Mode5E1);
    SetEnumItem(SkSerialMode::Mode6E1);
    SetEnumItem(SkSerialMode::Mode7E1);
    SetEnumItem(SkSerialMode::Mode8E1);
    SetEnumItem(SkSerialMode::Mode5E2);
    SetEnumItem(SkSerialMode::Mode6E2);
    SetEnumItem(SkSerialMode::Mode7E2);
    SetEnumItem(SkSerialMode::Mode8E2);
    SetEnumItem(SkSerialMode::Mode5O1);
    SetEnumItem(SkSerialMode::Mode6O1);
    SetEnumItem(SkSerialMode::Mode7O1);
    SetEnumItem(SkSerialMode::Mode8O1);
    SetEnumItem(SkSerialMode::Mode5O2);
    SetEnumItem(SkSerialMode::Mode6O2);
    SetEnumItem(SkSerialMode::Mode7O2);
    SetEnumItem(SkSerialMode::Mode8O2);
}

DeclareWrapper_ENUM(SkSerialBaud)
{
    SetupEnumWrapper(SkSerialBaud);

    SetEnumItem(SkSerialBaud::SB_1200);
    SetEnumItem(SkSerialBaud::SB_2400);
    SetEnumItem(SkSerialBaud::SB_4800);
    SetEnumItem(SkSerialBaud::SB_9600);
    SetEnumItem(SkSerialBaud::SB_19200);
    SetEnumItem(SkSerialBaud::SB_38400);
    SetEnumItem(SkSerialBaud::SB_56000);
    SetEnumItem(SkSerialBaud::SB_57600);
    SetEnumItem(SkSerialBaud::SB_115200);
    SetEnumItem(SkSerialBaud::SB_128000);
    SetEnumItem(SkSerialBaud::SB_230400);
    SetEnumItem(SkSerialBaud::SB_250000);
    SetEnumItem(SkSerialBaud::SB_500000);
    SetEnumItem(SkSerialBaud::SB_1000000);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkSerialSocket, open, bool, Arg_CStr, Arg_Enum(SkSerialBaud), Arg_Enum(SkSerialMode))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkSerialSocket, open, 1, bool, Arg_CStr, Arg_Enum(SkSerialBaud), Arg_Enum(SkSerialMode), Arg_Bool)
//DeclareMeth_INSTANCE_RET(SkSerialSocket, canReadLine, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkSerialSocket, bytesAvailable, uint64_t)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkSerialSocket, SkAbstractSocket,
                {
                    AddEnumType(SkSerialMode);
                    AddEnumType(SkSerialBaud);

                    AddMeth_INSTANCE_RET(SkSerialSocket, open);
                    AddMeth_INSTANCE_RET_OVERLOAD(SkSerialSocket, open, 1);
                    //AddMeth_INSTANCE_RET(SkSerialSocket, canReadLine);
                    AddMeth_INSTANCE_RET(SkSerialSocket, bytesAvailable);
                })
{
    bauds = SkSerialBaud::SB_9600;
    modeAttributes = SkSerialMode::Mode8N1;
    swCtrl = false;
    devName = "NODEV";
}

bool SkSerialSocket::open(CStr *serialDevice,
                          SkSerialBaud baudRate,
                          SkSerialMode mode,
                          bool softwareControl)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    devName = serialDevice;
    bauds = baudRate;
    modeAttributes = mode;
    swCtrl = softwareControl;

    if (devName.isEmpty())
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__ ,
                    "Cannot open serial device with an EMPTY name");
        return false;
    }

    int socketFD = ::open(devName.data(), O_RDWR | O_NOCTTY | O_SYNC);
    bool ok = (socketFD > -1);

    if (ok)
    {
        setFileDescriptor(socketFD);
        setInterfaceAttribs();
        connected(this);
        return true;
    }

    SkString errMsg = "Cannot open serial device: ";
    errMsg.append(devName);

    notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, errMsg.c_str());
    return false;
}

bool SkSerialSocket::setInterfaceAttribs()
{
    struct termios tty;
    memset(&tty, 0, sizeof(tty));

    if (tcgetattr(socketFD, &tty) != 0)
    {
        ObjectError("CANNOT get term attributes [" << strerror(errno) << "]");
        return false;
    }

    tcflag_t bits = CS8;
    bool parity = false;
    bool parityOdd = false;
    unsigned char stopBits = 1;

    if (modeAttributes == SkSerialMode::Mode5E1)
        bits = CS5;

    else if (modeAttributes == SkSerialMode::Mode6N1)
        bits = CS6;

    else if (modeAttributes == SkSerialMode::Mode7N1)
        bits = CS7;

    else if (modeAttributes == SkSerialMode::Mode8N1)
        bits = CS8;

    else if (modeAttributes == SkSerialMode::Mode5N2)
    {
        bits = CS5;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode6N2)
    {
        bits = CS6;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode7N2)
    {
        bits = CS7;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode8N2)
    {
        bits = CS8;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode5E1)
    {
        bits = CS5;
        parity = true;
    }

    else if (modeAttributes == SkSerialMode::Mode6E1)
    {
        bits = CS6;
        parity = true;
    }

    else if (modeAttributes == SkSerialMode::Mode7E1)
    {
        bits = CS7;
        parity = true;
    }

    else if (modeAttributes == SkSerialMode::Mode8E1)
    {
        bits = CS8;
        parity = true;
    }

    else if (modeAttributes == SkSerialMode::Mode5E2)
    {
        bits = CS5;
        parity = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode6E2)
    {
        bits = CS6;
        parity = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode7E2)
    {
        bits = CS7;
        parity = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode8E2)
    {
        bits = CS8;
        parity = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode5O1)
    {
        bits = CS5;
        parity = true;
        parityOdd = true;
    }

    else if (modeAttributes == SkSerialMode::Mode6O1)
    {
        bits = CS6;
        parity = true;
        parityOdd = true;
    }

    else if (modeAttributes == SkSerialMode::Mode7O1)
    {
        bits = CS7;
        parity = true;
        parityOdd = true;
    }

    else if (modeAttributes == SkSerialMode::Mode8O1)
    {
        bits = CS8;
        parity = true;
        parityOdd = true;
    }

    else if (modeAttributes == SkSerialMode::Mode5O2)
    {
        bits = CS5;
        parity = true;
        parityOdd = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode6O2)
    {
        bits = CS6;
        parity = true;
        parityOdd = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode7O2)
    {
        bits = CS7;
        parity = true;
        parityOdd = true;
        stopBits = 2;
    }

    else if (modeAttributes == SkSerialMode::Mode8O2)
    {
        bits = CS8;
        parity = true;
        parityOdd = true;
        stopBits = 2;
    }

    cfsetospeed(&tty, bauds);
    cfsetispeed(&tty, bauds);

    //Mask the character size bits
    tty.c_cflag &= ~CSIZE;

    // Select  data bits
    tty.c_cflag |= bits;

    //Enable the receiver and set local mode
    //ignore modem controls, enable reading
    tty.c_cflag |= (CLOCAL | CREAD);

    if (parity)
    {
        tty.c_cflag |= PARENB;

        if (parityOdd)
            tty.c_cflag |= PARODD;

        else
            tty.c_cflag &= ~PARODD;
    }

    else
    {
        tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~PARODD;
    }

    //shut off parity
    //tty.c_cflag &= ~(PARENB | PARODD);
    //tty.c_cflag |= parity;

    if (stopBits == 1)
        tty.c_cflag &= ~CSTOPB;

    else if (stopBits == 2)
        tty.c_cflag |= CSTOPB;

    if (swCtrl)
        tty.c_cflag |= CRTSCTS;

    else
        tty.c_cflag &= ~CRTSCTS;

    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         //disable break processing
    tty.c_lflag = 0;                //no signaling chars, no echo,
                                    //no canonical processing
    tty.c_oflag = 0;                //no remapping, no delays
    tty.c_cc[VMIN]  = 0;            //read doesn't block
    tty.c_cc[VTIME] = 5;            //0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    if (tcsetattr(socketFD, TCSANOW, &tty) != 0)
    {
        ObjectError("CANNOT set term attributes [" << strerror(errno) << "]");
        return false;
    }

    return true;
}

uint64_t SkSerialSocket::bytesAvailable()
{
    return rx.size();
}

bool SkSerialSocket::canReadLine(uint64_t &lineLength)
{
    if (!isOpen())
    {
        lineLength = 0;
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t sz = bytesAvailable();

    if (sz)
    {
        char *d = new char [sz];

        if (rx.getCustomBuffer(d, sz, true))
        {
            for(uint64_t i=0; i<sz; i++)
                if (d[i] == '\n')
                {
                    lineLength = i+1;
                    delete [] d;
                    return true;
                }
        }

        delete [] d;
    }

    lineLength = 0;
    return false;
}

void SkSerialSocket::onUpdateBase()
{
    int64_t readSize = -1;

    int count;

    if (ioctl(socketFD, FIONREAD, &count) < 0)
    {
        disconnect();
        return;
    }

    uint64_t sz = static_cast<uint64_t>(count);

    if (sz == 0)
        return;

    char *d = new char [sz];
    readSize = ::read(socketFD, d, sz);

    if (readSize > 0)
    {
        rx.addData(d, readSize);
        readyRead(this);
    }

    else
    {
        sckValid = false;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
    }

    delete [] d;
}

bool SkSerialSocket::readFromSocket(char *data, uint64_t &len, bool onlyPeek)
{
    uint64_t tempSizeAvailable = bytesAvailable();

    if (tempSizeAvailable == 0)
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Cannot READ because it has NOT data");
        return false;
    }

    if (len == 0)
        len = tempSizeAvailable;

    bool ok = rx.getCustomBuffer(data, len, onlyPeek);

    if (ok)
        notifyRead(len);

    return ok;
}

bool SkSerialSocket::writeToSocket(CStr *data, uint64_t &len)
{
    int64_t wrote = ::write(socketFD, data, len);

    if (wrote > 0)
    {
        len = static_cast<uint64_t>(wrote);
        return true;
    }

    if (errno == EPIPE)
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__, "SIGPIPE");

    else
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);

    sckValid = false;
    return false;
}
