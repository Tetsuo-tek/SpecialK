/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSERIALSOCKET_H
#define SKSERIALSOCKET_H

#include <Core/Containers/skringbuffer.h>
#include "Core/System/Network/skabstractsocket.h"

enum SkSerialMode
{
    Mode5N1,
    Mode6N1,
    Mode7N1,
    Mode8N1,
    Mode5N2,
    Mode6N2,
    Mode7N2,
    Mode8N2,
    Mode5E1,
    Mode6E1,
    Mode7E1,
    Mode8E1,
    Mode5E2,
    Mode6E2,
    Mode7E2,
    Mode8E2,
    Mode5O1,
    Mode6O1,
    Mode7O1,
    Mode8O1,
    Mode5O2,
    Mode6O2,
    Mode7O2,
    Mode8O2
};

enum SkSerialBaud
{
    SB_1200 = 1200,
    SB_2400 = 2400,
    SB_4800 = 4800,
    SB_9600 = 9600,
    SB_19200 = 19200,
    SB_38400 = 38400,
    SB_56000 = 56000,
    SB_57600 = 57600,
    SB_115200 = 115200,
    SB_128000 = 128000,
    SB_230400 = 230400,
    SB_250000 = 250000,
    SB_500000 = 500000,
    SB_1000000 = 1000000
};

class SPECIALK SkSerialSocket extends SkAbstractSocket
{
    public:
        Constructor(SkSerialSocket, SkAbstractDevice);

        bool open(CStr *serialDevice,
                  SkSerialBaud baudRate,
                  SkSerialMode mode,
                  bool softwareControl=false);

        bool canReadLine(uint64_t &lineLength);
        uint64_t bytesAvailable();

    private:
        SkString devName;
        SkSerialBaud bauds;
        SkSerialMode modeAttributes;
        bool swCtrl;
        SkRingBuffer rx;

        bool setInterfaceAttribs();
        void onUpdateBase();

        bool readFromSocket(char *data, uint64_t &len, bool onlyPeek=false);
        bool writeToSocket(CStr *data, uint64_t &len);
};

#endif // SKSERIALSOCKET_H


