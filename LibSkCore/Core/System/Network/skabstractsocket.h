/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTSOCKET_H
#define SKABSTRACTSOCKET_H

#include "Core/System/skabstractdevice.h"
#include "Core/System/Network/skabstractflatsocket.h"

class SPECIALK SkAbstractSocket extends SkAbstractDevice
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkAbstractSocket, SkAbstractDevice);

        /**
         * @brief Sets the internal file-descriptor
         * @param fd the file-descriptor
         * @return true if the file-descriptor is > -1, otherwise false
         */
        bool setFileDescriptor(int fd);

        /**
         * @brief Gets the internal file-descriptor
         * @return the internal file-descriptor
         */
        int getFileDescriptor();

        /**
         * @brief Makes specific operations for a socket on server-side
         */
        virtual void setAsServer(){}

        virtual void setWriteBuffer(uint64_t size);
        virtual uint64_t getWriteBuffer();

        virtual void setReadBuffer(uint64_t size);
        virtual uint64_t getReadBuffer();

        virtual bool waitForData(uint64_t bytes=1);

        /**
         * @brief Checks if it is sequencial (not seekable); on sockets it is
         * always true, because they are not seekable
         * @return true
         */
        bool isSequencial()         {return true;}

        /**
         * @brief Checks if it is possible to read a line, terminating with '\n'
         * @param lineLength the value reference that will be filled with the
         * line size
         * @return true if there is a line available
         */
        bool canReadLine(uint64_t &lineLength);

        /**
         * @brief Checks if device is ready for write operation
         * @return true if device is ready, otherwise false
         */
        bool canWrite();

        /**
         * @brief Flushes input/output of the file
         * @return true if there are not errors, otherwise false
         */
        bool flush();

        /**
         * @brief Flushes the input available data for a specified size
         * @param size the size to flush (if it is 0 flush it will all data available)
         * @return true if there are not errors, otherwise false
         */
        bool flushReadBuffer(uint64_t size);

        /**
         * @brief Flushes the output
         * @return true if there are not errors, otherwise false
         */
        bool flushWriteBuffer();

        /**
         * @brief Checks if the socket is connected
         * @return true if it is connected, otherwise false
         */
        virtual bool isConnected();

        /**
         * @brief Gets an invalid size because this is a sequencial device,
         * as a data stream without an end
         * @return always -1
         */
        int64_t size()              {return -1;}

        /**
         * @brief Gets data size available to read
         * @return the available size
         */
        uint64_t bytesAvailable();

        //AS DISCONNECT

        /**
         * @brief Close the socket, making the same operation of 'disconnect' Slot
         */
        void close();

        /**
         * @brief Triggered when a client connection is established or
         * when the file-descriptor is set, for example in a server-side socket
         */
        Signal(connected);

        /**
         * @brief Triggered when the file-descriptor is no longer valid
         */
        Signal(disconnected);

        /**
         * @brief Invoked to disconnect the socket, making the same operation
         * of 'close' method
         */
        Slot(disconnect);

        //THESE METHs ARE INTENDED ONLY FOR INTERNAL USE

        /**
         * @brief Internally used; invoked to perform tick on socket
         */
        Slot(updateBase);

        /**
         * @brief Internally used; invoked to perform socket connection checks
         */
        Slot(checkConnection);

    protected:
        int socketFD;
        bool sckValid;
        bool isServerSck;

        bool readInternal(char *data, uint64_t &len, bool onlyPeek=false);
        bool writeInternal(CStr *data, uint64_t &len);

        bool enableBaseUpdate(bool value);

        //OVERRIDE IF IT MUST USE DIFFERENT WAY TO CHECK readyRead() CONDITION
        virtual void onUpdateBase();

        //OVERRIDE IF IT MUST USE DIFFERENT WAY TO CHECK THE CONNECTION
        virtual bool isConnectionValid();

        //void onDisconnect();

        virtual void onBaseUpdateChangeState(bool){}

        virtual bool readFromSocket(char *data, uint64_t &len, bool onlyPeek=false);
        virtual bool writeToSocket(CStr *data, uint64_t &len);

        void onPreparingToDie();

    private:
        bool baseUpdateEnabled;
};

#endif // SKABSTRACTSOCKET_H
