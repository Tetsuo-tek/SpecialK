/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSSLSOCKET_H
#define SKSSLSOCKET_H

#if defined(ENABLE_SSL)

#include <openssl/ssl.h>

#include "Core/System/Network/skabstractsocket.h"
#include "Core/Containers/skringbuffer.h"
#include <arpa/inet.h>

class SPECIALK SkSslSocket extends SkAbstractSocket
{
    public:
        Constructor(SkSslSocket, SkAbstractSocket);

        bool connect(CStr *hostName, uint16_t port);

        bool setFileDescriptor(int fd, SSL_CTX *sslCtx);
        bool canReadLine(uint64_t &lineLength);
        uint64_t bytesAvailable();
        void setAsServer() override;

        CStr *peerAddress();
        CStr *peerHostName();

    private:
        SSL_CTX *ctx;
        SSL *ssl;

        //client
        BIO *certbio;
        BIO *outbio;
        X509 *cert;//?? for client

        struct sockaddr_in clnt;
        struct hostent *host;

        SkString hostNameStr;
        SkString peerAddrIP;

        SkRingBuffer rx;

        bool initSslCtxClient();

        bool sslConnect();
        bool sslAccept();

        bool readFromSocket(char *data, uint64_t &len, bool onlyPeek=false) override;
        bool writeToSocket(CStr *data, uint64_t &len) override;

        void onBaseUpdateChangeState(bool enabled) override;
        void onUpdateBase();
        bool isConnectionValid() override;

        void showCerts();
};

#endif

#endif // SKSSLSOCKET_H
