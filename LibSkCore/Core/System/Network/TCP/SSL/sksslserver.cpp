#include "sksslserver.h"

#if defined(ENABLE_SSL)

#include "Core/System/Filesystem/skfsutils.h"
#include "Core/Containers/skvariant.h"
#include "Core/System/Network/TCP/SSL/sksslsocket.h"
#include "openssl/err.h"
#include <unistd.h>

#if OPENSSL_VERSION_NUMBER > 0x10100000L
    #define SSL_SERVER_METHOD TLS_server_method
#else
    #define SSL_SERVER_METHOD SSLv23_server_method
#endif

ConstructorImpl(SkSslServer, SkTcpServer)
{
    ctx = nullptr;
}


bool SkSslServer::initSslCtx(CStr *certFile, CStr *keyFile)
{
    SSL_library_init();

    c = certFile;
    k = keyFile;

    OpenSSL_add_all_algorithms();// load & register all cryptos, etc.
    SSL_load_error_strings();// load all error messages

    const SSL_METHOD *method = SSL_SERVER_METHOD();//TLSv1_2_server_method()
    ctx = SSL_CTX_new(method);// create new context from method

    if (!ctx)
    {
        ObjectError("CANNOT create SSL context");
        ERR_print_errors_fp(stderr);
        return false;
    }

    if (!SSL_CTX_set_cipher_list(ctx, "ALL:eNULL"))
    {
        ERR_print_errors_fp(stderr);
        SSL_CTX_free(ctx);
        return false;
    }

    if (!loadCerts())
    {
        ERR_print_errors_fp(stderr);
        SSL_CTX_free(ctx);
        return false;
    }

    ObjectMessage("Ssl server INITIALIZED");
    return true;
}

bool SkSslServer::loadCerts()
{
    //New lines

    if (SSL_CTX_load_verify_locations(ctx, c.c_str(), k.c_str()) != 1)
    {
        ERR_print_errors_fp(stderr);
        //return false;
    }

    if (SSL_CTX_set_default_verify_paths(ctx) != 1)
    {
        ERR_print_errors_fp(stderr);
        //return false;
    }

    //End new lines

    // set the local certificate from CertFile //
    if (SSL_CTX_use_certificate_file(ctx, c.c_str(), SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        return false;
    }

    // set the private key from KeyFile (may be the same as CertFile) //
    char pw[] = "ABCDEF";
    SSL_CTX_set_default_passwd_cb_userdata(ctx, pw);

    if (SSL_CTX_use_PrivateKey_file(ctx, k.c_str(), SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        return false;
    }
    // verify private key //
    if (!SSL_CTX_check_private_key(ctx))
    {
        ObjectError("Private key does not match the public certificate");
        return false;
    }

    //New lines - Force the client-side have a certificate
    //SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
    //SSL_CTX_set_verify_depth(ctx, 4);
    //End new lines

    return true;
}

void SkSslServer::stop()
{
    if (svrSckFD != -1)
    {
        enableBaseUpdate(false);
        SSL_CTX_free(ctx);
        close(svrSckFD);
        svrSckFD = -1;
    }
}

SSL_CTX *SkSslServer::getCtx()
{
    return ctx;
}

/*
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
//https://wiki.openssl.org/index.php/Simple_TLS_Server
int create_socket(int port)
{
    int s;
    struct sockaddr_in addr;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
        perror("Unable to create socket");
        exit(EXIT_FAILURE);
    }

    if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("Unable to bind");
        exit(EXIT_FAILURE);
    }

    if (listen(s, 1) < 0) {
        perror("Unable to listen");
        exit(EXIT_FAILURE);
    }

    return s;
}

SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = TLS_server_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
        perror("Unable to create SSL context");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx)
{
    //Set the key and cert
    if (SSL_CTX_use_certificate_file(ctx, "cert.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "key.pem", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    int sock;
    SSL_CTX *ctx;

    ctx = create_context();

    configure_context(ctx);

    sock = create_socket(4433);

    // Handle connections
    while(1) {
        struct sockaddr_in addr;
        unsigned int len = sizeof(addr);
        SSL *ssl;
        const char reply[] = "test\n";

        int client = accept(sock, (struct sockaddr*)&addr, &len);
        if (client < 0) {
            perror("Unable to accept");
            exit(EXIT_FAILURE);
        }

        ssl = SSL_new(ctx);
        SSL_set_fd(ssl, client);

        if (SSL_accept(ssl) <= 0) {
            ERR_print_errors_fp(stderr);
        } else {
            SSL_write(ssl, reply, strlen(reply));
        }

        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(client);
    }

    close(sock);
    SSL_CTX_free(ctx);
}
*/


// // // // // // //
//HTTP SERVER
/*#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"

#define FAIL    -1

int OpenListener(int port)
{   int sd;
    struct sockaddr_in addr;

    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if ( bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {
        perror("can't bind port");
        abort();
    }
    if ( listen(sd, 10) != 0 )
    {
        perror("Can't configure listening port");
        abort();
    }
    return sd;
}

SSL_CTX* InitServerCTX(void)
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    OpenSSL_add_all_algorithms();  // load & register all cryptos, etc. //
    SSL_load_error_strings();   // load all error messages //

    method = TLSv1_2_server_method();
    ctx = SSL_CTX_new(method);   // create new context from method //
    if(ctx == NULL)
    {
        ERR_print_errors_fp(stderr);
        abort();
    }

    SSL_CTX_set_cipher_list(ctx, "ALL:eNULL");

    return ctx;
}

void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile)
{
    //New lines
    if (SSL_CTX_load_verify_locations(ctx, CertFile, KeyFile) != 1)
        ERR_print_errors_fp(stderr);

    if (SSL_CTX_set_default_verify_paths(ctx) != 1)
        ERR_print_errors_fp(stderr);
    //End new lines

    // set the local certificate from CertFile //
    if (SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    // set the private key from KeyFile (may be the same as CertFile) //
    SSL_CTX_set_default_passwd_cb_userdata(ctx, "12345678");
    if (SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    // verify private key //
    if (!SSL_CTX_check_private_key(ctx))
    {
        fprintf(stderr, "Private key does not match the public certificate\n");
        abort();
    }

    //New lines - Force the client-side have a certificate
    //SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
    //SSL_CTX_set_verify_depth(ctx, 4);
    //End new lines
}

void ShowCerts(SSL* ssl)
{
    X509 *cert;
    char *line;

    cert = SSL_get_peer_certificate(ssl); // Get certificates (if available) //
    if ( cert != NULL )
    {
        printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        printf("Subject: %s\n", line);
        free(line);
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        printf("Issuer: %s\n", line);
        free(line);
        X509_free(cert);
    }
    else
        printf("No certificates.\n");
}

void Servlet(SSL* ssl) // Serve the connection -- threadable //
{
    char buf[1024];
    int sd, bytes;

    char enter[3] = { 0x0d, 0x0a, 0x00 };

    char output[1024];
    strcpy(output, "HTTP/1.1 200 OK");
    strcat(output, enter);
    strcat(output, "Content-Type: text/html");
    strcat(output, enter);
    strcat(output, "Content-Length: 47");
    strcat(output, enter);
    strcat(output, enter);
    strcat(output, "<html><body><h1>Hello World!</h1></body></html>");

    if ( SSL_accept(ssl) == FAIL )     // do SSL-protocol accept //
        ERR_print_errors_fp(stderr);
    else
    {
        ShowCerts(ssl);        // get any certificates //
        bytes = SSL_read(ssl, buf, sizeof(buf)); // get request //
        if ( bytes > 0 )
        {
            buf[bytes] = 0;
            printf("Client msg: \"%s", buf);
            SSL_write(ssl, output, strlen(output)); // send reply //
        }
        else
            ERR_print_errors_fp(stderr);
    }
    sd = SSL_get_fd(ssl);       // get socket connection //
    SSL_free(ssl);         // release SSL state //
    close(sd);          // close connection //
}

int main(int argc, char **argv)
{
    SSL_CTX *ctx;
    int server;
    char portnum[]="5000";

    char CertFile[] = "key/certificate.crt";
    char KeyFile[] = "key/private_key.pem";

    SSL_library_init();

    ctx = InitServerCTX();        // initialize SSL //
    LoadCertificates(ctx, CertFile, KeyFile); // load certs //
    server = OpenListener(atoi(portnum));    // create server socket //
    while (1)
    {
        struct sockaddr_in addr;
        socklen_t len = sizeof(addr);
        SSL *ssl;

        int client = accept(server, (struct sockaddr*)&addr, &len);  // accept connection as usual //
        printf("Connection: %s:%d\n",inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        ssl = SSL_new(ctx);              // get new SSL state with context //
        SSL_set_fd(ssl, client);      // set connection socket to SSL state //
        Servlet(ssl);         // service connection //
    }
    close(server);          // close server socket //
    SSL_CTX_free(ctx);         // release context //
}*/

/*
With only 5 openssl commands, you can accomplish this.
(Please don't change your browser security settings.)

With the following code, you can (1) become your own CA, (2) then sign your SSL
 certificate as a CA. (3) Then import the CA certificate (not the SSL certificate,
 which goes onto your server) into Chrome/Chromium. (Yes, this works even on Linux.)

NB: For Windows, some reports say that openssl must be run with winpty to avoid a crash.

######################
# Become a Certificate Authority
######################

# Generate private key
openssl genrsa -des3 -out myCA.key 2048
# Generate root certificate
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 825 -out myCA.pem

######################
# Create CA-signed certs
######################

NAME=mydomain.com # Use your own domain name

# Generate a private key
openssl genrsa -out $NAME.key 2048

# Create a certificate-signing request
openssl req -new -key $NAME.key -out $NAME.csr

# Create a config file for the extensions
>$NAME.ext cat <<-EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment

subjectAltName = @alt_names

[alt_names]
DNS.1 = $NAME # Be sure to include the domain name here because Common Name is
 not so commonly honoured by itself
DNS.2 = bar.$NAME # Optionally, add additional domains (I've added a subdomain here)
IP.1 = 192.168.0.13 # Optionally, add an IP address (if the connection which
 you have planned requires it)
EOF

# Create the signed certificate
openssl x509 -req -in $NAME.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial \
-out $NAME.crt -days 825 -sha256 -extfile $NAME.ext
To recap:

Become a CA
Sign your certificate using your CA cert+key
Import myCA.pem as an "Authority" (not into "Your Certificates") in your Chrome
 settings (Settings > Manage certificates > Authorities > Import)
Use the $NAME.crt and $NAME.key files in your server
Extra steps (for Mac, at least):

Import the CA cert at "File > Import file", then also find it in the list, right
 click it, expand "> Trust", and select "Always"
Add extendedKeyUsage=serverAuth,clientAuth below basicConstraints=CA:FALSE, and
 make sure you set the "CommonName" to the same as $NAME when it's asking for setup
You can check your work to ensure that the certificate is built correctly:

openssl verify -CAfile myCA.pem -verify_hostname bar.mydomain.com mydomain.com.crt
*/

#endif
