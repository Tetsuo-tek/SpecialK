#include "sksslsocket.h"

#if defined(ENABLE_SSL)

#include "Core/App/skeventloop.h"
#include "Core/System/Network/sknetutils.h"
#include <openssl/err.h>
#include <unistd.h>
#include <netdb.h>

#if OPENSSL_VERSION_NUMBER > 0x10100000L
    #define SSL_CLIENT_METHOD TLS_client_method
#else
    #define SSL_CLIENT_METHOD SSLv23_server_method
#endif

ConstructorImpl(SkSslSocket, SkAbstractSocket)
{
    ctx = nullptr;
    ssl = nullptr;
    host = nullptr;

    certbio = nullptr;
    outbio = nullptr;
    cert = nullptr;
}

bool SkSslSocket::connect(CStr *hostName, uint16_t port)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    clnt.sin_family = AF_INET;
    clnt.sin_port = htons(port);

    host = gethostbyname(hostName);
    struct in_addr **addr_list = (struct in_addr **) host->h_addr_list;

    SkString addrNameStr(host->h_name);

    if (addr_list[0])
        peerAddrIP = inet_ntoa(*addr_list[0]);

    else
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    ObjectDebug("Connecting to: " << addrNameStr << ":" << port << " [" << peerAddrIP << "]");

    if (!host)
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    int socketFD = socket(AF_INET, SOCK_STREAM, 0);

    if (socketFD == -1)
    {
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
        return false;
    }

    bcopy(host->h_addr, &clnt.sin_addr, static_cast<uint64_t>(host->h_length));
    bool ok = (::connect(socketFD, (const struct sockaddr *) &clnt, sizeof(sockaddr_in)) == 0);

    if (ok)
    {
        hostNameStr = hostName;
        if (!setFileDescriptor(socketFD, nullptr))
            return false;
    }

    else
    {
        ::close(socketFD);
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
        notifyState(SkDeviceState::Closed);
    }

    return ok;
}

bool SkSslSocket::initSslCtxClient()
{
    OpenSSL_add_all_algorithms();// load & register all cryptos, etc.
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();
    SSL_load_error_strings();// load all error messages

    certbio = BIO_new(BIO_s_file());
    outbio  = BIO_new_fp(stdout, BIO_NOCLOSE);

    if(SSL_library_init() < 0)
      BIO_printf(outbio, "Could not initialize the OpenSSL library !\n");

    const SSL_METHOD *method = SSL_CLIENT_METHOD();
    ctx = SSL_CTX_new(method);// create new context from method

    if (!ctx)
    {
        ObjectError("Ssl context is NOT valid");
        ERR_print_errors_fp(stderr);
        return false;
    }

    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2);

    ObjectDebug("Ssl client INITIALIZED");
    return true;
}

bool SkSslSocket::setFileDescriptor(int fd, SSL_CTX *sslCtx)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    socketFD = fd;

    ObjectDebug("SslSocket activated: " << socketFD);
    rx.setObjectName(this, "RX");

    notifyState(SkDeviceState::Open);
    enableBaseUpdate(true);

    bool isSslClient = (sslCtx == nullptr);

    if (isSslClient)
        initSslCtxClient();
    else
        ctx = sslCtx;

    // new SSL state with context //
    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, socketFD);

    if (isSslClient)
    {
        if (!sslConnect())
        {
            //disconnect();
            //return false;
            return true;
        }
    }

    else if (!sslAccept())
    {
        //disconnect();
        //return false;
        return true;
    }

    showCerts();
    return true;
}

bool SkSslSocket::sslConnect()
{
    ObjectDebug("Ssl Connecting ..");

    SkNetUtils::setSocketBlocking(socketFD, false);
    // do SSL-protocol accept //

    int u = 0;
    int err = SSL_ERROR_WANT_READ;

    while(err != SSL_ERROR_NONE)
    {
        u = SSL_connect(ssl);
        err = SSL_get_error(ssl, u);

        ObjectDebug("Ssl connect error: " << err);

        if (err == SSL_ERROR_SSL)
        {
            ObjectWarning("Ssl client is disconnected [SSL_ERROR_SSL]");
            return false;
        }

        else if (err == SSL_ERROR_ZERO_RETURN)
        {
            ObjectWarning("Ssl client is disconnected [SSL_ERROR_ZERO_RETURN]");
            return false;
        }

        else if (err == SSL_ERROR_WANT_READ || err == SSL_ERROR_WANT_WRITE)
        {
            if (err == SSL_ERROR_WANT_READ)
            {ObjectDebug("Ssl client want read [SSL_ERROR_WANT_READ]");}
            else
            {ObjectDebug("Ssl client want read [SSL_ERROR_WANT_WRITE]");}

            fd_set fds;
            struct timeval timeout;

            int sock = SSL_get_rfd(ssl);

            FD_ZERO(&fds);
            FD_SET(sock, &fds);
            timeout.tv_sec = 0;
            timeout.tv_usec = 1000000;

            int selRet = 0;

            if (err == SSL_ERROR_WANT_READ)
                selRet = select(sock+1, &fds, NULL, NULL, &timeout);
            else
                selRet = select(sock+1, NULL, &fds, NULL, &timeout);

            if (selRet <= 0)
            {
                if (selRet == 0)
                {ObjectDebug("Select timeout as reached");}

                else
                {ObjectDebug("Ssl client want read [SSL_ERROR_WANT_READ]; but selecting on it return an error: " << selRet);}
            }

            else
            {ObjectDebug("Ssl client wanting read [SSL_ERROR_WANT_READ] has been selected successfully");}
        }

        else//
        {}//break;

        if (err != SSL_ERROR_NONE)
        {ObjectDebug("Ssl client has reached the connect loop-scope [SSL_connect: " << u << "; Select: " << err << "]");}
    }

    SkNetUtils::setSocketBlocking(socketFD, true);

    if (u != 1)
    {
        if (ERR_reason_error_string(u) && ERR_error_string(u, NULL))
        {
            SkString sslError = ERR_error_string(u, nullptr);
            sslError.trim();
            SkString sslReasonError = ERR_reason_error_string(u);
            sslReasonError.trim();

            ObjectDebug("SslConnect ret [" << u << "] -> " << sslError << " == " << sslReasonError);
        }

        else
        {
            ObjectDebug("SslConnect ret [" << u << "] -> [NULL]");
            //ERR_print_errors_fp(stderr);
        }

        return false;
    }

    ObjectDebug("Ssl Connected");
    return true;
}

bool SkSslSocket::sslAccept()
{
    ObjectDebug("Ssl Accepting ..");

    SkNetUtils::setSocketBlocking(socketFD, false);
    // do SSL-protocol accept //

    int u = 0;
    int err = SSL_ERROR_WANT_READ;

    while(err != SSL_ERROR_NONE)
    {
        u = SSL_accept(ssl);
        err = SSL_get_error(ssl, u);
        //cout << "!!!! " << u << " " << err <<"\n";

        ObjectDebug("Ssl accept error: " << err);

        if (err == SSL_ERROR_SSL)
        {
            ObjectWarning("Ssl client is disconnected [SSL_ERROR_SSL]");
            return false;
        }

        else if (err == SSL_ERROR_ZERO_RETURN)
        {
            ObjectWarning("Ssl client is disconnected [SSL_ERROR_ZERO_RETURN]");
            return false;
        }

        else if (err == SSL_ERROR_WANT_READ)
        {
            ObjectDebug("Ssl client want read [SSL_ERROR_WANT_READ]");

            fd_set fds;
            struct timeval timeout;

            int sock = SSL_get_rfd(ssl);

            FD_ZERO(&fds);
            FD_SET(sock, &fds);
            timeout.tv_sec = 0;
            timeout.tv_usec = 5000;

            err = select(sock+1, &fds, NULL, NULL, &timeout);

            if (err <= 0)
            {
                if (err == 0)
                    ObjectDebug("Ssl client want read [SSL_ERROR_WANT_READ]; but select timeout as reached [1 s]");

                else
                    ObjectDebug("Ssl client want read [SSL_ERROR_WANT_READ]; but selecting on it return an error: " << err);
            }

            else
                ObjectDebug("Ssl client wanting read [SSL_ERROR_WANT_READ] has been selected successfully");
        }

        ObjectDebug("Ssl client has reached the accepting loop-scope [SSL_accept: " << u << "; Select: " << err << "]");
    }

    SkNetUtils::setSocketBlocking(socketFD, true);

    if (u != 1)
    {
        if (ERR_reason_error_string(u) && ERR_error_string(u, NULL))
        {
            SkString sslError = ERR_error_string(u, nullptr);
            sslError.trim();
            SkString sslReasonError = ERR_reason_error_string(u);
            sslReasonError.trim();

            ObjectWarning("SslAcception ret [" << u << "] -> " << sslError << " == " << sslReasonError);
        }

        else
        {
            ObjectWarning("SslAcception ret [" << u << "] -> [NULL]");
            //ERR_print_errors_fp(stderr);
        }

        return false;
    }

    ObjectDebug("Ssl Accepted");
    return true;
}

void SkSslSocket::showCerts()
{
    //X509 *cert;
    char *line;

    // Get certificates (if available) //
    cert = SSL_get_peer_certificate(ssl);

    if (cert != NULL)
    {
        //printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        //printf("Subject: %s\n", line);
        ObjectDebug("Certificate subject: " << line);
        free(line);
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        //printf("Issuer: %s\n", line);
        ObjectDebug("Certificate issuer: " << line);
        free(line);
        X509_free(cert);
    }

    else
        ObjectDebug("No certificates");
}


uint64_t SkSslSocket::bytesAvailable()
{
    return rx.size();
}

bool SkSslSocket::canReadLine(uint64_t &lineLength)
{
    if (!isOpen())
    {
        lineLength = 0;
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    uint64_t sz = bytesAvailable();

    if (sz)
    {
        char *d = new char [sz];

        if (rx.getCustomBuffer(d, sz, true))
        {
            for(uint64_t i=0; i<sz; i++)
                if (d[i] == '\n')
                {
                    lineLength = i+1;
                    delete [] d;
                    return true;
                }
        }

        delete [] d;
    }

    lineLength = 0;
    return false;
}

#include <unistd.h>
#include <sys/ioctl.h>
#include <netdb.h>

void SkSslSocket::setAsServer()
{
    socklen_t addr_size = sizeof(struct sockaddr_in);

    if (getpeername(socketFD, (struct sockaddr *)&clnt, &addr_size) != 0)
    {
        ObjectError("Cannot grab peer-address from tcp-socket");
        return;
    }

    peerAddrIP = inet_ntoa(clnt.sin_addr);

    ObjectDebug("Grabbed peer address: " << peerAddrIP);
    isServerSck = true;
}

void SkSslSocket::onBaseUpdateChangeState(bool enabled)
{
    if (enabled)
    {}//buffer = new char [bufferSize];

    else
    {
        /*delete [] buffer;
        buffer = nullptr;*/

        int sslShutdownResult = 0;

        SkNetUtils::setSocketBlocking(socketFD, false);
        while(sslShutdownResult == 0)
        {
            sslShutdownResult = SSL_shutdown(ssl);

            if (sslShutdownResult == 0) {
                // La chiusura SSL/TLS è stata richiesta ma non ancora completata
                // Puoi continuare a chiamare SSL_shutdown() se necessario
            } else if (sslShutdownResult == 1) {
                // La chiusura SSL/TLS è stata completata
            } else {
                // Errore nella chiusura SSL/TLS
            }
        }
        SkNetUtils::setSocketBlocking(socketFD, true);

        //cout << "!!!!!!!!!!!" << SSL_get_fd(ssl) << "\n";

        //isClient
        if (cert)
        {
            X509_free(cert);
            SSL_CTX_free(ctx);
            BIO_printf(outbio, "Finished SSL/TLS connection with server.\n");

            certbio = nullptr;
            outbio = nullptr;
            cert = nullptr;
        }

        SSL_free(ssl);
        ssl = nullptr;

        ::close(socketFD);
        ObjectWarning("SSL Socket closed: " << socketFD);
        socketFD = -1;
    }
}

bool SkSslSocket::isConnectionValid()
{
    if (socketFD == -1 || !sckValid)
        return false;

    char data;
    int64_t ret;

    //cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
    if ((ret = ::recv(socketFD, &data, 1, MSG_PEEK | MSG_DONTWAIT)) == 0 && (errno != EAGAIN ))
    {
        ObjectDebug("Connection check FAILED [ret==" << ret << "]");
        return false;
    }

    return true;
}

void SkSslSocket::onUpdateBase()
{
    /*fd_set fds;
    struct timeval timeout;

    int sock = SSL_get_rfd(ssl);
    FD_ZERO(&fds);
    FD_SET(sock, &fds);
    timeout.tv_sec = 0;
    timeout.tv_usec = 100;//eventLoop()->getFastInterval();

    int err = select(sock+1, &fds, NULL, NULL, &timeout);

    if (err > 0)*/
    {
        int count;

        if (ioctl(SSL_get_rfd(ssl), FIONREAD, &count) < 0)
        {
            disconnect();
            return;
        }

        if (count == 0)
            return;

        char buffer[count];
        int bytes = SSL_read(ssl, buffer, count);

        if (bytes > 0)
        {
            rx.addData(buffer, bytes);
            readyRead();
        }

        else
        {
            int err = SSL_get_error(ssl, bytes);

            if (err == SSL_ERROR_ZERO_RETURN)
            {
                ObjectError("Ssl client is disconnected [SSL_ERROR_ZERO_RETURN]");
                sckValid = false;
                notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
            }

            else if (err == SSL_ERROR_WANT_READ)
            {
                ObjectError("Ssl client want read [SSL_ERROR_WANT_READ]");
            }
        }
    }

    /*else
    {
        if (err == 0) //select TIMEOUT
        {}

        else
        {
            ObjectError("selecting for read it return an error: " << err)
            sckValid = false;
            notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
        }
    }*/
}

bool SkSslSocket::readFromSocket(char *data, uint64_t &len, bool onlyPeek)
{
    uint64_t tempSizeAvailable = bytesAvailable();

    if (tempSizeAvailable == 0)
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Cannot READ because it has NOT data");
        return false;
    }

    if (len == 0)
        len = tempSizeAvailable;

    bool ok = rx.getCustomBuffer(data, len, onlyPeek);

    if (ok)
        notifyRead(len);

    return ok;
}

bool SkSslSocket::writeToSocket(CStr *data, uint64_t &len)
{
    bool ok = false;
    int bytes = SSL_write(ssl, data, len);
    ok = bytes > 0;

    if (ok)
        len = bytes;

    else
    {
        int err = SSL_get_error(ssl, bytes);

        if (err == SSL_ERROR_ZERO_RETURN)
        {
            ObjectError("Ssl client is disconnected [SSL_ERROR_ZERO_RETURN]");
        }

        else if (err == SSL_ERROR_WANT_WRITE)
        {
            ObjectError("Ssl client want write [SSL_ERROR_WANT_WRITE]");

            fd_set fds;
            struct timeval timeout;

            int sock = SSL_get_wfd(ssl);
            FD_ZERO(&fds);
            FD_SET(sock, &fds);
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;

            err = select(sock+1, NULL, &fds, NULL, &timeout);

            if (err > 0)
            {

            }

            else
            {
                if (err == 0)
                {
                    ObjectError("Ssl client want write [SSL_ERROR_WANT_WRITE]; but select timeout as reached [1 s]");
                }

                else
                {
                    ObjectError("Ssl client want write [SSL_ERROR_WANT_WRITE]; but selecting on it return an error: " << err);
                }
            }
        }

        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);
        sckValid = false;
    }

    return ok;
}

CStr *SkSslSocket::peerAddress()
{
    return peerAddrIP.c_str();
}

CStr *SkSslSocket::peerHostName()
{
    return hostNameStr.c_str();
}

#endif
