/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSSLSERVER_H
#define SKSSLSERVER_H

#if defined(ENABLE_SSL)

#include "Core/System/Network/TCP/sktcpserver.h"

#include "openssl/ssl.h"

/*
    sudo openssl \
        req \
        -x509 \
        -nodes \
        -days 365 \
        -newkey rsa:2048 \
        -keyout selfsigned.key \
        -out selfsigned.crt
*/

class SPECIALK SkSslServer extends SkTcpServer
{
    public:
        Constructor(SkSslServer, SkTcpServer);

        bool initSslCtx(CStr *certFile, CStr *keyFile);
        void stop() override;

        SSL_CTX *getCtx();

    private:
        SkString c;
        SkString k;

        SSL_CTX *ctx;

        bool loadCerts();
        void showCerts(SSL* ssl);
};

#endif

#endif // SKSSLSERVER_H
