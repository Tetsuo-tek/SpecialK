/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTCPSOCKET_H
#define SKTCPSOCKET_H

#include "Core/System/Network/skabstractsocket.h"
#include "Core/System/Network/TCP/skflattcpsocket.h"

#include "Core/System/Network/skipaddress.h"
#include <arpa/inet.h>

class SPECIALK SkTcpSocket extends SkAbstractSocket
{
    public:
        Constructor(SkTcpSocket, SkAbstractSocket);

        bool connect(CStr *hostName, UShort port);

        /**
         * @brief Connects as client to a TCP server hostName
         * @param ipV4Addr the server IP, as SkIPAddress reference
         * @param port the TCP port open on server
         * @return true if the connection is established, otherwise false
         */
        bool connect(SkIPAddress &ipV4Addr, UShort port);

        /**
         * @brief Internally used; get some infos on the remote peer, when using
         * a client connection
         */
        void setAsServer() override;

        /**
         * @brief Gets the peer address
         * @return a cstring with address
         */
        CStr *peerAddress();

        /**
         * @brief Gets the peer hostName
         * @return a cstring with hostName
         */
        CStr *peerHostName();

    protected:
        struct sockaddr_in clnt;
        struct hostent *host;

        SkString hostNameStr;
        SkString peerAddrIP;
};

#endif // SKTCPSOCKET_H
