#include "sktcpserver.h"

#include "Core/App/skapp.h"
#include <unistd.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkTcpServer, start, bool, Arg_CStr, Arg_UInt16, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkTcpServer, stop)
//DeclareMeth_INSTANCE_VOID(SkTcpServer, enablePhanckulizer, Arg_Bool)
//DeclareMeth_INSTANCE_RET(SkTcpServer, isPhanckulizer, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkTcpServer, SkAbstractServer,
                {
                    AddMeth_INSTANCE_RET(SkTcpServer, start);
                    AddMeth_INSTANCE_VOID(SkTcpServer, stop);
//                  AddMeth_INSTANCE_VOID(SkTcpServer, enablePhanckulizer);
//                  AddMeth_INSTANCE_RET(SkTcpServer, isPhanckulizer);
                })
{
    svrPort = 0;
    /*phanckulizeSockets = false;

    SignalSet(phanckulizerEnabled);
    SignalSet(phanckulized);*/
}

bool SkTcpServer::start(CStr *address, uint16_t port, uint64_t maxQueuedConnections)
{
    if (svrSckFD != -1)
    {
        notifyError(SkServerError::ServerAlreadyOpen);
        return false;
    }

    if ((svrSckFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        notifyError(SkServerError::CannotCreateServer);
        return false;
    }

    svrPort = port;
    svr.sin_family = AF_INET;
    svrAddress = address;
    svr.sin_port = htons(svrPort);

    if (address && memcmp(address, "0.0.0.0", strlen(address)) != 0)
    {
        svr.sin_addr.s_addr = inet_addr(address);
        ObjectMessage("Binding tcp server -> " << address << ":" << svrPort);
    }

    else
    {
        svr.sin_addr.s_addr = htonl(INADDR_ANY);
        ObjectMessage("Binding tcp server -> 0.0.0.0:" << svrPort);
    }

    socklen_t t = sizeof(sockaddr_in);

    int a = 1;

    if (::setsockopt(svrSckFD, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int)) == -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
        svrAddress.clear();
        return false;
    }

    if (::bind(svrSckFD, (const struct sockaddr *) &svr, t) == -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
        svrAddress.clear();

        notifyError(SkServerError::CannotBind);
        return false;
    }

    bool ok = enableLinistening(maxQueuedConnections);

    if (!ok)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
        svrAddress.clear();
    }

    return ok;
}

/*void SkTcpServer::executePhanckulization(int &fdClient, sockaddr_in &remote)
{
    SkString peerAddrIP = inet_ntoa(remote.sin_addr);
    ObjectWarning("A socket was phanckulized: " << peerAddrIP << " [" << fdClient << "]");

    close(fdClient);

    phanckulizedValue()->setVal(peerAddrIP);
    phanckulized();

    fdClient = -1;
}*/

bool SkTcpServer::tryToAcceptSD(int &fdClient)
{
    sockaddr_in remote;
    socklen_t t = sizeof(remote);
    bool ok = ((fdClient = accept(svrSckFD, (sockaddr *) &remote, &t)) > -1);

    /*if (ok && phanckulizeSockets)
    {
        executePhanckulization(fdClient, remote);
        ok = false;
    }*/

    return ok;
}

/*void SkTcpServer::onAcceptConnection(SkAbstractSocket *sck)
{
    sck->setAsServer();
    ObjectDebug("Accepted IP: " << dynamic_cast<SkTcpSocket*>(sck)->peerAddress());
}*/

void SkTcpServer::stop()
{
    if (svrSckFD != -1)
    {
        //if (isListening())
            enableBaseUpdate(false);

        close(svrSckFD);
        svrSckFD = -1;
    }
}

/*void SkTcpServer::enablePhanckulizer(bool value)
{
    phanckulizeSockets = value;
    phanckulizerEnabledValue()->setVal(phanckulizeSockets);
    phanckulizerEnabled();
}

bool SkTcpServer::isPhanckulizer()
{
    return phanckulizeSockets;
}*/

CStr *SkTcpServer::getServerAddress()
{
    return svrAddress.c_str();
}

uint16_t SkTcpServer::getServerPort()
{
    return svrPort;
}
