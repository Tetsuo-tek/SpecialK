/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKWEBCLIENT_H
#define SKWEBCLIENT_H

#if defined(ENABLE_HTTP)

#include <Core/Containers/skringbuffer.h>
#include <Core/Containers/skqueue.h>
#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

struct SkWsFrame
{
    bool ctrlByte_OK;
    bool fin;
    bool rsv1;
    bool rsv2;
    bool rsv3;
    uint8_t opCode;
    uint8_t ctrlInjectionOpCode;

    bool lenByte_OK;
    //uint8_t lastOpCode;//resetted ONLY on start and disconnection
    bool mask;
    uint8_t length;

    bool extLenLong_OK;
    ULong extendedLength;

    bool maskInt_OK;
    uint32_t maskingKey;

    bool payload_OK;
    SkDataBuffer payload;
    SkDataBuffer tempBuffer;

    bool isOpen;
    bool isFragmented;
    bool isCtrlInjection;
    bool isClosingFragment;

    SkWsFrame();
    void reset();
    void resetCtrlInjection();
};

enum SkWsSendingMode
{
    WSM_TEXT,
    WSM_BINARY
};

struct SkWsQueuePck
{
    SkWsSendingMode mode;
    ULong sz;
};

class SPECIALK SkWebSocket extends SkAbstractDevice
{
    SkHttpSocket *sck;
    bool baseUpdateEnabled;
    SkWsFrame fr;

    SkString wsVersion;
    SkString wsKey;
    SkString wsExtensions;

    SkRingBuffer rx;

    SkWsSendingMode currentSendingMode;

    public:
        Constructor(SkWebSocket, SkAbstractDevice);

        //DEFAULT IS SkWsSendingMode::WSM_BINARY
        void setSendingMode(SkWsSendingMode m);

        //THIS ACTIVATE A SERVERSOCKET AFTER HANDSHAKE IS FINISHED
        //THIS WS-SCK WILL BE CHILD OF HTTP-SCK
        bool open(SkHttpSocket *httpSck);

        bool isSequencial()         {return true;}
        bool isConnected();
        int64_t size()              {return -1;}
        ULong bytesAvailable();

        //AS DISCONNECT
        void close();

        CStr *getVersion();
        CStr *getKey();
        CStr *getExtensions();

        SkHttpSocket *getInternalSocket();

        void sendPing(CStr *data, ULong sz);
        void sendPong(CStr *data, ULong sz);

        Slot(disconnect);

        Signal(connected);
        Signal(disconnected);

        //text and binary SIGs HAVE LEN VALUES INSIDE
        Signal(textReadyRead);
        Signal(binaryReadyRead);

        Signal(textSent);
        Signal(binarySent);

        Signal(pingReceived);
        Signal(pongReceived);

        //THESE METHs ARE INTENDED ONLY FOR INTERNAL USE
        Slot(onSocketDisconnection);
        Slot(updateBase);

        Slot(onReadyRead);

    private:
        void readFrame();

        void sendFrameHeader(ULong payloadLen, uint8_t frame_t);
        bool writeDataFrame(CStr *data, ULong &len);

        void onPreparingToDie();

        bool enableBaseUpdate(bool value);

        bool readInternal(char *data, ULong &len, bool onlyPeek=false);
        bool writeInternal(CStr *data, ULong &len);
};

#endif

#endif // SKWEBCLIENT_H
