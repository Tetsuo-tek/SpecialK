#if defined(ENABLE_HTTP)

#include "skcookies.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //SK_DESTRUCTOR(OutputTh)

SkCookieAttribute::SkCookieAttribute()
{
    expires = false;
    secure = false;
}

SkCookieAttribute::SkCookieAttribute(const SkCookieAttribute &other)
{
    expires = other.expires;
    expiringDateTime = other.expiringDateTime;
    domain = other.domain;
    path = other.path;
    secure = other.secure;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //SK_DESTRUCTOR(OutputTh)

SkCookies::SkCookies()
{
}

SkCookies::~SkCookies()
{
    clear();
}

void SkCookies::add(SkString name, SkString value)
{
    SkCookieAttribute attribute;
    add(name, value, attribute);
}

void SkCookies::add(SkString name, SkString value, SkCookieAttribute &attribute)
{
    cookies[name] = value;
    SkCookieAttribute *a = new SkCookieAttribute(attribute);
    attributes << a;
}

void SkCookies::produceResponseHeaders(SkStringList &cookieHeadersList)
{
    if (cookies.isEmpty())
        return;

    SkStringList keysList;
    cookies.keys(keysList);

    for (uint64_t i=0; i<keysList.count(); i++)
    {
        cookieHeadersList.append("");//% encode
        SkString &cookie = cookieHeadersList.last();

        cookie.append("Set-Cookie: ");
        cookie.append(keysList.at(i));
        cookie.append("=");
        cookie.append(cookies[keysList.at(i)].toString());

        SkCookieAttribute &attribute = *attributes[i];

        if (attribute.expires)
        {
            if (attribute.expiringDateTime.isLocal())
                attribute.expiringDateTime.setAsUTC();

            SkString t("; Expires=");

            CStr *currentLocale = setlocale(LC_TIME, NULL);
            setlocale(LC_TIME, "C");
            t.append(attribute.expiringDateTime.toString("%a, %d %b %Y %H:%M:%S GMT"));
            setlocale(LC_TIME, currentLocale);

            cookie.append(t);
        }

        if (!attribute.domain.isEmpty())
        {
            cookie.append("; domain=");
            cookie.append(attribute.domain);
        }

        if (!attribute.path.isEmpty())
        {
            cookie.append("; path=");
            cookie.append(attribute.path);
        }

        if (attribute.secure)
            cookie.append("; secure");
    }
}

void SkCookies::produceRequestHeader(SkString &cookiesRequestHeader)
{
    if (cookies.isEmpty())
        return;

    SkStringList keysList;
    cookies.keys(keysList);

    cookiesRequestHeader = "Cookie: ";

    for (uint64_t i=0; i<keysList.count(); i++)
    {
        cookiesRequestHeader.append(keysList[i]);
        cookiesRequestHeader.append("=");
        cookiesRequestHeader.append(cookies[keysList.at(i)].toString());

        if (i<keysList.count()-1)
            cookiesRequestHeader.append("; ");
    }
}

uint64_t SkCookies::count()
{
    return cookies.count();
}

void SkCookies::clear()
{
    cookies.clear();

    for(uint64_t i=0; i<attributes.count(); i++)
        delete attributes[i];

    attributes.clear();
}

#endif
