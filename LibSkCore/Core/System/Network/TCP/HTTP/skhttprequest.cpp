#include "skhttprequest.h"

#if defined(ENABLE_HTTP)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/Containers/skarraycast.h>
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Filesystem/skfile.h>
#include <Core/System/Network/TCP/sktcpsocket.h>
#include <Core/System/Network/TCP/SSL/sksslsocket.h>
#include "Core/System/Network/TCP/HTTP/skcookies.h"
#include <Core/App/skeventloop.h>

SkHttpFormData::SkHttpFormData()
{
    boundary = "------------------------";
    std::ostringstream ss;
    ss << hex << SkArrayCast::toUInt64(this);
    boundary.append(ss.str().data());
}

CStr *SkHttpFormData::getBoundary()
{
    return boundary.c_str();
}

void SkHttpFormData::setTextField(CStr *name, CStr *text)
{
    SkString part = "--";
    part.append(boundary);
    part.append("\r\n");
    part.append("Content-Disposition: form-data; name=\"");
    part.append(name);
    part.append("\"\r\n");
    part.append("Content-Length: ");
    part.concat(strlen(text));
    part.append("\r\n\r\n");

    part.append(text);
    part.append("\r\n");

    insert(name, part);
}

bool SkHttpFormData::setFileField(CStr *name, CStr *filePath, CStr *fileReName)
{
    SkFileInfo nfo;
    SkFsUtils::fillFileInfo(filePath, nfo);

    if (!nfo.exists)
    {
        FlatError("The file NOT exists: " << filePath);
        return false;
    }

    if (!nfo.size)
    {
        FlatError("The file is empty: " << filePath);
        return false;
    }

    if (!nfo.isRegularFile)
    {
        FlatError("The file is NOT a regular file: " << filePath);
        return false;
    }

    if (!nfo.isReadable)
    {
        FlatError("The file is NOT readable: " << filePath);
        return false;
    }

    SkString part = "--";
    part.append(boundary);
    part.append("\r\n");
    part.append("Content-Disposition: form-data; name=\"");
    part.append(name);
    part.append("\"; filename=\"");

    if (fileReName)
        part.append(fileReName);
    else
        part.append(nfo.completeName);

    part.append("\"\r\n");
    part.append("Content-Length: ");
    part.concat(nfo.size);
    part.append("\r\n\r\n");

    SkArgsMap partMap;
    partMap["part"] = part;
    partMap["path"] = filePath;
    partMap["sz"] = nfo.size;

    insert(name, partMap);

    return true;
}

uint64_t SkHttpFormData::getLength()
{
    SkStringList l;
    keys(l);

    uint64_t sz = 0;

    for(uint64_t i=0; i<l.count(); i++)
    {
        SkVariant &v = value(l[i]);

        if (v.isString())
            sz += v.size()-1;

        else if (v.isMap())
        {
            SkArgsMap part;
            v.copyToMap(part);
            sz += (part["sz"].toUInt32()+2);//+"\r\n"
        }
    }

    sz += (boundary.size()+2);//boundary+"--"
    return sz;
}

bool SkHttpFormData::writeToDevice(SkAbstractDevice *device)
{
    SkStringList l;
    keys(l);

    for(uint64_t i=0; i<l.count(); i++)
    {
        SkVariant &v = value(l[i]);

        if (v.isString())
            device->write(v.data(), v.size()-1);

        else if (v.isMap())
        {
            SkArgsMap part;
            v.copyToMap(part);
            //sz += (part["sz"].toUInt32()+2);//+"\r\n"

            SkString filePath = part["path"].toString();

            SkFile *f = new SkFile(device);
            f->setFilePath(filePath.c_str());

            if (f->open(SkFileMode::FLM_ONLYREAD))
                return false;

            while(!f->atEof() && device->isOpen())
                if (!f->readHereWriteTo(device, 8192))
                {
                    f->close();
                    f->destroyLater();
                    return false;
                }

            f->close();
            f->destroyLater();

            device->write("\r\n", 2);
        }
    }

    device->write(boundary);
    device->write("--", 2);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkHttpRequest, SkObject)
{
    sck = nullptr;

    reset();

    timeout = 20.;

    SignalSet(responsed);
    SignalSet(dataAdded);
    SignalSet(completed);
    SlotSet(onClosedConnection);
    SlotSet(onReadyRead);
    SlotSet(onOneSecTick);
}

void SkHttpRequest::reset()
{
    if (sck)
        sck->destroyLater();

    sck = nullptr;

    methodRequested.clear();
    requestHeaders.clear();
    requestCookies = nullptr;
    responseHeaders.clear();
    protocolVersion.clear();
    responseDescription.clear();
    responseCode = SkHttpStatusCode::NullCode;
    isChuncked = false;
    nextChunkSize = 0;
    responseHeadersReceived = false;
    contentLength = -1;
    receivedDataSize = 0;
    output = nullptr;
    elapsedTime = 0.;

    requestHeaders["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36";
    //requestHeaders["Connection"] = "close";
    requestHeaders["Accept"] = "*/*";
    requestHeaders["Accept-Encoding"] = "gzip, deflate";
    requestHeaders["Accept"] = "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7";
}

void SkHttpRequest::setTimeout(double seconds)
{
    timeout = seconds;
}

void SkHttpRequest::setCookies(SkCookies *cookies)
{
    requestCookies = cookies;
}

/*bool SkHttpRequest::head(SkUrl &url, SkArgsMap headers)
{
    reset();

    SkString request;
    prepareRequest("HEAD", url, request, headers);
    return sendRequest(url, request);
}*/

bool SkHttpRequest::get(SkUrl &url, SkAbstractDevice *target, SkArgsMap headers)
{
    reset();
    output = target;

    SkString request;
    return (prepareRequest("GET", url, request, headers) && sendRequest(request));
}

bool SkHttpRequest::post(SkUrl &url, SkAbstractDevice *target, SkHttpFormData *formData, SkArgsMap headers)
{
    reset();

    output = target;

    formData->setObjectName(this, "FormData");

    SkString contentType = "multipart/form-data; boundary=";
    contentType.append(formData->getBoundary());
    headers["Content-Type"] = contentType;
    headers["Content-Length"] = formData->getLength();

    SkString request;

    bool ok = (prepareRequest("POST", url, request, headers) && sendRequest(request));

    if (ok)
        formData->writeToDevice(sck);

    return ok;
}

bool SkHttpRequest::post(SkUrl &url, SkAbstractDevice *target, SkArgsMap &jsonValue, SkArgsMap headers)
{
    reset();

    output = target;

    SkString json;
    jsonValue.toString(json);

    SkString contentType = "application/json";
    headers["Content-Type"] = contentType;
    headers["Content-Length"] = json.size();

    SkString request;

    bool ok = (prepareRequest("POST", url, request, headers) && sendRequest(request));

    if (ok)
        sck->write(json);

    return ok;
}


bool SkHttpRequest::put(SkUrl &url, SkArgsMap headers)
{
    reset();

    SkString request;
    return (prepareRequest("PUT", url, request, headers) && sendRequest(request));
}

bool SkHttpRequest::prepareRequest(CStr *httpMethod, SkUrl &url, SkString &request, SkArgsMap &headers)
{
    methodRequested = httpMethod;

    ObjectMessage("Requesting http-method [" << httpMethod << "] -> " << url.completeUrlString());

    if (sck)
        sck->destroyLater();

    if (SkString::compare(url.getProtocol(), "http"))
    {
        UShort port = url.getPort();

        if (!port)
            port = 80;

        SkTcpSocket *socket = new SkTcpSocket(this);
        socket->setObjectName(this, "TcpSck");

        if (!socket->connect(url.getHost(), port))
        {
            socket->destroyLater();
            return false;
        }

        sck = socket;
    }

    else if (SkString::isEmpty(url.getProtocol()) || SkString::compare(url.getProtocol(), "https"))
    {
        UShort port = url.getPort();

        if (!port)
            port = 443;

        SkSslSocket *socket = new SkSslSocket(this);
        socket->setObjectName(this, "SslSck");

        if (!socket->connect(url.getHost(), port))
        {
            socket->destroyLater();
            return false;
        }

        sck = socket;
    }

    else
    {
        ObjectError("Protocol NOT valid: " << url.getProtocol());
        return false;
    }

    Attach(sck, readyRead, this, onReadyRead, SkDirect);
    Attach(sck, disconnected, this, onClosedConnection, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick, SkQueued);

    request = methodRequested;
    request.append(" ");
    SkString encoded;
    SkUrl::encode(url.getPath(), encoded);
    request.append(encoded);
    request.append("?");
    request.append(url.getQuery());
    request.append(" HTTP/1.1");

    ObjectDebug("Requesting method: " << request);

    request.append("\r\n");

    SkString hostHeader = url.getHost();
    UShort port = url.getPort();

    if (port)
    {
        hostHeader.append(":");
        hostHeader.concat(port);
    }

    requestHeaders["Host"] = hostHeader;

    if (url.hasLogin())
    {
        SkString auth = url.getUserName();
        auth.append(":");
        auth.append(url.getPassword());

        SkDataBuffer b64Auth;
        SkDataBuffer encodingAuth;
        encodingAuth.setData(auth.c_str(), auth.size());
        encodingAuth.toBase64(b64Auth);

        SkString basicAuth = "Basic ";
        basicAuth.append(b64Auth.toString());

        requestHeaders["Authorization"] = basicAuth;
    }

    requestHeaders.insertMap(headers);

    SkStringList l;
    requestHeaders.keys(l);

    for(uint64_t i=0; i<l.count(); i++)
    {
        request.append(l[i]);
        request.append(": ");
        request.append(requestHeaders[l[i]].toString());
        request.append("\r\n");
    }

    if (requestCookies)
    {
        SkString ck;
        requestCookies->produceRequestHeader(ck);
        request.append(ck);
        request.append("\r\n");
    }

    request.append("\r\n");
    return true;
}

bool SkHttpRequest::sendRequest(SkString &request)
{
    if (!sck->write(request))
    {
        ObjectError("Cannot send the request");
        return false;
    }

    chrono.start();
    ObjectDebug("Request SENT");
    return true;
}

SlotImpl(SkHttpRequest, onReadyRead)
{
    SilentSlotArgsWarning();

    if (!responseHeadersReceived)
    {
        bool thereIsAline = true;

        while(thereIsAline && !responseHeadersReceived)
        {
            uint64_t sz;
            thereIsAline = sck->canReadLine(sz);

            if (thereIsAline)
            {
                SkString line;
                sck->read(line, sz);
                line.trim();

                if (line.isEmpty())
                {
                    responseHeadersReceived = true;
                    ObjectDebug("Responsed [" << responseCode << ", " << httpCodeString(responseCode) << "]");

                    SkVariantVector p;
                    p << static_cast<uint16_t>(responseCode);
                    responsed(p);

                    if (!contentLength)
                    {
                        ObjectWarning("Content-Length is ZERO [No content]");

                        if (sck->isConnected())
                            sck->disconnect();
                    }

                    else
                    {
                        if (sck->bytesAvailable() > 2)
                            eventLoop()->invokeSlot(onReadyRead_SLOT, this, this, nullptr, 0);
                    }
                }

                else
                {
                    SkStringList parsed;

                    if (responseCode == SkHttpStatusCode::NullCode)
                    {
                        line.split(' ', parsed);

                        if (parsed.count() > 2)
                        {
                            protocolVersion = parsed.first();
                            parsed.removeFirst();
                            responseCode = static_cast<SkHttpStatusCode>(parsed.first().toInt());
                            parsed.removeFirst();
                            responseDescription = parsed.join(" ");
                        }

                        else
                        {
                            ObjectError("Status code line is MALFORMED: " << line);
                            sck->disconnect();
                        }
                    }

                    else
                    {
                        line.split(": ", parsed);

                        if (parsed.count() > 1)
                        {
                            SkString headerName = parsed.first();
                            parsed.removeFirst();
                            SkString headerValue = parsed.join(": ");

                            isChuncked = (headerName == "Transfer-Encoding" && headerValue == "chunked");

                            if (headerName == "Set-Cookie")
                            {
                            }

                            else
                                responseHeaders[headerName] = headerValue;

                            if (headerName == "Content-Length")
                                contentLength = headerValue.toInt();
                        }

                        else
                        {
                            ObjectError("Header line is MALFORMED: " << line);
                            sck->disconnect();
                        }
                    }
                }
            }
        }
    }

    else
    {
        if (isChuncked)
        {
            if (nextChunkSize == 0)
            {
                ULong sz = 0;

                if (sck->canReadLine(sz))
                {
                    SkString s;
                    sck->read(s, sz);
                    s.trim();

                    //cout << "# " << s << "\n";

                    if (s == "0")
                    {
                        if (sck->isConnected())
                            sck->disconnect();

                        nextChunkSize = 0;
                    }

                    else
                    {
                        if (!s.isEmpty())
                            nextChunkSize = strtol(s.c_str(), nullptr, 16);

                        else
                            nextChunkSize = 0;
                    }
                }
            }

            if (nextChunkSize && sck->bytesAvailable() >= nextChunkSize)
            {
                sck->readHereWriteTo(output, nextChunkSize);
                sck->flushReadBuffer(2);

                SkVariantVector p;
                p << nextChunkSize;
                dataAdded(p);

                nextChunkSize = 0;

                if (sck->bytesAvailable() > 2)
                    eventLoop()->invokeSlot(onReadyRead_SLOT, this, this, nullptr, 0);
            }
        }

        else
        {
            uint64_t sz = sck->bytesAvailable();
            sck->readHereWriteTo(output, sz);

            receivedDataSize += sz;

            SkVariantVector p;
            p << sz;
            dataAdded(p);

            if (contentLength)
            {
                if (contentLength == static_cast<int64_t>(receivedDataSize))
                {
                    if (sck->isConnected())
                        sck->disconnect();
                }
            }
        }
    }
}

SlotImpl(SkHttpRequest, onClosedConnection)
{
    SilentSlotArgsWarning();

    elapsedTime = chrono.stop();

    SkVariantVector p;

    if (output)
        p << static_cast<uint64_t>(output->size());

    else
        p << 0;

    completed(p);

    Detach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecTick);
    ObjectDebug("Request COMPLETED: " << elapsedTime << " s");
}

SlotImpl(SkHttpRequest, onOneSecTick)
{
    SilentSlotArgsWarning();

    if (chrono.stop() > timeout)
    {
        ObjectError("Connection timeout reached [" << timeout << " seconds]");
        sck->disconnect();
    }
}

SkHttpStatusCode SkHttpRequest::getResponsedCode()
{
    return responseCode;
}

uint64_t SkHttpRequest::getReceivedSize()
{
    return receivedDataSize;
}

SkArgsMap &SkHttpRequest::getRequestedHeaders()
{
    return requestHeaders;
}

SkArgsMap &SkHttpRequest::getResponsedHeaders()
{
    return responseHeaders;
}

double SkHttpRequest::getElapsedTime()
{
    return elapsedTime;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
