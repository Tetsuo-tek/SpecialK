#include "skwebsocket.h"

#if defined(ENABLE_HTTP)

#include <Core/App/skeventloop.h>
#include "Core/Containers/skarraycast.h"
#include <Core/System/skbufferdevice.h>
#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h"

//OPCODES - 4 bits

//This frame continues the payload from the previous frame.
#define CONTINUING_FRAME        0x00

//Denotes a text frame. Text frames are UTF-8 decoded by the server.
#define TEXT_DATA               0x01

//Denotes a binary frame. Binary frames are delivered unchanged by the server.
#define BINARY_DATA             0x02

//Reserved for future use.
//0x03-0x07

//CTRL-FRAME - Denotes the client wishes to close the connection.
#define WOULD_CLOSE             0x08

//CTRL-FRAME -A ping frame. Serves as a heartbeat mechanism ensuring the
//connection is still alive. The receiver must respond with a pong.
#define PING_FRAME              0x09

//CTRL-FRAME -A pong frame. Serves as a heartbeat mechanism ensuring the
//connection is still alive. The receiver must respond with a ping frame.
#define PONG_FRAME              0x0A

//Reserved for future use.
//0x0b-0x0f

SkWsFrame::SkWsFrame()
{
    reset();
}

void SkWsFrame::reset()
{
    isOpen = false;
    isFragmented = false;
    isCtrlInjection = false;
    isClosingFragment = false;

    ctrlByte_OK = false;
    lenByte_OK = false;
    extLenLong_OK = false;
    maskInt_OK = false;

    //GRABBED BUT NOT USED
    fin = false;
    rsv1 = false;
    rsv2 = false;
    rsv3 = false;

    opCode = 0;
    ctrlInjectionOpCode = 0;

    mask = false;
    maskingKey = 0;

    length = 0;//<=125 (7 bit) , 126 (16 bit) or 127 (64 bit)
    extendedLength = 0;//frame payload sent lenght (for all size-type)

    payload_OK = false;
    payload.clear();
    tempBuffer.clear();
}

void SkWsFrame::resetCtrlInjection()
{
    tempBuffer.clear();
    isCtrlInjection = false;
    ctrlInjectionOpCode = 0;
}

ConstructorImpl(SkWebSocket, SkAbstractDevice)
{
    sck = nullptr;
    baseUpdateEnabled = false;
    currentSendingMode = SkWsSendingMode::WSM_BINARY;

    SlotSet(disconnect);

    SignalSet(connected);
    SignalSet(disconnected);

    SignalSet(textReadyRead);
    SignalSet(binaryReadyRead);

    SignalSet(textSent);
    SignalSet(binarySent);

    SignalSet(pingReceived);
    SignalSet(pongReceived);

    SlotSet(onSocketDisconnection);
    SlotSet(updateBase);

    SlotSet(onReadyRead);
}

void SkWebSocket::setSendingMode(SkWsSendingMode m)
{
    currentSendingMode = m;
}

CStr *SkWebSocket::getVersion()
{
    return wsVersion.c_str();
}

CStr *SkWebSocket::getKey()
{
    return wsKey.c_str();
}

CStr *SkWebSocket::getExtensions()
{
    return wsExtensions.c_str();
}

SkHttpSocket *SkWebSocket::getInternalSocket()
{
    return sck;
}

bool SkWebSocket::open(SkHttpSocket *httpSck)
{
    setParent(httpSck);
    sck = httpSck;

    SkString name = httpSck->objectName();
    name.append(".WS");
    setObjectName(name.c_str());

    if (!httpSck->isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    SkArgsMap &headers = httpSck->getHttpRequestHeaders();

    if (httpSck->isWebSocketRequest())
    {
        SkString name;
        name.append(httpSck->peerAddress());
        name.append("@");
        name.append(SkString::number(httpSck->getFileDescriptor()));
        name.append(":");
        name.append(httpSck->url().getPath());
        setObjectName(name.c_str());

        wsVersion = headers["Sec-WebSocket-Version"].toString();
        wsKey = headers["Sec-WebSocket-Key"].toString();
        wsExtensions = headers["Sec-WebSocket-Extensions"].toString();

        ObjectWarning("Activating WebSocket ["
                  << "Vers: " << wsVersion << "; "
                  << "Key: " << wsKey << "; "
                  << "Ext: " << wsExtensions << "]");

        //Chromium  -> Activating WebSocket [Vers: 13; Key: u/1zpwusiZ8DyWIQMbWBqA==; Ext: permessage-deflate; client_max_window_bits]
        //Chrome    -> Activating WebSocket [Vers: 13; Key: tgFqqoq7jJzrJY5dBtYnag==; Ext: permessage-deflate; client_max_window_bits]
        //Edge      -> Activating WebSocket [Vers: 13; Key: CtPzmYbKHvljWZExusN1ug==; Ext: permessage-deflate; client_max_window_bits]

        //Firefox   -> Activating WebSocket [Vers: 13; Key: E7Dj//OzeGoGGQfcQZDZ7g==; Ext: permessage-deflate]

        SkString hash = wsKey;
        hash.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");

        ObjectDebug("Creating WebSocket-accepting SHA1 of: " << hash << " ..");

        SkDataBuffer b;
        b.setData(hash.c_str(), hash.size());
        b.toBinaryHash(b, SkHashMode::HM_SHA1);
        SkDataBuffer b64;
        b.toBase64(b64);
        hash.clear();
        hash.append(b64.data(), b64.size());
        ObjectDebug("Sec-WebSocket-Accept: " << hash);

        SkHttpResponseHeaders httpHeader;
        //httpHeader.clear();
        httpHeader.setHttpCode(httpSck->getServerName(), SkHttpStatusCode::SwitchingProtocol, "Switching Protocols");
        httpHeader.setConnection("Upgrade");
        httpHeader.setUpgrade("websocket");
        httpHeader.setWsAccept(hash.c_str());
        //httpHeader.setWsExt("permessage-deflate");
        //httpHeader.setWsProtocol("chat");

        SkBufferDevice *outputHeadersDev = new SkBufferDevice(this);

        name = httpSck->objectName();
        name.append(".WBUF");
        outputHeadersDev->setObjectName(name.c_str());

        SkDataBuffer outputHeadersBuf;
        outputHeadersDev->open(outputHeadersBuf, SkBufferDeviceMode::BVM_ONLYWRITE);
        httpHeader.writeOnDevice(outputHeadersDev);
        outputHeadersDev->close();
        outputHeadersDev->destroyLater();

        httpSck->write(&outputHeadersBuf);
    }

    else
    {
        ObjectError("WebSocket-Request NOT VALID ['Connection' header is required]");
        return false;
    }

    notifyState(SkDeviceState::Open);
    enableBaseUpdate(true);

    Attach(sck->getInternalSocket(), readyRead, this, onReadyRead, SkDirect);
    Attach(sck, disconnected, this, onSocketDisconnection, SkDirect);
    return true;
}

bool SkWebSocket::isConnected()
{
    return (sck && sck->isConnected());
}

ULong SkWebSocket::bytesAvailable()
{
    return rx.size();
}

void SkWebSocket::close()
{
    //MUST SEND CLOSING FRAME - AFTER IT WILL CLOSE sck

    if (isConnected())
        sck->disconnect();
}

SlotImpl(SkWebSocket, disconnect)
{
    SilentSlotArgsWarning();

    close();
}

SlotImpl(SkWebSocket, onSocketDisconnection)
{
    SilentSlotArgsWarning();
}

bool SkWebSocket::enableBaseUpdate(bool value)
{
    if (value)
    {
        if (baseUpdateEnabled)
        {
            ObjectError("WEBSCK-UPDATE is ALREADY enabled");
            return false;
        }

        baseUpdateEnabled = true;
        Attach(eventLoop()->fastZone_SIG, pulse, this, updateBase, SkAttachMode::SkQueued);

        fr.reset();
        ObjectDebug("WEBSCK-UPDATE ENABLED");
        connected();
    }

    else
    {
        if (!baseUpdateEnabled)
        {
            ObjectError("WEBSCK-UPDATE is ALREADY disabled");
            return false;
        }

        baseUpdateEnabled = false;
        Detach(eventLoop()->fastZone_SIG, pulse, this, updateBase);

        fr.reset();
        ObjectDebug("WEBSCK-UPDATE DISABLED");
        disconnected();
    }

    return true;
}

SlotImpl(SkWebSocket, updateBase)
{
    SilentSlotArgsWarning();

    if (isPreparedToDie())
        return;

    if (!sck->isConnected())
        enableBaseUpdate(false);
}

SlotImpl(SkWebSocket, onReadyRead)
{
    SilentSlotArgsWarning();
    readFrame();
}

void SkWebSocket::readFrame()
{
    if (!fr.isOpen && !fr.ctrlByte_OK)
    {
        if (sck->bytesAvailable() < 1)
            return;

        bitset<8> bs =  {sck->readUInt8()};
        fr.fin = bs[7];
        fr.rsv1 = bs[6];
        fr.rsv2 = bs[5];
        fr.rsv3 = bs[4];

        bs[4] = 0;
        bs[5] = 0;
        bs[6] = 0;
        bs[7] = 0;

        fr.opCode = static_cast<uint8_t>(bs.to_ulong());
        SkString frameTypeName;

        if (fr.opCode == TEXT_DATA)
            frameTypeName = "TXT";

        else if (fr.opCode == BINARY_DATA)
            frameTypeName = "BIN";

        else
            frameTypeName = "CTRL";

        ObjectDebug("Frame type: " << frameTypeName << " -> "
                      << "FIN: " << fr.fin << "; RSV1: " << fr.rsv1 << "; "
                      << "RSV2: " << fr.rsv2 << "; RSV3: " << fr.rsv3 << "; "
                      << "OPCODE: " << bs[3] << bs[2] << bs[1] << bs[0]
                      << " (" << (int) fr.opCode << ")  -> 0x" << hex << setfill('0') << hex << setw(2) << (int) fr.opCode);

        fr.ctrlByte_OK = true;
        fr.isFragmented = !fr.fin;
        fr.isClosingFragment = fr.fin;
    }

    else if (fr.isOpen && fr.payload_OK)
    {
        if (sck->bytesAvailable() < 1)
            return;

        SkWsFrame tempFrame;

        bitset<8> bs =  {sck->readUInt8()};
        tempFrame.fin = bs[7];
        tempFrame.rsv1 = bs[6];
        tempFrame.rsv2 = bs[5];
        tempFrame.rsv3 = bs[4];

        bs[4] = 0;
        bs[5] = 0;
        bs[6] = 0;
        bs[7] = 0;

        tempFrame.opCode = static_cast<uint8_t>(bs.to_ulong());

        if (tempFrame.opCode == CONTINUING_FRAME)
        {
            SkString frameTypeName;

            if (fr.opCode == TEXT_DATA)
                frameTypeName = "TXT";

            else if (fr.opCode == BINARY_DATA)
                frameTypeName = "BIN";

            ObjectDebug("FRAGMENT (of "
                          << " -> type: " << frameTypeName
                              << " 0x" << hex << setfill('0') << hex << setw(2)<< (int) fr.opCode << ") "
                          << "FIN: " << tempFrame.fin << "; RSV1: " << tempFrame.rsv1 << "; "
                          << "RSV2: " << tempFrame.rsv2 << "; RSV3: " << tempFrame.rsv3 << "; "
                          << "OPCODE: " << bs[3] << bs[2] << bs[1] << bs[0]
                          << " (" << (int) tempFrame.opCode << ") -> 0x" << hex << setfill('0') << hex << setw(2) << (int) tempFrame.opCode);
        }

        else
        {
            SkString frameTypeName;

            if (fr.opCode == WOULD_CLOSE)
                frameTypeName = "CLOSE";

            else if (fr.opCode == PING_FRAME)
                frameTypeName = "PING";

            else if (fr.opCode == PONG_FRAME)
                frameTypeName = "PONG";

            else
                frameTypeName = "UNKNOWN";

            ObjectDebug("CTRL frame type -> : " << frameTypeName << " -> "
                                         << "FIN: " << tempFrame.fin << "; RSV1: " << tempFrame.rsv1 << "; "
                                         << "RSV2: " << tempFrame.rsv2 << "; RSV3: " << tempFrame.rsv3 << "; "
                                         << "OPCODE: " << bs[3] << bs[2] << bs[1] << bs[0]
                                         << " (" << (int) tempFrame.opCode << ")  -> 0x" << hex << setfill('0') << hex << setw(2) << (int) tempFrame.opCode);

            fr.isCtrlInjection = true;
            fr.ctrlInjectionOpCode = tempFrame.opCode;
        }

        fr.isClosingFragment = tempFrame.fin;
        fr.lenByte_OK = false;
        fr.extLenLong_OK = false;
        fr.maskInt_OK = false;
        fr.payload_OK = false;
    }

    if (fr.ctrlByte_OK && !fr.lenByte_OK)
    {
        if (sck->bytesAvailable() < 1)
            return;

        bitset<8> bs = {sck->readUInt8()};

        fr.mask = bs[7];
        ObjectDebug("Has masking: " << SkVariant::boolToString(fr.mask));
        bs[7] = 0;

        fr.length = static_cast<ULong>(bs.to_ulong());
        ObjectDebug("Base lenght: " << (int) fr.length);

        fr.lenByte_OK = true;
    }

    if (fr.lenByte_OK && !fr.extLenLong_OK)
    {
        // ! ! ! ! // 7 bit
        if (fr.length <= 125)
        {
            fr.extendedLength = fr.length;

            ObjectDebug("Payload-length value size of 7 bit: " << fr.extendedLength);
            fr.extLenLong_OK = true;
        }

        else
        {
            // ! ! ! ! // 16 bit
            if (fr.length == 126)
            {
                if (sck->bytesAvailable() < 2)
                    return;

                fr.extendedLength = 0;

                uint8_t *p = SkArrayCast::toUInt8(&fr.extendedLength);

                for(int i=1; i>=0; i--)
                    p[i] = sck->readUInt8();

                ObjectDebug("Payload-length value size of 16 bit [" << (int) fr.length << "]: " << fr.extendedLength);
                fr.extLenLong_OK = true;
            }

            // ! ! ! ! // 64 bit
            else if (fr.length == 127)
            {
                if (sck->bytesAvailable() < 8)
                    return;

                fr.extendedLength = 0;
                uint8_t *p = SkArrayCast::toUInt8(&fr.extendedLength);

                for(int i=7; i>=0; i--)
                    p[i] = sck->readUInt8();

                ObjectDebug("Payload-length value size of 64 bit [" << (int) fr.length << "]: " << fr.extendedLength);
                fr.extLenLong_OK = true;
            }
        }
    }

    if (fr.extLenLong_OK && !fr.maskInt_OK)
    {
        if (fr.mask)
        {
            if (sck->bytesAvailable() < 4)
                return;

            fr.maskingKey = sck->readUInt32();
            ObjectDebug("Masking key: " << fr.maskingKey);
            fr.maskInt_OK = true;
        }

        else
        {
            ObjectDebug("No Masking key");
            fr.maskInt_OK = true;
        }
    }

    if (fr.maskInt_OK && !fr.payload_OK)
    {
        // ! ! ! ! // GRAB PAYLOAD

        if (fr.extendedLength)
        {
            ULong sz = fr.extendedLength - fr.tempBuffer.size();

            if (sck->bytesAvailable() < sz)
                sz = sck->bytesAvailable();

            sck->read(&fr.tempBuffer, sz);

            if (fr.tempBuffer.size() < fr.extendedLength)
            {
                ObjectDebug("Only partial payload-buffer arrived: " << fr.tempBuffer.size()  << "/" << fr.extendedLength);
                return;
            }

            else
                ObjectDebug("Payload-buffer completed: " << fr.tempBuffer.size()  << "/" << fr.extendedLength);

            if (fr.mask)
                for (ULong i=0; i<fr.extendedLength; i++)
                    fr.tempBuffer[i] ^= SkArrayCast::toChar(&fr.maskingKey)[i % 4];

            if (!fr.isCtrlInjection)
            {
                fr.payload.append(fr.tempBuffer);
                fr.tempBuffer.clear();
            }

            fr.payload_OK = true;
        }

        // ! ! ! ! // EVALUATE IF IT IS CTRL

        bool isCtrl = false;

        if (fr.opCode == WOULD_CLOSE || fr.ctrlInjectionOpCode == WOULD_CLOSE)
        {
            isCtrl = true;
            ObjectDebug("CLOSE frame type received -> [len_T: " << (int) fr.length << " / len: " << fr.extendedLength << " B]");
            disconnect();
        }

        else if (fr.opCode == PING_FRAME || fr.ctrlInjectionOpCode == PING_FRAME)
        {
            isCtrl = true;
            ObjectDebug("PING frame type received -> [len_T: " << (int) fr.length << " / len: " << fr.extendedLength << " B]");
            pingReceived();

            SkDataBuffer *b;

            if (fr.isCtrlInjection)
                b = &fr.tempBuffer;

            else
                b = &fr.payload;

            sendPong(b->data(), b->size());
        }

        else if (fr.opCode == PONG_FRAME || fr.ctrlInjectionOpCode == PONG_FRAME)
        {
            isCtrl = true;
            ObjectDebug("PONG frame type received -> [len_T: " << (int) fr.length << " / len: " << fr.extendedLength << " B]");
            pongReceived();//IT IS FAKE AND NOT CONTROL IF THE CONTENT MATCH THE PING REQUEST
        }

        if (fr.isCtrlInjection)
        {
            fr.resetCtrlInjection();
            fr.tempBuffer.clear();
            return;
        }

        else if (isCtrl && fr.isClosingFragment)
        {
            fr.reset();
            return;
        }

        // ! ! ! ! // EVALUATE CLOSING OR CONTINUING

        if (fr.isClosingFragment)
        {
            if (!fr.payload.isEmpty())
                rx.addData(fr.payload.data(), fr.payload.size());
        }

        else
        {
            fr.isOpen = true;
            return;
        }

        // ! ! ! ! // SIGNALING GRABBED DATA AND CLOSE THE FRAMEs-SEQUENCE

        if (fr.opCode == TEXT_DATA)
        {
            ObjectDebug("TEXT-DATA frame type received -> [len: " << fr.payload.size() << " B]");
            SkVariantVector p;
            p << static_cast<uint32_t>(fr.payload.size());
            textReadyRead(p);
        }

        else if (fr.opCode == BINARY_DATA)
        {
            ObjectDebug("BINARY-DATA frame type received -> [len: " << fr.payload.size() << " B]");
            SkVariantVector p;
            p << static_cast<uint32_t>(fr.payload.size());
            binaryReadyRead(p);
        }

        fr.reset();

        // ! ! ! ! //
    }
}

void SkWebSocket::sendFrameHeader(ULong payloadLen, uint8_t frame_t)
{
    bitset<8> bs = {frame_t};
    bs[7] = true;       //FIN
    bs[6] = false;      //RSV1
    bs[5] = false;      //RSV2
    bs[4] = false;      //RSV3

    sck->writeUInt8(static_cast<uint8_t>(bs.to_ulong()));

    if (payloadLen <= 125)
    {
        bs = {payloadLen};
        bs[7] = false;      //MASK
        sck->writeUInt8(static_cast<uint8_t>(bs.to_ulong()));
    }

    else if (payloadLen <= 65535)
    {
        bs = {126};
        bs[7] = false;      //MASK
        sck->writeUInt8(static_cast<uint8_t>(bs.to_ulong()));

        //BUILDING THE FUCKED NETWORK ORDER!
        //sck->writeUInt16(static_cast<UShort>(payloadLen));

        UShort p[1];
        p[0] = static_cast<UShort>(payloadLen);

        for(int i=1; i>=0; i--)
            sck->writeUInt8(SkArrayCast::toUInt8(p)[i]);
    }

    else
    {
        bs = {127};
        bs[7] = false;      //MASK
        sck->writeUInt8(static_cast<uint8_t>(bs.to_ulong()));

        //BUILDING THE FUCKED NETWORK ORDER!
        //sck->writeUInt64(static_cast<ULong>(payloadLen));

        ULong p[1];
        p[0] = static_cast<ULong>(payloadLen);

        for(int i=7; i>=0; i--)
            sck->writeUInt8(SkArrayCast::toUInt8(p)[i]);
    }
}

bool SkWebSocket::writeDataFrame(CStr *data, ULong &len)
{
    uint8_t m = BINARY_DATA;

    if (currentSendingMode == SkWsSendingMode::WSM_TEXT)
        m = TEXT_DATA;

    sendFrameHeader(len, m);

    if (!sck->write(data, len))
        return false;

    if (currentSendingMode == SkWsSendingMode::WSM_BINARY)
        binarySent(this);

    else
        textSent(this);

    return true;
}

void SkWebSocket::sendPing(CStr *data, ULong sz)
{
    if (sz > 125)
    {
        ObjectError("Cannot send PING-FRAME with payload length > 125 Bytes; we are sending: " << sz << " B");
        return;
    }

    if (!SkString::isEmpty(data))
        sz = strlen(data);

    sendFrameHeader(sz, PING_FRAME);
    sck->write(data, sz);

    ObjectDebug("Ping sent");
}

void SkWebSocket::sendPong(CStr *data, ULong sz)
{
    if (sz > 125)
    {
        ObjectError("Cannot send PONG-FRAME with payload length > 125 Bytes; we are sending: " << sz << " B");
        return;
    }

    if (!SkString::isEmpty(data))
        sz = strlen(data);

    sendFrameHeader(sz, PONG_FRAME);
    sck->write(data, sz);

    ObjectDebug("Pong sent");
}

bool SkWebSocket::readInternal(char *data, ULong &len, bool onlyPeek)
{
    if (!isOpen())
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = rx.getCustomBuffer(data, len, onlyPeek);

    if (ok)
        notifyRead(len);

    return ok;
}

bool SkWebSocket::writeInternal(CStr *data, ULong &len)
{
    if (!isOpen())
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return writeDataFrame(data, len);
}

void SkWebSocket::onPreparingToDie()
{
    if (baseUpdateEnabled)
        enableBaseUpdate(false);
}

#endif

//RFC 6455 The WebSocket Protocol
//THE FRAME WIRE-FORMAT
/*                                                             32 bits
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|15 4 3 2 1 0 9 8 7             0
+-+-+-+-+-------+-+-------------+-------------------------------+
|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
|N|V|V|V|       |S|             |   (if payload len==126/127)   |
| |1|2|3|       |K|             |                               |
+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
|     Extended payload length continued, if payload len == 127  |
+ - - - - - - - - - - - - - - - +-------------------------------+
|                               |Masking-key, if MASK set to 1  |
+-------------------------------+-------------------------------+
| Masking-key (continued)       |   Extension/Payload Data      |
+-------------------------------- - - - - - - - - - - - - - - - +
:                     Payload Data continued ...                :
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|                     Payload Data continued ...                |
+---------------------------------------------------------------+
FIN: 1 bit
Indicates that this is the final fragment in a message. The first fragment may
 also be the final fragment.

RSV1, RSV2, RSV3, RSV4 (OR MASK): 1 bit each
Must be 0 unless an extension is negotiated which defines meanings for non-zero
 values

Opcode: 4 bits
Defines the interpretation of the payload data

Payload length: 7 bits
The length of the payload: if 0-125, that is the payload length.
 If 126, the following 2 bytes interpreted as a 16 bit unsigned integer are the
 payload length.
 If 127, the following 8 bytes interpreted as a 64-bit unsigned integer (the
 high bit must be 0) are the payload length. Multibyte length quantities are
 expressed in network byte order. The payload length is the length of the
 Extension data + the length of the Application Data. The length of the
 Extension data may be zero, in which case the Payload length is the length of
 the Application data.

 *All control frames MUST be 125 bytes or less in length and MUST NOT be fragmented.

Extension data:
n bytes

The extension data is 0 bytes unless there is a reserved op-code or reserved
bit present in the frame which indicates an extension has been negotiated.
Any extension MUST specify the length of the extension data, or how that length
may be calculated, and its use MUST be negotiated during the handshake.
If present, the extension data is included in the total payload length.
Application data:
n bytes

Arbitrary application data, taking up the remainder of the frame after any extension data.
The length of the Application data is equal to the payload length minus the length of
the Extension data.
ws-frame               = frame-fin
                            frame-rsv1
                            frame-rsv2
                            frame-rsv3
                            frame-opcode
                            frame-rsv4
                            frame-length
                            frame-extension
                            application-data;

frame-fin              = %x0 ; more frames of this message follow
                      / %x1 ; final frame of message

frame-rsv1             = %x0 ; 1 bit, must be 0
frame-rsv2             = %x0 ; 1 bit, must be 0
frame-rsv3             = %x0 ; 1 bit, must be 0

frame-opcode           = %x0 ; continuation frame
                      / %x1 ; connection close
                      / %x2 ; ping
                      / %x3 ; pong
                      / %x4 ; text frame
                      / %x5 ; binary frame
                      / %x6-F ; reserved

frame-rsv4             = %x0 ; 1 bit, must be 0

frame-length           = %x00-7D
                      / %x7E frame-length-16
                      / %x7F frame-length-63

frame-length-16        = %x0000-FFFF
frame-length-63        = %x0000000000000000-7FFFFFFFFFFFFFFF
frame-extension        = *( %x00-FF ) ; to be defined later
application-data       = *( %x00-FF )
*/


/*
    Frame type: BIN -> FIN: 0; RSV1: 0; RSV2: 0; RSV3: 0; OPCODE: 0010 (2)  -> 0x02
    Has masking: true
    Base lenght: 127
    Payload-length value size of 64 bit [127]: 88503
    Masking key: 1411939302
    Only partial payload-buffer arrived: 16370/88503
    Only partial payload-buffer arrived: 32754/88503
    Only partial payload-buffer arrived: 49138/88503
    Only partial payload-buffer arrived: 65522/88503
    Only partial payload-buffer arrived: 81906/88503
    Payload-buffer completed: 88503/88503
    FRAGMENT (of  -> type: BIN 0x02) FIN: 1; RSV1: 0; RSV2: 0; RSV3: 0; OPCODE: 0000 (0) -> 0x00
    Has masking: true
    Base lenght: 126
    Payload-length value size of 16 bit [126]: 5986
    Masking key: 468908644
    Payload-buffer completed: 5986/5986
    BINARY-DATA frame type received -> [len: 94489 B]
*/
