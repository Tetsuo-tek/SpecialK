/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKCOOKIESETTER_H
#define SKCOOKIESETTER_H

#if defined(ENABLE_HTTP)

#include <Core/Containers/skargsmap.h>
#include <Core/System/Time/skdatetime.h>
#include <Core/Containers/skarray.h>

struct SPECIALK SkCookieAttribute
{
    bool expires;
    SkDateTime expiringDateTime;
    SkString domain;
    SkString path;
    bool secure;

    SkCookieAttribute();
    SkCookieAttribute(const SkCookieAttribute &other);
};

class SPECIALK SkCookies extends SkFlatObject
{
    public:
        /**
         * @brief Simple contructor
         */
        SkCookies();

        ~SkCookies();

        /**
         * @brief Adds a simple cookie variable
         * @param name the name of the variable
         * @param value the value of the variable
         */
        void add(SkString name, SkString value);

        /**
         * @brief Adds a cookie variable with attributes
         * @param name the name of the variable
         * @param value the value of the variable
         * @param attribute the attributes reference
         */
        void add(SkString name, SkString value, SkCookieAttribute &attribute);

        /**
         * @brief Produces a list of 'Set-Cookie' HTTP-SERVER headers
         * @param cookiesList the cookies HTTP-SERVER headers strings
         */
        void produceResponseHeaders(SkStringList &cookieHeadersList);

        /**
         * @brief Produces a the 'Cookie' HTTP-CLIENT header value with all cookies
         * @param cookiesRequest the cookies HTTP-SERVER header value
         */
        void produceRequestHeader(SkString &cookiesRequest);

        /**
         * @brief Gets the cookies count
         * @return the count number
         */
        uint64_t count();

        /**
         * @brief Clears cookies
         */
        void clear();

    private:
        SkArgsMap cookies;
        SkArray<SkCookieAttribute *> attributes;
};

#endif

#endif // SKCOOKIESETTER_H
