/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKWSREDISTR_H
#define SKWSREDISTR_H

#if defined(ENABLE_HTTP)

#include <Core/System/skdeviceredistr.h>
#include "skwebsocket.h"

class SkWsRedistr extends SkDeviceRedistr
{
    public:
        Constructor(SkWsRedistr, SkDeviceRedistr);

        void setHeaderMode(SkWsSendingMode m);

        void sendTextToAll(SkString &txt);
        void sendTextToAll(CStr *txt, uint64_t size);

        void sendBinaryToAll(SkDataBuffer &data);
        void sendBinaryToAll(CStr *data, uint64_t size);

    private:
        SkWsSendingMode headerMode;
        virtual void onAddDevice(SkAbstractDevice *target);
};

#endif

#endif // SKWSREDISTR_H
