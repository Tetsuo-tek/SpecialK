#include "skmultipartredistr.h"

#if defined(ENABLE_HTTP)

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkMultipartRedistr, init, Arg_CStr)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 1, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 2, Arg_CStr, Arg_CStr, Arg_Custom(SkCookies))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 3, Arg_CStr, Arg_CStr, Arg_Custom(SkCookies), *Arg_Custom(SkArgsMap))

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkMultipartRedistr, SkDeviceRedistr,
                {
                    AddMeth_INSTANCE_VOID(SkMultipartRedistr, init);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 1);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 2);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkMultipartRedistr, init, 3);
                })
{
    initialized = false;
}

void SkMultipartRedistr::init(CStr *serverName,
                              CStr *partMime,
                              SkCookies *cookies,
                              SkArgsMap optionalHeaders)
{
    SkHttpResponseHeaders header;
    header.setHttpCode(serverName, SkHttpStatusCode::Ok);

    SkString boundary = uuid();

    SkString multipartHeader = "multipart/x-mixed-replace;boundary=--";
    multipartHeader.append(boundary);

    header.setContentType(multipartHeader.c_str());

    if (!optionalHeaders.isEmpty())
        header.append(optionalHeaders);

    SkString hStr = header.toString(cookies);
    setHeader(hStr.c_str(), hStr.size());

    partBaseHeader = "----";
    partBaseHeader.append(boundary);
    partBaseHeader.append("\r\n");

    partBaseHeader.append("Content-Type: ");

    if (SkString::isEmpty(partMime))
        partBaseHeader.append("application/octet-stream");
    else
        partBaseHeader.append(partMime);

    partBaseHeader.append("\r\n");
    partBaseHeader.append("Content-Length: ");//to be completed on sending data

    initialized = true;
}

void SkMultipartRedistr::sendToAll(CStr *data, uint64_t size)
{
    if (!initialized)
    {
        ObjectError("Redistr is NOT initialized");
        return;
    }

    if (isEmpty())
        return;

    SkString baseHeaderWithSize = partBaseHeader;
    baseHeaderWithSize.concat(static_cast<int>(size));
    baseHeaderWithSize.append("\r\n\r\n");

    for(uint64_t i=0; i<count(); i++)
    {
        if (getDevice(i)->isOpen())
        {
            getDevice(i)->write(baseHeaderWithSize);
            getDevice(i)->write(data, size);
            getDevice(i)->write("\r\n", 2);
        }
    }
}

#endif
