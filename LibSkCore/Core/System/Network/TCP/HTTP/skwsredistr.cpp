#include "skwsredistr.h"

#if defined(ENABLE_HTTP)

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkWsRedistr, setHeaderMode, Arg_Enum(SkWsSendingMode))
DeclareMeth_INSTANCE_VOID(SkWsRedistr, sendTextToAll, Arg_StringRef)
DeclareMeth_INSTANCE_VOID(SkWsRedistr, sendBinaryToAll, *Arg_Custom(SkDataBuffer))

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkWsRedistr, SkDeviceRedistr,
                {
                    AddMeth_INSTANCE_VOID(SkWsRedistr, setHeaderMode);
                    AddMeth_INSTANCE_VOID(SkWsRedistr, sendTextToAll);
                    AddMeth_INSTANCE_VOID(SkWsRedistr, sendBinaryToAll);
                })
{
    headerMode = SkWsSendingMode::WSM_TEXT;
}

void SkWsRedistr::setHeaderMode(SkWsSendingMode m)
{
    headerMode = m;
}

void SkWsRedistr::sendTextToAll(SkString &txt)
{
    sendTextToAll(txt.c_str(), txt.size());
}

void SkWsRedistr::sendTextToAll(CStr *txt, uint64_t size)
{
    for(uint64_t i=0; i<count(); i++)
        dynamic_cast<SkWebSocket *>(getDevice(i))->setSendingMode(SkWsSendingMode::WSM_TEXT);

    sendToAll(txt, size);
}

void SkWsRedistr::sendBinaryToAll(SkDataBuffer &data)
{
    sendBinaryToAll(data.data(), data.size());
}

void SkWsRedistr::sendBinaryToAll(CStr *data, uint64_t size)
{
    for(uint64_t i=0; i<count(); i++)
        dynamic_cast<SkWebSocket *>(getDevice(i))->setSendingMode(SkWsSendingMode::WSM_BINARY);

    sendToAll(data, size);
}

void SkWsRedistr::onAddDevice(SkAbstractDevice *target)
{
    dynamic_cast<SkWebSocket *>(target)->setSendingMode(headerMode);
}

#endif
