#ifndef SKHTTPREQUEST_H
#define SKHTTPREQUEST_H

#if defined(ENABLE_HTTP)

#include "Core/System/Time/skelapsedtime.h"
#include "Core/System/Network/skurl.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h"
#include "Core/Object/skobject.h"

class SkAbstractDevice;
class SkAbstractSocket;
class SkSslSocket;
class SkCookies;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SPECIALK SkHttpFormData extends SkArgsMap
{
    public:
        SkHttpFormData();

        CStr *getBoundary();

        void setTextField(CStr *name, CStr *text);
        bool setFileField(CStr *name, CStr *filePath, CStr *fileReName=nullptr);

        uint64_t getLength();
        bool writeToDevice(SkAbstractDevice *device);

    private:
        SkString boundary;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//ONLY multipart/form-data (spec points to RFC2388) is supported

class SPECIALK SkHttpRequest extends SkObject
{
    public:
        Constructor(SkHttpRequest, SkObject);

        void setTimeout(double seconds);

        void setCookies(SkCookies *cookies);

        //bool head(SkUrl &url, SkArgsMap headers=SkArgsMap());
        bool get(SkUrl &url, SkAbstractDevice *target, SkArgsMap headers=SkArgsMap());
        bool post(SkUrl &url, SkAbstractDevice *target, SkHttpFormData *formData, SkArgsMap headers=SkArgsMap());
        bool post(SkUrl &url, SkAbstractDevice *target, SkArgsMap &jsonValue, SkArgsMap headers=SkArgsMap());
        bool put(SkUrl &url, SkArgsMap headers=SkArgsMap());

        SkHttpStatusCode getResponsedCode();
        uint64_t getReceivedSize();
        SkArgsMap &getRequestedHeaders();
        SkArgsMap &getResponsedHeaders();
        double getElapsedTime();

        Signal(responsed);
        Signal(dataAdded);
        Signal(completed);

        Slot(onReadyRead);
        Slot(onClosedConnection);

        Slot(onOneSecTick);

        SkAbstractSocket *sck;
    private:

        SkArgsMap requestHeaders;

        SkString methodRequested;
        SkString protocolVersion;
        SkHttpStatusCode responseCode;
        SkString responseDescription;
        SkArgsMap responseHeaders;

        SkCookies *requestCookies;

        SkArgsMap postFormData;

        bool responseHeadersReceived;
        bool isChuncked;
        ULong nextChunkSize;
        int64_t contentLength;
        uint64_t receivedDataSize;
        SkAbstractDevice *output;

        SkElapsedTime chrono;
        double elapsedTime;
        double timeout;

        void reset();
        bool prepareRequest(CStr *httpMethod, SkUrl &url, SkString &request, SkArgsMap &headers);
        bool sendRequest(SkString &request);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKHTTPREQUEST_H
