﻿/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKHTTPSOCKET_H
#define SKHTTPSOCKET_H

#if defined(ENABLE_HTTP)

#include <Core/Containers/skringbuffer.h>
#include <Core/Containers/skdatabuffer.h>
#include <Core/System/Network/TCP/sktcpsocket.h>
#include <Core/System/Network/TCP/SSL/sksslsocket.h>
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Time/skelapsedtime.h>

#include "skhttpresponseheaders.h"
#include "skpostparser.h"
#include "Core/Containers/HTML/skwebpage.h"
#include "Core/System/Network/TCP/HTTP/skcookies.h"
#include "Core/System/Network/skurl.h"
#include "Mountpoints/skgenericmountpoint.h"

class SkFile;
class SkLocalSocket;

class SPECIALK SkHttpSocket extends SkAbstractDevice
{
    SkString svrName;

    SkString httpMethod;
    SkArgsMap httpRequest;
    SkUrl urlRequest;
    SkArgsMap requestHeaders;
    SkArgsMap cookies;
    SkString user;
    SkString password;

    bool requestHeadersReceived;
    uint64_t requestHeaderSize;
    uint64_t maxHSize;

    SkElapsedTime activityCheckChrono;
    double maxIdleSecs;

    SkPostParser postParser;
    uint32_t postCurrentSize;
    uint32_t postLength;

    bool responseSent;

    SkMountPointType mpType;
    uint64_t blockSize;
    SkRingBuffer svltProduction;
    SkAbstractDevice *dataSource;

    SkAbstractSocket *sck;
    bool ssl;

    public:
        Constructor(SkHttpSocket, SkAbstractDevice);

        void setup(SkString serverName, bool enableSsl, uint64_t maxHeaderSize, uint32_t maxIdleSeconds);

        bool setFileDescriptor(int fd, SSL_CTX *ctx=nullptr);
        int getFileDescriptor();

        bool isSslEnabled();

        CStr *getServerName();
        CStr *getHttpMethod();
        SkArgsMap &getHttpRequest();
        SkArgsMap &getHttpRequestHeaders();
        bool isWebSocketRequest();
        SkArgsMap &getHttpCookies();
        CStr *getUsername();
        CStr *getPassword();
        SkUrl &url();

        bool send(SkHttpStatusCode code=SkHttpStatusCode::Ok,
                  SkCookies *cookies=nullptr,
                  SkArgsMap optionalHeaders=SkArgsMap());

        bool send(SkWebPage *page,
                  SkHttpStatusCode code=SkHttpStatusCode::Ok,
                  SkCookies *cookies=nullptr,
                  SkArgsMap optionalHeaders=SkArgsMap());

        bool send(SkString &txt,
                  CStr *contentType,
                  SkHttpStatusCode code=SkHttpStatusCode::Ok,
                  SkCookies *cookies=nullptr,
                  SkArgsMap optionalHeaders=SkArgsMap());

        bool send(CStr *data,
                  int64_t size,
                  CStr *contentType,
                  SkHttpStatusCode code=SkHttpStatusCode::Ok,
                  SkCookies *cookies=nullptr,
                  SkArgsMap optionalHeaders=SkArgsMap());

        bool sendFile(SkFile *f, SkFileInfo *nfo, uint64_t dataBlockSize);

        //THE RESPONSE DEPEND ON SERVLET ACTIVITY
        bool requestToServlet(SkMountPointType t, SkAbstractSocket *svlt, uint64_t dataBlockSize);

        SkPostParser *getPostParser();

        Signal(request);
        Signal(failure);

        Slot(onReadyRead);
        Slot(sendingTick);
        Slot(svltGrabTick);
        Slot(oneSecTick);

        virtual void setAsServer();

        virtual void setWriteBuffer(uint64_t size);
        virtual uint64_t getWriteBuffer();

        virtual void setReadBuffer(uint64_t size);
        virtual uint64_t getReadBuffer();

        virtual bool waitForData(uint64_t bytes=1);

        bool isSequencial()         {return true;}

        bool canReadLine(uint64_t &lineLength);
        bool canWrite();
        bool flush();

        bool flushReadBuffer(uint64_t size);
        bool flushWriteBuffer();

        virtual bool isConnected();
        int64_t size()              {return -1;}
        uint64_t bytesAvailable();

        void close();

        CStr *peerAddress();
        //CStr *peerHostName();

        SkAbstractSocket *getInternalSocket();

        Signal(connected);
        Signal(disconnected);

        Slot(disconnect);

        Slot(onSckConnect);
        Slot(onSckDisconnect);

    private:
        bool analyzeHeaders(SkString &line);

        bool sendResponse(CStr *data,
                          int64_t size,
                          CStr *contentType,
                          SkHttpStatusCode code,
                          SkCookies *cookies,
                          SkArgsMap &optionalHeaders);


        void executeRequestMethod();
        bool checkSentResponse();
        bool capturePostInputData();

        SkTcpSocket *tcpSck();
        SkSslSocket *sslSck();

        bool readInternal(char *data, uint64_t &len, bool onlyPeek=false);
        bool writeInternal(CStr *data, uint64_t &len);
};

#endif

#endif // SKHTTPSOCKET_H
