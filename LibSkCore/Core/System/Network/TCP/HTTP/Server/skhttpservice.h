#ifndef SKHTTPSERVICE_H
#define SKHTTPSERVICE_H

#include <Core/System/Network/TCP/SSL/sksslserver.h>

#if defined(ENABLE_HTTP) && defined(ENABLE_XML)

#include "skhttpstatuscodes.h"
#include "Mountpoints/skgenericmountpoint.h"

class SkCookies;
class SkWebPage;
class SkWebSocket;

class SkHttpService extends SkObject
{
    public:
        Constructor(SkHttpService, SkObject);

        void setup(CStr *serverName, bool enableSsl, uint64_t maxReceivingHeaderSize, uint32_t maxClientsIdleSeconds=60);

        bool start(CStr *address, uint16_t port, uint64_t maxQueuedConnections);
        void stop();

        bool existsMountPoint(CStr *mpPath);
        bool addMountPoint(SkGenericMountPoint *mp);
        bool delMountPoint(CStr *mpPath);
        void clearMountPoints();

        void sendStatusPageAndKill(SkHttpSocket *httpSck,
                                   SkHttpStatusCode code,
                                   SkCookies *cookies=nullptr,
                                   SkArgsMap optionalHeaders=SkArgsMap());

        CStr *getServerName();

        SkTcpServer *tcpSvr();
        SkSslServer *sslSvr();
        SkAbstractServer *absSvr();

        bool isListening();

        Signal(newHttpSocketConnection);
        Signal(newWebSocketConnection);

        Slot(buildQueuedSockets);
        Slot(removeSocketWithoutRequest);
        Slot(onRequest);
        Slot(onFailure);

    protected:
        //Permits to customize status page before the server sending it
        virtual SkWebPage *onBuildStatusPage(SkHttpStatusCode code);

    private:
        SkString svrName;
        uint64_t maxHeaderSize;
        uint32_t maxClientsIdleSecs;

        SkAbstractServer *svr;
        bool ssl;

        SkTreeMap<SkString, SkGenericMountPoint *> mountPoints;

        SkHttpStatusCode acceptHttpSocket(SkGenericMountPoint *mp, SkHttpSocket *httpSck);
        SkHttpStatusCode acceptWebSocket(SkGenericMountPoint *mp, SkHttpSocket *httpSck);
        bool buildWebSocket(SkHttpSocket *httpSck, SkWebSocket *wsSck);
};

#endif

#endif // SKHTTPSERVICE_H
