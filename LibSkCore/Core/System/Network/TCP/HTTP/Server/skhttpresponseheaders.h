/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKHTTPRESPONSEHEADERS_H
#define SKHTTPRESPONSEHEADERS_H

#if defined(ENABLE_HTTP)

#include <Core/Containers/skargsmap.h>
#include "skhttpstatuscodes.h"

class SkWebPage;
class SkCookies;
class SkAbstractDevice;

class SPECIALK SkHttpResponseHeaders extends SkArgsMap
{
    public:
        SkHttpResponseHeaders();

        void setHttpCode(CStr *svrName, SkHttpStatusCode httpCode, CStr *codeDesc="");
        void setupAsRedirect(CStr *location);

        void setContentType(CStr *value);
        void setContentLength(int64_t value);
        void setAttachmentName(CStr *value);
        void setConnection(CStr *value);
        void setCacheControl(CStr *value);
        void setUpgrade(CStr *value);
        void setWsAccept(CStr *value);
        void setWsExt(CStr *value);
        void setWsProtocol(CStr *value);

        void append(SkArgsMap &headers);

        //bool setOnWebPage(SkWebPage *page);

        SkString toString(SkCookies *cookies=nullptr, bool terminateHeaders=true);
        bool writeOnDevice(SkAbstractDevice *device, SkCookies *cookies=nullptr, bool terminateHeaders=true);

    private:
        SkHttpStatusCode responseCode;
        SkString responseDesc;
};

#endif

#endif // SKHTTPRESPONSEHEADERS_H
