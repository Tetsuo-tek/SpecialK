#include "skhttpservice.h"

#if defined(ENABLE_HTTP) && defined(ENABLE_XML)

#include "skhttpsocket.h"
#include "Mountpoints/skfsmountpoint.h"
#include "Mountpoints/skrawredistrmountpoint.h"
#include "Mountpoints/skpartredistrmountpoint.h"
#include "Mountpoints/skwsredistrmountpoint.h"
#include "Core/System/Network/TCP/HTTP/skcookies.h"

ConstructorImpl(SkHttpService, SkObject)
{
    ssl = false;
    svr = nullptr;

    maxHeaderSize = 0;
    maxClientsIdleSecs = 0;

    SignalSet(newHttpSocketConnection);
    SignalSet(newWebSocketConnection);

    SlotSet(buildQueuedSockets);
    SlotSet(removeSocketWithoutRequest);
    SlotSet(onRequest);
    SlotSet(onFailure);
}

SkTcpServer *SkHttpService::tcpSvr()
{
    if (ssl || !svr)
        return nullptr;

    return dynamic_cast<SkTcpServer *>(svr);
}

SkSslServer *SkHttpService::sslSvr()
{
    if (!ssl || !svr)
        return nullptr;

    return dynamic_cast<SkSslServer *>(svr);
}

SkAbstractServer *SkHttpService::absSvr()
{
    return svr;
}

bool SkHttpService::isListening()
{
    if (!svr)
        return false;

    return svr->isListening();
}

void SkHttpService::setup(CStr *serverName, bool enableSsl, uint64_t maxReceivingHeaderSize, uint32_t maxClientsIdleSeconds)
{
    ssl = enableSsl;

    if (ssl)
        svr = new SkSslServer(this);

    else
        svr = new SkTcpServer(this);

    svrName = serverName;
    maxHeaderSize = maxReceivingHeaderSize;
    maxClientsIdleSecs = maxClientsIdleSeconds;

    Attach(svr, newSvrConnection, this, buildQueuedSockets, SkAttachMode::SkQueued);
}

bool SkHttpService::start(CStr *address, uint16_t port, uint64_t maxQueuedConnections)
{
    if (ssl)
        return sslSvr()->start(address, port, maxQueuedConnections);

    return tcpSvr()->start(address, port, maxQueuedConnections);
}

void SkHttpService::stop()
{
    Detach(svr, newSvrConnection, this, buildQueuedSockets);
    clearMountPoints();

    if (ssl)
        return sslSvr()->stop();

    return tcpSvr()->stop();
}

bool SkHttpService::existsMountPoint(CStr *mpPath)
{
    return mountPoints.contains(mpPath);
}

bool SkHttpService::addMountPoint(SkGenericMountPoint *mp)
{
    CStr *mpPath = mp->getPath();

    if (mountPoints.contains(mpPath))
    {
        ObjectError("The MountPoint path ALREADY exists: " << mpPath);
        return false;
    }

    mountPoints[mpPath] = mp;
    SkMountPointType t = mp->getType();

    if (t == SkMountPointType::MP_FILESYSTEM)
        ObjectMessage("ADDED MountPoint [" << SkGenericMountPoint::mountTypeToString(t)
                  << ", FsPath: " << dynamic_cast<SkFsMountPoint *>(mp)->getRealPath()
                  << "]: " << mpPath);

    else if (t == SkMountPointType::MP_RDSTR)
        ObjectMessage("ADDED MountPoint [" << SkGenericMountPoint::mountTypeToString(t)
                  << ", Type: " << SkRedistrMountPoint::redistrTypeToString(dynamic_cast<SkRedistrMountPoint *>(mp)->getRedistrType())
                  << "]: " << mpPath);

    else if (t == SkMountPointType::MP_GENERIC)
        ObjectMessage("ADDED MountPoint [" << SkGenericMountPoint::mountTypeToString(t) << "]: " << mpPath);

    /*else if (t == SkMountPointType::MP_SVLT)
    {}*/

    return true;
}

bool SkHttpService::delMountPoint(CStr *mpPath)
{
    SkGenericMountPoint *mp = mountPoints[mpPath];
    SkMountPointType t = mp->getType();

    if (t == SkMountPointType::MP_RDSTR)
        dynamic_cast<SkRedistrMountPoint *>(mp)->close();

    mp->destroyLater();
    mountPoints.remove(mpPath);
    ObjectMessage("[" << SkGenericMountPoint::mountTypeToString(t) << "] DELETED: " << mpPath);
    return true;
}

void SkHttpService::clearMountPoints()
{
    SkBinaryTreeVisit<SkString, SkGenericMountPoint *> *itr = mountPoints.iterator();

    while(itr->next())
        delMountPoint(itr->item().key().c_str());

    delete itr;
}

void SkHttpService::sendStatusPageAndKill(SkHttpSocket *httpSck,
                                          SkHttpStatusCode code,
                                          SkCookies *cookies,
                                          SkArgsMap optionalHeaders)
{
    ObjectWarning("[" << httpSck->objectName() << "] "
              << "Killing with a status page: " << httpCodeString(code));

    Detach(httpSck, request, this, onRequest);
    Detach(httpSck, failure, this, onFailure);

    SkWebPage *page = onBuildStatusPage(code);

    if (page)
    {
        httpSck->send(page, code, cookies, optionalHeaders);
        page->destroyLater();
    }

    else
        httpSck->send(code, cookies, optionalHeaders);

    httpSck->destroyLater();
}

CStr *SkHttpService::getServerName()
{
    return svrName.c_str();
}

SkWebPage *SkHttpService::onBuildStatusPage(SkHttpStatusCode code)
{
    SkWebPage *page = new SkWebPage(this);

    SkString title = svrName;
    title.append(" [Status]");

    page->setTitle(title);

    /*if (favIconHref)
        defaultStatusPage->setFavIconHref(favIconHref);*/

    SkWuiElement *meta = new SkWuiElement;
    meta->setTag("meta");
    meta->attr("charset", "utf-8");
    page->addOptionalHeaderElement(meta);

    meta = new SkWuiElement;
    meta->setTag("meta");
    meta->attr("http-equiv", "X-UA-Compatible");
    meta->attr("content", "IE=edge");
    page->addOptionalHeaderElement(meta);
    page->build();

    SkString txt("[");
    txt.append(SkString::number(code));
    txt.append("] ");
    txt.append(httpCodeString(code));

    SkWuiElement::wui_H(nullptr, 2, txt.c_str(), page->getBody());
    return page;
}

SlotImpl(SkHttpService, buildQueuedSockets)
{
    SilentSlotArgsWarning();

    while (svr->hasQueuedSockets())
    {
        int fdClient;
        bool ok = svr->dequeueSD(fdClient);

        ObjectWarning("New Socket descriptor: " << fdClient);

        if (ok)
        {
            SkHttpSocket *httpSck = new SkHttpSocket(this);

            SkString s;
            s.append("SCK[");
            s.append(SkString::number(fdClient));
            s.append("]");

            httpSck->setObjectName(this, s.c_str());
            httpSck->setup(svrName, ssl, maxHeaderSize, maxClientsIdleSecs);

            bool ok = false;

            if (ssl)
                ok = httpSck->setFileDescriptor(fdClient, sslSvr()->getCtx());

            else
                ok = httpSck->setFileDescriptor(fdClient, nullptr);

            if (ok)
            {
                httpSck->setAsServer();

                Attach(httpSck, request, this, onRequest, SkAttachMode::SkQueued);
                Attach(httpSck, failure, this, onFailure, SkAttachMode::SkQueued);
                Attach(httpSck, disconnected, this, removeSocketWithoutRequest, SkAttachMode::SkQueued);

                ObjectMessage_EXT(httpSck,"New HTTP connection");
            }

            else
                httpSck->destroyLater();
        }
    }
}

SlotImpl(SkHttpService, removeSocketWithoutRequest)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);
    ObjectDebug("[" << httpSck->objectName() << "] deleted client without request");
    httpSck->destroyLater();
}

SlotImpl(SkHttpService, onRequest)
{
    SilentSlotArgsWarning();

    SkHttpStatusCode code = SkHttpStatusCode::NotFound;
    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);
    Detach(httpSck, request, this, onRequest);
    Detach(httpSck, disconnected, this, removeSocketWithoutRequest);

    SkString requestedPath = httpSck->url().getPath();

    ObjectMessage_EXT(httpSck, "Evaluating request -> " << requestedPath);

    if (mountPoints.contains(requestedPath.c_str()))
    {
        SkGenericMountPoint *mp = mountPoints[requestedPath];

        if (httpSck->isWebSocketRequest())
            code = acceptWebSocket(mp, httpSck);

        else
            code = acceptHttpSocket(mp, httpSck);

        if (code == SkHttpStatusCode::Ok)
            return;
    }

    SkBinaryTreeVisit<SkString, SkGenericMountPoint *> *itr = mountPoints.iterator();

    while(itr->next())
    {
        SkString &mpPath = itr->item().key();
        SkGenericMountPoint *mp = itr->item().value();

        if (mp->isPathExpandable() && requestedPath.startsWith(mpPath.c_str()))
        {
            if (mp->getType() == SkMountPointType::MP_FILESYSTEM)
            {
                if (httpSck->isWebSocketRequest())
                    code = SkHttpStatusCode::MethodNotAllowed;

                else
                    code = acceptHttpSocket(mp, httpSck);
            }

            else
            {
                if (httpSck->isWebSocketRequest())
                    code = acceptWebSocket(mp, httpSck);

                else
                    code = acceptHttpSocket(mp, httpSck);
            }

            if (code == SkHttpStatusCode::Ok)
            {
                delete itr;
                return;
            }
        }
    }

    delete itr;

    sendStatusPageAndKill(httpSck, code);
}

SkHttpStatusCode SkHttpService::acceptHttpSocket(SkGenericMountPoint *mp, SkHttpSocket *httpSck)
{
    SkHttpStatusCode code = SkHttpStatusCode::NullCode;

    if (mp->getType() == SkMountPointType::MP_RDSTR)
    {
        SkRedistrMountPoint *redistr = dynamic_cast<SkRedistrMountPoint *>(mp);
        SkRedistrType rType = redistr->getRedistrType();

        if (rType == SkRedistrType::RAW_REDISTR)
            code = dynamic_cast<SkRawRedistrMountPoint *>(redistr)->acceptSocket(httpSck);

        else if (rType == SkRedistrType::MULTIPART_REDISTR)
            code = dynamic_cast<SkPartRedistrMountPoint *>(redistr)->acceptSocket(httpSck);

        else if (rType == SkRedistrType::WS_REDISTR)
            code = SkHttpStatusCode::MethodNotAllowed;
    }

    else if (mp->getType() == SkMountPointType::MP_FILESYSTEM)
        code = dynamic_cast<SkFsMountPoint *>(mp)->acceptSocket(httpSck);

    else if (mp->getType() == SkMountPointType::MP_GENERIC)
    {
        code = SkHttpStatusCode::Ok;
        mp->acceptSocket(httpSck);
    }

    else
        code = SkHttpStatusCode::NotFound;

    if (code == SkHttpStatusCode::Ok)
        newHttpSocketConnection(httpSck);

    return code;
}

SkHttpStatusCode SkHttpService::acceptWebSocket(SkGenericMountPoint *mp, SkHttpSocket *httpSck)
{
    if (mp->getType() == SkMountPointType::MP_RDSTR && dynamic_cast<SkRedistrMountPoint *>(mp)->getRedistrType() == SkRedistrType::WS_REDISTR)
    {
        SkWebSocket *wsSck = new SkWebSocket(this);

        if (buildWebSocket(httpSck, wsSck))
        {
            dynamic_cast<SkWsRedistrMountPoint *>(mp)->acceptSocket(wsSck);
            return SkHttpStatusCode::Ok;
        }
    }

    else if (mp->getType() == SkMountPointType::MP_GENERIC)
    {
        SkWebSocket *wsSck = new SkWebSocket(this);

        if (buildWebSocket(httpSck, wsSck))
        {
            mp->acceptSocket(wsSck);
            return SkHttpStatusCode::Ok;
        }
    }

    return SkHttpStatusCode::MethodNotAllowed;
}

bool SkHttpService::buildWebSocket(SkHttpSocket *httpSck, SkWebSocket *wsSck)
{
    if (wsSck->open(httpSck))
    {
        SkString name = "WS#";
        name.append(httpSck->peerAddress());
        name.append("@");
        name.append(SkString::number(httpSck->getFileDescriptor()));
        name.append(":");
        name.append(httpSck->url().getPath());
        wsSck->setObjectName(name.c_str());

        SkString str;
        wsSck->getInternalSocket()->getHttpRequestHeaders().toString(str, true);
        ObjectMessage("WebSocket request: " << str);

        newWebSocketConnection(wsSck);
        return true;
    }

    wsSck->destroyLater();
    return false;
}

SlotImpl(SkHttpService, onFailure)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);
    sendStatusPageAndKill(httpSck, static_cast<SkHttpStatusCode>(Arg_UInt16));
}

#endif
