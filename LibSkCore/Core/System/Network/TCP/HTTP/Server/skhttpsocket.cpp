#if defined(ENABLE_HTTP)

#include "skhttpsocket.h"
#include <Core/App/skeventloop.h>
#include "Core/System/Network/skurl.h"
#include <Core/System/Filesystem/skfile.h>
#include <Core/System/Time/skdatetime.h>
#include <Core/Containers/skdatabuffer.h>
#include <Core/System/Network/LOCAL/sklocalsocket.h>

ConstructorImpl(SkHttpSocket, SkAbstractDevice)
{
    sck = nullptr;
    ssl = false;

    requestHeadersReceived = false;
    requestHeaderSize = 0;
    maxHSize = 0;
    maxIdleSecs = 0;

    postCurrentSize = 0;
    postLength = 0;

    responseSent = false;

    mpType = SkMountPointType::MP_GENERIC;
    blockSize = 0;
    dataSource = nullptr;

    SignalSet(request);
    SignalSet(failure);

    SlotSet(onReadyRead);
    SlotSet(sendingTick);
    SlotSet(svltGrabTick);
    SlotSet(oneSecTick);

    SignalSet(connected);
    SignalSet(disconnected);
    SlotSet(disconnect);

    SlotSet(onSckConnect);
    SlotSet(onSckDisconnect);
}

SkTcpSocket *SkHttpSocket::tcpSck()
{
    if (ssl || !sck)
        return nullptr;

    return dynamic_cast<SkTcpSocket *>(sck);
}

SkSslSocket *SkHttpSocket::sslSck()
{
    if (!ssl || !sck)
        return nullptr;

    return dynamic_cast<SkSslSocket *>(sck);
}

void SkHttpSocket::setup(SkString serverName, bool enableSsl, uint64_t maxHeaderSize, uint32_t maxIdleSeconds)
{
    ssl= enableSsl;

    setProperty("PROTO-TYPE", "HTTP-VOID");

    if (ssl)
    {
        sck = new SkSslSocket(this);
        sck->setObjectName(this, "SSLSCK");
    }

    else
    {
        sck = new SkTcpSocket(this);
        sck->setObjectName(this, "TCPSCK");
    }

    svrName = serverName;
    maxHSize = maxHeaderSize;
    maxIdleSecs = static_cast<double>(maxIdleSeconds);

    Attach(sck, connected, this, onSckConnect, SkAttachMode::SkQueued);
    Attach(sck, disconnected, this, onSckDisconnect, SkAttachMode::SkQueued);
    Attach(sck, readyRead, this, onReadyRead, SkAttachMode::SkQueued);

    Attach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkQueued);

    activityCheckChrono.start();
}

bool SkHttpSocket::setFileDescriptor(int fd, SSL_CTX *ctx)
{
    bool ok = false;

    if (ssl)
        ok = sslSck()->setFileDescriptor(fd, ctx);

    else
        ok = tcpSck()->setFileDescriptor(fd);

    if (ok)
        notifyState(SkDeviceState::Open);

    return ok;
}

int SkHttpSocket::getFileDescriptor()
{
    if (ssl)
        return sslSck()->getFileDescriptor();

    return tcpSck()->getFileDescriptor();
}

bool SkHttpSocket::isSslEnabled()
{
    return ssl;
}

CStr *SkHttpSocket::getServerName()
{
    return svrName.c_str();
}

CStr *SkHttpSocket::getHttpMethod()
{
    return httpMethod.c_str();
}

SkArgsMap &SkHttpSocket::getHttpRequest()
{
    return httpRequest;
}

SkArgsMap &SkHttpSocket::getHttpRequestHeaders()
{
    return requestHeaders;
}

bool SkHttpSocket::isWebSocketRequest()
{
    if (requestHeaders.contains("Connection"))
    {
        SkString connection = requestHeaders["Connection"].toString();

        return connection.contains("Upgrade")
                && requestHeaders.contains("Sec-WebSocket-Version")
                && requestHeaders.contains("Sec-WebSocket-Key")
                /* && requestHeaders.contains("Sec-WebSocket-Extensions")*/;
    }

    return false;
}

SkArgsMap &SkHttpSocket::getHttpCookies()
{
    return cookies;
}

CStr *SkHttpSocket::getUsername()
{
    return user.c_str();
}

CStr *SkHttpSocket::getPassword()
{
    return password.c_str();
}

bool SkHttpSocket::send(SkHttpStatusCode code,
                        SkCookies *cookies,
                        SkArgsMap optionalHeaders)
{
    return sendResponse(nullptr, 0, nullptr, code, cookies, optionalHeaders);
}

bool SkHttpSocket::send(SkWebPage *page,
                        SkHttpStatusCode code,
                        SkCookies *cookies,
                        SkArgsMap optionalHeaders)
{

    if (page)
    {
        SkDataBuffer buffer;
        buffer.setObjectName(this, "PageBuffer");
        page->writeOnDataBuffer(buffer, false);

        return sendResponse(buffer.data(), static_cast<int64_t>(buffer.size()), "text/html; charset=utf8", code, cookies, optionalHeaders);
    }

    return sendResponse(nullptr, 0, nullptr, code, cookies, optionalHeaders);
}

bool SkHttpSocket::send(SkString &txt,
                        CStr *contentType,
                        SkHttpStatusCode code,
                        SkCookies *cookies,
                        SkArgsMap optionalHeaders)
{
    return sendResponse(txt.c_str(), static_cast<int64_t>(txt.size()), contentType, code, cookies, optionalHeaders);
}

bool SkHttpSocket::send(CStr *data,
                        int64_t size,
                        CStr *contentType,
                        SkHttpStatusCode code,
                        SkCookies *cookies,
                        SkArgsMap optionalHeaders)
{
    return sendResponse(data, size, contentType, code, cookies, optionalHeaders);
}

bool SkHttpSocket::checkSentResponse()
{
    if (responseSent)
    {
        ObjectError("Cannot send a response; it is ALREADY responsed");
        return false;
    }

    return true;
}

bool SkHttpSocket::sendResponse(CStr *data,
                                int64_t size,
                                CStr *contentType,
                                SkHttpStatusCode code,
                                SkCookies *cookies,
                                SkArgsMap &optionalHeaders)
{
    if (!checkSentResponse())
        return false;

    SkHttpResponseHeaders header;
    header.setHttpCode(svrName.c_str(), code);

    if (contentType)
        header.setContentType(contentType);

    header.setContentLength(size);

    if (!optionalHeaders.isEmpty())
        header.append(optionalHeaders);

    bool ok = header.writeOnDevice(this, cookies, true);

    if (ok && size > 0 && data)
        ok = write(data, static_cast<uint64_t>(size));

    responseSent = true;
    flush();
    disconnect();
    return ok;
}

bool SkHttpSocket::sendFile(SkFile *f, SkFileInfo *nfo, uint64_t dataBlockSize)
{
    if (!checkSentResponse())
        return false;

    if (!f->isOpen())
    {
        ObjectError("Cannot send file; it MUST be open");
        return false;
    }

    mpType = SkMountPointType::MP_FILESYSTEM;
    dataSource = f;

    SkHttpResponseHeaders header;
    header.setHttpCode(svrName.c_str(), SkHttpStatusCode::Ok);
    header.setContentType(nfo->mimeType.c_str());
    header.setContentLength(nfo->size);

    if (nfo->mimeType == "application/octet-stream")
        header.setAttachmentName(nfo->completeName.c_str());

    if (!header.writeOnDevice(this, nullptr, true))
    {
        ObjectError("Cannot write header");
        disconnect();
        return false;
    }

    blockSize = dataBlockSize;
    responseSent = true;

    ObjectMessage("Sending file: " << nfo->completeAbsolutePath << " [" << nfo->size << " B]");
    Attach(eventLoop()->fastZone_SIG, pulse, this, sendingTick, SkAttachMode::SkQueued);
    return true;
}

bool SkHttpSocket::requestToServlet(SkMountPointType t, SkAbstractSocket *svlt, uint64_t dataBlockSize)
{
    if (!checkSentResponse())
        return false;

    if (!svlt->isConnected())
    {
        ObjectError("Cannot request for TCP-SERVLET; it MUST be connected");
        return false;
    }

    mpType = t;

    Attach(svlt, readyRead, this, svltGrabTick, SkAttachMode::SkQueued);
    Attach(eventLoop()->fastZone_SIG, pulse, this, sendingTick, SkAttachMode::SkQueued);

    SkArgsMap request = getHttpRequest();
    request["headers"] = getHttpRequestHeaders();
    request["cookies"] = getHttpCookies();

    SkString s;
    request.toString(s);
    svlt->writeUInt32(static_cast<uint32_t>(s.size()));
    svlt->write(s);

    dataSource = svlt;
    blockSize = dataBlockSize;
    responseSent = true;
    return true;
}

SlotImpl(SkHttpSocket, onReadyRead)
{
    SilentSlotArgsWarning();

    if (responseSent)
        return;

    if (!requestHeadersReceived)
    {
        bool thereIsAline = true;

        while(thereIsAline)
        {
            uint64_t sz;
            thereIsAline = canReadLine(sz);

            if (thereIsAline)
            {
                SkString line;
                read(line, sz);
                line.trim();

                if (!analyzeHeaders(line))
                    return;

                if (requestHeadersReceived)
                {
                    executeRequestMethod();
                    return;
                }
            }
        }
    }

    else if (postLength > 0)
    {
        if (capturePostInputData())
        {
            SkStringList l;
            postParser.getFieldsList(l);
            //cout << "!!!! " << l.join(", ") << "\n";
            //cout << "!!!! " << postParser.getFieldContent("username") << " " << postParser.getFieldContent("password") << "\n";
            ObjectMessage("Initializing the POST response: " << httpRequest["url"].toString());
            request(this);
            Detach(sck, readyRead, this, onReadyRead);
        }
    }
}
/*
POST /login HTTP/1.1
Host: 127.0.0.1:9001
Connection: keep-alive
Content-Length: 250
Cache-Control: max-age=0
sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Linux"
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36
Origin: http://127.0.0.1:9001
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryFtuE77E7deQBZvfa
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,* / *;q=0.8,application/signed-exchange;v=b3;q=0.7
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: http://127.0.0.1:9001/tests/test-post.html
Accept-Encoding: gzip, deflate, br
Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7

------WebKitFormBoundaryFtuE77E7deQBZvfa
Content-Disposition: form-data; name="username"

admin&£èàù
------WebKitFormBoundaryFtuE77E7deQBZvfa
Content-Disposition: form-data; name="password"

jj
------WebKitFormBoundaryFtuE77E7deQBZvfa--
*/
bool SkHttpSocket::analyzeHeaders(SkString &line)
{
    if (requestHeaderSize > maxHSize)
    {
        Detach(sck, readyRead, this, onReadyRead);
        ObjectError("Request size it TOO LONG [" << requestHeaderSize << " B], but allowed max-size is " << maxHSize << " B");

        SkVariantVector p;
        p << static_cast<uint16_t>(SkHttpStatusCode::RequestEntityTooLarge);
        failure(p);

        return false;
    }

    if (!line.isEmpty())
    {
        ObjectMessage("Analyzing line: " << line);
        requestHeaderSize += line.size();

        SkStringList tempLineParsed;
        line.split(":", tempLineParsed);

        if (tempLineParsed.count() == 1)
        {
            tempLineParsed.clear();
            line.split(" ", tempLineParsed);

            if (tempLineParsed.count() == 3)
            {
                SkString &rawUrl = tempLineParsed.at(1);

                //urlRequest = rawUrl;
                /*if (!SkUrl::decode(rawUrl.c_str(), urlRequest))
                {
                    Detach(sck, readyRead, this, onReadyRead);
                    ObjectError("Requested URL is NOT valid: " << line)

                    SkVariantVector p;
                    p << static_cast<uint16_t>(SkHttpStatusCode::BadHttpRequest);
                    failure(p);

                    return false;
                }
                */

                urlRequest.set(rawUrl.c_str());

                httpMethod = tempLineParsed.first();
                httpRequest["type"] = httpMethod;
                httpRequest["rawUrl"] = rawUrl;
                httpRequest["url"] = urlRequest.getPath();
                httpRequest["hasQuery"] = urlRequest.hasQuery();
                httpRequest["protocol"] = tempLineParsed.last();

                ObjectDebug("Requested PATH is: " << urlRequest.getPath());
            }

            else
            {
                Detach(sck, readyRead, this, onReadyRead);
                ObjectError("Request is NOT valid: " << line);

                SkVariantVector p;
                p << static_cast<uint16_t>(SkHttpStatusCode::BadHttpRequest);
                failure(p);

                return false;
            }
        }

        else if (tempLineParsed.count() >= 2)
        {
            SkString headerName = tempLineParsed.first();
            tempLineParsed.removeFirst();

            SkString headerVal;

            if (tempLineParsed.count() > 1)
                headerVal = tempLineParsed.join(":");

            else
                headerVal = tempLineParsed.last();

            headerName.trim();
            headerVal.trim();

            requestHeaders[headerName] = headerVal;

            if (headerName == "Cookie")
            {
                SkStringList cookiesParsed;
                headerVal.split(";", cookiesParsed);

                for(uint64_t i=0; i<cookiesParsed.count(); i++)
                {
                    cookiesParsed.at(i).trim();

                    SkStringList singleCookieParsed;
                    cookiesParsed.at(i).split("=", singleCookieParsed);

                    if (singleCookieParsed.count() == 2)
                        cookies[singleCookieParsed.first()] = singleCookieParsed.last();
                }

                SkString json;
                getHttpCookies().toString(json, true);
            }
        }
    }

    else
    {
        requestHeadersReceived = true;
        ObjectMessage("Request headers terminated");
    }

    return true;
}

void SkHttpSocket::executeRequestMethod()
{
    if (httpMethod == "GET")//OR HEAD
    {
        Detach(sck, readyRead, this, onReadyRead);
        ObjectMessage("Initializing the GET response: " << httpRequest["url"].toString());
        request(this);
    }

    else if (httpMethod == "POST")
    {
        //THIS CANNOT BE HAPPENs, FOR NOW
        if (!postParser.isEmpty())
            postParser.reset();

        if (requestHeaders.contains("Content-Type")
                && requestHeaders.contains("Content-Length"))
        {
            postParser.setTemporaryDirectory(osEnv()->getSystemTempPath());
            postParser.setContentType(requestHeaders["Content-Type"].toString());

            postCurrentSize = 0;
            postLength = requestHeaders["Content-Length"].toUInt32();
            //cout << "!!! " << postLength << "\n";
        }
    }

    /*else if (httpMethod == "PUT")
    {
        ObjectMessage("Initializing the PUT response: " << httpRequest["url"].toString());
        request(this);
    }*/

    else
    {
        Detach(sck, readyRead, this, onReadyRead);
        ObjectError("Http method NOT supported: " << httpMethod);

        SkVariantVector p;
        p << static_cast<uint16_t>(SkHttpStatusCode::MethodNotAllowed);
        failure(p);
    }
}

bool SkHttpSocket::capturePostInputData()
{
    if (bytesAvailable() > 0)
    {
        SkDataBuffer tempBuffer;
        read(&tempBuffer, bytesAvailable());

        //cout << "!!!! " << tempBuffer.toString() <<"\n";

        postCurrentSize += tempBuffer.size();
        postParser.grow(tempBuffer.data(), tempBuffer.size());
    }

    if (postCurrentSize == postLength)
    {
        /*request(this);
        Detach(sck, readyRead, this, onReadyRead);*/

        //if (postParser.parse())
            return true;

        ObjectError("Post parsing problems");

        SkVariantVector p;
        p << static_cast<uint16_t>(SkHttpStatusCode::InternalServerError);
        failure(p);
    }

    return false;
}

SkPostParser *SkHttpSocket::getPostParser()
{
    return &postParser;
}

SkUrl &SkHttpSocket::url()
{
    return urlRequest;
}

SlotImpl(SkHttpSocket, sendingTick)
{
    SilentSlotArgsWarning();

    if (!isConnected() || !canWrite())
        return;

    if (mpType == SkMountPointType::MP_FILESYSTEM)
    {
        uint64_t available = dataSource->bytesAvailable();

        bool ok = false;

        if (available >= blockSize)
            ok = dataSource->readHereWriteTo(this, blockSize);

        else if (available < blockSize)
            ok = dataSource->readHereWriteTo(this, available);

        if (dataSource->atEof() || !ok)
        {
            ObjectMessage("Sending file TERMINATED: " << dataSource->objectName());
            dataSource->close();
            dataSource->destroyLater();
            dataSource = nullptr;
            Detach(eventLoop()->fastZone_SIG, pulse, this, sendingTick);
            //flushWriteBuffer();
            disconnect();
        }
    }

    /*else if (mpType == SkMountPointType::MP_LSVLT || mpType == SkMountPointType::MP_TSVLT)
    {
        if (svltProduction.isEmpty())
        {
            SkAbstractSocket *svltSck = dynamic_cast<SkAbstractSocket *>(dataSource);

            if (!svltSck->isConnected())
            {
                svltSck->destroyLater();
                Detach(eventLoop()->fastZone_SIG, pulse, this, sendingTick);
                flush();
                disconnect();
            }
        }

        else
        {
            uint64_t available = svltProduction.size();

            if (available >= blockSize)
                write(&svltProduction, blockSize);

            else if (available < blockSize)
                write(&svltProduction, available);
        }
    }*/
}

SlotImpl(SkHttpSocket, svltGrabTick)
{
    SilentSlotArgsWarning();

    dataSource->read(&svltProduction);
}

SlotImpl(SkHttpSocket, oneSecTick)
{
    SilentSlotArgsWarning();

    if (activityCheckChrono.stop() >= maxIdleSecs)
    {
        if (!getRxDataCounter() && !getTxDataCounter())
        {
            ObjectWarning("Destroying because IDLE since " << maxIdleSecs << " seconds");
            disconnect();

            //if (!requestHeadersReceived)
                //destroyLater();
        }

        else
        {
            resetRxDataCounter();
            resetTxDataCounter();
        }

        activityCheckChrono.start();
    }
}

void SkHttpSocket::setAsServer()
{
    if (ssl)
        sslSck()->setAsServer();

    else
        tcpSck()->setAsServer();
}

void SkHttpSocket::setWriteBuffer(uint64_t size)
{
    if (ssl)
        sslSck()->setWriteBuffer(size);

    else
        tcpSck()->setWriteBuffer(size);
}

uint64_t SkHttpSocket::getWriteBuffer()
{
    if (ssl)
        return sslSck()->getWriteBuffer();

    return tcpSck()->getWriteBuffer();
}

void SkHttpSocket::setReadBuffer(uint64_t size)
{
    if (ssl)
        sslSck()->setReadBuffer(size);

    else
        tcpSck()->setReadBuffer(size);
}

uint64_t SkHttpSocket::getReadBuffer()
{
    if (ssl)
        return sslSck()->getReadBuffer();

    return tcpSck()->getReadBuffer();
}

bool SkHttpSocket::waitForData(uint64_t bytes)
{
    if (ssl)
        return sslSck()->waitForData(bytes);

    return tcpSck()->waitForData(bytes);
}

bool SkHttpSocket::canReadLine(uint64_t &lineLength)
{
    if (ssl)
        return sslSck()->canReadLine(lineLength);

    return tcpSck()->canReadLine(lineLength);
}

bool SkHttpSocket::canWrite()
{
    if (ssl)
        return sslSck()->canWrite();

    return tcpSck()->canWrite();
}

bool SkHttpSocket::flush()
{
    if (ssl)
        return sslSck()->flush();

    return tcpSck()->flush();
}

bool SkHttpSocket::flushReadBuffer(uint64_t size)
{
    if (ssl)
        return sslSck()->flushReadBuffer(size);

    return tcpSck()->flushReadBuffer(size);
}

bool SkHttpSocket::flushWriteBuffer()
{
    if (ssl)
        return sslSck()->flushWriteBuffer();

    return tcpSck()->flushWriteBuffer();
}

bool SkHttpSocket::isConnected()
{
    if (ssl)
        return sslSck()->isConnected();

    return tcpSck()->isConnected();
}

uint64_t SkHttpSocket::bytesAvailable()
{
    if (ssl)
        return sslSck()->bytesAvailable();

    return tcpSck()->bytesAvailable();
}

void SkHttpSocket::close()
{
    if (ssl)
        sslSck()->close();

    else
        tcpSck()->close();
}

CStr *SkHttpSocket::peerAddress()
{
    if (ssl)
        return sslSck()->peerAddress();

    return tcpSck()->peerAddress();
}

/*CStr *SkHttpSocket::peerHostName()
{
    if (ssl)
        return sslSck()->peerHostName();

    return tcpSck()->peerHostName();
}*/

SkAbstractSocket *SkHttpSocket::getInternalSocket()
{
    return sck;
}

SlotImpl(SkHttpSocket, disconnect)
{
    SilentSlotArgsWarning();

    if (ssl)
        sslSck()->disconnect(this);

    else
        tcpSck()->disconnect(this);
}

SlotImpl(SkHttpSocket, onSckConnect)
{
    SilentSlotArgsWarning();
    notifyState(SkDeviceState::Open);
    connected(this);
}

SlotImpl(SkHttpSocket, onSckDisconnect)
{
    SilentSlotArgsWarning();
    notifyState(SkDeviceState::Closed);
    disconnected(this);
}

bool SkHttpSocket::readInternal(char *data, uint64_t &len, bool onlyPeek)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = false;

    if (ssl)
        ok = sslSck()->read(data, len, onlyPeek);

    else
        ok = tcpSck()->read(data, len, onlyPeek);

    //cout << "!!R! " << len << " " << data;

    if (ok)
        notifyRead(len);

    else
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
    }

    return ok;
}

bool SkHttpSocket::writeInternal(CStr *data, uint64_t &len)
{
    if (!isConnected())
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    //cout << "!!W! " << len << " " << data;

    bool ok = false;

    if (ssl)
        ok = sslSck()->write(data, len);

    else
        ok = tcpSck()->write(data, len);

    if (ok)
        notifyWrite(len);

    else
    {
        len = 0;
        //notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);
    }

    return ok;
}

#endif
