#include "skhttpstatuscodes.h"

#if defined(ENABLE_HTTP)

CStr *httpCodeString(SkHttpStatusCode code)
{
    //INFORMATIONAL
    if (code == SkHttpStatusCode::Continue)
        return "Continue";

    else if (code == SkHttpStatusCode::Processing)
        return "Processing";

    else if (code == SkHttpStatusCode::SwitchingProtocol)
        return "Switching Protocol";

    //SUCCESS
    else if (code == SkHttpStatusCode::Ok)
        return "OK";

    else if (code == SkHttpStatusCode::Created)
        return "Created";

    else if (code == SkHttpStatusCode::Accepted)
        return "Accepted";

    else if (code == SkHttpStatusCode::NonAuthoritativeInformation)
        return "Non Authoritative Information";

    else if (code == SkHttpStatusCode::NoContent)
        return "No Content";

    else if (code == SkHttpStatusCode::ResetContent)
        return "Reset Content";

    else if (code == SkHttpStatusCode::PartialContent)
        return "Partial Content";

    else if (code == SkHttpStatusCode::MultiStatus)
        return "MultiStatus";

    else if (code == SkHttpStatusCode::AlreadyReported)
        return "Already Reported";

    else if (code == SkHttpStatusCode::ImUsed)
        return "IM Used";

    //REDIRECTION
    else if (code == SkHttpStatusCode::MultipleChoices)
        return "Multiple Choices";

    else if (code == SkHttpStatusCode::MovedPermanently)
        return "Moved Permanently";

    else if (code == SkHttpStatusCode::Found)
        return "Found";

    else if (code == SkHttpStatusCode::SeeOther)
        return "See Other";

    else if (code == SkHttpStatusCode::NotModified)
        return "Not Modified";

    else if (code == SkHttpStatusCode::UseProxy)
        return "Use Proxy";

    else if (code == SkHttpStatusCode::TemporaryRedirect)
        return "Temporary Redirect";

    else if (code == SkHttpStatusCode::PermanentlyRedirect)
        return "Permanently Redirect";

    //CLIENT-ERROR
    else if (code == SkHttpStatusCode::BadHttpRequest)
        return "BadRequest";

    else if (code == SkHttpStatusCode::Unauthorized)
        return "Unauthorized";

    else if (code == SkHttpStatusCode::PaymentRequired)
        return "Payment Required";

    else if (code == SkHttpStatusCode::Forbidden)
        return "Forbidden";

    else if (code == SkHttpStatusCode::NotFound)
        return "Not Found";

    else if (code == SkHttpStatusCode::MethodNotAllowed)
        return "Method Not Allowed";

    else if (code == SkHttpStatusCode::NotAcceptable)
        return "Not Acceptable";

    else if (code == SkHttpStatusCode::ProxyAuthenticationRequired)
        return "Proxy Authentication Required";

    else if (code == SkHttpStatusCode::RequestTimeout)
        return "Request Timeout";

    else if (code == SkHttpStatusCode::Conflict)
        return "Conflict";

    else if (code == SkHttpStatusCode::Gone)
        return "Gone";

    else if (code == SkHttpStatusCode::LengthRequired)
        return "Length Required";

    else if (code == SkHttpStatusCode::PreconditionFailed)
        return "Precondition Failed";

    else if (code == SkHttpStatusCode::RequestEntityTooLarge)
        return "Request Entity Too Large";

    else if (code == SkHttpStatusCode::RequestUriTooLong)
        return "Request URI Too Long";

    else if (code == SkHttpStatusCode::UnsupportedMediaType)
        return "Unsupported Media Type";

    else if (code == SkHttpStatusCode::RequestRangeNotSatisfable)
        return "Request Range Not Satisfable";

    else if (code == SkHttpStatusCode::ExpectationFailed)
        return "Expectation Failed";

    else if (code == SkHttpStatusCode::EnhanceYourCalm)
        return "Enhance Your Calm";

    else if (code == SkHttpStatusCode::UnprocessableEntity)
        return "Unprocessable Entity";

    else if (code == SkHttpStatusCode::Locked)
        return "Locked";

    else if (code == SkHttpStatusCode::FailedDependency)
        return "Failed Dependency";

    else if (code == SkHttpStatusCode::UpgradeRequired)
        return "UpgradeRequired";

    else if (code == SkHttpStatusCode::PreconditionRequired)
        return "Precondition Required";

    else if (code == SkHttpStatusCode::ToManyRequests)
        return "To Many Requests";

    else if (code == SkHttpStatusCode::RequestHeaderFieldsTooLarge)
        return "Request Header Fields Too Large";

    else if (code == SkHttpStatusCode::UnavailableForLegalReasons)
        return "Unavailable For Legal Reasons";

    //SERVER-ERROR
    else if (code == SkHttpStatusCode::InternalServerError)
        return "Internal Server Error";

    else if (code == SkHttpStatusCode::NotImplemented)
        return "Not Implemented";

    else if (code == SkHttpStatusCode::BadGateway)
        return "Bad Gateway";

    else if (code == SkHttpStatusCode::ServiceUnavailable)
        return "Service Unavailable";

    else if (code == SkHttpStatusCode::GatewayTimeout)
        return "Gateway Timeout";

    else if (code == SkHttpStatusCode::HttpVersionNotSupported)
        return "HTTP Version Not Supported";

    else if (code == SkHttpStatusCode::InsufficientStorage)
        return "Insufficient Storage";

    else if (code == SkHttpStatusCode::LoopDetected)
        return "Loop Detected";

    else if (code == SkHttpStatusCode::BandLimitExceeded)
        return "Band Limit Exceeded";

    else if (code == SkHttpStatusCode::NotExtended)
        return "Not Extended";

    else if (code == SkHttpStatusCode::NetworkAuthenticationRequired)
        return "Network Authentication Required";

    else if (code == SkHttpStatusCode::NetworkReadTimeout)
        return "Network Read Timeout";

    else if (code == SkHttpStatusCode::NetworkConnectTimeout)
        return "Network Connect Timeout";

    return "Unknown";
}

#endif
