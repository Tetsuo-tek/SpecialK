#ifndef SKHTTPSTATUSCODES_H
#define SKHTTPSTATUSCODES_H

#include "skdefines.h"

#if defined(ENABLE_HTTP)

enum SkHttpStatusCode
{
    NullCode,

    //INFORMATIONAL
    Continue                        =100,
    SwitchingProtocol               =101,
    Processing                      =102, //WebDAV

    //SUCCESS
    Ok                              =200,
    Created                         =201,
    Accepted                        =202,
    NonAuthoritativeInformation     =203,
    NoContent                       =204,
    ResetContent                    =205,
    PartialContent                  =206,
    MultiStatus                     =207, //WebDAV
    AlreadyReported                 =208, //WebDAV
    ImUsed                          =226,

    //REDIRECTION
    MultipleChoices                 =300,
    MovedPermanently                =301,
    Found                           =302,
    SeeOther                        =303,
    NotModified                     =304,
    UseProxy                        =305,
    TemporaryRedirect               =307,
    PermanentlyRedirect             =308,

    //CLIENT-ERROR
    BadHttpRequest                  =400,
    Unauthorized                    =401,
    PaymentRequired                 =402,
    Forbidden                       =403,
    NotFound                        =404,
    MethodNotAllowed                =405,
    NotAcceptable                   =406,
    ProxyAuthenticationRequired     =407,
    RequestTimeout                  =408,
    Conflict                        =409,
    Gone                            =410,
    LengthRequired                  =411,
    PreconditionFailed              =412,
    RequestEntityTooLarge           =413,
    RequestUriTooLong               =414,
    UnsupportedMediaType            =415,
    RequestRangeNotSatisfable       =416,
    ExpectationFailed               =417,
    EnhanceYourCalm                 =420,
    UnprocessableEntity             =422, //WebDAV
    Locked                          =423, //WebDAV
    FailedDependency                =424, //WebDAV
    UpgradeRequired                 =426,
    PreconditionRequired            =428,
    ToManyRequests                  =429,
    RequestHeaderFieldsTooLarge     =431,
    UnavailableForLegalReasons      =451,

    //SERVER-ERROR
    InternalServerError             =500,
    NotImplemented                  =501,
    BadGateway                      =502,
    ServiceUnavailable              =503,
    GatewayTimeout                  =504,
    HttpVersionNotSupported         =505,
    InsufficientStorage             =507, //WebDAV
    LoopDetected                    =508, //WebDAV
    BandLimitExceeded               =509, //Apache
    NotExtended                     =510,
    NetworkAuthenticationRequired   =511,
    NetworkReadTimeout              =598,
    NetworkConnectTimeout           =599
};

CStr *httpCodeString(SkHttpStatusCode code);

#endif

#endif // SKHTTPSTATUSCODES_H
