#include "skrawredistrmountpoint.h"

#if defined(ENABLE_HTTP)

#include <Core/System/skdeviceredistr.h>
#include "Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h"
#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

ConstructorImpl(SkRawRedistrMountPoint, SkRedistrMountPoint)
{
    rType = SkRedistrType::RAW_REDISTR;
    cacheSize = 0;
}

bool SkRawRedistrMountPoint::init(CStr *svrName, CStr *mime, SkDataBuffer *header)
{
    if (!checkPathConfigured())
        return false;

    r = new SkDeviceRedistr(this);

    if (!SkString::isEmpty(mime))
        m = mime;

    else
        m = "application/octet-stream";

    serverName = svrName;

    setHeader(header);

    ObjectMessage(SkRedistrMountPoint::redistrTypeToString(rType) << " Initizialized [mime: " << m << "; header: " << r->getHeader().size() << " B]");
    initialized = true;
    return true;
}

void SkRawRedistrMountPoint::setHeader(SkDataBuffer *header)
{
    SkHttpResponseHeaders httpHeader;
    httpHeader.setHttpCode(serverName.c_str(), SkHttpStatusCode::Ok);
    httpHeader.setContentType(m.c_str());
    httpHeader.setContentLength(-1);

    SkString hStr = httpHeader.toString();
    ULong sz = hStr.size();

    if (header)
    {
        header->prepend(hStr);
        r->setHeader(header->data(), header->size());
        sz = header->size();
    }

    else
        r->setHeader(hStr.c_str(), hStr.size());

    ObjectMessage("Header setup: " << sz << " B");
}

bool SkRawRedistrMountPoint::setMaxCacheSize(uint64_t size)
{
    if (!initialized)
    {
        ObjectError("Cannot set CacheSize because the MountPoint is NOT initialized");
        return false;
    }

    if (rType != SkRedistrType::RAW_REDISTR)
    {
        ObjectError("Only RawRedistr-MountPoints can use cache");
        return false;
    }

    SkString name = objectName();
    name.append(".CACHE");
    cache.setObjectName(name.c_str());

    cacheSize = size;
    ObjectDebug("Setup cache size to: " << cacheSize << " B");
    return true;
}

uint64_t SkRawRedistrMountPoint::getMaxCacheSize()
{
    return cacheSize;
}

uint64_t SkRawRedistrMountPoint::getCurrentCacheSize()
{
    return cache.size();
}

SkHttpStatusCode SkRawRedistrMountPoint::acceptSocket(SkHttpSocket *httpSck)
{
    if (!initialized)
    {
        ObjectError("Cannot accept because the MountPoint is NOT initialized");
        return SkHttpStatusCode::InternalServerError;
    }

    r->addDevice(httpSck);

    if (rType == SkRedistrType::RAW_REDISTR)
        sendCache(httpSck);

    SkGenericMountPoint::acceptSocket(httpSck);

    return SkHttpStatusCode::Ok;
}

void SkRawRedistrMountPoint::onRemovingTarget(SkAbstractDevice *dev)
{
    if (r->existsDevice(dev))
        r->delDevice(dev);
}

bool SkRawRedistrMountPoint::send(CStr *data, uint64_t size)
{
    if (!initialized)
    {
        ObjectError("Cannot send data because the MountPoint is NOT initialized");
        return false;
    }

    if (size == 0 || !data)
    {
        ObjectError("ZeroData to redistr (null or empty)");
        return false;
    }

    if (cacheSize)
        loadCache(data, size);

    if (!r->isEmpty())
        r->sendToAll(data, size);

    return true;
}

void SkRawRedistrMountPoint::loadCache(CStr *data, uint64_t size)
{
    while (cache.size() > cacheSize && !cacheSizes.isEmpty())
        cache.flush(cacheSizes.dequeue());

    cacheSizes.enqueue(size);
    cache.addData(data, size);
}

void SkRawRedistrMountPoint::sendCache(SkAbstractDevice *httpSck)
{
    if (cache.isEmpty())
        return;

    uint64_t sz = cache.size();
    char *d = new char [sz];
    cache.getCustomBuffer(d, sz, true);
    httpSck->write(d, sz);
    delete [] d;

    ObjectDebug("Sent stream cache to client [" << httpSck->objectName() << "]: " << sz << " B");
}

#endif
