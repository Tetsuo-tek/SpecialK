#include "skservletmountpoint.h"

#if defined(ENABLE_HTTP)

ConstructorImpl(SkServletMountPoint, SkGenericMountPoint)
{
    mpType = SkMountPointType::MP_SVLT;
}

#endif
