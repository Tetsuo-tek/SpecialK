#ifndef SKFSMOUNTPOINT_H
#define SKFSMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include <Core/System/Filesystem/skfsutils.h>
#include "skgenericmountpoint.h"

class SkFsMountPoint extends SkGenericMountPoint
{
    public:
        Constructor(SkFsMountPoint, SkGenericMountPoint);

        bool init(CStr *fsPath, uint64_t sendingBlockSize=8192);
        SkHttpStatusCode acceptSocket(SkHttpSocket *httpSock);
        CStr *getRealPath();

    private:
        SkString containerRealPath;
        SkFileInfo containerInfo;
};

#endif

#endif // SKFSMOUNTPOINT_H
