#include "skredistrmountpoint.h"

#if defined(ENABLE_HTTP)

#include <Core/Containers/skdatabuffer.h>
#include <Core/System/skdeviceredistr.h>
#include "Core/System/Network/TCP/HTTP/Server/skhttpsocket.h"

ConstructorImpl(SkRedistrMountPoint, SkGenericMountPoint)
{
    r = nullptr;
    mpType = SkMountPointType::MP_RDSTR;
    rType = SkRedistrType::NO_REDISTR;
}

void SkRedistrMountPoint::close()
{
    if (!r)
        return;

    for(uint64_t i=0; i<r->count(); i++)
        r->getDevice(i)->close();

    r->clear();
}

bool SkRedistrMountPoint::isEmpty()
{
    if (!r)
        return false;

    return r->isEmpty();
}

uint64_t SkRedistrMountPoint::count()
{
    if (!r)
        return 0;

    return r->count();
}

SkDeviceRedistr *SkRedistrMountPoint::getRedistr()
{
    return r;
}

SkRedistrType SkRedistrMountPoint::getRedistrType()
{
    return rType;
}

CStr *SkRedistrMountPoint::redistrTypeToString(SkRedistrType t)
{
    if (t == SkRedistrType::RAW_REDISTR)
        return "RawRedistr";

    else if (t == SkRedistrType::MULTIPART_REDISTR)
        return "MultiPartRedistr";

    else if (t == SkRedistrType::WS_REDISTR)
        return "WebSocketRedistr";

    return "NoRedistr";
}

SkRedistrType SkRedistrMountPoint::redistrTypeFromString(CStr *s)
{
    SkString str = s;

    if (str == "RawRedistr")
        return SkRedistrType::RAW_REDISTR;

    else if (str == "MultiPartRedistr")
        return SkRedistrType::MULTIPART_REDISTR;

    else if (str == "WebSocketRedistr")
        return SkRedistrType::WS_REDISTR;

    return SkRedistrType::NO_REDISTR;
}

#endif
