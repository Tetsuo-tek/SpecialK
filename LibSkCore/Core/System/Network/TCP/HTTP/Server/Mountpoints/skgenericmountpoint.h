#ifndef SKGENERICMOUNTPOINT_H
#define SKGENERICMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include <Core/Object/skobject.h>
#include "Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h"

class SkAbstractDevice;
class SkHttpSocket;
class SkWebSocket;

enum SkMountPointType
{
    MP_GENERIC,
    MP_FILESYSTEM,
    MP_SVLT,
    MP_RDSTR
};

class SkGenericMountPoint extends SkObject
{
    public:
        Constructor(SkGenericMountPoint, SkObject);

        bool setup(CStr *mpPath, bool mpPublic);

        //if pathExpandable==false, then the requested-path MUST be exactly as the mpPath
        //else the requested-path could be equal to mpPath only on string-start
        void setPathAsExpandable(bool value);
        bool isPathExpandable();

        CStr *getPath();
        bool isPublic();
        uint64_t getBlockSize();
        uint64_t count();
        SkMountPointType getType();

        static CStr *mountTypeToString(SkMountPointType t);
        static SkMountPointType mountTypeFromString(CStr *s);

        void acceptSocket(SkHttpSocket *httpSck);
        void acceptSocket(SkWebSocket *webSck);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);
        Slot(removeTarget);
        Signal(accepted);

    private:
        SkString path;
        bool pathExpandable;

    protected:
        bool initialized;
        SkMountPointType mpType;
        bool publicMode;
        uint64_t blockSize;
        uint64_t targetsCounter;

        bool checkPathConfigured();

        void attachFast();
        void attachSlow();
        void attachOneSec();

        void detachFast();
        void detachSlow();
        void detachOneSec();

        virtual void onFastTick(){}
        virtual void onSlowTick(){}
        virtual void onOneSecTick(){}

        virtual void onRemovingTarget(SkAbstractDevice *){}
};

#endif

#endif // SKGENERICMOUNTPOINT_H
