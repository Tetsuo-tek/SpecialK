#ifndef SKRAWREDISTRMOUNTPOINT_H
#define SKRAWREDISTRMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include "skredistrmountpoint.h"
#include <Core/Containers/skringbuffer.h>
#include <Core/Containers/skqueue.h>

class SkRawRedistrMountPoint extends SkRedistrMountPoint
{
    public:
        Constructor(SkRawRedistrMountPoint, SkRedistrMountPoint);

        bool init(CStr *svrName, CStr *mime=nullptr, SkDataBuffer *header=nullptr);
        void setHeader(SkDataBuffer *header);//It will prepend the buffer with the http header response (NOT INSERT any http headers)
        bool setMaxCacheSize(uint64_t size);
        uint64_t getMaxCacheSize();
        uint64_t getCurrentCacheSize();
        SkHttpStatusCode acceptSocket(SkHttpSocket *httpSck);
        bool send(CStr *data, uint64_t size);

     private:
        SkString serverName;
        SkRingBuffer temporaryHeaderBuffer;

        SkString m;

        uint64_t cacheSize;
        SkQueue<uint64_t> cacheSizes;
        SkRingBuffer cache;

        void onRemovingTarget(SkAbstractDevice *dev) override;

        void loadCache(CStr *data, uint64_t size);
        void sendCache(SkAbstractDevice *httpSck);
};

#endif

#endif // SKRAWREDISTRMOUNTPOINT_H
