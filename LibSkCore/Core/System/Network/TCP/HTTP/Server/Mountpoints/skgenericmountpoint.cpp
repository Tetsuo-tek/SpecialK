#include "skgenericmountpoint.h"

#if defined(ENABLE_HTTP)

#include <Core/App/skeventloop.h>
#include <Core/System/Network/TCP/HTTP/Server/skhttpsocket.h>
#include "Core/System/Network/TCP/HTTP/skwebsocket.h"

ConstructorImpl(SkGenericMountPoint, SkObject)
{
    mpType = SkMountPointType::MP_GENERIC;
    initialized = false;
    publicMode = false;
    blockSize = 0;
    targetsCounter = 0;
    pathExpandable = false;

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
    SlotSet(removeTarget);

    SignalSet(accepted);
}

bool SkGenericMountPoint::setup(CStr *mpPath, bool mpPublic)
{
    if (!path.isEmpty())
    {
        ObjectError("MountPoint path is ALREADY set");
        return false;
    }

    path = mpPath;
    publicMode = mpPublic;

    SkString name = SkGenericMountPoint::mountTypeToString(mpType);
    name.append(":");
    name.append(path);
    setObjectName(name.c_str());

    return true;
}

void SkGenericMountPoint::setPathAsExpandable(bool value)
{
    pathExpandable = value;
}

bool SkGenericMountPoint::isPathExpandable()
{
    return pathExpandable;
}

bool SkGenericMountPoint::checkPathConfigured()
{
    if (path.isEmpty())
    {
        ObjectError("MountPoint path is NOT set");
        return false;
    }

    return true;
}

CStr *SkGenericMountPoint::getPath()
{
    return path.c_str();
}

bool SkGenericMountPoint::isPublic()
{
    return publicMode;
}

uint64_t SkGenericMountPoint::getBlockSize()
{
    return blockSize;
}

uint64_t SkGenericMountPoint::count()
{
    return targetsCounter;
}

SkMountPointType SkGenericMountPoint::getType()
{
    return mpType;
}

#include "Core/System/Network/skurl.h"
void SkGenericMountPoint::acceptSocket(SkHttpSocket *httpSck)
{
    targetsCounter++;
    httpSck->setProperty("PROTO-TYPE", "HTTP-SOCKET");

    httpSck->setParent(this);
    Attach(httpSck, disconnected, this, removeTarget, SkDirect);
    ObjectMessage("ADDED ServiceSocket [T: " << httpSck->property("PROTO-TYPE").data() << "]: " << httpSck->objectName());

    SkVariantVector p;
    p << httpSck->property("PROTO-TYPE").data();

    accepted(httpSck, p);
}

void SkGenericMountPoint::acceptSocket(SkWebSocket *webSck)
{
    targetsCounter++;
    webSck->getInternalSocket()->setProperty("PROTO-TYPE", "HTTP-WEBSOCKET");
    webSck->getInternalSocket()->setParent(this);

    //onRemovetarget(dev) wants dev as HttpSocket
    Attach(webSck->getInternalSocket(), disconnected, this, removeTarget, SkDirect);
    ObjectMessage("ADDED ServiceSocket [T: " << webSck->getInternalSocket()->property("PROTO-TYPE").data() << "]: " << webSck->objectName());

    SkVariantVector p;
    p << webSck->getInternalSocket()->property("PROTO-TYPE").data();

    //ONLY HttpSocket has this property
    accepted(webSck, p);
}

void SkGenericMountPoint::attachFast()
{
    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkQueued);
}

void SkGenericMountPoint::attachSlow()
{
    Attach(eventLoop()->slowZone_SIG, pulse, this, slowTick, SkAttachMode::SkQueued);
}

void SkGenericMountPoint::attachOneSec()
{
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkQueued);
}

void SkGenericMountPoint::detachFast()
{
    Detach(eventLoop()->fastZone_SIG, pulse, this, fastTick);
}

void SkGenericMountPoint::detachSlow()
{
    Detach(eventLoop()->slowZone_SIG, pulse, this, slowTick);
}

void SkGenericMountPoint::detachOneSec()
{
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTick);
}

SlotImpl(SkGenericMountPoint, fastTick)
{
    SilentSlotArgsWarning();

    onFastTick();
}

SlotImpl(SkGenericMountPoint, slowTick)
{
    SilentSlotArgsWarning();

    onSlowTick();
}

SlotImpl(SkGenericMountPoint, oneSecTick)
{
    SilentSlotArgsWarning();

    onOneSecTick();
}

SlotImpl(SkGenericMountPoint, removeTarget)
{
    SilentSlotArgsWarning();

    targetsCounter--;
    SkAbstractDevice *dev = dynamic_cast<SkAbstractDevice *>(referer);
    onRemovingTarget(dev);

    ObjectMessage("REMOVED ServiceSocket [T: " << dev->property("PROTO-TYPE").data() << "; " << dev->objectName() << "]");

    dev->destroyLater();
}

CStr *SkGenericMountPoint::mountTypeToString(SkMountPointType t)
{
    if (t == SkMountPointType::MP_FILESYSTEM)
        return "FileSystem";

    else if (t == SkMountPointType::MP_SVLT)
        return "Servlet";

    else if (t == SkMountPointType::MP_RDSTR)
        return "Redistr";

    return "Generic";
}

SkMountPointType SkGenericMountPoint::mountTypeFromString(CStr *s)
{
    SkString str = s;

    if (str == "FileSystem")
        return SkMountPointType::MP_FILESYSTEM;

    else if (str == "Servlet")
        return SkMountPointType::MP_SVLT;

    else if (str == "Redistr")
        return SkMountPointType::MP_RDSTR;

    return SkMountPointType::MP_GENERIC;
}

#endif
