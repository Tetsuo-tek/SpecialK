#include "skwsredistrmountpoint.h"

#if defined(ENABLE_HTTP)

#include "Core/System/Network/TCP/HTTP/skwsredistr.h"

ConstructorImpl(SkWsRedistrMountPoint, SkRedistrMountPoint)
{
    rType = SkRedistrType::WS_REDISTR;
}

bool SkWsRedistrMountPoint::init()
{
    if (!checkPathConfigured())
        return false;

    r = new SkWsRedistr(this);
    initialized = true;
    return true;
}

bool SkWsRedistrMountPoint::setHeader(SkDataBuffer *header, SkWsSendingMode headerMode)
{
    if (!initialized)
    {
        ObjectError("Cannot set header because the MountPoint is NOT initialized");
        return false;
    }

    if (header && !header->isEmpty())
    {
        r->setHeader(header->data(), header->size());
        dynamic_cast<SkWsRedistr *>(r)->setHeaderMode(headerMode);
        return true;
    }

    ObjectError("Header is NOT valid (null or empty)");
    return false;
}

SkHttpStatusCode SkWsRedistrMountPoint::acceptSocket(SkWebSocket *sck)
{
    if (!initialized)
    {
        ObjectError("Cannot accept because the MountPoint is NOT initialized");
        return SkHttpStatusCode::InternalServerError;
    }

    SkGenericMountPoint::acceptSocket(sck);
    r->addDevice(sck);
    return SkHttpStatusCode::Ok;
}


void SkWsRedistrMountPoint::onRemovingTarget(SkAbstractDevice *dev)
{
    //dev is internal HttpSocket
    if (!r->isEmpty())
    {
        for(uint64_t i=0; i<r->count(); i++)
            if (dynamic_cast<SkWebSocket *>(r->getDevice(i))->getInternalSocket() == dev)
                r->delDevice(r->getDevice(i));
    }
}

bool SkWsRedistrMountPoint::send(SkWsSendingMode mode, CStr *data, uint64_t size)
{
    if (!initialized)
    {
        ObjectError("Cannot send data because the MountPoint is NOT initialized");
        return false;
    }

    if (size == 0 || !data)
    {
        ObjectError("ZeroData to redistr (null or empty)");
        return false;
    }

    if (!r->isEmpty())
    {
        SkWsRedistr *wsRedistr = dynamic_cast<SkWsRedistr *>(r);

        if (mode == SkWsSendingMode::WSM_TEXT)
            wsRedistr->sendTextToAll(data, size);

        else if (mode == SkWsSendingMode::WSM_BINARY)
            wsRedistr->sendBinaryToAll(data, size);
    }

    return true;
}

#endif
