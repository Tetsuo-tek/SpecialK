#include "skpartredistrmountpoint.h"

#if defined(ENABLE_HTTP)

#include "Core/System/Network/TCP/HTTP/skmultipartredistr.h"

ConstructorImpl(SkPartRedistrMountPoint, SkRawRedistrMountPoint)
{
    rType = SkRedistrType::MULTIPART_REDISTR;
}

bool SkPartRedistrMountPoint::init(CStr *svrName, CStr *mime)
{
    if (!checkPathConfigured())
        return false;

    r = new SkMultipartRedistr(this);

    dynamic_cast<SkMultipartRedistr *>(r)->init(svrName, mime);
    initialized = true;
    return true;
}

#endif

