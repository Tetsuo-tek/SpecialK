#ifndef SKWSREDISTRMOUNTPOINT_H
#define SKWSREDISTRMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include "skredistrmountpoint.h"
#include "Core/System/Network/TCP/HTTP/skwebsocket.h"

class SkWsRedistrMountPoint extends SkRedistrMountPoint
{
    public:
        Constructor(SkWsRedistrMountPoint, SkRedistrMountPoint);

        bool init();
        bool setHeader(SkDataBuffer *header, SkWsSendingMode headerMode);

        SkHttpStatusCode acceptSocket(SkWebSocket *sck);
        bool send(SkWsSendingMode mode, CStr *data, uint64_t size);

    private:
       void onRemovingTarget(SkAbstractDevice *dev);

       void sendData(SkWsSendingMode m);
};

#endif

#endif // SKWSREDISTRMOUNTPOINT_H
