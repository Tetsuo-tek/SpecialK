#ifndef SKREDISTRMOUNTPOINT_H
#define SKREDISTRMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include "skgenericmountpoint.h"

class SkDataBuffer;
class SkDeviceRedistr;
class SkAbstractDevice;

enum SkRedistrType
{
    NO_REDISTR,
    RAW_REDISTR,
    MULTIPART_REDISTR,
    WS_REDISTR
};

class SkRedistrMountPoint extends SkGenericMountPoint
{
    public:
        Constructor(SkRedistrMountPoint, SkGenericMountPoint);

        void close();

        bool isEmpty();
        uint64_t count();

        SkDeviceRedistr *getRedistr();
        SkRedistrType getRedistrType();

        static CStr *redistrTypeToString(SkRedistrType t);
        static SkRedistrType redistrTypeFromString(CStr *s);

    protected:
        SkRedistrType rType;
        SkDeviceRedistr *r;
};

#endif

#endif // SKREDISTRMOUNTPOINT_H
