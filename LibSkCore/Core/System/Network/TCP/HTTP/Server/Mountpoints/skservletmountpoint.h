#ifndef SKSERVLETMOUNTPOINT_H
#define SKSERVLETMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include "skgenericmountpoint.h"

typedef enum
{
    SVLT_NOTYPE,
    SVLT_LOCAL,
    SVLT_TCP
} SkServletType;

class SkServletMountPoint extends SkGenericMountPoint
{
    public:
        Constructor(SkServletMountPoint, SkGenericMountPoint);

    private:
        SkServletType svltType;
};

#endif

#endif // SKSERVLETMOUNTPOINT_H
