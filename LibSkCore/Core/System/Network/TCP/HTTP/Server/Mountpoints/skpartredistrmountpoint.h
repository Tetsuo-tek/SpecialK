#ifndef SKPARTREDISTRMOUNTPOINT_H
#define SKPARTREDISTRMOUNTPOINT_H

#if defined(ENABLE_HTTP)

#include "skrawredistrmountpoint.h"

class SkPartRedistrMountPoint extends SkRawRedistrMountPoint
{
    public:
        Constructor(SkPartRedistrMountPoint, SkRawRedistrMountPoint);
        bool init(CStr *svrName, CStr *mime);
};

#endif

#endif // SKPARTREDISTRMOUNTPOINT_H
