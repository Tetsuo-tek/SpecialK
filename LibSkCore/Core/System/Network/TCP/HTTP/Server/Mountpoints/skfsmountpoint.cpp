#include "skfsmountpoint.h"

#if defined(ENABLE_HTTP)

#include <Core/System/Filesystem/skfile.h>
#include <Core/System/Network/TCP/HTTP/Server/skhttpsocket.h>

ConstructorImpl(SkFsMountPoint, SkGenericMountPoint)
{
    setPathAsExpandable(true);
    mpType = SkMountPointType::MP_FILESYSTEM;
    blockSize = 0;
}

bool SkFsMountPoint::init(CStr *fsPath, uint64_t sendingBlockSize)
{
    if (!checkPathConfigured())
        return false;

    if (!containerRealPath.isEmpty())
    {
        ObjectError("FileSystem path is ALREADY set: " << fsPath);
        return false;
    }

    SkFsUtils::fillFileInfo(fsPath, containerInfo);

    if (!containerInfo.exists)
    {
        containerInfo = SkFileInfo();
        ObjectError("FileSystem path does NOT exist: " << fsPath);
        return false;
    }

    if (!containerInfo.isDir)
    {
        containerInfo = SkFileInfo();
        ObjectError("FileSystem path is NOT a directory: " << fsPath);
        return false;
    }

    if (!containerInfo.isReadable)
    {
        containerInfo = SkFileInfo();
        ObjectError("FileSystem path is NOT readable: " << fsPath);
        return false;
    }

    containerRealPath = SkFsUtils::adjustPathEndSeparator(fsPath);
    blockSize = sendingBlockSize;
    initialized = true;
    return true;
}

SkHttpStatusCode SkFsMountPoint::acceptSocket(SkHttpSocket *httpSck)
{
    if (!initialized)
    {
        ObjectError("Cannot accept because the MountPoint is NOT initialized");
        return SkHttpStatusCode::InternalServerError;
    }

    CStr *mpPath = getPath();

    SkString requestedPath = httpSck->url().getPath();
    SkString requestedRealPath = requestedPath;

    requestedRealPath.replace(mpPath, containerRealPath.c_str(), 0, true);

    if (requestedRealPath.endsWith("/") && SkFsUtils::isDir(requestedRealPath.c_str()))
        requestedRealPath.append("index.html");

    SkFileInfo requestedFileInfo;
    SkFsUtils::fillFileInfo(requestedRealPath.c_str(), requestedFileInfo);

    if (requestedFileInfo.exists
            && requestedFileInfo.isRegularFile
            && requestedFileInfo.isReadable)
    {
        SkFile *f = new SkFile(httpSck);
        f->setFilePath(requestedRealPath);

        if (!f->open(SkFileMode::FLM_ONLYREAD)
                || !httpSck->sendFile(f, &requestedFileInfo, blockSize))
        {
            return SkHttpStatusCode::InternalServerError;
        }

        SkGenericMountPoint::acceptSocket(httpSck);
        return SkHttpStatusCode::Ok;
    }

    if (!requestedFileInfo.exists)
        return SkHttpStatusCode::NotFound;

    return SkHttpStatusCode::MethodNotAllowed;
}

CStr *SkFsMountPoint::getRealPath()
{
    return containerRealPath.c_str();
}

#endif
