/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKPOSTPARSER_H
#define SKPOSTPARSER_H

#if defined(ENABLE_HTTP)

#include <Core/Containers/skmap.h>
#include <Core/Containers/skstringlist.h>
#include <Core/Containers/skstring.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// THIS IS BULLSHIT; IT MUST BE RE-WRITE, OF COURSE
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkPostFilesPosition
{
    PFP_NOPOSITION,
    StoreUploadedFilesInFilesystem,
    StoreUploadedFilesInMemory
};

enum SkPostFieldType
{
    PFT_NOTYPE,
    TextType,
    FileType
};

class SkPostField extends SkFlatObject
{
    public:
        SkPostField();
        ~SkPostField();

        bool setType(SkPostFieldType type);
        SkPostFieldType getType();

        bool acceptSomeData(char *data, uint64_t length);

        // File functions
        void setUploadedFilesStorage(SkPostFilesPosition where);
        void setTempDir(SkString dir);

        void setFileName(SkString name);
        SkString getFileName();

        void setFileMimeType(SkString type);
        SkString getFileMimeType();

        char *getFileContent();
        int64_t getFileContentSize();

        SkString getTempFileName();
        SkString getTextTypeContent();

    private:
        unsigned long fieldContentLength;

        SkPostFilesPosition whereToStoreUploadedFiles;

        SkString tempDir;
        SkString tempFile;
        SkString fileContentType;
        SkString fileName;

        SkPostFieldType type;
        char * fieldContent;
        std::ofstream file;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkPostParserStatus
{
    PST_NOSTATUS,
    Status_LookingForStartingBoundary,
    Status_ProcessingHeaders,
    Status_ProcessingContentOfTheField
};

class SkDataBuffer;

class SPECIALK SkPostParser extends SkFlatObject
{
    public:
        SkPostParser();
        ~SkPostParser();

        void setTemporaryDirectory(SkString dirPathName);

        //parsing-buffer MAX-ALLOWED (for files instead buffer is not used, putting data directly inside a tempFile)
        void setMaxParsingBufferLength(uint64_t maxSize);
        bool setContentType(SkString contentType);

        bool grow(CStr *data, uint64_t length);

        bool isEmpty();
        ULong count();
        void getFieldsList(SkStringList &fieldNames);
        bool existsField(SkString fieldName);
        bool isFile(SkString fieldName);
        SkString getFieldContent(SkString fieldName);
        int64_t getFieldSize(SkString fieldName);

        void reset();

    private:
        SkPostFilesPosition whereToStoreUploadedFiles;

        SkMap<SkString, SkPostField *> fields;

        SkString tempDir;
        int currentStatus;

        SkString boundary;
        SkString processingFieldName;
        bool headersOfTheFieldAreProcessed;
        char *dataCollector;
        uint64_t dataCollectorLength;
        uint64_t maxDataCollectorLength;

        bool findStartingBoundaryAndTruncData();
        void processData();
        bool parseHeaders(SkString headers);
        bool waitForHeadersEndAndParseThem();
        void truncateDataCollectorFromTheBeginning(uint64_t n);
        int64_t boundaryPositionInDataCollector();
        bool processContentOfTheField();

        SkPostField *getRawField(SkString fieldName);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKPOSTPARSER_H
