#include "skpostparser.h"

#if defined(ENABLE_HTTP)

#include <Core/sklogmachine.h>
#include <Core/Containers/skarraycast.h>
#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkPostField::SkPostField()
{
    type = SkPostFieldType::PFT_NOTYPE;
    fieldContent = nullptr;
    fieldContentLength = 0;
}

SkPostField::~SkPostField()
{
    if (fieldContent)
        delete fieldContent;

    if (type == SkPostFieldType::FileType)
    {
        if (file.is_open())//CHANGED!
            file.close();

        SkString s = tempDir;
        s.append("/");
        s.append(tempFile);
        SkFsUtils::rm(s.c_str());//CHANGED!
    }
}

bool SkPostField::setType(SkPostFieldType type)
{
    if ((type == SkPostFieldType::TextType) || (type == SkPostFieldType::FileType))
    {
        this->type = type;
        return true;
    }

    FlatError("Trying to set type of field, but type is NOT correct");
    return false;
}

SkPostFieldType SkPostField::getType()
{
    if (type <= 0)
    {
        FlatError("Trying to get type of field, but the type was NOT set");
        return SkPostFieldType::PFT_NOTYPE;
    }

    return type;
}

bool SkPostField::acceptSomeData(char *data, uint64_t length)
{
    if (type == TextType)
    {
        if (fieldContent == nullptr)
            fieldContent = new char[length + 1];

        else
            fieldContent = SkArrayCast::toChar(realloc(fieldContent, fieldContentLength + length + 1));

        memcpy(fieldContent + fieldContentLength, data, length);
        fieldContentLength += length;

        fieldContent[fieldContentLength] = 0;
    }

    else if (type == SkPostFieldType::FileType)
    {
        if (whereToStoreUploadedFiles == SkPostFilesPosition::StoreUploadedFilesInFilesystem)
        {
            if (tempDir.length() > 0)
            {
                if (!file.is_open())
                {
                    int i = 1;
                    std::ifstream testfile;
                    SkString tempfile;

                    do
                    {
                        if (testfile.is_open())
                            testfile.close();

                        std::stringstream ss;
                        ss << "MPFD_Temp_" << i;
                        tempFile = ss.str();

                        tempfile = tempDir;
                        tempfile.append("/");
                        tempfile.append(tempFile);

                        testfile.open(tempfile.c_str(), std::ios::in);
                        i++;
                    } while (testfile.is_open());

                    file.open(tempfile.c_str(), std::ios::out | std::ios::binary | std::ios_base::trunc);
                }

                if (file.is_open())
                {
                    file.write(data, static_cast<int64_t>(length));
                    file.flush();
                }

                else
                {
                    FlatError("Cannot write to file " << tempDir << tempFile);
                    return false;
                }
            }

            else
            {
                FlatError("Trying to AcceptSomeData for a file but TempDir is NOT set");
                return false;
            }
        }

        else
        { // If files are stored in memory
            if (fieldContent == nullptr)
                fieldContent = new char[length];

            else
                fieldContent = SkArrayCast::toChar(realloc(fieldContent, fieldContentLength + length));

            memcpy(fieldContent + fieldContentLength, data, length);
            fieldContentLength += length;
        }
    }

    else
    {
        FlatError("Trying to AcceptSomeData but the type was NOT set");
        return false;
    }

    return true;
}

void SkPostField::setTempDir(SkString dir)
{
    tempDir = SkFsUtils::adjustPathEndSeparator(dir.c_str());
}

int64_t SkPostField::getFileContentSize()
{
    if (type > 0)
    {
        if (type == SkPostFieldType::FileType)
        {
            if (whereToStoreUploadedFiles == SkPostFilesPosition::StoreUploadedFilesInMemory)
                return static_cast<int64_t>(fieldContentLength);

            else
            {
                SkString s = tempDir;
                s.append("/");
                s.append(tempFile);
                return SkFsUtils::size(s.c_str());//CHANGED!
            }
        }

        else
        {
            FlatError("Trying to get file content size, but the type is not file");
            return -1;
        }
    }

    FlatError("Trying to get file content size, but the type was NOT set");
    return -1;
}

char *SkPostField::getFileContent()
{
    if (type != SkPostFieldType::PFT_NOTYPE)
    {
        if (type == SkPostFieldType::FileType)
        {
            if (whereToStoreUploadedFiles == SkPostFilesPosition::StoreUploadedFilesInMemory)
                return fieldContent;

            else
            {
                FlatError("Trying to get file content, but uploaded files are stored in filesystem");
                return nullptr;
            }
        }

        else
        {
            FlatError("Trying to get file content, but the type is NOT file");
            return nullptr;
        }
    }

    FlatError("Trying to get file content, but the type was NOT set");
    return nullptr;
}

SkString SkPostField::getTextTypeContent()
{
    if (type != SkPostFieldType::PFT_NOTYPE)
    {
        if (type != TextType)
        {
            FlatError("Trying to get content of the field, but the type is NOT text");
            return "";
        }

        else
        {
            if (fieldContent == nullptr)
                return "";

            else
                return fieldContent;
        }
    }

    FlatError("Trying to get text content of the field, but the type was NOT set");
    return "";
}

SkString SkPostField::getTempFileName()
{
    if (type > 0)
    {
        if (type == SkPostFieldType::FileType)
        {
            if (whereToStoreUploadedFiles == SkPostFilesPosition::StoreUploadedFilesInFilesystem)
                return SkString(tempDir + tempFile);

            else
            {
                FlatError("Trying to get file temp name, but uplaoded files are stored in memory");
                return "";
            }
        }

        else
        {
            FlatError("Trying to get file temp name, but the type is NOT file");
            return "";
        }
    }

    FlatError("Trying to get file temp name, but the type was NOT set");
    return "";
}

SkString SkPostField::getFileName()
{
    if (type > 0)
    {
        if (type == SkPostFieldType::FileType)
            return fileName;

        else
        {
            FlatError("Trying to get file name, but the type is NOT file");
            return "";
        }
    }

    FlatError("Trying to get file name, but the type was NOT set");
    return "";
}

void SkPostField::setFileName(SkString name)
{
    fileName = name;
}

void SkPostField::setUploadedFilesStorage(SkPostFilesPosition where)
{
    whereToStoreUploadedFiles = where;
}

void SkPostField::setFileMimeType(SkString type)
{
    fileContentType = type;
}

SkString SkPostField::getFileMimeType()
{
    if (type > 0)
    {
        if (type != SkPostFieldType::FileType)
        {
            FlatError("Trying to get mime type of the field, but the type is NOT File");
            return "";
        }

        else
            return fileContentType;
    }

    FlatError("Trying to get mime type of file, but the type was NOT set");
    return "";
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkPostParser::SkPostParser()
{
    dataCollector = nullptr;
    dataCollectorLength = 0;
    headersOfTheFieldAreProcessed = false;
    currentStatus = SkPostParserStatus::Status_LookingForStartingBoundary;
    maxDataCollectorLength = 16 * 1024 * 1024; // 16 Mb default data collector size.
    whereToStoreUploadedFiles = SkPostFilesPosition::StoreUploadedFilesInFilesystem;
}

SkPostParser::~SkPostParser()
{
    reset();
}

void SkPostParser::setTemporaryDirectory(SkString dirPathName)
{
    tempDir = dirPathName;
}

void SkPostParser::setMaxParsingBufferLength(uint64_t maxSize)
{
    maxDataCollectorLength = maxSize;
}

bool SkPostParser::setContentType(SkString contentType)
{
    if (contentType.contains("\""))
        contentType.replace("\"", "");

    if (contentType.find("multipart/form-data;") != 0)
    {
        FlatError("Content type is not \"multipart/form-data\"\nIt is \"" << contentType << "\"");
        return false;
    }

    uint64_t bp = contentType.find("boundary=");

    if (bp == SkString::npos)
    {
        FlatError("Cannot find boundary in Content-type: \"" << contentType << "\"");
        return false;
    }

    boundary = SkString("--") + contentType.substr(bp + 9, contentType.length() - bp);
    return true;
}

bool SkPostParser::grow(CStr *data, uint64_t length)
{
    if (boundary.length() > 0)
    {
        // Append data to existing accumulator
        if (dataCollector == nullptr)
        {
            dataCollector = new char[length];
            memcpy(dataCollector, data, length);
            dataCollectorLength = length;
        }

        else
        {
            dataCollector = SkArrayCast::toChar(realloc(dataCollector, dataCollectorLength + length));
            memcpy(dataCollector + dataCollectorLength, data, length);
            dataCollectorLength += length;
        }

        if (dataCollectorLength > maxDataCollectorLength)
        {
            FlatError("Maximum data collector length reached");
            return false;
        }

        processData();
        return true;
    }

    FlatError("Accepting data, but content type was NOT set");
    return false;
}

void SkPostParser::reset()
{
    SkVector<SkPostField *> f;
    fields.values(f);

    for(uint64_t i=0; i<f.count(); i++)
        delete f[i];

    if (dataCollector)
        delete dataCollector;
}

void SkPostParser::processData()
{
    // If some data left after truncate, process it right now.
    // Do not wait for AcceptSomeData called again
    bool needToRepeat;

    do
    {
        needToRepeat = false;

        if (currentStatus == SkPostParserStatus::Status_LookingForStartingBoundary)
        {
            if (findStartingBoundaryAndTruncData())
            {
                currentStatus = SkPostParserStatus::Status_ProcessingHeaders;
                needToRepeat = true;
            }
        }

        else if (currentStatus == SkPostParserStatus::Status_ProcessingHeaders)
        {
            if (waitForHeadersEndAndParseThem())
            {
                currentStatus = SkPostParserStatus::Status_ProcessingContentOfTheField;
                needToRepeat = true;
            }
        }

        else if (currentStatus == SkPostParserStatus::Status_ProcessingContentOfTheField)
        {
            if (processContentOfTheField())
            {
                currentStatus = SkPostParserStatus::Status_LookingForStartingBoundary;
                needToRepeat = true;
            }
        }
    } while (needToRepeat);
}

bool SkPostParser::findStartingBoundaryAndTruncData()
{
    int64_t n = boundaryPositionInDataCollector();

    if (n >= 0)
    {
        truncateDataCollectorFromTheBeginning(static_cast<uint64_t>(n) + boundary.length());
        return true;
    }

    return false;
}

bool SkPostParser::waitForHeadersEndAndParseThem()
{
    for (uint64_t i=0; i<dataCollectorLength-3; i++)
    {
        if (memcmp(&dataCollector[i], "\r\n\r\n", 4) == 0)
        {
            uint64_t headers_length = i;
            char *headers = new char [headers_length + 1];

            memset(headers, 0, headers_length + 1);
            memcpy(headers, dataCollector, headers_length);

            parseHeaders(headers);
            truncateDataCollectorFromTheBeginning(i + 4);

            delete [] headers;
            return true;
        }
    }

    return false;
}

void SkPostParser::truncateDataCollectorFromTheBeginning(uint64_t n)
{
    uint64_t truncatedDataCollectorLength = dataCollectorLength - n;

    char *tmp = dataCollector;
    dataCollector = new char[truncatedDataCollectorLength];
    memcpy(dataCollector, tmp + n, truncatedDataCollectorLength);
    dataCollectorLength = truncatedDataCollectorLength;
    delete tmp;
}

bool SkPostParser::processContentOfTheField()
{
    int64_t boundaryPosition = boundaryPositionInDataCollector();
    uint64_t dataLengthToSendToField;

    if (boundaryPosition >= 0)
    {
        // 2 is the \r\n before boundary we do not need them
        dataLengthToSendToField = static_cast<uint64_t>(boundaryPosition) - 2;
    }

    else
    {
        // We need to save +2 chars for \r\n chars before boundary
        dataLengthToSendToField = dataCollectorLength - (boundary.size() + 2);
    }

    if (dataLengthToSendToField > 0)
    {
        fields.value(processingFieldName)->acceptSomeData(dataCollector, dataLengthToSendToField);
        truncateDataCollectorFromTheBeginning(dataLengthToSendToField);
    }

    if (boundaryPosition >= 0)
    {
        currentStatus = Status_LookingForStartingBoundary;
        return true;
    }

    else
        return false;
}

int64_t SkPostParser::boundaryPositionInDataCollector()
{
    CStr *b = boundary.c_str();
    uint64_t bl = boundary.size();

    if (dataCollectorLength >= bl)
    {
        bool found = false;

        for (uint64_t i=0; (i<=dataCollectorLength-bl) && !found; i++)
        {
            found = true;

            for (uint64_t j=0; (j < bl) && (found); j++)
                if (dataCollector[i + j] != b[j])
                    found = false;

            if (found)
                return static_cast<int64_t>(i);
        }
    }

    return -1;
}

bool SkPostParser::parseHeaders(SkString headers)
{
    // Check if it is form data
    if (headers.find("Content-Disposition: form-data;") == SkString::npos)
    {
        FlatError("Accepted headers of field does not contain \"Content-Disposition: form-data;\". The headers are: \"" << headers << "\"");
        return false;
    }

    // Find name
    uint64_t name_pos = headers.find("name=\"");

    if (name_pos == SkString::npos)
    {
        FlatError("Accepted headers of field does not contain \"name=\". The headers are: \"" << headers << "\"");
        return false;
    }

    else
    {
        uint64_t name_end_pos = headers.find("\"", name_pos + 6);

        if (name_end_pos == SkString::npos)
        {
            FlatError("Cannot find closing quote of \"name=\" attribute. The headers are: \"" << headers << "\"");
            return false;
        }

        else
        {
            processingFieldName = headers.substr(name_pos + 6, name_end_pos - (name_pos + 6));
            fields.add(processingFieldName, new SkPostField());
        }

        // find filename if exists
        uint64_t filename_pos = headers.find("filename=\"");
        SkPostField *f = fields.value(processingFieldName);

        if (filename_pos == SkString::npos)
            f->setType(SkPostFieldType::TextType);

        else
        {
            f->setType(SkPostFieldType::FileType);
            f->setTempDir(tempDir);
            f->setUploadedFilesStorage(whereToStoreUploadedFiles);

            uint64_t filename_end_pos = headers.find("\"", filename_pos + 10);

            if (filename_end_pos == SkString::npos)
            {
                FlatError("Cannot find closing quote of \"filename=\" attribute. The headers are: \"" << headers << "\"");
                return false;
            }

            else
            {
                SkString filename = headers.substr(filename_pos + 10, filename_end_pos - (filename_pos + 10));
                f->setFileName(filename);
            }

            // find Content-Type if exists
            uint64_t content_type_pos = headers.find("Content-Type: ");

            if (content_type_pos != SkString::npos)
            {
                uint64_t content_type_end_pos = 0;

                for (uint64_t i = content_type_pos + 14; (i < headers.length()) && (!content_type_end_pos); i++)
                    if ((headers[i] == ' ') || (headers[i] == 10) || (headers[i] == 13))
                        content_type_end_pos = i - 1;

                SkString content_type = headers.substr(content_type_pos + 14, content_type_end_pos - (content_type_pos + 14));
                f->setFileMimeType(content_type);
            }
        }
    }

    return true;
}

bool SkPostParser::isEmpty()
{
    return fields.isEmpty();
}

ULong SkPostParser::count()
{
    return fields.count();
}

void SkPostParser::getFieldsList(SkStringList &fieldNames)
{
    fields.keys(fieldNames);
}

bool SkPostParser::existsField(SkString fieldName)
{
    return fields.contains(fieldName);
}

SkPostField *SkPostParser::getRawField(SkString fieldName)
{
    SkPostField *tempRawField = nullptr;

    if (fields.contains(fieldName))
        tempRawField = fields.value(fieldName);

    return tempRawField;
}

bool SkPostParser::isFile(SkString fieldName)
{
    return (getRawField(fieldName)->getType() == SkPostFieldType::FileType);
}

SkString SkPostParser::getFieldContent(SkString fieldName)
{
    SkPostField *tempRawField = getRawField(fieldName);

    if (tempRawField->getType() == SkPostFieldType::TextType)
        return tempRawField->getTextTypeContent();

    return tempRawField->getTempFileName();
}

int64_t SkPostParser::getFieldSize(SkString fieldName)
{
    SkPostField *tempRawField = getRawField(fieldName);

    if (tempRawField->getType() == SkPostFieldType::TextType)
        return static_cast<int64_t>(tempRawField->getTextTypeContent().size());

    return tempRawField->getFileContentSize();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
