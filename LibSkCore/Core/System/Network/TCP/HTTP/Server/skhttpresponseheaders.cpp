#include "skhttpresponseheaders.h"

#if defined(ENABLE_HTTP)

#include <Core/System/skabstractdevice.h>
#include "Core/Containers/HTML/skwebpage.h"
#include "Core/System/Network/TCP/HTTP/skcookies.h"

SkHttpResponseHeaders::SkHttpResponseHeaders()
{
    responseCode = SkHttpStatusCode::NullCode;
    insert("Connection", "close");
    insert("Cache-Control", "no-store,no-cache");
    //COULD BE ALSO insert("Cache-Control", "public");
}

void SkHttpResponseHeaders::setHttpCode(CStr *svrName, SkHttpStatusCode httpCode, CStr *codeDesc)
{
    responseCode = httpCode;
    responseDesc.clear();

    if (SkString::isEmpty(codeDesc))
    {
        if (responseCode == SkHttpStatusCode::Ok)
            responseDesc = httpCodeString(responseCode);

        else if (SkString::isEmpty(codeDesc))
            responseDesc = httpCodeString(httpCode);
    }

    else
        responseDesc = codeDesc;

    if (!SkString::isEmpty(svrName))
        insert("Server", svrName);
}

void SkHttpResponseHeaders::setupAsRedirect(CStr *location)
{
    responseCode = SkHttpStatusCode::Found;
    responseDesc = httpCodeString(SkHttpStatusCode::Found);
    insert("Location", location);
}

void SkHttpResponseHeaders::setContentType(CStr *value)
{
    insert("Content-Type", value);
}

void SkHttpResponseHeaders::setContentLength(int64_t value)
{
    insert("Content-Length", value);
}

void SkHttpResponseHeaders::setAttachmentName(CStr *value)
{
    SkString s("attachment; filename=\"");
    s.append(value);
    s.append("\"");
    insert("Content-Disposition", s.c_str());
}

void SkHttpResponseHeaders::setConnection(CStr *value)
{
    insert("Connection", value);
}

void SkHttpResponseHeaders::setCacheControl(CStr *value)
{
    insert("Cache-Control", value);
}


void SkHttpResponseHeaders::setUpgrade(CStr *value)
{
    insert("Upgrade", value);
}

void SkHttpResponseHeaders::setWsAccept(CStr *value)
{
    insert("Sec-WebSocket-Accept", value);
}

void SkHttpResponseHeaders::setWsProtocol(CStr *value)
{
    insert("Sec-WebSocket-Protocol", value);
}

void SkHttpResponseHeaders::setWsExt(CStr *value)
{
    insert("Sec-WebSocket-Extensions", value);
}

void SkHttpResponseHeaders::append(SkArgsMap &headers)
{
    insertMap(headers);
}

/*bool SkHttpResponseHeader::setOnWebPage(SkWebPage *page)
{
    if (!responseCode)
    {
        FlatError("HTTP response-code is REQUIRED to set WebPage-headers");
        return false;
    }

    page->clearHttpHeadersList();

    SkString header("HTTP/1.1 ");
    header.append(SkString::number(responseCode));
    header.append(" ");
    header.append(responseDesc);

    page->addHttpHeader(header);

    SkStringList k;
    keys(k);

    for(uint64_t i=0; i<k.count(); i++)
    {
        header = k.at(i);
        header.append(": ");
        header.append(value(k.at(i)).toString());

        page->addHttpHeader(header);
    }

    return true;
}*/

SkString SkHttpResponseHeaders::toString(SkCookies *cookies, bool terminateHeaders)
{
    SkDateTime dt(true);
    dt.setToCurrent();
    SkString s = dt.toString("%a, %d %b %Y %H:%M:%S %Z");
    insert("Date", s);

    SkString httpHeader;

    if (responseCode)
    {
        httpHeader.append("HTTP/1.1 ");
        httpHeader.append(SkString::number(responseCode));
        httpHeader.append(" ");
        httpHeader.append(responseDesc);
        httpHeader.append("\r\n");
    }

    SkStringList k;
    keys(k);

    for(uint64_t i=0; i<k.count(); i++)
    {
        httpHeader.append(k.at(i));
        httpHeader.append(": ");
        httpHeader.append(value(k.at(i)).toString());
        httpHeader.append("\r\n");
    }

    if (cookies)
    {
        SkStringList cookiesList;
        cookies->produceResponseHeaders(cookiesList);
        httpHeader.append(cookiesList.join("\r\n"));
        httpHeader.append("\r\n");
    }

    if (terminateHeaders)
        httpHeader += "\r\n";

    //cout << httpHeader;

    return httpHeader;
}


bool SkHttpResponseHeaders::writeOnDevice(SkAbstractDevice *device, SkCookies *cookies, bool terminateHeaders)
{
    SkString s = toString(cookies, terminateHeaders);
    return device->write(s);
}


#endif
