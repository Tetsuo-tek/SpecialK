/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTCPSERVER_H
#define SKTCPSERVER_H

#include "Core/System/Network/skabstractserver.h"
#include "Core/System/Network/TCP/skflattcpserver.h"

#include "sktcpsocket.h"

class SPECIALK SkTcpServer extends SkAbstractServer
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkTcpServer, SkAbstractServer);

        /**
         * @brief Opens the server, starting to listen for clients
         * @param address the address to use as server;
         * for example, using 0.0.0.0 it will listen on all network interfaces,
         * instead using 127.0.0.1 it will listen only on the loopback interface
         * @param port the TCP port to open
         * @param maxQueuedConnections max number of connection that this server
         * can store as pending file-descriptors
         * @return true if there are not errors, otherwise false
         */
        bool start(CStr *address, uint16_t port, uint64_t maxQueuedConnections);

        /**
         * @brief Closes the server listening
         */
        void stop();

        /**
         * @brief Enables the experimental feature that permits to have some
         * resiliency attributes during a DDOS attack
         * @param value if true it will be enabled, otherwise not
         * @warning does not using this feature because it is incomplete in its
         * code port from previous code not using SpecialK
         */
        //void enablePhanckulizer(bool value);

        /**
         * @brief Checks if the Phanckulizer is enabled
         * @return true if it is enabled, otherwise not
         * @warning does not using this feature because it is incomplete in its
         * code port from previous code not using SpecialK
         */
        //bool isPhanckulizer();

        CStr *getServerAddress();
        uint16_t getServerPort();

        //void phanckulizerEnabled(bool value);
        //void phanckulized(QString phanckulizedAddressName);

        /**
         * @brief Triggered when the server enables its Phackulizer
         * @warning does not using this feature because it is incomplete in its
         * code port from previous code not using SpecialK
         */
        //Signal(phanckulizerEnabled);

        /**
         * @brief Triggered when the server Phanckulize a client
         * @warning does not using this feature because it is incomplete in its
         * code port from previous code not using SpecialK
         */
        //Signal(phanckulized);

    private:
        struct sockaddr_in svr;
        SkString svrAddress;
        unsigned short int svrPort;

    protected:
        /*bool phanckulizeSockets;

        void executePhanckulization(int &fdClient, sockaddr_in &remote);*/

        bool tryToAcceptSD(int &fdClient);
        //void onAcceptConnection(SkAbstractSocket *sck);

};

#endif // SKTCPSERVER_H
