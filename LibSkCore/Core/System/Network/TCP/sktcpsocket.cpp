#include "sktcpsocket.h"

#include "Core/Containers/skstring.h"
#include <unistd.h>
#include <netdb.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkTcpSocket, connect, bool, Arg_CStr, Arg_UInt16)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkTcpSocket, connect, 1, bool, Arg_CStr, Arg_UInt16)
DeclareMeth_INSTANCE_VOID(SkTcpSocket, setAsServer)
DeclareMeth_INSTANCE_RET(SkTcpSocket, peerAddress, CStr*)
DeclareMeth_INSTANCE_RET(SkTcpSocket, peerHostName, CStr*)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkTcpSocket, SkAbstractSocket,
                {
                    AddMeth_INSTANCE_RET(SkTcpSocket, connect);
                    AddMeth_INSTANCE_RET_OVERLOAD(SkTcpSocket, connect, 1);
                    AddMeth_INSTANCE_VOID(SkTcpSocket, setAsServer);
                    AddMeth_INSTANCE_RET(SkTcpSocket, peerAddress);
                    AddMeth_INSTANCE_RET(SkTcpSocket, peerHostName);
                })
{}

bool SkTcpSocket::connect(CStr *hostName, UShort port)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (SkString::isEmpty(hostName))
    {
        ObjectError("HostName is NOT valid");
        return false;
    }

    clnt.sin_family = AF_INET;
    clnt.sin_port = htons(port);

    host = gethostbyname(hostName);
    struct in_addr **addr_list = (struct in_addr **) host->h_addr_list;

    SkString addrNameStr(host->h_name);

    if (addr_list[0])
        peerAddrIP = inet_ntoa(*addr_list[0]);

    else
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    ObjectDebug("Connecting to: " << addrNameStr << ":" << port << " [" << peerAddrIP << "]");

    if (!host)
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    int socketFD = socket(AF_INET, SOCK_STREAM, 0);

    if (socketFD == -1)
    {
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
        return false;
    }

    bcopy(host->h_addr, &clnt.sin_addr, static_cast<uint64_t>(host->h_length));
    bool ok = (::connect(socketFD, (const struct sockaddr *) &clnt, sizeof(sockaddr_in)) == 0);

    if (ok)
    {
        hostNameStr = hostName;
        setFileDescriptor(socketFD);
    }

    else
    {
        ::close(socketFD);
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
        notifyState(SkDeviceState::Closed);
    }

    return ok;
}

bool SkTcpSocket::connect(SkIPAddress &ipV4Addr, UShort port)
{
    SkString ipStr = ipV4Addr.toString();
    return connect(ipStr.c_str(), port);
}

void SkTcpSocket::setAsServer()
{
    socklen_t addr_size = sizeof(struct sockaddr_in);

    if (getpeername(socketFD, (struct sockaddr *)&clnt, &addr_size) != 0)
    {
        ObjectError("Cannot grab peer-address from tcp-socket");
        return;
    }

    peerAddrIP = inet_ntoa(clnt.sin_addr);

    ObjectDebug("Grabbed peer address: " << peerAddrIP);
    isServerSck = true;
}

CStr *SkTcpSocket::peerAddress()
{
    return peerAddrIP.c_str();
}

CStr *SkTcpSocket::peerHostName()
{
    return hostNameStr.c_str();
}

