#include "skabstractserver.h"
#include "Core/App/skapp.h"
#include <unistd.h>
#include <sys/fcntl.h>
#include <netdb.h>

void SkAbstractServerClose(SkObject *obj)
{
    SkAbstractServer *f = static_cast<SkAbstractServer *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkAbstractServerClose()");

    if (f->isListening())
        f->stop();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkServerError)
{
    SetupEnumWrapper(SkServerError);

    SetEnumItem(SkServerError::NoServerError);
    SetEnumItem(SkServerError::CannotCreateServer);
    SetEnumItem(SkServerError::CannotBind);
    SetEnumItem(SkServerError::CannotListen);
    SetEnumItem(SkServerError::CannotGetSckParameter);
    SetEnumItem(SkServerError::CannotSetSckParameter);
    SetEnumItem(SkServerError::ServerAlreadyOpen);
    SetEnumItem(SkServerError::ServerNotValid);
}

DeclareWrapper_ENUM(SkAbstractServerState)
{
    SetupEnumWrapper(SkAbstractServerState);

    SetEnumItem(SkAbstractServerState::ServerListening);
    SetEnumItem(SkAbstractServerState::ServerClosed);
}

// // // // // // // // // // // // // // // // // // // // //

    DeclareMeth_INSTANCE_RET(SkAbstractServer, isListening, bool)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, getCurrentState, SkAbstractServerState)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, getCurrentStateString, CStr*)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, getCurrentError, SkServerError)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, getCurrentErrorString, CStr*)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, dequeueSD, bool, Arg_Int32_REAL)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, hasQueuedSockets, bool)
    DeclareMeth_INSTANCE_RET(SkAbstractServer, queuedSocketsCount, uint64_t)
    DeclareMeth_INSTANCE_VOID(SkAbstractServer, stop)

// // // // // // // // // // // // // // // // // // // // //

AbstractConstructorImpl(SkAbstractServer, SkObject,
                        {
                            AddEnumType(SkServerError);
                            AddEnumType(SkAbstractServerState);

                            AddMeth_INSTANCE_RET(SkAbstractServer, isListening);
                            AddMeth_INSTANCE_RET(SkAbstractServer, getCurrentState);
                            AddMeth_INSTANCE_RET(SkAbstractServer, getCurrentStateString);
                            AddMeth_INSTANCE_RET(SkAbstractServer, getCurrentError);
                            AddMeth_INSTANCE_RET(SkAbstractServer, getCurrentErrorString);
                            AddMeth_INSTANCE_RET(SkAbstractServer, dequeueSD);
                            AddMeth_INSTANCE_RET(SkAbstractServer, hasQueuedSockets);
                            AddMeth_INSTANCE_RET(SkAbstractServer, queuedSocketsCount);
                            AddMeth_INSTANCE_VOID(SkAbstractServer, stop);
                        })
{
    svrSckFD = -1;
    maxQueuedSD = 1;
    baseUpdateEnabled = false;

    error = SkServerError::NoServerError;
    state = SkAbstractServerState::ServerClosed;

    SignalSet(svrOpen);
    SignalSet(svrClosed);
    SignalSet(newSvrConnection);
    SlotSet(updateBase);
    SignalSet(stateChanged);
    SignalSet(errorOccurred);

    addDtorCompanion(SkAbstractServerClose);
}

bool SkAbstractServer::enableLinistening(uint64_t sdQueueMax)
{
    if (sdQueueMax < 1)
        sdQueueMax = 10;

    maxQueuedSD = sdQueueMax;

    if (listen(svrSckFD, static_cast<int>(maxQueuedSD)) == -1)
    {
        notifyError(SkServerError::CannotListen);
        return false;
    }

    int flags = ::fcntl(svrSckFD, F_GETFL);

    if (flags < 0)
    {
        notifyError(SkServerError::CannotGetSckParameter);
        return false;
    }

    if (fcntl(svrSckFD, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        notifyError(SkServerError::CannotSetSckParameter);
        return false;
    }

    enableBaseUpdate(true);
    //eventLoop()->addDescriptor(socketFD, this);

    return true;
}

bool SkAbstractServer::enableBaseUpdate(bool value)
{
    if (value)
    {
        if (baseUpdateEnabled)
        {
            ObjectError("SVR-UPDATE is ALREADY enabled");
            return false;
        }

        baseUpdateEnabled = true;

        ObjectDebug("Enabling SVR-UPDATE ..");
        Attach(eventLoop()->fastZone_SIG, pulse, this, updateBase, SkAttachMode::SkQueued);

        notifyState(SkAbstractServerState::ServerListening);
        svrOpen();

        ObjectDebug("SVR-UPDATE ENABLED");
        //eventLoop()->addDescriptor(svrSckFD);
     }

    else
    {
        if (!baseUpdateEnabled)
        {
            ObjectError("SVR-UPDATE is ALREADY disabled");
            return false;
        }

        //eventLoop()->delDescriptor(svrSckFD);

        ObjectDebug("Disabling SVR-UPDATE ..");
        baseUpdateEnabled = false;

        Detach(eventLoop()->fastZone_SIG, pulse, this, updateBase);

        notifyState(SkAbstractServerState::ServerClosed);

        if (!isPreparedToDie())
            svrClosed();

        ObjectDebug("SVR-UPDATE DISABLED");
    }

    return true;
}

SlotImpl(SkAbstractServer, updateBase)
{
    SilentSlotArgsWarning();

    if (state == SkAbstractServerState::ServerListening)
    {
        int newClient = -1;
        bool ok = false;

        while (tryToAcceptSD(newClient))
        {
            if (!ok)
                ok = true;

            ObjectDebug("New SD cached: " << newClient);
            sdQueue.enqueue(newClient);
            checkSdQueue();
        }

        if (ok)
            newSvrConnection();
    }
}

void SkAbstractServer::checkSdQueue()
{
    if (sdQueue.count() > maxQueuedSD)
    {
        int killingSck;

        if (dequeueSD(killingSck))
        {
            ObjectDebug("Killing SD cached [MaxQueuedSD: " << maxQueuedSD <<  "]: " << killingSck);
            ::close(killingSck);
        }
    }
}

bool SkAbstractServer::dequeueSD(int &sd)
{
    SkMutexLocker locker(&sdQueueEx);

    if (sdQueue.isEmpty())
    {
        sd = -1;
        return false;
    }

    /*int *tempSD = sdQueue.dequeue();
    sd = *tempSD;
    ObjectDebug("Dequeueing SD cached: " << sd)
    delete tempSD;*/

    sd = sdQueue.dequeue();
    ObjectDebug("Dequeueing SD cached: " << sd);
    return true;
}

bool SkAbstractServer::hasQueuedSockets()
{
    SkMutexLocker locker(&sdQueueEx);
    return !sdQueue.isEmpty();
}

uint64_t SkAbstractServer::queuedSocketsCount()
{
    SkMutexLocker locker(&sdQueueEx);
    return sdQueue.count();
}

bool SkAbstractServer::isListening()
{
    return (state == SkAbstractServerState::ServerListening);
}

SkAbstractServerState SkAbstractServer::getCurrentState()
{
    return state;
}

CStr *SkAbstractServer::getCurrentStateString()
{
    if (state == SkAbstractServerState::ServerListening)
        return "Listening";

    return "Closed";
}

SkServerError SkAbstractServer::getCurrentError()
{
    return error;
}

CStr *SkAbstractServer::getCurrentErrorString()
{
    if (error == SkServerError::CannotCreateServer)
        return "Cannot create server";

    else if (error == SkServerError::CannotBind)
        return "Cannot bind";

    else if (error == SkServerError::CannotListen)
        return "Cannot listen";

    else if (error == SkServerError::ServerAlreadyOpen)
        return "Device is ALREADY open";

    else if (error == SkServerError::ServerNotValid)
        return "Target is NOT valid";

    return "NoError";
}

void SkAbstractServer::notifyState(SkAbstractServerState st)
{
    state = st;
    stateChanged();
}

void SkAbstractServer::notifyError(SkServerError err)
{
    error = err;
    ObjectError("Server problem [" << typeName() << "]: " << SkAbstractServer::getCurrentErrorString());
    errorOccurred();
}

#include "skabstractsocket.h"

bool SkAbstractServer::acceptConnection(SkAbstractSocket *sck)
{
    int fdClient;
    bool ok = dequeueSD(fdClient);

    if (ok)
    {
        SkString s(objectName());
        s.append(".SCK[");
        s.append(SkString::number(fdClient));
        s.append("]");

        sck->setObjectName(s.c_str());
        sck->setFileDescriptor(fdClient);
        onAcceptConnection(sck);
    }

    return ok;
}

void SkAbstractServer::onPreparingToDie()
{
    if (baseUpdateEnabled)
        enableBaseUpdate(false);
}
