/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKBUFFERDEVICE_H
#define SKBUFFERDEVICE_H

#include "skabstractdevice.h"
#include "Core/Containers/skdatabuffer.h"

enum SkBufferDeviceMode
{
    BVM_NOMODE,
    BVM_ONLYREAD,
    BVM_ONLYWRITE,
    BVM_READWRITE
};

class SkBufferDevice extends SkAbstractDevice
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkBufferDevice, SkAbstractDevice);

        /**
         * @brief Opens the device based on a data-buffer (on RAM)
         * @param buf the data-buffer to use
         * @param m the opening mode
         * @return true if there are not errors, otherwise false
         */
        bool open(SkDataBuffer &buf, SkBufferDeviceMode m=SkBufferDeviceMode::BVM_READWRITE);

        /**
         * @brief Gets the cursor position of the buffer
         * @return current position if it is valid, otherwise -1
         */
        int64_t pos();

        /**
         * @brief Reset the position to 0 on the buffer
         * @return true if there are not errors, otherwise false
         */
        bool rewind();

        /**
         * @brief Seeks a position on the buffer
         * @param position the position to seek
         * @return true if the position is valid, otherwise false
         */
        bool seek(uint64_t position);

        /**
         * @brief Checks if the position is at the end of the buffer
         * @return true if the position is at the end and there are not errors,
         * otherwise false
         */
        bool atEof();

        /**
         * @brief Checks if the implemented device is squencial (not seekable)
         * @return false because a buffer is not sequencial and always seekable
         */
        bool isSequencial(){return false;}

        /**
         * @brief Checks if it is possible to read a line, terminating with '\n'
         * @param lineLength the value reference that will be filled with the
         * line size
         * @return true if there is a line available
         */
        bool canReadLine(uint64_t &lineLength);

        /**
         * @brief Gets the size of the buffer
         * @return the size if it is valid, otherwise -1
         */
        int64_t size();

        /**
         * @brief Gets available data to read from the buffer, calculating from
         * current position to the end
         * @return the available size to read
         */
        uint64_t bytesAvailable();//REMAINING DATA FROM pos() TO size()

        /**
         * @brief Closes the device
         */
        void close();

        /**
         * @brief Gets the current device mode
         * @return the current mode value
         */
        SkBufferDeviceMode currentMode();

        /**
         * @brief Gets the current device mode as string
         * @return the current mode value as string
         */
        CStr *getCurrentModeString();

    private:
        SkDataBuffer *buffer;
        SkBufferDeviceMode openMode;
        uint64_t cursor;

        bool readInternal(char *data, uint64_t &len, bool onlyPeek=false);
        bool writeInternal(CStr *data, uint64_t &len);
};

#endif // SKBUFFERDEVICE_H
