#ifndef SKSTINDEX_H
#define SKSTINDEX_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skstringlist.h"
#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skvariant.h"

class SkFile;
class SkStorage;
class SkStContainer;
class SkStContainerAttribute;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkStIndexNode extends SkFlatObject
{
    public:
        SkStIndexNode();
        SkStIndexNode(SkFile *indexData, uint64_t position);
        ~SkStIndexNode();

        //MUST be initialized from an existing file position (ALSO if it is NOT valid)
        bool load(SkFile *indexData, uint64_t position);
        bool reload();
        bool synchronize();

        bool set(uint64_t key, uint64_t tuple);
        bool set(uint64_t tuple);
        bool setAsRoot();
        bool setAsNil();

        uint64_t position();

        uint64_t key();
        uint64_t tuple();

        bool isInitialized();
        bool isValid();
        bool isRoot();
        bool isNil();

        bool setFather(uint64_t father);
        bool setFather(SkStIndexNode &father);

        bool hasFather();
        uint64_t father();

        bool hasGrandFather();
        uint64_t grandFather();

        bool setLeftChild(uint64_t child);
        bool setRightChild(uint64_t child);

        bool setLeftChild(SkStIndexNode &child);
        bool setRightChild(SkStIndexNode &child);

        bool hasLeftChild();
        bool hasRightChild();

        uint64_t leftChild();
        uint64_t rightChild();

        bool setBlack();
        bool setDoubleBlack();
        bool unsetDoubleBlack();
        bool setRed();

        bool isBlack();
        bool isDoubleBlack();
        bool isRed();

        bool toggleRedBlack();

        bool isLeftChild();
        bool isRightChild();

        uint64_t brother();
        uint64_t uncle();

        uint64_t min();
        uint64_t max();
        uint64_t ancestor();
        uint64_t successor();

        uint64_t childrenCount();


    private:
        SkFile *index;
        uint64_t pos;

        bool modified;
        bitset<8> state;
        uint64_t data[5];
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkStIndex extends SkFlatObject
{
    public:
        SkStIndex(SkStContainerAttribute *attribute);
        ~SkStIndex();

        bool open();
        bool close();
        bool isOpen();

        bool add(uint64_t k, uint64_t t);
        bool remove(uint64_t k);
        bool contains(uint64_t k);

        uint64_t value(uint64_t k);
        uint64_t min();
        uint64_t max();
        bool isEmpty();
        uint64_t size();

        CStr *filePath();

    private:
        SkStorage *st;
        SkStContainerAttribute *a;
        SkStIndexNode r;
        SkFile *index;

        uint64_t search(uint64_t key);

        uint64_t buildNilNode(SkStIndexNode *father);
        void setNode(SkStIndexNode &n, uint64_t key, uint64_t tuple, bool asBlack);

        void abscendingFixup(SkStIndexNode &z);
        void fixup(SkStIndexNode &n);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKSTINDEX_H
