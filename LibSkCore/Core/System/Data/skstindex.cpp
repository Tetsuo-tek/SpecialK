#include "skstindex.h"
#include "skstorage.h"
#include "Core/System/Filesystem/skfile.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/Containers/skarraycast.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//STATE
#define IS_VALID        0
#define IS_NIL          1
#define IS_RED          2
#define IS_DOUBLEBLACK  3
#define HAS_FATHER      4
#define HAS_LEFT        5
#define HAS_RIGHT       6

// DATA
#define FATHER          0
#define KEY             1
#define TUPLE           2
#define LEFT            3
#define RIGHT           4

// DATA OFFSET
#define FATHER_OFFSET   1+0
#define KEY_OFFSET      1+(1*sizeof(uint64_t))
#define TUPLE_OFFSET    1+(2*sizeof(uint64_t))
#define LEFT_OFFSET     1+(3*sizeof(uint64_t))
#define RIGHT_OFFSET    1+(4*sizeof(uint64_t))

#define DATA_SZ         5*sizeof(uint64_t)

SkStIndexNode::SkStIndexNode()
{
    index = nullptr;

    modified = false;
    pos = 0;
    state = 0;
    memset(data, 0, DATA_SZ);
}

SkStIndexNode::SkStIndexNode(SkFile *indexData, uint64_t position)
{
    if (!load(indexData, position))
    {
        index = nullptr;
        modified = false;
        pos = 0;
        state = 0;
        memset(data, 0, DATA_SZ);
    }
}

SkStIndexNode::~SkStIndexNode()
{
    synchronize();
}

bool SkStIndexNode::load(SkFile *indexData, uint64_t position)
{
    if (!indexData)
    {
        FlatError("NOT initialized");
        return false;
    }

    if (position > indexData->size())
    {
        FlatError("File-Data position NOT valid: " << position << " [file size is " << indexData->size() << " B]");
        return false;
    }

    if (isValid())
        synchronize();

    modified = false;
    pos = position;
    index = indexData;

    return reload();
}

bool SkStIndexNode::reload()
{
    if (!index)
    {
        FlatError("NOT initialized");
        return false;
    }

    index->seek(pos);
    state = index->readUInt8();

    uint64_t sz = DATA_SZ;

    if (state[IS_VALID])
        index->read(SkArrayCast::toChar(data), sz);

    else
    {
        state = 0;
        memset(SkArrayCast::toChar(data), 0, sz);
    }

    return true;
}

bool SkStIndexNode::synchronize()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!modified)
        return true;

    if (!state[IS_VALID])
        state[IS_VALID] = true;

    index->seek(pos);
    index->writeUInt8(state.to_ulong());

    index->write(SkArrayCast::toChar(data), DATA_SZ);

    return true;
}

bool SkStIndexNode::set(uint64_t key, uint64_t tuple)
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (isNil())
        state[IS_NIL] = false;

    data[KEY] = key;
    data[TUPLE] = tuple;

    modified = true;
    return true;
}

bool SkStIndexNode::set(uint64_t tuple)
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (isNil())
    {
        FlatError("Need key to set tuple; it IS_NIL");
        return false;
    }

    data[TUPLE] = tuple;

    modified = true;
    return true;
}

bool SkStIndexNode::setAsRoot()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    state[HAS_FATHER] = false;
    data[FATHER] = 0;

    synchronize();

    index->seek(0);
    index->writeUInt64(pos);

    return true;
}

bool SkStIndexNode::setAsNil()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    data[KEY] = 0;
    data[TUPLE] = 0;
    data[LEFT] = 0;
    data[RIGHT] = 0;

    state[IS_VALID] = true;
    state[IS_NIL] = true;
    state[IS_RED] = false;
    state[IS_DOUBLEBLACK] = false;
    state[HAS_LEFT] = false;
    state[HAS_RIGHT] = false;

    modified = true;

    return true;
}

uint64_t SkStIndexNode::position()
{
    return pos;
}

uint64_t SkStIndexNode::key()
{
    return data[KEY];
}

uint64_t SkStIndexNode::tuple()
{
    return data[TUPLE];
}

bool SkStIndexNode::isInitialized()
{
    return (index != nullptr);
}

bool SkStIndexNode::isValid()
{
    return state[IS_VALID];
}

bool SkStIndexNode::isRoot()
{
    return (isValid() && !hasFather());
}

bool SkStIndexNode::isNil()
{
    return state[IS_NIL];
}

bool SkStIndexNode::setFather(uint64_t father)
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    if (!father)
    {
        FlatError("Father CANNOT be NULL");
        return false;
    }

    data[FATHER] = father;
    state[HAS_FATHER] = (father!=0);

    modified = true;
    return true;
}

bool SkStIndexNode::setFather(SkStIndexNode &father)
{
    return setFather(father.position());
}

bool SkStIndexNode::hasFather()
{
    return state[HAS_FATHER];
}

uint64_t SkStIndexNode::father()
{
    return data[FATHER];
}

bool SkStIndexNode::hasGrandFather()
{
    if (state[HAS_FATHER])
    {
        index->seek(data[FATHER]);
        bitset<8> fatherState = index->readUInt8();
        return fatherState[HAS_FATHER];
    }

    return false;
}

uint64_t SkStIndexNode::grandFather()
{
    if (state[HAS_FATHER])
    {
        index->seek(data[FATHER]);
        bitset<8> fatherState = index->readUInt8();

        if (fatherState[HAS_FATHER])
            return index->readUInt64();
    }

    return 0;
}

bool SkStIndexNode::setLeftChild(uint64_t child)
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    data[LEFT] = child;
    state[HAS_LEFT] = (child!=0);

    modified = true;

    return true;
}

bool SkStIndexNode::setRightChild(uint64_t child)
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    data[RIGHT] = child;
    state[HAS_RIGHT] = (child!=0);

    modified = true;

    return true;
}

bool SkStIndexNode::setLeftChild(SkStIndexNode &child)
{
    return setLeftChild(child.position());
}

bool SkStIndexNode::setRightChild(SkStIndexNode &child)
{
    return setRightChild(child.position());
}

bool SkStIndexNode::hasLeftChild()
{
    return state[HAS_LEFT];
}

bool SkStIndexNode::hasRightChild()
{
    return state[HAS_RIGHT];
}

uint64_t SkStIndexNode::leftChild()
{
    return data[LEFT];
}

uint64_t SkStIndexNode::rightChild()
{
    return data[RIGHT];
}

bool SkStIndexNode::setBlack()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    state[IS_RED] = false;
    modified = true;

    return true;
}

bool SkStIndexNode::setDoubleBlack()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    if (!isBlack())
    {
        FlatError("To be DoubleBlack it must be Black first");
        return false;
    }

    state[IS_DOUBLEBLACK] = true;
    modified = true;

    return true;
}

bool SkStIndexNode::unsetDoubleBlack()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    if (!isDoubleBlack())
    {
        FlatError("To unset DoubleBlack it must be DoubleBlack first");
        return false;
    }

    state[IS_DOUBLEBLACK] = false;
    modified = true;

    return true;
}

bool SkStIndexNode::setRed()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    state[IS_RED] = true;
    modified = true;

    return true;
}

bool SkStIndexNode::isBlack()
{
    return !state[IS_RED];
}

bool SkStIndexNode::isDoubleBlack()
{
    return !state[IS_DOUBLEBLACK];
}

bool SkStIndexNode::isRed()
{
    return state[IS_RED];
}

bool SkStIndexNode::toggleRedBlack()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    state[IS_RED] = !state[IS_RED];
    modified = true;

    return true;
}

bool SkStIndexNode::isLeftChild()
{
    if (state[HAS_FATHER])
    {
        index->seek(data[FATHER]+LEFT_OFFSET);
        return (index->readUInt64() == pos);
    }

    return false;
}

bool SkStIndexNode::isRightChild()
{
    if (state[HAS_FATHER])
    {
        index->seek(data[FATHER]+RIGHT_OFFSET);
        return (index->readUInt64() == pos);
    }

    return false;
}

uint64_t SkStIndexNode::brother()
{
    if (state[HAS_FATHER])
    {
        if (isLeftChild())
        {
            index->seek(data[FATHER]+RIGHT_OFFSET);
            return index->readUInt64();
        }

        else
        {
            index->seek(data[FATHER]+LEFT_OFFSET);
            return index->readUInt64();
        }
    }

    return 0;
}

uint64_t SkStIndexNode::uncle()
{
    uint64_t g = grandFather();

    if (g)
    {
        SkStIndexNode grandFather(index, g);

        if (isLeftChild())
            return grandFather.rightChild();

        else
            return grandFather.leftChild();
    }

    return 0;
}

uint64_t SkStIndexNode::min()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    SkStIndexNode n(index, pos);
    uint64_t currentPos = 0;

    while(!n.isNil())
    {
        currentPos = n.position();
        n.load(index, leftChild());
    }

    return currentPos;
}

uint64_t SkStIndexNode::max()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    if (!isValid())
    {
        FlatError("NOT valid");
        return false;
    }

    SkStIndexNode n(index, pos);
    uint64_t currentPos = 0;

    while(!n.isNil())
    {
        currentPos = n.position();
        n.load(index, rightChild());
    }

    return currentPos;
}

uint64_t SkStIndexNode::ancestor()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    SkStIndexNode n(index, leftChild());

    uint64_t currentPos = 0;

    if (!n.isNil())
        return n.max();

    else
    {
        if (!hasFather())
            return pos;

        n.load(index, father());

        while(n.hasFather() && !n.isRightChild())
        {
            n.load(index, n.father());

            if (n.isRightChild())
                currentPos = n.position();
        }
    }

    return currentPos;
}

uint64_t SkStIndexNode::successor()
{
    if (!isInitialized())
    {
        FlatError("NOT initialized");
        return false;
    }

    SkStIndexNode n(index, leftChild());

    uint64_t currentPos = 0;

    if (!n.isNil())
        return n.min();

    else
    {
        if (!hasFather())
            return pos;

        n.load(index, father());

        while(n.hasFather() && !n.isLeftChild())
        {
            n.load(index, n.father());

            if (n.isLeftChild())
                currentPos = n.position();
        }
    }

    return currentPos;
}

uint64_t SkStIndexNode::childrenCount()
{
    return (state[HAS_LEFT] + state[HAS_RIGHT]);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkStIndex::SkStIndex(SkStContainerAttribute *attribute)
{
    a = attribute;
    st = a->storage();

    setObjectName(a, "index");

    SkString indexPath = st->getWorkingDir();
    indexPath.append(attribute->container()->name());
    indexPath.append("/");
    indexPath.append("index-");
    indexPath.append(attribute->name());

    index = new SkFile;
    setObjectName(a, "index");

    index->setFilePath(indexPath);
}

SkStIndex::~SkStIndex()
{
    if (index->isOpen())
        index->close();

    index->destroyLater();
}

bool SkStIndex::open()
{
    index->setObjectName(this, "file");

    if (!SkFsUtils::exists(index->getFilePath()))
    {
        if (!SkFsUtils::touch(index->getFilePath()))
            return false;

        FlatMessage("Index data file CREATED: " << index->getFilePath());
    }

    if (index->isOpen())
    {
        FlatError("Index data file is ALREADY open: " << index->getFilePath());
        return false;
    }

    if (!index->open())
        return false;

    if (!index->size())
    {
        index->writeUInt64(0);
        r.load(index, buildNilNode(nullptr));
        r.setAsRoot();
        FlatMessage("Index data  INITIALIZED: " << index->getFilePath());
        return true;
    }

    FlatMessage("Index data file OPEN: " << index->getFilePath());
    return r.load(index, index->readUInt64());
}

bool SkStIndex::close()
{
    if (!index->isOpen())
    {
        FlatError("Index file-data is NOT open: " << index->getFilePath());
        return false;
    }

    FlatMessage("Index data file CLOSED: " << index->getFilePath());
    index->close();
    return true;
}

bool SkStIndex::isOpen()
{
    return index->isOpen();
}

uint64_t SkStIndex::buildNilNode(SkStIndexNode *father)
{
    index->seek(index->size());
    uint64_t position = index->pos();

    bitset<8> state = 0;
    state[IS_VALID] = true;
    state[IS_NIL] = true;
    state[HAS_FATHER] = (father != nullptr);

    index->writeUInt8(state.to_ulong());

    uint8_t data[DATA_SZ] = {0};

    if (state[HAS_FATHER])
        SkArrayCast::toUInt64(data)[FATHER] = father->position();

    index->write(SkArrayCast::toChar(data), DATA_SZ);

    return position;
}

void SkStIndex::setNode(SkStIndexNode &n, uint64_t key, uint64_t tuple, bool asBlack)
{
    if (n.isNil())
    {
        n.setLeftChild(buildNilNode(&n));
        n.setRightChild(buildNilNode(&n));
    }

    n.set(key, tuple);

    if (asBlack)
        n.setBlack();

    else
        n.setRed();

    n.synchronize();
}

uint64_t SkStIndex::search(uint64_t key)
{
    bool isEmpty = a->container()->isEmpty();

    if (isEmpty)
        return r.position();

    SkStIndexNode n(index, r.position());

    while(!n.isNil())
    {
        if (a->usesDataFile())
        {

        }

        else
        {
            if (key < n.key())
                n.load(index, n.leftChild());

            else if (key > n.key())
                n.load(index, n.rightChild());

            else if (key == n.key())
                return n.position();
        }
    }

    return n.position();
}

bool SkStIndex::add(uint64_t k, uint64_t t)
{
    if (r.isNil())
        setNode(r, k, t, true);

    else
    {
        uint64_t pos = search(k);
        SkStIndexNode z(index, pos);

        if (!z.isNil())
        {
            z.set(t);
            z.synchronize();

            if (z.position() == r.position())
                return r.reload();
        }

        setNode(z, k, t, false);
        abscendingFixup(z);
    }

    return true;
}

void SkStIndex::abscendingFixup(SkStIndexNode &z)
{
    bool isDoubleBlack = z.isDoubleBlack();

    if (isDoubleBlack)
        z.unsetDoubleBlack();

    while(z.hasFather())
    {
        fixup(z);

        z.load(index, z.father());

        if (isDoubleBlack && z.isRed())
        {
            z.setBlack();
            isDoubleBlack = false;
        }
    }
}

void SkStIndex::fixup(SkStIndexNode &n)
{
    SkStIndexNode p(index, n.father());
    SkStIndexNode g(index, n.grandFather());
    SkStIndexNode u(index, n.uncle());

    if (n.isRed() && p.isRed())
    {
        //case 1
        if (u.isRed())
        {
            p.toggleRedBlack();
            u.toggleRedBlack();
            g.toggleRedBlack();
        }

        else if (u.isBlack())
        {
            //case 2
            if (n.isRightChild() && p.isLeftChild())
            {
                //cout << n->label << " - violation: case 2a\n";

                g.setLeftChild(n);
                n.setFather(g);
                p.setRightChild(n.leftChild());
                SkStIndexNode y(index, p.rightChild());
                y.setFather(p);
                n.setLeftChild(p);
                p.setFather(n);

                fixup(p);
            }

            else if (n.isLeftChild() && p.isRightChild())
            {
                //cout << n->label << " - violation: case 2b\n";

                g.setRightChild(n);
                n.setFather(g);
                p.setLeftChild(n.rightChild());
                SkStIndexNode y(index, p.leftChild());
                y.setFather(p);
                n.setRightChild(p);
                p.setFather(n);

                fixup(p);
            }

            //case 3
            else if (n.isLeftChild() && p.isLeftChild())
            {
                //cout << n->label << " - violation: case 3a\n";

                if (g.hasFather())
                {
                    if (g.isRightChild())
                    {
                        SkStIndexNode y(index, g.father());
                        y.setRightChild(p);
                    }

                    else
                    {
                        SkStIndexNode y(index, g.father());
                        y.setLeftChild(p);
                    }
                }

                else
                {
                    r.load(index, p.position());
                    r.setAsRoot();
                    //r = p;
                }

                p.setFather(g.father());
                g.setFather(p);
                g.setLeftChild(p.rightChild());
                SkStIndexNode y(index, g.leftChild());
                y.setFather(g);
                p.setRightChild(g);

                p.toggleRedBlack();
                g.toggleRedBlack();
            }

            else if (n.isRightChild() && p.isRightChild())
            {
                //cout << n->label << " - violation: case 3b\n";

                if (g.hasFather())
                {
                    if (g.isRightChild())
                    {
                        SkStIndexNode y(index, g.father());
                        y.setRightChild(p);
                    }

                    else
                    {
                        SkStIndexNode y(index, g.father());
                        y.setLeftChild(p);
                    }
                }

                else
                {
                    r.load(index, p.position());
                    r.setAsRoot();
                    //r = p;
                }

                p.setFather(g.father());
                g.setFather(p);
                g.setRightChild(p.leftChild());
                SkStIndexNode y(index, g.rightChild());
                y.setFather(g);
                p.setLeftChild(g);

                p.toggleRedBlack();
                g.toggleRedBlack();
            }
        }

        r.reload();

        if (r.hasFather())
        {
            cout << "!!!!!!!!!!!!!!! root CANNOT have father\n";
            exit(1);
        }

        if (r.isRed())
            r.toggleRedBlack();
    }
}

bool SkStIndex::remove(uint64_t k)
{
    return false;
}

bool SkStIndex::contains(uint64_t k)
{
    SkStIndexNode y(index, search(k));
    return !y.isNil();
}

uint64_t SkStIndex::value(uint64_t k)
{
    SkStIndexNode y(index, search(k));

    if (y.isBlack())
        return 0;

    return y.tuple();
}

uint64_t SkStIndex::min()
{
    return r.min();
}

uint64_t SkStIndex::max()
{
    return r.max();
}

bool SkStIndex::isEmpty()
{
    //NOT A GOOD CHOICE
    return a->container()->isEmpty();
}

uint64_t SkStIndex::size()
{
    //NOT A GOOD CHOICE
    return a->container()->count();
}

CStr *SkStIndex::filePath()
{
    return index->getFilePath();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
