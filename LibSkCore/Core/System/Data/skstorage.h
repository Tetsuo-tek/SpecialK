#ifndef SKSTORAGE_H
#define SKSTORAGE_H

#include "skstcontainer.h"

class SkStorage extends SkFlatObject
{
    public:
        SkStorage();

        bool create(CStr *dbPath, CStr *dbName);
        bool destroy(CStr *dbPath);

        bool open(CStr *dbPath);
        bool close();

        //they NEED schemaSynch() to be valid changes
        bool newContainer(CStr *name, SkList<SkStContainerAttributeProps *> &attributes, bool isTree=false);
        bool removeContainer(CStr *name);

        bool setCurrentContainer(CStr *name);
        void unsetCurrentContainer();
        bool insert(SkTreeMap<SkString, SkVariant> &values);
        bool update(SkTreeMap<SkString, SkVariant> &values, uint64_t id);
        bool remove(uint64_t id);
        bool isEmpty();
        uint64_t count();
        uint64_t lastInsertedID();

        bool isOpen();
        CStr *getWorkingDir();
        void containers(SkStringList &l);

        bool schemaSynch();

        CStr *name();

    private:
        bool enabled;

        SkString n;
        SkString workingDir;
        SkString schemaPath;

        SkTreeMap<SkString, SkStContainer *> repository;
        SkStContainer *currentContainer;

        bool loadSchema();
        bool saveSchema();
};

#endif // SKSTORAGE_H
