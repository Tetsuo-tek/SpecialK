#include "skstcontainer.h"
#include "skstorage.h"

#include "Core/System/Filesystem/skfile.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Time/skdatetime.h"

SkStContainer::SkStContainer(SkStorage *storage)
{
    dataFile = new SkFile;
    st = storage;
    isTreeContainer = false;
}

SkStContainer::SkStContainer(SkStorage *storage, CStr *name, SkList<SkStContainerAttributeProps *> &attributes, bool isTree)
{
    dataFile = new SkFile;
    st = storage;
    n = name;
    isTreeContainer = isTree;

    setObjectName(st, n.c_str());

    int insertingOrder = 0;

    SkStContainerAttributeProps props;
    props.name = "id";
    props.type = T_UINT64;
    props.isNotNull = true;
    props.isUnique = true;
    props.hasIndex = true;
    props.isKeyElement = false;

    SkStContainerAttribute *id = new SkStContainerAttribute(st, this, props);

    attributesFromKeyOrder.add("id", id);
    attributesFromInsertOrder.add(insertingOrder++, id);

    FlatMessage("Identifier GENERATED: " << st->name() << "." << n << ".id");

    props.name = "ts";
    props.type = T_UINT64;
    props.isNotNull = true;
    props.isUnique = true;
    props.hasIndex = true;
    props.isKeyElement = false;

    SkStContainerAttribute *ts = new SkStContainerAttribute(st, this, props);

    attributesFromKeyOrder.add("ts", ts);
    attributesFromInsertOrder.add(insertingOrder++, ts);

    FlatMessage("Timestamp GENERATED: " << st->name() << "." << n << ".ts");

    if (isTreeContainer)
    {
        props.name = "idParent";
        props.type = T_UINT64;
        props.isNotNull = true;
        props.isUnique = false;
        props.hasIndex = true;
        props.isKeyElement = false;
        props.relatingContainerName = n;

        SkStContainerAttribute *idParent = new SkStContainerAttribute(st, this, props);

        attributesFromKeyOrder.add("idParent", idParent);
        attributesFromInsertOrder.add(insertingOrder++, idParent);

        FlatMessage("Tree parent-reference GENERATED: " << st->name() << "." << n << ".idParent");
    }

    SkAbstractListIterator<SkStContainerAttributeProps *> *itr = attributes.iterator();

    while(itr->next())
    {
        SkStContainerAttributeProps *props = itr->item();
        SkStContainerAttribute *a = new SkStContainerAttribute(st, this, *props);

        attributesFromInsertOrder.add(insertingOrder++, a);
        attributesFromKeyOrder.add(a->name(), a);
    }

    delete itr;
}

SkStContainer::~SkStContainer()
{
    if (dataFile->isOpen())
        dataFile->close();

    dataFile->destroyLater();

    SkBinaryTreeVisit<int, SkStContainerAttribute *> *subItr = attributesFromInsertOrder.iterator();

    while(subItr->next())
        delete subItr->item().value();

    delete subItr;

    attributesFromInsertOrder.clear();
    attributesFromKeyOrder.clear();
}

void SkStContainer::drop()
{
    //MUST DELETE RELATED DATA-FILEs
}

bool SkStContainer::loadSchema(SkFile *schema)
{
    uint8_t sz = schema->readUInt8();
    schema->read(n, sz);

    setObjectName(st, n.c_str());

    isTreeContainer = static_cast<bool>(schema->readUInt8());

    FlatMessage("Container header LOADED: " << st->name() << "." << n);

    uint8_t attributesNumber = schema->readUInt8();

    int insertingOrder = 0;

    while(attributesNumber > attributesFromInsertOrder.count() && !schema->atEof())
    {
        SkStContainerAttribute *a = new SkStContainerAttribute(st, this);

        if (!a->loadSchema(schema))
            return false;

        attributesFromKeyOrder.add(a->name(), a);
        attributesFromInsertOrder.add(insertingOrder++, a);
    }

    return openDataFile();
}

bool SkStContainer::saveSchema(SkFile *schema)
{
    schema->writeUInt8(n.size());
    schema->write(n);

    schema->writeUInt8(isTreeContainer);

    schema->writeUInt8(attributesFromInsertOrder.count());

    FlatMessage("Container header SAVED: " << n << "." << n);

    if (!buildDataFile() || !openDataFile())
        return false;

    SkBinaryTreeVisit<int, SkStContainerAttribute *> *itr = attributesFromInsertOrder.iterator();

    while(itr->next())
    {
        SkStContainerAttribute *a = itr->item().value();

        if (!a->saveSchema(schema))
        {
            delete itr;
            return false;
        }
    }

    delete itr;
    return true;
}

bool SkStContainer::buildDataFile()
{
    SkString containerPath = st->getWorkingDir();
     containerPath.append(n);
     containerPath.append("/");

     if (!SkFsUtils::exists(containerPath.c_str()))
     {
         if (!SkFsUtils::mkdir(containerPath.c_str()))
             return false;

         FlatMessage("Container directory CREATED: " << containerPath);
     }

     SkString containerDataPath = containerPath;
     containerDataPath.append("container");

     if (!SkFsUtils::exists(containerDataPath.c_str()))
     {
         SkFile *f = new SkFile;

         f->setFilePath(containerDataPath.c_str());

         if (!f->open(SkFileMode::FLM_ONLYWRITE))
             return false;

         f->writeUInt64(0);//count
         f->writeUInt64(0);//lastID (0 at start)

         f->close();
         f->destroyLater();

         FlatMessage("Container data INITIALIZED: " << n << " [" << containerDataPath << "]");
     }

     return true;
}

bool SkStContainer::openDataFile()
{
    if (dataFile->isOpen())
        return true;

    SkString dataPath = st->getWorkingDir();
    dataPath.append(n);
    dataPath.append("/container");

    dataFile->setFilePath(dataPath.c_str());

    bool ok = dataFile->open();

    if (ok)
        FlatMessage("Container data file OPEN: " << dataPath);

    return ok;
}

bool SkStContainer::validation(SkTreeMap<SkString, SkVariant> &values)
{
    SkBinaryTreeVisit<SkString, SkVariant> *itr1 = values.iterator();
    SkBinaryTreeVisit<SkString, SkStContainerAttribute *> *itr2 = attributesFromKeyOrder.iterator();

    while(itr1->next())
    {
        bool ok = false;

        while(!ok && itr2->next())
            ok = (itr2->item().key() == itr1->item().key());

        if (ok)
        {
            SkStContainerAttribute *a = itr2->item().value();

            if (!a->setValue(itr1->item().value()))
            {
                delete itr1;
                delete itr2;

                return false;
            }
        }

        else
        {
            FlatError("Attribute NOT found: " << n << "." << itr1->item().key());

            delete itr1;
            delete itr2;

            return false;
        }
    }

    delete itr1;
    delete itr2;

    return true;
}

bool SkStContainer::writeTuple(uint64_t existingID)
{
    SkString containerPath = st->getWorkingDir();
    containerPath.append(n);
    containerPath.append("/");

    dataFile->rewind();

    uint64_t lastCount = dataFile->readUInt64();
    uint64_t lastID = dataFile->readUInt64();

    /*cout << lastCount << "\n";
    cout << lastID << "\n";*/

    uint64_t unixTime = SkDateTime::currentUnixTime();

    dataFile->seek(dataFile->size());

    uint64_t statePos = dataFile->pos();

    cout << "tuplePos: " << statePos << "\n";
    dataFile->writeUInt8(false);

    SkBinaryTreeVisit<int, SkStContainerAttribute *> *itr = attributesFromInsertOrder.iterator();

    while(itr->next())
    {
        SkString k = itr->item().value()->name();

        //these attributes will be wrote only on INSERT
        if (existingID && (k == "id" || k == "ts"))
            continue;

        SkStContainerAttribute *a = itr->item().value();

        {
            SkVariant tempVal;

            if (k == "id")
                tempVal.setVal(++lastID);

            else if (k == "ts")
                tempVal.setVal(unixTime);

            if (k == "id" || k == "ts")
                a->setValue(tempVal);
        }

        a->saveValue(dataFile, statePos);
    }

    delete itr;

    dataFile->rewind();

    dataFile->writeUInt64(++lastCount);
    dataFile->writeUInt64(lastID);

    dataFile->seek(statePos);
    dataFile->writeUInt8(true);

    return true;
}

bool SkStContainer::insert(SkTreeMap<SkString, SkVariant> &values)
{
    if (!validation(values))
        return false;

    return writeTuple();
}

bool SkStContainer::update(SkTreeMap<SkString, SkVariant> &values, uint64_t id)
{
    //validation(..) DOES NOT KNOW if it is an INSERT or an UPDATE
    if (!validation(values))
        return false;

    return writeTuple(id);
}

bool SkStContainer::remove(uint64_t id)
{
    return false;
}

uint64_t SkStContainer::readCount()
{
    dataFile->seek(0);
    return dataFile->readUInt64();
}

uint64_t SkStContainer::readLastInsertedID()
{
    dataFile->seek(8);
    return dataFile->readUInt64();
}

SkStorage *SkStContainer::storage()
{
    return st;
}

CStr *SkStContainer::name()
{
    return n.c_str();
}

bool SkStContainer::isTree()
{
    return isTreeContainer;
}

SkBinaryTreeVisit<int, SkStContainerAttribute *> *SkStContainer::insertOrderAttributesIterator()
{
    return attributesFromInsertOrder.iterator();
}

SkBinaryTreeVisit<SkString, SkStContainerAttribute *> *SkStContainer::nameOrderAttributesIterator()
{
    return attributesFromKeyOrder.iterator();
}

bool SkStContainer::isEmpty()
{
    return (count() == 0);
}

uint64_t SkStContainer::count()
{
    return readCount();
}

uint64_t SkStContainer::lastInsertedID()
{
    return readLastInsertedID();
}
