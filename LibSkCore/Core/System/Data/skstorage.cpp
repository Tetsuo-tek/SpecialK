#include "skstorage.h"

#include "Core/System/Filesystem/skfile.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Time/skdatetime.h"

SkStorage::SkStorage()
{
    currentContainer = nullptr;
    enabled = false;
}

bool SkStorage::create(CStr *dbPath, CStr *dbName)
{
    if (isOpen())
    {
        FlatError("This Database is open; CANNOT create a new Database from this object");
        return false;
    }

    if (SkFsUtils::exists(dbPath))
    {
        FlatError("Directory ALREADY exists: " << dbPath);
        return false;
    }

    n = dbName;

    if (n.size()>255)
    {
        FlatError("Database name CANNOT be greater than 255 characters");
        return false;
    }

    if (!SkFsUtils::mkdir(dbPath))
        return false;

    workingDir = SkFsUtils::adjustPathEndSeparator(dbPath);
    schemaPath = workingDir;
    schemaPath.append("schema");

    bool ok = saveSchema();

    FlatMessage("Database CREATED: " << n << " [" << dbPath << "]");

    n.clear();
    schemaPath.clear();
    workingDir.clear();

    return ok;
}

bool SkStorage::destroy(CStr *dbPath)
{
    if (isOpen())
    {
        FlatError("This Database is open; CANNOT destroy a Database from this object");
        return false;
    }

    if (!SkFsUtils::exists(dbPath))
    {
        FlatError("Directory NOT found: " << dbPath);
        return false;
    }

    SkString schemaPath = SkFsUtils::adjustPathEndSeparator(dbPath);
    schemaPath.append("schema");

    if (!SkFsUtils::exists(schemaPath.c_str()))
    {
        FlatError("Schema NOT found: " << schemaPath);
        return false;
    }

    FlatMessage("Database DESTROYED: " << dbPath);
    return SkFsUtils::rmdir(dbPath, true);
}

bool SkStorage::open(CStr *dbPath)
{
    if (isOpen())
    {
        FlatError("Database ALREADY open");
        return false;
    }

    if (!SkFsUtils::exists(dbPath))
    {
        FlatError("Directory NOT exists: " << dbPath);
        return false;
    }

    schemaPath = SkFsUtils::adjustPathEndSeparator(dbPath);
    schemaPath.append("schema");

    if (!SkFsUtils::exists(schemaPath.c_str()))
    {
        FlatError("Schema NOT exists: " << schemaPath);
        schemaPath.clear();
        return false;
    }

    workingDir = SkFsUtils::adjustPathEndSeparator(dbPath);
    enabled = loadSchema();

    if (enabled)
        FlatMessage("Database OPEN: " << n);

    else
    {
        schemaPath.clear();
        workingDir.clear();
    }

    return enabled;
}

bool SkStorage::close()
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    SkBinaryTreeVisit<SkString, SkStContainer *> *itr = repository.iterator();

    while(itr->next())
    {
        SkStContainer *c = itr->item().value();
        delete c;
    }

    delete itr;

    repository.clear();

    FlatMessage("Database CLOSED: " << n);

    enabled = false;

    n.clear();
    workingDir.clear();
    schemaPath.clear();
    currentContainer = nullptr;

    return true;
}

bool SkStorage::loadSchema()
{
    //HERE WE DO NOT PERFORM ANY TEST ON THE FILE DATA-CONSISTENCY

    SkFile *f = new SkFile;

    f->setFilePath(schemaPath.c_str());

    if (!f->open(SkFileMode::FLM_ONLYREAD))
        return false;

    uint8_t sz = f->readUInt8();
    f->read(n, sz);

    uint16_t containersNumber = f->readUInt16();

    FlatMessage("Header LOADED: " << n);

    if (containersNumber)
    {
        while(containersNumber > repository.count() && !f->atEof())
        {
            SkStContainer *c = new SkStContainer(this);
            c->loadSchema(f);
            repository.add(c->name(), c);
        }
    }

    f->close();
    f->destroyLater();

    FlatMessage("Schema LOADED: " << schemaPath);

    return true;
}

bool SkStorage::saveSchema()
{
    SkFile *f = new SkFile;

    f->setFilePath(schemaPath.c_str());

    //IT WILL DESTROY THE LAST SCHEMA

    if (!f->open(SkFileMode::FLM_ONLYWRITE))
        return false;

    f->writeUInt8(n.size());
    f->write(n.c_str(), n.size());

    f->writeUInt16(repository.count());

    FlatMessage("Header SAVED: " << n);

    SkBinaryTreeVisit<SkString, SkStContainer *> *itr = repository.iterator();

    while(itr->next())
    {
        SkStContainer *c = itr->item().value();

        if (!c->saveSchema(f))
        {
            delete itr;

            f->close();
            f->destroyLater();

            return false;
        }
    }

    delete itr;

    f->close();
    f->destroyLater();

    FlatMessage("Schema SAVED: " << schemaPath);

    return true;
}

bool SkStorage::newContainer(CStr *name, SkList<SkStContainerAttributeProps *> &attributes, bool isTree)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (repository.contains(name))
    {
        FlatError("Contrainer ALREADY exists: " << name);
        return false;
    }

    SkStContainer *c = new SkStContainer(this, name, attributes, isTree);
    repository.add(c->name(), c);

    FlatMessage("Container ADDED (need schema-synch): " << c->name());

    return true;
}

bool SkStorage::removeContainer(CStr *name)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (!repository.contains(name))
    {
        FlatError("Contrainer NOT found: " << name);
        return false;
    }

    SkStContainer *c = repository[name];
    c->drop();
    delete c;

    repository.remove(name);
    FlatMessage("Container REMOVED (need schema-synch): " << name);

    return true;
}

bool SkStorage::setCurrentContainer(CStr *name)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (!repository.contains(name))
    {
        FlatError("Container NOT found");
        return false;
    }

    currentContainer = repository[name];
    FlatMessage("Current Container SELECTED: " << name);
    return true;
}

void SkStorage::unsetCurrentContainer()
{
    currentContainer = nullptr;
}

bool SkStorage::insert(SkTreeMap<SkString, SkVariant> &values)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (!currentContainer)
    {
        FlatError("Current Container is NOT valid");
        return 0;
    }

    return currentContainer->insert(values);
}

bool SkStorage::update(SkTreeMap<SkString, SkVariant> &values, uint64_t id)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (!currentContainer)
    {
        FlatError("Current Container is NOT valid");
        return 0;
    }

    return currentContainer->insert(values);
}

bool SkStorage::remove(uint64_t id)
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return false;
    }

    if (!currentContainer)
    {
        FlatError("Current Container is NOT valid");
        return 0;
    }

    return currentContainer->remove(id);
}

bool SkStorage::isEmpty()
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return 0;
    }

    if (!currentContainer)
    {
        FlatError("Current Container NOT valid");
        return 0;
    }

    return currentContainer->isEmpty();
}

uint64_t SkStorage::count()
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return 0;
    }

    if (!currentContainer)
    {
        FlatError("Current Container NOT valid");
        return 0;
    }

    return currentContainer->count();
}

uint64_t SkStorage::lastInsertedID()
{
    if (!isOpen())
    {
        FlatError("Database NOT open");
        return 0;
    }

    if (!currentContainer)
    {
        FlatError("Current Container NOT valid");
        return 0;
    }

    return currentContainer->lastInsertedID();
}

bool SkStorage::schemaSynch()
{
    bool ok = saveSchema();

    if (ok)
        FlatMessage("Schema SYNCHRONIZED: " << schemaPath);

    return ok;
}

CStr *SkStorage::name()
{
    return n.c_str();
}

bool SkStorage::isOpen()
{
    return enabled;
}

CStr *SkStorage::getWorkingDir()
{
    return workingDir.c_str();
}

void SkStorage::containers(SkStringList &l)
{
    repository.keys(l);
}

//ID, TIMESTAMP, and idParent (when it is Tree) are default attributes for Containers

/* Container/container RECORD
    [tuples count]                      uint64_t
    [lastID]                            uint64_t (first inserting ID is 1)

    //TUPLE 1 (ID could be > 1 if the previous items was deleted)

    [state]                             uint8_t (enabled/disabled; if it is disabled the tuple simply does not exist)
    [id]                                uint64_t
    [ts]                         uint64_t
    [AttrName_1]                        uint64_t (if it is a number stay here, eventually casted, if not the value is the position where data is located on it Attribute.data file)
    [AttrName_2]                        uint64_t (..)
    ..

    //TUPLE N

    [state]                             uint8_t
    [id]                                uint64_t
    [ts]                         uint64_t
    ..
*/

/* Attribute/attribute-<NAME> RECORD
    //VAL 1

    [state]                             uint8_t (enabled/disabled; if it is disabled the val simply does not exist)
    [size]                              uint32_t
    [data]                              uint8_t[]
    ..

    //VAL N

    [state]                             uint8_t
    [size]                              uint32_t
    [data]                              uint8_t[]
    ..
*/

/* Index/index-<NAME> RECORD
    [root]                              uint64_t the root position (>=1) if it is NOT 1 it need a vacuum

    // NODE 1
    [state]                             uint8_t (bits)
    [father]                            uint64_t local position
    [key]                               uint64_t (a casted number or a position for a non-number on its data file)
    [tuple]                             uint64_t container data position
    [leftchild]                         uint64_t local position
    [rightchild]                        uint64_t local position


   Bits state:
       1        0           means
    0: Valid    NOT Valid
    1: Nil      Not nil
    2: RED      BLACK       it is Red or Black
    3: DBLBLACK NOT         it is DoubleBlack or NOT
    4: Father   NULL        it has (or NOT) father
    5: Left     NULL        it has (or NOT) LeftChild
    6: Right    NULL        it has (or NOT) RghtChild
*/

/* Schema RECORD
    [DbNameLen]                         uint8_t
    [DbName]                            CStr *

    [#containers]                       uint16_t

    //CONTAINER 1

    [ContainerNameLen_1]                uint8_t
    [ContainerName_1]                   CStr *
    [ContainerIsTree_1]                 uint8_t (bool)

    [#attributes]                       uint8_t

    [AttrNameLen_1_1]                   uint8_t
    [AttrName_1_1]                      CStr *
    [AttrType_1_1]                      uint8_t (SkVariant_T)
    [AttrIsNotNull_1_1]                 uint8_t (bool)
    [AttrIsUnique_1_1]                  uint8_t (bool)
    [AttrHasIndex1_1]                   uint8_t (bool)
    [AttrIsKeyElement1_1]               uint8_t (bool)
    [AttrRelatingContainerNameLen_1_1]  uint8_t (bool)
    [AttrRelatingContainerName_1_1_]    CStr *
    [AttrRelatingAttrNameLen_1_1]       uint8_t (bool)
    [AttrRelatingAttrName_1_1_]         CStr *

    ..

    //CONTAINER N
    ..
*/
