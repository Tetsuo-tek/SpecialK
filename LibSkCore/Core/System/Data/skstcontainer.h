#ifndef SKSTCONTAINER_H
#define SKSTCONTAINER_H

#include "skstcontainerattribute.h"

class SkStContainer extends SkFlatObject
{
    public:
        SkStContainer(SkStorage *storage);
        SkStContainer(SkStorage *storage, CStr *name, SkList<SkStContainerAttributeProps *> &attributes, bool isTree);
        ~SkStContainer();

        void drop();

        SkStorage *storage();
        CStr *name();
        bool isTree();

        bool insert(SkTreeMap<SkString, SkVariant> &values);
        bool update(SkTreeMap<SkString, SkVariant> &values, uint64_t id);
        bool remove(uint64_t id);

        bool isEmpty();
        uint64_t count();
        uint64_t lastInsertedID();

        SkBinaryTreeVisit<int, SkStContainerAttribute *> *insertOrderAttributesIterator();
        SkBinaryTreeVisit<SkString, SkStContainerAttribute *> *nameOrderAttributesIterator();

        bool loadSchema(SkFile *schema);
        bool saveSchema(SkFile *schema);

    private:
        SkStorage *st;
        SkFile *dataFile;
        SkString n;
        bool isTreeContainer;

        SkTreeMap<int, SkStContainerAttribute *> attributesFromInsertOrder;
        SkTreeMap<SkString, SkStContainerAttribute *> attributesFromKeyOrder;

        bool validation(SkTreeMap<SkString, SkVariant> &values);
        bool writeTuple(uint64_t existingID=0);

        bool buildDataFile();
        bool openDataFile();

        uint64_t readCount();
        uint64_t readLastInsertedID();
};

#endif // SKSTCONTAINER_H
