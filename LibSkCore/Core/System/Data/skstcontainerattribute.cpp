#include "skstcontainerattribute.h"
#include "skstcontainer.h"
#include "skstorage.h"
#include "Core/System/Filesystem/skfile.h"
#include "Core/System/Filesystem/skfsutils.h"

SkStContainerAttribute::SkStContainerAttribute(SkStorage *storage, SkStContainer *container)
{
    index = nullptr;

    st = storage;
    c = container;

    dataFile = nullptr;
    relatingContainer = nullptr;
}

SkStContainerAttribute::SkStContainerAttribute(SkStorage *storage, SkStContainer *container, SkStContainerAttributeProps &properties)
{
    index = nullptr;

    st = storage;
    c = container;
    props = properties;

    setObjectName(c, props.name.c_str());

    dataFile = nullptr;
    relatingContainer = nullptr;
}

SkStContainerAttribute::~SkStContainerAttribute()
{
    if (dataFile)
    {
        if (dataFile->isOpen())
            dataFile->close();

        dataFile->destroyLater();
    }

    if (index)
    {
        if (index->isOpen())
            index->close();

        delete index;
    }
}

bool SkStContainerAttribute::usesDataFile()
{
    return (!SkVariant::isNumber(props.type) && !SkVariant::isBoolean(props.type));
}

bool SkStContainerAttribute::loadSchema(SkFile *schema)
{
    uint8_t sz = schema->readUInt8();
    schema->read(props.name, sz);

    setObjectName(c, props.name.c_str());

    props.type = static_cast<SkVariant_T>(schema->readUInt8());

    props.isNotNull = static_cast<bool>(schema->readUInt8());
    props.isUnique = static_cast<bool>(schema->readUInt8());
    props.hasIndex = static_cast<bool>(schema->readUInt8());
    props.isKeyElement = static_cast<bool>(schema->readUInt8());

    sz = schema->readUInt8();

    if (sz)
        schema->read(props.relatingContainerName, sz);

    FlatMessage("Attribute header LOADED: " << props.name << " ["
                << "t: " << SkVariant::variantTypeName(props.type) << ", "
                << "notNull: " << SkVariant::boolToString(props.isNotNull) << ", "
                << "unique: " << SkVariant::boolToString(props.isUnique) << ", "
                << "indexed: " << SkVariant::boolToString(props.hasIndex) << ", "
                << "keyElement: " << SkVariant::boolToString(props.isKeyElement) << "]");


    if (props.hasIndex && !openIndex())
        return false;

    return openDataFile();
}

bool SkStContainerAttribute::saveSchema(SkFile *schema)
{
    schema->writeUInt8(props.name.size());
    schema->write(props.name);

    schema->writeUInt8(props.type);

    schema->writeUInt8(props.isNotNull);
    schema->writeUInt8(props.isUnique);
    schema->writeUInt8(props.hasIndex);
    schema->writeUInt8(props.isKeyElement);

    if (props.relatingContainerName.isEmpty())
        schema->writeUInt8(0);

    else
    {
        schema->writeUInt8(props.relatingContainerName.size());
        schema->write(props.relatingContainerName);
    }

    FlatMessage("Attribute header SAVED: " << props.name << " ["
                << "t: " << SkVariant::variantTypeName(props.type) << ", "
                << "notNull: " << SkVariant::boolToString(props.isNotNull) << ", "
                << "unique: " << SkVariant::boolToString(props.isUnique) << ", "
                << "indexed: " << SkVariant::boolToString(props.hasIndex) << ", "
                << "keyElement: " << SkVariant::boolToString(props.isKeyElement) << "]");

    SkString containerPath = st->getWorkingDir();
    containerPath.append(c->name());
    containerPath.append("/");

    if (!buildDataFile() || !openDataFile())
        return false;

    if (props.hasIndex && !openIndex())
        return false;

    /*{
        SkString indexData = containerPath;
        indexData.append("index-");
        indexData.append(props.name);

        if (!SkFsUtils::exists(indexData.c_str()))
        {
            SkFsUtils::touch(indexData.c_str());
            FlatMessage("Index data INITIALIZED: " << props.name << " [" << indexData << "]");
        }
    }*/

    return true;
}

bool SkStContainerAttribute::buildDataFile()
{
    if (!usesDataFile())
        return true;

    SkString attributeDataPath = st->getWorkingDir();
    attributeDataPath.append(c->name());
    attributeDataPath.append("/");
    attributeDataPath.append("attribute-");
    attributeDataPath.append(props.name);

    if (!SkFsUtils::exists(attributeDataPath.c_str()))
    {
        if (!SkFsUtils::touch(attributeDataPath.c_str()))
            return false;

        FlatMessage("Attribute data CREATED: " << props.name << " [" << attributeDataPath << "]");
    }

    return true;
}

bool SkStContainerAttribute::openDataFile()
{
    if (!usesDataFile())
        return true;

    if (!dataFile)
    {
        SkString attributeDataPath = st->getWorkingDir();
        attributeDataPath.append(c->name());
        attributeDataPath.append("/");
        attributeDataPath.append("attribute-");
        attributeDataPath.append(props.name);

        dataFile = new SkFile;
        dataFile->setFilePath(attributeDataPath);
    }

    if (!dataFile->isOpen() && !dataFile->open())
        return false;

    FlatMessage("Attribute data file OPEN: " << dataFile->getFilePath());

    return true;
}

bool SkStContainerAttribute::openIndex()
{
    if (!props.hasIndex)
        return true;

    if (!index)
        index = new SkStIndex(this);

    if (index->isOpen())
        return true;

    bool ok = index->open();

    if (ok)
        FlatMessage("Index data INITIALIZED: " << props.name << " [" << index->filePath() << "]");

    return ok;
}

CStr *SkStContainerAttribute::name()
{
    return props.name.c_str();
}

SkVariant_T SkStContainerAttribute::attrType()
{
    return props.type;
}

bool SkStContainerAttribute::hasIndex()
{
    return props.hasIndex;
}

bool SkStContainerAttribute::isNotNull()
{
    return props.isNotNull;
}

bool SkStContainerAttribute::isUnique()
{
    return props.isUnique;
}

bool SkStContainerAttribute::isKeyElement()
{
    return props.isKeyElement;
}

CStr *SkStContainerAttribute::relatingContainerName()
{
    return props.relatingContainerName.c_str();
}

bool SkStContainerAttribute::setValue(SkVariant &v)
{
    SkVariant_T suppliedType = v.variantType();
    SkVariant_T requestedType = props.type;

    if (suppliedType != requestedType)
    {
        FlatError("Attribute TYPE mismatch [" << SkVariant::variantTypeName(suppliedType) << "]; "
                  << "data-type MUST be: " << SkVariant::variantTypeName(requestedType));

        return false;
    }
    // // //

    lastValue = v;
    return true;
}

uint64_t SkStContainerAttribute::size()
{
    return lastValue.size();
}

CVoid *SkStContainerAttribute::data()
{
    return lastValue.data();
}

bool SkStContainerAttribute::saveValue(SkFile *containerData, uint64_t tuplePosition)
{
    if (props.isNotNull && lastValue.isEmpty())
    {
        FlatError("Attribute is Null [isNotNull=true]: " << name());
        return false;
    }

    cout << name() << ": " << lastValue.toString() << "\n";

    uint64_t value = 0;

    if (usesDataFile())
    {
        dataFile->seek(dataFile->size());
        value = dataFile->pos();

        dataFile->writeUInt32(lastValue.size());
        dataFile->write(lastValue.data(), lastValue.size());
    }

    else
        value = lastValue.toUInt64();

    containerData->writeUInt64(value);

    /*if (props.hasIndex)
        index->add(value, tuplePosition);*/

    return true;
}

/*bool SkStContainerAttribute::addToIndex(uint64_t value)
{
    SkString indexPath = st->getWorkingDir();
    indexPath.append(c->name());
    indexPath.append("/");
    indexPath.append("index-");
    indexPath.append(props.name);

    SkFile *i = new SkFile;
    i->setFilePath(indexPath.c_str());

    if (!i->open())
        return false;

    i->close();
    i->destroyLater();

    return true;
}*/

SkStorage *SkStContainerAttribute::storage()
{
    return st;
}

SkStContainer *SkStContainerAttribute::container()
{
    return c;
}
