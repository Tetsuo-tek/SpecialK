#ifndef SKSTCONTAINERATTRIBUTE_H
#define SKSTCONTAINERATTRIBUTE_H

#include "skstindex.h"

struct SkStContainerAttributeProps
{
    SkString name;
    SkVariant_T type;

    bool hasIndex;
    bool isNotNull;
    bool isUnique;
    bool isKeyElement;

    SkString relatingContainerName;
};

class SkStContainerAttribute extends SkFlatObject
{
    public:
        SkStContainerAttribute(SkStorage *storage, SkStContainer *container);
        SkStContainerAttribute(SkStorage *storage, SkStContainer *container, SkStContainerAttributeProps &properties);
        ~SkStContainerAttribute();

        CStr *name();
        SkVariant_T attrType();

        bool hasIndex();
        bool isNotNull();
        bool isUnique();
        bool isKeyElement();

        CStr *relatingContainerName();

        bool setValue(SkVariant &v);
        uint64_t size();
        CVoid *data();

        bool loadSchema(SkFile *schema);
        bool saveSchema(SkFile *schema);

        bool saveValue(SkFile *containerData, uint64_t tuplePosition);

        bool usesDataFile();

        SkStorage *storage();
        SkStContainer *container();

    private:
        SkStorage *st;
        SkStContainer *c;
        SkStContainer *relatingContainer;

        SkStContainerAttributeProps props;

        SkFile *dataFile;//If it is a number or a boolean it is NULL
        SkStIndex *index;

        SkVariant lastValue;

        bool buildDataFile();
        bool openDataFile();

        bool openIndex();

        //bool addToIndex(uint64_t value);
};

#endif // SKSTCONTAINERATTRIBUTE_H
