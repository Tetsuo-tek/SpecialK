﻿/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTDEVICE_H
#define SKABSTRACTDEVICE_H

/**
 * @file skabstractdevice.h
 */

#include "Core/Object/skobject.h"
#include "Core/System/skabstractflatdevice.h"

class SkDataBuffer;
class SkRingBuffer;

/**
 * @brief The SkAbstractDevice class
 */
class SPECIALK SkAbstractDevice extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkAbstractDevice, SkObject);

        /**
         * @brief Checks if the device is open
         * @return true if it is open, otherwise false
         */
        bool isOpen();

        /**
         * @brief Checks if the implemented device is squencial (not seekable)
         * @return true if it is sequencial
         */
        virtual bool isSequencial()                         {return false;}

        /**
         * @brief Checks if it is possible to read a line, terminating with '\n'
         * @return true if there is a line available
         */
        virtual bool canReadLine(uint64_t &)                  {return false;}

        /**
         * @brief Checks if device is ready for write operation
         * @return true if device is ready, otherwise false
         */
        virtual bool canWrite()                             {return isOpen();}

        /**
         * @brief Checks if device is ready for write operation
         * @return true if device is ready, otherwise false
         */
        virtual bool canRead()                              {return (bytesAvailable()>0);}

        /**
         * @brief Flushes input/output of the device
         * @return true if there are not errors
         */
        virtual bool flush()                                {return false;}

        /**
         * @brief Gets the size of the not sequencial device
         * @return the size if it is valid, otherwise -1
         */
        virtual int64_t size()                              {return -1;}

        /**
         * @brief Gets the cursor position of the not sequencial device
         * @return the current position if it is valid, otherwise -1
         */
        virtual int64_t pos()                               {return -1;}

        /**
         * @brief Gets available data to read from the device
         * @return the available size to read
         */
        virtual uint64_t bytesAvailable()                     {return 0;}

        /**
         * @brief Reset the position to 0 on not sequencial devices
         * @return true if the device is not sequencial and there are not errors,
         * otherwise false
         */
        virtual bool rewind()                               {return false;}

        /**
         * @brief Seeks a position not sequencial devices
         * @return true if the seek operation is executed with success, otherwise false
         */
        virtual bool seek(uint64_t)                           {return false;}

        /**
         * @brief Checks if the position is at the end of the stream on not
         * sequencial devices
         * @return true if the position is at the end and there are not errors,
         * otherwise false
         */
        virtual bool atEof()                                {return false;}

        /**
         * @brief Closes the device
         */
        virtual void close()                                {}

        /**
         * @brief Reads from a device and write into this device
         * @param dev the device to read from
         * @param len the length to read and write
         * @return true if there are not errors, otherwise false
         */
        bool readFromWriteHere(SkAbstractDevice *dev, uint64_t &len);

        /**
         * @brief Reads from this device and write to another device
         * @param dev the device to write to
         * @param len the length to read and write
         * @return true if there are not errors
         */
        bool readHereWriteTo(SkAbstractDevice *dev, uint64_t len=0);

        /**
         * @brief Reads one char of data
         * @param c the char reference
         * @param onlyPeek if true data are not consumed, otherwise is normal read
         * @return true if there are not errors, otherwise false
         */
        bool read(char &c, bool onlyPeek=false);

        /**
         * @brief Reads a block of data
         * @param data the char data pointer to fill with read data
         * @param len the length to read
         * @param onlyPeek if true data are not consumed, otherwise is normal read
         * @return true if there are not errors, otherwise false
         */
        bool read(char *data, uint64_t &len, bool onlyPeek=false);

        //THESE READING/PEEKING METHs WILL APPEND DATA TO TARGET
        //(SkString, SkDataBuffer AND SkRingBuffer)

        /**
         * @brief Reads a block of data inserting it to a string;
         * data will be appended to target
         * @warning it is required that the data are valid characters for strings
         * @param s the string reference to fill with data
         * @param len the length to read; if the length is 0 all data available is read
         * @param onlyPeek if true data are not consumed, otherwise is normal read
         * @return true if there are not errors, otherwise false
         */
        bool read(SkString &s, uint64_t len=0, bool onlyPeek=false);

        /**
         * @brief Reads a block of data inserting it to a data-buffer;
         * data will be appended to target
         * @param buf the data-buffer pointer to fill
         * @param len the length to read; if the length is 0 all data available is read
         * @param onlyPeek if true data are not consumed, otherwise is normal read
         * @return true if there are not errors, otherwise false
         */
        bool read(SkDataBuffer *buf, uint64_t len=0, bool onlyPeek=false);

        /**
         * @brief Reads a block of data inserting it to a ring-buffer;
         * data will be appended to target
         * @param buf the ring-buffer pointer to fill
         * @param len the length to read; if the length is 0 all data available is read
         * @param onlyPeek if true data are not consumed, otherwise is normal read
         * @return true if there are not errors, otherwise false
         */
        bool read(SkRingBuffer *buf, uint64_t len=0, bool onlyPeek=false);

        /**
         * @brief Peeks a block of data inserting it to a string;
         * data will be appended to target
         * @warning it is required that the data are valid characters for strings
         * @param s the string reference to fill with data
         * @param len the length to read; if the length is 0 all data available is read
         * @return true if there are not errors, otherwise false
         */
        bool peek(SkString &s, uint64_t len=0);

        /**
         * @brief Peeks a block of data inserting it to a data-buffer;
         * data will be appended to target
         * @param buf the data-buffer pointer to fill
         * @param len the length to read; if the length is 0 all data available is read
         * @return true if there are not errors, otherwise false
         */
        bool peek(SkDataBuffer *buf, uint64_t len=0);

        /**
         * @brief Reads a block of data inserting it to a ring-buffer;
         * data will be appended to target
         * @param buf the ring-buffer pointer to fill
         * @param len the length to read; if the length is 0 all data available is read
         * @return true if there are not errors, otherwise false
         */
        bool peek(SkRingBuffer *buf, uint64_t len=0);

        //readAll AND peekAll (IN THIS CASE HERE HAVING THE SAME SEMANTIC) RETURN
        //ALL DATA AVAILABLE IN THE DEVICE
        //
        //IF THE DEV IS NOT SEQUENCIAL, "AVAILABLE" MEANS size()
        //
        //IF THE DEV IS NOT SEQUENCIAL, IT WILL HOLD THE POSITION BEFORE TO READ,
        //SO, WHEN THE OPERATION IS FINISHED, IT WILL SEEK ON THE HOLDED-POS

        /**
         * @brief Reads all available data inserting it to a string;
         * @param s the string reference to fill with data
         * @return true if there are not errors, otherwise false
         */
        bool readAll(SkString &s);

        /**
         * @brief Reads all available data inserting it to a data-buffer;
         * @param buf the data-buffer pointer to fill
         * @return true if there are not errors, otherwise false
         */
        bool readAll(SkDataBuffer *buf);

        /**
         * @brief Reads all available data inserting it to a ring-buffer;
         * @param buf the ring-buffer pointer to fill
         * @return true if there are not errors, otherwise false
         */
        bool readAll(SkRingBuffer *buf);

        /**
         * @brief Peeks all available data inserting it to a string;
         * @param s the string reference to fill with data
         * @return true if there are not errors, otherwise false
         */
        bool peekAll(SkString &s);

        /**
         * @brief Peeks all available data inserting it to a data-buffer;
         * @param buf the data-buffer pointer to fill
         * @return true if there are not errors, otherwise false
         */
        bool peekAll(SkDataBuffer *buf);

        /**
         * @brief Peeks all available data inserting it to a ring-buffer;
         * @param buf the ring-buffer pointer to fill
         * @return true if there are not errors, otherwise false
         */
        bool peekAll(SkRingBuffer *buf);

        /**
         * @brief Writes a singe char on device
         * @param c the char to write
         * @return true if there are not errors, otherwise false
         */
        bool write(char c);

        /**
         * @brief Writes a string on device
         * @param s the string to write
         * @return true if there are not errors, otherwise false
         */
        bool write(SkString &s);

        /**
         * @brief Writes a data block on device
         * @param data the char data pointer to write
         * @param len the length to write
         * @return true if there are not errors, otherwise false
         */
        bool write(CStr *data, uint64_t len);

        /**
         * @brief Writes the data-buffer contents on device
         * @param buf the data-buffer pointer with data to write
         * @param len the length to write; if the length is 0 all data will be write
         * @return true if there are not errors, otherwise false
         */
        bool write(SkDataBuffer *buf, uint64_t len=0);

        /**
         * @brief Writes the ring-buffer contents on device
         * @param buf the ring-buffer pointer with data to write
         * @param len the length to write; if the length is 0 all data will be write
         * @return true if there are not errors, otherwise false
         */
        bool write(SkRingBuffer *buf, uint64_t len=0);

        //THESE METHs W/R PRIMITIVE C TYPES BINARY-RAW-CASTED TO/FROM LE-BYTEARRAY
        //add*  METHs PRODUCE DATA
        //get*  METHs CONSUME DATA
        //peek* METHs DOES NOT CONSUME DATA

        /**
         * @brief Writes a uint8_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeUInt8(uint8_t val);

        /**
         * @brief Reads a uint8_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint8_t readUInt8();

        /**
         * @brief Peeks a uint8_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint8_t peekUInt8();

        /**
         * @brief Writes a int8_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeInt8(int8_t val);

        /**
         * @brief Reads a int8_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int8_t readInt8();

        /**
         * @brief Peeks a int8_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int8_t peekInt8();

        /**
         * @brief Writes a uint16_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeUInt16(uint16_t val);

        /**
         * @brief Reads a uint16_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint16_t readUInt16();

        /**
         * @brief Peeks a uint16_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint16_t peekUInt16();

        /**
         * @brief Writes a int16_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeInt16(int16_t val);

        /**
         * @brief Reads a int16_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int16_t readInt16();

        /**
         * @brief Peeks a int16_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int16_t peekInt16();

        /**
         * @brief Writes a uint32_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeUInt32(uint32_t val);

        /**
         * @brief Reads a uint32_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint32_t readUInt32();

        /**
         * @brief Peeks a uint32_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint32_t peekUInt32();

        /**
         * @brief Writes a int32_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeInt32(int32_t val);

        /**
         * @brief Reads a int32_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int32_t readInt32();

        /**
         * @brief Peeks a int32_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int32_t peekInt32();

        /**
         * @brief Writes a uint64_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeUInt64(uint64_t val);

        /**
         * @brief Reads a uint64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint64_t readUInt64();

        /**
         * @brief Peeks a uint64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        uint64_t peekUInt64();

        /**
         * @brief Writes a int64_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeInt64(int64_t val);

        /**
         * @brief Reads a int64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int64_t readInt64();

        /**
         * @brief Peeks a int64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        int64_t peekInt64();

        /**
         * @brief Writes a uint64_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        //bool writeSizeT(uint64_t val);

        /**
         * @brief Reads a uint64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        //uint64_t readSizeT();

        /**
         * @brief Peeks a uint64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        //uint64_t peekSizeT();

        /**
         * @brief Writes a int64_t value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        //bool writeSSizeT(int64_t val);

        /**
         * @brief Reads a int64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        //int64_t readSSizeT();

        /**
         * @brief Peeks a int64_t value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        //int64_t peekSSizeT();

        /**
         * @brief Writes a float value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeFloat(float val);

        /**
         * @brief Reads a float value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        float readFloat();

        /**
         * @brief Peeks a float value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        float peekFloat();

        /**
         * @brief Writes a double value in Little-Endian mode to the the devices
         * @param val the value to  write
         * @return true if there are not errors, otherwise false
         */
        bool writeDouble(double val);

        /**
         * @brief Reads a double value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        double readDouble();

        /**
         * @brief Peeks a double value in Little-Endian mode from the device
         * @return the value, otherwise 0
         */
        double peekDouble();

        /**
         * @brief Gets the TX counter value
         * @return bytes count
         */
        uint64_t getTxDataCounter();

        /**
         * @brief Reset the TX counter
         */
        void resetTxDataCounter();

        /**
         * @brief Gets the RX counter value
         * @return bytes count
         */
        uint64_t getRxDataCounter();

        /**
         * @brief Reset the RX counter
         */
        void resetRxDataCounter();

        /**
         * @brief Reset the TX and RX counters
         */
        void resetAllDataCounters();

        /**
         * @brief Gets the current state of the device
         * @return the state of the device
         */
        SkDeviceState currentState();

        /**
         * @brief Gets the current state as string of the device
         * @return the state as string of the device
         */
        CStr *getCurrentStateString();

        /**
         * @brief Gets the error as string
         * @param error the error value
         * @return the string representation of the error
         */
        static CStr *getErrorString(SkDeviceError error);

        /**
         * @brief Triggered when there are data available on sequencial
         * devices
         */
        Signal(readyRead);

        /**
         * @brief Triggered when data are written
         */
        Signal(bytesWritten);

        /**
         * @brief Triggered when data are read
         */
        Signal(bytesRead);

        /**
         * @brief Triggered when the device state is changed
         */
        Signal(stateChanged);

        /**
         * @brief Triggered when the device makes errors
         */
        Signal(errorOccurred);

        Signal(open);
        Signal(closed);

    protected:
        virtual bool readInternal(char *, uint64_t &, bool=false)     {return false;}
        virtual bool writeInternal(CStr *, uint64_t &)                {return false;}

        void notifyState(SkDeviceState st);
        void notifyError(SkDeviceError err, CStr *prettyFrom, CStr *comment=nullptr);

        void notifyRead(uint64_t &sz);
        void notifyWrite(uint64_t &sz);

    private:
        SkDeviceState state;
        uint64_t wroteDataCounter;
        uint64_t readDataCounter;
};

#endif // SKABSTRACTDEVICE_H
