#include "skmutexlocker.h"

SkMutexLocker::SkMutexLocker(SkMutex *m)
{
    ex = m;
    ex->lock();
}

SkMutexLocker::~SkMutexLocker()
{
    ex->unlock();
}
