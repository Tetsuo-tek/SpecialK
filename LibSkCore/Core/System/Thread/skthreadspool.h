/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTHREADSPOOL_H
#define SKTHREADSPOOL_H

#include "Core/System/Thread/skthread.h"

enum SkThreadsPoolMode
{
    TP_SEQUENCIAL,
    TP_PARALLEL
};

class SPECIALK SkThreadsPool extends SkObject
{
    public:
        /**
         * @brief Constructor
         */
        Constructor(SkThreadsPool, SkObject);

        /**
         * @brief Sets the pooling mode
         * @param m the pooling mode
         */
        void setMode(SkThreadsPoolMode m);

        /**
         * @brief Adds a new thread to the pool
         * @param th the thread to add
         */
        virtual void add(SkThread *th);

        /**
         * @brief Searches for the index of a thread stored in the pool
         * @param th the thread to search
         * @return the index if it is found, otherwise -1
         */
        int64_t indexOf(SkThread *th);

        bool isEmpty();
        uint64_t count();

        /**
         * @brief Removes a thread from the pool
         * @param th the thread to remove
         */
        virtual void del(SkThread *th);
        virtual void del(uint64_t index);

        /**
         * @brief Gets the thread object stored in the pool from index
         * @param index the index of the thread stored in the pool
         * @return the thread object if the index is valid, otherwise false
         */
        virtual SkThread *get(uint64_t index);

        /**
         * @brief Checks if the threads pool is running
         * @return true if it is running, otherwise false
         */
        bool isRunning();

        /**
         * @brief Triggered when the thread is started
         */
        Signal(started);

        /**
         * @brief Triggered when the thread is finished
         */
        Signal(finished);

        /**
         * @brief Invoked to start the threads pool
         */
        Slot(start);

        /**
         * @brief Invoked to quit the threads pool
         */
        Slot(quit);

        Slot(onThreadRunning);
        Slot(onFinish);

    protected:
        SkVector<SkThread *> threads;

    private:
        SkThreadsPoolMode poolingMode;
        SkThread *nextSequencialTh;
        bool poolIsRunning;
        int closingThread;

        virtual void onStart(){}
        virtual void onStop(){}

};

#endif // SKTHREADSPOOL_H
