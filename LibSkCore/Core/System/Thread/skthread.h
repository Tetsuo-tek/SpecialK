/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTHREAD_H
#define SKTHREAD_H

#include "Core/Object/skobject.h"
#include "Core/App/skeventloop.h"

class SPECIALK SkThread extends SkObject
{
    public:
        Constructor(SkThread, SkObject);

        //MAKE THIS METH ONLY BEFORE start()
        //IF NOT CALL THIS METH, DEFAULT SPEED WILL BE INHERITED FROM skApp

        /**
         * @brief Sets the looping intervals and the mode for the internal eventLoop
         * that the thread will create
         * @warning call this method before thread is started, otherwise it will
         * use the skApp eventLoop intervals and mode as default
         * @param fastTickInterval microseconds interval for FAST-ZONE
         * @param slowTickInterval microseconds interval for SLOW-ZONE
         * @param timerMode looping mode
         */
        void setLoopIntervals(ulong fastTickInterval,
                              ulong slowTickInterval,
                              SkLoopTimerMode timerMode=SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING);

        /**
         * @brief Sets the thread priority using standard POSIX rules
         * @warning this method is disabled and does nothing for now
         * @warning call this method before the thread is started, otherwise
         * the value will not be used
         * @param priority the value of the theread priority
         */
        void setPriority(int /*policy*/, int priority=0);

        /**
         * @brief Checks if the thread is running
         * @return true if it is running, otherwise false
         */
        bool isRunning();

        /**
         * @brief Waits for thread finish
         * @return true if the thread is finished correctly, otherwise false
         */
        bool wait();

        /**
         * @brief Gets the internal eventLoop
         * @return the internall eventLoop
         */
        SkEventLoop *thEventLoop();

        /**
         * @brief Gets the threadID created by this thread object
         * @warning returned value is not valid if the thread is not started,
         * and absolutely it is NOT the same returned by the method getThreadID()'
         * inherited from SkObject
         * @return the threadID that is running, or invalid value if it is not started
         */
        SkThID getExecutionThID();

        //RETURN THE SkThID FOR THE CALLING SCOPE
        /**
         * @brief Gets the current threadID for the calling code scope
         * @return the current threadID
         */
        static SkThID currentThID();

        /**
         * @brief Gets the internal SkStdThread (std::thread) for customizing purposes
         * @return the SkStdThread reference
         */
        SkStdThread &getStdThread();

        /**
         * @brief Triggered when the thread is started and after the virtual
         * method 'customRunSetup()'
         */
        Signal(started);

        /**
         * @brief Triggered when the thread is running and after the virtual
         * method 'customRunning()'
         */
        Signal(running);

        /**
         * @brief Triggered when the thread is running and before the virtual
         * method 'customClosing()'
         */
        Signal(finished);

        /**
         * @brief Invoked to start the thread, staring its eventLoop
         */
        Slot(start);

        /**
         * @brief Invoked to quit the thread, stopping its eventLoop
         * @warning when the internal eventLoop die it destroies all objects living
         * in the thread
         */
        Slot(quit);

        Slot(onEventLoopQuit);

    protected:
        //run() METH COULD BE OVERRIDED, IF YOU WANT CUSTOMIZE THE TH
        //
        //THE METHs BELOW ARE NOT CALLED IF run() IS OVERRIDED
        //
        virtual void run();

        //OVERRIDING THESE METHs WE CAN CUSTOMIZE THE INIT AND CLOSE TH-PHASEs
        //HERE, THE TH USEs ITs EventLoop INITIALIZED INSIDE run(), IF IT IS NOT OVERRIDED
        virtual bool customRunSetup()                       {return true;}//MUST DEBUG IF RETURN FALSE
        virtual void customRunning()                        {}
        virtual void customClosing()                        {}

        //default is ENABLED (useful to exec manually by derivatives class)
        void disableEventLoopExec();

    private:
        SkStdThread th;

        int schedulerPolicy;
        sched_param schedulerParams;

        SkMutex thEx;
        SkThID tidExecution;
        SkEventLoop *thLoop;
        bool executeLoop;

        ulong fastInterval_USECS;
        ulong slowInterval_USECS;
        SkLoopTimerMode loopTimerMode;

};

#endif // SKTHREAD_H
