#include "skthread.h"
#include "Core/App/skapp.h"

void SkThreadDestroyInternal(SkObject *obj)
{
    SkThread *f = static_cast<SkThread *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkThreadDestroyInternal()");

    if (f->isRunning())
    {ObjectError_EXT(f, "Thread is running on its destruction! Problems could be happen ..");}
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkThread, setLoopIntervals, Arg_UInt, Arg_UInt, Arg_Enum(SkLoopTimerMode))
DeclareMeth_INSTANCE_RET(SkThread, isRunning, bool)
DeclareMeth_INSTANCE_RET(SkThread, wait, bool)
DeclareMeth_INSTANCE_RET(SkThread, thEventLoop, SkEventLoop *)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkThread, SkObject,
                {
                    AddMeth_INSTANCE_VOID(SkThread, setLoopIntervals);
                    AddMeth_INSTANCE_RET(SkThread, isRunning);
                    AddMeth_INSTANCE_RET(SkThread, wait);
                    AddMeth_INSTANCE_RET(SkThread, thEventLoop);
                })
{
    schedulerPolicy = -1;

    tidExecution = SkThID();
    thLoop = nullptr;

    fastInterval_USECS = 0;
    slowInterval_USECS = 0;
    loopTimerMode = SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING;

    executeLoop = true;

    SignalSet(started);
    SignalSet(running);
    SignalSet(finished);

    SlotSet(start);
    SlotSet(quit);

    SlotSet(onEventLoopQuit);

    addDtorCompanion(SkThreadDestroyInternal);
}

void SkThread::setLoopIntervals(ulong fastTickInterval,
                                ulong slowTickInterval,
                                SkLoopTimerMode timerMode)
{
    fastInterval_USECS = fastTickInterval;
    slowInterval_USECS = slowTickInterval;
    loopTimerMode = timerMode;
}

void SkThread::setPriority(int /*policy*/, int /*priority*/)
{
#if defined(UMBA)
    schedulerParams./*__sched_priority*/sched_priority = priority;
#endif
}

SlotImpl(SkThread, start)
{
    SilentSlotArgsWarning();

    if (isRunning())
        return;

    if (fastInterval_USECS == 0)
    {
        fastInterval_USECS = skApp->getFastInterval();
        slowInterval_USECS = skApp->getSlowInterval();
        loopTimerMode = skApp->getTimerMode();

        SkString mode;

        if (loopTimerMode == SkLoopTimerMode::SK_TIMEDLOOP_RT)
            mode = "EventLoop-RtTimer";

        else if (loopTimerMode == SkLoopTimerMode::SK_TIMEDLOOP_SLEEPING)
            mode = "EventLoop-SleepingTimer";

        else if (loopTimerMode == SkLoopTimerMode::SK_EVENTLOOP_WAITING)
            mode = "EventLoop-Waiter";

        else if (loopTimerMode == SkLoopTimerMode::SK_FREELOOP)
            mode = "EventLoop-Extern";

        ObjectWarning("Setup default loop period and type from skApp: "
                      << "fast: " << fastInterval_USECS << " us; "
                      << "slow: " << slowInterval_USECS << " us; "
                      << "mode: " << mode);
    }

    th = std::thread(&SkThread::run, this);
}

SlotImpl(SkThread, quit)
{
    SilentSlotArgsWarning();

    if (!isRunning())
    {
        ObjectError("Thread is NOT running");
        return;
    }

    thLoop->quit();
}

void SkThread::disableEventLoopExec()
{
    executeLoop = false;
}

void SkThread::run()
{
    thEx.lock();

    thLoop = new SkEventLoop(this);
    tidExecution = thLoop->threadID();

    skApp->registerThread(this);
    thLoop->setObjectName(this, "Loop");

#if defined(__linux__)
    pthread_setname_np(pthread_self(), objectName());

    /*if(schedulerPolicy > -1)
        if (pthread_setschedparam(pthread_self(), schedulerPolicy, &schedulerParams) != 0)
            ObjectError("CANNOT set thread-scheduling priority: " << strerror(errno));*/
#endif

    thEx.unlock();
    thLoop->initLoop(fastInterval_USECS, slowInterval_USECS, loopTimerMode);

    if (customRunSetup())
    {
        SkEventLoopOnClosingSlot onClosingSlot;
        onClosingSlot.owner = this;
        onClosingSlot.name = "onEventLoopQuit";

        started(this);
        customRunning();        
        running(this);

        if (executeLoop)
            thLoop->exec(&onClosingSlot);

        finished(this);
    }

    else
        ObjectError("SkThread '" << this->objectName() << "' returned false on custom setup");

    thEx.lock();
    ObjectDebug("CLOSING TH ..");
    skApp->deregisterThread(this);
    delete thLoop;
    thLoop = nullptr;
    tidExecution = SkThID();
    thEx.unlock();
}

SlotImpl(SkThread, onEventLoopQuit)
{
    SilentSlotArgsWarning();
    customClosing();
}

bool SkThread::isRunning()
{
    if (SkThread::currentThID() == tidExecution)
        return (thLoop != nullptr);

    SkMutexLocker locker(&thEx);
    return (thLoop != nullptr);
}

bool SkThread::wait()
{
    if (!isRunning())
    {
        ObjectError("Thread is NOT running");
        return false;
    }

    ObjectDebug("Waiting for thread finish ..");

    if (thLoop == nullptr)
    {
        ObjectWarning("thLoop is ALREADY destroyed");
        return false;
    }

    if (!th.joinable())
    {
        ObjectError("Thread is NOT joinable");
        return false;
    }

    th.join();
    ObjectDebug("Thread closed");

    return true;
}

SkEventLoop *SkThread::thEventLoop()
{
    return thLoop;
}

SkThID SkThread::getExecutionThID()
{
    return tidExecution;
}

SkThID SkThread::currentThID()
{
    return std::this_thread::get_id();
}

SkStdThread &SkThread::getStdThread()
{
    return th;
}
