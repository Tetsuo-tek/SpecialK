#include "skthreadspool.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkThreadsPoolMode)
{
    SetupEnumWrapper(SkThreadsPoolMode);

    SetEnumItem(SkThreadsPoolMode::TP_SEQUENCIAL);
    SetEnumItem(SkThreadsPoolMode::TP_PARALLEL);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkThreadsPool, setMode, Arg_Enum(SkThreadsPoolMode))
DeclareMeth_INSTANCE_VOID(SkThreadsPool, add, Arg_Custom(SkThread))
DeclareMeth_INSTANCE_RET(SkThreadsPool, indexOf, int64_t, Arg_Custom(SkThread))
DeclareMeth_INSTANCE_VOID(SkThreadsPool, del, Arg_Custom(SkThread))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkThreadsPool, del, 1, Arg_UInt64)
//DeclareMeth_INSTANCE_RET(SkThreadsPool, getThread, SkThread*, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkThreadsPool, isRunning, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkThreadsPool, SkObject,
                {
                    AddEnumType(SkThreadsPoolMode);

                    AddMeth_INSTANCE_VOID(SkThreadsPool, setMode);
                    AddMeth_INSTANCE_VOID(SkThreadsPool, add);
                    AddMeth_INSTANCE_RET(SkThreadsPool, indexOf);
                    AddMeth_INSTANCE_VOID(SkThreadsPool, del);
                    AddMeth_INSTANCE_VOID_OVERLOAD(SkThreadsPool, del, 1);
                    //AddMeth_INSTANCE_RET(SkThreadsPool, getThread);
                    AddMeth_INSTANCE_RET(SkThreadsPool, isRunning);
                })
{
    poolingMode = SkThreadsPoolMode::TP_PARALLEL;
    poolIsRunning = false;
    nextSequencialTh = nullptr;
    closingThread = 0;

    SignalSet(started);
    SignalSet(finished);
    SlotSet(start);
    SlotSet(onThreadRunning);
    SlotSet(onFinish);
    SlotSet(quit);
}

void SkThreadsPool::setMode(SkThreadsPoolMode m)
{
    poolingMode = m;
}

void SkThreadsPool::add(SkThread *th)
{
    if (threads.contains(th))
    {
        ObjectError("The Adding thread is ALREADY in the ThreadsPool");
        return;
    }

    threads.append(th);
}

int64_t SkThreadsPool::indexOf(SkThread *th)
{
    return threads.indexOf(th);
}

bool SkThreadsPool::isEmpty()
{
    return threads.isEmpty();
}

uint64_t SkThreadsPool::count()
{
    return threads.count();
}

void SkThreadsPool::del(SkThread *th)
{
    if (!threads.contains(th))
    {
        ObjectError("The Removing thread is NOT in the ThreadsPool");
        return;
    }

    threads.remove(th);
}

void SkThreadsPool::del(uint64_t index)
{
    if (index < threads.count())
        threads.removeAt(index);
}

SkThread *SkThreadsPool::get(uint64_t index)
{
    if (index < threads.count())
        return threads.at(index);

    return nullptr;
}

bool SkThreadsPool::isRunning()
{
    return poolIsRunning;
}

SlotImpl(SkThreadsPool, start)
{
    SilentSlotArgsWarning();

    if (poolIsRunning)
    {
        ObjectError("The ThreadsPool is ALREADY running");
        return;
    }

    if (threads.isEmpty())
    {
        ObjectError("The ThreadsPool is EMPTY");
        return;
    }

    nextSequencialTh = nullptr;

    for(uint64_t i=0; i<threads.count(); i++)
    {
        Attach(threads[i], running, this, onThreadRunning, SkOneShotQueued);
        //Attach(threads[i], finished, this, onFinish, SkAttachMode::SkOneShotQueued);
    }

    if (poolingMode == SkThreadsPoolMode::TP_PARALLEL)
    {
        ObjectMessage("Starting PARALLEL ThreadsPool ..");

        for(uint64_t i=0; i<threads.count(); i++)
            threads[i]->start();
    }

    else if (poolingMode == SkThreadsPoolMode::TP_SEQUENCIAL)
    {
        ObjectMessage("Starting SEQUENCIAL ThreadsPool ..");
        nextSequencialTh = threads.first();
        nextSequencialTh->start();
    }
}

static uint64_t i = 0;

SlotImpl(SkThreadsPool, onThreadRunning)
{
    SilentSlotArgsWarning();

    if (poolIsRunning)
        return;

    closingThread = 0;

    if (poolingMode == SkThreadsPoolMode::TP_PARALLEL)
    {
        i++;

        if (i == threads.count())
        {
            poolIsRunning = true;
            i = 0;

            ObjectMessage("PARALLEL ThreadsPool STARTED");
            onStart();
            started();
        }
    }

    else if (poolingMode == SkThreadsPoolMode::TP_SEQUENCIAL)
    {
        if (nextSequencialTh)
        {
            uint64_t id = static_cast<uint64_t>(threads.indexOf(nextSequencialTh));
            ObjectMessage("Thread started [ID: " << id << "]: " << nextSequencialTh->objectName());

            if (id < threads.count() - 1)
            {
                nextSequencialTh = threads.at(id+1);
                nextSequencialTh->start();
            }

            else
            {
                poolIsRunning = true;
                nextSequencialTh = nullptr;

                ObjectMessage("SEQUENCIAL ThreadsPool STARTED");
                onStart();
                started();
            }
        }
    }
}

SlotImpl(SkThreadsPool, quit)
{
    SilentSlotArgsWarning();

    if (!poolIsRunning)
    {
        ObjectError("The ThreadsPool is ALREADY stopped");
        return;
    }

    ObjectMessage("Closing ThreadsPool ..");

    onStop();

    closingThread = threads.count()-1;

    for(uint64_t i=0; i<threads.count(); i++)
        Attach(threads[i], finished, this, onFinish, SkAttachMode::SkOneShotQueued);

    SkThread *last = threads.last();
    eventLoop()->invokeSlot(last->quit_SLOT, last, last);
}

SlotImpl(SkThreadsPool, onFinish)
{
    SilentSlotArgsWarning();

    //LET ME HERE
    SkThread *last = threads[closingThread];
    last->getStdThread().join();
    //

    closingThread--;

    if (closingThread == -1)
    {
        ObjectMessage("ThreadsPool CLOSED");

        poolIsRunning = false;
        finished();
    }

    else
    {
        SkThread *last = threads[closingThread];
        eventLoop()->invokeSlot(last->quit_SLOT, last, last);
        last->quit();
    }
}
