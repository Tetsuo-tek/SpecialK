#include "skfifo.h"
#include "Core/System/Filesystem/skfsutils.h"
#include <sys/fcntl.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkFifo, setFilePath, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkFifo, getFilePath, CStr*)
DeclareMeth_INSTANCE_RET(SkFifo, open, bool, Arg_Enum(SkFifoMode))
DeclareMeth_INSTANCE_RET(SkFifo, remove, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkFifo, SkAbstractFifo,
                {
                    AddMeth_INSTANCE_VOID(SkFifo, setFilePath);
                    AddMeth_INSTANCE_RET(SkFifo, getFilePath);
                    AddMeth_INSTANCE_RET(SkFifo, open);
                    AddMeth_INSTANCE_RET(SkFifo, remove);
                })
{}

void SkFifo::setFilePath(CStr *path)
{
    filePath = path;
}

CStr *SkFifo::getFilePath()
{
    return filePath.c_str();
}

bool SkFifo::open(SkFifoMode m)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    openMode = SkFifoMode::FFM_NOMODE;

    if (filePath.isEmpty())
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__ ,
                    "Cannot open file device with an EMPTY name");
        return false;
    }

    int fd = -1;

    if (m == SkFifoMode::FFM_ONLYREAD)
        fd = ::open(filePath.c_str(), O_RDONLY);

    else
        fd = ::open(filePath.c_str(), O_WRONLY);

    if (fd > 0)
    {
        openMode = m;
        ObjectDebug("Fifo device is OPEN [mode: " << getCurrentModeString() << "; path: " << filePath << "]");
        setFileDescriptor(fd);
        return true;
    }

    SkString errMsg = "Cannot open fifo device: ";
    errMsg.append(filePath);

    notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, errMsg.c_str());
    return false;
}

bool SkFifo::remove()
{
    if (filePath.isEmpty())
        return false;

    if (isOpen())
        this->close();

    return SkFsUtils::rm(filePath.c_str());
}

