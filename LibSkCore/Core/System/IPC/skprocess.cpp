#include "skprocess.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/skosenv.h"

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkProcessWatchDog, SkThread)
{
    watchingTermination = false;
    killed = false;
    processID = 0;
    SlotSet(oneSecTick);
}

void SkProcessWatchDog::setup(pid_t pid, UInt seconds)
{
    processID = pid;
    timeoutInterval = seconds;
}

void SkProcessWatchDog::watchForTermination()
{
    watchingTermination = true;
}

SlotImpl(SkProcessWatchDog, oneSecTick)
{
    SilentSlotArgsWarning();

    if (!processID)
        return;

    if (chrono.stop() > timeoutInterval)
    {
        if (watchingTermination && !killed)
        {
            killed = true;
            kill(processID, SIGKILL);
            ObjectMessage("Dog has KILLED the pid: " << processID);
        }
    }
}

void SkProcessWatchDog::customRunning()
{
    Attach(thEventLoop()->oneSecZone_SIG, pulse, this, oneSecTick, SkDirect);
    ObjectMessage("Dog is WATCHING pid: " << processID);
}

void SkProcessWatchDog::customClosing()
{
    ObjectMessage("Dog has terminated to WATCH pid: " << processID);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skstdinput.h"

ConstructorImpl(SkProcessExecutor, SkThread)
{
    pid = 0;
    disableEventLoopExec();
    setLoopIntervals(2000, 100000, SK_TIMEDLOOP_SLEEPING);

    SignalSet(executed);
}

bool SkProcessExecutor::customRunSetup()
{
    return true;
}

void SkProcessExecutor::execute(CStr *name, SkStringList &arguments, SkStringList &environment, CStr *stdinRedir, CStr *stdoutRedir, CStr *stderrRedir, CStr *workingDirectory)
{
    prg = name;
    args = arguments;
    env = environment;
    inPath = stdinRedir;
    outPath = stdoutRedir;
    errPath = stderrRedir;
    workingDir = workingDirectory;

    start();
}

#include <fcntl.h>
#include <termios.h>

void SkProcessExecutor::customRunning()
{
    ObjectMessage("Process LAUNCH");
    //int ret = system(cli.c_str());
    /*FILE *fp = popen(cli.c_str(), "w");
    pclose(fp);*/

    pid = fork();

    AssertKiller(pid == -1);

    if (pid == 0)
    {
        SkString cli;

        Stringify(cli, prg << " " << args.join(" ")
                               << inPath.c_str()
                               << outPath.c_str()
                               << errPath.c_str());

        ObjectMessage("Command line: " << cli);

        bool ok = true;

        if (!workingDir.isEmpty())
        {
            ObjectMessage("Setting WORKING-DIRECTORY: " << workingDir);
            ok = SkFsUtils::chdir(workingDir.c_str());
        }

        if (ok)
        {
            execl("/bin/sh", "sh", "-c", cli.c_str(), nullptr);

            //SOME PROGRAMs DON'T WORK CORRECTLY
            /*if (!inPath.isEmpty())
            {
                int input_fd = ::open(inPath.c_str(), O_RDONLY);
                dup2(input_fd, 0);
                close(input_fd);
            }

            if (!outPath.isEmpty())
            {
                int output_fd = ::open(outPath.c_str(), O_WRONLY);

                dup2(output_fd, 1);
                close(output_fd);
            }

            if (!errPath.isEmpty())
            {
                int error_fd = ::open(errPath.c_str(), O_WRONLY);

                dup2(error_fd, 2);
                close(error_fd);
            }

            char *arguments[args.count()+2];
            arguments[0] = const_cast<char *>(prg.c_str());
            arguments[args.count()+1] = nullptr;

            for(ULong i=0; i<args.count(); i++)
                arguments[i+1] = const_cast<char *>(args[i].c_str());

            char *environment[env.count()+1];
            environment[env.count()] = nullptr;

            for(ULong i=0; i<env.count(); i++)
            {
                environment[i] = const_cast<char *>(env[i].c_str());
                cout << environment[i] << "\n";
            }*/

            //execvp(prg.c_str(), arguments);
            //execvpe(prg.c_str(), arguments, environment);
        }

        // CHILD PROCESS WOULD NOT COME HERE
        ObjectError("ERROR on execl call");
        exit(1);
    }

    ObjectMessage("LAUNCHED sucessfully pid: " << pid);
    waitpid(pid, NULL, 0);
    ObjectMessage("TERMINATED sucessfully pid: " << pid);

    executed();

    SkEventLoopOnClosingSlot onClosingSlot;
    onClosingSlot.owner = this;
    onClosingSlot.name = "onEventLoopQuit";

    thEventLoop()->exec(&onClosingSlot);
}

void SkProcessExecutor::customClosing()
{
    ObjectMessage("FINISHED on pid: " << pid);
}

pid_t SkProcessExecutor::processID()
{
    return pid;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkProcess, SkObject)
{
    wd = nullptr;
    Stringify(fifoTempDir, osEnv()->getSystemTempPath() << uuid() << "/");

    streams[PCH_STDIN].name = "Input.FIFO";
    streams[PCH_STDOUT].name = "Output.FIFO";
    streams[PCH_STDERR].name = "Error.FIFO";

    SlotSet(onStart);
    SlotSet(onFinish);

    SlotSet(onExecute);
    SignalSet(executed);

    SignalSet(started);
    SignalSet(finished);

    SlotSet(onOutputAvailable);
    SignalSet(outputAvailable);

    SlotSet(onErrorAvailable);
    SignalSet(errorAvailable);

    executor = new SkProcessExecutor(this);

    Attach(executor, started, this, onStart, SkQueued);
    Attach(executor, executed, this, onExecute, SkQueued);
    Attach(executor, finished, this, onFinish, SkQueued);
}

void SkProcess::setFifoTempDir(CStr *tempPath)
{
    fifoTempDir = SkFsUtils::adjustPathEndSeparator(tempPath);
}

void SkProcess::setProgram(CStr *name, SkStringList &arguments, SkStringList &environment, CStr *workingDirectory)
{
    prg = name;
    args = arguments;
    env = environment;

    if (!SkString::isEmpty(workingDirectory))
        workingDir = workingDirectory;

    ObjectMessage("Command line setup: " << name << " {" << arguments.join(" ") << "}");
}

void SkProcess::setMerged(SkProcessStream_T streamID, bool merge)
{
    streams[streamID].merged = merge;

    ObjectMessage(streams[streamID].name << " is MERGED: " << SkVariant::boolToString(streams[streamID].merged));
}

bool SkProcess::execute()
{
    executor->setObjectName(this, "Executor");

    //MUST CHECK FILE PROGRAM FOR EXIST AND EXEC

    if (!SkFsUtils::exists(fifoTempDir.c_str()) && !SkFsUtils::mkdir(fifoTempDir.c_str()))
        return false;

    createFifo(PCH_STDIN);
    createFifo(PCH_STDOUT);
    createFifo(PCH_STDERR);

    ObjectMessage("Executing ..");

    SkString inPath;
    SkString outPath;
    SkString errPath;

    if (!streams[PCH_STDIN].merged)
        inPath = streams[PCH_STDIN].device->getFilePath();

    if (!streams[PCH_STDOUT].merged)
        outPath = streams[PCH_STDOUT].device->getFilePath();

    if (!streams[PCH_STDERR].merged)
        errPath = streams[PCH_STDERR].device->getFilePath();

    executor->execute(prg.c_str(), args, env,
                      inPath.c_str(),
                      outPath.c_str(),
                      errPath.c_str(),
                      workingDir.c_str());

    return true;
}

void SkProcess::terminate()
{
    if (!executor)
    {
        ObjectError("Process is NOT running");
        return;
    }

    wd = new SkProcessWatchDog;
    wd->setObjectName(this, "Dog");
    wd->setup(executor->processID(), 2);
    wd->watchForTermination();
    wd->start();

    ObjectWarning("Sending SIGTERM to pid: " << executor->processID());
    ::kill(executor->processID(), SIGTERM);
}

void SkProcess::kill()
{
    if (!executor)
    {
        ObjectError("Process is NOT running");
        return;
    }

    ObjectWarning("Sending SIGKILL to pid: " << executor->processID());
    ::kill(executor->processID(), SIGKILL);
}

void SkProcess::release()
{
    if (executor->isRunning())
    {
        ObjectWarning("Closing executor for pid: " << executor->processID());

        executor->quit();
        executor->wait();

        if (wd)
        {
            ObjectWarning("Closing WatchDog for pid: " << executor->processID());

            wd->quit();
            wd->wait();
            wd->destroyLater();
            wd = nullptr;
        }

        ObjectMessage("Released pid: " << executor->processID());
    }
}

bool SkProcess::createFifo(SkProcessStream_T t)
{
    SkProcessStream &p = streams[t];

    if (p.merged)
        return true;

    SkString fifoFilePath;
    Stringify(fifoFilePath, fifoTempDir << p.name);

    p.t = t;
    p.device = new SkFifo(this);
    p.device->setObjectName(this, p.name.c_str());
    p.device->setFilePath(fifoFilePath.c_str());

    if (t == PCH_STDIN)
        Stringify(p.pipeTxt, " < " << fifoFilePath.c_str() << " ");

    else if (t == PCH_STDOUT)
    {
        Stringify(p.pipeTxt, " 1> " << fifoFilePath.c_str() << " ");
        Attach(p.device, readyRead, this, onOutputAvailable, SkDirect);
    }

    else if (t == PCH_STDERR)
    {
        Stringify(p.pipeTxt, " 2> " << fifoFilePath.c_str() << " ");
        Attach(p.device, readyRead, this, onErrorAvailable, SkDirect);
    }

    SkFilePerm perm;
    perm.owner = SkFilePermItem::PM_RW_;
    perm.group = SkFilePermItem::PM_NOPERM;
    perm.other = SkFilePermItem::PM_NOPERM;

    if (!SkFsUtils::exists(fifoFilePath.c_str()) && !SkFsUtils::mkfifo(fifoFilePath.c_str(), perm))
        return false;

    ObjectMessage("CREATED fifo: " << fifoFilePath);
    return true;
}

bool SkProcess::destroyFifo(SkProcessStream_T t)
{
    SkProcessStream &p = streams[t];

    if (p.merged)
        return true;

    p.pipeTxt.clear();

    SkString fifoFilePath = p.device->getFilePath();

    if (p.device->isConnected())
        p.device->disconnect();

    p.device->destroyLater();
    p.device = nullptr;

    if (!SkFsUtils::rm(fifoFilePath.c_str()))
        return false;

    ObjectMessage("DESTROYED fifo: " << fifoFilePath);
    return true;
}

SlotImpl(SkProcess, onStart)
{
    SilentSlotArgsWarning();

    onExecutorStart();

    ObjectMessage("Started");
    started();
}

bool SkProcess::openFifos()
{
    if (streams[PCH_STDIN].device && !streams[PCH_STDIN].device->open(FFM_ONLYWRITE))
        return false;

    if (streams[PCH_STDOUT].device && !streams[PCH_STDOUT].device->open(FFM_ONLYREAD))
        return false;

    if (streams[PCH_STDERR].device && !streams[PCH_STDERR].device->open(FFM_ONLYREAD))
        return false;

    return true;
}

void SkProcess::onExecutorStart()
{
    openFifos();
}

SlotImpl(SkProcess, onExecute)
{
    SilentSlotArgsWarning();
    onProcessFinished();
    executed();
}

SlotImpl(SkProcess, onFinish)
{
    SilentSlotArgsWarning();

    onExecutorFinished();

    destroyFifo(PCH_STDIN);
    destroyFifo(PCH_STDOUT);
    destroyFifo(PCH_STDERR);

    SkFsUtils::rmdir(fifoTempDir.c_str());

    ObjectMessage("Finished");
    finished();
}

SlotImpl(SkProcess, onOutputAvailable)
{
    SilentSlotArgsWarning();
    outputAvailable();
}

SlotImpl(SkProcess, onErrorAvailable)
{
    SilentSlotArgsWarning();
    errorAvailable();
}

bool SkProcess::isRunning()
{
    return executor->isRunning();
}

pid_t SkProcess::pid()
{
    return executor->processID();
}

bool SkProcess::isMerged(SkProcessStream_T streamID)
{
    return streams[streamID].merged;
}

SkFifo *SkProcess::device(SkProcessStream_T streamID)
{
    return streams[streamID].device;
}

CStr *SkProcess::deviceName(SkProcessStream_T streamID)
{
    return streams[streamID].name.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
