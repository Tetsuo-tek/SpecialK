#include "skabstractfifo.h"
#include <unistd.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkFifoMode)
{
    SetupEnumWrapper(SkFifoMode);

    SetEnumItem(SkFifoMode::FFM_NOMODE);
    SetEnumItem(SkFifoMode::FFM_ONLYREAD);
    SetEnumItem(SkFifoMode::FFM_ONLYWRITE);
}

// // // // // // // // // // // // // // // // // // // // //

//DeclareMeth_INSTANCE_RET(SkAbstractFifo, canReadLine, bool, Arg_UInt64_REAL)
DeclareMeth_INSTANCE_RET(SkAbstractFifo, currentMode, const SkFifoMode&)
DeclareMeth_INSTANCE_RET(SkAbstractFifo, getCurrentModeString, CStr*)

// // // // // // // // // // // // // // // // // // // // //

AbstractConstructorImpl(SkAbstractFifo, SkAbstractSocket,
                        {
                            AddEnumType(SkFifoMode);

                            //AddMeth_INSTANCE_RET(SkAbstractFifo, canReadLine);
                            AddMeth_INSTANCE_RET(SkAbstractFifo, currentMode);
                            AddMeth_INSTANCE_RET(SkAbstractFifo, getCurrentModeString);
                        })
{    
    openMode = SkFifoMode::FFM_NOMODE;
}

bool SkAbstractFifo::canReadLine(uint64_t &)
{
    ObjectError("Cannot PEEK from FifoDevice");
    return false;
}

bool SkAbstractFifo::readFromSocket(char *data, uint64_t &len, bool onlyPeek)
{
    if (openMode != SkFifoMode::FFM_ONLYREAD)
    {
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "This is a FifoDevice open as OnlyWrite");
        return false;
    }

    if (onlyPeek)
    {
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Cannot PEEK on FifoDevice");
        return false;
    }

    uint64_t tempSizeAvailable = bytesAvailable();

    if (tempSizeAvailable == 0)
    {
        len = 0;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "Cannot READ because it has NOT data");
        return false;
    }

    if (len == 0)
        len = tempSizeAvailable;

    int64_t readSize = -1;
    //errno = 0;

    readSize = ::read(socketFD, data, len);

    if (readSize > 0)
        len = static_cast<uint64_t>(readSize);

    else
    {
        len = 0;
        sckValid = false;
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);
    }

    return (len > 0);
}

bool SkAbstractFifo::writeToSocket(CStr *data, uint64_t &len)
{
    if (openMode != SkFifoMode::FFM_ONLYWRITE)
    {
        //notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__, "This is a FifoDevice open as OnlyRead");
        return false;
    }

    int64_t wrote = ::write(socketFD, data, len);

    if (wrote > 0)
    {
        len = static_cast<uint64_t>(wrote);
        return true;
    }

    if (errno == EPIPE)
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__, "SIGPIPE");

    else
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);

    sckValid = false;
    return false;
}

const SkFifoMode &SkAbstractFifo::currentMode()
{
    return openMode;
}

CStr *SkAbstractFifo::getCurrentModeString()
{
    if (openMode == SkFifoMode::FFM_ONLYREAD)
        return "OnlyRead";

    else if (openMode == SkFifoMode::FFM_ONLYWRITE)
        return "OnlyWrite";

    return "NoMode";
}

/*void SkAbstractFifo::onBaseUpdateChangeState(bool enabled)
{
    if (!enabled)
        openMode = SkFifoMode::FFM_NOMODE;
}*/
