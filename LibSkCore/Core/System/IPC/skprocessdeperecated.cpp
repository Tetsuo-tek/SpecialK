#include "skprocessdeperecated.h"
#include "Core/App/skapp.h"
#include "Core/System/skosenv.h"
#include "Core/System/Filesystem/skfsutils.h"

ConstructorImpl(SkProcessDeprecated, SkThread)
{
    reset();

    SignalSet(errorOccurred);
}

void SkProcessDeprecated::reset()
{
    ObjectDebug("Setup RESET");

    pid = -1;
    openMode = SkProcessMode::PRM_NOMODE;
    cliPRE.clear();
    cliCMD.clear();
    cliPOST.clear();

    mergedStdIN = true;
    mergedStdOUT = true;
    mergedStdERR = true;

    stdIN = nullptr;
    stdOUT = nullptr;
    stdERR = nullptr;
}

void SkProcessDeprecated::setStdStreamMerged(SkProcessStream_T channel, bool merged)
{
    if (channel == SkProcessStream_T::PCH_STDIN)
        mergedStdIN = merged;

    else if (channel == SkProcessStream_T::PCH_STDOUT)
        mergedStdOUT = merged;

    else if (channel == SkProcessStream_T::PCH_STDERR)
        mergedStdERR = merged;
}

bool SkProcessDeprecated::isStdStreamMerged(SkProcessStream_T channel)
{
    if (channel == SkProcessStream_T::PCH_STDIN)
        return mergedStdIN;

    else if (channel == SkProcessStream_T::PCH_STDOUT)
        return mergedStdOUT;

    else if (channel == SkProcessStream_T::PCH_STDERR)
        return mergedStdERR;

    return false;
}

bool SkProcessDeprecated::checkBinary(const char *bin)
{
    if (!SkFsUtils::isFile(bin))
    {
        ObjectError("Cannot EXECUTE the process because binary is NOT a file: " << bin);
        return false;
    }

    if (!SkFsUtils::isReadable(bin))
    {
        ObjectError("Cannot EXECUTE the process because binary is NOT readable: " << bin);
        return false;
    }

    if (!SkFsUtils::isExecutable(bin))
    {
        ObjectError("Cannot EXECUTE the process because binary is NOT executable: " << bin);
        return false;
    }

    return true;
}

bool SkProcessDeprecated::setProgram(const char *bin, const char *argsLine)
{
    if (SkString::isEmpty(bin))
    {
        ObjectError("Cannot EXECUTE the process because the bin is EMPTY");
        return false;
    }

    bool isOnSysPaths = false;

    if (SkFsUtils::exists(bin))
    {
        if (!checkBinary(bin))
            return false;
    }

    else
    {
        //CHECK ON SYSPATHs
        SkStringList &sysPaths = osEnv()->getSystemPaths();

        for(uint64_t i=0; i<sysPaths.count() && !isOnSysPaths; i++)
        {
            SkString tempPath = SkFsUtils::adjustPathEndSeparator(sysPaths.at(i).c_str());
            tempPath.append(bin);

            if (SkFsUtils::exists(tempPath.c_str()) && checkBinary(tempPath.c_str()))
                isOnSysPaths = true;
        }

        if (!isOnSysPaths)
        {
            ObjectError("Cannot EXECUTE the process because binary is NOT valid: " << bin);
            return false;
        }
    }

    if (!isOnSysPaths && bin[0] != '/')
        cliCMD = "./";

    cliCMD.append(bin);
    cliCMD.append(" ");
    cliCMD.append(argsLine);

    return true;
}

static pid_t launch(const char *cmd, const char *workingDir)
{
    /*pid_t  pid;

    //int status;
    pid = ::fork();

    if (pid == -1)
    {
        FlatWarning("Cannot FORK new process");
        return -1;
    }

    else if (pid == 0)
    {
        skApp->setAsChildProcess();

        pid = ::getpid();

        if (workingDir && (::strlen(workingDir) > 0) && (::chdir(workingDir) != 0))
        {
            FlatError("Cannot change working directory: " << workingDir << "; aborting execution and forcing exit ..");
            ::KillApp();
        }

        errno = 0;

        int ret = ::system(cmd);

        if (errno)
            FlatError("Child process error [pid: " << pid << "; errno: " << errno << "]: " << strerror(errno));

        if (ret)
            FlatWarning("Child process TERMINATED [pid: " << pid << "; exiting with: " << ret << "]");
        else
            FlatMessage("Child process TERMINATED [pid: " << pid << "; exiting with: " << ret << "]");

        exit(ret);
    }

    FlatWarning("Child process LAUNCHED [pid: " << pid << "]");
    return pid;*/
    return 0;
}

bool SkProcessDeprecated::execute(SkProcessMode m, const char *workingDir)
{
    if (isRunning())
    {
        ObjectError("Cannot EXECUTE the process because it is ALREADY running: " << cliCMD);
        return false;
    }

    if (cliCMD.isEmpty())
    {
        ObjectError("Cannot EXECUTE the process because the program is NOT defined");
        return false;
    }

    SkFileInfo dirInfo;
    bool wrkDir = false;

    if (!SkString::isEmpty(workingDir))
    {
        if (SkFsUtils::fillFileInfo(workingDir, dirInfo))
        {
            if (!dirInfo.isDir)
            {
                ObjectError("Cannot EXECUTE the process because wordinkgDir is NOT a directory: " << dirInfo.completeAbsolutePath);
                return false;
            }

            if (!dirInfo.isReadable)
            {
                ObjectError("Cannot EXECUTE the process because wordinkgDir is NOT readable: " << dirInfo.completeAbsolutePath);
                return false;
            }

            wrkDir = true;
        }

        else
        {
            ObjectError("Cannot EXECUTE the process because wordinkgDir is NOT valid: " << workingDir);
            return false;
        }
    }

    //cliCMD = cliCommand;
    openMode = m;

    setupStdInput();
    setupStdOutput();
    setupStdError();

    SkString cli = cliPRE;
    cli.append(cliCMD);
    cli.append(cliPOST);

    ObjectDebug("Executing the process: " << cli << " ..");

    if (wrkDir)
        pid = ::launch(cli.c_str(), workingDir);
    else
        pid = ::launch(cli.c_str(), "");

    if (pid == -1)
    {
        errorOccurred();
        reset();
        return false;
    }

    start();

    if (stdIN && !stdIN->open(SkFifoMode::FFM_ONLYWRITE))
        return false;

    if (stdOUT && !stdOUT->open(SkFifoMode::FFM_ONLYREAD))
        return false;

    if (stdERR && !stdERR->open(SkFifoMode::FFM_ONLYREAD))
        return false;

    return true;
}

bool SkProcessDeprecated::setupFifoName(const char* streamLabel, SkFifo *fifo)
{
    SkString fifoName = osEnv()->getSystemTempPath();
    fifoName.append(objectName());
    fifoName.append(".");
    fifoName.append(streamLabel);
    fifoName.append(".FIFO");

    fifo->setObjectName(fifoName.c_str());
    fifo->setFilePath(fifoName.c_str());

    SkFilePerm perm;
    perm.owner = SkFilePermItem::PM_RW_;
    perm.group = SkFilePermItem::PM_NOPERM;
    perm.other = SkFilePermItem::PM_NOPERM;

    return SkFsUtils::mkfifo(fifoName.c_str(), perm);
}

void SkProcessDeprecated::setupStdInput()
{
    if (openMode != SkProcessMode::PRM_ONLYWRITE
            && openMode != SkProcessMode::PRM_READWRITE)
    {
        return;
    }

    if (!mergedStdIN)
    {
        ObjectDebug("Creating fifo for process StdInput");
        stdIN = new SkFifo;
        setupFifoName("STDIN", stdIN);

        cliPOST.append(" < ");
        cliPOST.append(stdIN->getFilePath());
    }

    return;
}

void SkProcessDeprecated::setupStdOutput()
{
    if (openMode != SkProcessMode::PRM_ONLYREAD
            && openMode != SkProcessMode::PRM_ONLYREAD_STDOUT
            && openMode != SkProcessMode::PRM_READWRITE)
    {
        return;
    }

    if (!mergedStdOUT)
    {
        ObjectDebug("Creating fifo for process StdOutput");
        stdOUT = new SkFifo;
        setupFifoName("STDOUT", stdOUT);

        cliPOST.append(" 1> ");
        cliPOST.append(stdOUT->getFilePath());
    }

    return;
}

void SkProcessDeprecated::setupStdError()
{
    if (openMode != SkProcessMode::PRM_ONLYREAD
            && openMode != SkProcessMode::PRM_ONLYREAD_STDERR
            && openMode != SkProcessMode::PRM_READWRITE)
    {
        return;
    }

    if (!mergedStdERR)
    {
        ObjectDebug("Creating fifo for process StdError");
        stdERR = new SkFifo;
        setupFifoName("STDERR", stdERR);

        cliPOST.append(" 2> ");
        cliPOST.append(stdERR->getFilePath());
    }

    return;
}

void SkProcessDeprecated::customRunning()
{
}

void SkProcessDeprecated::customClosing()
{
    int status;
    //::waitpid(pid, &status, 0);
    ObjectDebug("Process exited: " << cliCMD << " [pid: " << pid << "] ..");

    pid = -1;

    if (stdIN)
    {
        stdIN->disconnect();
        SkFsUtils::rm(stdIN->getFilePath());
    }

    if (stdOUT)
    {
        stdOUT->disconnect();
        SkFsUtils::rm(stdOUT->getFilePath());
    }

    if (stdERR)
    {
        stdERR->disconnect();
        SkFsUtils::rm(stdERR->getFilePath());
    }

    reset();
}

bool SkProcessDeprecated::isRunning()
{
    return (pid != -1);
}

bool SkProcessDeprecated::sendSIG(int sig)
{
    if (!isRunning())
    {
        ObjectError("Cannot SEND-SIGNAL [" << sig << "] because the process is NOT running");
        return false;
    }

    bool ok = (::kill(pid, sig)==0);

    if (!ok)
        ObjectError("Cannot SEND-SIGNAL [" << sig << "] to the process: " << cliCMD << " [pid: " << pid << "]");

    return ok;
}

bool SkProcessDeprecated::kill()
{
    if (!isRunning())
    {
        ObjectError("Cannot KILL the process because it is NOT running");
        return false;
    }

    ObjectDebug("Killing (SIGKILL) the process: " << cliCMD << " [pid: " << pid << "] ..");
    bool ok = (::kill(pid, SIGKILL)==0);

    if (!ok)
        ObjectError("Cannot KILL the process: " << cliCMD << " [pid: " << pid << "]");

    quit();
    return ok;
}

bool SkProcessDeprecated::terminate()
{
    if (!isRunning())
    {
        ObjectError("Cannot TERMINATE the process because it is NOT running");
        return false;
    }

    ObjectDebug("Terminating (SIGTERM) the process: " << cliCMD << " [pid: " << pid << "] ..");

    bool ok = (::kill(pid, SIGTERM)==0);

    if (!ok)
        ObjectError("Cannot TERMINATE the process: " << cliCMD << " [pid: " << pid << "]");

    quit();
    return ok;
}

SkFifo *SkProcessDeprecated::stdInputStream()
{
    return stdIN;
}

SkFifo *SkProcessDeprecated::stdOutputStream()
{
    return stdOUT;
}

SkFifo *SkProcessDeprecated::stdErrorStream()
{
    return stdERR;
}

SkFifo *SkProcessDeprecated::stdStream(SkProcessStream_T channel)
{
    if (channel == SkProcessStream_T::PCH_STDIN)
        return stdIN;

    else if (channel == SkProcessStream_T::PCH_STDOUT)
        return stdOUT;

    else if (channel == SkProcessStream_T::PCH_STDERR)
        return stdERR;

    return nullptr;
}

pid_t SkProcessDeprecated::getPID()
{
    return pid;
}
