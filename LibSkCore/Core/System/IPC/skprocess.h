#ifndef SKPROCESS_H
#define SKPROCESS_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfifo.h"
#include "Core/System/Thread/skthread.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkProcessStream_T
{
    PCH_STDIN,
    PCH_STDOUT,
    PCH_STDERR,
    PCH_NULL
};

struct SkProcessStream
{
    SkString name;
    SkString pipeTxt;
    bool merged;
    SkProcessStream_T t;
    SkFifo *device;
    SkString channelName;

    SkProcessStream()
    {
        device = nullptr;
        merged = false;
        t = PCH_NULL;
    }
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkProcessWatchDog extends SkThread
{
    UInt timeoutInterval;
    SkElapsedTime chrono;
    pid_t processID;
    bool watchingTermination;
    bool killed;

    public:
        Constructor(SkProcessWatchDog, SkThread);

        void setup(pid_t pid, UInt seconds=1);
        void watchForTermination();

        Slot(oneSecTick);

    protected:
        void customRunning()            override;
        void customClosing()            override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkProcessExecutor extends SkThread
{
    SkString prg;
    SkStringList args;
    SkStringList env;
    SkString inPath;
    SkString outPath;
    SkString errPath;
    SkString workingDir;
    pid_t pid;

    public:
        Constructor(SkProcessExecutor, SkThread);

        void execute(CStr *name, SkStringList &arguments, SkStringList &environment, CStr *stdinRedir, CStr *stdoutRedir, CStr *stderrRedir, CStr *workingDirectory);
        Signal(executed);

        pid_t processID();

    private:
        bool customRunSetup()           override;
        void customRunning()            override;
        void customClosing()            override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkProcess extends SkObject
{
    SkString prg;
    SkStringList args;
    SkStringList env;
    SkProcessStream streams[3];
    SkString workingDir;
    SkString fifoTempDir;
    SkProcessExecutor *executor;
    SkProcessWatchDog *wd;

    public:
        Constructor(SkProcess, SkObject);

        void setFifoTempDir(CStr *tempPath);
        void setProgram(CStr *name, SkStringList &arguments, SkStringList &environment, CStr *workingDirectory="");

        void setMerged(SkProcessStream_T streamID, bool merge);

        bool execute();
        void terminate();
        void kill();
        void release();

        bool isRunning();
        pid_t pid();
        bool isMerged(SkProcessStream_T streamID);
        SkFifo *device(SkProcessStream_T streamID);
        CStr *deviceName(SkProcessStream_T streamID);

        Slot(onStart);
        Signal(started);

        Slot(onExecute);
        Signal(executed);

        Slot(onFinish);
        Signal(finished);

        Slot(onOutputAvailable);
        Signal(outputAvailable);

        Slot(onErrorAvailable);
        Signal(errorAvailable);

    protected:
        bool openFifos();

        virtual void onExecutorStart();
        virtual void onExecutorFinished()                   {}

        virtual void onProcessFinished()                    {}

    private:
        bool createFifo(SkProcessStream_T t);
        bool destroyFifo(SkProcessStream_T t);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKPROCESS_H
