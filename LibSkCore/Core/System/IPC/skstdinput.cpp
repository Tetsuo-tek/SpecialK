#include "skstdinput.h"
#include <termios.h>

void SkStdInputReset(SkObject *obj)
{
    SkStdInput *f = static_cast<SkStdInput *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkAbstractSocketClose()");

    if (!f->isEnabledStdInputECHO())
        f->enableStdInputECHO();

    if (!f->isEnabledStdInputICANON())
        f->enableStdInputICANON();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_RET(SkStdInput, open, bool)

DeclareMeth_INSTANCE_RET(SkStdInput, enableStdInputECHO, bool)
DeclareMeth_INSTANCE_RET(SkStdInput, disableStdInputECHO, bool)
DeclareMeth_INSTANCE_RET(SkStdInput, isEnabledStdInputECHO, bool)

DeclareMeth_INSTANCE_RET(SkStdInput, enableStdInputICANON, bool)
DeclareMeth_INSTANCE_RET(SkStdInput, disableStdInputICANON, bool)
DeclareMeth_INSTANCE_RET(SkStdInput, isEnabledStdInputICANON, bool)

// // // // // // // // // // // // // // // // // // // // //

ConstructorImpl(SkStdInput, SkAbstractFifo,
                {
                    AddMeth_INSTANCE_RET(SkStdInput, open);

                    AddMeth_INSTANCE_RET(SkStdInput, enableStdInputECHO);
                    AddMeth_INSTANCE_RET(SkStdInput, disableStdInputECHO);
                    AddMeth_INSTANCE_RET(SkStdInput, isEnabledStdInputECHO);

                    AddMeth_INSTANCE_RET(SkStdInput, enableStdInputICANON);
                    AddMeth_INSTANCE_RET(SkStdInput, disableStdInputICANON);
                    AddMeth_INSTANCE_RET(SkStdInput, isEnabledStdInputICANON);
                })
{
    echoEnabled = true;
    iCanonEnabled = true;

    setObjectName("StandardInput");
    //closeFileDescriptorWhenFinished = false;

    addDtorCompanion(SkStdInputReset);
}

bool SkStdInput::open()
{
    if (isOpen())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    openMode = SkFifoMode::FFM_ONLYREAD;
    ObjectDebug("StdInput device is OPEN");
    setFileDescriptor(0);
    return true;
}

bool SkStdInput::enableStdInputECHO()
{
    if (echoEnabled)
        return false;

    echoEnabled = setTermAttr(ECHO);
    return true;
}

bool SkStdInput::disableStdInputECHO()
{
    if (!echoEnabled)
        return false;

    echoEnabled = !unSetTermAttr(ECHO);
    return true;
}

bool SkStdInput::isEnabledStdInputECHO()
{
    return echoEnabled;
}

bool SkStdInput::enableStdInputICANON()
{
    if (iCanonEnabled)
        return false;

    iCanonEnabled = setTermAttr(ICANON);
    return true;
}

bool SkStdInput::disableStdInputICANON()
{
    if (!iCanonEnabled)
        return false;

    iCanonEnabled = !unSetTermAttr(ICANON);
    return true;
}

bool SkStdInput::isEnabledStdInputICANON()
{
    return iCanonEnabled;
}

bool SkStdInput::setTermAttr(uint flag)
{
    struct termios values;
    int res = tcgetattr (1, &values);

    if (res == 0)
    {
        values.c_lflag |= (flag);
        res = tcsetattr (1, TCSANOW, &values);
    }

    return (res == 0);
}

bool SkStdInput::unSetTermAttr(uint flag)
{
    struct termios values;
    int res = tcgetattr (1, &values);

    if (res == 0)
    {
        values.c_lflag &= (~flag);
        values.c_cc[VMIN] = 1;
        values.c_cc[VTIME] = 0;
        res = tcsetattr (1, TCSANOW, &values);
    }

    return (res == 0);
}
