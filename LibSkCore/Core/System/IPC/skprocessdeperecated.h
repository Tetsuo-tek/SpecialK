#ifndef SKPROCESSDEPERECATED_H
#define SKPROCESSDEPERECATED_H

#include "skprocess.h"
#include "Core/System/Thread/skthread.h"

enum SkProcessMode
{
    PRM_NOMODE,
    PRM_ONLYREAD,
    PRM_ONLYREAD_STDOUT,
    PRM_ONLYREAD_STDERR,
    PRM_ONLYWRITE,
    PRM_READWRITE
};

class SPECIALK SkProcessDeprecated extends SkThread
{
    public:
        Constructor(SkProcessDeprecated, SkThread);

        //MERGE THE SELECTED StdStream OF THE CHILD-PROCESS WITH THE APPLICATION-PROCESS
        //STREAMS COULD BE SET AS MERGED ONLY BEFORE THE START OF PROCESS
        //BY DEFAULT THE StdStreams ARE ALL MERGED
        void setStdStreamMerged(SkProcessStream_T channel, bool merged);
        bool isStdStreamMerged(SkProcessStream_T channel);

        //bin could be a relative path, an absolute path or a binName existing in SystemPaths
        bool setProgram(const char *bin, const char *args);

        //IN THE cliCommand DO NOT PUT ANY REDIRECT OF PROCESS StdStreams
        //THEY WILL BE PUTTED INTERNALY WHEN REQUIRED BY SETUP
        bool execute(SkProcessMode m, const char *workingDir="");

        bool isRunning();

        //sendSIG, IF IT GOES TO PROC-DIE, IT REQUIRES TO quit() MANUALLY, AFTER
        //IF WE ARE CLOSING THE PROC FROM SENDING-SIG WE MUST CALL wait() AFTER
        bool sendSIG(int sig);

        //AFTER CALLED THESE METHs WE MUST CALL wait() AFTER
        bool kill();
        bool terminate();

        //StdStreams ARE HANDLED BY SkFifo OBJECTs
        //DO NOT DESTROY FIFOs FROM EXTERN OF THIS OBJECT
        //IT WILL MANAGE THEIR LIFE-CYCLEs
        SkFifo *stdInputStream();
        SkFifo *stdOutputStream();
        SkFifo *stdErrorStream();
        SkFifo *stdStream(SkProcessStream_T channel);

        pid_t getPID();

        Signal(errorOccurred);

    private:
        pid_t pid;
        SkProcessMode openMode;

        SkString cliCMD;
        SkString cliPRE;
        SkString cliPOST;

        bool mergedStdIN;
        SkFifo *stdIN;

        bool mergedStdOUT;
        SkFifo *stdOUT;

        bool mergedStdERR;
        SkFifo *stdERR;

        void reset();

        void setupStdInput();
        void setupStdOutput();
        void setupStdError();

        bool setupFifoName(const char *streamLabel, SkFifo *fifo);

        bool checkBinary(const char *bin);

        void customRunning();
        void customClosing();
};

#endif // SKPROCESSDEPERECATED_H
