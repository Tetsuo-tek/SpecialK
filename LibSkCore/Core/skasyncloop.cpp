#include "skasyncloop.h"

SkAsyncLoop::SkAsyncLoop()
{
    callBack = nullptr;
    reset();
}

void SkAsyncLoop::setInterval(int64_t from, int64_t to)
{
    min = from;
    max = to;
    counter = from;
}

void SkAsyncLoop::setCallBack(void (*cb)(int64_t &))
{
    callBack = cb;
}

int64_t &SkAsyncLoop::getCounter()
{
    return counter;
}

bool SkAsyncLoop::tick()
{
    if (counter >= this->max)
        return false;

    //CALLING FUNCT CALLBACK
    if (callBack)
        (*callBack)(counter);

    //SUBCLASSING
    onLoop();

    counter++;
    return true;
}

void SkAsyncLoop::reset()
{
    min = 0;
    max = 0;
    counter = 0;
}
