#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkgauge.h"
#include "skfltklcddisplay.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkGaugeWidget::SkFltkGaugeWidget(int x, int y, int w, int h) : Fl_Box(x, y, w, h)
{
    owner = nullptr;
    fgColor1 = FL_DARK_GREEN;
    fgColor2 = FL_RED;
    bgColor = FL_BLACK;
    margin = 6;
    minValue = 0.f;
    minValue = 100.f;
    v = 0.f;
    showUDM = true;
    showValue = true;
}

SkFltkGaugeWidget::~SkFltkGaugeWidget()
{
}

int SkFltkGaugeWidget::handle(int e)
{
    return Fl_Box::handle(e);
}

static const float epsilon = 0.00001f;

void SkFltkGaugeWidget::draw()
{
    double o = 20;

    if (!showValue)
        o = 90;

    double a1 = 0. - o;
    double a2 = 180. + o;

    if (w() != h())
        size(w(), w());

    Fl_Box::draw();
    drw = SkRegion(x()+margin, y()+margin, w()-(margin*2), h()-(margin*2));

    fl_push_clip(drw.x, drw.y, drw.w, drw.h);
    fl_rectf(drw.x, drw.y, drw.w, drw.h, bgColor);
    fl_pop_clip();

    drw.x += 8;
    drw.y += 8;
    drw.w -= 16;
    drw.h -= 16;

    double outerDiameter = drw.w;
    double innerDiameter = drw.w-(drw.w*0.35);
    int thickness = (outerDiameter-innerDiameter)/2.;

    float val = v.toFloat();
    if (val > maxValue)
        val = maxValue;

    else if (v.toFloat() < minValue)
        val = minValue;

    float boundsDiff = fabs(minValue - maxValue);//20
    float angleDiff = fabs(a1 - a2);//220
    float valRatio = (val-minValue) / boundsDiff;
    float angle = a2-(valRatio * angleDiff);

    fl_color(30,30,30);
    fl_pie(drw.x, drw.y, outerDiameter, outerDiameter, a1, a2);

    fl_color(60,60,60);
    fl_arc(drw.x-2, drw.y-2, outerDiameter+4, outerDiameter+4, a1, a2);

    if (valRatio > 0)
    {
        //Fl_Color valuedColor = fl_color_average(FL_YELLOW, FL_GREEN, valRatio);
        Fl_Color valuedColor = fl_color_average(fl_rgb_color(255, 100, 0), FL_GREEN, valRatio);

        fl_color(valuedColor);

        //FLAT
        //fl_pie(drw.x, drw.y, outerDiameter, outerDiameter, a2, angle);

        //CICRCUS
        int arcOffset = thickness/4;

        for(int i=0; i<thickness; i+=arcOffset)
        {
            fl_color(fl_color_average(valuedColor, fl_rgb_color(60, 60, 60), 1-(float)i/thickness));

            int len = outerDiameter-(i*2);
            fl_pie(drw.x+i, drw.y+i, len, len, a2, angle);
            //fl_arc(drw.x+i, drw.y+i, len, len, a2, angle);
        }
    }

    fl_color(FL_BLACK);
    fl_pie(drw.x, drw.y, outerDiameter, outerDiameter, a1, -180.+o);//-135);
    fl_pie(drw.x+thickness, drw.y+thickness, innerDiameter, innerDiameter, 0, 360);

    fl_color(60,60,60);
    fl_arc(drw.x+thickness+1, drw.y+thickness+1, innerDiameter-2, innerDiameter-2, a1, a2);

    if (showValue || showUDM)
    {
        int middleH = drw.h/2;

        float s = fabs(::sin(a2 * (M_PI / 180.0)) * middleH);

        int margin = 5;
        SkRegion lcdDrw;
        lcdDrw.h = middleH - s - (margin*2);

        if (showValue)
        {
            int fixedCiphersCount = 0;//4;
            int floatPrecisionCount = 3;

            lcdDrw.x = drw.x + margin;
            lcdDrw.y = drw.y + middleH + s + 10;
            lcdDrw.w = drw.w - (margin*2);

            Fl_Color fgColor = FL_RED;
            Fl_Color on = fgColor;

            if (!owner->isEnabled())
                on = fl_rgb_color(30, 30, 30);

            Fl_Color bgColor = FL_BLACK;

            SkFltkLcdDisplayWidget::drawDisplay(owner, v, lcdDrw, bgColor, on, fl_rgb_color(30, 30, 30), fixedCiphersCount, false, floatPrecisionCount);
        }

        if (showUDM)
        {
            int charSize = lcdDrw.h/2.1;
            fl_font(FL_SCREEN, charSize);

            SkString temoUdm = "[msecs]";
            temoUdm.append(udm);
            temoUdm.append("]");

            SkRegion textRegion;

            fl_measure(udm.c_str(), textRegion.w, textRegion.h);

            textRegion.x = drw.x + ((drw.w - textRegion.w) / 2);
            textRegion.y = drw.y + drw.h - lcdDrw.h - fl_descent()- (textRegion.h/2) - margin;

            fl_color(FL_GREEN);
            fl_draw(udm.c_str(), textRegion.x, textRegion.y);
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define gauge   DynCast(SkFltkGaugeWidget, widget)

ConstructorImpl(SkFltkGauge, SkFltkWidgetWrapper)
{}

void SkFltkGauge::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkGaugeWidget(region.x, region.y, region.w, region.h));
    gauge->owner = this;
}

bool SkFltkGauge::setParameter(SkString &k, SkVariant &v)
{
    if (k == "bounds")
    {
        double min;
        double max;

        if (!grabBounds(k.c_str(), v, min, max))
            return false;

        setBounds(min, max);
    }

    else if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else if (k == "showValue")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        showValue(val);
    }

    else if (k == "showUDM")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        showUDM(val);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkGauge::setBounds(float min, float max)
{
    gauge->minValue = min;
    gauge->maxValue = max;
}

void SkFltkGauge::setValue(const SkVariant &val)
{
    gauge->v = val;
}

void SkFltkGauge::setBackGroundColor(SkColor bg)
{
    gauge->bgColor = bg.toFlColor();
}

void SkFltkGauge::setUDM(CStr *udm)
{
    gauge->udm = udm;
}

void SkFltkGauge::showValue(bool val)
{
    gauge->showValue = val;
}

void SkFltkGauge::showUDM(bool val)
{
    gauge->showUDM = val;
}

SkVariant &SkFltkGauge::value()
{
    return gauge->v;
}

#endif
