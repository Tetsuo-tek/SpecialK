#if defined(ENABLE_FLTK)

#include "skfltkvaluators.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define valuator        DynCast(Fl_Valuator, widget)

ConstructorImpl(SkFltkValuator, SkFltkWidgetWrapper)
{
    /*if (!inh)
        setWidget(new Fl_Valuator(0,0,0,0));*/
    SignalSet(changed);
}

void SkFltkValuator::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Valuator(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) w_CB, this);
}

bool SkFltkValuator::setParameter(SkString &k, SkVariant &v)
{
    if (k == "bounds")
    {
        double min;
        double max;

        if (!grabBounds(k.c_str(), v, min, max))
            return false;

        setBounds(min, max);
    }

    else if (k == "step")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setStep(val);
    }

    else if (k == "precision")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setPrecision(val);
    }

    else if (k == "value")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setValue(val);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkValuator::setBounds(double min, double max)
{
    valuator->bounds(min, max);
}

void SkFltkValuator::setStep(double value)
{
    valuator->step(value);
}

void SkFltkValuator::setPrecision(int value)
{
    valuator->precision(value);
}

void SkFltkValuator::setValue(double value)
{
    valuator->value(value);
}

double SkFltkValuator::increment(double incr, int count)
{
    return valuator->increment(incr, count);
}

double SkFltkValuator::round(double value)
{
    return valuator->round(value);
}

double SkFltkValuator::clamp(double value)
{
    return valuator->clamp(value);
}

double SkFltkValuator::getMininum()
{
    return valuator->minimum();
}

double SkFltkValuator::getMaximum()
{
    return valuator->maximum();
}

double SkFltkValuator::getValue()
{
    valuator->value();
}

double SkFltkValuator::getStep()
{
    valuator->step();
}

void SkFltkValuator::w_CB(Fl_Slider *, void *data)
{
    SkFltkValuator * wWrapper = static_cast<SkFltkValuator *>(data);
    Fl_Event tempFlEvt = static_cast<Fl_Event>(Fl::event());
    ObjectMessage_EXT(wWrapper, "ValuatorChanged (EVT: " << fl_eventnames[tempFlEvt] << "[" << tempFlEvt << "])");

    SkVariantVector p;
    p << wWrapper->getValue();
    wWrapper->changed(p);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define dial        DynCast(Fl_Dial, widget)

ConstructorImpl(SkFltkDial, SkFltkValuator)
{
    /*if (!inh)
        setWidget(new Fl_Dial(0,0,0,0));*/
}

void SkFltkDial::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Dial(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) SkFltkValuator::w_CB, this);
}

bool SkFltkDial::setParameter(SkString &k, SkVariant &v)
{
    if (k == "angle1")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setFirstAngle(val);
    }

    else if (k == "angle2")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setSecondAngle(val);
    }

    else
        return SkFltkValuator::setParameter(k, v);

    return true;
}

void SkFltkDial::setFirstAngle(short angle)
{
    return dial->angle1(angle);
}

void SkFltkDial::setSecondAngle(short angle)
{
    dial->angle2(angle);
}

short SkFltkDial::getFirstAngle()
{
    return dial->angle1();
}

short SkFltkDial::getSecondAngle()
{
    return dial->angle2();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define roller      DynCast(Fl_Roller, widget)

ConstructorImpl(SkFltkRoller, SkFltkValuator)
{
    /*if (!inh)
        setWidget(new Fl_Roller(0,0,0,0));*/
}

void SkFltkRoller::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Roller(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) SkFltkValuator::w_CB, this);
}

bool SkFltkRoller::setParameter(SkString &k, SkVariant &v)
{
    if (k == "orientation")
    {
        bool isHorizontal;

        if (!grabOrientation(k.c_str(), v, isHorizontal))
            return false;

        if (isHorizontal)
            setHorizontal();
        else
            setVertical();
    }

    else
        return SkFltkValuator::setParameter(k, v);

    return true;
}

void SkFltkRoller::setHorizontal()
{
    return roller->type(FL_HORIZONTAL);
}

void SkFltkRoller::setVertical()
{
    return roller->type(FL_VERTICAL);
}

bool SkFltkRoller::isHorizontal()
{
    return roller->type() == FL_HORIZONTAL;
}

bool SkFltkRoller::isVertical()
{
    return roller->type() == FL_VERTICAL;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
/*
#define slider      DynCast(Fl_Slider, widget)

ConstructorImpl(SkFltkSlider, SkFltkValuator)
{
}

void SkFltkSlider::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Slider(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) SkFltkValuator::w_CB, this);
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkSlider, SkFltkValuator, Fl_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkValueSlider, SkFltkSlider, Fl_Value_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkHSlider, SkFltkSlider, Fl_Hor_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkNiceSlider, SkFltkSlider, Fl_Nice_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkHNiceSlider, SkFltkSlider, Fl_Hor_Nice_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkFillSlider, SkFltkSlider, Fl_Fill_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkHFillSlider, SkFltkSlider, Fl_Hor_Fill_Slider, SkFltkValuator::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
