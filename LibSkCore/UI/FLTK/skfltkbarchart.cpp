#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkbarchart.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkBarChartWidget::SkFltkBarChartWidget(int x, int y, int w, int h) : Fl_Widget(x, y, w, h)
{
    bgColor = SkColor(0, 0, 0).toFlColor();
    alertColor = SkColor(255, 0, 0).toFlColor();
    peakHolderColor = SkColor(255, 0, 200).toFlColor();
    segmentsOnColor = SkColor(0, 255, 0).toFlColor();
    segmentsOffColor = SkColor(30, 30, 30).toFlColor();

    margin = 1;

    alertEnabled = true;
    peakHolderEnabled = true;
    peakHolderReducingTime = 0.1;

    gradientEnabled = true;
    gradientAverage = 0;

    minValue = 0;
    maxValue = 0;
}

SkFltkBarChartWidget::~SkFltkBarChartWidget()
{
    clear();
}

void SkFltkBarChartWidget::clear()
{
    for(ULong i=0; i<bars.count(); i++)
        delete bars.at(i);

    bars.clear();
}

int SkFltkBarChartWidget::handle(int e)
{
    return Fl_Widget::handle(e);
}

void SkFltkBarChartWidget::calculateGeometries(int margin)
{
    //DRAWING rectangle
    drawingRect = SkRegion(x(), y(), w(), h()); //m on width will be considered from segmentDraw

    ah = 0;

    if (alertEnabled)
        ah = 4;

    //bar width and height
    bw = static_cast<float>(drawingRect.w) / static_cast<float>(bars.count());
    bh = drawingRect.h-ah-margin; //segments

    //Values and increment per segment
    range = fabs(maxValue - minValue);
    valIncr = range / static_cast<float>(bh);
    gradientAverage = 1.f/static_cast<float>(bh);

    for(ULong i=0; i<bars.count(); i++)
        bars.at(i)->lastPeakHolderSegNum = 0;

    lastRect = SkRegion(x(), y(), w(), h());
}

void SkFltkBarChartWidget::draw()
{
    fl_push_clip(x(), y(), w(), h());
    fl_rectf(x(), y(), w(), h(), bgColor);
    //draw_label();
    fl_pop_clip();

    if (bars.isEmpty())
        return;

    if (x() != lastRect.x || y() != lastRect.y || w() != lastRect.w || h() != lastRect.h)
        calculateGeometries(margin);

    if (static_cast<int>(bw) <= margin)
        return;

    float segVal = 0.f;
    ULong barIDX = 0;
    int segY = 0;
    int x0 = 0;

    reducePeakHolder();

    for(float x=0.f; x<static_cast<float>(drawingRect.w) && barIDX < bars.count() ; x+=bw)
    {
        //cout << barIDX << "-\n";
        SkFltkMeterBar *meterBar = bars.at(barIDX++);
        x0 = drawingRect.x + static_cast<int>(x);

        if (alertEnabled && meterBar->alert)
        {
            //cout << "!!! "<< x0 << " " << drawingRect.y << " " << bw << " "<< static_cast<int>(bw) << " " << margin << " " << ah << "\n";
            fl_rectf(x0, drawingRect.y, static_cast<int>(bw)-margin, ah, alertColor);
            //meterBar->alert = false;
        }

        for(int y=drawingRect.h-1, segCounter=0; y>=ah+margin; y-=2, segCounter+=2)
        {
            segY = drawingRect.y+y;
            segVal = minValue + (valIncr*static_cast<float>(segCounter));

            if (segVal > meterBar->value)
            {
                if (peakHolderEnabled && segCounter == meterBar->lastPeakHolderSegNum)
                {
                    if (gradientEnabled)
                        fl_color(fl_color_average(peakHolderColor, segmentsOffColor, gradientAverage*static_cast<float>(segCounter)));

                    else
                        fl_color(peakHolderColor);
                }

                else
                    fl_color(segmentsOffColor);
            }

            else
            {
                if (peakHolderEnabled && segCounter > meterBar->lastPeakHolderSegNum && segVal+valIncr > meterBar->value)
                {
                    meterBar->lastPeakHolderSegNum = segCounter;

                    if (gradientEnabled)
                        fl_color(fl_color_average(peakHolderColor, segmentsOffColor, gradientAverage*static_cast<float>(segCounter)));

                    else
                        fl_color(peakHolderColor);
                }

                else
                {
                    if (gradientEnabled)
                        fl_color(fl_color_average(segmentsOnColor, segmentsOffColor, gradientAverage*static_cast<float>(segCounter)));

                    else
                        fl_color(segmentsOnColor);
                }
            }

            fl_line(x0, segY, x0+static_cast<int>(bw)-margin*2, segY);
        }
    }
}

void SkFltkBarChartWidget::reducePeakHolder()
{
    if (peakHolderEnabled && chronoReducingPeakHolder.stop() > peakHolderReducingTime)
    {
        for(ULong i=0; i<bars.count(); i++)
        {
            SkFltkMeterBar *meterBar = bars.at(i);

            if (peakHolderEnabled && meterBar->lastPeakHolderSegNum)
                meterBar->lastPeakHolderSegNum -= 2;
        }

        chronoReducingPeakHolder.start();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define barChart   DynCast(SkFltkBarChartWidget, widget)

ConstructorImpl(SkFltkBarChart, SkFltkWidgetWrapper)
{}

void SkFltkBarChart::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkBarChartWidget(region.x, region.y, region.w, region.h));
}

bool SkFltkBarChart::setParameter(SkString &k, SkVariant &v)
{
    if (k == "bounds")
    {
        double min;
        double max;

        if (!grabBounds(k.c_str(), v, min, max))
            return false;

        setBounds(min, max);
    }

    else if (k == "alert")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableAlert(val);
    }

    else if (k == "gradient")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableGradient(val);
    }

    else if (k == "enablePeakHolderLeds")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enablePeakHolderLeds(val);
    }

    else if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else if (k == "peakColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setPeakHolderColor(c);
    }

    else if (k == "segmentColor_ON")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setSegmentsColor_ON(c);
    }

    else if (k == "segmentColor_OFF")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setSegmentsColor_OFF(c);
    }


    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkBarChart::setBounds(float min, float max)
{
    barChart->minValue = min;
    barChart->maxValue = max;
}

void SkFltkBarChart::setBackGroundColor(SkColor bg)
{
    barChart->bgColor = bg.toFlColor();
}

void SkFltkBarChart::enableAlert(bool value)
{
    barChart->alertEnabled = value;
}

void SkFltkBarChart::enablePeakHolderLeds(bool value, double reducingTime)
{
    barChart->peakHolderEnabled = value;
    barChart->peakHolderReducingTime = reducingTime;
}

void SkFltkBarChart::setPeakHolderColor(SkColor pk)
{
    barChart->peakHolderColor = pk.toFlColor();
}

void SkFltkBarChart::setSegmentsColor_ON(SkColor on)
{
    barChart->segmentsOnColor = on.toFlColor();
}

void SkFltkBarChart::setSegmentsColor_OFF(SkColor off)
{
    barChart->segmentsOffColor = off.toFlColor();
}

void SkFltkBarChart::enableGradient(bool value)
{
    barChart->gradientEnabled = value;
}

void SkFltkBarChart::addMeter(SkFltkMeterBar *meterBar)
{
    meterBar->value = barChart->minValue;
    barChart->bars.append(meterBar);
}

void SkFltkBarChart::delMeter(SkFltkMeterBar *meterBar)
{
    delete barChart->bars.remove(meterBar);
}

SkFltkMeterBar *SkFltkBarChart::getMeter(ULong index)
{
    return barChart->bars.at(index);
}

float SkFltkBarChart::getMin()
{
    return barChart->minValue;
}

float SkFltkBarChart::getMax()
{
    return barChart->maxValue;
}

ULong SkFltkBarChart::count()
{
    return barChart->bars.count();
}

void SkFltkBarChart::clear()
{
    barChart->clear();
    redraw();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
