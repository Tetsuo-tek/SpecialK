#ifndef SKFLTKVLAYOUT_H
#define SKFLTKVLAYOUT_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltklayout.h"
#include "FL/Fl_Group.H"
#include "Core/Containers/skset.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkVLayout;

class SkFltkVLayoutWidget extends Fl_Group
{
    public:
        SkFltkVLayout *owner;
        int spacing;
        bool stretch;

        SkFltkVLayoutWidget(int x, int y, int w, int h);
        ~SkFltkVLayoutWidget();

        void recalculateChildrenSizes();

    protected:
        int handle(int)                                         override;
        void resize(int x, int y, int w, int h)                 override;
        void draw()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkVLayout extends SkFltkLayout
{
    public:
        Constructor(SkFltkVLayout, SkFltkLayout);

        void setSpacing(int i);
        void enableStretch(bool enable);

        void add(SkFltkWidgetWrapper *wrapper)                      override;
        void remove(SkFltkWidgetWrapper *wrapper)                   override;

        int spacing();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);

        void onShow()                                           override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKVLAYOUT_H
