#ifndef SKFLTKLAYOUT_H
#define SKFLTKLAYOUT_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapper(SkFltkLayoutSpacer, SkFltkBox, Fl_Box);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Group.H"

class SkFltkLayout extends SkFltkWidgetWrapper
{
    public:
        virtual void add(SkFltkWidgetWrapper *)                             =0;
        //virtual void insert(SkFltkWidgetWrapper *, int)                     =0;
        virtual void remove(SkFltkWidgetWrapper *)                          =0;

        void removeFirst();
        void removeAt(int index);
        void removeLast();

        ULong childrenCount();
        SkFltkWidgetWrapper *child(int index);

        void clear();

        static void adaptWidgetToCell(Fl_Widget *widget, SkRegion &region);

    protected:
        AbstractConstructor(SkFltkLayout, SkFltkWidgetWrapper);

        bool setParameter(SkString &k, SkVariant &v);
};

#endif

#endif // SKFLTKLAYOUT_H
