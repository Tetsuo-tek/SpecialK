#ifndef SKFLTKWIDGETWRAPPER_H
#define SKFLTKWIDGETWRAPPER_H

#if defined(ENABLE_FLTK)

#include <FL/names.h>
#include <FL/Fl.H>
#include <FL/Fl.H>
#include "FL/Fl_Widget.H"

#include "Core/Object/skobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkPoint
{
    int x;
    int y;

    SkPoint();
    SkPoint(int x, int y);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkSize
{
    int w;
    int h;

    SkSize();
    SkSize(int w, int h);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkRegion
{
    int x;
    int y;
    int w;
    int h;

    SkRegion();
    SkRegion(int x, int y, int w, int h);
    SkRegion(SkPoint origin, SkSize size);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkColor
{
    int r;
    int g;
    int b;

    SkColor();
    SkColor(int r, int g, int b);
    SkColor(const Fl_Color &flColor);
    SkColor(const SkColor &other);

    SkColor &operator  =    (const SkColor &operand);
    bool     operator  ==   (const SkColor &operand);

    Fl_Color toFlColor();

    static SkColor white();
    static SkColor silver();
    static SkColor lightGray();
    static SkColor gray();
    static SkColor darkGray();
    static SkColor ultraDarkGray();
    static SkColor black();

    static SkColor red();
    static SkColor darkRed();
    static SkColor ultraDarkRed();

    static SkColor green();
    static SkColor darkGreen();
    static SkColor ultraDarkGreen();

    static SkColor blue();
    static SkColor darkBlue();
    static SkColor ultraDarkBlue();

    static SkColor maroon();
    static SkColor yellow();
    static SkColor olive();
    static SkColor cyan();
    static SkColor darkCyan();
    static SkColor navy();
    static SkColor magenta();
    static SkColor purple();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkUI;
class SkFltkWindow;

typedef uchar SkFltkWidget_T;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkFltkHAlignPolicy
{
    HCenterAlign,
    LeftAlign,
    RightAlign
};

enum SkFltkVAlignPolicy
{
    VCenterAlign,
    TopAlign,
    BottomAlign
};

enum SkFltkResizePolicy
{
    Resizable,
    FixedHeight,
    FixedWidth,
    Fixed
};

class SkFltkWidgetWrapper extends SkObject
{
    public:
        void setup(CStr *name, SkRegion region=SkRegion());
        void setup(CStr *name, SkTreeMap<SkString, SkVariant> &props);

        void changeWidgetName(CStr *name);

        void setHorizontalAlignPolicy(SkFltkHAlignPolicy policy);
        void setVerticalAlignPolicy(SkFltkVAlignPolicy policy);
        void setResizePolicy(SkFltkResizePolicy policy);
        //void setWidgetType(SkFltkWidget_T t);

        void setBackGroundColor(SkColor color);
        void setSelectionColor(SkColor color);

        void setLabel(CStr *text, bool asCopy=false);
        void setLabelType(Fl_Labeltype type);
        void setLabelFont(Fl_Font font);
        void setLabelFontSize(int size);
        void setLabelFontColor(SkColor textColor);
        void setToolTip(CStr *text);

        void setAlignment(Fl_Align align);
        void setBoxType(Fl_Boxtype type);

        void setEnabled(bool value);
        void setVisible(bool value);
        void setFocus();

        void move(SkPoint point);
        void resize(SkSize size);
        void resize(SkRegion region);

        virtual void show();
        void hide();
        void redraw();

        void destroyWidgetLater();

        SkFltkHAlignPolicy hAlignPolicy();
        SkFltkVAlignPolicy vAlignPolicy();
        SkFltkResizePolicy resizePolicy();

        int x();
        int y();
        int w();
        int h();

        SkPoint origin();
        SkRegion region();
        SkSize size();

        SkFltkWidget_T widgetType();

        SkColor getBackGroundColor();
        SkColor getSelectionColor();
        CStr *getLabel();
        Fl_Font getLabelFont();
        Fl_Fontsize getLabelFontSize();
        SkColor getLabelTextColor();
        Fl_Labeltype getLabelType();
        SkSize getLabelSize();
        CStr *getToolTip();
        Fl_Align getAlignment();
        Fl_Boxtype getBoxType();

        bool isEnabled();
        bool isVisible();
        bool hasFocus();

        bool hasTopWindow();
        SkFltkWindow *topWindow();
        bool isValid();

        CStr *name();
        Fl_Widget *internalWidget();
        SkFltkUI *intefaceRegister();

        void invokeCallBack();

    protected:
        Fl_Widget *widget;

        AbstractConstructor(SkFltkWidgetWrapper, SkObject);

        void setWidget(Fl_Widget *w);
        virtual void onWidgetSetup(SkRegion &region)                    = 0;
        virtual bool onWidgetParameterSetup(SkString &, SkVariant &)    = 0;

        //Each subclasser type has its own shadowing method as this, if it ha parameters
        bool setParameter(SkString &k, SkVariant &v);

        //Each subclasser type can use these meths as JSON-config utilities
        bool grabVariantList(CStr *k, SkVariant &v, SkVariantVector &l);
        bool grabColor(CStr *k, SkVariant &v, SkColor &c);
        bool grabColorList(CStr *k, SkVariant &v, SkVector<SkColor> &l);
        bool grabPoint(CStr *k, SkVariant &v, SkPoint &p);
        bool grabSize(CStr *k, SkVariant &v, SkSize &sz);
        bool grabSizeRange(CStr *k, SkVariant &v, SkSize &s1, SkSize &s2);
        bool grabRegion(CStr *k, SkVariant &v, SkRegion &r);
        bool grabFont(CStr *k, SkVariant &v, Fl_Font &f);
        bool grabCursor(CStr *k, SkVariant &v, Fl_Cursor &cr);
        bool grabBoxType(CStr *k, SkVariant &v, Fl_Boxtype &bt);
        bool grabAlign(CStr *k, SkVariant &v, Fl_Align &a);
        bool grabOrientation(CStr *k, SkVariant &v, bool &horizontal);
        bool grabScrollOrientation(CStr *k, SkVariant &v, int &orientation);
        bool grabHAlignPolicy(CStr *k, SkVariant &v, SkFltkHAlignPolicy &policy);
        bool grabVAlignPolicy(CStr *k, SkVariant &v, SkFltkVAlignPolicy &policy);
        bool grabResizePolicy(CStr *k, SkVariant &v, SkFltkResizePolicy &policy);
        bool grabBounds(CStr *k, SkVariant &v, double &min, double &max);
        bool grabBool(CStr *k, SkVariant &v, bool &val);
        bool grabInteger(CStr *k, SkVariant &v, int &val);
        bool grabDouble(CStr *k, SkVariant &v, double &val);
        bool grabString(CStr *k, SkVariant &v, SkString &val);
        bool grabStringList(CStr *k, SkVariant &v, SkStringList &l);

        void setCallBack(Fl_Callback *cb, SkFltkWidgetWrapper *skWidget);

        friend class SkFltkWindow;
        virtual void onShow()                                           {redraw();}
        virtual void onHide()                                           {}

    private:
        SkString widgetName;

        SkFltkHAlignPolicy hAlignPol;
        SkFltkVAlignPolicy vAlignPol;
        SkFltkResizePolicy resizePol;

        void onPreparingToDie()                                         override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_RGB_Image.H>

#if defined(ENABLE_CV)
    #include <Multimedia/Image/skmat.h>
#endif
class SkFltkImageBox extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkImageBox, SkFltkWidgetWrapper);

        void setImage(uint8_t *jpeg);

#if defined(ENABLE_CV)
        void setImage(Mat &im);
#endif

    private:
        //Fl_JPEG_Image *rgbJpegFL;
        Fl_Image *rgbFL_resized;

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define StupidWrapper(WRAPPER_T, SUPERWRAPPER_T, FL_T) \
    class WRAPPER_T extends SUPERWRAPPER_T \
    { \
        public: \
            Constructor(WRAPPER_T, SUPERWRAPPER_T); \
            void onWidgetSetup(SkRegion &region) override; \
            bool onWidgetParameterSetup(SkString &k, SkVariant &v) override \
            {return setParameter(k, v);} \
    }

#define StupidWrapperImpl(WRAPPER_T, SUPERWRAPPER_T, FL_T, CB) \
    ConstructorImpl(WRAPPER_T, SUPERWRAPPER_T) \
    {} \
    void  WRAPPER_T::onWidgetSetup(SkRegion &region) \
    { \
        setWidget(new FL_T(region.x, region.y, region.w, region.h)); \
        if (CB) \
            setCallBack((Fl_Callback *) CB, this); \
    }

#define WindowWrapperImpl(WRAPPER_T, SUPERWRAPPER_T, FL_T, CB) \
    ConstructorImpl(WRAPPER_T, SUPERWRAPPER_T) \
    {} \
    void WRAPPER_T::onWidgetSetup(SkRegion &region) \
    { \
        setWidget(new FL_T(region.w, region.h)); \
        if (CB) \
            setCallBack((Fl_Callback *) CB, this); \
    }

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Box.H"
StupidWrapper(SkFltkBox, SkFltkWidgetWrapper, Fl_Box);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKWIDGETWRAPPER_H
