#if defined(ENABLE_FLTK)

#include "skfltkwindows.h"
#include "skfltkui.h"
#include "Core/App/skapp.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define window      DynCast(Fl_Window, widget)

ConstructorImpl(SkFltkWindow, SkAbstractFltkGroup)
{
    winLayout = nullptr;
    footerLayout = nullptr;

    SignalSet(open);
    SignalSet(closed);
    SlotSet(toggleFullScreen);
}

void SkFltkWindow::onWidgetSetup(SkRegion &region)
{
    if (region.x == 0 && region.y == 0)
        setWidget(new Fl_Window(region.w, region.h));

    else
        setWidget(new Fl_Window(region.x, region.y, region.w, region.h));

    setCallBack((Fl_Callback *) w_CB, this);
}

bool SkFltkWindow::setParameter(SkString &k, SkVariant &v)
{
    if (k == "title")
    {
        SkString str;

        if (!grabString(k.c_str(), v, str))
            return false;

        setTitle(str.c_str(), true);
    }

    else if (k == "defaultCursor")
    {
        Fl_Cursor cr;

        if (!grabCursor(k.c_str(), v, cr))
            return false;

        setDefaultCursor(cr);
    }

    else if (k == "cursor")
    {
        Fl_Cursor cr;

        if (!grabCursor(k.c_str(), v, cr))
            return false;

        setCursor(cr);
    }

    else if (k == "sizeRange")
    {
        SkSize s1;
        SkSize s2;

        if (!grabSizeRange(k.c_str(), v, s1, s2))
            return false;

        setSizeRange(s1, s2, 1, 1);
    }

    else if (k == "decorations")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableWmDecorations(val);
    }

    else if (k == "layout")
    {
        if (winLayout)
        {
            ObjectError("Layout ALREADY exists");
            return false;
        }

        if (!v.isMap())
        {
            ObjectError("Layout element MUST be a T_MAP, instead it is: " << v.variantTypeName());
            return false;
        }

        SkTreeMap<SkString, SkVariant> props;
        v.copyToMap(props);

        SkString t("SkFltk");
        t.append(props["type"].toString());

        if (!isSuperClassOf("SkFltkLayout", t.c_str()))
        {
            ObjectError("Layout element MUST be a derivate of SkFltkLayout, instead it is: " << t);
            return false;
        }

        bool resizable = true;

        if (props.contains("resizable"))
            resizable = props.remove("resizable").value().toBool();

        bool hasFooter = props.contains("footer");
        SkVariant footerVal;
        int footerHeight = 0;

        if (hasFooter)
        {
            footerVal = props.remove("footer").value();

            if (!footerVal.isMap())
            {
                ObjectError("Layout.Footer element MUST be a T_MAP, instead it is: " << footerVal.variantTypeName());
                return false;
            }

            SkTreeMap<SkString, SkVariant> props;
            footerVal.copyToMap(props);

            SkString t("SkFltk");
            t.append(props["type"].toString());

            if (!isSuperClassOf("SkFltkLayout", t.c_str()))
            {
                ObjectError("Layout.Footer element MUST be a derivate of SkFltkLayout, instead it is: " << t);
                return false;
            }

            if (!props.contains("height"))
            {
                ObjectError("Layout.Footer MUST have an HEIGHT");
                return false;
            }

            grabInteger("height", props["height"], footerHeight);
            props.remove("height");

            SkVariantList region;
            region << x() << h()-footerHeight << w() << footerHeight;
            props["region"] = region;

            SkString n = name();
            n.append(".Footer");
            props["name"] = n;

            SkFltkWidgetWrapper *wrapper = SkFltkUI::buildWrapper(props);

            if (!wrapper)
                return false;

            setFooter(DynCast(SkFltkLayout, wrapper));
        }

        SkVariantList region;
        region << x() << y() << w() << (h()-footerHeight);
        props["region"] = region;

        SkString n = name();
        n.append(".Layout");
        props["name"] = n;

        SkFltkWidgetWrapper *wrapper = SkFltkUI::buildWrapper(props);

        if (!wrapper)
            return false;

        setLayout(DynCast(SkFltkLayout, wrapper), resizable);
    }

    else
        return SkAbstractFltkGroup::setParameter(k, v);

    return true;
}

void SkFltkWindow::setLayout(SkFltkLayout *layout, bool resizable)
{
    if (winLayout)
    {
        ObjectError("Layout ALREADY exists");
        return;
    }

    winLayout = layout;
    add(winLayout, resizable);
}

void SkFltkWindow::setFooter(SkFltkLayout *footer)
{
    if (footerLayout)
    {
        ObjectError("Layout.Footer ALREADY exists");
        return;
    }

    footerLayout = footer;
    add(footerLayout);
}

void SkFltkWindow::setTitle(CStr *text, bool asCopy)
{
    if (asCopy)
    {
        window->copy_label(text);
        return;
    }

    window->label(text);
}

void SkFltkWindow::setDefaultCursor(Fl_Cursor cursor)
{
    window->default_cursor(cursor);
}

void SkFltkWindow::setCursor(Fl_Cursor cursor)
{
    window->cursor(cursor);
}

void SkFltkWindow::setSizeRange(SkSize min, SkSize max, int wSteps, int hSteps)
{
    window->size_range(min.w, min.h, max.w, max.h, wSteps, hSteps);
}

void SkFltkWindow::enableWmDecorations(bool value)
{
    window->border(value);
}

void SkFltkWindow::enableFullScreen()
{
    if (!isEnabledFullscreen())
        window->fullscreen();
}

void SkFltkWindow::disableFullScreen()
{
    if (isEnabledFullscreen())
        window->fullscreen_off();
}

void SkFltkWindow::disableFullScreen(SkRegion toRegion)
{
    if (isEnabledFullscreen())
        window->fullscreen_off(toRegion.x, toRegion.y, toRegion.w, toRegion.h);
}

void SkFltkWindow::iconize()
{
    window->iconize();
}

void SkFltkWindow::show()
{
    Fl::visual(FL_RGB);
    window->show();
    window->wait_for_expose();
    notifyOpen();
}

void SkFltkWindow::notifyOpen()
{
    SkList<SkObject *> childrenWrappers;
    recursiveChildren(childrenWrappers);

    SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

    while(itr->next())
    {
        SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());
        child->onShow();
    }

    delete itr;

    open();
}

void SkFltkWindow::notifyClosed()
{
    SkList<SkObject *> childrenWrappers;
    recursiveChildren(childrenWrappers);

    SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

    while(itr->next())
    {
        SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());
        child->onHide();
    }

    delete itr;

    hide();
    closed();
}

void SkFltkWindow::close()
{
    invokeCallBack();
}

SkFltkLayout *SkFltkWindow::layout()
{
    return winLayout;
}

SkFltkLayout *SkFltkWindow::footer()
{
    return footerLayout;
}

bool SkFltkWindow::isEnabledFullscreen()
{
    return window->fullscreen_active();
}

bool SkFltkWindow::isModal()
{
    return window->modal();
}

bool SkFltkWindow::hasWmDecorations()
{
    return window->border();
}

SkSize SkFltkWindow::getWmDecorationsSize()
{
    return SkSize(window->decorated_w(), window->decorated_h());
}

SkPoint SkFltkWindow::absPosition()
{
    return SkPoint(window->x_root(), window->y_root());
}

bool SkFltkWindow::isShown()
{
    return window->shown();
}

CStr *SkFltkWindow::title()
{
    return window->label();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFltkWindow, toggleFullScreen)
{
    SilentSlotArgsWarning();

    if (isEnabledFullscreen())
    {
        disableFullScreen();
        redraw();
    }

    else
        enableFullScreen();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFltkWindow::w_CB(Fl_Window *, void *data)
{
    SkFltkWindow * wWrapper = static_cast<SkFltkWindow *>(data);
    Fl_Event tempFlEvt = static_cast<Fl_Event>(Fl::event());
    ObjectMessage_EXT(wWrapper, "Close (EVT: " << fl_eventnames[tempFlEvt] << "[" << tempFlEvt << "])");

    wWrapper->notifyClosed();
    wWrapper->destroyLater();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define dblWindow   DynCast(Fl_Double_Window, widget)

ConstructorImpl(SkFltkDoubleWindow, SkFltkWindow)
{}

void SkFltkDoubleWindow::onWidgetSetup(SkRegion &region)
{
    if (region.x == 0 && region.y == 0)
        setWidget(new Fl_Double_Window(region.w, region.h));

    else
        setWidget(new Fl_Double_Window(region.x, region.y, region.w, region.h));

    setCallBack((Fl_Callback *) SkFltkWindow::w_CB, this);
}

void SkFltkDoubleWindow::show()
{
    Fl::visual(FL_DOUBLE);
    dblWindow->show();
    dblWindow->wait_for_expose();
    notifyOpen();
}
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFltkModal, SkFltkWindow)
{}

void SkFltkModal::onWidgetSetup(SkRegion &region)
{
    if (region.x == 0 && region.y == 0)
        setWidget(new Fl_Double_Window(region.w, region.h));

    else
        setWidget(new Fl_Double_Window(region.x, region.y, region.w, region.h));

    window->set_modal();
    setCallBack((Fl_Callback *) SkFltkWindow::w_CB, this);
}

void SkFltkModal::show()
{
    Fl::visual(FL_DOUBLE);
    window->show();
    window->wait_for_expose();
    notifyOpen();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
