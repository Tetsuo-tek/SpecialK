#if defined(ENABLE_FLTK)

#include "skfltkbuttons.h"

#define button      DynCast(Fl_Button, widget)

ConstructorImpl(SkFltkButton, SkFltkWidgetWrapper)
{
    SignalSet(clicked);
}

void SkFltkButton::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Button(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) w_CB, this);
}

bool SkFltkButton::setParameter(SkString &k, SkVariant &v)
{
    if (k == "checked")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        setChecked(val);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkButton::setChecked(bool value)
{
    button->value(value);
}

bool SkFltkButton::isChecked()
{
    return button->value();
}

void SkFltkButton::w_CB(Fl_Button *, void *data)
{
    SkFltkButton *wWrapper = static_cast<SkFltkButton *>(data);
    Fl_Event tempFlEvt = static_cast<Fl_Event>(Fl::event());
    ObjectMessage_EXT(wWrapper, "BtnClick (EVT: " << fl_eventnames[tempFlEvt] << "[" << tempFlEvt << "])");
    wWrapper->clicked();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkRepeatButton, SkFltkButton, Fl_Repeat_Button, SkFltkButton::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkLightButton, SkFltkButton, Fl_Light_Button, SkFltkButton::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkCheckButton, SkFltkLightButton, Fl_Check_Button, SkFltkButton::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkRadioButton, SkFltkLightButton, Fl_Round_Button, SkFltkButton::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkRadioRoundButton, SkFltkRadioButton, Fl_Radio_Round_Button, SkFltkButton::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
