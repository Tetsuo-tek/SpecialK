#ifndef SKFLTKINDICATORS_H
#define SKFLTKINDICATORS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Progress.H"

class SkFltkProgressBar extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkProgressBar, SkFltkWidgetWrapper);

        void setMinimum(float value);
        void setMaximum(float value);
        void setValue(float value);

        float getMinimum();
        float getMaximum();
        float getValue();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Time/skelapsedtime.h"

class SkFltkLed extends SkFltkWidgetWrapper
{
        bool state;
        bool pulsed;
        bool blinking;
        SkColor c_ON;
        SkColor c_OFF;
        SkElapsedTimeMillis pulsingChrono;
        ULong timeOut_OFF;

    public:
        Constructor(SkFltkLed, SkFltkWidgetWrapper);

        void setColor_ON(SkColor on);
        void setColor_OFF(SkColor off);
        void setTimeout(int timeOutMS);
        void switchState(bool state);

        Slot(on);
        Slot(off);
        Slot(pulse);
        Slot(blink);
        Slot(toggle);
        Slot(toggleBlinking);

        Slot(fastTick);
        Slot(slowTick);

    protected:
        void onWidgetSetup(SkRegion &region);
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
SkFltkGauge*/

#endif

#endif // SKFLTKINDICATORS_H
