#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkhlayout.h"
#include "skfltkui.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkHLayoutWidget::SkFltkHLayoutWidget(int x, int y, int w, int h) : Fl_Group(x, y, w, h)
{
    owner = nullptr;
    spacing = 0;
    stretch = false;
    resizable(nullptr);
}

SkFltkHLayoutWidget::~SkFltkHLayoutWidget()
{
}

int SkFltkHLayoutWidget::handle(int e)
{
    if (!e)
        return 0;

    //ObjectMessage_EXT(owner, "Event [" << e << "] -> " << fl_eventnames[e]);
    return Fl_Group::handle(e);
}

void SkFltkHLayoutWidget::resize(int x, int y, int w, int h)
{
    Fl_Group::resize(x, y, w, h);
    recalculateChildrenSizes();
}

void SkFltkHLayoutWidget::draw()
{
    Fl_Group::draw();
    recalculateChildrenSizes();
}

void SkFltkHLayoutWidget::recalculateChildrenSizes()
{
    if (!children())
        return;

    SkRegion region;
    region.x = x();
    region.y = y();
    region.h = h();

    region.w = (w() - ((children()-1) * spacing)) / children();

    if (stretch)
    {
        SkList<SkObject *> &childrenWrappers = owner->children();
        SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

        int i = 0;
        int o = 0;
        int notConstrained = 0;
        int widths[children()];
        memset(widths, 0, children()*sizeof(int));

        while(itr->next())
        {
            SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());
            SkFltkResizePolicy resizePol = child->resizePolicy();

            if (resizePol == Fixed || resizePol == FixedWidth)
            {
                o += (region.w - child->w());
                widths[i] = child->w();
            }

            else
                notConstrained++;

            i++;
        }

        itr->goToBegin();

        int maxWidth = region.w;

        if (notConstrained)
            maxWidth += (o/notConstrained);

        i = 0;

        do
        {
            SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());

            if (widths[i])
                region.w = widths[i];

            else
                region.w = maxWidth;

            SkFltkLayout::adaptWidgetToCell(child->internalWidget(), region);
            region.x += (region.w+spacing);
            i++;
        } while(itr->next());

        delete itr;
    }

    else
    {
        for (int i=0; i<children(); i++)
        {
            Fl_Widget *widget = child(i);
            SkFltkLayout::adaptWidgetToCell(widget, region);
            region.x += (region.w+spacing);
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define hLayout  DynCast(SkFltkHLayoutWidget, widget)

ConstructorImpl(SkFltkHLayout, SkFltkLayout)
{}

void SkFltkHLayout::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkHLayoutWidget(region.x, region.y, region.w, region.h));
    hLayout->owner = this;
}

void SkFltkHLayout::onShow()
{
    hLayout->recalculateChildrenSizes();
}

bool SkFltkHLayout::setParameter(SkString &k, SkVariant &v)
{
    if (k == "spacing")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setSpacing(val);
    }

    else if (k == "stretch")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableStretch(val);
    }

    else
        return SkFltkLayout::setParameter(k, v);

    return true;
}

void SkFltkHLayout::setSpacing(int i)
{
    hLayout->spacing = i;
}

void SkFltkHLayout::enableStretch(bool enable)
{
    hLayout->stretch = enable;
}

void SkFltkHLayout::add(SkFltkWidgetWrapper *wrapper)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    SkFltkResizePolicy policy = wrapper->resizePolicy();

    if ((policy == Fixed || policy == FixedHeight))
    {
        if (h() < wrapper->h())
            resize(SkSize(w(), wrapper->h()));
    }

    else
        wrapper->resize(SkSize(wrapper->w(), h()));

    hLayout->add(*wrapper->internalWidget());
    ObjectMessage("Wrapper ADDED: " << wrapper->name() << " [" << wrapper->typeName() << "]");

    wrapper->setParent(this);
}

void SkFltkHLayout::remove(SkFltkWidgetWrapper *wrapper)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    hLayout->remove(*wrapper->internalWidget());
    wrapper->setParent(nullptr);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
