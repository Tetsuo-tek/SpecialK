#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkcurvechart.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkCurvesChartWidget::SkFltkCurvesChartWidget(int x, int y, int w, int h) : Fl_Box(x, y, w, h)
{
    range = 0.;
    valIncr = 0.;
    minValue = -1.;
    maxValue = 1.;
    curvesDrawType = SkFltkCurveType::PointCurve;
    bgColor = SkColor(255, 255, 255).toFlColor();
    subSpikesColor = SkColor::darkGray().toFlColor();
}

SkFltkCurvesChartWidget::~SkFltkCurvesChartWidget()
{
    clear();
}

void SkFltkCurvesChartWidget::clear()
{
    for(ULong i=0; i<curves.count(); i++)
        delete curves.at(i);

    curves.clear();
}

int SkFltkCurvesChartWidget::handle(int e)
{
    return Fl_Box::handle(e);
}

void SkFltkCurvesChartWidget::draw()
{
    Fl_Box::draw();

    /*fl_push_clip(x(), y(), w(), h());
    fl_rectf(x(), y(), w(), h(), bgColor);
    fl_pop_clip();*/

    if (x() != drw.x || y() != drw.y || w() != drw.w || h() != drw.h)
    {
        for(ULong z=0; z<curves.count(); z++)
            curves.at(z)->samples.clear();

        drw = SkRegion(x(), y(), w(), h());
        range = fabs(maxValue - minValue);

        if (range == 0.)
            valIncr = 0;
        else
            valIncr = range/static_cast<double>(drw.h);
    }

    fl_push_clip(drw.x, drw.y, drw.w, drw.h);
    fl_rectf(drw.x, drw.y, drw.w, drw.h, bgColor);
    fl_pop_clip();

    if (curves.isEmpty())
        return;

    if (range == 0.)
        return;

    int min = minValue / valIncr;
    int zero = maxValue / valIncr;

    if (curvesDrawType == PointCurve || curvesDrawType == PointSpikeCurve || curvesDrawType == SpikeCurve)
    {
        bool withSpikes = (curvesDrawType == PointSpikeCurve || (curvesDrawType == SpikeCurve));
        bool withPoints = (curvesDrawType == PointCurve || (curvesDrawType == PointSpikeCurve));

        for(ULong z=0; z<curves.count(); z++)
        {
            SkFltkCurve *curve = curves.at(z);

            int startX = drw.x;

            /*if (curve->samples.count() < static_cast<ULong>(drw.w))
                startX = drw.x + (drw.w - curve->samples.count());*/

            SkAbstractListIterator<double> *itr = curve->samples.iterator();

            int x = startX;
            int y = 0;

            for(int i=0; i<drw.w; i++)
            {
                itr->next();

                double &v = itr->item();
                y = drw.h - (v / valIncr) + min;

                if (withSpikes)
                {
                    fl_color(subSpikesColor);
                    fl_line(x, zero, x, y);
                }

                if (withPoints)
                {
                    fl_color(curve->color);
                    fl_point(x, y);
                }

                x++;
            }

            delete itr;            
        }
    }

    if (curvesDrawType == LineCurve || curvesDrawType == LineSpikeCurve
             || curvesDrawType == StepCurve || curvesDrawType == StepSpikeCurve)
    {
        bool withSpikes = (curvesDrawType == LineSpikeCurve || curvesDrawType == StepSpikeCurve);

        int x0 = 0;
        int y0 = 0;
        int x1 = 0;
        int y1 = 0;

        for(ULong z=0; z<curves.count(); z++)
        {
            SkFltkCurve *curve = curves.at(z);

            int startX = drw.x;

            /*if (curve->samples.count() < static_cast<ULong>(drw.w))
                startX = drw.x + (drw.w - curve->samples.count());*/

            SkAbstractListIterator<double> *itr = curve->samples.iterator();
            itr->next();

            x0 = startX;
            y0 = drw.h - (itr->item() / valIncr) + min;

            for(int i=x0; i<drw.w; i++)
            {
                if (!itr->hasNext())
                    break;

                itr->next();
                double &v = itr->item();

                x1 = x0 + 1;
                y1 = drw.h - (v / valIncr) + min;

                if (withSpikes)
                {
                    fl_color(subSpikesColor);
                    fl_line(x0, zero, x0, y0);
                }

                fl_color(curve->color);

                if (curvesDrawType == LineCurve || curvesDrawType == LineSpikeCurve)
                    fl_line(x0, y0, x1, y1);

                else
                    fl_line(x0, y0, x1, y0);

                x0 = x1;
                y0 = y1;
            }

            delete itr;
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define curveChart   DynCast(SkFltkCurvesChartWidget, widget)

ConstructorImpl(SkFltkCurvesChart, SkFltkWidgetWrapper)
{}

void SkFltkCurvesChart::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkCurvesChartWidget(region.x, region.y, region.w, region.h));
}

bool SkFltkCurvesChart::setParameter(SkString &k, SkVariant &v)
{
    if (k == "bounds")
    {
        double min;
        double max;

        if (!grabBounds(k.c_str(), v, min, max))
            return false;

        setBounds(min, max);
    }

    else if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else if (k == "curve")
    {
        SkString str = v.toString();

        if (str == "Point")
            setCurveType(PointCurve);

        else if (str == "PointSpike")
            setCurveType(PointSpikeCurve);

        else if (str == "Line")
            setCurveType(LineCurve);

        else if (str == "LineSpike")
            setCurveType(LineSpikeCurve);

        else if (str == "Step")
            setCurveType(StepCurve);

        else if (str == "StepSpike")
            setCurveType(StepSpikeCurve);

        else if (str == "Spike")
            setCurveType(SpikeCurve);

        else
        {
            ObjectError("Curve type MUST be one of this: Point, PointSpike, Line, LineSpike, Step, StepSpike, Spike");
            return false;
        }
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

/*void SkFltkCurvesChart::enableAxis(bool value)
{
    curveChart->axis.enabled = value;
}

void SkFltkCurvesChart::setAxis(SkColor axisCol)
{
    curveChart->axis.color = axisCol;
}*/

void SkFltkCurvesChart::setBounds(double yMin, double yMax)
{
    if (yMin >= yMax)
    {
        ObjectError("Max MUST be greater than Min; reset to default [-1, +1]");
        return;
    }

    curveChart->minValue = yMin;
    curveChart->maxValue = yMax;
}

void SkFltkCurvesChart::setBackGroundColor(SkColor bg)
{
    curveChart->bgColor = bg.toFlColor();
}

void SkFltkCurvesChart::setCurveType(SkFltkCurveType type)
{
    curveChart->curvesDrawType = type;
}

void SkFltkCurvesChart::addCurve(SkFltkCurve *curve)
{
    curveChart->curves.append(curve);
}

void SkFltkCurvesChart::delCurve(SkFltkCurve *curve)
{
    curveChart->curves.remove(curve);
    delete curve;
}

void SkFltkCurvesChart::clear()
{
    curveChart->clear();
    redraw();
}

ULong SkFltkCurvesChart::count()
{
    return curveChart->curves.count();
}

SkFltkCurve *SkFltkCurvesChart::curve(ULong index)
{
    return curveChart->curves.at(index);
}

SkFltkCurveType SkFltkCurvesChart::curveType()
{
    return curveChart->curvesDrawType;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
