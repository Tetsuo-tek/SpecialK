#ifndef SKFLTKBUTTONS_H
#define SKFLTKBUTTONS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Button.H"

class SkFltkButton extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkButton, SkFltkWidgetWrapper);

        void setChecked(bool value);
        bool isChecked();

        Signal(clicked);

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);

        static void w_CB(Fl_Button *, void *data);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Repeat_Button.H"
StupidWrapper(SkFltkRepeatButton, SkFltkButton, Fl_Repeat_Button);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Light_Button.H"
StupidWrapper(SkFltkLightButton, SkFltkButton, Fl_Light_Button);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Check_Button.H"
StupidWrapper(SkFltkCheckButton, SkFltkLightButton, Fl_Check_Button);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Round_Button.H"
StupidWrapper(SkFltkRadioButton, SkFltkLightButton, Fl_Round_Button);
//  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Radio_Round_Button.H"
StupidWrapper(SkFltkRadioRoundButton, SkFltkRadioButton, Fl_Radio_Round_Button);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKBUTTONS_H
