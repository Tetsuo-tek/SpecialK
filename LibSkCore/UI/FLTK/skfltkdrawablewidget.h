#ifndef SKFLTKDRAWABLEWIDGET_H
#define SKFLTKDRAWABLEWIDGET_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkFltkDrawableGeometry_T
{
    PointGeometry,
    LineGeometry,
    PolyLineGeometry,
    RectangleGeometry,
    EllipseGeometry,
    PolygonGeometry,
    TextGeometry
};

struct SkFltkDrawableGeometry
{
    SkFltkDrawableGeometry_T t;
    SkString name;
    SkColor color;
};

struct SkFltkDrawablePoint extends SkFltkDrawableGeometry
{
    SkPoint p;

    SkFltkDrawablePoint()
    {t = PointGeometry;}
};

struct SkFltkDrawableLine extends SkFltkDrawableGeometry
{
    SkPoint p1;
    SkPoint p2;

    SkFltkDrawableLine()
    {t = LineGeometry;}
};

struct SkFltkDrawablePolyLine extends SkFltkDrawableGeometry
{
    SkList<SkPoint *> poly;

    SkFltkDrawablePolyLine()
    {t = PolyLineGeometry;}
};

struct SkFltkDrawableRectangle extends SkFltkDrawableGeometry
{
    SkRegion r;

    SkFltkDrawableRectangle()
    {t = RectangleGeometry;}
};

struct SkFltkDrawableEllipse extends SkFltkDrawableGeometry
{
    SkPoint origin;
    int radius1;
    int radius2;
    int angle1;
    int angle2;

    SkFltkDrawableEllipse()
    {t = EllipseGeometry;}
};

struct SkFltkDrawablePolygon extends SkFltkDrawableGeometry
{
    SkFltkDrawableText()
    {t = PolygonGeometry;}
};

struct SkFltkDrawableText extends SkFltkDrawableGeometry
{
    SkPoint origin;
    Fl_Font font;
    Fl_Fontsize fontSize;
    SkString text;

    SkFltkDrawableText()
    {t = TextGeometry;}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkDrawableWidget extends Fl_Widget
{
    public:
        SkFltkDrawableWidget(int x, int y, int w, int h);
        ~SkFltkDrawableWidget();

        SkTreeMap<CStr *, SkFltkDrawableGeometry *> geometries;

        void clear();

    protected:
        int handle(int)                                         override;
        void draw()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkDrawable extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkDrawable, SkFltkWidgetWrapper);

        bool addGeometry(CStr *name, SkFltkDrawableGeometry *geometry);
        SkFltkDrawableGeometry *getGeometry(CStr *name);
        void delGeometry(CStr *name);

        void clear();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);

    private:
};

#endif

#endif // SKFLTKDRAWABLEWIDGET_H
