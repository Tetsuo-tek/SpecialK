#ifndef SKFLTKFORMINPUTS_H
#define SKFLTKFORMINPUTS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Input.H"

class SkFltkLineInput extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkLineInput, SkFltkWidgetWrapper);

        void setText(const char *text);

        void setCursorColor(SkColor color);

        void setFont(Fl_Font font);
        void setFontSize(int size);
        void setFontColor(SkColor textColor);

        void setSelectionMark(int to);
        void setCurrentPos(int pos);
        void setCurrentPos(int pos, int markTo);
        void setNavTab(int tab);
        void enableTextWrap(bool value);
        void setReadOnly(bool value);
        void setMaxInputLen(int max);

        void clear();
        void undo();

        const char *text();

        SkColor getCursorColor();
        Fl_Font getFont();
        Fl_Fontsize getFontSize();
        SkColor getTextColor();
        int getSelectionMark();
        int getCurrentPos();
        int getNavTab();
        bool isTextWrapEnabled();
        bool isReadOnly();
        int getInputType();
        Long currentByteSize();
        int getMaxInputLen();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Secret_Input.H"
StupidWrapper(SkFltkPassword, SkFltkLineInput, Fl_Secret_Input);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Multiline_Input.H"
StupidWrapper(SkFltkMultiline, SkFltkLineInput, Fl_Multiline_Input);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_File_Input.H"
StupidWrapper(SkFltkFileInput, SkFltkLineInput, Fl_File_Input);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Int_Input.H"

class SkFltkIntInput extends SkFltkLineInput
{
    public:
        Constructor(SkFltkIntInput, SkFltkLineInput);

        void setValue(int val);
        int value();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Float_Input.H"

class SkFltkFloatInput extends SkFltkLineInput
{
    public:
        Constructor(SkFltkFloatInput, SkFltkLineInput);

        void setValue(float val, int precision=1, bool fixed=true);
        float value();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKFORMINPUTS_H
