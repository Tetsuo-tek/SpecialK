#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkvlayout.h"
#include "skfltkui.h"
#include "FL/Fl.H"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkVLayoutWidget::SkFltkVLayoutWidget(int x, int y, int w, int h) : Fl_Group(x, y, w, h)
{
    owner = nullptr;
    spacing = 0;
    stretch = false;
    resizable(nullptr);
}

SkFltkVLayoutWidget::~SkFltkVLayoutWidget()
{
}

int SkFltkVLayoutWidget::handle(int e)
{
    if (!e)
        return 0;

    //ObjectMessage_EXT(owner, "Event [" << e << "] -> " << fl_eventnames[e]);
    return Fl_Group::handle(e);
}

void SkFltkVLayoutWidget::resize(int x, int y, int w, int h)
{
    Fl_Group::resize(x, y, w, h);
    recalculateChildrenSizes();
}

void SkFltkVLayoutWidget::draw()
{
    Fl_Group::draw();
    recalculateChildrenSizes();
}

void SkFltkVLayoutWidget::recalculateChildrenSizes()
{
    if (!children())
        return;

    SkRegion region;
    region.x = x();
    region.y = y();
    region.w = w();

    region.h = (h() - ((children()-1) * spacing)) / children();

    if (stretch)
    {
        SkList<SkObject *> &childrenWrappers = owner->children();
        SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

        int i = 0;
        int o = 0;
        int notConstrained = 0;
        int heights[children()];
        memset(heights, 0, children()*sizeof(int));

        while(itr->next())
        {
            SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());
            SkFltkResizePolicy resizePol = child->resizePolicy();

            if (resizePol == Fixed || resizePol == FixedHeight)
            {
                o += (region.h - child->h());
                heights[i] = child->h();
            }

            else
                notConstrained++;

            i++;
        }

        itr->goToBegin();

        int maxHeight = region.h;

        if (notConstrained)
            maxHeight += (o/notConstrained);

        i = 0;

        do
        {
            SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());

            if (heights[i])
                region.h = heights[i];

            else
                region.h = maxHeight;

            SkFltkLayout::adaptWidgetToCell(child->internalWidget(), region);
            region.y += (region.h+spacing);
            i++;
        } while(itr->next());

        delete itr;
    }

    else
    {
        for (int i=0; i<children(); i++)
        {
            Fl_Widget *widget = child(i);
            SkFltkLayout::adaptWidgetToCell(widget, region);
            region.y += (region.h+spacing);
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define vLayout  DynCast(SkFltkVLayoutWidget, widget)

ConstructorImpl(SkFltkVLayout, SkFltkLayout)
{}

void SkFltkVLayout::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkVLayoutWidget(region.x, region.y, region.w, region.h));
    vLayout->owner = this;
}

void SkFltkVLayout::onShow()
{
    vLayout->recalculateChildrenSizes();
}

bool SkFltkVLayout::setParameter(SkString &k, SkVariant &v)
{
    if (k == "spacing")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setSpacing(val);
    }

    else if (k == "stretch")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableStretch(val);
    }

    else
        return SkFltkLayout::setParameter(k, v);

    return true;
}

void SkFltkVLayout::setSpacing(int i)
{
    vLayout->spacing = i;
}

void SkFltkVLayout::enableStretch(bool enable)
{
    vLayout->stretch = enable;
}

void SkFltkVLayout::add(SkFltkWidgetWrapper *wrapper)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    SkFltkResizePolicy policy = wrapper->resizePolicy();

    if (policy == Fixed || policy == FixedWidth)
    {
        if (w() < wrapper->w())
            resize(SkSize(wrapper->w(), h()));
    }

    else
        wrapper->resize(SkSize(w(), wrapper->h()));

    vLayout->add(*wrapper->internalWidget());
    ObjectMessage("Wrapper ADDED: " << wrapper->name() << " [" << wrapper->typeName() << "]");

    wrapper->setParent(this);
}

void SkFltkVLayout::remove(SkFltkWidgetWrapper *wrapper)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    vLayout->remove(*wrapper->internalWidget());
    wrapper->setParent(nullptr);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
