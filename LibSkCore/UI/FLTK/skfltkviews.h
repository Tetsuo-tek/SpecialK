#ifndef SKFLTKVIEWS_H
#define SKFLTKVIEWS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkforminputs.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Output.H"
StupidWrapper(SkFltkLabel, SkFltkLineInput, Fl_Output);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Multiline_Output.H"
StupidWrapper(SkFltkTextView, SkFltkLabel, Fl_Multiline_Output);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKVIEWS_H
