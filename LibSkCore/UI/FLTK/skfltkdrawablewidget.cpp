#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkdrawablewidget.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkDrawableWidget::SkFltkDrawableWidget(int x, int y, int w, int h) : Fl_Widget(x, y, w, h)
{

}

SkFltkDrawableWidget::~SkFltkDrawableWidget()
{
    clear();
}

int SkFltkDrawableWidget::handle(int e)
{
    return Fl_Widget::handle(e);
}

void SkFltkDrawableWidget::draw()
{

}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define drawable    DynCast(SkFltkDrawableWidget, widget)

ConstructorImpl(SkFltkDrawable, SkFltkWidgetWrapper)
{}

void SkFltkDrawable::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkDrawableWidget(region.x, region.y, region.w, region.h));
}

bool SkFltkDrawable::setParameter(SkString &k, SkVariant &v)
{
    if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFltkDrawable::addGeometry(CStr *name, SkFltkDrawableGeometry *geometry)
{
    if (drawable->geometries.contains(name))
    {
        ObjectError("Geometry name ALREADY exists: " << name);
        return;
    }

    drawable->geometries[name] = geometry;
}

SkFltkDrawableGeometry *SkFltkDrawable::getGeometry(CStr *name)
{
    if (!drawable->geometries.contains(name))
    {
        ObjectError("Geometry name NOT found: " << name);
        return;
    }

    return drawable->geometries[name];
}

void SkFltkDrawable::delGeometry(CStr *name)
{
    if (!drawable->geometries.contains(name))
    {
        ObjectError("Geometry name NOT found: " << name);
        return;
    }

    delete drawable->geometries.remove(name).value();
}

void SkFltkDrawable::clear()
{
    //drawable->clear();
    redraw();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
