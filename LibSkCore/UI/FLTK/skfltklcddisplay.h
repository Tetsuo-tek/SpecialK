#ifndef SKFLTKLCDDISPLAY_H
#define SKFLTKLCDDISPLAY_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkLcdDisplay;

class SkFltkLcdDisplayWidget extends Fl_Box
{
    public:
        SkFltkLcdDisplay *owner;

        Fl_Color bgColor;
        Fl_Color onColor;
        Fl_Color offColor;
        int margin;
        ULong fixedCiphersCount;
        bool fixedSurplusCiphersLedOn;
        ULong floatPrecisionCount;
        SkVariant v;

        SkFltkLcdDisplayWidget(int x, int y, int w, int h);
        ~SkFltkLcdDisplayWidget();

        void clear();

        static void drawDisplay(SkFltkWidgetWrapper *owner,
                                SkVariant &v,
                                SkRegion &drw,
                                Fl_Color bgColor,
                                Fl_Color c_ON,
                                Fl_Color c_OFF,
                                int fixedCiphersCount, bool fixedSurplusCiphersLedOn,
                                int floatPrecisionCount);

        static void drawSegments(SkRegion &drw,
                                 Fl_Color c_ON,
                                 Fl_Color c_OFF,
                                 int cipher,
                                 float segHLen,
                                 float segVLen,
                                 float o1,
                                 float o2);

    protected:
        int handle(int)                             override;
        void draw()                                 override;

    private:
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkLcdDisplay extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkLcdDisplay, SkFltkWidgetWrapper);

        void setBackGroundColor(SkColor bg);
        void setSegmentsOnColor(SkColor c);
        void setSegmentsOffColor(SkColor c);
        void setFixed(int count);
        void setSurplusZerosLedOn(bool val);
        void setPrecision(int count);
        void setValue(const SkVariant &val);
        SkVariant &value();
        void clear();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKLCDDISPLAY_H
