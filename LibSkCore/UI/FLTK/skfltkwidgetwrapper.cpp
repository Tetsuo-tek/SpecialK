#if defined(ENABLE_FLTK)

#include "skfltkwidgetwrapper.h"
#include "skfltkwindows.h"
#include "skfltkui.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkPoint::SkPoint()
{
    this->x = 0;
    this->y = 0;
}

SkPoint::SkPoint(int x, int y)
{
    this->x = x;
    this->y = y;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkSize::SkSize()
{
    this->w = 0;
    this->h = 0;
}

SkSize::SkSize(int w, int h)
{
    this->w = w;
    this->h = h;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkRegion::SkRegion()
{
    this->x = 0;
    this->y = 0;
    this->w = 0;
    this->h = 0;
}

SkRegion::SkRegion(int x, int y, int w, int h)
{
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
}

SkRegion::SkRegion(SkPoint origin, SkSize size)
{
    this->x = origin.x;
    this->y = origin.y;
    this->w = size.w;
    this->h = size.h;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkColor::SkColor()
{
    this->r = 0;
    this->g = 0;
    this->b = 0;
}

SkColor::SkColor(int r, int g, int b)
{
    this->r = r;
    this->g = g;
    this->b = b;
}

SkColor::SkColor(const Fl_Color &flColor)
{
    this->r = (flColor >> 24) & 255;
    this->g = (flColor >> 16) & 255;
    this->b = (flColor >>  8) & 255;
}

SkColor::SkColor(const SkColor &other)
{
    this->r = other.r;
    this->g = other.g;
    this->b = other.b;
}

SkColor &SkColor::operator = (const SkColor &operand)
{
    this->r = operand.r;
    this->g = operand.g;
    this->b = operand.b;

    return *this;
}

bool SkColor::operator == (const SkColor &operand)
{
    return (r == operand.r && g == operand.g && b == operand.b);
}

Fl_Color SkColor::toFlColor()   {return fl_rgb_color(r, g, b);}

SkColor SkColor::white()            {return SkColor(255, 255, 255);}
SkColor SkColor::silver()           {return SkColor(192, 192, 192);}
SkColor SkColor::lightGray()        {return SkColor(160, 160, 160);}
SkColor SkColor::gray()             {return SkColor(128, 128, 128);}
SkColor SkColor::darkGray()         {return SkColor(80, 80, 80);}
SkColor SkColor::ultraDarkGray()    {return SkColor(40, 40, 40);}
SkColor SkColor::black()            {return SkColor(0, 0, 0);}

SkColor SkColor::red()              {return SkColor(255, 0, 0);}
SkColor SkColor::darkRed()          {return SkColor(80, 0, 0);}
SkColor SkColor::ultraDarkRed()     {return SkColor(40, 0, 0);}
SkColor SkColor::green()            {return SkColor(0, 255, 0);}
SkColor SkColor::darkGreen()        {return SkColor(0, 80, 0);}
SkColor SkColor::ultraDarkGreen()   {return SkColor(0, 40, 0);}
SkColor SkColor::blue()             {return SkColor(0, 0, 255);}
SkColor SkColor::darkBlue()         {return SkColor(0, 0, 80);}
SkColor SkColor::ultraDarkBlue()    {return SkColor(0, 0, 40);}

SkColor SkColor::maroon()           {return SkColor(128, 0, 0);}
SkColor SkColor::yellow()           {return SkColor(255, 255, 0);}
SkColor SkColor::olive()            {return SkColor(128, 128, 0);}
SkColor SkColor::cyan()             {return SkColor(0, 255, 255);}
SkColor SkColor::darkCyan()         {return SkColor(0, 128, 128);}
SkColor SkColor::navy()             {return SkColor(0, 0, 128);}
SkColor SkColor::magenta()          {return SkColor(255, 0, 255);}
SkColor SkColor::purple()           {return SkColor(128, 0, 128);}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkFltkWidgetWrapper, SkObject)
{
    resizePol = Resizable;
    hAlignPol = HCenterAlign;
    vAlignPol = VCenterAlign;
    widget = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//called from DERIVATED::onWidgetSetup
void SkFltkWidgetWrapper::setWidget(Fl_Widget *w)
{
    widget = w;

    Fl_Group *g = w->as_group();

    if (g)
    {
        ObjectMessage("New group CREATED [region: " << widget->x() << ", " << widget->y() << ", "
                                                    << widget->w() << ", " << widget->h() << "]"
                                                    << ": " << typeName());

        //NOT USING THIS MODE TO ADD WIDGET
        g->end();
    }

    else
        ObjectMessage("New widget CREATED [region: " << widget->x() << ", " << widget->y() << ", "
                                                     << widget->w() << ", " << widget->h() << "]"
                                                     << ": " << typeName());


}

void SkFltkWidgetWrapper::setup(CStr *name, SkRegion region)
{
    setObjectName(name);
    widgetName = name;
    onWidgetSetup(region);
    ui->registerWidget(name, this);
}

void SkFltkWidgetWrapper::setup(CStr *name, SkTreeMap<SkString, SkVariant> &props)
{
    setObjectName(name);
    widgetName = name;

    SkRegion r;
    SkVariantVector region;

    if (props.contains("region"))
    {
        props.remove("region").value().copyToList(region);

        if (region.count() != 4)
            ObjectError("'region' list parameter has wrong number of elements (must be 4 for x, y, h and w)");

        AssertKiller(region.count() != 4);

        for(ULong i=0; i<4; i++)
            AssertKiller(!region[i].isInteger());

        r.x = region[0].toInt();
        r.y = region[1].toInt();
        r.w = region[2].toInt();
        r.h = region[3].toInt();
    }

    else if (props.contains("size"))
    {
        props.remove("size").value().copyToList(region);

        if (region.count() != 2)
            ObjectError("'size' list parameter has wrong number of elements (must be 2 for h and w)");

        AssertKiller(region.count() != 2);

        for(ULong i=0; i<2; i++)
            AssertKiller(!region[i].isInteger());

        r.w = region[0].toInt();
        r.h = region[1].toInt();
    }

    onWidgetSetup(r);
    AssertKiller(widget == nullptr);

    ui->registerWidget(name, this);

    SkBinaryTreeVisit<SkString, SkVariant> *itr = props.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        AssertKiller(!onWidgetParameterSetup(k, v));
    }

    delete itr;
}

bool SkFltkWidgetWrapper::setParameter(SkString &k, SkVariant &v)
{
    if (k == "label")
    {
        SkString str;

        if (!grabString(k.c_str(), v, str))
            return false;

        setLabel(str.c_str(), true);
    }

    else if (k == "labelFont")
    {
        Fl_Font f;

        if (!grabFont(k.c_str(), v, f))
            return false;

        setLabelFont(f);
    }

    else if (k == "labelFontSize")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setLabelFontSize(val);
    }

    else if (k == "labelFontColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setLabelFontColor(c);
    }

    else if (k == "tooltip")
    {
        SkString str;

        if (!grabString(k.c_str(), v, str))
            return false;

        setToolTip(str.c_str());
    }

    else if (k == "box")
    {
        Fl_Boxtype bt;

        if (!grabBoxType(k.c_str(), v, bt))
            return false;

        setBoxType(bt);
    }

    else if (k == "align")
    {
        Fl_Align a;

        if (!grabAlign(k.c_str(), v, a))
            return false;

        setAlignment(a);
    }

    else if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else if (k == "hAlignPolicy")
    {
        SkFltkHAlignPolicy policy;

        if (!grabHAlignPolicy(k.c_str(), v, policy))
            return false;

        setHorizontalAlignPolicy(policy);
    }

    else if (k == "vAlignPolicy")
    {
        SkFltkVAlignPolicy policy;

        if (!grabVAlignPolicy(k.c_str(), v, policy))
            return false;

        setVerticalAlignPolicy(policy);
    }

    else if (k == "resizePolicy")
    {
        SkFltkResizePolicy policy;

        if (!grabResizePolicy(k.c_str(), v, policy))
            return false;

        setResizePolicy(policy);
    }

    else if (k == "enable")
    {
        bool enable;

        if (!grabBool(k.c_str(), v, enable))
            return false;

        setEnabled(enable);
    }

    else if (k == "visible")
    {
        bool visible;

        if (!grabBool(k.c_str(), v, visible))
            return false;

        setVisible(visible);
    }

    else
    {
        ObjectError("'" << k << "' widget-parameter UNKNOWN");
        return false;
    }

    return true;
}

bool SkFltkWidgetWrapper::grabVariantList(CStr *k, SkVariant &v, SkVariantVector &l)
{
    if (v.variantType() != T_LIST)
    {
        ObjectError("'" << k << "' MUST be a T_LIST, instead it is: " << v.variantTypeName());
        return false;
    }

    v.copyToList(l);
    return true;
}

bool SkFltkWidgetWrapper::grabColor(CStr *k, SkVariant &v, SkColor &c)
{
    SkVariantVector rgb;

    if (!grabVariantList(k, v, rgb))
        return false;

    v.copyToList(rgb);

    if (rgb.count() != 3)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 3 elements (rgb), instead its count is: " << rgb.count());
        return false;
    }

    for(ULong i=0; i<3; i++)
        if (!rgb[i].isInteger())
        {
            ObjectError("'" << k << "' color component MUST be an integer, instead it is: " << rgb[i].variantTypeName());
            return false;
        }

    c.r = rgb[0].toInt();
    c.g = rgb[1].toInt();
    c.b = rgb[2].toInt();

    return true;
}

bool SkFltkWidgetWrapper::grabColorList(CStr *k, SkVariant &v, SkVector<SkColor> &l)
{
    SkVariantVector rgbList;

    if (!grabVariantList(k, v, rgbList))
        return false;

    for(ULong i=0; i<rgbList.count(); i++)
    {
        SkColor c;

        if (!grabColor(k, rgbList[i], c))
            return false;

        l << c;
    }

    return true;
}

bool SkFltkWidgetWrapper::grabPoint(CStr *k, SkVariant &v, SkPoint &p)
{
    SkVariantVector point;

    if (!grabVariantList(k, v, point))
        return false;

    if (point.count() != 2)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 2 elements (x, y), instead its count is: " << point.count());
        return false;
    }

    for(ULong i=0; i<2; i++)
        if (!point[i].isInteger())
        {
            ObjectError("'" << k << "' point component MUST be an integer, instead it is: " << point[i].variantTypeName());
            return false;
        }

    p.x = point[0].toInt();
    p.y = point[1].toInt();

    return true;
}

bool SkFltkWidgetWrapper::grabSize(CStr *k, SkVariant &v, SkSize &sz)
{
    SkVariantVector size;

    if (!grabVariantList(k, v, size))
        return false;

    if (size.count() != 2)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 2 elements (w, h), instead its count is: " << size.count());
        return false;
    }

    for(ULong i=0; i<2; i++)
        if (!size[i].isInteger())
        {
            ObjectError("'" << k << "' size component MUST be an integer, instead it is: " << size[i].variantTypeName());
            return false;
        }

    sz.w = size[0].toInt();
    sz.h = size[1].toInt();

    return true;
}

bool SkFltkWidgetWrapper::grabSizeRange(CStr *k, SkVariant &v, SkSize &s1, SkSize &s2)
{
    SkVariantVector range;

    if (!grabVariantList(k, v, range))
        return false;

    if (range.count() != 4)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 4 elements (w1, h1, w2, h2), instead its count is: " << range.count());
        return false;
    }

    for(ULong i=0; i<4; i++)
        if (!range[i].isInteger())
        {
            ObjectError("'" << k << "' size-range component MUST be an integer, instead it is: " << range[i].variantTypeName());
            return false;
        }

    s1.h = range[0].toInt();
    s1.h = range[1].toInt();
    s2.w = range[2].toInt();
    s2.h = range[3].toInt();

    return true;
}

bool SkFltkWidgetWrapper::grabRegion(CStr *k, SkVariant &v, SkRegion &r)
{
    SkVariantVector region;

    if (!grabVariantList(k, v, region))
        return false;

    if (region.count() != 4)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 4 elements (x, y, w, h), instead its count is: " << region.count());
        return false;
    }

    for(ULong i=0; i<4; i++)
        if (!region[i].isInteger())
        {
            ObjectError("'" << k << "' region component MUST be an integer, instead it is: " << region[i].variantTypeName());
            return false;
        }

    r.x = region[0].toInt();
    r.y = region[1].toInt();
    r.w = region[2].toInt();
    r.h = region[3].toInt();

    return true;
}

bool SkFltkWidgetWrapper::grabFont(CStr *k, SkVariant &v, Fl_Font &f)
{
    if (v.variantType() != T_STRING)
    {
        ObjectError("'" << k << "' parameter MUST be a T_STRING, instead it is: " << v.variantTypeName());
        return false;
    }

    SkString str = v.toString();

    if (!SkFltkUI::fonts().contains(str))
    {
        ObjectError("Font NOT found: " << str);
        return false;
    }

    f = SkFltkUI::fonts()[str];
    return true;
}

bool SkFltkWidgetWrapper::grabCursor(CStr *k, SkVariant &v, Fl_Cursor &cr)
{
    if (v.variantType() != T_STRING)
    {
        ObjectError("'" << k << "' parameter MUST be a T_STRING, instead it is: " << v.variantTypeName());
        return false;
    }

    SkString str = v.toString();

    if (!SkFltkUI::cursors().contains(str))
    {
        ObjectError("Cursor NOT found: " << str);
        return false;
    }

    cr = SkFltkUI::cursors()[str];
    return true;
}

bool SkFltkWidgetWrapper::grabBoxType(CStr *k, SkVariant &v, Fl_Boxtype &bt)
{
    if (v.variantType() != T_STRING)
    {
        ObjectError("'" << k << "' parameter MUST be a T_STRING, instead it is: " << v.variantTypeName());
        return false;
    }

    SkString str = v.toString();

    if (!SkFltkUI::boxTypes().contains(str))
    {
        ObjectError("Box-type NOT found: " << str);
        return false;
    }

    bt = SkFltkUI::boxTypes()[str];
    return true;
}

bool SkFltkWidgetWrapper::grabAlign(CStr *k, SkVariant &v, Fl_Align &a)
{
    if (v.variantType() != T_STRING &&  v.variantType() != T_LIST)
    {
        ObjectError("'" << k << "' parameter MUST be a T_STRING or a T_LIST, instead it is: " << v.variantTypeName());
        return false;
    }

    SkVariantVector l;
    v.copyToList(l);

    if (l.isEmpty())
        return false;

    if (!SkFltkUI::alignTypes().contains(l.first().toString()))
    {
        ObjectError("Align NOT found: " << l.first());
        return false;
    }

    a = SkFltkUI::alignTypes()[l.first().toString()];

    for(ULong i=1; i<l.count(); i++)
    {
        if (l[i].variantType() != T_STRING)
        {
            ObjectError("'" << k << "' element MUST be a T_STRING, instead it is: " << l[i].variantTypeName());
            return false;
        }

        if (!SkFltkUI::alignTypes().contains(l[i].toString()))
        {
            ObjectError("Align NOT found: " << l[i]);
            return false;
        }

        a |= SkFltkUI::alignTypes()[l[i].toString()];
    }

    return true;
}

bool SkFltkWidgetWrapper::grabOrientation(CStr *k, SkVariant &v, bool &horizontal)
{
    SkString str;

    if (!grabString(k, v, str))
        return false;

    if (str != "HORIZONTAL" && str != "VERTICAL")
    {
        ObjectError("Orientation MUST be a T_STRING in {'HORIZONTAL', 'VERTICAL'}, instead it is: " << v.toString());
        return false;
    }

    horizontal = (str == "HORIZONTAL");

    return true;
}

bool SkFltkWidgetWrapper::grabScrollOrientation(CStr *k, SkVariant &v, int &orientation)
{
    SkString str;

    if (!grabString(k, v, str))
        return false;

    if (!SkFltkUI::scrollOrientations().contains(str))
    {
        ObjectError("Scroll-orientation NOT found: " << str);
        return false;
    }

    orientation = SkFltkUI::scrollOrientations()[str];

    return true;
}

bool SkFltkWidgetWrapper::grabHAlignPolicy(CStr *k, SkVariant &v, SkFltkHAlignPolicy &policy)
{
    SkString str;

    if (!grabString(k, v, str))
        return false;

    if (!SkFltkUI::hAlignPolicies().contains(str))
    {
        ObjectError("Horizontal align-policy NOT found: " << str);
        return false;
    }

    policy = SkFltkUI::hAlignPolicies()[str];

    return true;
}

bool SkFltkWidgetWrapper::grabVAlignPolicy(CStr *k, SkVariant &v, SkFltkVAlignPolicy &policy)
{
    SkString str;

    if (!grabString(k, v, str))
        return false;

    if (!SkFltkUI::vAlignPolicies().contains(str))
    {
        ObjectError("Vertical align-policy NOT found: " << str);
        return false;
    }

    policy = SkFltkUI::vAlignPolicies()[str];

    return true;
}

bool SkFltkWidgetWrapper::grabResizePolicy(CStr *k, SkVariant &v, SkFltkResizePolicy &policy)
{
    SkString str;

    if (!grabString(k, v, str))
        return false;

    if (!SkFltkUI::resizePolicies().contains(str))
    {
        ObjectError("Resize-policy NOT found: " << str);
        return false;
    }

    policy = SkFltkUI::resizePolicies()[str];

    return true;
}

bool SkFltkWidgetWrapper::grabBounds(CStr *k, SkVariant &v, double &min, double &max)
{
    SkVariantVector bounds;

    if (!grabVariantList(k, v, bounds))
        return false;

    if (bounds.count() != 2)
    {
        ObjectError("'" << k << "' MUST be a T_LIST of 4 elements ([min, max]), instead its count is: " << bounds.count());
        return false;
    }

    for(ULong i=0; i<2; i++)
        if (!bounds[i].isNumber())
        {
            ObjectError("'" << k << "' element MUST be an number, instead it is: " << bounds[i].variantTypeName());
            return false;
        }

    min = bounds[0].toDouble();
    max = bounds[1].toDouble();

    return true;
}

bool SkFltkWidgetWrapper::grabBool(CStr *k, SkVariant &v, bool &val)
{
    if (v.variantType() != T_BOOL)
    {
        ObjectError("'" << k << "' MUST be a T_BOOL, instead it is: " << v.variantTypeName());
        return false;
    }

    val = v.toBool();
    return true;
}

bool SkFltkWidgetWrapper::grabInteger(CStr *k, SkVariant &v, int &val)
{
    if (!v.isInteger())
    {
        ObjectError("'" << k << "' MUST be an integer, instead it is: " << v.variantTypeName());
        return false;
    }

    val = v.toInt();
    return true;
}

bool SkFltkWidgetWrapper::grabDouble(CStr *k, SkVariant &v, double &val)
{
    if (!v.isReal())
    {
        ObjectError("'" << k << "' MUST be T_FLOAT or T_DOUBLE, instead it is: " << v.variantTypeName());
        return false;
    }

    val = v.toDouble();
    return true;
}

bool SkFltkWidgetWrapper::grabString(CStr *k, SkVariant &v, SkString &val)
{
    if (!v.isString())
    {
        ObjectError("'" << k << "' MUST be T_STRING, instead it is: " << v.variantTypeName());
        return false;
    }

    val = v.toString();
    return true;
}

bool SkFltkWidgetWrapper::grabStringList(CStr *k, SkVariant &v, SkStringList &l)
{
    if (v.variantType() != T_LIST)
    {
        ObjectError("'" << k << "' MUST be a be a T_LIST, instead it is: " << v.variantTypeName());
        return false;
    }

    v.copyToStringList(l);
    return true;
}

void SkFltkWidgetWrapper::onPreparingToDie()
{
    if (ui)
        ui->unRegisterWidget(widgetName.c_str());

    else
        ObjectError("Inteface register is NOT set");

    if (widget)
    {
        ObjectMessage("Destroying widget: " << name() << " [" << typeName() << "]");
        //Fl::delete_widget(widget);
    }
}

void SkFltkWidgetWrapper::changeWidgetName(CStr *name)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << name);
        return;
    }

    ui->unRegisterWidget(widgetName.c_str());
    widgetName = name;
    setObjectName(widgetName.c_str());
    ui->registerWidget(widgetName.c_str(), this);
}

void SkFltkWidgetWrapper::setVerticalAlignPolicy(SkFltkVAlignPolicy policy)
{
    vAlignPol = policy;
}

void SkFltkWidgetWrapper::setHorizontalAlignPolicy(SkFltkHAlignPolicy policy)
{
    hAlignPol = policy;
}

void SkFltkWidgetWrapper::setResizePolicy(SkFltkResizePolicy policy)
{
    resizePol = policy;
}

/*void SkFltkWidgetWrapper::setWidgetType(SkFltkWidget_T t)
{
    widget->type(t);
}*/

void SkFltkWidgetWrapper::setBackGroundColor(SkColor color)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->color(color.toFlColor());
    redraw();
}

void SkFltkWidgetWrapper::setSelectionColor(SkColor color)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->selection_color(color.toFlColor());
    redraw();
}

void SkFltkWidgetWrapper::setLabel(CStr *text, bool asCopy)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    if (asCopy)
    {
        widget->copy_label(text);
        return;
    }

    widget->label(text);
    widget->redraw();
}

void SkFltkWidgetWrapper::setLabelType(Fl_Labeltype type)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->labeltype(type);
    widget->redraw();
}

void SkFltkWidgetWrapper::setLabelFont(Fl_Font font)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->labelfont(font);
    widget->redraw();
}

void SkFltkWidgetWrapper::setLabelFontSize(int size)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->labelsize(size);
    widget->redraw();
}

void SkFltkWidgetWrapper::setLabelFontColor(SkColor textColor)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->labelcolor(textColor.toFlColor());
    widget->redraw();
}

void SkFltkWidgetWrapper::setToolTip(CStr *text)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->tooltip(text);
}

void SkFltkWidgetWrapper::setAlignment(Fl_Align align)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->align(align);

    redraw();
}

void SkFltkWidgetWrapper::setBoxType(Fl_Boxtype type)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->box(type);

    redraw();
}

void SkFltkWidgetWrapper::setEnabled(bool value)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    if (value)
        widget->activate();

    else
        widget->deactivate();

    redraw();
}

void SkFltkWidgetWrapper::setVisible(bool value)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    if (value)
        widget->set_visible();

    else
        widget->hide();

    redraw();
}

void SkFltkWidgetWrapper::setFocus()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->take_focus();
}

void SkFltkWidgetWrapper::move(SkPoint point)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->position(point.x, point.y);
}

void SkFltkWidgetWrapper::resize(SkSize size)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->size(size.w, size. h);
}

void SkFltkWidgetWrapper::resize(SkRegion region)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->resize(region.x, region.y, region.w, region.h);
}

void SkFltkWidgetWrapper::show()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->show();
}

void SkFltkWidgetWrapper::hide()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->hide();
}

void SkFltkWidgetWrapper::redraw()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    if (!hasTopWindow())
        return;

    widget->redraw();
}

void SkFltkWidgetWrapper::destroyWidgetLater()
{
    if (widget)
        Fl::delete_widget(widget);

    destroyLater();
}

SkFltkHAlignPolicy SkFltkWidgetWrapper::hAlignPolicy()
{
    return hAlignPol;
}

SkFltkVAlignPolicy SkFltkWidgetWrapper::vAlignPolicy()
{
    return vAlignPol;
}

SkFltkResizePolicy SkFltkWidgetWrapper::resizePolicy()
{
    return resizePol;
}

int SkFltkWidgetWrapper::x()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->x();
}

int SkFltkWidgetWrapper::y()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->y();
}

int SkFltkWidgetWrapper::w()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->w();
}

int SkFltkWidgetWrapper::h()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->h();
}

SkPoint SkFltkWidgetWrapper::origin()
{
    return SkPoint(x(), y());
}

SkRegion SkFltkWidgetWrapper::region()
{
    return SkRegion(x(), y(), w(), h());
}

SkSize SkFltkWidgetWrapper::size()
{
    return SkSize(w(), h());
}

SkFltkWidget_T SkFltkWidgetWrapper::widgetType()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->type();
}

SkColor SkFltkWidgetWrapper::getBackGroundColor()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return SkColor();
    }

    return widget->color();
}

SkColor SkFltkWidgetWrapper::getSelectionColor()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return SkColor();
    }

    return widget->selection_color();
}

CStr *SkFltkWidgetWrapper::getLabel()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return nullptr;
    }

    return widget->label();
}

Fl_Font SkFltkWidgetWrapper::getLabelFont()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->labelfont();
}

Fl_Fontsize SkFltkWidgetWrapper::getLabelFontSize()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return 0;
    }

    return widget->labelsize();
}

SkColor SkFltkWidgetWrapper::getLabelTextColor()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return SkColor();
    }

    return widget->labelcolor();
}

Fl_Labeltype SkFltkWidgetWrapper::getLabelType()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return FL_NO_LABEL;
    }

    return widget->labeltype();
}

SkSize SkFltkWidgetWrapper::getLabelSize()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return SkSize();
    }

    SkSize labelSz;
    widget->measure_label(labelSz.w, labelSz.h);
    return labelSz;
}

CStr *SkFltkWidgetWrapper::getToolTip()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return nullptr;
    }

    return widget->tooltip();
}

Fl_Align SkFltkWidgetWrapper::getAlignment()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return -1;
    }

    return widget->align();
}

Fl_Boxtype SkFltkWidgetWrapper::getBoxType()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return FL_NO_BOX;
    }

    return widget->box();
}

bool SkFltkWidgetWrapper::isEnabled()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return false;
    }

    return widget->active();
}

bool SkFltkWidgetWrapper::isVisible()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return false;
    }

    return widget->visible();
}

bool SkFltkWidgetWrapper::hasFocus()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return false;
    }

    return widget->visible_focus();
}

bool SkFltkWidgetWrapper::hasTopWindow()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return false;
    }

    return (widget->top_window() != nullptr);
}

SkFltkWindow *SkFltkWidgetWrapper::topWindow()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return nullptr;
    }

    return dynamic_cast<SkFltkWindow *>(ui->get(widget->top_window()));
}

bool SkFltkWidgetWrapper::isValid()
{
    return (widget != nullptr/* || widget->top_window() == nullptr*/);
}

void SkFltkWidgetWrapper::setCallBack(Fl_Callback *cb, SkFltkWidgetWrapper *skWidget)
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->callback(cb, skWidget);
}

void SkFltkWidgetWrapper::invokeCallBack()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return;
    }

    widget->do_callback();
}

CStr *SkFltkWidgetWrapper::name()
{
    if (!isValid())
    {
        ObjectError("Widget NOT registered yet: " << typeName());
        return nullptr;
    }

    return widgetName.c_str();
}

Fl_Widget *SkFltkWidgetWrapper::internalWidget()
{
    return widget;
}

SkFltkUI *SkFltkWidgetWrapper::intefaceRegister()
{
    return ui;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define img     DynCast(Fl_Box, widget)

ConstructorImpl(SkFltkImageBox, SkFltkWidgetWrapper)
{
    //rgbJpegFL = nullptr;
    rgbFL_resized = nullptr;
}

void SkFltkImageBox::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Box(region.x, region.y, region.w, region.h));
}

bool SkFltkImageBox::setParameter(SkString &k, SkVariant &v)
{
    return SkFltkWidgetWrapper::setParameter(k, v);
}

void SkFltkImageBox::setImage(uint8_t *jpeg)
{
    if (rgbFL_resized)
    {
        delete rgbFL_resized;
        rgbFL_resized = nullptr;
    }

    Fl_JPEG_Image *rgbJpegFL = new Fl_JPEG_Image(nullptr, jpeg);

    //Ubuntu-16.04 FLTK-1.3.3 has NOT fail()
    /*if (rgbJpegFL->fail())
    {
        ObjectError("Failed to load JPEG");
        return;
    }*/

    float ratio = (float) rgbJpegFL->h()/rgbJpegFL->w();
    int windowH = h();//-10;
    int windowW = w();//-10;

    if (((float) windowH / w()) > ratio)
        rgbFL_resized = rgbJpegFL->copy(windowW, windowW*ratio);

    else
        rgbFL_resized = rgbJpegFL->copy(windowH/ratio, windowH);

    delete rgbJpegFL;
    //rgbJpegFL = nullptr;

    img->image(rgbFL_resized);
    img->redraw();
}

#if defined(ENABLE_CV)
void SkFltkImageBox::setImage(Mat &im)
{
    if (rgbFL_resized)
    {
        delete rgbFL_resized;
        rgbFL_resized = nullptr;
    }

    Mat rgb;
    cvtColor(im, rgb, cv::COLOR_BGR2RGB);

    Fl_RGB_Image *flImage = new Fl_RGB_Image(rgb.data, rgb.cols, rgb.rows, rgb.channels());

    //Ubuntu-16.04 FLTK-1.3.3 has NOT fail()
    /*if (flImage->fail())
    {
        ObjectError("Failed to load cv::Mat");
        return;
    }*/

    float ratio = (float) flImage->h()/flImage->w();
    int windowH = h();//-10;
    int windowW = w();//-10;

    if (((float) windowH / w()) > ratio)
        rgbFL_resized = flImage->copy(windowW, windowW*ratio);

    else
        rgbFL_resized = flImage->copy(windowH/ratio, windowH);

    delete flImage;

    img->image(rgbFL_resized);
    img->redraw();
}
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkBox, SkFltkWidgetWrapper, Fl_Box, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
