#if defined(ENABLE_FLTK)

#include "skfltkforminputs.h"
#include "skfltkui.h"

#define input   DynCast(Fl_Input, widget)

ConstructorImpl(SkFltkLineInput, SkFltkWidgetWrapper)
{
}

void SkFltkLineInput::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Input(region.x, region.y, region.w, region.h));
}

bool SkFltkLineInput::setParameter(SkString &k, SkVariant &v)
{
    if (k == "text")
    {
        SkString txt;

        if (!grabString(k.c_str(), v, txt))
            return false;

        setText(txt.c_str());
    }

    else if (k == "font")
    {
        Fl_Font f;

        if (!grabFont(k.c_str(), v, f))
            return false;

        setFont(f);
    }

    else if (k == "fontSize")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setFontSize(val);
    }

    else if (k == "fontColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setFontColor(c);
    }

    else if (k == "readOnly")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        setReadOnly(val);
    }

    else if (k == "maxLen")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setMaxInputLen(val);
    }

    else if (k == "textWrap")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        enableTextWrap(val);
    }

    else if (k == "navTab")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        enableTextWrap(val);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}
void SkFltkLineInput::setText(const char *text)
{
    input->value(text);
}

void SkFltkLineInput::setCursorColor(SkColor color)
{
    input->cursor_color(color.toFlColor());
}

void SkFltkLineInput::setFont(Fl_Font font)
{
    input->textfont(font);
}

void SkFltkLineInput::setFontSize(int size)
{
    input->textsize(size);
}

void SkFltkLineInput::setFontColor(SkColor textColor)
{
    input->textcolor(textColor.toFlColor());
}

void SkFltkLineInput::setSelectionMark(int to)
{
    input->mark(to);
}

void SkFltkLineInput::setCurrentPos(int pos)
{
    input->position(pos);
}

void SkFltkLineInput::setCurrentPos(int pos, int markTo)
{
    input->position(pos, markTo);
}

void SkFltkLineInput::setNavTab(int tab)
{
    input->tab_nav(tab);
}

void SkFltkLineInput::enableTextWrap(bool value)
{
    input->wrap(value);
}

void SkFltkLineInput::setReadOnly(bool value)
{
    input->readonly(value);
}

void SkFltkLineInput::setMaxInputLen(int max)
{
    input->maximum_size(max);
}

void SkFltkLineInput::clear()
{
    input->value("");
}

void SkFltkLineInput::undo()
{
    input->undo();
}

const char *SkFltkLineInput::text()
{
    return input->value();
}

SkColor SkFltkLineInput::getCursorColor()
{
    return input->cursor_color();
}

Fl_Font SkFltkLineInput::getFont()
{
    return input->textfont();
}

Fl_Fontsize SkFltkLineInput::getFontSize()
{
    return input->textsize();
}

SkColor SkFltkLineInput::getTextColor()
{
    return input->textcolor();
}

int SkFltkLineInput::getSelectionMark()
{
    return input->mark();
}

int SkFltkLineInput::getCurrentPos()
{
    return input->position();
}

int SkFltkLineInput::getNavTab()
{
    return input->tab_nav();
}

bool SkFltkLineInput::isTextWrapEnabled()
{
    return input->wrap();
}

bool SkFltkLineInput::isReadOnly()
{
    return input->readonly();
}

int SkFltkLineInput::getInputType()
{
    return input->input_type();
}

Long SkFltkLineInput::currentByteSize()
{
    return input->size();
}

int SkFltkLineInput::getMaxInputLen()
{
    return input->maximum_size();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkPassword, SkFltkLineInput, Fl_Secret_Input, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkMultiline, SkFltkLineInput, Fl_Multiline_Input, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkFileInput, SkFltkLineInput, Fl_File_Input, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define intInput    DynCast(Fl_Float_Input, widget)

ConstructorImpl(SkFltkIntInput, SkFltkLineInput)
{}

void SkFltkIntInput::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Int_Input(region.x, region.y, region.w, region.h));
}

bool SkFltkIntInput::setParameter(SkString &k, SkVariant &v)
{
    if (k == "value")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setValue(val);
    }

    else
        return SkFltkLineInput::setParameter(k, v);

    return true;
}
void SkFltkIntInput::setValue(int val)
{
    SkString valString(val);
    intInput->value(valString.c_str());
}

int SkFltkIntInput::value()
{
    SkString txt(intInput->value());
    return txt.toInt();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define floatInput      DynCast(Fl_Float_Input, widget)

ConstructorImpl(SkFltkFloatInput, SkFltkLineInput)
{}

void SkFltkFloatInput::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Float_Input(region.x, region.y, region.w, region.h));
}

bool SkFltkFloatInput::setParameter(SkString &k, SkVariant &v)
{
    if (k == "value")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setValue(val);
    }

    else
        return SkFltkLineInput::setParameter(k, v);

    return true;
}
void SkFltkFloatInput::setValue(float val, int precision, bool fixed)
{
    SkString valString(val, precision, fixed);
    floatInput->value(valString.c_str());
}

float SkFltkFloatInput::value()
{
    SkString txt(floatInput->value());
    return txt.toFloat();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
