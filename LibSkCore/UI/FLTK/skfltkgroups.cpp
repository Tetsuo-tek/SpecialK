#if defined(ENABLE_FLTK)

#include "skfltkgroups.h"
#include "skfltkui.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define group   DynCast(Fl_Group, widget)

AbstractConstructorImpl(SkAbstractFltkGroup, SkFltkWidgetWrapper)
{}

bool SkAbstractFltkGroup::setParameter(SkString &k, SkVariant &v)
{
    if (k == "children")
    {
        if (v.variantType() != T_LIST)
        {
            ObjectError("Children MUST be a T_LIST, instead it is: " << v.variantTypeName());
            return false;
        }

        SkVariantVector children;
        v.copyToList(children);

        for(ULong i=0; i<children.count(); i++)
        {
            if (!children[i].isMap())
            {
                ObjectError("Each child element MUST be a T_MAP, instead it is: " << children[i].variantTypeName());
                return false;
            }

            SkTreeMap<SkString, SkVariant> props;
            children[i].copyToMap(props);

            bool resizable = false;

            if (props.contains("resizable"))
                resizable = props.remove("resizable").value().toBool();

            SkFltkWidgetWrapper *wrapper = SkFltkUI::buildWrapper(props);

            if (!wrapper)
                return false;

            add(wrapper, resizable);
        }
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkAbstractFltkGroup::add(SkFltkWidgetWrapper *wrapper, bool asResizable)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    if (asResizable)
    {
        group->add_resizable(*wrapper->internalWidget());
        ObjectMessage("Wrapper ADDED (as RESIZABLE): " << wrapper->name() << " [" << wrapper->typeName() << "]");
    }

    else
    {
        group->add(*wrapper->internalWidget());
        ObjectMessage("Wrapper ADDED: " << wrapper->name() << " [" << wrapper->typeName() << "]");
    }

    wrapper->setParent(this);
}

void SkAbstractFltkGroup::del(SkFltkWidgetWrapper *wrapper)
{
    if (!isValid())
    {
        ObjectError("Wrapper is NOT valid");
        return;
    }

    group->remove(*wrapper->internalWidget());
    wrapper->setParent(nullptr);
}

ULong SkAbstractFltkGroup::childrenCount()
{
    return group->children();
}

SkFltkWidgetWrapper *SkAbstractFltkGroup::child(int index)
{
    Fl_Widget *w = group->child(index);

    if (!w)
        return nullptr;

    return ui->get(w);
}

void SkAbstractFltkGroup::SkAbstractFltkGroup::clear()
{
    SkList<SkObject *> &childrenWrappers = children();
    SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

    while(itr->next())
    {
        SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());
        del(child);
        child->destroyLater();
    }

    delete itr;
}

bool SkAbstractFltkGroup::hasResizableWidget()
{
    return (group->resizable() != nullptr);
}

SkFltkWidgetWrapper *SkAbstractFltkGroup::resizableWidget()
{
    if (!hasResizableWidget())
        return nullptr;

    return ui->get(group->resizable());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define pack    DynCast(Fl_Pack, widget)

ConstructorImpl(SkFltkPack, SkAbstractFltkGroup)
{}

void SkFltkPack::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Pack(region.x, region.y, region.w, region.h));

    //defaults
    setHorizontal();
    setSpacing(5);
    setBoxType(FL_NO_BOX);
}

bool SkFltkPack::setParameter(SkString &k, SkVariant &v)
{
    if (k == "orientation")
    {
        bool isHorizontal;

        if (!grabOrientation(k.c_str(), v, isHorizontal))
            return false;

        if (isHorizontal)
            setHorizontal();
        else
            setVertical();
    }

    else if (k == "spacing")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setSpacing(val);
    }

    else
        return SkAbstractFltkGroup::setParameter(k, v);

    return true;
}

void SkFltkPack::setSpacing(int i)
{
    pack->spacing(i);
}

void SkFltkPack::setHorizontal()
{
    pack->type(Fl_Pack::HORIZONTAL);
}

void SkFltkPack::setVertical()
{
    pack->type(Fl_Pack::VERTICAL);
}

bool SkFltkPack::isHorizontal()
{
    return pack->type() == Fl_Pack::HORIZONTAL;
}

bool SkFltkPack::isVertical()
{
    return pack->type() == Fl_Pack::VERTICAL;
}

int SkFltkPack::spacing()
{
    return pack->spacing();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define scrollArea  DynCast(Fl_Scroll, widget)

ConstructorImpl(SkFltkScrollArea, SkAbstractFltkGroup)
{}

void SkFltkScrollArea::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Scroll(region.x, region.y, region.w, region.h));
}

bool SkFltkScrollArea::setParameter(SkString &k, SkVariant &v)
{
    if (k == "orientation")
    {
        int mode;

        if (!grabScrollOrientation(k.c_str(), v, mode))
            return false;

        setScrollOrientation(mode);
    }

    else
        return SkAbstractFltkGroup::setParameter(k, v);

    return true;
}

void SkFltkScrollArea::setScrollOrientation(int mode)
{
    scrollArea->type(mode);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkTile, SkAbstractFltkGroup, Fl_Tile, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif
