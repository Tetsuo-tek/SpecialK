#ifndef SKFLTKHLAYOUT_H
#define SKFLTKHLAYOUT_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltklayout.h"
#include "FL/Fl_Group.H"
#include "Core/Containers/skset.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkHLayout;

class SkFltkHLayoutWidget extends Fl_Group
{
    public:
        SkFltkHLayout *owner;
        int spacing;
        bool stretch;

        SkFltkHLayoutWidget(int x, int y, int w, int h);
        ~SkFltkHLayoutWidget();

        void recalculateChildrenSizes();

    protected:
        int handle(int)                                         override;
        void resize(int x, int y, int w, int h)                 override;
        void draw()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkHLayout extends SkFltkLayout
{
    public:
        Constructor(SkFltkHLayout, SkFltkLayout);

        void setSpacing(int i);
        void enableStretch(bool enable);

        void add(SkFltkWidgetWrapper *wrapper)                  override;
        //void insert(SkFltkWidgetWrapper *wrapper, int pos)      override;
        void remove(SkFltkWidgetWrapper *wrapper)               override;

        int spacing();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);

        void onShow()                                           override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKHLAYOUT_H
