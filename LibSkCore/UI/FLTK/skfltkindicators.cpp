#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkindicators.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define progressbar    DynCast(Fl_Progress, widget)

ConstructorImpl(SkFltkProgressBar, SkFltkWidgetWrapper)
{}

void SkFltkProgressBar::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Progress(region.x, region.y, region.w, region.h));
}

bool SkFltkProgressBar::setParameter(SkString &k, SkVariant &v)
{
    if (k == "min")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setMinimum(val);
    }

    else if (k == "max")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setMaximum(val);
    }

    else if (k == "value")
    {
        double val;

        if (!grabDouble(k.c_str(), v, val))
            return false;

        setValue(val);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkProgressBar::setMinimum(float value)
{
    progressbar->minimum(value);
}

void SkFltkProgressBar::setMaximum(float value)
{
    progressbar->maximum(value);
}

void SkFltkProgressBar::setValue(float value)
{
    if (isnan(value))
        value = 0;

    progressbar->value(value);
}

float SkFltkProgressBar::getMinimum()
{
    return progressbar->minimum();
}

float SkFltkProgressBar::getMaximum()
{
    return progressbar->maximum();
}

float SkFltkProgressBar::getValue()
{
    return progressbar->value();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/App/skeventloop.h"

#define led     DynCast(Fl_Box, widget)

ConstructorImpl(SkFltkLed, SkFltkWidgetWrapper)
{
    state = false;
    pulsed = false;
    blinking = false;
    c_ON = SkColor(255, 0, 0);
    c_OFF = SkColor(0, 0, 0);
    timeOut_OFF = 150;

    SlotSet(on);
    SlotSet(off);
    SlotSet(pulse);
    SlotSet(blink);
    SlotSet(toggle);
    SlotSet(toggleBlinking);
    SlotSet(fastTick);
    SlotSet(slowTick);
}

void SkFltkLed::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Box(region.x, region.y, region.w, region.h));
    setBackGroundColor(c_OFF);
    setBoxType(FL_DOWN_BOX);
    setAlignment(FL_ALIGN_CENTER);
    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(eventLoop()->slowZone_SIG, pulse, this, slowTick, SkQueued);
}

bool SkFltkLed::setParameter(SkString &k, SkVariant &v)
{
    if (k == "onColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setColor_ON(c);
    }

    else if (k == "offColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setColor_OFF(c);
    }

    else if (k == "timeoutMS")
    {
        int timeout;

        if (!grabInteger(k.c_str(), v, timeout))
            return false;

        setTimeout(timeout);
    }
    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkLed::setColor_ON(SkColor on)
{
    c_ON = on;
}

void SkFltkLed::setColor_OFF(SkColor off)
{
    c_OFF = off;
}

void SkFltkLed::setTimeout(int timeOutMS)
{
    timeOut_OFF = timeOutMS;
}

void SkFltkLed::switchState(bool state)
{
    if (state)
        on();

    else
        off();
}

SlotImpl(SkFltkLed, on)
{
    SilentSlotArgsWarning();
    setBackGroundColor(c_ON);
    state = true;
    redraw();
}

SlotImpl(SkFltkLed, off)
{
    SilentSlotArgsWarning();
    setBackGroundColor(c_OFF);
    state = false;
    blinking = false;
    redraw();
}

SlotImpl(SkFltkLed, pulse)
{
    SilentSlotArgsWarning();
    pulsed = true;
    on();
    pulsingChrono.start();
}

SlotImpl(SkFltkLed, blink)
{
    SilentSlotArgsWarning();
    blinking = true;
    pulse();
}

SlotImpl(SkFltkLed, toggle)
{
    SilentSlotArgsWarning();

    state = !state;

    if (state)
    {
        on();
        return;
    }

    off();
}

SlotImpl(SkFltkLed, toggleBlinking)
{
    SilentSlotArgsWarning();
    blinking = !blinking;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFltkLed, fastTick)
{
    SilentSlotArgsWarning();

    if (pulsed)
    {
        if (pulsingChrono.stop() > timeOut_OFF)
        {
            pulsingChrono.start();
            pulsed = false;
            setBackGroundColor(c_OFF);
            state = false;
            redraw();
        }
    }

    else
    {
        if (blinking)
        {
            if (pulsingChrono.stop() > timeOut_OFF)
            {
                pulsingChrono.start();
                pulse();
            }
        }
    }
}

SlotImpl(SkFltkLed, slowTick)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
