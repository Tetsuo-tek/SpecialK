#ifndef SKFLTKWINDOWS_H
#define SKFLTKWINDOWS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkgroups.h"
#include "skfltklayout.h"
#include "FL/Fl_Window.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkWindow extends SkAbstractFltkGroup
{
    SkFltkLayout *winLayout;
    SkFltkLayout *footerLayout;

    public:
        Constructor(SkFltkWindow, SkAbstractFltkGroup);

        void setLayout(SkFltkLayout *layout, bool resizable=true);
        void setFooter(SkFltkLayout *footer);

        void setTitle(CStr *text, bool asCopy=false);

        void setDefaultCursor(Fl_Cursor cursor);
        void setCursor(Fl_Cursor cursor);

        void setSizeRange(SkSize min, SkSize max, int wSteps, int hSteps);

        void enableWmDecorations(bool value);

        void enableFullScreen();
        void disableFullScreen();
        void disableFullScreen(SkRegion toRegion);

        void iconize();

        void show()                                             override;

        void close();

        SkFltkLayout *layout();
        SkFltkLayout *footer();

        bool isEnabledFullscreen();
        bool isModal();

        bool hasWmDecorations();
        SkSize getWmDecorationsSize();

        SkPoint absPosition();

        bool isShown();

        CStr *title();

        Signal(open);
        Signal(closed);

        Slot(toggleFullScreen);

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
        void notifyOpen();
        void notifyClosed();

        friend void w_CB(Fl_Window *, void *);
        static void w_CB(Fl_Window *, void *data);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Double_Window.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkDoubleWindow extends SkFltkWindow
{
    public:
        Constructor(SkFltkDoubleWindow, SkFltkWindow);

        void show()         override;

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return SkFltkWindow::setParameter(k, v);}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkModal extends SkFltkWindow
{
    public:
        Constructor(SkFltkModal, SkFltkWindow);

        void show()         override;

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return SkFltkWindow::setParameter(k, v);}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKWINDOWS_H
