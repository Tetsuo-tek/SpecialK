#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltklcddisplay.h"
#include "FL/fl_draw.H"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


/*
        0
     --------
    |        |
  5 |        | 1
    |   6    |
     --------
    |        |
  4 |        | 2
    |        |
     --------
        3
*/

SkFltkLcdDisplayWidget::SkFltkLcdDisplayWidget(int x, int y, int w, int h) : Fl_Box(x, y, w, h)
{
    owner = nullptr;
    bgColor = FL_BLACK;
    onColor = FL_YELLOW;
    offColor = fl_rgb_color(50, 50, 50);
    v = 0;
    margin = 6;
    fixedCiphersCount = 0;
    fixedSurplusCiphersLedOn = true;
    floatPrecisionCount = 1;
}

SkFltkLcdDisplayWidget::~SkFltkLcdDisplayWidget()
{
}

int SkFltkLcdDisplayWidget::handle(int e)
{
    return Fl_Box::handle(e);
}

static bool enabled[10][7] = {
    {1, 1, 1, 1, 1, 1, 0},  //0
    {0, 1, 1, 0, 0, 0, 0},  //1
    {1, 1, 0, 1, 1, 0, 1},  //2
    {1, 1, 1, 1, 0, 0, 1},  //3
    {0, 1, 1, 0, 0, 1, 1},  //4
    {1, 0, 1, 1, 0, 1, 1},  //5
    {1, 0, 1, 1, 1, 1, 1},  //6
    {1, 1, 1, 0, 0, 0, 0},  //7
    {1, 1, 1, 1, 1, 1, 1},  //8
    {1, 1, 1, 1, 0, 1, 1}   //9
};


void SkFltkLcdDisplayWidget::draw()
{
    Fl_Box::draw();
    SkRegion drw = SkRegion(x()+margin, y()+margin, w()-(margin*2), h()-(margin*2));

    Fl_Color on = onColor;

    if (!owner->isEnabled())
        on = offColor;

    SkFltkLcdDisplayWidget::drawDisplay(owner, v, drw, bgColor, on, offColor, fixedCiphersCount, fixedSurplusCiphersLedOn, floatPrecisionCount);
}

void SkFltkLcdDisplayWidget::drawDisplay(SkFltkWidgetWrapper *owner,
                                         SkVariant &v,
                                         SkRegion &drw,
                                         Fl_Color bgColor,
                                         Fl_Color c_ON,
                                         Fl_Color c_OFF,
                                         int fixedCiphersCount,
                                         bool fixedSurplusCiphersLedOn,
                                         int floatPrecisionCount)
{
    fl_push_clip(drw.x, drw.y, drw.w, drw.h);
    fl_rectf(drw.x, drw.y, drw.w, drw.h, bgColor);
    fl_pop_clip();

    int realHeight = drw.h;

    SkString str;

    if (v.isReal() && floatPrecisionCount)
    {
        float val = v.toFloat();
        str.concat(val, floatPrecisionCount);
    }

    else
        str = v.toString();

    bool hasPoint = str.contains(".") || str.contains(",");
    int totalCiphers = str.size() - hasPoint;
    int surplusCiphers = 0;

    if (fixedCiphersCount > totalCiphers)
    {
        surplusCiphers = fixedCiphersCount - totalCiphers;
        SkString s;
        s.fill('0', surplusCiphers);
        str.prepend(s.c_str());
        totalCiphers += surplusCiphers;
    }

    float segHLen = realHeight * 0.6f;
    float segVLen  = 0.f;
    float o1 = realHeight / 10.f;
    float o2 = 0.f;

    float width = (totalCiphers * segHLen) + ((totalCiphers - 1.f) * o1);

    if (hasPoint)
        width += (2.f*o1);//o1 + o1

    if (width > drw.w)
    {
        if (hasPoint)
            drw.h = (drw.w * (10.f / ((7.f * totalCiphers) + 1)));

        else
            drw.h = (drw.w * (10.f / ((7.f * totalCiphers) - 1)));

        drw.y += (realHeight - drw.h) / 2.f;

        segHLen = drw.h * 0.6f;
        o1 = drw.h/10.f;

        drw.w = ceil(totalCiphers * segHLen) + ((totalCiphers-1.f) * o1);

        if (hasPoint)
            drw.w += (2.f * o1);//o1 + o1
    }

    else if (width < drw.w)
    {
        drw.x += ((drw.w - width) / 2.f);
        drw.w = width;
    }

    o2 = o1/2.f;
    segVLen = drw.h/2.f;

    for(ULong i=0; i<str.size(); i++)
    {
        char c = str.at(i);

        if (/*c != '-' && */c != ',' && c != '.')
        {
            SkString charStr(c);
            int cipher = charStr.toInt();

            if (fixedCiphersCount && !fixedSurplusCiphersLedOn)
            {
                if (surplusCiphers < 1)
                {
                    SkFltkLcdDisplayWidget::drawSegments(drw, c_ON, c_OFF, cipher, segHLen, segVLen, o1, o2);
                }
                else
                    SkFltkLcdDisplayWidget::drawSegments(drw, c_OFF, c_OFF, cipher, segHLen, segVLen, o1, o2);

                surplusCiphers--;
            }

            else
                SkFltkLcdDisplayWidget::drawSegments(drw, c_ON, c_OFF, cipher, segHLen, segVLen, o1, o2);

            drw.x += (segHLen + o1);
        }

        else
        {
            //MUST ADD SIGNED SYMBOLs
            if (c != ',' || c != '.')
            {
                fl_rectf(drw.x+2, drw.y+(segVLen*2)-o1, o1, o1, c_ON);
                drw.x += (o1 * 2);
            }
        }
    }
}

void SkFltkLcdDisplayWidget::drawSegments(SkRegion &drw,
                                          Fl_Color c_ON,
                                          Fl_Color c_OFF,
                                          int cipher,
                                          float segHLen,
                                          float segVLen,
                                          float o1,
                                          float o2)
{
    //Fl_Color c_OFF = fl_rgb_color(40,40,40);

    if (enabled[cipher][0])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x+2,              drw.y);

        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x+segHLen-4,        p.y);
        fl_vertex(p.x+segHLen-4-o1,     p.y+o1);
        fl_vertex(p.x+o1,               p.y+o1);
    }
    fl_end_polygon();

    if (enabled[cipher][1])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x+segHLen,        drw.y);

        fl_vertex(p.x-o1,               p.y+o1);
        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x,                  p.y+segVLen);
        fl_vertex(p.x-o1,               p.y+segVLen-o2);
    }
    fl_end_polygon();

    if (enabled[cipher][2])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x+segHLen,        drw.y+segVLen+1);

        fl_vertex(p.x-o1,               p.y+o2);
        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x,                  p.y+segVLen-1);
        fl_vertex(p.x-o1,               p.y+segVLen-o1-1);
    }
    fl_end_polygon();

    if (enabled[cipher][3])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x+2,              drw.y+drw.h);

        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x+segHLen-4,        p.y);
        fl_vertex(p.x+segHLen-4-o1,     p.y-o1);
        fl_vertex(p.x+o1,               p.y-o1);
    }
    fl_end_polygon();

    if (enabled[cipher][4])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x,                drw.y+segVLen+1);

        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x+o1,               p.y+o2);
        fl_vertex(p.x+o1,               p.y+segVLen-o1-1);
        fl_vertex(p.x,                  p.y+segVLen-1);
    }
    fl_end_polygon();

    if (enabled[cipher][5])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x,                drw.y);

        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x+o1,               p.y+o1);
        fl_vertex(p.x+o1,               p.y+segVLen-o2);
        fl_vertex(p.x,                  p.y+segVLen);
    }
    fl_end_polygon();

    if (enabled[cipher][6])
        fl_color(c_ON);
    else
        fl_color(c_OFF);

    fl_begin_polygon();
    {
        SkPoint p(drw.x+2,              drw.y+segVLen);

        fl_vertex(p.x,                  p.y);
        fl_vertex(p.x+o1,               p.y+o2);
        fl_vertex(p.x+segHLen-o1-4,     p.y+o2);
        fl_vertex(p.x+segHLen-4,        p.y);
        fl_vertex(p.x+segHLen-4-o1,     p.y-o2);
        fl_vertex(p.x+o1,               p.y-o2);
    }
    fl_end_polygon();
}

void SkFltkLcdDisplayWidget::clear()
{
    v.nullify();
    redraw();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define lcdDisplay  DynCast(SkFltkLcdDisplayWidget, widget)

ConstructorImpl(SkFltkLcdDisplay, SkFltkWidgetWrapper)
{}

void SkFltkLcdDisplay::onWidgetSetup(SkRegion &region)
{
    setWidget(new SkFltkLcdDisplayWidget(region.x, region.y, region.w, region.h));
    lcdDisplay->owner = this;
}

bool SkFltkLcdDisplay::setParameter(SkString &k, SkVariant &v)
{
    if (k == "color")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setBackGroundColor(c);
    }

    else if (k == "segmentsOnColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setSegmentsOnColor(c);
    }

    else if (k == "segmentsOffColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setSegmentsOffColor(c);
    }

    else if (k == "fixed")
    {
        int count;

        if (!grabInteger(k.c_str(), v, count))
            return false;

        setFixed(count);
    }

    else if (k == "surplusZerosLedOn")
    {
        bool val;

        if (!grabBool(k.c_str(), v, val))
            return false;

        setSurplusZerosLedOn(val);
    }

    else if (k == "precision")
    {
        int precision;

        if (!grabInteger(k.c_str(), v, precision))
            return false;

        setPrecision(precision);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkLcdDisplay::setBackGroundColor(SkColor bg)
{
    lcdDisplay->bgColor = bg.toFlColor();
}

void SkFltkLcdDisplay::setSegmentsOnColor(SkColor c)
{
    lcdDisplay->onColor = c.toFlColor();
}

void SkFltkLcdDisplay::setSegmentsOffColor(SkColor c)
{
    lcdDisplay->offColor = c.toFlColor();
}

void SkFltkLcdDisplay::setFixed(int count)
{
    lcdDisplay->fixedCiphersCount = count;
}

void SkFltkLcdDisplay::setSurplusZerosLedOn(bool val)
{
    lcdDisplay->fixedSurplusCiphersLedOn = val;
}

void SkFltkLcdDisplay::setPrecision(int count)
{
    lcdDisplay->floatPrecisionCount = count;
}

void SkFltkLcdDisplay::setValue(const SkVariant &val)
{
    lcdDisplay->v = val;

    if (!lcdDisplay->v.isNumber())
    {
        ObjectError("Only number, integer or float, are accepted!");
        lcdDisplay->v.nullify();
    }
}

void SkFltkLcdDisplay::clear()
{
    lcdDisplay->clear();
}

SkVariant &SkFltkLcdDisplay::value()
{
    return lcdDisplay->v;
}

#endif
