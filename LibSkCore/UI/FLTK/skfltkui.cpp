#if defined(ENABLE_FLTK)

#include "skfltkui.h"
#include "Core/App/skapp.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Time/skdatetime.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static SkTreeMap<SkString, Fl_Boxtype>              boxTypes;
static SkTreeMap<SkString, Fl_Align>                alignTypes;
static SkTreeMap<SkString, Fl_Font>                 fonts;
static SkTreeMap<SkString, Fl_Cursor>               cursors;

#include <FL/Fl_Scroll.H>
static SkTreeMap<SkString, int>                     scrollOrientations;

static SkTreeMap<SkString, SkFltkHAlignPolicy>      hAlignPolicies;
static SkTreeMap<SkString, SkFltkVAlignPolicy>      vAlignPolicies;
static SkTreeMap<SkString, SkFltkResizePolicy>      resizePolicies;

StaticAutoLaunch(SkStaticWidgetInit, protoInit)
{
    boxTypes["NO_BOX"] = FL_NO_BOX;
    boxTypes["FLAT_BOX"] = FL_FLAT_BOX;
    boxTypes["UP_BOX"] = FL_UP_BOX;
    boxTypes["DOWN_BOX"] = FL_DOWN_BOX;
    boxTypes["UP_FRAME"] = FL_UP_FRAME;
    boxTypes["DOWN_FRAME"] = FL_DOWN_FRAME;
    boxTypes["THIN_UP_BOX"] = FL_THIN_UP_BOX;
    boxTypes["THIN_DOWN_BOX"] = FL_THIN_DOWN_BOX;
    boxTypes["THIN_UP_FRAME"] = FL_THIN_UP_FRAME;
    boxTypes["THIN_DOWN_FRAME"] = FL_THIN_DOWN_FRAME;
    boxTypes["ENGRAVED_BOX"] = FL_ENGRAVED_BOX;
    boxTypes["EMBOSSED_BOX"] = FL_EMBOSSED_BOX;
    boxTypes["ENGRAVED_FRAME"] = FL_ENGRAVED_FRAME;
    boxTypes["EMBOSSED_FRAME"] = FL_EMBOSSED_FRAME;
    boxTypes["BORDER_BOX"] = FL_BORDER_BOX;
    boxTypes["SHADOW_BOX"] = _FL_SHADOW_BOX;
    boxTypes["BORDER_FRAME"] = FL_BORDER_FRAME;
    boxTypes["SHADOW_FRAME"] = _FL_SHADOW_FRAME;
    boxTypes["ROUNDED_BOX"] = _FL_ROUNDED_BOX;
    boxTypes["RSHADOW_BOX"] = _FL_RSHADOW_BOX;
    boxTypes["ROUNDED_FRAME"] = _FL_ROUNDED_FRAME;
    boxTypes["RFLAT_BOX"] = _FL_RFLAT_BOX;
    boxTypes["ROUND_UP_BOX"] = _FL_ROUND_UP_BOX;
    boxTypes["ROUND_DOWN_BOX"] = _FL_ROUND_DOWN_BOX;
    boxTypes["DIAMOND_UP_BOX"] = _FL_DIAMOND_UP_BOX;
    boxTypes["DIAMOND_DOWN_BOX"] = _FL_DIAMOND_DOWN_BOX;
    boxTypes["OVAL_BOX"] = _FL_OVAL_BOX;
    boxTypes["OSHADOW_BOX"] = _FL_OSHADOW_BOX;
    boxTypes["OVAL_FRAME"] = _FL_OVAL_FRAME;
    boxTypes["OFLAT_BOX"] = _FL_OFLAT_BOX;
    boxTypes["PLASTIC_UP_BOX"] = _FL_PLASTIC_UP_BOX;                //< plastic version of FL_UP_BOX
    boxTypes["PLASTIC_DOWN_BOX"] = _FL_PLASTIC_DOWN_BOX;            //< plastic version of FL_DOWN_BOX
    boxTypes["PLASTIC_UP_FRAME"] = _FL_PLASTIC_UP_FRAME;            //< plastic version of FL_UP_FRAME
    boxTypes["PLASTIC_DOWN_FRAME"] = _FL_PLASTIC_DOWN_FRAME;        //< plastic version of FL_DOWN_FRAME
    boxTypes["PLASTIC_THIN_UP_BOX"] = _FL_PLASTIC_THIN_UP_BOX;      //< plastic version of FL_THIN_UP_BOX
    boxTypes["PLASTIC_THIN_DOWN_BOX"] = _FL_PLASTIC_THIN_DOWN_BOX;  //< plastic version of FL_THIN_DOWN_BOX
    boxTypes["PLASTIC_ROUND_UP_BOX"] = _FL_PLASTIC_ROUND_UP_BOX;    //< plastic version of FL_ROUND_UP_BOX
    boxTypes["PLASTIC_ROUND_DOWN_BOX"] = _FL_PLASTIC_ROUND_DOWN_BOX;//< plastic version of FL_ROUND_DOWN_BOX
    boxTypes["GTK_UP_BOX"] = _FL_GTK_UP_BOX;                        //< gtk+ version of FL_UP_BOX
    boxTypes["GTK_DOWN_BOX"] = _FL_GTK_DOWN_BOX;                    //< gtk+ version of FL_DOWN_BOX
    boxTypes["GTK_UP_FRAME"] = _FL_GTK_UP_FRAME;                    //< gtk+ version of FL_UP_FRAME
    boxTypes["GTK_DOWN_FRAME"] = _FL_GTK_DOWN_FRAME;                //< gtk+ version of FL_DOWN_FRAME
    boxTypes["GTK_THIN_UP_BOX"] = _FL_GTK_THIN_UP_BOX;              //< gtk+ version of FL_THIN_UP_BOX
    boxTypes["GTK_THIN_DOWN_BOX"] = _FL_GTK_THIN_DOWN_BOX;          //< gtk+ version of FL_THIN_DOWN_BOX
    boxTypes["GTK_THIN_UP_FRAME"] = _FL_GTK_THIN_UP_FRAME;          //< gtk+ version of FL_THIN_UP_FRAME
    boxTypes["GTK_THIN_DOWN_FRAME"] = _FL_GTK_THIN_DOWN_FRAME;      //< gtk+ version of FL_THIN_DOWN_FRAME
    boxTypes["GTK_ROUND_UP_BOX"] = _FL_GTK_ROUND_UP_BOX;            //< gtk+ version of FL_ROUND_UP_BOX
    boxTypes["GTK_ROUND_DOWN_BOX"] = _FL_GTK_ROUND_DOWN_BOX;        //< gtk+ version of FL_ROUND_DOWN_BOX
    boxTypes["GLEAM_UP_BOX"] = _FL_GLEAM_UP_BOX;                    //< gleam version of FL_UP_BOX
    boxTypes["GLEAM_DOWN_BOX"] = _FL_GLEAM_DOWN_BOX;                //< gleam version of FL_DOWN_BOX
    boxTypes["GLEAM_UP_FRAME"] = _FL_GLEAM_UP_FRAME;                //< gleam version of FL_UP_FRAME
    boxTypes["GLEAM_DOWN_FRAME"] = _FL_GLEAM_DOWN_FRAME;            //< gleam version of FL_DOWN_FRAME
    boxTypes["GLEAM_THIN_UP_BOX"] = _FL_GLEAM_THIN_UP_BOX;          //< gleam version of FL_THIN_UP_BOX
    boxTypes["GLEAM_THIN_DOWN_BOX"] = _FL_GLEAM_THIN_DOWN_BOX;      //< gleam version of FL_THIN_DOWN_BOX
    boxTypes["GLEAM_ROUND_UP_BOX"] = _FL_GLEAM_ROUND_UP_BOX;        //< gleam version of FL_ROUND_UP_BOX
    boxTypes["GLEAM_ROUND_DOWN_BOX"] = _FL_GLEAM_ROUND_DOWN_BOX;    //< gleam version of FL_ROUND_DOWN_BOX
    boxTypes["FREE_BOXTYPE"] = FL_FREE_BOXTYPE;                     //< the first free box type for creation of new box types

    alignTypes["ALIGN_CENTER"] = FL_ALIGN_CENTER;
    alignTypes["ALIGN_TOP"] = FL_ALIGN_TOP;
    alignTypes["ALIGN_BOTTOM"] = FL_ALIGN_BOTTOM;
    alignTypes["ALIGN_LEFT"] = FL_ALIGN_LEFT;
    alignTypes["ALIGN_RIGHT"] = FL_ALIGN_RIGHT;
    alignTypes["ALIGN_INSIDE"] = FL_ALIGN_INSIDE;
    alignTypes["ALIGN_TEXT_OVER_IMAGE"] = FL_ALIGN_TEXT_OVER_IMAGE;
    alignTypes["ALIGN_IMAGE_OVER_TEXT"] = FL_ALIGN_IMAGE_OVER_TEXT;
    alignTypes["ALIGN_CLIP"] = FL_ALIGN_CLIP;
    alignTypes["ALIGN_WRAP"] = FL_ALIGN_WRAP;
    alignTypes["ALIGN_IMAGE_NEXT_TO_TEXT"] = FL_ALIGN_IMAGE_NEXT_TO_TEXT;
    alignTypes["ALIGN_TEXT_NEXT_TO_IMAGE"] = FL_ALIGN_TEXT_NEXT_TO_IMAGE;
    alignTypes["ALIGN_IMAGE_BACKDROP"] = FL_ALIGN_IMAGE_BACKDROP;
    alignTypes["ALIGN_TOP_LEFT"] = FL_ALIGN_TOP_LEFT;
    alignTypes["ALIGN_TOP_RIGHT"] = FL_ALIGN_TOP_RIGHT;
    alignTypes["ALIGN_BOTTOM_LEFT"] = FL_ALIGN_BOTTOM_LEFT;
    alignTypes["ALIGN_BOTTOM_RIGHT"] = FL_ALIGN_BOTTOM_RIGHT;
    alignTypes["ALIGN_LEFT_TOP"] = FL_ALIGN_LEFT_TOP;
    alignTypes["ALIGN_RIGHT_TOP"] = FL_ALIGN_RIGHT_TOP;
    alignTypes["ALIGN_LEFT_BOTTOM"] = FL_ALIGN_LEFT_BOTTOM;
    alignTypes["ALIGN_RIGHT_BOTTOM"] = FL_ALIGN_RIGHT_BOTTOM;
    alignTypes["ALIGN_NOWRAP"] = FL_ALIGN_NOWRAP;
    alignTypes["ALIGN_POSITION_MASK"] = FL_ALIGN_POSITION_MASK;
    alignTypes["ALIGN_IMAGE_MASK"] =  FL_ALIGN_IMAGE_MASK;

    fonts["HELVETICA"] = FL_HELVETICA;
    fonts["HELVETICA_BOLD"] = FL_HELVETICA_BOLD;
    fonts["HELVETICA_ITALIC"] = FL_HELVETICA_ITALIC;
    fonts["HELVETICA_BOLD_ITALIC"] = FL_HELVETICA_BOLD_ITALIC;
    fonts["COURIER"] = FL_COURIER;
    fonts["COURIER_BOLD"] = FL_COURIER_BOLD;
    fonts["COURIER_ITALIC"] = FL_COURIER_ITALIC;
    fonts["COURIER_BOLD_ITALIC"] = FL_COURIER_BOLD_ITALIC;
    fonts["TIMES"] = FL_TIMES;
    fonts["TIMES_BOLD"] = FL_TIMES_BOLD;
    fonts["TIMES_ITALIC"] = FL_TIMES_ITALIC;
    fonts["TIMES_BOLD_ITALIC"] = FL_TIMES_BOLD_ITALIC;
    fonts["SYMBOL"] = FL_SYMBOL;
    fonts["SCREEN"] = FL_SCREEN;
    fonts["SCREEN_BOLD"] = FL_SCREEN_BOLD;
    fonts["ZAPF_DINGBATS"] = FL_ZAPF_DINGBATS;
    fonts["FREE_FONT"] = FL_FREE_FONT;
    fonts["BOLD"] = FL_BOLD;
    fonts["ITALIC"] = FL_ITALIC;
    fonts["BOLD_ITALIC"] = FL_BOLD_ITALIC;

    cursors["DEFAULT"] = FL_CURSOR_DEFAULT;
    cursors["ARROW"] = FL_CURSOR_ARROW;
    cursors["CROSS"] = FL_CURSOR_CROSS;
    cursors["WAIT"] = FL_CURSOR_WAIT;
    cursors["INSERT"] = FL_CURSOR_INSERT;
    cursors["HAND"] = FL_CURSOR_HAND;
    cursors["HELP"] = FL_CURSOR_HELP;
    cursors["MOVE"] = FL_CURSOR_MOVE;
    cursors["NS"] = FL_CURSOR_NS;
    cursors["WE"] = FL_CURSOR_WE;
    cursors["NWSE"] = FL_CURSOR_NWSE;
    cursors["NESW"] = FL_CURSOR_NESW;
    cursors["N"] = FL_CURSOR_N;
    cursors["NE"] = FL_CURSOR_NE;
    cursors["E"] = FL_CURSOR_E;
    cursors["SE"] = FL_CURSOR_SE;
    cursors["S"] = FL_CURSOR_S;
    cursors["SW"] = FL_CURSOR_SW;
    cursors["W"] = FL_CURSOR_W;
    cursors["NW"] = FL_CURSOR_NW;
    cursors["NONE"] = FL_CURSOR_NONE;

    scrollOrientations["HORIZONTAL"] = Fl_Scroll::HORIZONTAL;
    scrollOrientations["VERTICAL"] = Fl_Scroll::VERTICAL;
    scrollOrientations["BOTH"] = Fl_Scroll::BOTH;
    scrollOrientations["ALWAYS_ON"] = Fl_Scroll::ALWAYS_ON;
    scrollOrientations["HORIZONTAL_ALWAYS"] = Fl_Scroll::HORIZONTAL_ALWAYS;
    scrollOrientations["VERTICAL_ALWAYS"] = Fl_Scroll::VERTICAL_ALWAYS;
    scrollOrientations["BOTH_ALWAYS"] = Fl_Scroll::BOTH_ALWAYS;

    hAlignPolicies["CENTER"] = SkFltkHAlignPolicy::HCenterAlign;
    hAlignPolicies["LEFT"] = SkFltkHAlignPolicy::LeftAlign;
    hAlignPolicies["RIGHT"] = SkFltkHAlignPolicy::RightAlign;

    vAlignPolicies["CENTER"] = SkFltkVAlignPolicy::VCenterAlign;
    vAlignPolicies["TOP"] = SkFltkVAlignPolicy::TopAlign;
    vAlignPolicies["BOTTOM"] = SkFltkVAlignPolicy::BottomAlign;

    resizePolicies["RESIZABLE"] = SkFltkResizePolicy::Resizable;
    resizePolicies["FIXED_HEIGHT"] = SkFltkResizePolicy::FixedHeight;
    resizePolicies["FIXED_WIDTH"] = SkFltkResizePolicy::FixedWidth;
    resizePolicies["FIXED"] = SkFltkResizePolicy::Fixed;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFltkUI, SkObject)
{
    static int instances = 0;
    instances++;
    AssertKiller(instances > 1);

    ui = this;

    enabled = false;

    lastEvt = FL_NO_EVENT;

    setObjectName("FLTK");
    ui->setTheme("gtk+");

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkTreeMap<SkString, Fl_Boxtype>         &SkFltkUI::boxTypes()           {return ::boxTypes;}
SkTreeMap<SkString, Fl_Align>           &SkFltkUI::alignTypes()         {return ::alignTypes;}
SkTreeMap<SkString, Fl_Font>            &SkFltkUI::fonts()              {return ::fonts;}
SkTreeMap<SkString, Fl_Cursor>          &SkFltkUI::cursors()            {return ::cursors;}
SkTreeMap<SkString, int>                &SkFltkUI::scrollOrientations() {return ::scrollOrientations;}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkTreeMap<SkString, SkFltkHAlignPolicy> &SkFltkUI::hAlignPolicies()     {return ::hAlignPolicies;}
SkTreeMap<SkString, SkFltkVAlignPolicy> &SkFltkUI::vAlignPolicies()     {return ::vAlignPolicies;}
SkTreeMap<SkString, SkFltkResizePolicy> &SkFltkUI::resizePolicies()     {return ::resizePolicies;}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFltkUI::setTheme(CStr *name)
{
    Fl::scheme(name);
}

void SkFltkUI::start()
{
    if (enabled)
    {
        ObjectError("UI is ALREADY enabled");
        return;
    }

    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);

    enabled = true;

    ObjectWarning("Enabled");
}

void SkFltkUI::quit()
{
    if (!enabled)
    {
        ObjectError("UI is NOT enabled yet");
        return;
    }

    Detach(skApp->fastZone_SIG, pulse, this, fastTick);
    Detach(skApp->slowZone_SIG, pulse, this, slowTick);
    Detach(skApp->oneSecZone_SIG, pulse, this, oneSecTick);

    enabled = false;

    ObjectWarning("Disabled");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFltkWidgetWrapper *SkFltkUI::loadFromFile(CStr *uiFilePath)
{
    SkString json;
    SkVariant v;

    ObjectWarning("Loaging ui piece from JSON: " << uiFilePath);

    AssertKiller(!SkFsUtils::readTEXT(uiFilePath, json));
    AssertKiller(json.isEmpty());

    return loadFromJSON(json.c_str());
}

SkFltkWidgetWrapper *SkFltkUI::loadFromJSON(CStr *json)
{
    SkString jsonText(json);
    SkVariant v;

    AssertKiller(!v.fromJson(jsonText.c_str()));
    AssertKiller(v.variantType()!= T_MAP);

    SkTreeMap<SkString, SkVariant> uiTree;
    v.copyToMap(uiTree);

    SkFltkWidgetWrapper *wrapper = SkFltkUI::buildWrapper(uiTree);
    AssertKiller(wrapper == nullptr);
    wrapper->setParent(this);

    return wrapper;
}

SkFltkWidgetWrapper *SkFltkUI::buildWrapper(SkTreeMap<SkString, SkVariant> &props)
{
    if (!props.contains("type"))
    {
        StaticError("Each child element MUST have a widget-class-type");
        return nullptr;
    }

    SkString type("SkFltk");
    type.append(props.remove("type").value().toString());    

    if (!isSuperClassOf("SkFltkWidgetWrapper", type.c_str()))
    {
        StaticError("Each child element MUST be of a class-type derivating from SkFltkWidgetWrapper -> " << type);
        return nullptr;
    }

    SkFltkWidgetWrapper *wrapper = newObject<SkFltkWidgetWrapper *>(type.c_str());

    SkString name;

    if (props.contains("name"))
        name = props.remove("name").value().toString();

    else
    {
        SkString hashing = SkDateTime::currentDateTimeISOString();
        hashing.append(wrapper->uuid());

        name = type;
        name.append("_");
        name.append(stringHash(hashing));
    }

    wrapper->setup(name.c_str(), props);

    return wrapper;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFltkUI::registerWidget(CStr *name, SkFltkWidgetWrapper *w)
{
    if (wrappers.contains(name))
    {
        ObjectError_EXT(w, "Wrapper name is ALREADY registered: " << name);
        return false;
    }

    if (!w->isValid())
    {
        ObjectError_EXT(w, "Wrapper is NOT valid");
        return false;
    }

    w->setObjectName(name);
    wrappers[name] = w;
    widgetsToWrappers[w->internalWidget()] = w;

    ObjectMessage("Wrapper REGISTERED: " << name << " [" << w->typeName() << "]");
    return true;
}

bool SkFltkUI::unRegisterWidget(CStr *name)
{
    if (!wrappers.contains(name))
    {
        ObjectError("Wrapper is NOT registered yet: " << name);
        return false;
    }

    SkFltkWidgetWrapper *w = wrappers.remove(name).value();
    widgetsToWrappers.remove(w->internalWidget());

    ObjectMessage("Wrapper UN-REGISTERED: " << name << " [" << w->typeName() << "]");
    return true;
}

bool SkFltkUI::isRegistered(CStr *name)
{
    return wrappers.contains(name);
}

bool SkFltkUI::isRegistered(Fl_Widget *w)
{
    return widgetsToWrappers.contains(w);
}

SkFltkWidgetWrapper *SkFltkUI::get(CStr *name)
{
    if (!isRegistered(name))
    {
        ObjectError("Wrapper is NOT registered yet: " << name);
        return nullptr;
    }

    return wrappers[name];
}

SkFltkWidgetWrapper *SkFltkUI::get(Fl_Widget *w)
{
    if (!isRegistered(w))
    {
        ObjectError("Fl_Widget is NOT wrapped yet");
        return nullptr;
    }

    return widgetsToWrappers[w];
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFltkUI, fastTick)
{
    SilentSlotArgsWarning();

    int ret = Fl::check();
    Fl::do_widget_deletion();
    Fl_Event tempFlEvt = static_cast<Fl_Event>(Fl::event());

    if (tempFlEvt != FL_NO_EVENT)
        ObjectDebug("Event fired: " <<  fl_eventnames[tempFlEvt] << "[" << tempFlEvt << "]; CHK: " << ret);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkFltkUI, slowTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(SkFltkUI, oneSecTick)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool  SkFltkUI::isActive()
{
    return enabled;
}

Fl_Event SkFltkUI::lastEvent()
{
    return lastEvt;
}

CStr *SkFltkUI::lastEventName()
{
    return fl_eventnames[lastEvt];
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
