#if defined(ENABLE_FLTK)

#include "skfltklayout.h"
#include "skfltkui.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkLayoutSpacer, SkFltkBox, Fl_Box, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define layout   DynCast(Fl_Group, widget)

AbstractConstructorImpl(SkFltkLayout, SkFltkWidgetWrapper)
{}

bool SkFltkLayout::setParameter(SkString &k, SkVariant &v)
{
    if (k == "children")
    {
        if (v.variantType() != T_LIST)
        {
            ObjectError("Children MUST be a T_LIST, instead it is: " << v.variantTypeName());
            return false;
        }

        SkVariantVector children;
        v.copyToList(children);

        for(ULong i=0; i<children.count(); i++)
        {
            if (!children[i].isMap())
            {
                ObjectError("Each child element MUST be a mT_MAP, instead it is: " << children[i].variantTypeName());
                return false;
            }

            SkTreeMap<SkString, SkVariant> props;
            children[i].copyToMap(props);

            //DOES NOTHING: USE AlignPolicies instead
            //bool resizable = false;

            if (props.contains("resizable"))
                /*resizable = */props.remove("resizable").value()/*.toBool()*/;

            SkFltkWidgetWrapper *wrapper = SkFltkUI::buildWrapper(props);

            if (!wrapper)
                return false;

            add(wrapper);
        }
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkFltkLayout::adaptWidgetToCell(Fl_Widget *widget, SkRegion &region)
{
    if (!widget->visible())
        return;

    SkFltkWidgetWrapper *wrapper = ui->get(widget);
    AssertKiller(wrapper==nullptr);

    SkFltkHAlignPolicy hAlignPol = HCenterAlign;
    SkFltkVAlignPolicy vAlignPol = VCenterAlign;
    SkFltkResizePolicy resizePol = Resizable;

    if (wrapper)
    {
        hAlignPol = wrapper->hAlignPolicy();
        vAlignPol = wrapper->vAlignPolicy();
        resizePol = wrapper->resizePolicy();
    }

    if (resizePol == Resizable)
        widget->resize(region.x, region.y, region.w, region.h);

    else
    {
        int oX = 0;
        int oY = 0;

        if (resizePol == Fixed || resizePol == FixedWidth)
        {
            if (hAlignPol == LeftAlign)
            {}

            else if (hAlignPol == RightAlign)
                oX = region.w - widget->w();

            else if (hAlignPol == HCenterAlign)
                oX = (region.w - widget->w()) / 2;

            if (oX < 0)
                oX = 0;
        }

        if (resizePol == Fixed || resizePol == FixedHeight)
        {
            if (vAlignPol == TopAlign)
            {}

            else if (vAlignPol == BottomAlign)
                oY = region.h - widget->h();

            else if (vAlignPol == VCenterAlign)
                oY = (region.h - widget->h()) / 2;

            if (oY < 0)
                oY = 0;
        }

        if (resizePol == Fixed)
            widget->position(region.x+oX, region.y+oY);

        else if (resizePol == FixedHeight)
        {
            //cout << "!!! " << (region.x+oX)<< " " <<(region.y+oY )<< " " << (region.w) << " " << (widget->h()) << " " << wrapper->objectName() << "\n";
            widget->resize(region.x+oX, region.y+oY, region.w, widget->h());
        }

        else if (resizePol == FixedWidth)
            widget->resize(region.x+oX, region.y+oY, widget->w(), region.h);
    }
}

ULong SkFltkLayout::childrenCount()
{
    return layout->children();
}

SkFltkWidgetWrapper *SkFltkLayout::child(int index)
{
    Fl_Widget *w = layout->child(index);

    if (!w)
        return nullptr;

    return ui->get(w);
}

void SkFltkLayout::clear()
{
    //MISTIQUE DANGEROUS
    //layout->clear();

    SkList<SkObject *> childrenWrappers = children();
    SkAbstractListIterator<SkObject *> *itr = childrenWrappers.iterator();

    while(itr->next())
    {
        SkFltkWidgetWrapper *child = DynCast(SkFltkWidgetWrapper, itr->item());

        if (!child)
            continue;

        child->setParent(nullptr);
        remove(child);
    }

    delete itr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFltkLayout::removeFirst()
{
    if (layout->children())
        remove(child(0));
}

void SkFltkLayout::removeAt(int index)
{
    if (index < layout->children())
        remove(child(index));
}

void SkFltkLayout::removeLast()
{
    ULong count = layout->children();
    if (count)
        remove(child(count-1));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
