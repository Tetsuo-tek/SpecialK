#ifndef SKFLTKGROUPS_H
#define SKFLTKGROUPS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Group.H"

class SkAbstractFltkGroup extends SkFltkWidgetWrapper
{
    public:
        void add(SkFltkWidgetWrapper *wrapper, bool asResizable=false);
        void del(SkFltkWidgetWrapper *wrapper);

        ULong childrenCount();
        SkFltkWidgetWrapper *child(int index);

        void clear();

        bool hasResizableWidget();
        SkFltkWidgetWrapper *resizableWidget();

    protected:
        AbstractConstructor(SkAbstractFltkGroup, SkFltkWidgetWrapper);

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Pack.H"

class SkFltkPack extends SkAbstractFltkGroup
{
    public:
        Constructor(SkFltkPack, SkAbstractFltkGroup);

        void setSpacing(int i);

        void setHorizontal();
        void setVertical();

        bool isHorizontal();
        bool isVertical();

        int spacing();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Scroll.H"

class SkFltkScrollArea extends SkAbstractFltkGroup
{
    public:
        Constructor(SkFltkScrollArea, SkAbstractFltkGroup);

        void setScrollOrientation(int mode);

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Tile.H"
StupidWrapper(SkFltkTile, SkAbstractFltkGroup, Fl_Tile);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKGROUPS_H
