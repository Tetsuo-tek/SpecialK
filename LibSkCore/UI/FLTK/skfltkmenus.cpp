#if defined(ENABLE_FLTK)

#include "skfltkmenus.h"
#include "skfltkui.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define menu   DynCast(Fl_Menu_, widget)

AbstractConstructorImpl(SkAbstractFltkMenu, SkFltkWidgetWrapper)
{
    SignalSet(picked);
}

bool SkAbstractFltkMenu::setParameter(SkString &k, SkVariant &v)
{
    if (k == "font")
    {
        Fl_Font f;

        if (!grabFont(k.c_str(), v, f))
            return false;

        setFont(f);
    }

    else if (k == "fontSize")
    {
        int val;

        if (!grabInteger(k.c_str(), v, val))
            return false;

        setFontSize(val);
    }

    else if (k == "fontColor")
    {
        SkColor c;

        if (!grabColor(k.c_str(), v, c))
            return false;

        setFontColor(c);
    }

    else if (k == "downBox")
    {
        Fl_Boxtype bt;

        if (!grabBoxType(k.c_str(), v, bt))
            return false;

        setDownBoxType(bt);
    }

    else if (k == "items")
    {
        SkStringList items;

        if (!grabStringList(k.c_str(), v, items))
            return false;

        add(items);
    }

    else
        return SkFltkWidgetWrapper::setParameter(k, v);

    return true;
}

void SkAbstractFltkMenu::setFont(Fl_Font font)
{
    menu->textfont(font);
}

void SkAbstractFltkMenu::setFontSize(int size)
{
    menu->textsize(size);
}

void SkAbstractFltkMenu::setFontColor(SkColor textColor)
{
    menu->textcolor(textColor.toFlColor());
}

void SkAbstractFltkMenu::setDownBoxType(Fl_Boxtype type)
{
    menu->down_box(type);
}

void SkAbstractFltkMenu::add(SkStringList &labels)
{
    for(size_t i=0; i<labels.count(); i++)
        add(labels.at(i).c_str());
}

int SkAbstractFltkMenu::add(CStr *label, bool enabled, SkFltkMenuItemType type, int shortcut)
{
    return insert(-1, label, enabled, type, shortcut);
}

int SkAbstractFltkMenu::insert(int index, CStr *label, bool enabled, SkFltkMenuItemType type, int shortcut)
{
    int flags = 0;

    if (!enabled)
        flags = FL_MENU_INACTIVE;

    if (type != SkFltkMenuItemType::NormalItem)
    {
        int tempFlag = 0;

        if (type == ToggleItem)
            tempFlag = FL_MENU_TOGGLE;

        else if (type == RadioItem)
            tempFlag = FL_MENU_RADIO;

        else
            return -1;

        if (flags)
            flags |= tempFlag;

        else
            flags = tempFlag;
    }

    if (index == -1)
        return menu->add(label, shortcut, nullptr, nullptr, flags);

    return menu->insert(index, label, shortcut, nullptr, nullptr, flags);
}

int SkAbstractFltkMenu::addDivider()
{
    return menu->add(nullptr, 0, nullptr, nullptr, FL_MENU_DIVIDER);
}

void SkAbstractFltkMenu::setFlags(int index, int flags)
{
    menu->mode(index, flags);
}

void SkAbstractFltkMenu::replace(int index, CStr *text)
{
    menu->replace(index, text);
}

void SkAbstractFltkMenu::remove(int index)
{
    menu->remove(index);
}

void SkAbstractFltkMenu::clear()
{
    menu->clear();
}

int SkAbstractFltkMenu::find(CStr *text)
{
    return menu->find_index(text);
}

int SkAbstractFltkMenu::currentIndex()
{
    return menu->value();
}

CStr *SkAbstractFltkMenu::currentText()
{
    return menu->text();
}

CStr *SkAbstractFltkMenu::text(int index)
{
    return menu->text(index);
}

Fl_Font SkAbstractFltkMenu::getFont()
{
    return menu->textfont();
}

Fl_Fontsize SkAbstractFltkMenu::getFontSize()
{
    return menu->textsize();
}

SkColor SkAbstractFltkMenu::getTextColor()
{
    return menu->textcolor();
}

Fl_Boxtype SkAbstractFltkMenu::getDownBoxType()
{
    return menu->down_box();
}

int SkAbstractFltkMenu::count()
{
    return menu->size();
}

void SkAbstractFltkMenu::w_CB(Fl_Menu_ *, void *data)
{
    SkAbstractFltkMenu * wWrapper = static_cast<SkAbstractFltkMenu *>(data);
    Fl_Event tempFlEvt = static_cast<Fl_Event>(Fl::event());
    ObjectMessage_EXT(wWrapper, "ItemSelected (EVT: " << fl_eventnames[tempFlEvt] << "[" << tempFlEvt << "])");

    //menuWrapper->pickedValue()->setVal(menuWrapper->getCurrentIndex());
    SkVariantVector p;
    p << wWrapper->currentIndex();
    p << wWrapper->currentText();

    wWrapper->picked(p);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define combo   DynCast(Fl_Choice, widget)

ConstructorImpl(SkFltkComboBox, SkAbstractFltkMenu)
{
    currentIndexHolder = -1;
}

void SkFltkComboBox::onWidgetSetup(SkRegion &region)
{
    setWidget(new Fl_Choice(region.x, region.y, region.w, region.h));
    setCallBack((Fl_Callback *) w_CB, this);
}

void SkFltkComboBox::setCurrentIndex(int index)
{
    combo->value(index);
}

int SkFltkComboBox::currentIndex()
{
    return combo->value();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkMenuBar, SkAbstractFltkMenu, Fl_Menu_Bar, SkAbstractFltkMenu::w_CB)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
