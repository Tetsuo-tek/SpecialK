#ifndef SKFLTKBARCHART_H
#define SKFLTKBARCHART_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkFltkMeterBar
{
    bool enabled;
    float value;
    int lastPeakHolderSegNum;
    bool alert;

};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Time/skelapsedtime.h>

class SkFltkBarChartWidget extends Fl_Widget
{
    public:
        Fl_Color bgColor;
        Fl_Color alertColor;
        Fl_Color peakHolderColor;
        Fl_Color segmentsOnColor;
        Fl_Color segmentsOffColor;

        int margin;

        bool alertEnabled;

        bool peakHolderEnabled;
        SkElapsedTime chronoReducingPeakHolder;
        double peakHolderReducingTime;

        bool gradientEnabled;

        float minValue;
        float maxValue;

        SkVector<SkFltkMeterBar *> bars;

        SkFltkBarChartWidget(int x, int y, int w, int h);
        ~SkFltkBarChartWidget();

        void clear();

    private:
        SkRegion lastRect;
        SkRegion drawingRect; //m on width will be considered from segmentDraw

        int ah;//alert height
        float bw;//bar width
        int bh;//bar height (segments)

        //Values and increment per segment
        float range;
        float valIncr;

        float gradientAverage;

        void calculateGeometries(int margin);
        void reducePeakHolder();

    protected:
        int handle(int e)                                       override;
        void draw()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkBarChart extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkBarChart, SkFltkWidgetWrapper);

        void setBounds(float min, float max);
        void setBackGroundColor(SkColor bg);
        void enableAlert(bool value);
        void enablePeakHolderLeds(bool value, double reducingTime=0.1);
        void setPeakHolderColor(SkColor pk);
        void setSegmentsColor_ON(SkColor on);
        void setSegmentsColor_OFF(SkColor off);
        void enableGradient(bool value);

        void addMeter(SkFltkMeterBar *meterBar);
        void delMeter(SkFltkMeterBar *meterBar);

        SkFltkMeterBar *getMeter(ULong index);

        float getMin();
        float getMax();

        void clear();

        ULong count();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

#endif

#endif // SKFLTKBARCHART_H
