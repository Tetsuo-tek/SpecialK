#ifndef SKFLTKUI_H
#define SKFLTKUI_H

#if defined(ENABLE_FLTK)

#include "skfltkwidgetwrapper.h"
#include "Core/Containers/sktreemap.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkUI extends SkObject
{
    public:
        Constructor(SkFltkUI, SkObject);

        //plastic, gtk+, gleam
        void setTheme(CStr *name);

        void start();
        void quit();

        SkFltkWidgetWrapper *loadFromFile(CStr *uiFilePath);
        SkFltkWidgetWrapper *loadFromJSON(CStr *json);

        bool isActive();
        Fl_Event lastEvent();
        CStr *lastEventName();

        SkFltkWidgetWrapper *get(CStr *name);
        SkFltkWidgetWrapper *get(Fl_Widget *w);

        bool isRegistered(CStr *name);
        bool isRegistered(Fl_Widget *w);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

        static SkTreeMap<SkString, Fl_Boxtype>              &boxTypes();
        static SkTreeMap<SkString, Fl_Align>                &alignTypes();
        static SkTreeMap<SkString, Fl_Font>                 &fonts();
        static SkTreeMap<SkString, Fl_Cursor>               &cursors();
        static SkTreeMap<SkString, int>                     &scrollOrientations();

        static SkTreeMap<SkString, SkFltkHAlignPolicy>      &hAlignPolicies();
        static SkTreeMap<SkString, SkFltkVAlignPolicy>      &vAlignPolicies();
        static SkTreeMap<SkString, SkFltkResizePolicy>      &resizePolicies();

        static SkFltkWidgetWrapper                          *buildWrapper(SkTreeMap<SkString, SkVariant> &props);

    private:
        bool enabled;
        Fl_Event lastEvt;

        SkTreeMap<SkString, SkFltkWidgetWrapper *> wrappers;
        SkTreeMap<Fl_Widget *, SkFltkWidgetWrapper *> widgetsToWrappers;

        friend class SkFltkWidgetWrapper;

        bool registerWidget(CStr *name, SkFltkWidgetWrapper *w);
        bool unRegisterWidget(CStr *name);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

extern SkFltkUI *ui;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKUI_H
