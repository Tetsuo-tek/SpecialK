#if defined(ENABLE_FLTK)

#include "skfltkviews.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkLabel, SkFltkLineInput, Fl_Output, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
StupidWrapperImpl(SkFltkTextView, SkFltkLabel, Fl_Multiline_Output, nullptr)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
