#ifndef SKFLTKMENUS_H
#define SKFLTKMENUS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Menu_.H"

enum SkFltkMenuItemType
{
    NormalItem,
    ToggleItem,
    RadioItem
};

class SkAbstractFltkMenu extends SkFltkWidgetWrapper
{
    public:
        void setFont(Fl_Font font);
        void setFontSize(int size);
        void setFontColor(SkColor textColor);

        void setDownBoxType(Fl_Boxtype type);

        void add(SkStringList &labels);
        int add(CStr *label, bool enabled=true, SkFltkMenuItemType type=SkFltkMenuItemType::NormalItem, int shortcut=0);
        int insert(int index, CStr *label, bool enabled=true, SkFltkMenuItemType type=SkFltkMenuItemType::NormalItem, int shortcut=0);
        int addDivider();

        void setFlags(int index, int flags);
        void replace(int index, CStr *text);

        void remove(int index);
        void clear();

        int find(CStr *text);

        virtual int currentIndex();
        CStr *currentText();

        CStr *text(int index);

        Fl_Font getFont();
        Fl_Fontsize getFontSize();
        SkColor getTextColor();

        Fl_Boxtype getDownBoxType();

        int count();

        Signal(picked);

    protected:
        AbstractConstructor(SkAbstractFltkMenu, SkFltkWidgetWrapper);

        //ONLY INSIDE ABSTRACT, IF HAS NOT THAT KEY, IT MUST GO DOWN ON SUPER
        bool setParameter(SkString &k, SkVariant &v);

        static void w_CB(Fl_Menu_ *, void *data);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Choice.H"

class SkFltkComboBox extends SkAbstractFltkMenu
{
        int currentIndexHolder;//it will be grabbed on setParameter and set on show

    public:
        Constructor(SkFltkComboBox, SkAbstractFltkMenu);

        void setCurrentIndex(int index);
        int currentIndex()                                      override;

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Menu_Bar.H"
StupidWrapper(SkFltkMenuBar, SkAbstractFltkMenu, Fl_Menu_Bar);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKMENUS_H
