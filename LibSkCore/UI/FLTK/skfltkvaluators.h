#ifndef SKFLTKVALUATORS_H
#define SKFLTKVALUATORS_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "FL/Fl_Valuator.H"

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkValuator extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkValuator, SkFltkWidgetWrapper);

        void setBounds(double min, double max);
        void setStep(double value);
        void setPrecision(int value);
        void setValue(double value);
        double increment(double incr, int count=1);
        double round(double value);
        double clamp(double value);

        double getMininum();
        double getMaximum();
        double getValue();
        double getStep();

        Signal(changed);

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <FL/Fl_Dial.H>

class SkFltkDial extends SkFltkValuator
{
    public:
        Constructor(SkFltkDial, SkFltkValuator);

        void setFirstAngle(short angle);
        void setSecondAngle(short angle);

        short getFirstAngle();
        short getSecondAngle();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);

        static void w_CB(Fl_Slider *, void *data);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <FL/Fl_Roller.H>

class SkFltkRoller extends SkFltkValuator
{
    public:
        Constructor(SkFltkRoller, SkFltkValuator);

        void setHorizontal();
        void setVertical();

        bool isHorizontal();
        bool isVertical();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*#include <FL/Fl_Slider.H>

class SkFltkSlider extends SkFltkValuator
{
    public:
        Constructor(SkFltkSlider, SkFltkValuator);

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v){return false;}
};*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Slider.H"
StupidWrapper(SkFltkSlider, SkFltkValuator, Fl_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Value_Slider.H"
StupidWrapper(SkFltkValueSlider, SkFltkSlider, Fl_Value_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Hor_Slider.H"
StupidWrapper(SkFltkHSlider, SkFltkSlider, Fl_Hor_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Nice_Slider.H"
StupidWrapper(SkFltkNiceSlider, SkFltkSlider, Fl_Nice_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Hor_Nice_Slider.H"
StupidWrapper(SkFltkHNiceSlider, SkFltkSlider, Fl_Hor_Nice_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Fill_Slider.H"
StupidWrapper(SkFltkFillSlider, SkFltkSlider, Fl_Fill_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#include "FL/Fl_Hor_Fill_Slider.H"
StupidWrapper(SkFltkHFillSlider, SkFltkSlider, Fl_Hor_Fill_Slider);
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKVALUATORS_H
