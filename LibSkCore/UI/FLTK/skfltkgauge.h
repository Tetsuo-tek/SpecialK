#ifndef SKFLTKGAUGE_H
#define SKFLTKGAUGE_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkGauge;

class SkFltkGaugeWidget extends Fl_Box
{
    public:
        SkFltkGauge *owner;

        Fl_Color fgColor1;
        Fl_Color fgColor2;
        Fl_Color bgColor;
        int margin;
        SkRegion drw;
        float minValue;
        float maxValue;
        SkVariant v;
        SkString udm;
        bool showUDM;
        bool showValue;

        SkFltkGaugeWidget(int x, int y, int w, int h);
        ~SkFltkGaugeWidget();

    protected:
        int handle(int)                             override;
        void draw()                                 override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkGauge extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkGauge, SkFltkWidgetWrapper);

        void setBounds(float min, float max);
        void setValue(const SkVariant &val);
        void setBackGroundColor(SkColor bg);
        void setUDM(CStr *udm);
        void showValue(bool val);
        void showUDM(bool val);
        SkVariant &value();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKGAUGE_H
