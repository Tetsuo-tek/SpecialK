#ifndef SKFLTKCURVECHART_H
#define SKFLTKCURVECHART_H

#if defined(ENABLE_FLTK)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skfltkwidgetwrapper.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkFltkCurveType
{
    PointCurve,
    PointSpikeCurve,
    LineCurve,
    LineSpikeCurve,
    StepCurve,
    StepSpikeCurve,
    SpikeCurve
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkFltkCurve
{
    bool enabled;
    Fl_Color color;
    SkQueue<double> samples;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkCurvesChartWidget extends Fl_Box
{
    public:
        Fl_Color bgColor;
        Fl_Color subSpikesColor;
        SkFltkCurveType curvesDrawType;
        SkVector<SkFltkCurve *> curves;

        double minValue;
        double maxValue;

        SkFltkCurvesChartWidget(int x, int y, int w, int h);
        ~SkFltkCurvesChartWidget();

        void clear();

    private:
        SkRegion drw;

        double range;
        double valIncr;

    protected:
        int handle(int e)                                       override;
        void draw()                                             override;

};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFltkCurvesChart extends SkFltkWidgetWrapper
{
    public:
        Constructor(SkFltkCurvesChart, SkFltkWidgetWrapper);

        void setBounds(double yMin, double yMax);
        void setBackGroundColor(SkColor bg);
        void setCurveType(SkFltkCurveType type);

        //ONCE CURVE IS ADDED IT WILL BE MANAGED FROM HERE (MEANING ALSO DESTROY)
        void addCurve(SkFltkCurve *curve);
        void delCurve(SkFltkCurve *curve);

        void clear();

        ULong count();
        SkFltkCurve *curve(ULong index);
        SkFltkCurveType curveType();

    protected:
        void onWidgetSetup(SkRegion &region)                    override;
        bool onWidgetParameterSetup(SkString &k, SkVariant &v)  override
        {return setParameter(k, v);}

        bool setParameter(SkString &k, SkVariant &v);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLTKCURVECHART_H
