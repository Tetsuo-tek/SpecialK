CONFIG += c++11
CONFIG -= qt

CONFIG += link_pkgconfig

INCLUDEPATH += $$PWD/../LibSkFlat
include($$PWD/../LibSkFlat/module-src.pri)

LIBS += -lpthread
LIBS += -lz
LIBS += -lcrypto

DEFINES += ENABLE_SKAPP

DEFINES += ENABLE_AES
LIBS += -lssl

DEFINES += ENABLE_XML

DEFINES += ENABLE_STATGRAB
LIBS += -lstatgrab

LIBS += -lhiredis

DEFINES += SPECIALK_APPLICATION

equals(TEMPLATE , lib) : {
    message("Template: LIBRARY")
}

else : {
    message("Template: APPLICATION")
    target.path = /usr/local/bin
    INSTALLS += target
}

message("OS: $$QMAKE_HOST.os")
message("CPUs: $$QMAKE_HOST.cpu_count")
message("HOSTNAME: $$QMAKE_HOST.name")
message("SYS VERS: $$QMAKE_HOST.version")
message("SYS VERS STRING: $$QMAKE_HOST.version_string")
message("ARCH: $$QMAKE_HOST.arch")
message("Config: $$CONFIG")
message("IncludePath: $$INCLUDEPATH")
message("Defines: $$DEFINES")
message("Libs: $$LIBS")
