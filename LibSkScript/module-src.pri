HEADERS += \
    $$PWD/Core/Scripting/SkAsm/skasmisa.h \
    $$PWD/Core/Scripting/SkAsm/skasm.h \
    $$PWD/Core/Scripting/SkAsm/skasmcodecontext.h \
    $$PWD/Core/Scripting/SkAsm/skasmcodeline.h \
    $$PWD/Core/Scripting/SkAsm/skasmheap.h \
    $$PWD/Core/Scripting/SkAsm/skasmidentifier.h \
    $$PWD/Core/Scripting/SkAsm/skasmprogram.h \
    $$PWD/Core/Scripting/SkAsm/skasmslot.h \
    $$PWD/Core/Scripting/SkAsm/skasmstack.h \
    $$PWD/Core/Scripting/SkCodeTemplate/sktemplatemanager.h \
    $$PWD/Core/Scripting/SkPlancton/skplancton.h \
    $$PWD/Core/Scripting/SkPlancton/skplanctonchunk.h \
    $$PWD/Core/Scripting/SkPlancton/skplanctonobject.h \
    $$PWD/Core/Scripting/SkPlancton/skplanctonparser.h \
    $$PWD/Core/Scripting/SkPlancton/skplanctonscanner.h \
    $$PWD/Core/Scripting/SkCodeTemplate/sktemplatemodeler.h \
    $$PWD/Core/Scripting/SkPlancton/skplanctonvm.h

SOURCES += \
    $$PWD/Core/Scripting/SkAsm/skasmisa.cpp \
    $$PWD/Core/Scripting/SkAsm/skasm.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmcodecontext.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmcodeline.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmheap.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmidentifier.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmprogram.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmslot.cpp \
    $$PWD/Core/Scripting/SkAsm/skasmstack.cpp \
    $$PWD/Core/Scripting/SkCodeTemplate/sktemplatemanager.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplancton.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplanctonchunk.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplanctonobject.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplanctonparser.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplanctonscanner.cpp \
    $$PWD/Core/Scripting/SkCodeTemplate/sktemplatemodeler.cpp \
    $$PWD/Core/Scripting/SkPlancton/skplanctonvm.cpp

DISTFILES +=
