#include "sktemplatemanager.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/Filesystem/skfileinfoslist.h"
#include "Core/Containers/skringbuffer.h"

SkTemplateManager::SkTemplateManager()
{}

SkTemplateManager::~SkTemplateManager()
{}

bool SkTemplateManager::init(CStr *rootPath)
{
    if (!SkFsUtils::exists(rootPath) || !SkFsUtils::isDir(rootPath))
    {
        FlatError("Templates RootPath is NOT valid: " << rootPath);
        return false;
    }

    root = SkFsUtils::adjustPathEndSeparator(rootPath);

    if (!SkFsUtils::exists(root.c_str()) || !SkFsUtils::isDir(root.c_str()) || !SkFsUtils::isReadable(root.c_str()))
    {
        FlatError("Templates RootDirectory is NOT valid: " << root);
        return false;
    }

    templatesFilePath = root;
    templatesFilePath.append("data.Variant");

    templatesHashFilePath = templatesFilePath;
    templatesHashFilePath.append(".md5");

    updateCheck();

    FlatMessage("Templates READY");
    return true;
}

bool SkTemplateManager::checkTemplatesVariant()
{
    if (!SkFsUtils::exists(templatesHashFilePath.c_str()))
    {
        FlatWarning("Current hash for templates VariantDatabase NOT found: " << templatesHashFilePath);
        return false;
    }

    SkString currentHash;

    if (!SkFsUtils::readTEXT(templatesHashFilePath.c_str(), currentHash))
    {
        FlatError("CANNOT grab current hash for templates VariantDatabase: " << templatesHashFilePath);
        return false;
    }

    SkFileInfosList *infoList = new SkFileInfosList;

    if (!SkFsUtils::ls(root.c_str(), infoList, true))
    {
        FlatError("CANNOT scan templates files from: " << root);
        infoList->destroyLater();
        return false;
    }

    SkString hash;

    if (infoList->open())
    {
        if (infoList->isEmpty())
        {
            infoList->close();
            infoList->destroyLater();
            return false;
        }

        SkPathInfo pathInfo;
        SkFsUtils::fillPathInfo(root.c_str(), pathInfo);

        SkDataBuffer b;

        while(infoList->next())
        {
            SkString relativePath = infoList->currentPath();
            relativePath.replace(pathInfo.absoluteParentPath.c_str(), "");
            relativePath = &relativePath.c_str()[1];

            if (relativePath == templatesFilePath || relativePath == templatesHashFilePath)
                continue;

            b.append(relativePath);

            if (!infoList->currentPathIsDir())
            {
                if (!SkFsUtils::readDATA(infoList->currentPath().c_str(), b))
                {
                    FlatError("CANNOT read template file: " << infoList->currentPath());
                    infoList->close();
                    infoList->destroyLater();
                    return false;
                }
            }
        }

        infoList->close();
        b.toStringHash(hash);
    }

    infoList->destroyLater();

    if (currentHash != hash)
    {
        FlatWarning("Templates VariantDatabase requires to UPDATE [" << currentHash << " -> " << hash << "]: " << templatesFilePath);
        return false;
    }

    return true;
}

bool SkTemplateManager::buildTemplatesVariant()
{
    SkPathInfo pathInfo;
    SkFsUtils::fillPathInfo(root.c_str(), pathInfo);

    FlatMessage("Building VariantDatabase: " << templatesFilePath);

    templates.clear();
    SkFileInfosList *infoList = new SkFileInfosList;

    SkFsUtils::ls(root.c_str(), infoList, true);

    if (infoList->open())
    {
        if (infoList->isEmpty())
        {
            FlatError("Templates directory is EMPTY: " << templatesFilePath);
            return false;
        }

        FlatMessage("Loading templates from directory to VariantDatabase: " << templatesFilePath);

        SkDataBuffer b;

        while(infoList->next())
        {
            SkString relativePath = infoList->currentPath();
            relativePath.replace(pathInfo.absoluteParentPath.c_str(), "");
            relativePath = &relativePath.c_str()[1];

            if (relativePath == templatesFilePath || relativePath == templatesHashFilePath)
                continue;

            b.append(relativePath);

            if (!infoList->currentPathIsDir())
            {
                SkString content;
                SkFsUtils::readTEXT(infoList->currentPath().c_str(), content);

                b.append(content);

                templates[relativePath] = content;

                //create md5 reminder
                FlatMessage("ADDED template to VariantDatabase: " << relativePath << " [" << content.size() << " B]");
            }
        }

        infoList->close();

        SkString hash;
        b.toStringHash(hash);

        if (!SkFsUtils::writeTEXT(templatesHashFilePath.c_str(), hash.c_str()))
        {
            FlatError("CANNOT save current hash for templates VariantDatabase [" << hash << "]: " << templatesHashFilePath);
            return false;
        }

        FlatMessage("SAVED templates current hash [" << hash << "]: " << templatesHashFilePath);
    }

    infoList->destroyLater();

    SkRingBuffer valBuff;
    SkVariant v(templates);
    v.toData(&valBuff);

    SkDataBuffer fileBuff;
    valBuff.copyTo(fileBuff);

    if (!SkFsUtils::writeDATA(templatesFilePath.c_str(), fileBuff))
    {
        FlatError("CANNOT save templates to VariantDatabase: " << templatesFilePath);
        return false;
    }

    FlatMessage("SAVED templates to VariantDatabase: " << templatesFilePath);
    return true;
}

bool SkTemplateManager::loadTemplatesVariant()
{
    if (!checkTemplatesVariant())
    {
        if (!buildTemplatesVariant())
        {
            FlatError("CANNOT load database from VariantDatabase: " << templatesFilePath);
            return false;
        }
    }

    SkDataBuffer fileBuff;

    if (!SkFsUtils::readDATA(templatesFilePath.c_str(), fileBuff))
    {
        FlatError("CANNOT load database from VariantDatabase: " << templatesFilePath);
        return false;
    }

    SkRingBuffer valBuff;
    valBuff.addData(fileBuff.data(), fileBuff.size());

    SkVariant v;
    v.fromData(&valBuff, v);

    templates.clear();
    v.copyToMap(templates);

    FlatMessage("LOADED templates from VariantDatabase: " << templatesFilePath << " [" << fileBuff.size() << " B]");
    return true;
}

void SkTemplateManager::updateCheck()
{
    if (SkFsUtils::exists(templatesFilePath.c_str()) && checkTemplatesVariant())
        return;

    if (buildTemplatesVariant())
        loadTemplatesVariant();
}

SkHtmlTemplate *SkTemplateManager::newHtml(CStr *htmlTemplateFilePath)
{
    SkString filePath = root;
    filePath.append("html/");
    filePath.append(htmlTemplateFilePath);

    SkHtmlTemplate *t = nullptr;

    if (templates.isEmpty())
        t = new SkHtmlTemplate(filePath.c_str(), nullptr);
    else
        t = new SkHtmlTemplate(filePath.c_str(), templates[filePath].data());

    modelers << t;
    return dynamic_cast<SkHtmlTemplate *>(modelers.last());
}

SkJsTemplate *SkTemplateManager::newJs(CStr *jsTemplateFilePath)
{
    SkString filePath = root;
    filePath.append("js/");
    filePath.append(jsTemplateFilePath);

    SkJsTemplate *t = nullptr;

    if (templates.isEmpty())
        t = new SkJsTemplate(filePath.c_str(), nullptr);
    else
        t = new SkJsTemplate(filePath.c_str(), templates[filePath].data());

    modelers << t;
    return dynamic_cast<SkJsTemplate *>(modelers.last());
}

SkJsonTemplate *SkTemplateManager::newJson(CStr *jsonTemplateFilePath)
{
    SkString filePath = root;
    filePath.append("json/");
    filePath.append(jsonTemplateFilePath);

    SkJsonTemplate *t = nullptr;

    if (templates.isEmpty())
        t = new SkJsonTemplate(filePath.c_str(), nullptr);
    else
        t = new SkJsonTemplate(filePath.c_str(), templates[filePath].data());

    modelers << t;
    return dynamic_cast<SkJsonTemplate *>(modelers.last());
}

uint64_t SkTemplateManager::count()
{
    return modelers.count();
}

void SkTemplateManager::clear()
{
    if (modelers.isEmpty())
        return;

    SkAbstractListIterator<SkTemplateModeler *> *itr = modelers.iterator();

    while(itr->next())
        delete itr->item();

    delete itr;
    modelers.clear();
}

SkTreeMap<SkString, SkVariant> &SkTemplateManager::database()
{
    return templates;
}

CStr *SkTemplateManager::getTemplatesFilePath()
{
    return templatesFilePath.c_str();
}

CStr *SkTemplateManager::getTemplatesHashFilePath()
{
    return templatesHashFilePath.c_str();
}
