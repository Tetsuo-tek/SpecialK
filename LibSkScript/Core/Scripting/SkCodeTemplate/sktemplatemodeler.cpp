#include "sktemplatemodeler.h"
#include "Core/System/Filesystem/skfsutils.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkTemplateModeler::SkTemplateModeler()
{
    currentRow = 1;
    currentCol = 1;
}

SkTemplateModeler::SkTemplateModeler(CStr *filePath, CStr *data)
{
    setup(filePath, data);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTemplateModeler::setup(CStr *filePath, CStr *data)
{
    currentRow = 1;
    currentCol = 1;
    templatePath = filePath;

    if (data)
        templateData = data;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTemplateModeler::setVariable(CStr *key, CStr *val)
{
    variables.add(key, val);
}

void SkTemplateModeler::setBlock(CStr *key, CStr *val)
{
    SkStringList l;
    l << val;
    setBlock(key, l);
}

void SkTemplateModeler::setBlock(CStr *key, SkStringList &val)
{
    blocks.add(key, val);
}

void SkTemplateModeler::setBlock(CStr *key, SkTemplateModeler *block)
{
    if (!block)
    {
        setBlock(key, "");
        return;
    }

    SkList<SkTemplateModeler *> blocks;
    blocks << block;

    children.add(key, blocks);
}

void SkTemplateModeler::setBlocks(CStr *key, SkList<SkTemplateModeler *> &blocks)
{
    children.add(key, blocks);
}

void SkTemplateModeler::setCustomization(SkArgsMap &m)
{
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        if (itr->item().key() == "defaultsForVariables")
        {
            SkArgsMap defaultVariables;
            itr->item().value().copyToMap(defaultVariables);

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr1 = defaultVariables.iterator();

            while(itr1->next())
            {
                SkString s = itr1->item().value().toString();
                setVariable(itr1->item().key().c_str(), s.c_str());
            }

            delete itr1;
        }

        else if (itr->item().key() == "defaultsForBlocks")
        {
            SkArgsMap defaultBlocks;
            itr->item().value().copyToMap(defaultBlocks);

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr1 = defaultBlocks.iterator();

            while(itr1->next())
            {
                SkString s = itr1->item().value().toString();
                setBlock(itr1->item().key().c_str(), s.c_str());
            }

            delete itr1;
        }
    }

    delete itr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTemplateModeler::build(SkString &output)
{
    SkString fileContent;

    if (templateData.isEmpty())
    {
        //cout << "!!!!!! " << templatePath << "\n";
        SkFsUtils::readTEXT(templatePath.c_str(), fileContent);
    }

    else
        fileContent = templateData;

    //'parseConfig' create 'codeTemplate'
    if (!parseConfig(fileContent))
        return false;

    CStr *d = codeTemplate.c_str();

    currentRow = 1;
    currentCol = 1;

    uint64_t i=0;

    for(; i<codeTemplate.size(); i++)
    {
        if (d[i] ==  '\n')
        {
            currentRow++;
            currentCol = 1;
        }

        else
            currentCol++;

        // 6 == 2($$) + 2(?$) + >=1(name)
        // when the key name is of 1 char at minimum
        if (i < codeTemplate.size()-5)
        {
            if (d[i] == '$'
                    && ((d[i+1] == '$' && !parseVariableIdentifier(output, i))
                        || (d[i+1] == '#' && !parseBlockIdentifier(output, i))))
            {
                return false;
            }
        }

        output.concat(d[i]);
    }

    return true;
}

bool SkTemplateModeler::build(SkStringList &output)
{
    SkString outputBuffer;

    if (!build(outputBuffer))
        return false;

    outputBuffer.split("\n", output);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTemplateModeler::parseConfig(SkString &content)
{
    codeTemplate.clear();

    uint64_t i = 0;
    CStr *d = content.c_str();

    for(; i<content.size(); i++)
    {
        if (i < content.size()-4 && SkString::compare(&d[i+1], "$%$", 3))
            break;
        else
            codeTemplate.concat(d[i]);
    }

    if (i == content.size())
        return true;

    for(; i<content.size(); i++)
        if (d[i] == '{')
            break;

    SkString cfg;

    for(; i<content.size(); i++)
        cfg.concat(d[i]);

    if (!configMap.isEmpty())
        configMap.clear();

    if (!configMap.fromString(cfg.c_str()))
    {
        FlatError("Template config is NOT valid: "
                  << " [" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
        return false;
    }

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = configMap.iterator();

    while(itr->next())
    {
        if (itr->item().key() == "defaultsForVariables")
        {
            SkArgsMap defaultVariables;
            itr->item().value().copyToMap(defaultVariables);

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr1 = defaultVariables.iterator();

            while(itr1->next())
                if (!variables.contains(itr1->item().key()))
                {
                    SkString s = itr1->item().value().toString();
                    setVariable(itr1->item().key().c_str(), s.c_str());
                }

            delete itr1;
        }

        else if (itr->item().key() == "defaultsForBlocks")
        {
            SkArgsMap defaultBlocks;
            itr->item().value().copyToMap(defaultBlocks);

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr1 = defaultBlocks.iterator();

            while(itr1->next())
                if (!variables.contains(itr1->item().key()))
                {
                    SkString s = itr1->item().value().toString();
                    setBlock(itr1->item().key().c_str(), s.c_str());
                }

            delete itr1;
        }
    }

    delete itr;
    return true;
}

bool SkTemplateModeler::parseVariableIdentifier(SkString &output, uint64_t &i)
{
    i+=2;
    CStr *d = codeTemplate.c_str();

    SkString vKey;

    while(!(d[i] == '$' && d[i+1] == '$'))
    {
        if (i > codeTemplate.size()-2)
        {
            FlatError("End of variable key NOT found: "
                      << "[" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
            return false;
        }

        vKey.concat(d[i]);
        i++;
    }

    i+=2;

    if (vKey.isEmpty())
    {
        FlatError("The variable key CANNOT be empty: "
                  << "[" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
        return false;
    }

    if (variables.contains(vKey))
        output.concat(variables[vKey].c_str());

    else
    {
        SkString replaced;

        if (!onKeyNotFound(vKey.c_str(), replaced))
        {
            FlatError("Variable key NOT found; it has NOT a default: " << vKey
                      << " [" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
            return false;
        }

        output.concat(replaced);
    }

    return true;
}

bool SkTemplateModeler::parseBlockIdentifier(SkString &output, uint64_t &i)
{
    i+=2;
    CStr *d = codeTemplate.c_str();

    SkString bKey;

    while(!(d[i] == '#' && d[i+1] == '$'))
    {
        if (i > codeTemplate.size()-2)
        {
            FlatError("End of block key NOT found: "
                      << "[" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
            return false;
        }

        bKey.concat(d[i]);
        i++;
    }

    i+=2;

    if (bKey.isEmpty())
    {
        FlatError("The block key CANNOT be empty: "
                  << "[" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
        return false;
    }

    if (children.contains(bKey))
    {
        SkStringList lines;

        SkList<SkTemplateModeler *> &l = children[bKey];
        SkAbstractListIterator<SkTemplateModeler *> *itr = l.iterator();

        while(itr->next())
            if (!itr->item()->build(lines))
            {
                delete itr;
                return false;
            }

        delete itr;

        if (!substituteBlock(lines, output, i))
            return false;
    }

    else if (blocks.contains(bKey))
    {
        SkStringList &lines = blocks[bKey];

        if (!substituteBlock(lines, output, i))
            return false;
    }

    else
    {
        SkString replaced;

        if (!onKeyNotFound(bKey.c_str(), replaced))
        {
            FlatError("Block key NOT found; it has NOT a default: " << bKey
                      << " [" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
            return false;
        }

        SkStringList lines;
        lines << replaced;

        if (!substituteBlock(lines, output, i))
            return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTemplateModeler::substituteBlock(SkStringList &lines, SkString &output, uint64_t &i)
{
    if (!lines.isEmpty())
    {
        for(uint64_t t=0; t<lines.count(); t++)
        {
            if (t > 0)
                output.concat(SkString::buildFilled(' ', currentCol-2));

            output.concat(lines[t]);
            output.concat('\n');
        }
    }

    return checkSurplusAtferBlock(i);
}

bool SkTemplateModeler::checkSurplusAtferBlock(uint64_t &i)
{
    CStr *d = codeTemplate.c_str();

    //DROP REAINING CODE (MUST BE EMPTY)
    while(d[i] != '\n' && i<codeTemplate.size())
    {
        if (d[i] != ' ')
        {
            FlatError("Remaining line-slice is not empty: "
                      << " [" << currentRow << ", " << currentCol << "] (" << templatePath << ")");
            return false;
        }

        i++;
    }

    if (d[i] == '\n')
        i++;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTemplateModeler::reset()
{
    variables.clear();
    blocks.clear();
    children.clear();
    codeTemplate.clear();
    configMap.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkHtmlTemplate::SkHtmlTemplate(CStr *filePath, CStr *data) : SkTemplateModeler(filePath, data)
{

}

void SkHtmlTemplate::setBlocks(CStr *key, SkList<SkHtmlTemplate *> &blocks)
{
    SkList<SkTemplateModeler *> l;

    for(uint64_t i=0; i<blocks.count(); i++)
        l << blocks[i];

    dynamic_cast<SkTemplateModeler *>(this)->setBlocks(key, l);
}

SkString SkHtmlTemplate::genericElement(CStr *id, CStr *tagName, CStr *content, CStr *classes, CStr *optAttributes)
{
    SkString output = "<";
    output.append(tagName);

    if (!SkString::isEmpty(id))
    {
        output.append(" id=\"");
        output.append(id);
        output.append("\"");
    }

    if (!SkString::isEmpty(classes))
    {
        output.append(" class=\"");
        output.append(classes);
        output.append("\"");
    }

    if (!SkString::isEmpty(optAttributes))
        output.append(optAttributes);

    output.append(">");

    if (!SkString::isEmpty(content))
        output.append(content);

    output.append("</");
    output.append(tagName);
    output.append(">");

    return output;
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkJsTemplate::SkJsTemplate(CStr *filePath, CStr *data) : SkTemplateModeler(filePath, data)
{}

void SkJsTemplate::setBlocks(CStr *key, SkList<SkJsTemplate *> &blocks)
{
    SkList<SkTemplateModeler *> l;

    for(uint64_t i=0; i<blocks.count(); i++)
        l << blocks[i];

    dynamic_cast<SkTemplateModeler *>(this)->setBlocks(key, l);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkJsonTemplate::SkJsonTemplate(CStr *filePath, CStr *data) : SkTemplateModeler(filePath, data)
{}

void SkJsonTemplate::setBlocks(CStr *key, SkList<SkJsonTemplate *> &blocks)
{
    SkList<SkTemplateModeler *> l;

    for(uint64_t i=0; i<blocks.count(); i++)
        l << blocks[i];

    dynamic_cast<SkTemplateModeler *>(this)->setBlocks(key, l);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
