#ifndef SKTEMPLATEMODELER_H
#define SKTEMPLATEMODELER_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skstringlist.h"
#include "Core/Containers/skargsmap.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
    template variable
        $$<VARNAME>$$      -> COULD BE INSIDE OTHER CODE

    template block
        $#<BLOCKNAME>#$    -> MUST BE ONLY ONE WORD ON A TEMPLATE LINE,
                           -> MUST BE LONELY, WHITHOUT OTHER CODE ON IT
                           -> THIS SUBSTITUTION PRESERVE ORIGINAL INDENTATION
                           -> FOR UNFORMATTED OR ONE-LINE CODE USE variable

    separator code/cfg
        $%$                -> ITS LINE CONTAINS ONLY IT (WITHOUT OTHER CHARs)
                           -> COULD NOT EXISTS
                           -> IT IS FOLLOWED BY CONFIG JSON DOC WITH DEFAULT FOR
                              - defaultsForVariables
                              - defaultsForBlocks
*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkTemplateModeler extends SkFlatObject
{
    public:
        SkTemplateModeler();
        SkTemplateModeler(CStr *filePath, CStr *data);

        void setup(CStr *filePath, CStr *data);

        void setVariable(CStr *key, CStr *val);

        void setBlock(CStr *key, CStr *val);
        void setBlock(CStr *key, SkStringList &val);
        void setBlock(CStr *key, SkTemplateModeler *block);
        void setBlocks(CStr *key, SkList<SkTemplateModeler *> &blocks);

        void setCustomization(SkArgsMap &m);

        bool build(SkString &output);
        bool build(SkStringList &output);

        void reset();

    protected:
        virtual bool onKeyNotFound(CStr *, SkString &){return false;}

    private:
        SkString templatePath;
        SkString templateData;
        SkTreeMap<SkString, SkString> variables;
        SkTreeMap<SkString, SkStringList> blocks;
        SkTreeMap<SkString, SkList<SkTemplateModeler *>> children;
        SkString codeTemplate;
        uint64_t currentRow;
        uint64_t currentCol;
        SkArgsMap configMap;

        bool parseConfig(SkString &codeTemplate);

        bool parseVariableIdentifier(SkString &output, uint64_t &i);
        bool parseBlockIdentifier(SkString &output, uint64_t &i);

        bool substituteBlock(SkStringList &lines, SkString &output, uint64_t &i);
        bool checkSurplusAtferBlock(uint64_t &i);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHtmlTemplate extends SkTemplateModeler
{
public:
    SkHtmlTemplate(CStr *filePath, CStr *data);

    void setBlocks(CStr *key, SkList<SkHtmlTemplate *> &blocks);

    static SkString genericElement(CStr *id, CStr *tagName, CStr *codeTemplate="", CStr *classes="", CStr *optAttributes="");
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkJsTemplate extends SkTemplateModeler
{
public:
    SkJsTemplate(CStr *filePath, CStr *data);

    void setBlocks(CStr *key, SkList<SkJsTemplate *> &blocks);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkJsonTemplate extends SkTemplateModeler
{
public:
    SkJsonTemplate(CStr *filePath, CStr *data);

    void setBlocks(CStr *key, SkList<SkJsonTemplate *> &blocks);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKTEMPLATEMODELER_H
