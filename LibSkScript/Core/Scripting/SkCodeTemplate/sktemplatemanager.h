#ifndef SKTEMPLATEMANAGER_H
#define SKTEMPLATEMANAGER_H

#include "sktemplatemodeler.h"

class SkTemplateManager extends SkFlatObject
{
    public:
        SkTemplateManager();
        ~SkTemplateManager();

        //could be re-initialized
        bool init(CStr *rootPath);
        void updateCheck();

        //returned pointers will be stored inside this object
        //DO NOT DELETE returned items by hand
        SkHtmlTemplate *newHtml(CStr *httpTemplateFilePath);
        SkJsTemplate *newJs(CStr *jsTemplateFilePath);
        SkJsonTemplate *newJson(CStr *jsonTemplateFilePath);

        uint64_t count();

        //detroys all SkTemplateModeler instances created
        void clear();

        SkTreeMap<SkString, SkVariant> &database();

        CStr *getTemplatesFilePath();
        CStr *getTemplatesHashFilePath();

    private:
        SkString root;
        SkString templatesFilePath;
        SkString templatesHashFilePath;
        SkTreeMap<SkString, SkVariant> templates;
        SkString dflt;
        SkList<SkTemplateModeler *> modelers;

        bool checkTemplatesVariant();
        bool buildTemplatesVariant();
        bool loadTemplatesVariant();
};

#endif // SKTEMPLATEMANAGER_H
