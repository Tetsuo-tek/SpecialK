#ifndef SKPLANCTON_H
#define SKPLANCTON_H

#if defined(SKPLANCTON_VM)

#include <Core/Containers/skstring.h>
#include <Core/Containers/skvariant.h>
#include <Core/Containers/skarray.h>

#include "skplanctonvm.h"

#define PK_DEBUG_PRINT_CODE
#define UINT8_COUNT (UINT8_MAX + 1)

namespace SkPlancton
{
    SkPlanctonInterpretResult runFile(CStr *filePath);
    SkPlanctonInterpretResult runPrompt();
    void error(UInt line, CStr *message);
    void report(UInt line, CStr *where, CStr *message);
};

#endif

#endif // SKPLANCTON_H
