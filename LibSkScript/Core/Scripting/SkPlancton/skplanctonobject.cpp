#include "skplanctonobject.h"

#if defined(SKPLANCTON_VM)

SkPlanctonObject::SkPlanctonObject()
{

}

SkString SkPlanctonClass::getName() const
{
    return name;
}

void SkPlanctonClass::setName(const SkString &newName)
{
    name = newName;
}

//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//

SkPlanctonChunk SkPlanctonFunction::getChunk() const
{
    return chunk;
}

void SkPlanctonFunction::setChunk(const SkPlanctonChunk &newChunk)
{
    chunk = newChunk;
}

SkString SkPlanctonFunction::getName() const
{
    return name;
}

void SkPlanctonFunction::setName(const SkString &newName)
{
    name = newName;
}

Int SkPlanctonFunction::getArity() const
{
    return arity;
}

void SkPlanctonFunction::setArity(Int newArity)
{
    arity = newArity;
}

//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//

SkPlanctonClass *SkPlanctonInstance::getKlass() const
{
    return klass;
}

void SkPlanctonInstance::setKlass(SkPlanctonClass *newKlass)
{
    klass = newKlass;
}

//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//

#endif
