#include "skplancton.h"
#include "skplanctonscanner.h"
#include "Core/System/Filesystem/skfsutils.h"

#if defined(SKPLANCTON_VM)

using namespace SkPlancton;

SkPlanctonInterpretResult SkPlancton::runFile(CStr *filePath)
{
    SkString source;
    if (!SkFsUtils::readTEXT(filePath, source))
        return INTERP_COMPILE_ERROR;

    std::cout << source.c_str() << std::endl;



    SkPlanctonScanner *s = new SkPlanctonScanner(source);

    SkPlanctonToken t;
    while ((t = s->getToken()).getType() != TOKEN_EOF)
    {
        std::cout <<  t.getLexeme().c_str() << std::endl;
    }
/*
    SkPlanctonVm *vm = new SkPlanctonVm();
    SkPlanctonInterpretResult result = vm->interpret(source);
    delete vm;
    return result;
*/
    return INTERP_OK;
}

SkPlanctonInterpretResult SkPlancton::runPrompt()
{
    SkString line;
    return INTERP_OK;
}

void SkPlancton::error(UInt line, CStr *message)
{
    report(line, "", message);
}

void SkPlancton::report(UInt line, CStr *where, CStr *message)
{
    std::cout << "[line " << line << "] Error"
              << where << ": " << message << std::endl;
}

#endif
