#include "skplanctonparser.h"

#if defined(SKPLANCTON_VM)

SkPlanctonParser::SkPlanctonParser(SkString &source)
{
    scanner = new SkPlanctonScanner(source);
}

SkPlanctonFunction *SkPlanctonParser::compile()
{
    SkPlanctonCompiler compiler;
    compilerInit(&compiler);

    hadError = false;
    panicMode = false;

    advance();

    while (!match(TOKEN_EOF))
        declaration();

    SkPlanctonFunction *function = compilerEnd();

    return hadError ? nullptr : function;
}

void SkPlanctonParser::compilerInit(SkPlanctonCompiler *compiler)
{
    compiler->enclosing = nullptr;
    compiler->localCount = 0;
    currentCompiler = compiler;
}

#endif
