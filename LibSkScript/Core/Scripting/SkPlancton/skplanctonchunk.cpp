#include "skplanctonchunk.h"

#if defined(SKPLANCTON_VM)

void SkPlanctonChunk::write(UByte byte, UInt line)
{
    writeLine(line);
    code.append(byte);
}

UInt SkPlanctonChunk::getLine(size_t offset)
{
    for (size_t i = 0, c_pos = 0; i < lines.size(); ++i)
    {
        c_pos += lines[i] >> 32;

        if (c_pos >= offset)
            return lines[i] & 0xffffffff;
    }

    return lines[lines.size() - 1] & 0xffffffff;
}

size_t SkPlanctonChunk::pushConstant(SkVariant value)
{
    constants.append(value);
    return constants.size() - 1;
}

void SkPlanctonChunk::writeLine(UInt line)
{
    if (lines.size() > 0 && (lines[lines.size() - 1] & 0xffffffff) == line)
    {
        UInt count = (lines[lines.size() - 1] >> 32);
        UInt c_line = lines[lines.size() - 1] & 0xffffffff;
        lines[lines.size() - 1] = c_line | (((ULong)count + 1) << 32);
    }

    else
    {
        lines.append(line);
    }
}
#endif
