#ifndef SKPLANCTONOBJECT_H
#define SKPLANCTONOBJECT_H

#if defined(SKPLANCTON_VM)

#include "skplanctonchunk.h"

class SkPlanctonObject
{
public:
    SkPlanctonObject();
};

class SkPlanctonFunction : SkPlanctonObject
{
public:
    Int getArity() const;
    void setArity(Int newArity);
    SkPlanctonChunk getChunk() const;
    void setChunk(const SkPlanctonChunk &newChunk);
    SkString getName() const;
    void setName(const SkString &newName);

private:
    Int arity;
    SkPlanctonChunk chunk;
    SkString name;
};

class SkPlanctonClass : SkPlanctonObject
{
public:

    SkString getName() const;
    void setName(const SkString &newName);

private:
    SkString name;
    //HashTable<SkVariant> methods;
};

class SkPlanctonInstance : SkPlanctonObject
{
public:
    SkPlanctonClass *getKlass() const;
    void setKlass(SkPlanctonClass *newKlass);

private:
    SkPlanctonClass *klass;
    //HashTable<SkVariant> fields;
};

class SkPlanctonBoundMethod : SkPlanctonObject
{

};

#endif

#endif // SKPLANCTONOBJECT_H
