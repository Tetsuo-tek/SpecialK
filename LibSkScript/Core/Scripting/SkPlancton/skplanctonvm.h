#ifndef SKPLANCTONVM_H
#define SKPLANCTONVM_H

#if defined(SKPLANCTON_VM)

//#include "skplancton.h"
#include <Core/Containers/skvariant.h>
#include <Core/Containers/skstring.h>
//#include "skplanctonobject.h"

#define FRAMES_MAX 64
#define STACK_MAX (FRAMES_MAX * (UINT8_MAX + 1))

enum SkPlanctonInterpretResult
{
    INTERP_OK,
    INTERP_COMPILE_ERROR,
    INTERP_RUNTIME_ERROR
};

struct SkPlanctonCallFrame
{
//    SkPlanctonFunction *function;
    UByte *ip;
    SkVariant *slots;
};

class SkPlanctonVm extends SkFlatObject
{
public:

    SkPlanctonVm();
    ~SkPlanctonVm();

    SkPlanctonInterpretResult interpret(SkString &source);
 //   void push (SkVariant value);
 //   SkVariant pop();

private:
    
  //  SkPlanctonInterpretResult run();
    SkVariant resetStack();
  //  SkVariant peek();

//    bool call(SkPlanctonFunction *function, Int argCount);
    //bool callVariant (SkVariant callee, Int arg_count);

    SkPlanctonCallFrame frames[FRAMES_MAX];
    size_t frameSize;

    SkVariant stack[STACK_MAX];
    SkVariant *stackTop;

    size_t bytesAllocated;
};

#endif

#endif // SKPLANCTONVM_H
