#ifndef SKPLANCTONPARSER_H
#define SKPLANCTONPARSER_H

#include "skplanctonscanner.h"
#include "skplanctonobject.h"

#if defined(SKPLANCTON_VM)

class SkPlanctonExpr
{
};

class SkPlanctonBinaryExpr : SkPlanctonExpr
{
public:
    SkPlanctonBinaryExpr(SkPlanctonExpr *left, SkPlanctonToken *op, SkPlanctonExpr *right)
        : left(left), op(op), right(right) {}
    SkPlanctonExpr *left;
    SkPlanctonToken *op;
    SkPlanctonExpr *right;
};

class SkPlanctonGroupingExpr : SkPlanctonExpr
{
public:
    SkPlanctonGroupingExpr(SkPlanctonExpr *expression)
        : expression(expression) {}
    SkPlanctonExpr *expression;
};

class SkPlanctonUnaryExpr : SkPlanctonExpr
{
public:
    SkPlanctonUnaryExpr(SkPlanctonExpr *left, SkPlanctonToken *op, SkPlanctonExpr *right)
        : left(left), op(op), right(right) {}
    SkPlanctonExpr *left;
    SkPlanctonToken *op;
    SkPlanctonExpr *right;
};
/*enum SkPlanctonPrecedence
{
    PREC_NONE,
    PREC_ASSIGNMENT,
    PREC_OR,
    PREC_AND,
    PREC_EQUALITY,
    PREC_COMPARISON,
    PREC_TERM,
    PREC_FACTOR,
    PREC_UNARY,
    PREC_CALL,
    PREC_PRIMARY
};

typedef void (*SkPlanctonParseFunc)(bool isLvalue);

struct SkPlanctonParseRule
{
    SkPlanctonParseFunc prefix;
    SkPlanctonParseFunc infix;
    SkPlanctonPrecedence precedence;
};

struct SkPlanctonLocal
{
    SkPlanctonToken name;
    Int depth;
    bool isCaptured;
};

enum SkPlanctonFunctionType
{
    INITIALIZER,
    METHOD,
    FUNCTION,
    SIGNAL,
    SLOT,
    SCRIPT
};

struct SkPlanctonCompiler
{
    SkPlanctonCompiler *enclosing;

    SkPlanctonLocal locals[UINT8_MAX + 1];
    UInt localCount;

    Int scopeDepth;
};

struct SkPlanctonClassCompiler
{
    bool hasSuperclass;
    SkPlanctonClassCompiler *enclosing;
};

class SkPlanctonParser extends SkFlatObject
{
public:
    SkPlanctonParser(SkString &source);
    SkPlanctonFunction *compile();
private:
    SkPlanctonCompiler *currentCompiler;
    SkPlanctonScanner *scanner;

    SkPlanctonToken previousToken;
    SkPlanctonToken currentToken;
    bool hadError;
    bool panicMode;

    void compilerInit(SkPlanctonCompiler *compiler);
    SkPlanctonFunction *compilerEnd();
    void advance();
    bool match(SkPlanctonTokenType type);

    // parsing functions
    void parsePrecedence(SkPlanctonPrecedence precedence);
    SkPlanctonParseRule *getRule(SkPlanctonTokenType type);
    //expressions
    void expression(bool isLvalue);
    void expBinary(bool isLvalue);
    void expGrouping(bool isLvalue);
    void expUnary(bool isLvalue);

    void declaration();
    // statements
    void statement();
    void expressionStatement();
    void jumpStatement();
    void returnStatement();
};
*/
#endif

#endif // SKPLANCTONPARSER_H
