#include "skplanctonvm.h"
#include "skplanctonobject.h"
#include "skplanctonparser.h"

#if defined(SKPLANCTON_VM)

SkPlanctonVm::SkPlanctonVm()
{
    resetStack();
    bytesAllocated = 0;
}

SkPlanctonVm::~SkPlanctonVm()
{

}

SkPlanctonInterpretResult SkPlanctonVm::interpret(SkString &source)
{
    SkPlanctonParser *parser = new SkPlanctonParser(source);

    SkPlanctonFunction *function = parser->compile();

    if (function == nullptr)
        return INTERP_COMPILE_ERROR;

    //call(function, 0);

    return INTERP_OK;
    //return run();
}

void SkPlanctonVm::resetStack()
{
    stackTop = stack;
    frameSize = 0;
}
/*
void SkPlanctonVm::push(SkVariant v) 
{
    *stackTop = v;
    stackTop++;
}

SkVariant SkPlanctonVm::pop()
{
    stackTop--;
    return *stackTop;
}

bool SkPlanctonVm::call(SkPlanctonFunction *function, Int argCount)
{
    if (argCount != function->arity) 
    {
        //runtim eerror
        return false;
    }

    if (frameSize == FRAMES_MAX) 
    {
        return false;
    }

    SkPlanctonCallFrame *frame = &frames[frameSize++];

    frame->function = function;
    frame->ip = frunction->chunk.code;
    frame->slots = stackTop - argCount - 1;

    return true;
}

#define READ_BYTE() \
    (*frame->ip++)
#define READ_CONSTANT() \
    (frame->function->chunk.constants.values[READ_BYTE()])
#define READ_SHORT() \
    (frame->ip += 2, (UShort)((frame->ip[-2] << 8) | frame->ip[-1]))
#define READ_STRING() \
    (READ_CONSTANT().toString())
//#define BINARY_OP \
//    do {\
//        if ()
//    } while (false)

SkPlanctonInterpretResult SkPlanctonVm::run()
{
    SkPlanctonCallFrame *frame = &frames[frameSize - 1];
    UByte instruction;

    while (true) 
    {
        switch (instruction = READ_BYTE())
        {
            case OP_

        }
    }
}
*/

#endif
