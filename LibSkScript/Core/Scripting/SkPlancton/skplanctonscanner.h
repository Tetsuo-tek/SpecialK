#ifndef SKPLANCTONSCANNER_H
#define SKPLANCTONSCANNER_H

#if defined(SKPLANCTON_VM)

#include "Core/Containers/skstring.h"

enum SkPlanctonTokenType
{
    // flow control
    TOKEN_AND, TOKEN_OR, 
    TOKEN_IF, TOKEN_ELSE,
    TOKEN_WHILE, TOKEN_FOR,
    TOKEN_RETURN,

    // literals
    TOKEN_TRUE, TOKEN_FALSE,
    TOKEN_INT, TOKEN_REAL, 
    TOKEN_NULL, TOKEN_STRING,
    TOKEN_IDENTIFIER,

    // declarators/initializers
    TOKEN_VAR, TOKEN_THIS,
    TOKEN_CLASS, TOKEN_FUNC,
    TOKEN_SUPER, TOKEN_SLOT, 
    TOKEN_SIGNAL,

    // operators
    TOKEN_BANG, TOKEN_BANG_EQUAL,
    TOKEN_EQUAL, TOKEN_EQUAL_EQUAL,
    TOKEN_GREATER, TOKEN_GREATER_EQUAL,
    TOKEN_LESS, TOKEN_LESS_EQUAL,
    TOKEN_PLUS, TOKEN_PLUS_EQUAL,
    TOKEN_MINUS, TOKEN_MINUS_EQUAL,
    TOKEN_SLASH, TOKEN_SLASH_EQUAL,
    TOKEN_STAR, TOKEN_STAR_EQUAL,
    TOKEN_MOD, TOKEN_MOD_EQUAL,
    
    // others
    TOKEN_LEFT_PAREN, TOKEN_RIGHT_PAREN,
    TOKEN_LEFT_SQUARE_BRACE, TOKEN_RIGHT_SQUARE_BRACE,
    TOKEN_LEFT_BRACE, TOKEN_RIGHT_BRACE,
    TOKEN_COMMA, TOKEN_DOT,
    TOKEN_SEMICOLON, TOKEN_COLON,
    TOKEN_PLUS_PLUS, TOKEN_MINUS_MINUS,
    TOKEN_ERROR, TOKEN_EOF,
};

class SkPlanctonToken
{
public:
    SkPlanctonToken(SkPlanctonTokenType type, SkString lexeme, UInt line);
    SkPlanctonToken() {};

    SkString &toString();

    UInt getLine() const;
    SkPlanctonTokenType getType() const;
    SkString getLexeme() const;

private:
    SkPlanctonTokenType type;
    SkString lexeme;
    UInt line;
};


class SkPlanctonScanner
{
public:
    SkPlanctonScanner(SkString &source);
    SkPlanctonToken getToken();

private:

    char *start;
    char *current;
    UInt line;
    
    void scannerInit();
    
    SkPlanctonToken makeToken (SkPlanctonTokenType type);
    SkPlanctonToken makeTokenFrom(SkPlanctonTokenType type, SkString &str);
    SkPlanctonToken makeIdentifier();
    SkPlanctonToken makeString(); 
    SkPlanctonToken makeNumber(); 
    SkPlanctonToken makeInt(); 
    SkPlanctonToken makeReal(); 
    SkPlanctonToken errorToken (CStr *message);

    char advance ();
    bool match (char c);
    char peek ();
    char peekNext();

    char isDigit(char c);
    char isAlpha(char c);
    SkPlanctonTokenType isKeyword(UByte start, UByte length, CStr *rest, SkPlanctonTokenType type);
    SkPlanctonTokenType identifierType();
    void skipWhitespace();

    inline bool isAtEnd()
    {
        return *current == '\0';
    }
};

#endif

#endif // SKPLANCTONSCANNER_H
