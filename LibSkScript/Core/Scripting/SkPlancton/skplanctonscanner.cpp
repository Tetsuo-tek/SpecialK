#include "skplanctonscanner.h"

#if defined(SKPLANCTON_VM)

SkPlanctonScanner::SkPlanctonScanner(SkString &source)
{
    start = (char*) source.c_str();
    current = (char*) source.c_str();
    line = 1;
}

SkPlanctonToken SkPlanctonScanner::getToken()
{
    skipWhitespace();
    start = current;

    if (isAtEnd())
        return makeToken(TOKEN_EOF);

    char c = advance();

    if (isAlpha(c))
        return makeIdentifier();

    if (isDigit(c))
        return makeNumber();
    
    switch (c) 
    {
        case '(': return makeToken(TOKEN_LEFT_PAREN);
        case ')': return makeToken(TOKEN_RIGHT_PAREN);
        case '{': return makeToken(TOKEN_LEFT_BRACE);
        case '}': return makeToken(TOKEN_RIGHT_BRACE);
        case '[': return makeToken(TOKEN_LEFT_SQUARE_BRACE);
        case ']': return makeToken(TOKEN_RIGHT_SQUARE_BRACE);
        case ';': return makeToken(TOKEN_SEMICOLON);
        case ':': return makeToken(TOKEN_COLON);
        case ',': return makeToken(TOKEN_COMMA);
        case '.': return makeToken(TOKEN_DOT);
        case '"': return makeString();

        case '*': return makeToken(match('=') ? TOKEN_STAR_EQUAL : TOKEN_STAR);
        case '/': return makeToken(match('=') ? TOKEN_SLASH_EQUAL : TOKEN_SLASH);
        case '%': return makeToken(match('=') ? TOKEN_MOD_EQUAL : TOKEN_MOD);
        case '!': return makeToken(match('=') ? TOKEN_BANG_EQUAL : TOKEN_BANG);
        case '=': return makeToken(match('=') ? TOKEN_EQUAL_EQUAL : TOKEN_EQUAL);
        case '<': return makeToken(match('=') ? TOKEN_LESS_EQUAL : TOKEN_LESS);
        case '>': return makeToken(match('=') ? TOKEN_GREATER_EQUAL : TOKEN_GREATER);
        case '-':
            if (match('='))
                return makeToken(TOKEN_MINUS_EQUAL);
            else if (match('-')) // ---a will be scanned as -- -a which will give error in parser
                return makeToken(TOKEN_MINUS_MINUS);

            return makeToken(TOKEN_MINUS);
        case '+':
            if (match('='))
                return makeToken(TOKEN_PLUS_EQUAL);
            else if (match('+'))
                return makeToken(TOKEN_PLUS_PLUS);

            return makeToken(TOKEN_PLUS);
        default:
            std::cout << "character :" << c <<":" << std::endl;
            return errorToken("Unknown character.");
    }
}

SkPlanctonToken SkPlanctonScanner::makeToken(SkPlanctonTokenType type)
{
    SkPlanctonToken token(type, SkString(start, (UInt)(current - start)), line);
    return token;
}

SkPlanctonToken SkPlanctonScanner::makeTokenFrom(SkPlanctonTokenType type, SkString &str)
{
    SkPlanctonToken token(type, str, line);
    return token;
}

SkPlanctonToken SkPlanctonScanner::makeIdentifier()
{
    while (isDigit(peek()) || isAlpha(peek()))
        advance();

    return makeToken(identifierType());
}

SkPlanctonToken SkPlanctonScanner::makeString()
{
    SkString string;
    UInt cline = line;
    string += advance();

    while (peek() != '"' && !isAtEnd()) 
    {
        if (peek() == '\n') 
        {
            string += peek();
            cline++;
        } 
        else if (peek() == '\\') 
        {
            advance();

            switch (peek()) 
            {
                case 'a': string += '\a';
                    break;
                case 'b': string += '\b';
                    break;
                case 'f': string += '\f';
                    break;
                case 'n': string += '\n';
                    break;
                case 'r': string += '\r';
                    break;
                case 't': string += '\t';
                    break;
                case 'v': string += '\v';
                    break;
                case '"': string += '\"';
                    break;
                case '\\': string += '\\';
                    break;
                default:
                    return errorToken("Escape code not implemented or wrong.");
            } 
        } 

        else string += peek();

        advance();
    }

    if (isAtEnd()) 
        return errorToken ("Unterminated string.");
    
    string += advance();
    line = cline;

    return makeTokenFrom(TOKEN_STRING, string);
}

SkPlanctonToken SkPlanctonScanner::makeNumber()
{
    bool isReal = false;
    while (isDigit(peek())) 
    {
        advance();
    }
    
    if (peek() == '.' && isDigit(peekNext())) 
    {
        isReal = true;
        advance();

        while (isDigit(peek()))
            advance();
    }

    return makeToken(isReal ? TOKEN_REAL : TOKEN_INT);
}

SkPlanctonToken SkPlanctonScanner::errorToken(CStr *message)
{
    SkPlanctonToken token(TOKEN_ERROR, message, line);
    return token;
}

char SkPlanctonScanner::advance()
{
    current++;
    return current[-1];
}

bool SkPlanctonScanner::match(char expected)
{    
    if (isAtEnd()) 
        return false;

    if (*current != expected)
        return false;

    current++;
    return true;
}

char SkPlanctonScanner::peek()
{
    return *current;
}

char SkPlanctonScanner::peekNext()
{
    if (isAtEnd ())
        return '\0';

    return current[1];
}

char SkPlanctonScanner::isDigit(char c)
{
    return c >= '0' && c <= '9';
}

char SkPlanctonScanner::isAlpha(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

SkPlanctonTokenType SkPlanctonScanner::isKeyword(UByte kstart, UByte length, 
        CStr *rest, SkPlanctonTokenType type)
{
    if (current - kstart == start + length 
        && memcmp (start + kstart, rest, length) == 0) 
        return type;

    return TOKEN_IDENTIFIER;
}

SkPlanctonTokenType SkPlanctonScanner::identifierType()
{
    switch (*start) 
    {
        case 'a': return isKeyword(1, 2, "nd", TOKEN_AND);
        case 'c': return isKeyword(1, 4, "lass", TOKEN_CLASS);
        case 'e': return isKeyword(1, 3, "lse", TOKEN_ELSE);
        case 'f': 
            if (current - start > 1) 
            {
                switch (start[1]) 
                {
                    case 'a': return isKeyword(2, 3, "lse", TOKEN_FALSE);
                    case 'o': return isKeyword(2, 1, "r", TOKEN_FOR);
                    case 'n': return isKeyword(2, 3, "unc", TOKEN_FUNC);
                }
            }
            break;
        case 'i': return isKeyword (1, 1, "f", TOKEN_IF);
        case 'n': return isKeyword (1, 3, "ull", TOKEN_NULL);
        case 'o': return isKeyword (1, 1, "r", TOKEN_OR);
        case 'r': return isKeyword (1, 5, "eturn", TOKEN_RETURN);
        case 's': 
            if (current - start > 1)
            {
                switch (start[1])
                {
                    case 'u': return isKeyword (2, 3, "per", TOKEN_SUPER);
                    case 'i': return isKeyword (2, 4, "gnal", TOKEN_SIGNAL);
                    case 'l': return isKeyword (2, 2, "ot", TOKEN_SLOT);
                }
            }
            break; 
        case 't':
            if (current - start > 1) 
            {
                switch (start[1]) 
                {
                    case 'h': return isKeyword (2, 2, "is", TOKEN_THIS);
                    case 'r': return isKeyword (2, 2, "ue", TOKEN_TRUE);
                }
            }
            break;
        case 'v': return isKeyword (1, 2, "ar", TOKEN_VAR);
        case 'w': return isKeyword (1, 4, "hile", TOKEN_WHILE);
    }

    return TOKEN_IDENTIFIER;
}

void SkPlanctonScanner::skipWhitespace()
{
    while (true) 
    {
        switch (peek()) 
        {
        case ' ':
        case '\t':
        case '\r':
            advance();
            break;
        case '\n':
            line++;
            advance();
            break;
        case '/': // comment ?
            if (peekNext() == '/') 
            {
                while (peek() != '\n' && !isAtEnd()) 
                    advance();

                if (peek() == '\n')
                    advance();

            } else return;
        default:
            return;
        } 
    }
}

SkPlanctonToken::SkPlanctonToken(SkPlanctonTokenType type, SkString lexeme, UInt line)
{
    this->type = type;
    this->lexeme = lexeme;
    this->line = line;
}

UInt SkPlanctonToken::getLine() const
{
    return line;
}

SkPlanctonTokenType SkPlanctonToken::getType() const
{
    return type;
}

SkString SkPlanctonToken::getLexeme() const
{
    return lexeme;
}

#endif
