#ifndef SKPLANCTONCHUNK_H
#define SKPLANCTONCHUNK_H

#include <Core/Containers/skstring.h>
#include <Core/Containers/skvariant.h>
#include <Core/Containers/skarray.h>

#if defined(SKPLANCTON_VM)

enum SkPlanctonOpCode
{
    OP_CONSTANT = 0,
    OP_NULL,
    OP_TRUE,
    OP_FALSE,
    OP_POP,
    OP_DEFINE_GLOBAL,
    OP_GET_GLOBAL,
    OP_SET_GLOBAL,
    OP_GET_LOCAL,
    OP_SET_LOCAL,
    OP_GET_PROPERTY,
    OP_SET_PROPERTY,
    OP_EQUAL,
    OP_GREATER,
    OP_LESS,
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_MOD,
    OP_NOT,
    OP_AND,
    OP_OR,
    OP_NEGATE,
    OP_LOOP,
    OP_CALL,
    OP_INVOKE,
    OP_SUPER_INVOKE,
    OP_JUMP,
    OP_JUMP_IF_FALSE,
    OP_RETURN,
    OP_CLASS,
    OP_GET_SUPER,
    OP_INHERIT,
    OP_METHOD
};

class SkPlanctonChunk extends SkFlatObject
{
public:
    void write(UByte byte, UInt line);
    UInt getLine(size_t offset);
    size_t pushConstant(SkVariant value);

private:

    SkArray<UByte> code;
    SkArray<ULong> lines;

    SkVariantVector constants;

    void writeLine(UInt line);
};

#endif

#endif // SKPLANCTONCHUNK_H
