/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKASMSTACK_H
#define SKASMSTACK_H

#if defined(ENABLE_SKASM)

#include "Core/Object/skobject.h"
#include "Core/Containers/skargsmap.h"

class SkAsm;
class SkAsmProgram;
class SkAsmCodeLine;
class SkAsmAbstractFunction;
class SkAsmIdentifier;
struct SkAsmHeapLoc;

struct SkAsmStackLoc
{
    SkVariant v;
    SkAsmHeapLoc *heapLoc;

    SkAsmStackLoc()
    {heapLoc = nullptr;}
};

class SKSCRIPT SkAsmStack extends SkFlatObject
{
    public:
        SkAsmStack();

        void setProgram(SkAsmProgram *p, SkAsmAbstractFunction *fOwner);

        bool contains(CStr *identifier);
        bool def(CStr *identifier, SkVariant &val);
        bool def(CStr *identifier, SkAsmHeapLoc *hLoc);
        bool set(CStr *identifier, SkVariant &val);
        bool set(CStr *identifier, SkAsmHeapLoc *hLoc);
        bool udef(CStr *identifier);

        //realVariable is used only for containers
        //realVariable means real SkAsmHeapLocation for containers
        SkVariant &get(SkAsmIdentifier *id);

        SkVariant &getStructProperty(CStr *identifier, CStr *property);
        bool setStructProperty(CStr *identifier, CStr *property, SkVariant &val);

        SkAsmStackLoc *getStackLocation(CStr *identifier);

        void clear();

    private:
        SkMap<SkString, SkAsmStackLoc *> stack;

        // A PATCH TO RETURN REFERENCE FOR MAP PROPs AND LIST ITEMs CONTAINED IN
        // A MAP OR IN A LIST STORED ON VARIANT
        SkVariant temporaryStableValue;

        SkAsmProgram *prg;
        SkAsmAbstractFunction *fCTX;

        bool setupVariable(bool definingIt, CStr *identifier, SkVariant &val, SkAsmHeapLoc *hLoc=nullptr);
};

void *varPtrToCustom(SkVariant *v);
SkString varPtrToCustomString(SkVariant *v);

#endif

#endif // SKASMSTACK_H
