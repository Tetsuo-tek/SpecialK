#ifndef SKASMISA_H
#define SKASMISA_H

#if defined(ENABLE_SKASM)

#include "Core/Object/skobject.h"

struct SkAsmCodeLine;
struct SkAsmProgram;

typedef bool (SkAsmInstrFunct)(SkAsmProgram *, SkAsmCodeLine *);

struct SkAsmInstr//NO typedef
{
    uint64_t id;

    //the name of the instruction
    CStr *name;
    uint64_t nameLen;

    //required operands number
    uint8_t reqOperandsCount;

    //required operands number
    uint8_t maxOperandsCount;

    SkAsmInstrFunct *exec;
};

class SkAsmISA
{
    public:
        SkAsmISA();
        SkAsmInstr *getInstructions(uint64_t &count);

    private:
        void addInstr(CStr *name,
                      uint8_t reqOperandsCount,
                      uint8_t maxOperandsCount,
                      SkAsmInstrFunct *exec);

        SkAsmInstr *instructions;
        uint64_t instrCount;

        //INSTRUCTIONs
        static bool jmp     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool call    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool ret     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool end     (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool def     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool udef    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool move    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool gglv    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sglv    (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool blt     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool ble     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bgt     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bge     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool beq     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bne     (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool bltz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool blez    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bgtz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bgez    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool beqz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool bnez    (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool slt     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sle     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sgt     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sge     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool seq     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sne     (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool sltz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool slez    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sgtz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sgez    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool seqz    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool snez    (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool add     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sub     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool mul     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool div     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool rem     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool pow     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool inc     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool dec     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool neg     (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool prt     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool prtl    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool msg     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool wrn     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool err     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool dbg     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool pdbg    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool sys     (SkAsmProgram *p, SkAsmCodeLine *l);

        static bool mks     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool mko     (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool atch    (SkAsmProgram *p, SkAsmCodeLine *l);
        static bool dtch    (SkAsmProgram *p, SkAsmCodeLine *l);
};

#endif

#endif // SKASMISA_H
