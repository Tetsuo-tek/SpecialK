#include "skasmprogram.h"
#include "skasm.h"
#include "Core/App/skapp.h"
#include "skasmcodeline.h"

#if defined(ENABLE_SKASM)

SkAsmProgram::SkAsmProgram(CStr *prgName, SkAsm *engine)
{
    loopActive = false;
    pc = 0;
    name = prgName;
    e = engine;
    isLibrary = false;

    setObjectName(e, name.c_str());
    lastExternMethodReturn.setObjectName(this, "ExtRetVal");

    codeLines.setObjectName(this, "CodeLines");
    contexts.getInternalVector().setObjectName(this, "Contexts");
    jumps.setObjectName(this, "Jumps");

    stack = new SkAsmStack;
    stack->setProgram(this, nullptr);

    heap = new SkAsmHeap;
    heap->setProgram(this);

    included.setObjectName(this, "Included");

    lastCalledFunction = nullptr;
    errorOccurred  = false;
    endRequested = false;
}

SkAsmProgram::~SkAsmProgram()
{
    if (!isLibrary)
    {
        delete stack;
        delete heap;
    }
}

bool SkAsmProgram::addExternalStructPointer(CStr *typeName, CStr *globalVarName, void *ptr)
{
    SkAsmHeapLoc *hLoc = heap->storeExternStructPointer(typeName, ptr);

    if (hLoc)
    {
        stack->def(globalVarName, hLoc);
        return true;
    }

    return false;
}

bool SkAsmProgram::addExternalClassPointer(CStr *typeName, CStr *globalVarName, void *ptr)
{
    SkAsmHeapLoc *hLoc = heap->storeExternClassPointer(typeName, ptr);

    if (hLoc)
    {
        stack->def(globalVarName, hLoc);
        return true;
    }

    return false;
}

void SkAsmProgram::setCounter(uint64_t index)
{
    pc = index;
    FlatPlusDebug("Setup program-counter (pc): " << pc);
}

void SkAsmProgram::addCodeLine(SkAsmCodeLine *codeLine)
{
    codeLines << codeLine;
    FlatPlusDebug("Parsing line: " << codeLine->getLineNumber());
}

void SkAsmProgram::addContext(SkAsmCodeContext *ctx)
{
    contexts[ctx->getName()] = ctx;
    FlatPlusDebug("Added Context [" << SkAsmCodeContext::ctxTypeName(ctx->getCtxType()) << "]: " << ctx->getName());
}

void SkAsmProgram::addJump(uint64_t lineNumber)
{
    jumps << lineNumber;
}

bool SkAsmProgram::load(CStr *filePath)
{
    SkFsUtils::fillPathInfo(filePath, pathInfo);

    if (!pathInfo.exists)
    {
        FlatError("File NOT found: " << filePath);
        return false;
    }

    codeFile = pathInfo.completeAbsolutePath;

    FlatPlusDebug("Acquiring code [size: " << SkFsUtils::size(codeFile.c_str()) << " B]: " << codeFile);

    SkString code;

    if (SkFsUtils::readTEXT(codeFile.c_str(), code))
        return fetch(code);

    return false;
}

void SkAsmProgram::setAsLibrary(SkAsmStack *externStack, SkAsmHeap *externHeap)
{
    delete stack;
    delete heap;

    stack = externStack;
    heap = externHeap;

    isLibrary = true;
    FlatPlusDebug("Inizialized as LIBRARY [size: " << SkFsUtils::size(codeFile.c_str()) << " B]: " << codeFile);
}

bool SkAsmProgram::fetch(SkString &code)
{
    SkStringList l;
    code.split("\n", l);

    FlatPlusDebug("Lines count: " << l.count());

    SkAsmCodeContext *lastCtx = nullptr;

    for(uint64_t i=0; i<l.count(); i++)
    {
        SkString &line = l[i];
        line.trim();

        if (!parseDirective(line))
            return false;

        bool isCTX = line.endsWith(":");
        bool isEndCTX = (line.startsWith("!") || line.startsWith("f!"));

        SkAsmCodeLine *codeLine = new SkAsmCodeLine(this, i, line.c_str());

        if (!codeLine->parse(lastCtx))
            return false;

        if (isCTX)
            lastCtx = codeLine->getContext();

        if (isEndCTX)
            lastCtx = nullptr;
    }

    return true;
}

bool SkAsmProgram::parseDirective(SkString &line)
{
    if (line.startsWith("@"))
    {
        SkString directive = &line.c_str()[1];
        line.clear();

        if (directive.startsWith("include:"))
            return include(directive);
    }

    return true;
}

bool SkAsmProgram::include(SkString &directive)
{
    SkStringList l;
    directive.split(":", l);

    if (l.count() != 2)
    {
        ASM_ERR(e, nullptr, SkAsmError::SKASM_DIRECTIVE_ERROR, "Including File-path is REQUIRED")
        return false;
    }

    else
    {
        l.last().trim();

        SkString includePath = SkFsUtils::adjustPathEndSeparator(pathInfo.absoluteParentPath.c_str());
        includePath.append(l.last());

        SkPathInfo pInfo;
        SkFsUtils::fillPathInfo(includePath.c_str(), pInfo);

        SkAsmProgram *includedPrg = new SkAsmProgram(pInfo.name.c_str(), e);
        FlatDebug("Including code: " << pInfo.completeAbsolutePath);

        if (includedPrg->load(pInfo.completeAbsolutePath.c_str()))
        {
            includedPrg->setAsLibrary(stack, heap);

            included << includedPrg;
            FlatPlusDebug("Code INCLUDED: " << pInfo.completeAbsolutePath);

            return true;
        }
    }

    return false;
}

bool SkAsmProgram::exec(bool withLoopEvent, SkVector<uint64_t> *breakPoints)
{
    useLoopEvent = withLoopEvent;
    pc = 0;

    SkAbstractIterator<SkAsmProgram *> *itr = included.iterator();

    while(itr->next())
    {
        FlatPlusDebug("Starting library: " << itr->item()->getName());
        AssertKiller(!itr->item()->exec(withLoopEvent));
    }

    delete itr;

    SkAsmCodeContext *main = nullptr;

    if (!contexts.contains("main"))
    {
        if (!isLibrary)
        {
            ASM_ERR(e, nullptr, SkAsmError::SKASM_MAIN_NOT_FOUND,
                    "Program MUST have a main label block")

            return false;
        }
    }

    else
        main = contexts["main"];

    if (contexts.contains("data"))
    {}

    if (!isLibrary)
    {
        addExternalClassPointer("SkApp", "skApp", skApp);
        addExternalClassPointer("SkLogMachine", "logger", logger);
        addExternalClassPointer("SkAsm", "engine", e);
        //addExternalObjectPointer("SkAsmProgram", "program", this);
    }

    if (main)
        pc = main->getStartLineNumber();
    else
        pc = 1;

    errorOccurred = false;
    endRequested = false;
    loopActive = true;

    FlatDebug("Program started [main at line " << pc+1 << "]");

    while(pc && !errorOccurred && !endRequested)
    {
        tick();

        if (breakPoints && pc>0 && breakPoints->contains(pc-1))
            break;
    }

    loopActive = false;

    if (errorOccurred)
    {
        withLoopEvent = false;
        FlatError("Program terminated with errors");
    }

    if (useLoopEvent)
    {
        if (main)
            setCounter(main->getEndLineNumber());
        else
            pc = 1;

        FlatDebug("EventLoop is ENABLED; pc is at the endOfCTX [line: " << main->getEndLineNumber() << "] of 'main' now ..");
    }

    else
    {
        FlatDebug("Program successfully terminated");
        stack->clear();
        e->shutdown();
    }

    return !errorOccurred;
}

bool SkAsmProgram::tick()
{
    bool isTheEddOfCTX = false;
    uint64_t tempPC = pc;

    SkAsmCodeLine *l = codeLines[pc];

    if (l->getInstruction())
    {
        FlatPlusDebug("[" << l->getLineNumber() << "] Running instr-line: " << l->getInstruction()->name);

        if (!l->getInstruction()->exec(this, l) || errorOccurred)
        {}
    }

    else
    {
        if (l->getContext())
        {
            if (pc == l->getContext()->getStartLineNumber())
                FlatPlusDebug("[" << l->getLineNumber() << "] Entering start-ctx: " << l->getContext()->getName());

            else if (pc == l->getContext()->getEndLineNumber())
            {
                endOfContextReached(l);
                isTheEddOfCTX =  true;
            }
        }
    }

    if (pc == tempPC)
        pc++;

    return isTheEddOfCTX;
}

static SkVariant vNULL;

bool SkAsmProgram::setVar(bool definingIt, SkVariant &operand, SkVariant &val, SkAsmHeapLoc *loc)
{
    if (operand.isEmpty())
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_SYNTAX_ERROR,
                "Cannot set variable because operand is empty")

        return false;
    }

    SkAsmIdentifier id(this);

    if (!id.evaluateIdentifier(operand.data()))
        return false;

    if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_FUNCTRET_VAR_ID)
    {
        static_cast<SkAsmFunctionCtx *>(id.getLine()->getContext())->getReturnVar().setVal(val);
        return true;
    }

    //IN THE STACK
    if (definingIt)
    {
        if(loc)
            return id.getStack()->def(id.getIdentifier(), loc);

        return id.getStack()->def(id.getIdentifier(), val);
    }

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_STRCTPROP_ID)
        return id.getStack()->setStructProperty(id.getHierarchy().first().c_str(), id.getHierarchy().last().c_str(), val);

    if(loc)
        return id.getStack()->set(id.getIdentifier(), loc);

    return id.getStack()->set(id.getIdentifier(), val);
}

SkVariant &SkAsmProgram::getVar(SkVariant &operand)
{
    if (operand.isEmpty())
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_SYNTAX_ERROR,
                "Cannot get variable because operand is empty")

        return vNULL;
    }

    //IT IS ALREADY A VAL (AN IMMEDIATE TRANSLATED ON FETCH PHASE)
    if (operand.data()[0] != '#')
        return operand;

    SkAsmIdentifier id(this);

    if (!id.evaluateIdentifier(operand.data()))
        return vNULL;

    if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_FUNCTRET_VAR_ID)
        return static_cast<SkAsmFunctionCtx *>(id.getLine()->getContext())->getReturnVar();

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_STRCTPROP_ID)
        return id.getStack()->getStructProperty(id.getHierarchy().first().c_str(), id.getHierarchy().last().c_str());

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_EXTERNCTX_ID)
    {}

    return id.getStack()->get(&id);
}

bool SkAsmProgram::delVar(SkVariant &operand)
{
    if (operand.isEmpty())
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_SYNTAX_ERROR,
                "Cannot delete variable because operand is empty")

        return false;
    }

    if (operand.data()[0] != '#')
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_SYNTAX_ERROR,
                "Cannot delete without an identifier")

        return false;
    }

    SkAsmIdentifier id(this);

    if (!id.evaluateIdentifier(operand.data()))
        return false;

    if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_FUNCTRET_VAR_ID)
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_INSTR_ERROR,
                "Cannot use the ret-val identifier here")

        return false;
    }

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_FUNCT_ID)
    {
        ASM_ERR(e, getCurrentLine(), SkAsmError::SKASM_INSTR_ERROR,
                "Cannot use SmartFunctionCalls here")

        return false;
    }

    return id.getStack()->udef(id.getIdentifier());
}

bool SkAsmProgram::compare(SkVariant &v1, SkVariant &v2, SkAsmCompOpers op)
{
    bool cmp = false;

    if (op == SkAsmCompOpers::SKASM_LT)
        cmp = (v1 < v2);

    else if (op == SkAsmCompOpers::SKASM_LE)
        cmp = (v1 <= v2);

    else if (op == SkAsmCompOpers::SKASM_GT)
        cmp = (v1 > v2);

    else if (op == SkAsmCompOpers::SKASM_GE)
        cmp = (v1 >= v2);

    else if (op == SkAsmCompOpers::SKASM_EQ)
        cmp = (v1 == v2);

    else if (op == SkAsmCompOpers::SKASM_NE)
        cmp = (v1 != v2);

    FlatPlusDebug("Compare [" << v1.toString() << SkAsmProgram::compareOperatorName(op) << v2.toString()
               << " is " << SkVariant::boolToString(cmp) << "]: "
               << getCurrentLine()->getInstruction()->name);

    return cmp;
}

bool SkAsmProgram::jump(CStr *target)
{
    SkAsmCodeLine *l = getCurrentLine();
    SkAsmCodeContext *ctx = l->getContext();
    SkAsmCodeContext *targetContext = nullptr;

    if (target[0] == '!')
    {
        targetContext = ctx;
        FlatPlusDebug("Jumping to the procedure-end: " << ctx->getName());
        setCounter(ctx->getEndLineNumber());
    }

    else if (memcmp(target, "f!", 2) == 0)
    {
        targetContext = ctx;
        FlatPlusDebug("Jumping to the function-end: " << ctx->getName());
        setCounter(ctx->getEndLineNumber());
    }

    else if (ctx && ctx->containsLabel(target))
    {
        FlatPlusDebug("Jumping to the label [" << ctx->getName() << "]: " << target);
        setCounter(ctx->getLabelLineNumber(target));
    }

    else
    {
        targetContext = getContext(target);

        if (!targetContext)
        {
            ASM_ERR(e, l, SkAsmError::SKASM_CTX_NOT_FOUND, target)
            return false;
        }

        uint64_t newPos = targetContext->getStartLineNumber();

        if (targetContext != l->getContext())
        {
            if (ctx)
                FlatPlusDebug("Jumping to another context: " << ctx->getName() << " -> " << target);

            else
                FlatPlusDebug("Jumping to another context: StandBy -> " << target);

            addJump(getCounter() + 1);
        }

        else
            FlatPlusDebug("Jumping to the same context: " << target);

        setCounter(newPos);
    }

    return true;
}

bool SkAsmProgram::jumpByCondition(SkVariant &v1,
                                   SkVariant &v2,
                                   SkAsmCompOpers op,
                                   CStr *true_proc,
                                   CStr *false_proc)
{
    CStr *procedures[2] = {false_proc, true_proc};
    bool operandPosition = compare(v1, v2, op);

    if (!operandPosition && !false_proc)
        return true;

    if (procedures[operandPosition][0] == '!')
        return jump("!");

    return jump(&procedures[operandPosition][1]);
}

void SkAsmProgram::endOfContextReached(SkAsmCodeLine *l)
{
    SkAsmCodeContext *ctx = l->getContext();
    SkAsmContextType t = ctx->getCtxType();

    if (t == SkAsmContextType::SKASM_FUNCT || t == SkAsmContextType::SKASM_SLOT)
    {
        FlatPlusDebug("Clearing CTX-stack: " << ctx->getName());

        if (t == SkAsmContextType::SKASM_FUNCT)
            dynamic_cast<SkAsmFunctionCtx *>(ctx)->getStack()->clear();

        else if (t == SkAsmContextType::SKASM_SLOT)
            dynamic_cast<SkAsmSlotCtx *>(ctx)->getStack()->clear();

    }

    if (jumps.isEmpty())
    {
        FlatPlusDebug("[" << l->getLineNumber() << "] Entering end-ctx of the flow: " << ctx->getName());

        //THE PROGRAM FLOW IS STOPPED HERE
        //IT IS WAITNG FOR NEXT EXECUTION OR SPECIFIC CALL
        pc = 0;
    }

    else
    {
        uint64_t lastJump = jumps.last();
        jumps.removeLast();
        pc = lastJump;

        SkAsmCodeContext *returningToCtx = codeLines[lastJump]->getContext();

        if (returningToCtx)
        {
            if (returningToCtx == ctx)
            {
                FlatPlusDebug("[" << l->getLineNumber() << "] Return-to-theSame (" << lastJump+1 << ") from end-ctx: "
                           << ctx->getName() << " -> TheSame");

                pc--;
            }
            else
                FlatPlusDebug("[" << l->getLineNumber() << "] Return-to-orgin (" << lastJump+1 << ") from end-ctx: "
                           << ctx->getName() << " -> " << returningToCtx->getName());
        }

        else
            FlatPlusDebug("[" << l->getLineNumber() << "] Return-to-standBy (" << lastJump+1 << ") from end-ctx: "
                       << ctx->getName() << " -> StandBy");
    }
}

bool SkAsmProgram::call(CStr *identifier, SkVariant *params, uint64_t paramsCount)
{
    lastCalledFunction = nullptr;
    lastExternMethodReturn.nullify();

    SkAsmIdentifier id(this);

    if (!id.evaluateIdentifier(identifier))
        return false;

    if (id.getHierarchy().count() == 1)
        return callFunctionContext(&identifier[1], params, paramsCount);

    //MUST ADD CALL FOR SLOTs
    else if (id.getHierarchy().count() == 2)
        return callClassMethod(id, params, paramsCount);

    return false;
}

bool SkAsmProgram::callFunctionContext(SkAsmCodeContext *ctx, SkVariant *params, uint64_t paramsCount)
{
    if (!ctx)
    {
        FlatError("Function CTX is NOT valid");
        return false;
    }

    SkAsmCodeLine *l = getCurrentLine();
    CStr *functName = ctx->getName();
    SkAsmContextType t = ctx->getCtxType();

    if (t == SKASM_PRC)
    {
        ASM_ERR(e, l, SkAsmError::SKASM_CTX_IS_NOT_FUNCTION, functName)
        return false;
    }

    SkAsmAbstractFunction *f = static_cast<SkAsmAbstractFunction *>(ctx);

    {
        SkStringList argsTypesList;

        if (t == SkAsmContextType::SKASM_FUNCT)
        {
            if (f->getParamsCount() > paramsCount)
            {
                ASM_ERR(e, l, SkAsmError::SKASM_CTX_FUNCT_TOO_FEW_PARAMS, functName)
                return false;
            }

            for(uint64_t i=0; i<f->getParamsCount(); i++)
            {
                SkVariant &val = getVar(params[i]);
                f->getStack()->def(f->getParamName(i), val);
                argsTypesList << val.variantTypeName();
            }
        }

        else if (t == SkAsmContextType::SKASM_SLOT)
        {
            f->getStack()->def("referer", params[0]);
            argsTypesList << params[0].variantTypeName();

            for(uint64_t i=1; i<paramsCount; i++)
            {
                SkString s = SkString::number(i);
                f->getStack()->def(s.c_str(), params[i]);
                argsTypesList << params[i].variantTypeName();
            }
        }

        if (!paramsCount)
            FlatPlusDebug("Calling '" << functName << "' [" << SkAsmCodeContext::ctxTypeName(t) << "] WITHOUT  parameters");
        else
            FlatPlusDebug("Calling '" << functName << "' [" << SkAsmCodeContext::ctxTypeName(t) << "] WITH " << paramsCount << " parameters: " << argsTypesList.join(", "));
    }

    jump(functName);

    if (!loopActive)
    {
        FlatPlusDebug("Starting internal LOOP to execute '" << functName << "' [" << SkAsmCodeContext::ctxTypeName(t) << "] out of exec-flow");
        while(!tick());
        FlatPlusDebug("Internal LOOP to execute '" << functName << "' [" << SkAsmCodeContext::ctxTypeName(t) << "] out of exec-flow TERMINATED");
    }

    lastCalledFunction = f;
    return true;
}

bool SkAsmProgram::callFunctionContext(CStr *functName, SkVariant *params, uint64_t paramsCount)
{
    SkAsmCodeContext *ctx = getContext(functName);

    if (!ctx && !included.isEmpty())
    {
        SkAbstractIterator<SkAsmProgram *> *itr = included.iterator();
        SkAsmProgram *p = nullptr;

        while(itr->next())
        {
            p = itr->item();
            ctx = p->getContext(functName);

            if (ctx)
                break;
        }

        delete itr;

        if (ctx)
        {
            FlatPlusDebug("Calling '" << functName << "' function from another program (inclusion): " << p->getName() << "::" << functName);

            lastCalledFunction = static_cast<SkAsmAbstractFunction *>(ctx);
            return p->callFunctionContext(ctx, params, paramsCount);
        }

        ASM_ERR(e, nullptr, SkAsmError::SKASM_CTX_NOT_FOUND, functName)
        return false;
    }

    return callFunctionContext(ctx, params, paramsCount);
}

bool SkAsmProgram::callClassMethod(SkAsmIdentifier &id, SkVariant *params, uint64_t paramsCount)
{
    lastExternMethodReturn.nullify();
    CStr *objIdentifier = id.getHierarchy().first().c_str();

    bool ok = false;
    SkVariant **p = nullptr;
    SkVariant_T *p_T = nullptr;
    SkAsmHeapLoc **heapLocs = nullptr;

    CStr *methName = id.getHierarchy().last().c_str();

    if (!paramsCount)
        FlatPlusDebug("Calling method [" << methName << "] WITHOUT parameters");

    else
    {
        p = new SkVariant * [paramsCount];
        p_T = new SkVariant_T [paramsCount];
        heapLocs = new SkAsmHeapLoc * [paramsCount];

        SkStringList l;

        for(uint64_t i=0; i<paramsCount; i++)
        {
            p[i] = &getVar(params[i]);
            p_T[i] = p[i]->variantType();

            if (p_T[i] == SkVariant_T::T_RAWPOINTER)
                heapLocs[i] = static_cast<SkAsmHeapLoc *>(p[i]->toVoid());
            else
                heapLocs[i] = nullptr;

            l << p[i]->variantTypeName();
        }

        FlatPlusDebug("Calling method [" << methName << "] WITH " << paramsCount << " parameters: " << l.join(", "));
    }

    if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_STCMETH_ID)
        ok = callStaticMethod(id.getHierarchy().first().c_str(),
                              methName,
                              p, p_T, paramsCount,
                              &lastExternMethodReturn);

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_OBJMETH_ID)
    {
        SkAsmHeapLoc *hLoc = id.getStack()->getStackLocation(objIdentifier)->heapLoc;

        if (hLoc)
        {
            SkFlatObject *o = static_cast<SkFlatObject *>(hLoc->ptr);

            if (o->isFlatObject())
                ok = callInstanceMethod(o,
                                        methName,
                                        p, p_T, paramsCount,
                                        &lastExternMethodReturn);

            else
            {
                SkObject *o = static_cast<SkObject *>(hLoc->ptr);
                SkSignal *sig = o->getSignal(methName);

                if (sig)
                {
                    if (paramsCount)
                    {
                        SkVariantVector vct;

                        for(uint64_t i=0; i<paramsCount; i++)
                            vct << *p[i];

                        sig->setParameters(vct);
                    }

                    sig->trigger();

                    delete [] p;
                    delete [] p_T;
                    delete [] heapLocs;
                    return true;
                }

                else
                    ok = callInstanceMethod(o,
                                            methName,
                                            p, p_T, paramsCount,
                                            &lastExternMethodReturn);
            }
        }
    }

    else if (id.getIdentifierType() == SkAsmIdentifierType::SKASM_VARMEMBER_ID)
    {
        // USING SkVariant variable AS
        ok = callInstanceMethod(&id.getStack()->get(&id),
                                methName,
                                p, p_T, paramsCount,
                                &lastExternMethodReturn);
    }

    if (ok && paramsCount)
    {
        FlatPlusDebug("Checking Referenced-parameters changed from method: " << methName);

        for(uint64_t i=0; i<paramsCount; i++)
        {
            if (p_T[i] == SkVariant_T::T_RAWPOINTER)
            {
                if (heapLocs[i]->loc_T == SkAsmHeapLocType::STRUCT_PTR)
                {
                    SkStructType *t = static_cast<SkStructType *>(heapLocs[i]->t);
                    t->toMap(heapLocs[i]->ptr, heapLocs[i]->structProiection);
                    FlatPlusDebug("Updated STRUCT-PROIECTION-MAP [toMap()]: " << params[i]);
                }
            }

            else if (p_T[i] == SkVariant_T::T_STRING)
                p[i]->updateOnFromStringRef();
        }
    }

    delete [] p;
    delete [] p_T;
    delete [] heapLocs;

    if (!lastExternMethodReturn.isNull())
    {
        FlatPlusDebug("Grabbed method Return value [" << lastExternMethodReturn.variantTypeName() << "]"
                   << ", with real-type: " << lastExternMethodReturn.getOriginalRealType());
    }

    return ok;
}

bool SkAsmProgram::attach(CStr *identifier, CStr *slotName, CStr *mode)
{
    SkAsmIdentifier id(this);

    if (!id.evaluateIdentifier(identifier))
        return false;

    //MUST ADD CALL FOR SLOTs
    else if (id.getHierarchy().count() == 2)
    {
        if (id.getIdentifierType() != SkAsmIdentifierType::SKASM_OBJMETH_ID)
        {
            FlatError("Cannot attach, because identifier is NOT a Signal: " << identifier
                      << " [" << SkAsmIdentifier::identifierTypeName(SkAsmIdentifierType::SKASM_OBJMETH_ID) << "]");
            return false;
        }

        CStr *objIdentifier = id.getHierarchy().first().c_str();
        SkAsmHeapLoc *hLoc = id.getStack()->getStackLocation(objIdentifier)->heapLoc;

        if (hLoc)
        {
            SkFlatObject *o = static_cast<SkFlatObject *>(hLoc->ptr);

            if (o->isFlatObject())
            {
                FlatError("Object is FlatObject anc CANNOT have Signals: " << objIdentifier << " [" << o->typeName()<< "]");
                return false;
            }

            else
            {
                SkObject *o = static_cast<SkObject *>(hLoc->ptr);
                CStr *sigName = id.getHierarchy()[1].c_str();
                SkSignal *sig = o->getSignal(sigName);

                if (sig)
                {
                    if (!contexts.contains(&slotName[1]))
                    {
                        FlatError("Signal NOT found: " << slotName);
                        return false;
                    }

                    if (contexts[&slotName[1]]->getCtxType() != SkAsmContextType::SKASM_SLOT)
                    {
                        FlatError("Cannot attach, because identifier is NOT a Slot: " << slotName);
                        return false;
                    }

                    SkAsmSlotCtx *sltCtx = static_cast<SkAsmSlotCtx *>(contexts[&slotName[1]]);
                    SkEnumType *enumeration = searchEnumType("SkAttachMode");

                    if (!enumeration)
                    {
                        FlatError("Cannot find system enumeration: SkAttachMode");
                        KillApp();
                    }

                    SkArgsMap &map = *enumeration->enumeration;

                    if (map.contains(&mode[0]))
                        sig->attach(o, sltCtx->getAsmSlot(), static_cast<SkAttachMode>(map[&mode[0]].toInt()), __PRETTY_FUNCTION__);

                    else
                    {
                        FlatError("SkAttachMode NOT found: " << mode);
                        return false;
                    }
                }

                else
                {
                    FlatError("Signal NOT found: " << sigName);
                    return false;
                }
            }
        }
    }

    return true;
}

bool SkAsmProgram::attach(CStr *identifier, SkObject *target, CStr *slotName, CStr *mode)
{
    return true;
}

bool SkAsmProgram::detach(CStr *identifier, CStr *slotName)
{
    return true;
}

bool SkAsmProgram::detach(CStr *identifier, SkObject *target, CStr *slotName)
{
    return true;
}

bool SkAsmProgram::containsContext(CStr *name)
{
    return contexts.contains(name);
}

SkAsmCodeContext *SkAsmProgram::getContext(CStr *name)
{
    SkAsmCodeContext *ctx = nullptr;
    SkAbstractIterator<SkPair<SkString, SkAsmCodeContext *>> *itr = contexts.iterator();

    while(itr->next())
        if (itr->item().key().equals(name))
        {
            ctx = itr->item().value();
            break;
        }

    delete itr;

    return ctx;
}

CStr *SkAsmProgram::getName()
{
    return name.c_str();
}

uint64_t SkAsmProgram::getCounter()
{
    return pc;
}

SkAsmCodeLine *SkAsmProgram::getCurrentLine()
{
    return codeLines[pc];
}

SkAsmCodeLine *SkAsmProgram::getLine(uint64_t number)
{
    if (number > 0 && number <= codeLines.count())
        return codeLines[number-1];

    FlatError("Line NOT found: " << number);
    return nullptr;
}

SkAsm *SkAsmProgram::getEngine()
{
    return e;
}

SkList<SkAsmProgram *> &SkAsmProgram::getIncludedCode()
{
    return included;
}

SkAsmStack *SkAsmProgram::getStack()
{
    return stack;
}

SkAsmHeap *SkAsmProgram::getHeap()
{
    return heap;
}

SkAsmAbstractFunction *SkAsmProgram::getLastCalledFunction()
{
    return lastCalledFunction;
}

SkVariant &SkAsmProgram::getLastExternMethodReturn()
{
    return lastExternMethodReturn;
}


void SkAsmProgram::setEndRequest()
{
    endRequested = true;
}

bool SkAsmProgram::hasError()
{
    return errorOccurred;
}

CStr *SkAsmProgram::errorToString(SkAsmError e)
{
    if (e == SkAsmError::SKASM_DIRECTIVE_ERROR)
        return "WRONG directive usage";

    else if (e == SkAsmError::SKASM_WRONG_OP_NUM)
        return "WRONG operands number";

    else if (e == SkAsmError::SKASM_WRONG_OP_TYPE)
        return "WRONG operands type";

    else if (e == SkAsmError::SKASM_TYPE_NOT_FOUND)
        return "Type NOT found";

    else if (e == SkAsmError::SKASM_VAR_NOT_FOUND)
        return "Variable NOT found";

    else if (e == SkAsmError::SKASM_VAR_ALREADY_EXISTS)
        return "Variable ALREADY exists";

    else if (e == SkAsmError::SKASM_HEAPLOC_NOT_FOUND)
        return "HeapLocation NOT found";

    else if (e == SkAsmError::SKASM_MAIN_NOT_FOUND)
        return "Main-procedure NOT found";

    else if (e == SkAsmError::SKASM_CTX_NOT_FOUND)
        return "Context NOT found";

    else if (e == SkAsmError::SKASM_CTX_INSIDE_CTX)
        return "Context INSIDE Context";

    else if (e == SkAsmError::SKASM_CTX_IS_NOT_FUNCTION)
        return "Context is NOT a Function";

    else if (e == SkAsmError::SKASM_CTX_RETVAL_FROM_SLOT)
        return "Cannot get retVal from Slot Contexts";

    else if (e == SkAsmError::SKASM_CTX_FUNCT_TOO_FEW_PARAMS)
        return "Function requires more input parameters";

    else if (e == SkAsmError::SKASM_CTX_FUNCT_IS_NOT_CALLED)
        return "Function is NOT called yet";

    else if (e == SkAsmError::SKASM_CTX_END_WITHOUT_START)
        return "Context-end WITHOUT Context-start";

    else if (e == SkAsmError::SKASM_LABEL_WITHOUT_CONTAINER)
        return "Label WITHOUT context";

    else if (e == SkAsmError::SKASM_INSTR_UNKNOWN)
        return "Instruction UNKNOWN";

    else if (e == SkAsmError::SKASM_INSTR_WITHOUT_CTX)
        return "Instruction WITHOUT Context";

    else if (e == SkAsmError::SKASM_INSTR_ERROR)
        return "Semantic issue";

    else if (e == SkAsmError::SKASM_SYNTAX_ERROR)
        return "Syntax error";

    return "NULL";
}

CStr *SkAsmProgram::compareOperatorName(SkAsmCompOpers op)
{
    if (op == SkAsmCompOpers::SKASM_LT)
        return "<";

    else if (op == SkAsmCompOpers::SKASM_LE)
        return "<=";

    else if (op == SkAsmCompOpers::SKASM_GT)
        return ">";

    else if (op == SkAsmCompOpers::SKASM_GE)
        return ">=";

    else if (op == SkAsmCompOpers::SKASM_EQ)
        return "==";

    return "";
}

CStr *SkAsmProgram::arithmeticOperatorName(SkAsmArithOpers op)
{
    if (op == SkAsmArithOpers::SKASM_SUM)
        return "+";

    else if (op == SkAsmArithOpers::SKASM_SUB || op == SkAsmArithOpers::SKASM_NEG)
        return "-";

    else if (op == SkAsmArithOpers::SKASM_MUL)
        return "*";

    else if (op == SkAsmArithOpers::SKASM_DIV)
        return "/";

    else if (op == SkAsmArithOpers::SKASM_REM)
        return "%";

    else if (op == SkAsmArithOpers::SKASM_POW)
        return "^";

    else if (op == SkAsmArithOpers::SKASM_INC)
        return "+=";

    else if (op == SkAsmArithOpers::SKASM_DEC)
        return "-=";

    return "";
}

#endif
