﻿#include "skasm.h"
#include "skasmcodeline.h"
#include "Core/App/skeventloop.h"

#if defined(ENABLE_SKASM)

static SkAsmISA skAsmISA;

DeclareMeth_INSTANCE_VOID(SkAsm, enableEventLoopTicks)
DeclareMeth_INSTANCE_VOID(SkAsm, disableEventLoopTicks)

ConstructorImpl(SkAsm, SkObject,
                {
                    AddMeth_INSTANCE_VOID(SkAsm, enableEventLoopTicks);
                    AddMeth_INSTANCE_VOID(SkAsm, disableEventLoopTicks);
                })
{
    evtLoopTicksEnabled = false;
    prg = nullptr;

    immediatePrimitiveTypes << "U8:";
    immediatePrimitiveTypes << "S8:";
    immediatePrimitiveTypes << "U16:";
    immediatePrimitiveTypes << "S16:";
    immediatePrimitiveTypes << "U32:";
    immediatePrimitiveTypes << "S32:";
    immediatePrimitiveTypes << "U64:";
    immediatePrimitiveTypes << "S64:";
    immediatePrimitiveTypes << "SSIZE:";
    immediatePrimitiveTypes << "USIZE:";
    immediatePrimitiveTypes << "F:";
    immediatePrimitiveTypes << "D:";

    SignalSet(error);
    SlotSet(custom);

    SlotSet(fastTickProxy);
    SignalSet(fastTick);
    SlotSet(slowTickProxy);
    SignalSet(slowTick);
    SlotSet(oneSecTickProxy);
    SignalSet(oneSecTick);
}

SkStringList &SkAsm::getImmediatePrimitiveTypes()
{
    return immediatePrimitiveTypes;
}

bool SkAsm::load(CStr *filePath, CStr *name)
{
    if (prg)
    {
        ObjectError("Program is ALREADY acquired");
        return false;
    }

    prg = new SkAsmProgram(name, this);
    return prg->load(filePath);
}

void SkAsm::enableEventLoopTicks()
{
    if (evtLoopTicksEnabled)
    {
        ObjectError("EventLoopTicks ALREADY active");
        return;
    }

    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTickProxy, SkAttachMode::SkDirect);
    Attach(eventLoop()->slowZone_SIG, pulse, this, slowTickProxy, SkAttachMode::SkDirect);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTickProxy, SkAttachMode::SkDirect);

    evtLoopTicksEnabled = true;
}

void SkAsm::disableEventLoopTicks()
{
    if (!evtLoopTicksEnabled)
    {
        ObjectError("EventLoopTicks ALREADY disabled");
        return;
    }

    Detach(eventLoop()->fastZone_SIG, pulse, this, fastTickProxy);
    Detach(eventLoop()->slowZone_SIG, pulse, this, slowTickProxy);
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecTickProxy);

    evtLoopTicksEnabled = false;
}

bool SkAsm::exec(bool withLoopEvent, SkVector<uint64_t> *breakPoints)
{
    if (!prg)
    {
        ObjectError("Program is NOT acquired");
        return false;
    }

    return prg->exec(withLoopEvent, breakPoints);
}

bool SkAsm::addExternalStructPointer(CStr *typeName, CStr *globalVarName, void *ptr)
{
    return prg->addExternalStructPointer(typeName, globalVarName, ptr);
}

bool SkAsm::addExternalClassPointer(CStr *typeName, CStr *globalVarName, void *ptr)
{
    return prg->addExternalClassPointer(typeName, globalVarName, ptr);
}

SlotImpl(SkAsm, custom){}
SlotImpl(SkAsm, fastTickProxy){fastTick();}
SlotImpl(SkAsm, slowTickProxy){slowTick();}
SlotImpl(SkAsm, oneSecTickProxy){oneSecTick();}

bool SkAsm::shutdown()
{
    ObjectDebug("Closing program ..");
    //MUST DESTROY ALL

    return false;
}

SkAsmInstr *SkAsm::isa(uint64_t &count)
{
    return skAsmISA.getInstructions(count);
}

void SkAsm::error(SkAsmCodeLine *line, SkAsmError e, CStr *desc)
{
    SkVector<SkVariant> l;
    l << e;

    if (line)
    {
        l << static_cast<uint64_t>(line->getLineNumber());
        l << line->getLine();

        SkAsmCodeContext *ctx = line->getContext();

        if (ctx)
        {
            SkAsmInstr *instr = line->getInstruction();

            if (instr)
                FlatError_EXT(line->getProgram(), "[L: " << line->getLineNumber() << "; I: " << instr->name << "; CTX: " << ctx->getName() << "] "
                                << SkAsmProgram::errorToString(e) << "(" << e << ") " << desc);
            else
                FlatError_EXT(line->getProgram(), "[L: " << line->getLineNumber() << "; I: null; CTX: " << ctx->getName() << "] "
                                << SkAsmProgram::errorToString(e) << "(" << e << ") " << desc);
        }

        else
            FlatError_EXT(line->getProgram(), "[L: " << line->getLineNumber() << "; I: null; CTX: null] "
                            << SkAsmProgram::errorToString(e) << "(" << e << ")" << desc);

    }

    else
    {
        l << -1;
        l << "";
        FlatError_EXT(line->getProgram(), "[L: null; I: null; CTX: null] " << SkAsmProgram::errorToString(e) << "(" << e << ") " << desc);
    }

    prg->setCounter(0);
    prg->errorOccurred = true;

    SkVariantVector p;
    p << l;
    error(p);
}

#endif
