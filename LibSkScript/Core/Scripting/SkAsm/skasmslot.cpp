#include "skasmslot.h"
#include "skasm.h"

#if defined(ENABLE_SKASM)

SkAsmSlot::SkAsmSlot()
{
    prg = nullptr;
    ctx = nullptr;
}

void SkAsmSlot::setProgram(SkAsmProgram *p)
{
    prg = p;
}

void SkAsmSlot::setContext(SkAsmSlotCtx *slotFunction)
{
    ctx = slotFunction;

    method = *prg->getEngine()->custom_SLOT->getFlatMethod();
    method.name = ctx->getName();
    setMethod(&method);

    setObjectName(prg, method.name);
    addSlot(this);

    FlatPlusDebug("SkAsmSlot initialized");
}

void SkAsmSlot::onCall(SkObject *, SkFlatObject *referer, SkVariant **p, uint64_t pCount)
{
    SkVariant *params = new SkVariant [pCount+1];
    params[0] = referer;

    SkStringList l;

    for(uint64_t i=1; i<pCount+1; i++)
    {
        params[i].setVal(*p[i-1]);
        l << params[i].variantTypeName();
    }

    SkString identifier("#");
    identifier.append(ctx->getName());

    prg->call(identifier.c_str(), params, pCount);

    delete [] params;
}

#endif
