#ifndef SKASMSLOT_H
#define SKASMSLOT_H

#include "Core/Object/skslot.h"

#if defined(ENABLE_SKASM)

class SkAsmProgram;
class SkAsmSlotCtx;

class SkAsmSlot extends SkSlot
{
    public:
        SkAsmSlot();

        void setProgram(SkAsmProgram *p);
        void setContext(SkAsmSlotCtx *slotFunction);

        virtual bool isCanonical(){return false;}

    private:
        SkMethod method;
        SkAsmProgram *prg;
        SkAsmSlotCtx *ctx;
        void onCall(SkObject *owner, SkFlatObject *referer, SkVariant **p, uint64_t pCount);
};

#endif

#endif // SKASMSLOT_H
