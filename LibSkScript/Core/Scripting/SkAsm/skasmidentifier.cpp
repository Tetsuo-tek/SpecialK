#include "skasmidentifier.h"
#include "skasmcodeline.h"

#if defined(ENABLE_SKASM)

#include "skasm.h"

SkAsmIdentifier::SkAsmIdentifier(SkAsmProgram *p)
{
    prg = p;
    includedProgram = nullptr;
    t = SkAsmIdentifierType::SKASM_NO_ID;
    line = nullptr;
    stack = nullptr;
    isLocal = false;

    setObjectName(prg, "Identifier");
}

bool SkAsmIdentifier::evaluateIdentifier(CStr *idStr)
{
    FlatPlusDebug("Evaluating identifier: " << idStr);

    if (prg->getCounter() > 0)
        line = prg->getLine(prg->getCounter());

    else
        line = nullptr;

    if (!idStr || strlen(idStr) <= 1 || idStr[0] != '#')
    {
        if (idStr)
            ASM_ERR(prg->getEngine(), line, SkAsmError::SKASM_SYNTAX_ERROR,
                    "Value identifier is NOT valid: " << idStr)
        else
            ASM_ERR(prg->getEngine(), line, SkAsmError::SKASM_SYNTAX_ERROR,
                    "Value identifier string is NULL")

        return false;
    }

    CStr *s = &idStr[1];

    if (memcmp(s, "this.", 5) == 0)
    {
        //THIS IS SOME VALUE FROM FUNCTION STACK
        if (!line || !checkThisInsideFunctCtx(line))
            return false;

        t = SkAsmIdentifierType::SKASM_LOCAL_VAR_ID;
        pfx = "this";
        identifier = &s[5];
        stack = static_cast<SkAsmFunctionCtx *>(line->getContext())->getStack();
        isLocal = true;
    }

    else if (memcmp(s, "this", 4) == 0)
    {
        //THIS IS THE RETURN-VALUE OF A FUNCTION
        if (!checkThisInsideFunctCtx(line))
            return false;

        t = SkAsmIdentifierType::SKASM_FUNCTRET_VAR_ID;
        pfx = "this";
        identifier = "";
        stack = nullptr;
        isLocal = true;

        FlatPlusDebug("Identifier evaluated [" << SkAsmIdentifier::identifierTypeName(t) << "]: " << pfx << "." << identifier
                   << " [CTX: " << line->getContext()->getName() << "; Hier: " << hierarchy.join("/") << "]");

        return true;
    }

    else
    {
        //MUST CHECK IF THER IS PREFIX ABOUT SKOBJECT REFERENCED FROM HEAP

        t = SkAsmIdentifierType::SKASM_GLOBAL_VAR_ID;
        pfx = "";
        identifier = s;
        stack = prg->getStack();
        isLocal = false;
    }

    return evaluateIdentifierStructure();
}

bool SkAsmIdentifier::evaluateIdentifierStructure()
{
    uint64_t sz = identifier.size();
    CStr *idStr = identifier.c_str();

    for(uint64_t i=1, from=0; i<=sz; i++)
    {
        if (i == sz || idStr[i] == '.' || idStr[i] == '@' || idStr[i] == '#')
        {
            SkString str((char *) &idStr[from], i-from);
            from = i+1;

            hierarchy << str;

            if (idStr[i] == '@' || idStr[i] == '#')
            {
                if (i<sz-1)
                {
                    str.clear();
                    str.append((char *) &idStr[from-1], sz-from+1);
                    hierarchy << str;
                }

                break;
            }
        }
    }

    //FlatPlusDebug("Identifier hierarchy: " << hierarchy.join("/"));

    if (hierarchy.count() == 1)
    {
        SkAsmCodeContext *ctx = prg->getContext(hierarchy.first().c_str());

        if (ctx)
        {
            if (ctx->getCtxType() == SkAsmContextType::SKASM_FUNCT)
                t = SkAsmIdentifierType::SKASM_FUNCT_ID;
        }

        else if (!prg->getIncludedCode().isEmpty())
        {
            SkList<SkAsmProgram *> &incl = prg->getIncludedCode();

            SkAbstractIterator<SkAsmProgram *> *itr = incl.iterator();

            while(itr->next())
                if (itr->item()->getContext(identifier.c_str()))
                {
                    includedProgram = itr->item();
                    t = SkAsmIdentifierType::SKASM_EXTERNCTX_ID;
                    break;
                }

            delete itr;
        }
    }

    else if (hierarchy.count() == 2)
    {
        SkAsmStackLoc *sLoc = stack->getStackLocation(hierarchy.first().c_str());

        if (sLoc && sLoc->heapLoc)
        {
            if (sLoc->heapLoc->loc_T == SkAsmHeapLocType::OBJECT_PTR)
                t = SkAsmIdentifierType::SKASM_OBJMETH_ID;

            else if (sLoc->heapLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
                t = SkAsmIdentifierType::SKASM_STRCTPROP_ID;

            //FlatPlusDebug("Identifier is referering to an heap pointer [" << SkAsmHeap::locTypeName(hLoc->loc_T) << "]: " << hierarchy.first());
        }

        // SURELY IS STATIC IF A CLASS-TYPE IS SHOWED AS FIRST OF HIER

        else if (classTypeID(hierarchy.first().c_str()) > -1)
            t = SkAsmIdentifierType::SKASM_STCMETH_ID;

        else
            t = SkAsmIdentifierType::SKASM_VARMEMBER_ID;
    }

    if (line)
    {
        if (pfx.isEmpty())
        {
            FlatPlusDebug("Identifier evaluated [" << SkAsmIdentifier::identifierTypeName(t) << "]: " << identifier
                       << " [CTX: " << line->getContext()->getName() << "; Hier: " << hierarchy.join("/") << "]");
        }

        else
        {
            FlatPlusDebug("Identifier evaluated [" << SkAsmIdentifier::identifierTypeName(t) << "]: " << pfx << "." << identifier
                       << " [CTX: " << line->getContext()->getName() << "; Hier: " << hierarchy.join("/") <<"]");
        }
    }

    else
        FlatPlusDebug("Identifier evaluated [" << SkAsmIdentifier::identifierTypeName(t) << "]: " << identifier
                   << " [CTX: NULL; Hier: " << hierarchy.join("/") <<"]");

    return true;
}

bool SkAsmIdentifier::checkThisInsideFunctCtx(SkAsmCodeLine *l)
{
    if (l->getContext()->getCtxType() != SKASM_FUNCT)
    {
        ASM_ERR(prg->getEngine(), l, SkAsmError::SKASM_CTX_IS_NOT_FUNCTION,
                "'this' identifier prefix can be used ONLY inside functions scope")

        return false;
    }

    return true;
}

SkAsmProgram *SkAsmIdentifier::getProgram()
{
    return prg;
}

SkAsmProgram *SkAsmIdentifier::getIncludedProgram()
{
    return includedProgram;
}

SkStringList &SkAsmIdentifier::getHierarchy()
{
    return hierarchy;
}

CStr *SkAsmIdentifier::getPrefix()
{
    return pfx.c_str();
}

CStr *SkAsmIdentifier::getIdentifier()
{
    return identifier.c_str();
}

SkAsmIdentifierType SkAsmIdentifier::getIdentifierType()
{
    return t;
}

SkAsmStack *SkAsmIdentifier::getStack()
{
    return stack;
}

SkAsmCodeLine *SkAsmIdentifier::getLine()
{
    return line;
}

CStr *SkAsmIdentifier::identifierTypeName(SkAsmIdentifierType t)
{
    if (t == SkAsmIdentifierType::SKASM_GLOBAL_VAR_ID)
        return "VAR_ID";

    else if (t == SkAsmIdentifierType::SKASM_LOCAL_VAR_ID)
        return "LOCAL_VAR_ID";

    else if (t == SkAsmIdentifierType::SKASM_FUNCT_ID)
        return "FUNCT_ID";

    else if (t == SkAsmIdentifierType::SKASM_FUNCTRET_VAR_ID)
        return "FUNCTRET_VAR_ID";

    else if (t == SkAsmIdentifierType::SKASM_STCMETH_ID)
        return "STCMETH_ID";

    else if (t == SkAsmIdentifierType::SKASM_OBJMETH_ID)
        return "OBJMETH_ID";

    else if (t == SkAsmIdentifierType::SKASM_VARMEMBER_ID)
        return "VARMEMBER_ID";

    else if (t == SkAsmIdentifierType::SKASM_STRCTPROP_ID)
        return "STRCTPROP_ID";

    else if (t == SkAsmIdentifierType::SKASM_EXTERNCTX_ID)
        return "EXTERNCTX_ID";

    return "NO_ID";
}

#endif
