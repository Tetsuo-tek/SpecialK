#include "skasmisa.h"
#include "skasm.h"
#include "skasmcodeline.h"

#if defined(ENABLE_SKASM)

SkAsmISA::SkAsmISA()
{
    instructions = nullptr;
    instrCount = 0;

    //THE LANGUAGE AND THE INSTRUCTIONs (ISA) ARE DESCRIBED ON skasm.txt

    addInstr("jmp",  1, 1, &jmp);
    addInstr("call", 1, 2, &call);
    addInstr("ret",  1, 1, &ret);
    addInstr("end",  0, 0, &end);

    addInstr("def",  1, 2, &def);
    addInstr("udef", 1, 1, &udef);
    addInstr("move", 2, 2, &move);
    addInstr("gglv", 2, 2, &gglv);
    addInstr("sglv", 2, 2, &sglv);

    addInstr("blt",  3, 4, &blt);
    addInstr("ble",  3, 4, &ble);
    addInstr("bgt",  3, 4, &bgt);
    addInstr("bge",  3, 4, &bge);
    addInstr("beq",  3, 4, &beq);
    addInstr("bne",  3, 4, &bne);

    addInstr("bltz", 2, 3, &bltz);
    addInstr("blez", 2, 3, &blez);
    addInstr("bgtz", 2, 3, &bgtz);
    addInstr("bgez", 2, 3, &bgez);
    addInstr("beqz", 2, 3, &beqz);
    addInstr("bnez", 2, 3, &bnez);

    addInstr("slt",  3, 3, &slt);
    addInstr("sle",  3, 3, &sle);
    addInstr("sgt",  3, 3, &sgt);
    addInstr("sge",  3, 3, &sge);
    addInstr("seq",  3, 3, &seq);
    addInstr("sne",  3, 3, &sne);

    addInstr("sltz", 2, 2, &sltz);
    addInstr("slez", 2, 2, &slez);
    addInstr("sgtz", 2, 2, &sgtz);
    addInstr("sgez", 2, 2, &sgez);
    addInstr("seqz", 2, 2, &seqz);
    addInstr("snez", 2, 2, &snez);

    addInstr("add",  3, 3, &add);
    addInstr("sub",  3, 3, &sub);
    addInstr("mul",  3, 3, &mul);
    addInstr("div",  3, 3, &div);
    addInstr("rem",  3, 3, &rem);
    addInstr("pow",  3, 3, &pow);
    addInstr("inc",  1, 2, &inc);
    addInstr("dec",  1, 2, &dec);
    addInstr("neg",  1, 1, &neg);

    addInstr("prt",  1, 255, &prt);
    addInstr("prtl", 1, 255, &prtl);

    addInstr("msg",  1, 1,   &msg);
    addInstr("wrn",  1, 1,   &wrn);
    addInstr("err",  1, 1,   &err);
    addInstr("dbg",  1, 1,   &dbg);
    addInstr("pdbg", 1, 1,   &pdbg);
    addInstr("sys",  1, 100, &sys);

    addInstr("mks",  2, 2,  &mks);
    addInstr("mko",  2, 2,  &mko);
    addInstr("atch", 3, 4,  &atch);
    addInstr("dtch", 3, 4,  &dtch);
}

void SkAsmISA::addInstr(CStr *name,
                        uint8_t reqOperandsCount,
                        uint8_t maxOperandsCount,
                        SkAsmInstrFunct *exec)
{
    uint64_t temporaryCount = instrCount + 1;
    instructions = static_cast<SkAsmInstr*>(realloc(instructions, temporaryCount * sizeof(SkAsmInstr)));

    instructions[instrCount].id = instrCount;
    instructions[instrCount].name = name;
    instructions[instrCount].nameLen = strlen(name);
    instructions[instrCount].reqOperandsCount = reqOperandsCount;
    instructions[instrCount].maxOperandsCount = maxOperandsCount;
    instructions[instrCount].exec = exec;

    instrCount = temporaryCount;
}

SkAsmInstr *SkAsmISA::getInstructions(uint64_t &count)
{
    count = instrCount;
    return instructions;
}

//INSTRUCTIONS

bool SkAsmISA::jmp(SkAsmProgram *p, SkAsmCodeLine *l)
{
    CStr *s = &l->getOperands().first().data()[1];
    return p->jump(s);
}

bool SkAsmISA::call(SkAsmProgram *p, SkAsmCodeLine *l)
{
    return p->call(l->getOperands().first().data(),
                   &l->getOperands().data()[1], l->getOperands().count()-1);
}

bool SkAsmISA::ret(SkAsmProgram *p, SkAsmCodeLine *l)
{
    SkAsmAbstractFunction *f = p->getLastCalledFunction();
    SkVariant &extRetVal = p->getLastExternMethodReturn();

    if (extRetVal.isNull() && !f)
    {
        ASM_ERR(p->getEngine(), l, SkAsmError::SKASM_CTX_FUNCT_IS_NOT_CALLED,
                    "Return for called function or object-method is NOT valid")

        return false;
    }

    SkVariant &var = p->getVar(l->getOperands()[0]);

    if (f)
    {
        if (f->getCtxType() == SkAsmContextType::SKASM_FUNCT)
        {
            SkAsmFunctionCtx *funct = dynamic_cast<SkAsmFunctionCtx *>(f);
            var.setVal(funct->getReturnVar());
            funct->getReturnVar().nullify();
        }

        if (f->getCtxType() == SkAsmContextType::SKASM_SLOT)
        {
            ASM_ERR(p->getEngine(), l, SkAsmError::SKASM_CTX_FUNCT_IS_NOT_CALLED,
                        "Return for called function or object-method is NOT valid")

            return false;
        }
    }

    else if (!p->getLastExternMethodReturn().isNull())
    {
        SkVariant &returnedValue = p->getLastExternMethodReturn();

        if (returnedValue.variantType() == SkVariant_T::T_RAWPOINTER)
        {
            SkString realType = returnedValue.getOriginalRealType();
            realType.trim();

            if (realType.endsWith("*") && realType.endsWith("&"))
            {
                realType.chop(1);
                realType.trim();

                SkAsmHeapLoc *hLoc = nullptr;

                if (searchStructType(realType.c_str()))
                    hLoc = p->getHeap()->storeExternStructPointer(realType.c_str(), returnedValue.toVoid());

                else if (searchClassType(realType.c_str()))
                    hLoc = p->getHeap()->storeExternClassPointer(realType.c_str(), returnedValue.toVoid());

                if (!hLoc)
                {
                    StaticError("Original return T_RAWPOINTER type UNKNOWN: " << returnedValue.getOriginalRealType());
                    KillApp();
                }

                returnedValue = hLoc;
                returnedValue.setOriginalRealType("");
            }

        }

        var.setVal(returnedValue);
        returnedValue.nullify();
    }

    return true;
}

bool SkAsmISA::end(SkAsmProgram *p, SkAsmCodeLine *)
{
    p->setCounter(0);
    p->setEndRequest();
    return true;
}

static SkVariant vNULL;

bool SkAsmISA::def(SkAsmProgram *p, SkAsmCodeLine *l)
{
    if (l->getOperands().count() == 1)
        return p->setVar(true, l->getOperands().first(), vNULL);


    return p->setVar(true, l->getOperands().first(), p->getVar(l->getOperands().last()));
}

bool SkAsmISA::udef(SkAsmProgram *p, SkAsmCodeLine *l)
{
    return p->delVar(l->getOperands().first());
}

bool SkAsmISA::move(SkAsmProgram *p, SkAsmCodeLine *l)
{
    if (l->getOperands().count() == 1)
        return p->setVar(false, l->getOperands().first(), vNULL);


    return p->setVar(false, l->getOperands().first(), p->getVar(l->getOperands().last()));
}

bool SkAsmISA::gglv(SkAsmProgram *, SkAsmCodeLine *)
{
    return false;
}

bool SkAsmISA::sglv(SkAsmProgram *, SkAsmCodeLine *)
{
    return false;
}


#define COMPARE_WITH_JUMP(OPERATOR, V2) \
    if (l->getOperands().count() == 4) \
        return p->jumpByCondition(p->getVar(l->getOperands()[0]), \
                                  V2, \
                                  OPERATOR, \
                                  l->getOperands()[2].data(), \
                                  l->getOperands()[3].data()); \
    else \
        return p->jumpByCondition(p->getVar(l->getOperands()[0]), \
                                  V2, \
                                  OPERATOR, \
                                  l->getOperands()[2].data(), \
                                  nullptr)

bool SkAsmISA::blt(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_LT, p->getVar(l->getOperands()[1]));
}

bool SkAsmISA::ble(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_LE, p->getVar(l->getOperands()[1]));
}

bool SkAsmISA::bgt(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_GT, p->getVar(l->getOperands()[1]));
}

bool SkAsmISA::bge(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_GE, p->getVar(l->getOperands()[1]));
}

bool SkAsmISA::beq(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_EQ, p->getVar(l->getOperands()[1]));
}

bool SkAsmISA::bne(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_NE, p->getVar(l->getOperands()[1]));
}

static SkVariant zero(0);

bool SkAsmISA::bltz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_LT, zero);
}

bool SkAsmISA::blez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_LE, zero);
}

bool SkAsmISA::bgtz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_GT, zero);
}

bool SkAsmISA::bgez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_GE, zero);
}

bool SkAsmISA::beqz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_EQ, zero);
}

bool SkAsmISA::bnez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    COMPARE_WITH_JUMP(SkAsmCompOpers::SKASM_NE, zero);
}

bool SkAsmISA::slt(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_LT));
    return true;
}

bool SkAsmISA::sle(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_LE));
    return true;
}

bool SkAsmISA::sgt(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_GT));
    return true;
}

bool SkAsmISA::sge(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_GE));
    return true;
}

bool SkAsmISA::seq(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_EQ));
    return true;
}

bool SkAsmISA::sne(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             p->getVar(l->getOperands()[1]),
                                             SkAsmCompOpers::SKASM_NE));
    return true;
}

bool SkAsmISA::sltz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_LT));
    return true;
}

bool SkAsmISA::slez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_LE));
    return true;
}

bool SkAsmISA::sgtz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_GT));
    return true;
}

bool SkAsmISA::sgez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_GE));
    return true;
}

bool SkAsmISA::seqz(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_EQ));
    return true;
}

bool SkAsmISA::snez(SkAsmProgram *p, SkAsmCodeLine *l)
{
    l->getOperands()[1].setVal(p->compare(p->getVar(l->getOperands()[0]),
                                             zero,
                                             SkAsmCompOpers::SKASM_NE));
    return true;
}

# define ArithmeticOperation(SYMBOL) \
    SkVariant &v1 = p->getVar(l->getOperands()[0]); \
    SkVariant &v2 = p->getVar(l->getOperands()[1]); \
    SkVariant &v3 = p->getVar(l->getOperands()[2]); \
    if (&v1 == &v3) \
        v1 SYMBOL ## = v2; \
    else \
        v3 = v1 SYMBOL v2;

bool SkAsmISA::add(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(+)
    return true;
}

bool SkAsmISA::sub(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(-)
    return true;
}

bool SkAsmISA::mul(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(*)
    return true;
}

bool SkAsmISA::div(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(/)
    return true;
}

bool SkAsmISA::rem(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(%)
    return true;
}

bool SkAsmISA::pow(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ArithmeticOperation(^)
    return true;
}

bool SkAsmISA::inc(SkAsmProgram *p, SkAsmCodeLine *l)
{
    if (l->getOperands().count() == 1)
        p->getVar(l->getOperands().first()) += 1;

    else
        p->getVar(l->getOperands()[0]) += p->getVar(l->getOperands()[1]);

    return true;
}

bool SkAsmISA::dec(SkAsmProgram *p, SkAsmCodeLine *l)
{
    if (l->getOperands().count() == 1)
        p->getVar(l->getOperands().first()) -= 1;

    else
        p->getVar(l->getOperands()[0]) -= p->getVar(l->getOperands()[1]);

    return true;
}

bool SkAsmISA::neg(SkAsmProgram *p, SkAsmCodeLine *l)
{
    p->getVar(l->getOperands().first()) *= -1;
    return true;
}

bool SkAsmISA::prt(SkAsmProgram *p, SkAsmCodeLine *l)
{
    SkStringList tList;
    stringstream s;

    SkVector<SkVariant> &params = l->getOperands();

    for(uint64_t i=0; i<params.count(); i++)
    {
        SkVariant &val = p->getVar(params[i]);
        s << val.toString();
        tList << val.variantTypeName();
    }

    if (params.isEmpty())
        ObjectPlusDebug_EXT(p->getEngine(), "Calling '" << l->getInstruction()->name << "' Instruction WITHOUT parameters");
    else
        ObjectPlusDebug_EXT(p->getEngine(), "Calling '" << l->getInstruction()->name << "' Instruction WITH " << params.count() << " parameters: " << tList.join(", "));

    cout << s.str();
    cout.flush();
    return true;
}

bool SkAsmISA::prtl(SkAsmProgram *p, SkAsmCodeLine *l)
{
    bool ok = SkAsmISA::prt(p, l);
    if (ok)
        cout << "\n";
    return ok;
}

bool SkAsmISA::msg(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ObjectMessage_EXT(p->getEngine(), l->getOperands().first().data());
    return true;
}

bool SkAsmISA::wrn(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ObjectWarning_EXT(p->getEngine(), l->getOperands().first().data());
    return true;
}

bool SkAsmISA::err(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ObjectError_EXT(p->getEngine(), l->getOperands().first().data());
    return true;
}

bool SkAsmISA::dbg(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ObjectDebug_EXT(p->getEngine(), l->getOperands().first().data());
    return true;
}

bool SkAsmISA::pdbg(SkAsmProgram *p, SkAsmCodeLine *l)
{
    ObjectPlusDebug_EXT(p->getEngine(), l->getOperands().first().data());
    return true;
}

bool SkAsmISA::sys(SkAsmProgram *p, SkAsmCodeLine *l)
{
    SkVariant &val = p->getVar(l->getOperands().first());
    system(val.data());
    return false;
}

bool SkAsmISA::mks(SkAsmProgram *p, SkAsmCodeLine *l)
{
    SkAsmHeapLoc *heapLoc = p->getHeap()->allocateStruct(p->getVar(l->getOperands().first()).data());
    SkVariant fakeV;
    return p->setVar(true, l->getOperands().last(), fakeV, heapLoc);
}

bool SkAsmISA::mko(SkAsmProgram *p, SkAsmCodeLine *l)
{
    SkAsmHeapLoc *heapLoc = p->getHeap()->allocateObject(p->getVar(l->getOperands().first()).data());
    SkVariant fakeV;
    return p->setVar(true, l->getOperands().last(), fakeV, heapLoc);
}

bool SkAsmISA::atch(SkAsmProgram *p, SkAsmCodeLine *l)
{
    if (l->getOperands().count() == 3)
        return p->attach(l->getOperands()[0].data(), l->getOperands()[1].data(), l->getOperands()[2].data());

    return false;//p->attach(l->getOperands()[0].data(), l->getOperands()[1].data(), l->getOperands()[2].data(), l->getOperands()[2].data());
}

bool SkAsmISA::dtch(SkAsmProgram *, SkAsmCodeLine *)
{
    return false;
}

#endif
