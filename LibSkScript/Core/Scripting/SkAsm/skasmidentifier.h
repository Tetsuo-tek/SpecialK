#ifndef SKASMIDENTIFIER_H
#define SKASMIDENTIFIER_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skstringlist.h"

#if defined(ENABLE_SKASM)

class SkAsmProgram;
class SkAsmStack;
class SkAsmCodeLine;

enum SkAsmIdentifierType
{
    SKASM_NO_ID,
    SKASM_GLOBAL_VAR_ID,
    SKASM_LOCAL_VAR_ID,
    SKASM_FUNCT_ID,
    SKASM_FUNCTRET_VAR_ID,
    SKASM_STCMETH_ID,
    SKASM_OBJMETH_ID,
    SKASM_VARMEMBER_ID,
    SKASM_STRCTPROP_ID,
    SKASM_EXTERNCTX_ID
};

class SkAsmIdentifier extends SkFlatObject
{
    public:
        SkAsmIdentifier(SkAsmProgram *prg);
        bool evaluateIdentifier(CStr *idStr);
        bool evaluateIdentifierStructure();
        bool checkThisInsideFunctCtx(SkAsmCodeLine *l);

        static CStr *identifierTypeName(SkAsmIdentifierType t);

        SkAsmProgram *getProgram();
        SkAsmProgram *getIncludedProgram();

        SkStringList &getHierarchy();
        CStr *getPrefix();
        CStr *getIdentifier();
        SkAsmIdentifierType getIdentifierType();
        SkAsmStack *getStack();
        SkAsmCodeLine *getLine();

    private:
        SkAsmProgram *prg;
        SkAsmProgram *includedProgram;
        SkStringList hierarchy;
        SkString pfx;// ????
        SkString identifier;
        SkAsmIdentifierType t;
        SkAsmStack *stack;
        SkAsmCodeLine *line;
        bool isLocal;
};

#endif

#endif // SKASMIDENTIFIER_H
