#include "skasmheap.h"
#include "skasm.h"
#include "skasmcodeline.h"

#if defined(ENABLE_SKASM)

SkAsmHeap::SkAsmHeap()
{
    prg = nullptr;
}

void SkAsmHeap::setProgram(SkAsmProgram *p)
{
    prg = p;
    setObjectName(prg, "Heap");
}

SkAsmHeapLoc *SkAsmHeap::allocateStruct(CStr *typeName)
{
    return newLocation(typeName, true);
}

SkAsmHeapLoc *SkAsmHeap::allocateObject(CStr *typeName)
{
    return newLocation(typeName, false);
}

SkAsmHeapLoc *SkAsmHeap::storeExternStructPointer(CStr *typeName, void *ptr)
{
    return newLocation(typeName, true, ptr);
}

SkAsmHeapLoc *SkAsmHeap::storeExternClassPointer(CStr *typeName, void *ptr)
{
    return newLocation(typeName, false, ptr);
}

SkAsmHeapLoc *SkAsmHeap::newLocation(CStr *typeName, bool isStruct, void *extPtr)
{
    SkAsmHeapLocType loc_T;

    void *ptr = nullptr;
    void *type = nullptr;

    SkAsmHeapLoc *hLoc = new SkAsmHeapLoc;

    if (isStruct)
    {
        SkStructType *t = searchStructType(typeName);

        if (!t)
        {
            FlatError("StructType NOT found: " << typeName);
            return nullptr;
        }

        loc_T = SkAsmHeapLocType::STRUCT_PTR;
        type = t;

        if (extPtr)
            ptr = extPtr;

        else
            ptr = t->instancer();

        hLoc->structProiection = new SkArgsMap;
    }

    else
    {
        SkClassType *t = searchClassType(typeName);

        if (!t)
        {
            FlatError("ObjectType NOT found: " << typeName);
            return nullptr;
        }

        loc_T = SkAsmHeapLocType::OBJECT_PTR;
        type = t;

        if (extPtr)
            ptr = extPtr;

        else
            ptr = t->instancer(nullptr);

        hLoc->structProiection = nullptr;
    }

    hLoc->loc_T = loc_T;
    hLoc->t = type;
    hLoc->ptr = ptr;
    hLoc->isDestructable = (ptr==nullptr); //EXTERNAL ptr WILL NOT BE MANAGED
    hLoc->referencesCount = 0;

    heap[hLoc->ptr] = hLoc;

    if (extPtr)
    {
        if (isStruct)
            FlatPlusDebug("New Struct location from External-pointer [" << heap.count() << "]: " << typeName);
        else
            FlatPlusDebug("New Object location from External-pointer [" << heap.count() << "]: " << typeName);
    }

    else
    {
        if (isStruct)
            FlatPlusDebug("New Struct location [" << heap.count() << "]: " << typeName);
        else
            FlatPlusDebug("New Object location [" << heap.count() << "]: " << typeName);
    }

    return hLoc;
}

bool SkAsmHeap::newPointerReference(SkAsmStack *stack, void *ptr)
{
    if (!exists(ptr))
        return false;

    SkAsmHeapLoc *hLoc = heap[ptr];
    hLoc->referencesCount++;

    CStr *typeName;

    if (hLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
        typeName = static_cast<SkStructType *>(hLoc->t)->name;

    else if (hLoc->loc_T == SkAsmHeapLocType::OBJECT_PTR)
        typeName = static_cast<SkClassType *>(hLoc->t)->name;

    FlatPlusDebug("New location variable reference on stack [stk: " << stack->objectName()
               << "; loc_T: " << SkAsmHeap::locTypeName(hLoc->loc_T)
               << "; type: " << typeName
               << "; totRefs: " << hLoc->referencesCount << "]: " << ptr);

    return true;
}

bool SkAsmHeap::exists(void *ptr)
{
    return heap.contains(ptr);
}

void SkAsmHeap::remove(void *ptr)
{
    if (!heap.contains(ptr))
    {
        ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_HEAPLOC_NOT_FOUND,
                "Cannot remove; Pointer NOT found in the heap" << prg->getCurrentLine()->getLine())
        return;
    }

    SkAsmHeapLoc *hLoc = heap[ptr];
    hLoc->referencesCount--;

    if (hLoc->referencesCount == 0)
    {
        if (hLoc->isDestructable)
        {
            if (hLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
            {
                static_cast<SkStructType *>(hLoc->t)->destroyer(hLoc->ptr);
                delete hLoc->structProiection;
            }

            else if (hLoc->loc_T == SkAsmHeapLocType::OBJECT_PTR)
            {
                SkFlatObject *o = static_cast<SkObject *>(hLoc->ptr);

                if (o->isFlatObject())
                    delete o;

                else
                    dynamic_cast<SkObject *>(o)->destroyLater();
            }
        }

        heap.remove(hLoc->ptr);
        delete hLoc;
    }
}

 CStr *SkAsmHeap::locTypeName(SkAsmHeapLocType loc_T)
 {
     if (loc_T == SkAsmHeapLocType::STRUCT_PTR)
         return "STRUCT_PTR";

     return "OBJECT_PTR";
 }


#endif
