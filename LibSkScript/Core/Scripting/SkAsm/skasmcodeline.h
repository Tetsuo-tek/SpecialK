#ifndef SKASMCODELINE_H
#define SKASMCODELINE_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skvariant.h"

#if defined(ENABLE_SKASM)

struct SkAsmInstr;
class SkAsmCodeContext;
class SkAsmProgram;

class SkAsmCodeLine extends SkFlatObject
{
    public:
        SkAsmCodeLine(SkAsmProgram *program,
                      uint64_t lineNumber,
                      CStr *txtLine);

        bool parse(SkAsmCodeContext *context);

        SkAsmProgram *getProgram();
        uint64_t getLineNumber();
        CStr *getLine();
        SkAsmCodeContext *getContext();
        SkAsmInstr *getInstruction();
        SkVector<SkVariant> &getOperands();

    private:
        SkAsmProgram *prg;
        uint64_t number;
        SkString text;
        SkAsmInstr *instr;
        SkAsmCodeContext *ctx;
        SkVector<SkVariant> operands;

        bool parseInstruction();
        bool parseOperands(CStr *operandSubStr);
        bool parseImmediateOperand(CStr *content, uint64_t &len);
        bool parseImmediateTypedOperand(CStr *content, uint64_t &len, SkVariant &v);
        void parseIdentifierOperand(CStr *content, uint64_t &len);
};

#endif

#endif // SKASMCODELINE_H
