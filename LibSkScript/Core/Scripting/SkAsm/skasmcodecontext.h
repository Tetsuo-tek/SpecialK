#ifndef SKASMCODECONTEXT_H
#define SKASMCODECONTEXT_H

#include "skasmstack.h"
#include "skasmslot.h"

#if defined(ENABLE_SKASM)

// // // // // // // // // // // // // // // // // // // // //

typedef enum
{
    SKASM_PRC,
    SKASM_FUNCT,
    SKASM_SLOT
} SkAsmContextType;

// // // // // // // // // // // // // // // // // // // // //

class SkAsmCodeContext extends SkFlatObject
{
    public:
        SkAsmCodeContext();

        void setProgram(SkAsmProgram *p);

        void addLabel(SkAsmCodeLine *codeLine);
        void setContextEnd(uint64_t lineNumber);
        CStr *getName();
        bool containsLabel(CStr *lblName);
        uint64_t getLabelLineNumber(CStr *lblName);
        uint64_t getStartLineNumber();
        uint64_t getEndLineNumber();
        SkAsmContextType getCtxType();

        static CStr *ctxTypeName(SkAsmContextType t);

    protected:
        SkAsmProgram *prg;
        SkString name;
        SkAsmContextType t;

        uint64_t startLineNumber;
        uint64_t endLineNumber;

        SkMap<SkString, uint64_t> labels;
};

// // // // // // // // // // // // // // // // // // // // //

class SkAsmProcedureCtx extends SkAsmCodeContext
{
    public:
        SkAsmProcedureCtx(SkAsmProgram *p, CStr *procName, uint64_t lineNumber);
};
// // // // // // // // // // // // // // // // // // // // //

class SkAsmAbstractFunction extends SkAsmCodeContext
{
    public:
        SkAsmAbstractFunction(SkAsmProgram *p, CStr *functName, uint64_t lineNumber, SkAsmContextType ctxType);

        uint64_t getParamsCount();
        CStr *getParamName(uint64_t index);
        SkAsmStack *getStack();

    protected:
        SkAsmStack stack;
        SkStringList paramsNames;
};

// // // // // // // // // // // // // // // // // // // // //

class SkAsmFunctionCtx extends SkAsmAbstractFunction
{
    public:
        SkAsmFunctionCtx(SkAsmProgram *p, CStr *functName, uint64_t lineNumber);
        SkVariant &getReturnVar();

    protected:
        SkVariant retVal;
};

// // // // // // // // // // // // // // // // // // // // //

class SkAsmSlotCtx extends SkAsmAbstractFunction
{
    public:
        SkAsmSlotCtx(SkAsmProgram *p, CStr *functName, uint64_t lineNumber);

        SkAsmSlot *getAsmSlot();

    private:
        SkAsmSlot slt;
};

// // // // // // // // // // // // // // // // // // // // //

#endif

#endif // SKASMCODECONTEXT_H
