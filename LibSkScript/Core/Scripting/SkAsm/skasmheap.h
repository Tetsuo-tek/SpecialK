/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKASMHEAP_H
#define SKASMHEAP_H

#if defined(ENABLE_SKASM)

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skmap.h"
#include "Core/Containers/skstring.h"

class SkAsm;
class SkAsmProgram;
class SkAsmCodeLine;

typedef enum
{
    OBJECT_PTR,
    STRUCT_PTR
} SkAsmHeapLocType;

//NOT USING typedef HERE BECAUSE IT IS FORWARDED ON ASMSTACK
struct SkAsmHeapLoc
{
    void *ptr;
    SkArgsMap *structProiection;
    void *t;

    bool isDestructable;

    SkAsmHeapLocType loc_T;
    uint64_t referencesCount;//when it is 0 it will be destroyed
};

class SkAsmStack;

class SKSCRIPT SkAsmHeap extends SkFlatObject
{
    public:
        SkAsmHeap();

        void setProgram(SkAsmProgram *p);

        SkAsmHeapLoc *allocateStruct(CStr *typeName);
        SkAsmHeapLoc *allocateObject(CStr *typeName);

        //THESE POINTERs WILL NOT BE DESTROYED WHEN THEY ARE NOT REFERENCED ANYMORE
        SkAsmHeapLoc *storeExternStructPointer(CStr *typeName, void *ptr);
        SkAsmHeapLoc *storeExternClassPointer(CStr *typeName, void *ptr);

        bool newPointerReference(SkAsmStack *stack, void *ptr);

        bool exists(void *ptr);
        void remove(void *ptr);

        static CStr *locTypeName(SkAsmHeapLocType loc_T);

    private:
        SkMap<void *, SkAsmHeapLoc *> heap;
        SkAsmProgram *prg;

        SkAsmHeapLoc *newLocation(CStr *typeName, bool isStruct, void *extPtr=nullptr);
};

#endif

#endif // SKASMHEAP_H
