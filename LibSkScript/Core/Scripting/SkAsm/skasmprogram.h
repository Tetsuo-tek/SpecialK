#ifndef SKASMPROGRAM_H
#define SKASMPROGRAM_H

#include "skasmidentifier.h"
#include "skasmstack.h"
#include "skasmheap.h"
#include "skasmcodecontext.h"

#include "Core/System/Filesystem/skfsutils.h"

#if defined(ENABLE_SKASM)

enum SkAsmError
{
    SKASM_DIRECTIVE_ERROR,
    SKASM_WRONG_OP_NUM,
    SKASM_WRONG_OP_TYPE,
    SKASM_TYPE_NOT_FOUND,
    SKASM_VAR_NOT_FOUND,
    SKASM_VAR_ALREADY_EXISTS,
    SKASM_HEAPLOC_NOT_FOUND,
    SKASM_MAIN_NOT_FOUND,
    SKASM_CTX_NOT_FOUND,
    SKASM_CTX_INSIDE_CTX,
    SKASM_CTX_IS_NOT_FUNCTION,
    SKASM_CTX_RETVAL_FROM_SLOT,
    SKASM_CTX_FUNCT_TOO_FEW_PARAMS,
    SKASM_CTX_FUNCT_IS_NOT_CALLED,
    SKASM_CTX_END_WITHOUT_START,
    SKASM_LABEL_WITHOUT_CONTAINER,
    SKASM_INSTR_UNKNOWN,
    SKASM_INSTR_WITHOUT_CTX,
    SKASM_INSTR_ERROR,
    SKASM_SYNTAX_ERROR
};

enum SkAsmCompOpers
{
    SKASM_LT,
    SKASM_LE,
    SKASM_GT,
    SKASM_GE,
    SKASM_EQ,
    SKASM_NE
};

enum SkAsmArithOpers
{
    SKASM_SUM,
    SKASM_SUB,
    SKASM_MUL,
    SKASM_DIV,
    SKASM_REM,
    SKASM_POW,
    SKASM_INC,
    SKASM_DEC,
    SKASM_NEG
};

class SkAsm;

class SkAsmProgram extends SkFlatObject
{
    public:
        SkAsmProgram(CStr *prgName, SkAsm *engine);
        ~SkAsmProgram() override;

        bool addExternalStructPointer(CStr *typeName, CStr *globalVarName, void *ptr);
        bool addExternalClassPointer(CStr *typeName, CStr *globalVarName, void *ptr);

        void setCounter(uint64_t index);
        void addCodeLine(SkAsmCodeLine *codeLine);
        void addContext(SkAsmCodeContext *ctx);
        void addJump(uint64_t lineNumber);

        bool load(CStr *filePath);
        void setAsLibrary(SkAsmStack *externStack, SkAsmHeap *externHeap);
        bool exec(bool withLoopEvent=false, SkVector<uint64_t> *breakPoints=nullptr);
        bool tick();

        bool setVar(bool definingIt, SkVariant &operand, SkVariant &val, SkAsmHeapLoc *loc=nullptr);
        SkVariant &getVar(SkVariant &operand);
        bool delVar(SkVariant &operand);

        bool call(CStr *identifier, SkVariant *params, uint64_t paramsCount);

        bool containsContext(CStr *name);
        SkAsmCodeContext *getContext(CStr *name);

        SkAsmAbstractFunction *getLastCalledFunction();
        SkVariant &getLastExternMethodReturn();

        void setEndRequest();

        CStr *getName();
        uint64_t getCounter();
        SkAsmCodeLine *getCurrentLine();
        SkAsmCodeLine *getLine(uint64_t number);
        SkAsm *getEngine();
        SkList<SkAsmProgram *> &getIncludedCode();
        SkAsmStack *getStack();
        SkAsmHeap *getHeap();
        bool hasError();

        static CStr *errorToString(SkAsmError e);
        static CStr *compareOperatorName(SkAsmCompOpers op);
        static CStr *arithmeticOperatorName(SkAsmArithOpers op);

    private:
        SkString name;
        SkString codeFile;
        SkPathInfo pathInfo;
        SkArgsMap developInfos;

        bool useLoopEvent;
        uint64_t pc;
        SkAsm *e;
        SkAsmStack *stack;
        SkAsmHeap *heap;
        bool isLibrary;
        bool loopActive;

        SkVector<SkAsmCodeLine *> codeLines;
        SkVector<uint64_t> jumps;
        SkVariant lastExternMethodReturn;
        SkList<SkAsmProgram *> included;

        SkMap<SkString, SkAsmCodeContext *> contexts;
        SkAsmAbstractFunction *lastCalledFunction;

        friend class SkAsm;
        friend class SkAsmISA;

        bool errorOccurred;
        bool endRequested;

        bool fetch(SkString &code);
        bool parseDirective(SkString &line);
        bool include(SkString &directive);

        bool compare(SkVariant &v1, SkVariant &v2, SkAsmCompOpers op);
        bool jump(CStr *target);
        bool jumpByCondition(SkVariant &v1,
                             SkVariant &v2,
                             SkAsmCompOpers op,
                             CStr *true_proc,
                             CStr *false_proc=nullptr);

        void endOfContextReached(SkAsmCodeLine *l);

        bool callFunctionContext(CStr *functName, SkVariant *params, uint64_t paramsCount);
        bool callFunctionContext(SkAsmCodeContext *ctx, SkVariant *params, uint64_t paramsCount);

        bool callClassMethod(SkAsmIdentifier &id, SkVariant *params, uint64_t paramsCount);

        bool attach(CStr *identifier, CStr *slotName, CStr *mode);
        bool detach(CStr *identifier, CStr *slotName);

        bool attach(CStr *identifier, SkObject *target, CStr *slotName, CStr *mode);
        bool detach(CStr *identifier, SkObject *target, CStr *slotName);
};

#endif

#endif // SKASMPROGRAM_H
