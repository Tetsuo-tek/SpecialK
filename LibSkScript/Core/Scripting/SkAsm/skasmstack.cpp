#include "skasmstack.h"

#if defined(ENABLE_SKASM)

#include "skasm.h"

static SkVariant vNULL;

SkAsmStack::SkAsmStack()
{
    prg = nullptr;
    fCTX = nullptr;
}

void SkAsmStack::setProgram(SkAsmProgram *p, SkAsmAbstractFunction *fOwner)
{
    prg = p;
    fCTX = fOwner;

    if (fCTX)
    {
        setObjectName(prg, fCTX->getName());
        setObjectName(this, "Stack");
    }

    else
        setObjectName(prg, "Stack");
}

bool SkAsmStack::contains(CStr *identifier)
{
    return stack.contains(identifier);
}

bool SkAsmStack::def(CStr *identifier, SkVariant &val)
{
    return setupVariable(true, identifier, val);
}

bool SkAsmStack::def(CStr *identifier, SkAsmHeapLoc *hLoc)
{
    SkVariant val(static_cast<void *>(hLoc));
    return setupVariable(true, identifier, val, hLoc);
}

bool SkAsmStack::udef(CStr *identifier)
{
    if (!stack.contains(identifier))
    {
        SkAsmCodeLine *l = prg->getCurrentLine();

        ASM_ERR(prg->getEngine(), l, SkAsmError::SKASM_VAR_NOT_FOUND,
                "Cannot deallocate a variable that NOT exists: "
                << identifier)
    }

    SkAsmStackLoc *sLoc = stack[identifier];

    if (sLoc->heapLoc)
        prg->getHeap()->remove(sLoc->heapLoc->ptr);

    bool ok = stack.remove(identifier).key() == SkString(identifier);
    delete sLoc;

    FlatPlusDebug("Stack entry deleted: " << identifier);
    return ok;
}

bool SkAsmStack::set(CStr *identifier, SkVariant &val)
{
    return setupVariable(false, identifier, val);
}

bool SkAsmStack::set(CStr *identifier, SkAsmHeapLoc *hLoc)
{
    SkVariant val(static_cast<void *>(hLoc));
    prg->getHeap()->newPointerReference(this, hLoc->ptr);
    return setupVariable(false, identifier, val, hLoc);
}

bool SkAsmStack::setupVariable(bool definingIt, CStr *identifier, SkVariant &val, SkAsmHeapLoc *hLoc)
{
    bool varExists = stack.contains(identifier);
    SkAsmStackLoc *sLoc = nullptr;

    if (definingIt)
    {
        if (varExists)
        {
            ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_VAR_ALREADY_EXISTS,
                    "Cannot define a variable that ALREADY exists: "
                    << identifier)

            return false;
        }

        sLoc = new SkAsmStackLoc;
        sLoc->heapLoc = hLoc;
        stack[identifier] = sLoc;

        if (hLoc)
        {
            val.toCustom = &varPtrToCustom;
            val.toCustomString = &varPtrToCustomString;
            FlatPlusDebug("Adding callbacks to variable [" << val.variantTypeName() << "]: " << identifier);
        }

        FlatPlusDebug("Creating new stack entry [" << val.variantTypeName() << "; " << val.size() << " B]: " << identifier);
    }

    else
    {
        if (!varExists)
        {
            ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_VAR_NOT_FOUND,
                    "Cannot setup a variable that does NOT exist: "
                    << identifier)

            return false;
        }

        sLoc = stack[identifier];
        sLoc->heapLoc = hLoc;

        FlatPlusDebug("Updating stack entry ["
                   << sLoc->v.variantTypeName() << " -> " << val.variantTypeName()
                   << "; " << val.size() << " B]: " << identifier);
    }

    sLoc->v = val;

    SkString varName = "#";
    varName.append(identifier);

    if (fCTX)
        sLoc->v.setObjectName(fCTX, varName.c_str());

    else
        sLoc->v.setObjectName(prg, varName.c_str());

    if (val.toCustom)
    {
        sLoc->v.toCustom = &varPtrToCustom;
        sLoc->v.toCustomString = &varPtrToCustomString;
    }

    return true;
}


SkVariant &SkAsmStack::get(SkAsmIdentifier *id)
{
    SkAbstractIterator<SkPair<SkString, SkAsmStackLoc *>> *itr = stack.iterator();
    SkAsmStackLoc *sLoc = nullptr;

    while(itr->next())
        if (itr->item().key() == id->getHierarchy().first())
        {
            sLoc = itr->item().value();
            break;
        }

    delete itr;

    if (sLoc)
    {
        if (id->getHierarchy().count() == 2)
        {
            if (sLoc->v.isMap())
            {
                SkVariant propName(id->getHierarchy().last().c_str());

                if (sLoc->v.getMapProperty(propName, temporaryStableValue))
                    return temporaryStableValue;
            }

            else if (sLoc->v.isList() && ::isdigit(id->getHierarchy().last().c_str()[0]))
            {
                SkVariant index(id->getHierarchy().last().c_str());

                if (sLoc->v.getListItem(index.toUInt64(), temporaryStableValue))
                    return temporaryStableValue;
            }
        }

        return sLoc->v;
    }

    ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_VAR_NOT_FOUND,
            "Variable does NOT exist in the stack: "
            << id->getIdentifier())

    return vNULL;
}

SkVariant &SkAsmStack::getStructProperty(CStr *identifier, CStr *property)
{
    SkAbstractIterator<SkPair<SkString, SkAsmStackLoc *>> *itr = stack.iterator();
    SkAsmStackLoc *sLoc = nullptr;

    while(itr->next())
        if (itr->item().key() == identifier)
        {
            sLoc = itr->item().value();
            break;
        }

    delete itr;

    if (sLoc)
    {
        if (sLoc->heapLoc && sLoc->heapLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
        {
            SkPair<SkString, SkVariant> *pair = nullptr;
            SkAbstractIterator<SkPair<SkString, SkVariant>> *itr = sLoc->heapLoc->structProiection->iterator();

            while(itr->next())
                if (itr->item().key() == property)
                {
                    pair = &itr->item();
                    break;
                }

            delete itr;

            if (pair)
                return pair->value();
        }
    }

    ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_VAR_NOT_FOUND,
            "Struct property NOT found: "
            << identifier << "." << property)

    return vNULL;
}

bool SkAsmStack::setStructProperty(CStr *identifier, CStr *property, SkVariant &val)
{
    SkAbstractIterator<SkPair<SkString, SkAsmStackLoc *>> *itr = stack.iterator();
    SkAsmStackLoc *sLoc = nullptr;

    while(itr->next())
        if (itr->item().key() == identifier)
        {
            sLoc = itr->item().value();
            break;
        }

    delete itr;

    if (sLoc)
    {
        if (sLoc->heapLoc && sLoc->heapLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
        {
            (*sLoc->heapLoc->structProiection)[property] = val;
            static_cast<SkStructType *>(sLoc->heapLoc->t)->fromMap(sLoc->heapLoc->structProiection, sLoc->heapLoc->ptr);
            return true;
        }
    }

    ASM_ERR(prg->getEngine(), prg->getCurrentLine(), SkAsmError::SKASM_VAR_NOT_FOUND,
            "Struct property NOT found: "
            << identifier << "." << property)

    return false;
}

SkAsmStackLoc *SkAsmStack::getStackLocation(CStr *identifier)
{
    SkAbstractIterator<SkPair<SkString, SkAsmStackLoc *>> *itr = stack.iterator();
    SkAsmStackLoc *sLoc = nullptr;

    while(itr->next())
        if (itr->item().key() == identifier)
        {
            sLoc = itr->item().value();
            break;
        }

    delete itr;
    return sLoc;
}

void SkAsmStack::clear()
{
    SkAbstractListIterator<SkPair<SkString, SkAsmStackLoc *>> *itr = stack.iterator();

    while(itr->next())
    {
        SkAsmStackLoc *sLoc = itr->item().value();

        if (sLoc->heapLoc)
            prg->getHeap()->remove(sLoc->heapLoc->ptr);

        delete sLoc;
    }

    delete itr;
    stack.clear();
}

void *varPtrToCustom(SkVariant *v)
{
    return static_cast<SkAsmHeapLoc *>(v->toVoid())->ptr;
}

SkString varPtrToCustomString(SkVariant *v)
{
    SkAsmHeapLoc *hLoc = static_cast<SkAsmHeapLoc *>(v->toVoid());

    SkString ret;

    if (hLoc->loc_T == SkAsmHeapLocType::STRUCT_PTR)
    {
        SkStructType *t = static_cast<SkStructType *>(hLoc->t);
        SkArgsMap m;
        t->toMap(hLoc->ptr, &m);
        m.toString(ret);
    }

    else if (hLoc->loc_T == SkAsmHeapLocType::OBJECT_PTR)
    {
        //SkClassType *t = static_cast<SkClassType *>(hLoc->t);
        ret = static_cast<SkFlatObject *>(hLoc->ptr)->uuid();
    }

    return ret;
}

#endif
