/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKASM_H
#define SKASM_H

#if defined(ENABLE_SKASM)

#include "skasmisa.h"
#include "skasmprogram.h"

class SKSCRIPT SkAsm extends SkObject
{
    public:
        Constructor(SkAsm, SkObject);

        bool load(CStr *filePath, CStr *name);
        bool exec(bool withLoopEvent=false, SkVector<uint64_t> *breakPoints=nullptr);

        void enableEventLoopTicks();
        void disableEventLoopTicks();

        bool addExternalStructPointer(CStr *typeName, CStr *globalVarName, void *ptr);
        bool addExternalClassPointer(CStr *typeName, CStr *globalVarName, void *ptr);

        Signal(error);
        Slot(custom);

        Slot(fastTickProxy);
        Signal(fastTick);
        Slot(slowTickProxy);
        Signal(slowTick);
        Slot(oneSecTickProxy);
        Signal(oneSecTick);

        bool shutdown();

        static SkAsmInstr *isa(uint64_t &count);
        SkStringList &getImmediatePrimitiveTypes();

    private:
        bool evtLoopTicksEnabled;
        SkAsmProgram *prg;
        SkStringList immediatePrimitiveTypes;

        friend class SkAsmProgram;
        friend class SkAsmCodeLine;
        friend class SkAsmHeap;
        friend class SkAsmStack;
        friend class SkAsmParser;
        friend class SkAsmIdentifier;
        friend class SkAsmISA;

        //line can be null, but desc CANNOT be null
        void error(SkAsmCodeLine *line, SkAsmError e, CStr *desc);
};

#define ASM_ERR(ENG, LIN, ERR, DESC) \
    { \
        stringstream stream; \
        stream << "{from: " << __PRETTY_FUNCTION__ << "} -> " << DESC; \
        ENG->error(LIN, ERR, stream.str().data()); \
    }

#endif

#endif // SKASM_H
