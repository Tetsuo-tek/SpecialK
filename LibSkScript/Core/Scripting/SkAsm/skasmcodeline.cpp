#include "skasmcodeline.h"
#include "skasm.h"
#include "skasmcodecontext.h"

#if defined(ENABLE_SKASM)

SkAsmCodeLine::SkAsmCodeLine(SkAsmProgram *program, uint64_t lineNumber, CStr *txtLine)
{
    prg = program;
    number = lineNumber+1;
    text = txtLine;

    ctx = nullptr;
    instr = nullptr;

    SkString s("L#");
    s.concat(number);
    setObjectName(program, s.c_str());
}

bool SkAsmCodeLine::parse(SkAsmCodeContext *context)
{
    prg->addCodeLine(this);

    if (text.isEmpty())
    {
        ctx = context;
        FlatPlusDebug("Not a code-line [L: " << number << "]");
    }

    else if (text.charAt(0) == '#'
             || text.charAt(0) == ';'
             || text.charAt(0) == '/')
    {
        ctx = context;
        FlatPlusDebug("Comment [L: " << number<< "]");
    }

    else if (text.endsWith("%"))
    {
        if (!context)
            return false;

        text.chop(1);
        ctx = context;
        ctx->addLabel(this);
    }

    else if (text.endsWith(":"))
    {
        if (context)
        {
            ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_CTX_INSIDE_CTX,
                    "A context cannot exists inside another context: " << text)

            return false;
        }

        text.chop(1);

        if (text.startsWith("&"))
            context = new SkAsmSlotCtx(prg, &text.c_str()[1], number - 1);

        else if (text.startsWith(":"))
            context = new SkAsmFunctionCtx(prg, &text.c_str()[1], number - 1);

        else
            context = new SkAsmProcedureCtx(prg, text.c_str(), number - 1);

        ctx = context;
        prg->addContext(ctx);
    }

    else if (text.startsWith("!") || text.startsWith("f!"))
    {
        if (!context)
        {
            ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_CTX_END_WITHOUT_START, "")
            return false;
        }

        ctx = context;
        context->setContextEnd(number - 1);

        context = nullptr;
    }

    else
    {
        if (!context)
        {
            ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_INSTR_WITHOUT_CTX,
                    "An Instruction always requires a context container: " << text.simplify())

            return false;
        }

        ctx = context;

        if (!parseInstruction())
            return false;
    }

    //IT IS A LABEL
    if (text.endsWith("%"))
        text.chop(1);

    //IT IS A FUNCT/PROC
    else if (text.endsWith(":"))
        text.chop(1);

    return true;

    //OTHERS
    //return parseInstruction();
}

bool SkAsmCodeLine::parseInstruction()
{
    CStr *s = text.c_str();
    uint64_t z = 0;
    uint64_t sz = text.size();

    for(; z<sz; z++)
        if (::isspace(s[z]) || s[z] == ';')
            break;

    bool ok = false;

    uint64_t instrCount;
    SkAsmInstr *instructions = prg->getEngine()->isa(instrCount);

    for(uint64_t y=0; y<instrCount; y++)
        if ((instructions[y].nameLen == z) && memcmp(s, instructions[y].name, z) == 0)
        {
            instr = &instructions[y];
            ok = true;
            break;
        }

    if (!ok)
    {
        ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_INSTR_UNKNOWN, text)
        return false;
    }

    if (!parseOperands(&s[z]))
        return false;

    FlatPlusDebug("New instruction [L: " << number
               << "; C: " << ctx->getName()
               << "; I: " << instr->name
               << "; O: " << operands.count()
               << "]");

    return true;
}

bool SkAsmCodeLine::parseOperands(CStr *operandSubStr)
{
    if (instr->reqOperandsCount || instr->maxOperandsCount)
    {
        uint64_t sz = strlen(operandSubStr);
        uint64_t i;
        //SkVariant listItemVal;
        bool isValueExpected = true;

        for(i=0; i<sz; i++)
        {
            if (operandSubStr[i] == ',')
            {
                if (isValueExpected)
                {
                    ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                            "Operands List expects a value, but it has found a comma: "
                            << text)

                    return false;
                }

                else
                    isValueExpected = true;
            }

            else if (operandSubStr[i] == ';')
                break;

            else if (::isspace(operandSubStr[i]))
            {}

            else
            {
                if (isValueExpected)
                {
                    if (operandSubStr[i] == '@')
                    {
                        uint64_t len;

                        if (!parseImmediateOperand(&operandSubStr[i], len))
                            return false;

                        i+=len;
                    }

                    else if (operandSubStr[i] == '!')
                    {
                        FlatPlusDebug("Ctx return-to-origin as operand [id: " << operands.count() << "]");
                        operands << "!";
                    }

                    else if (::isalnum(operandSubStr[i]) || operandSubStr[i] == '_')
                    {
                        uint64_t len;
                        parseIdentifierOperand(&operandSubStr[i], len);
                        i+=len;
                    }

                    else
                    {
                        SkString s(text);
                        s.simplify();
                        ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                                "Operand type UNKNOWN: " << s)

                        return false;
                    }

                    isValueExpected = false;
                }

                else
                {
                    ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                            "Operands List does NOT expect a value, but it has found a value: "
                            << &operandSubStr[i])

                    return false;
                }
            }
        }

        if (isValueExpected)
        {
            ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                    "Operands List does NOT expect a value, but it has found a value: "
                    << &operandSubStr[i])

            return false;
        }
    }

    if (operands.count() < instr->reqOperandsCount)
    {
        ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_WRONG_OP_NUM,
                "(count: " << operands.count() <<
                ") Minimum operands count is " << (int) instr->reqOperandsCount)

        return false;
    }

    return true;
}

bool SkAsmCodeLine::parseImmediateOperand(CStr *content, uint64_t &len)
{
    if (strlen(content) == 1)
    {
        ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                "Immediate value is empty: " << text)

        return false;
    }

    FlatPlusDebug("Parsing immediate operand at line: " << number);

    SkVariant v;

    if (!parseImmediateTypedOperand(&content[1], len, v))
    {
        if (!SkVariant::parseJsonValue(&content[1], len, v))
        {
            ASM_ERR(prg->getEngine(), this, SkAsmError::SKASM_SYNTAX_ERROR,
                    "Immediate value parsing error: " << &content[1])

            return false;
        }
    }

    operands << v;

    SkString s = operands.last().toString();

    if (s.size() > 30)
    {
        s.chop(s.size()-25);
        s.append(" [..]");
    }

    FlatPlusDebug("Immediate as operand [id: " << operands.count()-1
               << "; T: " << operands.last().variantTypeName()
               << "]: " << s);

    return true;
}

bool SkAsmCodeLine::parseImmediateTypedOperand(CStr *content, uint64_t &len, SkVariant &v)
{
    if (content[0] != 'U' && content[0] != 'S' && content[0] != 'F' && content[0] != 'D')
        return false;

    SkString s(content);
    SkStringList &types = prg->getEngine()->getImmediatePrimitiveTypes();

    uint64_t offset = 0;
    uint64_t i = 0;

    for(; i<types.count(); i++)
    {
        if (s.startsWith(types[i].c_str()))
        {
            offset = types[i].size();
            break;
        }
    }

    len += offset;

    bool isInteger = (types[i].c_str()[0] != 'F' && types[i].c_str()[0] != 'D');
    bool floatingPointReceived = false;

    SkString num;

    for(uint64_t i=offset; i<s.size(); i++)
    {
        if (isInteger && !::isdigit(content[i]))
            break;

        if (!isInteger && (!::isdigit(content[i]) && content[i] != '.'))
        {
            if (content[i] == '.')
            {
                if (floatingPointReceived)
                {
                    FlatWarning("More than one point inside a floating-point type [" << types[i]<< "]: " << num);
                    break;
                }

                else
                    floatingPointReceived = true;
            }
        }

        num.concat(content[i]);
    }


    if (isInteger)
    {
        if (types[i] == "U8:")
            v = static_cast<uint8_t>(::atoi(num.c_str()));

        else if (types[i] == "S8:")
            v = static_cast<int8_t>(::atoi(num.c_str()));

        else if (types[i] == "U16:")
            v = static_cast<uint16_t>(::atoi(num.c_str()));

        else if (types[i] == "S16:")
            v = static_cast<int16_t>(::atoi(num.c_str()));

        else if (types[i] == "U32:")
            v = static_cast<uint32_t>(::atoi(num.c_str()));

        else if (types[i] == "S32:")
            v = static_cast<int32_t>(::atoi(num.c_str()));

        else if (types[i] == "U64:" || types[i] == "USIZE:")
            v = static_cast<uint64_t>(::stoull(num.c_str()));

        else if (types[i] == "S64:" || types[i] == "SSIZE:")
            v = static_cast<int64_t>(::atoll(num.c_str()));
    }

    else
    {
        if (types[i].c_str()[0] != 'F')
            v = ::strtof(num.c_str(), nullptr);

        else if (types[i].c_str()[0] != 'D')
            v = ::strtod(num.c_str(), nullptr);
    }

    FlatPlusDebug("Typed immediate value parsed [" << types[i]<< "]: " << v << " " << v.variantTypeName());

    len += num.size();
    return true;
}

void SkAsmCodeLine::parseIdentifierOperand(CStr *content, uint64_t &len)
{
    SkString varName = "#";

    FlatPlusDebug("Parsing identifier operand at line: " << number);

    uint64_t sz = strlen(content);
    uint64_t i;

    for(i=0; i<sz; i++)
    {
        if (!::isalnum(content[i]) && content[i] != '.' && content[i] != '_'
                /*&& content[i] != '#'
                && content[i] != '@'*/)
        {
            break;
        }
    }

    varName.append(content, i);
    len = varName.length()-2;//1 is # prepending the string

    FlatPlusDebug("Identifier operand [id: " << operands.count() << "]: " << varName);
    operands << varName;
}

SkAsmProgram *SkAsmCodeLine::getProgram()
{
    return prg;
}

uint64_t SkAsmCodeLine::getLineNumber()
{
    return number;
}

CStr *SkAsmCodeLine::getLine()
{
    return text.c_str();
}

SkAsmCodeContext *SkAsmCodeLine::getContext()
{
    return ctx;
}

SkAsmInstr *SkAsmCodeLine::getInstruction()
{
    return instr;
}

SkVector<SkVariant> &SkAsmCodeLine::getOperands()
{
    return operands;
}

#endif
