#include "skasmcodecontext.h"
#include "skasmcodeline.h"
#include "skasm.h"

#if defined(ENABLE_SKASM)

// // // // // // // // // // // // // // // // // // // // //

SkAsmCodeContext::SkAsmCodeContext()
{
    prg = nullptr;
    startLineNumber = 0;
    endLineNumber = 0;
}

void SkAsmCodeContext::addLabel(SkAsmCodeLine *codeLine)
{
    labels[codeLine->getLine()] = codeLine->getLineNumber();

    FlatPlusDebug("New Label [L: " << codeLine->getLineNumber()
              << "; C: " << name
              << "]: " << codeLine->getLine());
}


void SkAsmCodeContext::setContextEnd(uint64_t lineNumber)
{
    endLineNumber = lineNumber;

    FlatPlusDebug("End-of-Context [T: " << SkAsmCodeContext::ctxTypeName(t)
              << "; C: " << name
              << "; L: " << endLineNumber
              << "]");
}

CStr *SkAsmCodeContext::getName()
{
    return name.c_str();
}

bool SkAsmCodeContext::containsLabel(CStr *lblName)
{
    return labels.contains(lblName);
}

uint64_t SkAsmCodeContext::getLabelLineNumber(CStr *lblName)
{
    return labels[lblName];
}

uint64_t SkAsmCodeContext::getStartLineNumber()
{
    return startLineNumber;
}

uint64_t SkAsmCodeContext::getEndLineNumber()
{
    return endLineNumber;
}

SkAsmContextType SkAsmCodeContext::getCtxType()
{
    return t;
}

CStr *SkAsmCodeContext::ctxTypeName(SkAsmContextType t)
{
    if (t == SkAsmContextType::SKASM_PRC)
        return "PROCEDURE";

    else if (t == SkAsmContextType::SKASM_FUNCT)
        return "FUNCTION";

    else if (t == SkAsmContextType::SKASM_SLOT)
        return "SLOT";

    return "NULL";
}

// // // // // // // // // // // // // // // // // // // // //

SkAsmProcedureCtx::SkAsmProcedureCtx(SkAsmProgram *p, CStr *procName, uint64_t lineNumber)
{
    t = SkAsmContextType::SKASM_PRC;

    name = procName;
    startLineNumber = lineNumber;

    prg = p;
    setObjectName(prg, procName);

    FlatPlusDebug("New Context "
              << "[T: " << SkAsmCodeContext::ctxTypeName(t)
              << "; L: " << startLineNumber
              << "; C: " << name
              << "]");
}
// // // // // // // // // // // // // // // // // // // // //

SkAsmAbstractFunction::SkAsmAbstractFunction(SkAsmProgram *p, CStr *functName, uint64_t lineNumber, SkAsmContextType ctxType)
{
    name = functName;
    startLineNumber = lineNumber;
    t = ctxType;

    if (t == SkAsmContextType::SKASM_PRC)
    {
        FlatError("Cannot create new " << SkAsmCodeContext::ctxTypeName(t) << " CTX as AbstractFunction"
                  << "[L: " << startLineNumber
                  << "; C: " << name
                  << "]");

        return;
    }

    prg = p;
    stack.setProgram(prg, this);

    if (name.contains("$"))
    {
        int64_t pos = name.indexOf("$");
        SkString params = &name.c_str()[pos+1];
        name.chop(params.length() + 1);
        params.split(",", paramsNames);

        for(uint64_t i=0; i<paramsNames.count(); i++)
        {
            paramsNames[i].trim();
            FlatPlusDebug("New Context WITH parameters "
                      << "[T: " << SkAsmCodeContext::ctxTypeName(t)
                      << "; L: " << startLineNumber
                      << "; C: " << name
                      << "; P: " << paramsNames.join(",")
                      << "]");
        }
    }

    else
    {
        FlatPlusDebug("New Context WITHOUT parameters "
                  << "[T: " << SkAsmCodeContext::ctxTypeName(t)
                  << "; L: " << startLineNumber
                  << "; C: " << name
                  << "]");
    }
}

uint64_t SkAsmAbstractFunction::getParamsCount()
{
    return paramsNames.count();
}

CStr *SkAsmAbstractFunction::getParamName(uint64_t index)
{
    if (index >= paramsNames.count())
        return nullptr;

    return paramsNames[index].c_str();
}


SkAsmStack *SkAsmAbstractFunction::getStack()
{
    return &stack;
}

// // // // // // // // // // // // // // // // // // // // //

SkAsmFunctionCtx::SkAsmFunctionCtx(SkAsmProgram *p, CStr *functName, uint64_t lineNumber)
    : SkAsmAbstractFunction(p, functName, lineNumber, SkAsmContextType::SKASM_FUNCT)
{
    setObjectName(prg, functName);
    retVal.setObjectName(this, "retVal");
}

SkVariant &SkAsmFunctionCtx::getReturnVar()
{
    return retVal;
}

// // // // // // // // // // // // // // // // // // // // //

SkAsmSlotCtx::SkAsmSlotCtx(SkAsmProgram *p, CStr *functName, uint64_t lineNumber)
    : SkAsmAbstractFunction(p, functName, lineNumber, SkAsmContextType::SKASM_SLOT)
{
    slt.setProgram(prg);
    slt.setContext(this);
}

SkAsmSlot *SkAsmSlotCtx::getAsmSlot()
{
    return &slt;
}

#endif
