TARGET = SkScript
TEMPLATE = lib

# insert sources

include(LibSkScript.pri)
include(module-src.pri)

DEFINES += SKSCRIPT_LIBRARY

# include libSkCore

LIBS += -lSkScript
INCLUDEPATH += /usr/local/include/LibSkScript
include(/usr/local/include/LibSkScript/LibSkScript.pri)

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkScript/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkNetProto.pri

headersDataFiles.path = /usr/local/include/LibSkScript/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles
