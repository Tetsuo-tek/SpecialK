/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKIMAGEENCODER_H
#define SKIMAGEENCODER_H

#if defined(ENABLE_CV)

#include <Core/Object/skflatobject.h>
#include <Core/Containers/skstring.h>

#include <opencv2/imgcodecs.hpp>
using namespace cv;

enum SkEncoderFormat
{
    SK_NONE_ENC,
    SK_JPEG_ENC,
    SK_PNG_ENC,
    SK_TIFF_ENC,
    SK_BMP_ENC,
    SK_PGM_ENC
};

class SkImageEncoder extends SkFlatObject
{
    public:
        SkImageEncoder();

        bool setup(SkEncoderFormat format, int parameter=-1);
        void reset();

        bool saveToBuffer(Mat &src, vector<uchar> &outBuf);
        bool saveToFile(Mat &src, CStr *filePath);

        SkEncoderFormat getFormat();
        CStr *getFormatExtension();
        vector<int> &getParameters();
        CStr *getMimeType();

    private:
        bool initialized;

        SkEncoderFormat fmt;
        SkString encExt;
        vector<int> encParams;
        SkString mime;
};

#endif

#endif // SKIMAGEENCODER_H
