#if defined(ENABLE_CV)

#include "skimageutils.h"
#include <Core/sklogmachine.h>

#define IMUTIL_EXCPT_CORPUS(FUNCTION, ...) \
    try \
    { \
        FUNCTION( __VA_ARGS__ ); \
    } \
     \
    catch(cv::Exception &e) \
    { \
        StaticError(#FUNCTION << " - " <<  e.msg); \
        return false; \
    } \
     \
    return true

bool SkImageUtils::doColorConversion(Mat &src, Mat &tgt, int cvCode)
{
    IMUTIL_EXCPT_CORPUS(cvtColor, src, tgt, cvCode);
}

bool SkImageUtils::doFlip(Mat &src, Mat &tgt, SkImageFlippingMode mode)
{
    if (mode == SkImageFlippingMode::SK_NONE_FLIPPING)
        return true;

    int value = -1;//SkImageFlippingMode::SK_TOTAL_FLIPPING

    if (mode == SkImageFlippingMode::SK_HORIZONTAL_FLIPPING)
        value = 1;

    else if (mode == SkImageFlippingMode::SK_VERTICAL_FLIPPING)
        value = 0;

    IMUTIL_EXCPT_CORPUS(flip, src, tgt, value);
}

bool SkImageUtils::doResize(Mat &src, Mat &tgt, Size &sz)
{
    int interpolation = 0;

    if (src.cols < sz.width || src.rows < sz.height)
        interpolation = INTER_LINEAR;

    else
        interpolation = INTER_AREA;

    IMUTIL_EXCPT_CORPUS(resize, src, tgt, sz, 0, 0, interpolation);
}

bool SkImageUtils::doRotation(Mat &src, Mat &tgt, int cvRotateCode)
{
    IMUTIL_EXCPT_CORPUS(rotate, src, tgt, cvRotateCode);
}

bool SkImageUtils::doHistogramEqualization(Mat &src, Mat &tgt)
{
    IMUTIL_EXCPT_CORPUS(equalizeHist, src, tgt);
}

bool SkImageUtils::doSplitToChannels(Mat &src, vector<Mat> &channels)
{
    IMUTIL_EXCPT_CORPUS(split, src, channels);
}

bool SkImageUtils::doMergeFromChannels(vector<Mat> &channels, Mat &tgt)
{
    IMUTIL_EXCPT_CORPUS(merge, channels, tgt);
}

#endif
