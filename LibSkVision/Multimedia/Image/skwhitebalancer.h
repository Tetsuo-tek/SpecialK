/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKWHITEBALANCER_H
#define SKWHITEBALANCER_H

#if defined(ENABLE_CV)

#include <Core/Object/skflatobject.h>
#include <Core/Containers/skstring.h>

#include <opencv2/xphoto/white_balance.hpp>
using namespace cv;

enum SkWbAlgorithm
{
    SK_SIMPLE_WB,
    SK_GRAYWORLD_WB,
    SK_LEARNING_WB
};

class SKVISION SkWhiteBalancer extends SkFlatObject
{
    public:
        SkWhiteBalancer();

        bool setup(Mat *source,
                   Mat *target,
                   SkWbAlgorithm algorithm,
                   CStr *learningModel=nullptr);

        void tick();

        SkWbAlgorithm algorithm();

    private:
        Ptr<xphoto::WhiteBalancer> w;
        SkWbAlgorithm a;

        bool initialized;
        Mat *src;
        Mat *tgt;
};

#endif

#endif // SKWHITEBALANCER_H
