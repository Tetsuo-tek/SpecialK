#if defined(ENABLE_CV)

#include "skwhitebalancer.h"
#include <Core/sklogmachine.h>

SkWhiteBalancer::SkWhiteBalancer()
{
    a = SkWbAlgorithm::SK_SIMPLE_WB;
    src = nullptr;
    tgt = nullptr;
}

bool SkWhiteBalancer::setup(Mat *source,
                            Mat *target,
                            SkWbAlgorithm algorithm,
                            CStr *learningModel)
{
    if (!source || !target)
    {
        FlatError("Source and target images are REQUIRED");
        return false;
    }

    src = source;
    tgt = target;
    a = algorithm;

    if (!w.empty())
        w.release();

    if (algorithm == SkWbAlgorithm::SK_SIMPLE_WB)
    {
        w = xphoto::createSimpleWB();
        FlatMessage("Enabled WhiteBalancer [mode: SIMPLE]");
    }

    else if (algorithm == SkWbAlgorithm::SK_GRAYWORLD_WB)
    {
        w = xphoto::createGrayworldWB();
        FlatMessage("Enabled WhiteBalancer [mode: GRAYWORLD]");
    }

    else
    {
        if (SkString::isEmpty(learningModel))
        {
            FlatError("WhiteBalancer model is REQUIRED");
            return false;
        }

        w = xphoto::createLearningBasedWB(learningModel);
        FlatMessage("Enabled WhiteBalancer [mode: LEARNING; model: " << learningModel << "]");
    }

    return true;
}

void SkWhiteBalancer::tick()
{
    if (w.empty())
    {
        FlatError("WhiteBalancer is NOT initialized");
        return;
    }

    try
    {
        w->balanceWhite(*src, *tgt);
    }

    catch(cv::Exception &e)
    {
        FlatError("Balancing white - " <<  e.msg);
    }
}

SkWbAlgorithm SkWhiteBalancer::algorithm()
{
    return a;
}

#endif
