﻿#if defined(ENABLE_CV)

#include "skobjectdetect.h"
#include <Core/sklogmachine.h>
#include "Multimedia/Image/skimageutils.h"

#if defined(ENABLE_CUDA)

SkObjectDetect::SkObjectDetect()
{
    aMin = Size(-1, -1);
    aMax = Size(-1, -1);
    initialized = false;
    equalizeHist = false;
    src = nullptr;
}

bool SkObjectDetect::setup(Mat *source,
                           CStr *cascadeXML,
                           Size areaMin,
                           Size areaMax,
                           bool equalize)
{
    if (!source)
    {
        initialized = false;
        FlatError("Source image is REQUIRED");
        return false;
    }

    if (areaMin.width <= 0 || areaMin.height <= 0)
        aMin = Size(-1, -1);

    else
        aMin = areaMin;

    if (areaMax.width <= 0 || areaMax.height <= 0)
        aMax = Size(-1, -1);

    else
    {
        if (areaMax.width <= aMin.width || areaMax.height <= aMin.height)
        {
            initialized = false;
            FlatError("Maximum area to detect MUST be greater than minimum area");
            return false;
        }

        aMax = areaMax;
    }

    try
    {
        cascade = cuda::CascadeClassifier::create(cascadeXML);
        cascade->setMinObjectSize(aMin);
        cascade->setMaxObjectSize(aMax);
    }

    catch(cv::Exception &e)
    {
        FlatError("CANNOT create cuda::CascadeClassifier - " <<  e.msg);
        return false;
    }

    equalizeHist = equalize;
    src = source;
    initialized = true;
    return true;
}

void SkObjectDetect::tick()
{
    if (!initialized)
    {
        FlatError("Detector is NOT initialized");
        return;
    }

    if (src->empty())
        return;

    frame.upload(*src);

    if (frame.channels() == 1)
        gray = frame;

    else if (frame.channels() == 3)
        cv::cuda::cvtColor(frame, gray, COLOR_BGR2GRAY);

    if (equalizeHist)
        cuda::equalizeHist(gray, gray);

    /*srcGPU->setFindLargestObject(findLargestObject);
    srcGPU->setScaleFactor(1.2);
    srcGPU->setMinNeighbors((filterRects || findLargestObject) ? 4 : 0);*/
    cascade->detectMultiScale(gray, facesBuf);
    cascade->convert(facesBuf, canvases);
}

vector<Rect> &SkObjectDetect::getDetectedCanvases()
{
    return canvases;
}

bool SkObjectDetect::isEmpty()
{
    return canvases.empty();
}

uint64_t SkObjectDetect::count()
{
    return canvases.size();
}

bool SkObjectDetect::isInitialized()
{
    return initialized;
}

#else
SkObjectDetect::SkObjectDetect()
{
    src = nullptr;

    initialized = false;

    aMin = Size(-1, -1);
    aMax = Size(-1, -1);
    equalizeHist = true;
}

bool SkObjectDetect::setup(Mat *source,
                             CStr *cascadeXML,
                             Size areaMin,
                             Size areaMax,
                             bool equalize)
{
    if (!source)
    {
        initialized = false;
        FlatError("Source image is REQUIRED");
        return false;
    }

    if (!cascade.load(cascadeXML))
    {
        initialized = false;
        FlatError("Cannot init cascade: " << cascadeXML);
        return false;
    }

    if (areaMin.width <= 0 || areaMin.height <= 0)
        aMin = Size(-1, -1);

    else
        aMin = areaMin;

    if (areaMax.width <= 0 || areaMax.height <= 0)
        aMax = Size(-1, -1);

    else
    {
        if (areaMax.width <= aMin.width || areaMax.height <= aMin.height)
        {
            initialized = false;
            FlatError("Maximum area to detect MUST be greater than minimum area");
            return false;
        }

        aMax = areaMax;
    }

    equalizeHist = equalize;
    src = source;
    initialized = true;
    return true;
}

void SkObjectDetect::tick()
{
    if (!initialized)
    {
        FlatError("Detector is NOT initialized");
        return;
    }

    if (src->channels() == 1)
        gray = *src;

    else if (src->channels() == 3)
    {
        if (!SkImageUtils::doColorConversion(*src, gray, COLOR_BGR2GRAY))
            return;
    }

    if (equalizeHist && !SkImageUtils::doHistogramEqualization(gray, gray))
        return;

    try
    {
        if (aMax.width == -1)
            cascade.detectMultiScale(gray, canvases, 1.1, 2, 0|CASCADE_SCALE_IMAGE, aMin);

        else
            cascade.detectMultiScale(gray, canvases, 1.1, 2, 0|CASCADE_SCALE_IMAGE, aMin, aMax);
    }

    catch(cv::Exception &e)
    {
        FlatError("Detecting - " <<  e.msg);
        return;
    }
}

vector<Rect> &SkObjectDetect::getDetectedCanvases()
{
    return canvases;
}

bool SkObjectDetect::isEmpty()
{
    return canvases.empty();
}

uint64_t SkObjectDetect::count()
{
    return canvases.size();
}

bool SkObjectDetect::isInitialized()
{
    return initialized;
}

#endif

#endif
