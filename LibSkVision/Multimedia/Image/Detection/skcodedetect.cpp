#if defined(ENABLE_CV)

#include "skcodedetect.h"
#include <Core/sklogmachine.h>

SkCodeDetect::SkCodeDetect()
{
    src = nullptr;
    initialized = false;
}

bool SkCodeDetect::setup(Mat *source)
{
    if (!source)
    {
        initialized = false;
        FlatError("Source image is REQUIRED");
        return false;
    }

    if (scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1) != 0)
    {
        initialized = false;
        FlatError("Cannot initialize zbar::ImageScanner");
        return false;
    }

    src = source;
    initialized = true;
    return true;
}

void SkCodeDetect::tick()
{
    if (!initialized)
    {
        FlatError("Detector is NOT initialized");
        return;
    }

    if (src->channels() == 1)
        grey = *src;

    else if (src->channels() == 3)
        SkImageUtils::doColorConversion(*src, grey, COLOR_BGR2GRAY);

    Image image(static_cast<uint>(grey.cols),
                static_cast<uint>(grey.rows),
                "Y800",
                grey.data,
                static_cast<ulong>(src->cols) * static_cast<ulong>(src->rows));

    /*int n = */scanner.scan(image);
    vector<SkDecodedCode> tempCodes;

    for(Image::SymbolIterator symbol=image.symbol_begin();
        symbol!=image.symbol_end();
        ++symbol)
    {
        SkDecodedCode code;

        code.type = symbol->get_type_name();
        code.data = symbol->get_data();

        for(uint i=0; i< static_cast<uint>(symbol->get_location_size()); i++)
            code.location.push_back(Point(symbol->get_location_x(i), symbol->get_location_y(i)));

        tempCodes.push_back(code);
    }

    codes = tempCodes;
}

vector<SkDecodedCode> &SkCodeDetect::getDetectedCodes()
{
    return codes;
}

bool SkCodeDetect::isEmpty()
{
    return codes.empty();
}

uint64_t SkCodeDetect::count()
{
    return codes.size();
}

bool SkCodeDetect::isInitialized()
{
    return initialized;
}

#endif
