#if defined(ENABLE_CV)

#include "skmotiondetect.h"
#include "Core/sklogmachine.h"

#define Y   0
#define U   1
#define V   2

SkMotionDetect::SkMotionDetect()
{
    totalBlocks = 0;
    initialized = false;
    threshold = 0.15;
    blocks = nullptr;
}

void SkMotionDetect::setup(Size imageSize, Size blockSize, double changingThreshold)
{
    imgSz = imageSize;
    threshold = changingThreshold;

    blockSz = blockSize;

    int currentY = 0;
    int currentX = 0;

    totalBlocks = ceil(((float) imgSz.width/blockSz.width)*((float) imgSz.height/blockSz.height));
    blocks = new SkMotionGridBlock [totalBlocks];

    for(ULong blockID=0; blockID<totalBlocks; blockID++)
    {
        blocks[blockID].id = blockID;

        blocks[blockID].rmse = 0.f;

        blocks[blockID].region.x = currentX;
        blocks[blockID].region.y = currentY;

        blocks[blockID].region.width = blockSz.width;
        blocks[blockID].region.height = blockSz.height;

        if (blocks[blockID].region.x + blocks[blockID].region.width > imgSz.width)
            blocks[blockID].region.width = imgSz.width - blocks[blockID].region.x;

        if (blocks[blockID].region.y + blocks[blockID].region.height > imgSz.height)
            blocks[blockID].region.height = imgSz.height - blocks[blockID].region.y;

        currentX += blockSz.width;

        if (currentX>imgSz.width-1)
        {
            currentX = 0;
            currentY += blockSz.height;
        }

        //cout << ch << " - " << blockID  << " " << blockSz[ch].width << " " << blocks[blockID].chans[ch].region << "\n";
    }

    initialized = true;
}

void SkMotionDetect::release()
{
    if (blocks)
    {
        delete [] blocks;
        blocks = nullptr;
    }

    initialized = false;
}

void SkMotionDetect::tick(Mat &frame)
{
    if (!initialized || frame.empty())
        return;

#if defined(ENABLE_CUDA)
    if (frame.channels() == 1)
        currY.upload(frame);

    else if (frame.channels() == 3)
    {
        currYUV.upload(frame);
        cv::cuda::cvtColor(currYUV, currYUV, COLOR_BGR2YUV);
        vector<GpuMat> channels;
        cv::cuda::split(currYUV, channels);
        currY = channels[0];
    }

    currY.convertTo(currY, CV_32FC1, 1./255.);

    if (!prevY.empty())
    {
        cuda::absdiff(currY, prevY, diffY);
        diffY.download(diffY_CPU);
        grabBlocks();
    }

    currY.copyTo(prevY);

#else

    if (frame.channels() == 1)
        frame.copyTo(currY);

    else if (frame.channels() == 3)
    {
        SkImageUtils::doColorConversion(frame, currYUV, COLOR_BGR2YUV);
        vector<Mat> channels;
        split(currYUV, channels);

        channels[Y].copyTo(currY);
    }

    if (!prevY.empty())
        grabBlocks(currY, prevY);

    currY.copyTo(prevY);

#endif
}

#if defined(ENABLE_CUDA)
void SkMotionDetect::grabBlocks()
{
    changedBlocks.clear();

    Mat diff8QBlock;

    int minX = 10000;
    int minY = 10000;
    int maxX = 0;
    int maxY = 0;

    for(ULong blockID=0; blockID<totalBlocks; blockID++)
    {
        SkMotionGridBlock &blk = blocks[blockID];

        diff8QBlock = diffY_CPU(blk.region);
        float mse = cv::mean(diff8QBlock)[0];
        blk.rmse = std::sqrt(mse);

        //

        if (blk.rmse > threshold)
        {
            Point blkCenter(blk.region.x+(blk.region.width/2), blk.region.y+(blk.region.height/2));

            if (blkCenter.x < minX)
                minX = blkCenter.x;

            else if (blkCenter.x > maxX)
                maxX = blkCenter.x;

            if (blkCenter.y < minY)
                minY = blkCenter.y;

            else if (blkCenter.y > maxY)
                maxY = blkCenter.y;

            changedBlocks << blk.region;
        }
    }

    if (changedBlocks.isEmpty())
    {
        currentCanvas = Rect(0, 0, 0, 0);
        return;
    }

    currentCanvas.x = minX;
    currentCanvas.y = minY;
    currentCanvas.width = maxX-minX;
    currentCanvas.height = maxY-minY;
}

#else
void SkMotionDetect::grabBlocks(Mat &curr, Mat &prev)
{
    changedBlocks.clear();

    Mat currBlock;
    Mat prevBlock;
    Mat diff8QBlock;

    int minX = 10000;
    int minY = 10000;
    int maxX = 0;
    int maxY = 0;

    for(ULong blockID=0; blockID<totalBlocks; blockID++)
    {
        SkMotionGridBlock &blk = blocks[blockID];

        currBlock = curr(blk.region);
        currBlock.convertTo(currBlock, CV_32FC1, 1./255.);

        prevBlock = prev(blk.region);
        prevBlock.convertTo(prevBlock, CV_32FC1, 1./255.);

        absdiff(currBlock, prevBlock, diff8QBlock);

        float mse = cv::mean(diff8QBlock)[0];
        blk.rmse = std::sqrt(mse);

        if (blk.rmse > threshold)
        {
            Point blkCenter(blk.region.x+(blk.region.width/2), blk.region.y+(blk.region.height/2));

            if (blkCenter.x < minX)
                minX = blkCenter.x;

            else if (blkCenter.x > maxX)
                maxX = blkCenter.x;

            if (blkCenter.y < minY)
                minY = blkCenter.y;

            else if (blkCenter.y > maxY)
                maxY = blkCenter.y;

            changedBlocks << blk.region;
        }
    }

    if (changedBlocks.isEmpty())
    {
        currentCanvas = Rect(0, 0, 0, 0);
        return;
    }

    currentCanvas.x = minX;
    currentCanvas.y = minY;
    currentCanvas.width = maxX-minX;
    currentCanvas.height = maxY-minY;
}

#endif

Rect &SkMotionDetect::getDetectedCanvas()
{
    return currentCanvas;
}

SkList<Rect> &SkMotionDetect::getDetectedBlocks()
{
    return changedBlocks;
}

bool SkMotionDetect::isEmpty()
{
    return changedBlocks.isEmpty();
}

ULong SkMotionDetect::SkMotionDetect::count()
{
    return changedBlocks.count();
}

ULong SkMotionDetect::total()
{
    return totalBlocks;
}

bool SkMotionDetect::isInitialized()
{
    return initialized;
}

#endif
