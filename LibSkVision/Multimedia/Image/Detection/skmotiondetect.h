#ifndef SKMOTIONDETECT_H
#define SKMOTIONDETECT_H

#if defined(ENABLE_CV)

#include "Multimedia/Image/skimageutils.h"
#include "Core/Containers/sklist.h"

#if defined(ENABLE_CUDA)
    #include <opencv2/cudaobjdetect.hpp>
    #include "opencv2/cudaimgproc.hpp"
    #include "opencv2/cudawarping.hpp"
    #include <opencv2/cudaarithm.hpp>
    using namespace cv::cuda;
#endif

struct SkMotionGridBlock
{
    int id;
    Rect region;
    float rmse;
};

class SKVISION SkMotionDetect extends SkFlatObject
{
    bool initialized;

    Size imgSz;
    double threshold;

#if defined(ENABLE_CUDA)
    GpuMat currYUV;
    GpuMat prevY;
    GpuMat currY;
    GpuMat diffY;

    Mat diffY_CPU;

#else
    Mat currYUV;
    Mat prevY;
    Mat currY;

#endif

    ULong totalBlocks;
    Size blockSz;

    SkMotionGridBlock *blocks;

    Rect currentCanvas;
    SkList<Rect> changedBlocks;//ONLY Y

    public:
        SkMotionDetect();

        void setup(Size imageSize, Size blockSize, double changingThreshold);
        void release();

        void tick(Mat &frame);

        Rect &getDetectedCanvas();
        SkList<Rect> &getDetectedBlocks();

        bool isEmpty();
        ULong count();
        ULong total();
        bool isInitialized();

    private:
#if defined(ENABLE_CUDA)
        void grabBlocks();
#else
        void grabBlocks(Mat &curr, Mat &prev);
#endif
};

#endif

#endif // SKMOTIONDETECT_H
