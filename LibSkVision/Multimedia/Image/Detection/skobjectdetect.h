/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKOBJECTDETECT_H
#define SKOBJECTDETECT_H

#if defined(ENABLE_CV)

#include <Core/Object/skflatobject.h>
#include <Core/Containers/skstring.h>

#if defined(ENABLE_CUDA)

#include <opencv2/cudaobjdetect.hpp>
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"

using namespace cv;
using namespace cv::cuda;

class SKVISION SkObjectDetect extends SkFlatObject
{
    bool initialized;

    //ONLY LBP clascades work on CUDA
    Ptr<cuda::CascadeClassifier> cascade;
    Size aMin;
    Size aMax;
    bool equalizeHist;

    vector<Rect> canvases;

    Mat *src;

    GpuMat frame;
    GpuMat gray;
    GpuMat facesBuf;

    public:
        SkObjectDetect();

        //IF SOURCE IS MONOCHROM IT WILL NOT PERFORM THE CONVERSION
        bool setup(Mat *source,
                   CStr *cascadeXML,
                   Size areaMin=Size(-1,-1),
                   Size areaMax=Size(-1,-1),
                   bool equalize=true);

        void tick();

        vector<Rect> &getDetectedCanvases();
        bool isEmpty();
        uint64_t count();

        bool isInitialized();
};

#else
#include <opencv2/objdetect.hpp>
using namespace cv;

class SKVISION SkObjectDetect extends SkFlatObject
{
    bool initialized;

    CascadeClassifier cascade;
    Size aMin;
    Size aMax;
    bool equalizeHist;

    vector<Rect> canvases;

    Mat *src;
    Mat gray;

    public:
        SkObjectDetect();

        //IF SOURCE IS MONOCHROM IT WILL NOT PERFORM THE CONVERSION
        bool setup(Mat *source,
                   CStr *cascadeXML,
                   Size areaMin=Size(-1,-1),
                   Size areaMax=Size(-1,-1),
                   bool equalize=true);

        void tick();

        vector<Rect> &getDetectedCanvases();
        bool isEmpty();
        uint64_t count();

        bool isInitialized();

    private:
};

#endif

#endif

#endif // SKOBJECTDETECTION_H
