/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKCODEDETECT_H
#define SKCODEDETECT_H

#if defined(ENABLE_CV)

#include "Multimedia/Image/skimageutils.h"
#include <Core/Containers/skstring.h>

#include <zbar.h>
using namespace zbar;

typedef struct
{
  SkString type;
  SkString data;
  vector<Point> location;
} SkDecodedCode;

//PERHAPS IT IS TIME TO trash THIS CLASS

class SkCodeDetect extends SkFlatObject
{
    bool initialized;

    ImageScanner scanner;
    vector<SkDecodedCode> codes;

    Mat *src;
    Mat grey;

    public:
        SkCodeDetect();

        bool setup(Mat *source);

        void tick();

        vector<SkDecodedCode> &getDetectedCodes();
        bool isEmpty();
        uint64_t count();

        bool isInitialized();
};

#endif

#endif // SKCODEDETECT_H
