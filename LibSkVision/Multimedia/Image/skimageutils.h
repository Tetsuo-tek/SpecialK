/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKIMAGEUTILS_H
#define SKIMAGEUTILS_H

#if defined(ENABLE_CV)

#include "Core/Object/skflatobject.h"
#include <opencv2/imgproc.hpp>

using namespace cv;

enum SkImageFlippingMode
{
    SK_NONE_FLIPPING,
    SK_HORIZONTAL_FLIPPING,
    SK_VERTICAL_FLIPPING,
    SK_TOTAL_FLIPPING
};

class SKVISION SkImageUtils extends SkFlatObject
{
    public:
        static bool doColorConversion(Mat &src, Mat &tgt, int cvCode/*, ...*/);
        static bool doFlip(Mat &src, Mat &tgt, SkImageFlippingMode mode);
        static bool doResize(Mat &src, Mat &tgt, Size &sz);
        static bool doRotation(Mat &src, Mat &tgt, int cvRotateCode);
        static bool doHistogramEqualization(Mat &src, Mat &tgt);
        static bool doSplitToChannels(Mat &src, vector<Mat> &channels);
        static bool doMergeFromChannels(vector<Mat> &channels, Mat &tgt);
};

#endif

#endif // SKIMAGEUTILS_H
