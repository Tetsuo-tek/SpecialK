/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKIMAGECAPTURE_H
#define SKIMAGECAPTURE_H

#if defined(ENABLE_CV)

#include "Core/Object/skobject.h"

#include <opencv2/videoio.hpp>
#include "skimageutils.h"
using namespace cv;

struct SkVideoCaptureProperties
{
    bool enabled;
    //Not using resolution it will let the internal VideoCapture to set default
    //If it is used and it is not valid for the source it will be restored to the valid value
    Size resolution;
    bool forceResolution;

    //Setting fps the eventLoop() will be managed, setting FAST-ZONE interval
    //and attaching tick to it
    int fps;
    bool forceFps;

    SkString fourCC;

    //Requires to set fps valid value (greater than zero)
    bool realTimeMode=false;

    SkImageFlippingMode flipMode=SkImageFlippingMode::SK_NONE_FLIPPING;

    //OpenCv backend apis
    int apiReference=VideoCaptureAPIs::CAP_ANY;

    SkVideoCaptureProperties()
    {
        enabled = false;
        resolution = Size(-1, -1);
        forceResolution = false;
        fps = 0;
        forceFps = false;
        realTimeMode = false;
        flipMode = SkImageFlippingMode::SK_NONE_FLIPPING;
        apiReference = VideoCaptureAPIs::CAP_V4L2;
    }
};

class SKVISION SkImageCapture extends SkObject
{
    public:
        Constructor(SkImageCapture, SkObject);

        void setup(int device, SkVideoCaptureProperties &cfg);
        void setup(CStr *path, SkVideoCaptureProperties &cfg);

        bool setCustomCvProperty(int cvPropertyName, double value);
        double getCustomCvProperty(int cvPropertyName);

        void setEnabled(bool enable);
        bool isEnabled();

        bool open();
        bool isOpen();

        void close();

        Size getResolution();
        int getWidth();
        int getHeight();
        int getFps();
        Mat &getFrame();

        //If fps is not set it will return 0
        ulong getTickTimeMicros();

        SkString getBackendName();

        //IF props.fps IS VALID (>0), THIS HAPPENs ON BACKGROUND
        bool capture();

        void changeFlipMode(SkImageFlippingMode m);

        Slot(tick);
        Signal(captured);

    private:
        VideoCapture vc;
        SkVideoCaptureProperties props;

        int dev;
        SkString source;
        Mat frame;

        void changeFps();

    protected:
        virtual void onTick(){}
};

#endif

#endif // SKIMAGECAPTURE_H
