#if defined(ENABLE_CV)

#include "skimagedecoder.h"
#include <Core/sklogmachine.h>
#include <Core/System/Filesystem/skfsutils.h>

SkImageDecoder::SkImageDecoder()
{
    tgt = nullptr;
    flag = IMREAD_UNCHANGED ;
    initialized = false;
}

bool SkImageDecoder::setup(Mat *target, int cvFlag)
{
    if (!target)
    {
        FlatError("Target image is REQUIRED");
        return false;
    }

    tgt = target;
    flag = cvFlag;
    initialized = true;
    return true;
}

bool SkImageDecoder::loadFromBuffer(CStr *data, uint64_t sz)
{
    if (!initialized)
    {
        FlatError("Decoder is NOT initialized");
        return false;
    }

    vector<uint8_t> v(data, data+sz);

    try
    {
        *tgt = imdecode(v, flag);
    }

    catch(cv::Exception &e)
    {
        FlatError("Decoding from buffer [size: " << sz << " B] - " <<  e.msg);
        return false;
    }

    return true;
}

bool SkImageDecoder::loadFromFile(CStr *filePath)
{
    if (!initialized)
    {
        FlatError("Decoder is NOT initialized");
        return false;
    }

    try
    {
        *tgt = imread(filePath, flag);
    }

    catch(cv::Exception &e)
    {
        FlatError("Decoding from file [file: " << filePath << "; size: " << SkFsUtils::size(filePath) << " B] - " <<  e.msg);
        return false;
    }

    return true;
}

#endif
