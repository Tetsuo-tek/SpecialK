#if defined(ENABLE_CV)

#include "skimageencoder.h"
#include <Core/sklogmachine.h>

SkImageEncoder::SkImageEncoder()
{
    fmt = SkEncoderFormat::SK_NONE_ENC;
    initialized = false;
}

bool SkImageEncoder::setup(SkEncoderFormat format, int parameter)
{
    if (format == SkEncoderFormat::SK_JPEG_ENC)
    {
        int quality = parameter;

        if (quality < 0 || quality > 100)
        {
            FlatError("Jpeg encoder requires quality parameter [0..100]");
            return false;
        }

        encExt = ".jpg";
        encParams = vector<int>(2);
        encParams[0] = IMWRITE_JPEG_QUALITY;
        encParams[1] = quality;
        mime = "image/jpeg";
    }

    else if (format == SkEncoderFormat::SK_PNG_ENC)
    {
        int compression = parameter;

        if (compression < 0 || compression > 9)
        {
            FlatError("Jpeg encoder requires compression parameter [0..9]");
            return false;
        }

        encExt = ".png";
        encParams = vector<int>(2);
        encParams[0] = IMWRITE_PNG_COMPRESSION;
        encParams[1] = compression;
        mime = "image/png";
    }

    else if (format == SkEncoderFormat::SK_TIFF_ENC)
    {
        encExt = ".tiff";
        encParams.clear();
        mime = "image/tiff";
    }

    else if (format == SkEncoderFormat::SK_BMP_ENC)
    {
        encExt = ".bmp";
        encParams.clear();
        mime = "image/bmp";
    }

    else if (format == SkEncoderFormat::SK_PGM_ENC)
    {
        encExt = ".pgm";
        encParams.clear();
        mime = "image/x-portable-graymap";
    }

    initialized = true;
    return true;
}

void SkImageEncoder::reset()
{
    fmt = SkEncoderFormat::SK_NONE_ENC;
    encExt.clear();
    encParams.clear();
    mime.clear();
    initialized = false;
}

bool SkImageEncoder::saveToBuffer(Mat &src, vector<uchar> &outBuf)
{
    if (!initialized)
    {
        FlatError("Encoder is NOT initialized");
        return false;
    }

    try
    {
        imencode(encExt, src, outBuf, encParams);
    }

    catch(cv::Exception &e)
    {
        FlatError("Encoding to buffer [fmt: " << &encExt.c_str()[1] << "] - " <<  e.msg);
        return false;
    }

    return true;
}

bool SkImageEncoder::saveToFile(Mat &src, CStr *filePath)
{
    if (!initialized)
    {
        FlatError("Encoder is NOT initialized");
        return false;
    }

    SkString f = filePath;

    if (!f.endsWith(encExt.c_str()))
        f.append(encExt);

    try
    {
        imwrite(f, src, encParams);
    }

    catch(cv::Exception &e)
    {
        FlatError("Encoding to file [" << f << "] - " <<  e.msg);
        return false;
    }

    return true;
}

SkEncoderFormat SkImageEncoder::getFormat()
{
    return fmt;
}

CStr *SkImageEncoder::getFormatExtension()
{
    return encExt.c_str();
}

vector<int> &SkImageEncoder::getParameters()
{
    return encParams;
}

CStr *SkImageEncoder::getMimeType()
{
    return mime.c_str();
}

#endif
