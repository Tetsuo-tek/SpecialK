#if defined(ENABLE_CV)

#include "skimagegradient.h"
#include "Core/sklogmachine.h"

SkImageGradient::SkImageGradient()
{
    src = nullptr;
    tgt = nullptr;
    normAlpha = 0.;
    normDelta = 0.;
}

bool SkImageGradient::setup(Mat *source, Mat *target, double alpha, double delta, int gradient)
{
    src = source;
    tgt = target;
    grd = gradient;
    normAlpha = alpha;
    normDelta = delta;
    return true;
}

void SkImageGradient::tick()
{
    if (src->type() != CV_32FC1)
    {
        FlatError("Making gradient colorMap require source image of CV_32FC1 type");
        return;
    }

    try
    {
        normalize(*src, normalized, 0, 1, NORM_MINMAX, CV_32FC1);
        /*normalized = *src;
        double minVal, maxVal;
        minMaxLoc(normalized, &minVal, &maxVal);*/
        //normalized.convertTo(normalized8u, CV_8U, 255.0/(maxVal - minVal), -minVal);
        normalized.convertTo(normalized8u, CV_8UC1, 255);
        applyColorMap(normalized8u, *tgt, grd);
    }

    catch(cv::Exception &e)
    {
        FlatError("Making colorMap - " <<  e.msg);
    }
}

#endif
