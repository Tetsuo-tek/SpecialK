/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKMAT_H
#define SKMAT_H

#if defined(ENABLE_CV)

#include "skimageutils.h"

class SKVISION SkMat extends SkFlatObject
{
    Mat im;

    public:
        SkMat(Mat *image=nullptr);

        void set(const Mat &image);
        Mat &get();

        void clear();
        void copyTo(Mat &target);

        bool isEmpty();

        uint64_t channels();

        Size size();
        int width();
        int height();

        int cvType();
        int depth();

        bool toGrayScale();
        bool convert(int cvCode);
        bool flip(SkImageFlippingMode mode);
        bool resize(Size &sz);
        bool rotate(int cvRotateCode);
        bool equalizeHistogram();
        bool splitToChannels(vector<Mat> &channels);
        bool mergeFromChannels(vector<Mat> &channels);
        bool region(Rect &r, Mat &target);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//"WxH" -> Size
bool parseSize(CStr *label, CStr *s, Size *sz);

//"X,Y" -> Point
bool parsePoint(CStr *label, CStr *s, Point *p);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKMAT_H
