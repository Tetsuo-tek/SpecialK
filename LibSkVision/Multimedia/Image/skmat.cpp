#if defined(ENABLE_CV)

#include "skmat.h"
#include "Core/sklogmachine.h"
#include "Core/Containers/skstringlist.h"

SkMat::SkMat(Mat *image)
{
    if (image)
        im = *image;
}

void SkMat::set(const Mat &image)
{
    im = image.clone();
}

Mat &SkMat::get()
{
    return im;
}

void SkMat::clear()
{
    im = Mat();
}

void SkMat::copyTo(Mat &target)
{
    im.copyTo(target);
}

bool SkMat::isEmpty()
{
    return im.empty();
}

uint64_t SkMat::channels()
{
    return im.channels();
}

Size SkMat::size()
{
    return im.size();
}

int SkMat::width()
{
    return im.cols;
}

int SkMat::height()
{
    return im.rows;
}

int SkMat::cvType()
{
    return im.type();
}

int SkMat::depth()
{
    /*
        CV_8U - 8-bit unsigned integers ( 0..255 )
        CV_8S - 8-bit signed integers ( -128..127 )
        CV_16U - 16-bit unsigned integers ( 0..65535 )
        CV_16S - 16-bit signed integers ( -32768..32767 )
        CV_32S - 32-bit signed integers ( -2147483648..2147483647 )
        CV_32F - 32-bit floating-point numbers ( -FLT_MAX..FLT_MAX, INF, NAN )
        CV_64F - 64-bit floating-point numbers ( -DBL_MAX..DBL_MAX, INF, NAN )
    */

    return im.depth();
}

bool SkMat::toGrayScale()
{
    if (im.type() == CV_8UC3)
    {
        //MUST BE 'BGR' IN THIS CASE
        return SkImageUtils::doColorConversion(im, im, COLOR_BGR2GRAY);
    }

    return false;
}
bool SkMat::convert(int cvCode)
{
    return SkImageUtils::doColorConversion(im, im, cvCode);
}

bool SkMat::flip(SkImageFlippingMode mode)
{
    return SkImageUtils::doFlip(im, im, mode);
}

bool SkMat::resize(Size &sz)
{
    return SkImageUtils::doResize(im, im, sz);
}

bool SkMat::rotate(int cvRotateCode)
{
    return SkImageUtils::doRotation(im, im, cvRotateCode);
}

bool SkMat::equalizeHistogram()
{
    return SkImageUtils::doHistogramEqualization(im, im);
}

bool SkMat::splitToChannels(vector<Mat> &channels)
{
    return SkImageUtils::doSplitToChannels(im, channels);
}

bool SkMat::mergeFromChannels(vector<Mat> &channels)
{
    return SkImageUtils::doMergeFromChannels(channels, im);
}

bool SkMat::region(Rect &r, Mat &target)
{
    try
    {
        target = im(r);
    }

    catch(cv::Exception &e)
    {
        FlatError("Extracting detect canvas - " <<  e.msg);
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool parseSize(CStr *label, CStr *s, Size *sz)
{
    SkStringList l;

    SkString str(s);
    str.split('x', l);

    if (l.count() != 2)
    {
        StaticError("'" << label << "' is NOT valid [must be WxH]: " << s);
        return false;
    }

    sz->width = l[0].toInt();
    sz->height = l[1].toInt();

    if (sz->width < 0 || sz->height < 0)
    {
        StaticError("'" << label << "' is NOT valid [minimum would be: 0x0]");
        return false;
    }

    return true;
}

bool parsePoint(CStr *label, CStr *s, Point *p)
{
    SkStringList l;

    SkString str(s);
    str.split(',', l);

    if (l.count() != 2)
    {
        StaticError("'" << label << "' is NOT valid [must be (x,y)]: " << s);
        return false;
    }

    p->x = l[0].toInt();
    p->y = l[1].toInt();

    if (p->x < 0 || p->y < 0)
    {
        StaticError("'" << label << "' is NOT valid [minimum would be: (0,0)]");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
