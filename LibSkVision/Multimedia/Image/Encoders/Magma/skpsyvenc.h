#ifndef SKPSYVENC_H
#define SKPSYVENC_H

#if defined(ENABLE_CV)

#include "Multimedia/Magma/skabstractmagmaencoder.h"
#include "Multimedia/Magma/skabstractmagmadecoder.h"
#include "Multimedia/Image/skimageutils.h"
#include "Multimedia/Image/skimagedecoder.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkPsyVEncPacketType
{
    NonePck,
    BlocksPck,
    RefreshPck
};

enum SkPsyVCodecType
{
    NoneCodec,
    YuvCodec,
    YuvRatioCodec,
    PsychoRatioCodec
};

enum SkPsyYuvRatio
{
    Yuv444,
    Yuv422,
    Yuv421,
    Yuv411
};

struct SkPsy_BLOCK
{
    int id;
    int level;//0 means root
    Rect region;
    SkPsy_BLOCK *parent;
    SkPsy_BLOCK *children;
    int childrenCount;
    Mat diff8Q;
    float rmse;
};

struct SkPsyVEncChanBlock
{
    Rect region;
    float rmse;
    bool changed;
};

struct SkPsyVEncBlock
{
    int id;
    SkPsyVEncChanBlock chans[3];
};

struct SkPsyVEncChanPacket
{
    Mat frame;
    SkQueue<Rect> blocks;
    Mat diff8Q;
};

struct SkPsyVEncPacket extends SkFlatObject
{
    Mat frame;
    SkDataBuffer encoded;
    SkPsyVEncChanPacket chans[3];
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyVEnc extends SkAbstractMagmaEncoder
{
    SkPsyVCodecType cdc;
    Size imgSz;
    UInt fps;
    UInt keyFrameCount;
    UInt diffMemoryCount;
    UInt inputFramesCounter;

    SkPsyVEncPacket currentPck;

    Mat currYUV;

    Mat prevY;
    Mat prevU;
    Mat prevV;

    Mat prev8QY;
    Mat prev8QU;
    Mat prev8QV;

    vector<int> encParams;
    vector<uchar> outBuf;

    int bits;
    SkPsyYuvRatio yuvRatio;
    int yuvRatioValues[3];

    int totalBlocks;
    Size blockSz[3];

    SkPsy_BLOCK rootBlock;

    SkBufferDevice *pckWriter;
    SkBufferDevice *subPckWriter;
    ULong pckID;

    SkPsyVEncBlock *blocks;

    public:
        Constructor(SkPsyVEnc, SkAbstractMagmaEncoder);

        bool setup(Size imageSize,
                   UInt framesPerSecond,
                   SkPsyVCodecType codec,
                   SkPsyYuvRatio yuvChansRatio,
                   UInt encodingBits,
                   float quality);

        bool open(SkDataBuffer &header)                                 override;
        void close()                                                    override;

        void setData(void *bgrImageData)                                override;
        bool encode(SkDataBuffer &output)                               override;

        int ratioValueU();
        int ratioValueV();

        static void ratiosValuesUV(SkPsyYuvRatio yuvChansRatio, int *yuvRatioValues);

    private:
        void buildBlock(SkPsy_BLOCK &block, int &blockID);
        void checkBlock(Mat &frame, Mat &prev, SkPsy_BLOCK &block, double threshold);

        void resizeUV();
        void splitYUV();
        void grabBlocks(Mat &prev, Mat &prev8Q, int chIndex);
        void writeBlocks(int chIndex, double threshold);
        void writeJPEG(Mat &i);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*#include "skpsyvencth.h"

class SkPsyVEnc extends SkAbstractMagmaEncoder
{
    public:
        Constructor(SkPsyVEnc, SkAbstractMagmaEncoder);

        bool setup(Size imageSize,
                   UInt framesPerSecond,
                   SkPsyVCodecType codec,
                   SkPsyYuvRatio yuvChansRatio,
                   UInt encodingBits,
                   float quality);

        bool open(SkDataBuffer &header)                                 override;
        void close()                                                    override;

        void setData(void *bgrImageData)                                override;
        bool encode(SkDataBuffer &output)                               override;

        Mat &frame_BGR();

        Mat &frame_Y();
        Mat &frame_U();
        Mat &frame_V();

        Mat &frame_8QY();

        SkQueue<Rect> &blockRegionsY();
        SkQueue<Rect> &blockRegionsU();
        SkQueue<Rect> &blockRegionsV();

        int ratioValueU();
        int ratioValueV();

        float aseMaximum();

        float cpuLoadAvg();
        float jobTimeAvg();
        ULong queuedPcks();

        Slot(encoderStarted);
        Slot(loadEncoded);

    private:
        SkPsyVCodecType cdc;
        SkPsyYuvRatio yuvRatio;
        int yuvRatioValues[3];
        SkPsyVEncTh *encoder;

        int bits;
        float max_q;
        float quality;

        UInt fps;
        Size imgSz;

        SkPsyVEncChanPacket y;
        SkPsyVEncChanPacket u;
        SkPsyVEncChanPacket v;

        Size currBlkSz;
        float seaThreshold;
        float seaMax;
        int compression;

        SkQueue<SkPsyVEncPacket *> oPcks;
        Mat img;

        SkQueue<Rect> blocks;
};*/

#endif

#endif // SKPSYVENC_H
