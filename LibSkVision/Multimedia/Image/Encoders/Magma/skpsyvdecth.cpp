﻿#if defined(ENABLE_CV)

#include "skpsyvdecth.h"

#include "Core/System/skbufferdevice.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyVDecTicker, SkObject)
{
    fps = 0;
    cdc = NoneCodec;
    bits = 0;
    lastPckType = NonePck;

    SlotSet(decode);
    SignalSet(decoded);

    pckReader = new SkBufferDevice(this);
}

void SkPsyVDecTicker::init(SkPsyVCodecType codec, UInt encodingBits, Size resolution, UInt framesPerSecond)
{
    cdc = codec;
    bits = encodingBits;
    res = resolution;
    fps = framesPerSecond;
}

SlotImpl(SkPsyVDecTicker, decode)
{
    SilentSlotArgsWarning();

    SkPsyVDecPacket *pck = dynamic_cast<SkPsyVDecPacket *>(referer);
    pckReader->open(pck->encoded, SkBufferDeviceMode::BVM_ONLYREAD);

    pck->pckID = pckReader->readUInt64();

    if (cdc == YuvCodec)
    {
        readJPEG(pck->y);
        readJPEG(pck->u);
        readJPEG(pck->v);

        if (pck->u.size() != res)
            SkImageUtils::doResize(pck->u, pck->u, res);

        if (pck->v.size() != res)
            SkImageUtils::doResize(pck->v, pck->v, res);

        mergeYUV(pck);
    }

    else if (cdc == YuvRatioCodec)
    {
        lastPckType = static_cast<SkPsyVEncPacketType>(pckReader->readUInt8());

        if (lastPckType == RefreshPck)
        {
            readJPEG(prevY);
            readJPEG(prevU);
            readJPEG(prevV);

            prevY.copyTo(pck->y);
            prevU.copyTo(pck->u);
            prevV.copyTo(pck->v);

            if (pck->u.size() != res)
                SkImageUtils::doResize(pck->u, pck->u, res);

            if (pck->v.size() != res)
                SkImageUtils::doResize(pck->v, pck->v, res);

            mergeYUV(pck);
        }

        else if (lastPckType == BlocksPck)
        {
            if (prevY.empty())
            {
                //need yuvRatios
                /*prevY = Mat(res, CV_8UC1, Scalar(0));
                prevU = Mat(res, CV_8UC1, Scalar(0));
                prevV = Mat(res, CV_8UC1, Scalar(0));*/

                delete pck;
                pckReader->close();
                return;
            }

            readBlocks(prevY);
            readBlocks(prevU);
            readBlocks(prevV);

            prevY.copyTo(pck->y);
            prevU.copyTo(pck->u);
            prevV.copyTo(pck->v);

            if (pck->u.size() != res)
                SkImageUtils::doResize(pck->u, pck->u, res);

            if (pck->v.size() != res)
                SkImageUtils::doResize(pck->v, pck->v, res);

            mergeYUV(pck);
        }

        lastPckType = NonePck;
    }

    else if (cdc == PsychoRatioCodec)
    {
    }

    pckReader->close();

    decoded(pck);
}

void SkPsyVDecTicker::mergeYUV(SkPsyVDecPacket *pck)
{
    vector<Mat> channels;
    channels.push_back(pck->y);
    channels.push_back(pck->u);
    channels.push_back(pck->v);

    SkImageUtils::doMergeFromChannels(channels, pck->frame);
    SkImageUtils::doColorConversion(pck->frame, pck->frame, COLOR_YUV2BGR);
}

void SkPsyVDecTicker::readBlocks(Mat &channel)
{
    int count = pckReader->readUInt16();

    for(int i=0; i<count; i++)
    {
        Rect r;
        r.x = pckReader->readUInt16();
        r.y = pckReader->readUInt16();
        r.width = pckReader->readUInt16();
        r.height = pckReader->readUInt16();

        Mat block = channel(r);
        readJPEG(block);
    }
}

void SkPsyVDecTicker::readJPEG(Mat &i)
{
    ULong sz = pckReader->readUInt32();
    pckReader->read(&inBuf, sz);
    vector<uint8_t> data(inBuf.data(), inBuf.data() + inBuf.size());
    imdecode(data, IMREAD_GRAYSCALE).copyTo(i);
    inBuf.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyVDecTh, SkThread)
{
    fps = 0;
    cdc = NoneCodec;
    bits = 0;

    ticker = nullptr;

    setObjectName("MagmaVideoDecTh");
}

void SkPsyVDecTh::init(SkPsyVCodecType codec, UInt encodingBits, Size resolution, UInt framesPerSecond, float period)
{
    cdc = codec;
    bits = encodingBits;
    res = resolution;
    fps = framesPerSecond;

    float tickTimePeriod = period*1000000;
    setLoopIntervals(tickTimePeriod, eventLoop()->getSlowInterval(), SK_TIMEDLOOP_RT);
}

bool SkPsyVDecTh::customRunSetup()
{
    ticker = new SkPsyVDecTicker;
    ticker->init(cdc, bits, res, fps);

    return true;
}

void SkPsyVDecTh::customClosing()
{
    ticker = nullptr;
}

SkPsyVDecTicker *SkPsyVDecTh::getTicker()
{
    return ticker;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
