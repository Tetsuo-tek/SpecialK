#ifndef SKPSYVENCTH_H
#define SKPSYVENCTH_H

/*#if defined(ENABLE_CV)

#include "Core/System/Thread/skthread.h"

#include "Multimedia/Image/skimageutils.h"
#include "Multimedia/Image/skimageencoder.h"
#include "Multimedia/Image/skimagedecoder.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkBufferDevice;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkPsyVEncPacketType
{
    NonePck,
    BlocksPck,
    RefreshPck
};

enum SkPsyVCodecType
{
    NoneCodec,
    YuvCodec,
    YuvRatioCodec,
    PsychoRatioCodec
};

enum SkPsyYuvRatio
{
    Yuv444,
    Yuv422,
    Yuv421,
    Yuv411
};

struct SkPsy_BLOCK
{
    int id;
    int level;//0 means root
    Rect region;
    SkPsy_BLOCK *parent;
    SkPsy_BLOCK *children;
    int childrenCount;
    Mat diff8Q;
    float rmse;
};

struct SkPsyVEncChanBlock
{
    Rect region;
    float rmse;
    bool changed;
};

struct SkPsyVEncBlock
{
    int id;
    SkPsyVEncChanBlock chans[3];
};

struct SkPsyVEncChanPacket
{
    Mat frame;
    SkQueue<Rect> blocks;
    Mat diff8Q;
};

struct SkPsyVEncPacket extends SkFlatObject
{
    Mat frame;
    SkDataBuffer encoded;
    SkPsyVEncChanPacket chans[3];
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyVEncTicker extends SkObject
{
    public:
        Constructor(SkPsyVEncTicker, SkObject);

        void init(Size imageSize,
                  UInt framesPerSecond,
                  SkPsyVCodecType codec,
                  SkPsyYuvRatio yuvChansRatio,
                  UInt encodingBits,
                  UInt compression);

        void release();

        Slot(encode);
        Signal(encoded);

    private:
        SkPsyVCodecType cdc;
        Size imgSz;
        UInt fps;
        UInt keyFrameCount;
        UInt diffMemoryCount;
        UInt inputFramesCounter;

        Mat currYUV;

        Mat prevY;
        Mat prevU;
        Mat prevV;

        Mat prev8QY;
        Mat prev8QU;
        Mat prev8QV;

        vector<int> encParams;
        vector<uchar> outBuf;

        int bits;
        SkPsyYuvRatio yuvRatio;
        int yuvRatioValues[3];

        int totalBlocks;
        Size blockSz[3];

        SkPsy_BLOCK rootBlock;

        SkBufferDevice *pckWriter;
        SkBufferDevice *subPckWriter;
        ULong pckID;

        SkPsyVEncBlock *blocks;

        void buildBlock(SkPsy_BLOCK &block, int &blockID);
        void checkBlock(Mat &frame, Mat &prev, SkPsy_BLOCK &block, double threshold);

        void resizeUV(SkPsyVEncPacket *pck);
        void splitYUV(SkPsyVEncPacket *pck);
        void grabBlocks(SkPsyVEncPacket *pck, Mat &prev, Mat &prev8Q, int chIndex);
        void writeBlocks(SkPsyVEncPacket *pck, int chIndex, double threshold);
        void writeJPEG(Mat &i);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyVEncTh extends SkThread
{
    public:
        Constructor(SkPsyVEncTh, SkThread);

        void init(Size imageSize,
                  UInt framesPerSecond,
                  SkPsyVCodecType codec,
                  SkPsyYuvRatio yuvChansRatio,
                  UInt encodingBits,
                  UInt compression);

        SkPsyVEncTicker *getTicker();

        static void ratiosValuesUV(SkPsyYuvRatio yuvChansRatio, int *yuvRatioValues);

    private:
        SkPsyVCodecType cdc;
        Size imgSz;
        UInt fps;
        int bits;
        int compressionVal;
        SkPsyYuvRatio yuvRatio;

        SkPsyVEncTicker *ticker;

        bool customRunSetup()                                   override;
        void customClosing()                                    override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif*/

#endif // SKPSYVENCTH_H
