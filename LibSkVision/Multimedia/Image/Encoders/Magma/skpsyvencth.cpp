/*#if defined(ENABLE_CV)

#include "skpsyvencth.h"

#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

#define Y   0
#define U   1
#define V   2

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyVEncTicker, SkObject)
{
    fps = 0;
    cdc = NoneCodec;
    bits = 0;
    yuvRatio = Yuv444;

    keyFrameCount = 0;
    inputFramesCounter = 0;
    blocks = nullptr;

    totalBlocks = 0;

    setObjectName("MagmaVideoEncTicker");

    SlotSet(encode);
    SignalSet(encoded);

    encParams.push_back(IMWRITE_JPEG_QUALITY);
    encParams.push_back(0);

    pckWriter = new SkBufferDevice(this);
    subPckWriter = new SkBufferDevice(this);
}

void SkPsyVEncTicker::init(Size imageSize,
                           UInt framesPerSecond,
                           SkPsyVCodecType codec,
                           SkPsyYuvRatio yuvChansRatio,
                           UInt encodingBits,
                           UInt compression)
{
    imgSz = imageSize;
    fps = framesPerSecond;
    keyFrameCount = framesPerSecond*2;
    inputFramesCounter = 0;
    cdc = codec;
    bits = encodingBits;
    encParams[1] = compression;
    yuvRatio = yuvChansRatio;

    SkPsyVEncTh::ratiosValuesUV(yuvRatio, yuvRatioValues);

    blockSz[Y] = Size(40, 40);
    blockSz[U] = blockSz[Y] / yuvRatioValues[U];
    blockSz[V] = blockSz[Y] / yuvRatioValues[V];

    if (!blockSz[U].width || !blockSz[U].height)
        blockSz[U] = blockSz[Y];

    if (!blockSz[V].width || !blockSz[V].height)
        blockSz[V] = blockSz[Y];

    int currentY[3];
    currentY[Y] = 0;
    currentY[U] = 0;
    currentY[V] = 0;

    int currentX[3];
    currentX[Y] = 0;
    currentX[U] = 0;
    currentX[V] = 0;

    Rect region[3];

    int blockID = 0;

    region[Y] = Rect(0, 0, 0, 0);
    region[U] = Rect(0, 0, 0, 0);
    region[V] = Rect(0, 0, 0, 0);

    totalBlocks = ceil(((float) imgSz.width/blockSz[Y].width)*((float) imgSz.height/blockSz[Y].height));
    blocks = new SkPsyVEncBlock [totalBlocks];

    for(; blockID<totalBlocks; blockID++)
    {
        blocks[blockID].id = blockID;

        for(int ch=0 ; ch<3; ch++)
        {
            blocks[blockID].chans[ch].rmse = 0.f;

            region[ch].x = currentX[ch];
            region[ch].y = currentY[ch];

            region[ch].width = blockSz[ch].width;
            region[ch].height = blockSz[ch].height;

            blocks[blockID].chans[ch].region = region[ch];

            currentX[ch] += blockSz[ch].width;

            if (currentX[ch]>(imgSz/yuvRatioValues[ch]).width-1)
            {
                currentX[ch] = 0;
                currentY[ch] += blockSz[ch].height;
            }

            //cout << ch << " - " << blockID  << " " << blockSz[ch].width << " " << blocks[blockID].chans[ch].region << "\n";
        }
    }

    //rootBlock.id = 0;
    //rootBlock.level = 0;
    //rootBlock.parent = nullptr;
    //rootBlock.region = Rect(0, 0, imgSz.width, imgSz.height);
    //rootBlock.region = Rect(0, 0, 100, 100);
    //rootBlock.children = nullptr;
    //rootBlock.childrenCount = 1;

    //cout <<  rootBlock.id  << "\t" << rootBlock.childrenCount << "\t" << rootBlock.level << "\t" << rootBlock.region  << "\n";
    //buildBlock(rootBlock, rootBlock.childrenCount);
    //cout << "TOTAL: " << rootBlock.childrenCount << "\n";
    //exit(1);

    ObjectMessage("Total blocks [res: " << imgSz << ", blk: " << blockSz[Y] << "]: " << blockID << " [allocated: " << totalBlocks << "]");
}

void SkPsyVEncTicker::buildBlock(SkPsy_BLOCK &parent, int &blockID)
{
    Size size = parent.region.size();
    Size blkSize = Size(ceil(size.width/2.f), ceil(size.height/2.f));

    if (blkSize.width<=10 || blkSize.height<=10)
        return;

    int level = parent.level + 1;
    int childrenCount = 0;

    for(int y=0; y < size.height; y+=blkSize.height)
        for(int x=0; x < size.width; x+=blkSize.width)
        {
            parent.children = static_cast<SkPsy_BLOCK *>(realloc(parent.children, (childrenCount+1)*sizeof(SkPsy_BLOCK)));

            SkPsy_BLOCK &child = parent.children[childrenCount];
            child.id = blockID++;
            child.level = level;
            child.parent = &parent;
            child.region = Rect(x, y, blkSize.width, blkSize.height);

            int tempSum = child.region.x + child.region.width;

            if (tempSum > size.width)
                child.region.width = size.width - child.region.x;

            tempSum = child.region.y + child.region.height;

            if (tempSum > size.height)
                child.region.height = size.height - child.region.y;

            buildBlock(child, blockID);
            childrenCount++;

            cout <<  child.id  << "\t" << childrenCount << "\t" << child.level << "\t" << child.region  << "\n";
        }

    parent.childrenCount = childrenCount;
}

void SkPsyVEncTicker::checkBlock(Mat &frame, Mat &prev, SkPsy_BLOCK &block, double threshold)
{
    if (!block.childrenCount)
        return;

    Mat currBlock;
    Mat prevBlock;

    for(int i=0; i<block.childrenCount; i++)
    {
        SkPsy_BLOCK &child = block.children[i];
        Rect &region = child.region;

        currBlock = frame(region);
        currBlock.convertTo(currBlock, CV_32FC1, 1./255.);

        prevBlock = prev(region);
        prevBlock.convertTo(prevBlock, CV_32FC1, 1./255.);

        absdiff(currBlock, prevBlock, child.diff8Q);
        child.diff8Q.mul(child.diff8Q);

        float mse = cv::mean(child.diff8Q)[0];//Mean Square Error
        child.rmse = std::sqrt(mse);//Root Mean Square Error

        if (child.rmse > threshold && child.childrenCount)
            checkBlock(frame, prev, block, threshold);
    }
}

void SkPsyVEncTicker::release()
{
    if (blocks)
    {
        delete [] blocks;
        blocks = nullptr;
    }
}

SlotImpl(SkPsyVEncTicker, encode)
{
    SilentSlotArgsWarning();

    SkPsyVEncPacket *pck = dynamic_cast<SkPsyVEncPacket *>(referer);

    SkDataBuffer pckBuff;
    subPckWriter->open(pckBuff, SkBufferDeviceMode::BVM_ONLYWRITE);

    if (cdc == YuvCodec)
    {
        splitYUV(pck);
        resizeUV(pck);

        writeJPEG(pck->chans[Y].frame);
        writeJPEG(pck->chans[U].frame);
        writeJPEG(pck->chans[V].frame);
    }

    else if (cdc == YuvRatioCodec)
    {
        splitYUV(pck);
        resizeUV(pck);

        if (inputFramesCounter == 0)
        {
            pck->chans[Y].frame.copyTo(prevY);
            pck->chans[U].frame.copyTo(prevU);
            pck->chans[V].frame.copyTo(prevV);

            subPckWriter->writeUInt8(RefreshPck);

            writeJPEG(pck->chans[Y].frame);
            writeJPEG(pck->chans[U].frame);
            writeJPEG(pck->chans[V].frame);
        }

        else
        {
            if (!prevY.empty())
            {
                grabBlocks(pck, prevY, prev8QY, Y);
                grabBlocks(pck, prevU, prev8QU, U);
                grabBlocks(pck, prevV, prev8QV, V);

                pck->chans[Y].diff8Q.copyTo(prev8QY);
                pck->chans[U].diff8Q.copyTo(prev8QU);
                pck->chans[V].diff8Q.copyTo(prev8QV);

                subPckWriter->writeUInt8(BlocksPck);

                writeBlocks(pck, Y, 0.15);
                writeBlocks(pck, U, 0.15);
                writeBlocks(pck, V, 0.15);
            }

            pck->chans[Y].frame.copyTo(prevY);
            pck->chans[U].frame.copyTo(prevU);
            pck->chans[V].frame.copyTo(prevV);
        }

        if (inputFramesCounter == keyFrameCount)
        {
            inputFramesCounter = 0;

            prev8QY = Mat();
            prev8QU = Mat();
            prev8QV = Mat();
        }

        else
            inputFramesCounter++;
    }

    else if (cdc == PsychoRatioCodec)
    {
    }

    subPckWriter->close();

    pckWriter->open(pck->encoded, SkBufferDeviceMode::BVM_ONLYWRITE);

    if (!pckBuff.isEmpty())
    {
        pckWriter->writeUInt64(pckID++);
        pckWriter->write(&pckBuff);
    }

    pckWriter->close();

    //CRASH SAYING enc IS NULL, BUT IT IS NOT NULL :|
    //enc->eventLoop()->invokeSlot(enc->loadEncoded_SLOT, enc, pck);
    //USING SIGNAL AS SOLUTION, BUT I DON'T KNOW WHY OF THIS ERROR
    encoded(pck);
}

void SkPsyVEncTicker::splitYUV(SkPsyVEncPacket *pck)
{
    SkImageUtils::doColorConversion(pck->frame, currYUV, COLOR_BGR2YUV);

    vector<Mat> channels;
    split(currYUV, channels);

    //Yuv444
    channels[Y].copyTo(pck->chans[Y].frame);
    channels[U].copyTo(pck->chans[U].frame);
    channels[V].copyTo(pck->chans[V].frame);
}

void SkPsyVEncTicker::resizeUV(SkPsyVEncPacket *pck)
{
    if (yuvRatio != Yuv444)
    {
        Size resU = pck->frame.size()/yuvRatioValues[U];
        Size resV = pck->frame.size()/yuvRatioValues[V];

        SkImageUtils::doResize(pck->chans[U].frame, pck->chans[U].frame, resU);
        SkImageUtils::doResize(pck->chans[V].frame, pck->chans[V].frame, resV);
    }
}

void SkPsyVEncTicker::grabBlocks(SkPsyVEncPacket *pck, Mat &prev, Mat &prev8Q, int chIndex)
{
    Mat &diff8Q = pck->chans[chIndex].diff8Q = Mat(imgSz/yuvRatioValues[chIndex], CV_32FC1, Scalar(0));

    Mat currBlock;
    Mat prevBlock;
    Mat diff8QBlock;

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        Rect &region = blocks[blockID].chans[chIndex].region;

        currBlock = pck->chans[chIndex].frame(region);
        prevBlock = prev(region);

        currBlock.convertTo(currBlock, CV_32FC1, 1./255.);
        prevBlock.convertTo(prevBlock, CV_32FC1, 1./255.);

        diff8QBlock = diff8Q(region);
        absdiff(currBlock, prevBlock, diff8QBlock);
        //diff8QBlock.mul(diff8QBlock);

        float mse = cv::mean(diff8QBlock)[0];//Mean Square Error
        blocks[blockID].chans[chIndex].rmse = std::sqrt(mse);//Root Mean Square Error
    }

    //if (!prev8Q.empty())
        //addWeighted(diff8Q, 1.5, prev8Q, 0.95, 0.0, diff8Q);
}

void SkPsyVEncTicker::writeBlocks(SkPsyVEncPacket *pck, int chIndex, double threshold)
{
    int count = 0;

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        blocks[blockID].chans[chIndex].changed = (blocks[blockID].chans[chIndex].rmse > threshold);

        if (blocks[blockID].chans[chIndex].changed)
        {
            pck->chans[chIndex].blocks.enqueue(blocks[blockID].chans[chIndex].region);
            count++;
        }
    }

    subPckWriter->writeUInt16(count);

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        if (blocks[blockID].chans[chIndex].changed)
        {
            Rect &r = blocks[blockID].chans[chIndex].region;
            subPckWriter->writeUInt16(r.x);
            subPckWriter->writeUInt16(r.y);
            subPckWriter->writeUInt16(r.width);
            subPckWriter->writeUInt16(r.height);

            Mat block = pck->chans[chIndex].frame(r);
            writeJPEG(block);
        }
    }
}

void SkPsyVEncTicker::writeJPEG(Mat &i)
{
    imencode(".jpg", i, outBuf, encParams);
    subPckWriter->writeUInt32(outBuf.size());
    subPckWriter->write(SkArrayCast::toCStr(outBuf.data()), outBuf.size());
    outBuf.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyVEncTh, SkThread)
{
    fps = 0;
    cdc = NoneCodec;
    bits = 0;
    compressionVal = 0;
    yuvRatio = Yuv444;
    ticker = nullptr;
    setObjectName("MagmaVideoEncTh");
}

void SkPsyVEncTh::init(Size imageSize,
                       UInt framesPerSecond,
                       SkPsyVCodecType codec,
                       SkPsyYuvRatio yuvChansRatio,
                       UInt encodingBits,
                       UInt compression)
{
    imgSz = imageSize;
    fps = framesPerSecond;
    cdc = codec;
    yuvRatio = yuvChansRatio;
    bits = encodingBits;
    compressionVal = compression;

    if (fps)
    {
        float tickTimePeriod = (1.f/(fps))*1000000;
        setLoopIntervals(tickTimePeriod, eventLoop()->getSlowInterval(), SK_TIMEDLOOP_RT);
    }
}

bool SkPsyVEncTh::customRunSetup()
{
    ticker = new SkPsyVEncTicker;
    ticker->init(imgSz, fps, cdc, yuvRatio, bits, compressionVal);

    thEventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);

    return true;
}

void SkPsyVEncTh::customClosing()
{
    ticker->release();
    ticker = nullptr;
}

SkPsyVEncTicker *SkPsyVEncTh::getTicker()
{
    return ticker;
}

void SkPsyVEncTh::ratiosValuesUV(SkPsyYuvRatio yuvChansRatio, int *yuvRatioValues)
{
    if (yuvChansRatio == Yuv444)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 1;
        yuvRatioValues[V] = 1;
    }

    else if (yuvChansRatio == Yuv422)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 2;
        yuvRatioValues[V] = 2;
    }

    else if (yuvChansRatio == Yuv421)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 2;
        yuvRatioValues[V] = 4;
    }

    else if (yuvChansRatio == Yuv411)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 4;
        yuvRatioValues[V] = 4;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif*/
