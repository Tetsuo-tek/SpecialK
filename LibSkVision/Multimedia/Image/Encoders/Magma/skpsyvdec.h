#ifndef SKPSYVDEC_H
#define SKPSYVDEC_H

#if defined(ENABLE_CV)

#include "skpsyvdecth.h"

class SkPsyVDec extends SkAbstractMagmaDecoder
{
    public:
        Constructor(SkPsyVDec, SkAbstractMagmaDecoder);

        bool open(SkDataBuffer &header)                 override;
        void close()                                    override;

        void setData(SkDataBuffer &input)               override;
        bool decode()                                   override;

        Size resolution();

        Mat &frame_BGR();
        SkQueue<Rect> &blockRegions();

        float cpuLoadAvg();
        float jobTimeAvg();
        ULong queuedPcks();

        Slot(decoderStarted);
        Slot(loadDecoded);

    private:
        SkPsyVCodecType cdc;
        int bits;

        SkPsyVDecTh *decoder;

        UInt fps;
        Size imgSz;

        Mat img;

        SkQueue<Rect> blocks;
        SkQueue<SkPsyVDecBlock *> blocksData;

        SkQueue<SkPsyVDecPacket *> oPcks;
};

#endif

#endif // SKPSYVDEC_H
