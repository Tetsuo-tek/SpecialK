#if defined(ENABLE_CV)

#include "skpsyvenc.h"

#include "Core/App/skeventloop.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

#define Y   0
#define U   1
#define V   2

ConstructorImpl(SkPsyVEnc, SkAbstractMagmaEncoder)
{
    t = MGM_VIDEO;

    fps = 0;
    cdc = NoneCodec;
    bits = 0;
    yuvRatio = Yuv444;

    keyFrameCount = 0;
    inputFramesCounter = 0;
    blocks = nullptr;

    totalBlocks = 0;

    encParams.push_back(IMWRITE_JPEG_QUALITY);
    encParams.push_back(0);

    pckWriter = new SkBufferDevice(this);
    subPckWriter = new SkBufferDevice(this);
}

bool SkPsyVEnc::setup(Size imageSize,
                      UInt framesPerSecond,
                      SkPsyVCodecType codec,
                      SkPsyYuvRatio yuvChansRatio,
                      UInt encodingBits,
                      float quality)
{
    if (enabled)
    {
        ObjectError("Encoder is ALREADY enabled");
        return false;
    }

    if (framesPerSecond < 2)
    {
        ObjectError("Encoder parameter 'framesPerSecond' MUST be >= 2");
        return false;
    }

    imgSz = imageSize;
    fps = framesPerSecond;
    tickTimePeriod = 1.f/fps;
    keyFrameCount = framesPerSecond*2;
    inputFramesCounter = 0;
    cdc = codec;
    bits = encodingBits;
    encParams[1] = quality*100;
    yuvRatio = yuvChansRatio;

    SkPsyVEnc::ratiosValuesUV(yuvRatio, yuvRatioValues);

    blockSz[Y] = Size(40, 40);
    blockSz[U] = blockSz[Y] / yuvRatioValues[U];
    blockSz[V] = blockSz[Y] / yuvRatioValues[V];

    if (!blockSz[U].width || !blockSz[U].height)
        blockSz[U] = blockSz[Y];

    if (!blockSz[V].width || !blockSz[V].height)
        blockSz[V] = blockSz[Y];

    int currentY[3];
    currentY[Y] = 0;
    currentY[U] = 0;
    currentY[V] = 0;

    int currentX[3];
    currentX[Y] = 0;
    currentX[U] = 0;
    currentX[V] = 0;

    Rect region[3];

    int blockID = 0;

    region[Y] = Rect(0, 0, 0, 0);
    region[U] = Rect(0, 0, 0, 0);
    region[V] = Rect(0, 0, 0, 0);

    totalBlocks = ceil(((float) imgSz.width/blockSz[Y].width)*((float) imgSz.height/blockSz[Y].height));
    blocks = new SkPsyVEncBlock [totalBlocks];

    for(; blockID<totalBlocks; blockID++)
    {
        blocks[blockID].id = blockID;

        for(int ch=0 ; ch<3; ch++)
        {
            blocks[blockID].chans[ch].rmse = 0.f;

            region[ch].x = currentX[ch];
            region[ch].y = currentY[ch];

            region[ch].width = blockSz[ch].width;
            region[ch].height = blockSz[ch].height;

            blocks[blockID].chans[ch].region = region[ch];

            currentX[ch] += blockSz[ch].width;

            if (currentX[ch]>(imgSz/yuvRatioValues[ch]).width-1)
            {
                currentX[ch] = 0;
                currentY[ch] += blockSz[ch].height;
            }

            //cout << ch << " - " << blockID  << " " << blockSz[ch].width << " " << blocks[blockID].chans[ch].region << "\n";
        }
    }

    ObjectMessage("Total blocks [blk: " << blockSz[Y] << "]: " << blockID << " [allocated: " << totalBlocks << "]");

    ObjectMessage("PSY-V video parameters -> ["
                  << "resolution: " << imgSz << "; "
                  << "fps: " << fps << "; "
                  << "period: " << tickTimePeriod << " s]");
    return true;
}

void SkPsyVEnc::ratiosValuesUV(SkPsyYuvRatio yuvChansRatio, int *yuvRatioValues)
{
    if (yuvChansRatio == Yuv444)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 1;
        yuvRatioValues[V] = 1;
    }

    else if (yuvChansRatio == Yuv422)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 2;
        yuvRatioValues[V] = 2;
    }

    else if (yuvChansRatio == Yuv421)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 2;
        yuvRatioValues[V] = 4;
    }

    else if (yuvChansRatio == Yuv411)
    {
        yuvRatioValues[Y] = 1;
        yuvRatioValues[U] = 4;
        yuvRatioValues[V] = 4;
    }
}

bool SkPsyVEnc::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    writer->open(header, SkBufferDeviceMode::BVM_ONLYWRITE);

    writer->writeInt8('P');
    writer->writeInt8('S');
    writer->writeInt8('Y');
    writer->writeInt8('V');

    writer->writeUInt8(cdc);
    writer->writeUInt8(bits);
    writer->writeUInt16(imgSz.width);
    writer->writeUInt16(imgSz.height);
    writer->writeUInt8(fps);
    writer->writeFloat(tickTimePeriod);

    writer->close();

    //encoder->start();
    enabled = true;
    return true;
}

void SkPsyVEnc::close()
{
    if (!enabled)
        return;

    /*encoder->quit();
    encoder->wait();*/

    if (blocks)
    {
        delete [] blocks;
        blocks = nullptr;
    }

    enabled = false;
}

void SkPsyVEnc::setData(void *bgrImageData)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return;
    }

    currentPck.frame = Mat(imgSz, CV_8UC3, bgrImageData);
}

bool SkPsyVEnc::encode(SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    SkDataBuffer pckBuff;
    subPckWriter->open(pckBuff, SkBufferDeviceMode::BVM_ONLYWRITE);

    if (cdc == YuvCodec)
    {
        splitYUV();
        resizeUV();

        writeJPEG(currentPck.chans[Y].frame);
        writeJPEG(currentPck.chans[U].frame);
        writeJPEG(currentPck.chans[V].frame);
    }

    else if (cdc == YuvRatioCodec)
    {
        splitYUV();
        resizeUV();

        if (inputFramesCounter == 0)
        {
            currentPck.chans[Y].frame.copyTo(prevY);
            currentPck.chans[U].frame.copyTo(prevU);
            currentPck.chans[V].frame.copyTo(prevV);

            subPckWriter->writeUInt8(RefreshPck);

            writeJPEG(currentPck.chans[Y].frame);
            writeJPEG(currentPck.chans[U].frame);
            writeJPEG(currentPck.chans[V].frame);
        }

        else
        {
            if (!prevY.empty())
            {
                grabBlocks(prevY, prev8QY, Y);
                grabBlocks(prevU, prev8QU, U);
                grabBlocks(prevV, prev8QV, V);

                currentPck.chans[Y].diff8Q.copyTo(prev8QY);
                currentPck.chans[U].diff8Q.copyTo(prev8QU);
                currentPck.chans[V].diff8Q.copyTo(prev8QV);

                subPckWriter->writeUInt8(BlocksPck);

                writeBlocks(Y, 0.15);
                writeBlocks(U, 0.15);
                writeBlocks(V, 0.15);
            }

            currentPck.chans[Y].frame.copyTo(prevY);
            currentPck.chans[U].frame.copyTo(prevU);
            currentPck.chans[V].frame.copyTo(prevV);
        }

        if (inputFramesCounter == keyFrameCount)
        {
            inputFramesCounter = 0;

            prev8QY = Mat();
            prev8QU = Mat();
            prev8QV = Mat();
        }

        else
            inputFramesCounter++;
    }

    else if (cdc == PsychoRatioCodec)
    {
    }

    subPckWriter->close();

    pckWriter->open(currentPck.encoded, SkBufferDeviceMode::BVM_ONLYWRITE);

    if (!pckBuff.isEmpty())
    {
        pckWriter->writeUInt64(pckID++);
        pckWriter->write(&pckBuff);
    }

    pckWriter->close();

    output.append(currentPck.encoded);
    return true;
}

void SkPsyVEnc::buildBlock(SkPsy_BLOCK &parent, int &blockID)
{
    Size size = parent.region.size();
    Size blkSize = Size(ceil(size.width/2.f), ceil(size.height/2.f));

    if (blkSize.width<=10 || blkSize.height<=10)
        return;

    int level = parent.level + 1;
    int childrenCount = 0;

    for(int y=0; y < size.height; y+=blkSize.height)
        for(int x=0; x < size.width; x+=blkSize.width)
        {
            parent.children = static_cast<SkPsy_BLOCK *>(realloc(parent.children, (childrenCount+1)*sizeof(SkPsy_BLOCK)));

            SkPsy_BLOCK &child = parent.children[childrenCount];
            child.id = blockID++;
            child.level = level;
            child.parent = &parent;
            child.region = Rect(x, y, blkSize.width, blkSize.height);

            int tempSum = child.region.x + child.region.width;

            if (tempSum > size.width)
                child.region.width = size.width - child.region.x;

            tempSum = child.region.y + child.region.height;

            if (tempSum > size.height)
                child.region.height = size.height - child.region.y;

            buildBlock(child, blockID);
            childrenCount++;

            cout <<  child.id  << "\t" << childrenCount << "\t" << child.level << "\t" << child.region  << "\n";
        }

    parent.childrenCount = childrenCount;
}

void SkPsyVEnc::checkBlock(Mat &frame, Mat &prev, SkPsy_BLOCK &block, double threshold)
{
    if (!block.childrenCount)
        return;

    Mat currBlock;
    Mat prevBlock;

    for(int i=0; i<block.childrenCount; i++)
    {
        SkPsy_BLOCK &child = block.children[i];
        Rect &region = child.region;

        currBlock = frame(region);
        currBlock.convertTo(currBlock, CV_32FC1, 1./255.);

        prevBlock = prev(region);
        prevBlock.convertTo(prevBlock, CV_32FC1, 1./255.);

        absdiff(currBlock, prevBlock, child.diff8Q);
        child.diff8Q.mul(child.diff8Q);

        float mse = cv::mean(child.diff8Q)[0];//Mean Square Error
        child.rmse = std::sqrt(mse);//Root Mean Square Error

        if (child.rmse > threshold && child.childrenCount)
            checkBlock(frame, prev, block, threshold);
    }
}

void SkPsyVEnc::resizeUV()
{
    if (yuvRatio != Yuv444)
    {
        Size resU = currentPck.frame.size()/yuvRatioValues[U];
        Size resV = currentPck.frame.size()/yuvRatioValues[V];

        SkImageUtils::doResize(currentPck.chans[U].frame, currentPck.chans[U].frame, resU);
        SkImageUtils::doResize(currentPck.chans[V].frame, currentPck.chans[V].frame, resV);
    }
}

void SkPsyVEnc::splitYUV()
{
    SkImageUtils::doColorConversion(currentPck.frame, currYUV, COLOR_BGR2YUV);

    vector<Mat> channels;
    split(currYUV, channels);

    //Yuv444
    channels[Y].copyTo(currentPck.chans[Y].frame);
    channels[U].copyTo(currentPck.chans[U].frame);
    channels[V].copyTo(currentPck.chans[V].frame);
}

void SkPsyVEnc::grabBlocks(Mat &prev, Mat &prev8Q, int chIndex)
{
    Mat &diff8Q = currentPck.chans[chIndex].diff8Q = Mat(imgSz/yuvRatioValues[chIndex], CV_32FC1, Scalar(0));

    Mat currBlock;
    Mat prevBlock;
    Mat diff8QBlock;

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        Rect &region = blocks[blockID].chans[chIndex].region;

        currBlock = currentPck.chans[chIndex].frame(region);
        prevBlock = prev(region);

        currBlock.convertTo(currBlock, CV_32FC1, 1./255.);
        prevBlock.convertTo(prevBlock, CV_32FC1, 1./255.);

        diff8QBlock = diff8Q(region);
        absdiff(currBlock, prevBlock, diff8QBlock);
        //diff8QBlock.mul(diff8QBlock);

        float mse = cv::mean(diff8QBlock)[0];//Mean Square Error
        blocks[blockID].chans[chIndex].rmse = std::sqrt(mse);//Root Mean Square Error*/
    }

    /*if (!prev8Q.empty())
        addWeighted(diff8Q, 1.5, prev8Q, 0.95, 0.0, diff8Q);*/
}

void SkPsyVEnc::writeBlocks(int chIndex, double threshold)
{
    int count = 0;

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        blocks[blockID].chans[chIndex].changed = (blocks[blockID].chans[chIndex].rmse > threshold);

        if (blocks[blockID].chans[chIndex].changed)
        {
            currentPck.chans[chIndex].blocks.enqueue(blocks[blockID].chans[chIndex].region);
            count++;
        }
    }

    subPckWriter->writeUInt16(count);

    for(int blockID=0; blockID<totalBlocks; blockID++)
    {
        if (blocks[blockID].chans[chIndex].changed)
        {
            Rect &r = blocks[blockID].chans[chIndex].region;
            subPckWriter->writeUInt16(r.x);
            subPckWriter->writeUInt16(r.y);
            subPckWriter->writeUInt16(r.width);
            subPckWriter->writeUInt16(r.height);

            Mat block = currentPck.chans[chIndex].frame(r);
            writeJPEG(block);
        }
    }
}

void SkPsyVEnc::writeJPEG(Mat &i)
{
    imencode(".jpg", i, outBuf, encParams);
    subPckWriter->writeUInt32(outBuf.size());
    subPckWriter->write(SkArrayCast::toCStr(outBuf.data()), outBuf.size());
    outBuf.clear();
}

int SkPsyVEnc::ratioValueU()
{
    return yuvRatioValues[U];
}

int SkPsyVEnc::ratioValueV()
{
    return yuvRatioValues[V];
}

/*ConstructorImpl(SkPsyVEnc, SkAbstractMagmaEncoder)
{
    t = MGM_VIDEO;

    fps = 0;
    cdc = YuvCodec;
    bits = 0;
    compression = 0;
    yuvRatio = Yuv444;
    currBlkSz = Size(40, 40);
    seaThreshold = 0.03f;

    yuvRatioValues[0] = 0;
    yuvRatioValues[1] = 0;

    SlotSet(encoderStarted);
    SlotSet(loadEncoded);

    encoder = new SkPsyVEncTh(this);
    Attach(encoder, started, this, encoderStarted, SkQueued);
}

bool SkPsyVEnc::setup(Size imageSize,
                      UInt framesPerSecond,
                      SkPsyVCodecType codec,
                      SkPsyYuvRatio yuvChansRatio,
                      UInt encodingBits,
                      float quality)
{
    if (enabled)
    {
        ObjectError("Encoder is ALREADY enabled");
        return false;
    }

    if (framesPerSecond < 2)
    {
        ObjectError("Encoder parameter 'framesPerSecond' MUST be >= 2");
        return false;
    }

    imgSz = imageSize;
    fps = framesPerSecond;
    cdc = codec;
    yuvRatio = yuvChansRatio;
    bits = encodingBits;
    compression = quality*100;

    tickTimePeriod = 1.f/fps;

    SkPsyVEncTh::ratiosValuesUV(yuvRatio, yuvRatioValues);

    encoder->setObjectName(this, "Th");
    encoder->init(imgSz, fps, cdc, yuvRatio, bits, compression);

    ObjectMessage("PSY-V video parameters -> ["
                << "resolution: " << imgSz << "; "
                << "fps: " << fps << "; "
                << "period: " << tickTimePeriod << " s]");

    return true;
}

bool SkPsyVEnc::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    writer->open(header, SkBufferDeviceMode::BVM_ONLYWRITE);

    writer->writeInt8('P');
    writer->writeInt8('S');
    writer->writeInt8('Y');
    writer->writeInt8('V');

    writer->writeUInt8(cdc);
    writer->writeUInt8(bits);
    writer->writeUInt16(imgSz.width);
    writer->writeUInt16(imgSz.height);
    writer->writeUInt8(fps);
    writer->writeFloat(tickTimePeriod);

    writer->close();

    encoder->start();
    enabled = true;
    return true;
}

SlotImpl(SkPsyVEnc, encoderStarted)
{
    SilentSlotArgsWarning();
    ObjectMessage("Encoder thread STARTED");
    Attach(encoder->getTicker(), encoded, this, loadEncoded, SkQueued);
}

void SkPsyVEnc::close()
{
    if (!enabled)
        return;

    encoder->quit();
    encoder->wait();

    enabled = false;
}

void SkPsyVEnc::setData(void *bgrImageData)
{
    if (!encoder->isRunning())
        return;

    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return;
    }

    seaThreshold = 0.025f;
    currBlkSz = Size(40, 40);

    SkPsyVEncPacket *pck = new SkPsyVEncPacket;

    //pck->chans[Y].blockSz = currBlkSz;
    //pck->chans[U].blockSz = currBlkSz / yuvRatioValues[0];
    //pck->chans[V].blockSz = currBlkSz / yuvRatioValues[1];

    //cout << pck->chans[Y].blockSz << " "<< pck->chans[U].blockSz << " "<< pck->chans[V].blockSz << "\n";

    Mat input(imgSz, CV_8UC3, bgrImageData);
    input.copyTo(pck->frame);

    //pck->blockSz = currBlkSz;
    //pck->seaThreshold = seaThreshold;
    //pck->seaMax = 0.f;
    //pck->jpgCompression = compression;

    encoder->thEventLoop()->invokeSlot(encoder->getTicker()->encode_SLOT, encoder->getTicker(), pck);
}

bool SkPsyVEnc::encode(SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    if (oPcks.isEmpty())
        return false;

    SkPsyVEncPacket *pck = oPcks.dequeue();

    if (!pck->encoded.isEmpty())
        output.append(pck->encoded);

    delete pck;
    return true;
}

SlotImpl(SkPsyVEnc, loadEncoded)
{
    SilentSlotArgsWarning();

    SkPsyVEncPacket *pck = dynamic_cast<SkPsyVEncPacket *>(referer);
    pck->frame.copyTo(img);

    //blocks = pck->blocks;
    //seaMax = pck->seaMax;

    if (!pck->chans[Y].frame.empty())
    {
        y = pck->chans[Y];
        u = pck->chans[U];
        v = pck->chans[V];
    }

    oPcks.enqueue(pck);
}

Mat &SkPsyVEnc::frame_BGR()
{
    return img;
}

Mat &SkPsyVEnc::frame_Y()
{
    return y.frame;
}

Mat &SkPsyVEnc::frame_U()
{
    return u.frame;
}

Mat &SkPsyVEnc::frame_V()
{
    return v.frame;
}

Mat &SkPsyVEnc::frame_8QY()
{
    return y.diff8Q;
}

SkQueue<Rect> &SkPsyVEnc::blockRegionsY()
{
    return y.blocks;
}

SkQueue<Rect> &SkPsyVEnc::blockRegionsU()
{
    return u.blocks;
}

SkQueue<Rect> &SkPsyVEnc::blockRegionsV()
{
    return v.blocks;
}

int SkPsyVEnc::ratioValueU()
{
    return yuvRatioValues[U];
}

int SkPsyVEnc::ratioValueV()
{
    return yuvRatioValues[V];
}

float SkPsyVEnc::aseMaximum()
{
    return seaMax;
}

float SkPsyVEnc::cpuLoadAvg()
{
    if (!encoder->isRunning())
        return 0.f;

    return jobTimeAvg() / encoder->thEventLoop()->getLastPulseElapsedTimeAverage();
}

float SkPsyVEnc::jobTimeAvg()
{
    if (!encoder->isRunning())
        return 0.f;

    return encoder->thEventLoop()->getConsumedTimeAverage();
}

ULong SkPsyVEnc::queuedPcks()
{
    return oPcks.count();
}*/

#endif
