﻿#if defined(ENABLE_CV)

#include "skpsyvdec.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

ConstructorImpl(SkPsyVDec, SkAbstractMagmaDecoder)
{
    t = MGM_VIDEO;

    pckID = 0;
    fps = 0;
    bits = 0;

    //lastPckType = NonePck;

    SlotSet(decoderStarted);
    SlotSet(loadDecoded);

    decoder = new SkPsyVDecTh(this);
    Attach(decoder, started, this, decoderStarted, SkQueued);
    eventLoop()->performAttachs();
}

bool SkPsyVDec::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    pckID = 0;

    reader->open(header, SkBufferDeviceMode::BVM_ONLYREAD);

    char cc[4];
    cc[0] = reader->readInt8();
    cc[1] = reader->readInt8();
    cc[2] = reader->readInt8();
    cc[3] = reader->readInt8();

    if (cc[0] != 'P' || cc[1] != 'S' || cc[2] != 'Y' || cc[3] != 'V')
    {
        reader->close();
        return false;
    }

    cdc = static_cast<SkPsyVCodecType>(reader->readUInt8());
    bits = reader->readUInt8();

    int w = reader->readUInt16();
    int h = reader->readUInt16();
    imgSz = Size(w, h);

    fps = reader->readUInt8();
    tickTimePeriod = reader->readFloat();

    img = Mat(imgSz, CV_8UC3, Scalar(0, 0, 0));

    decoder->init(cdc, bits, imgSz, fps, tickTimePeriod);

    FlatMessage("PSY-V video parameters -> ["
                << "resolution: " << imgSz << "; "
                << "fps: " << fps << "; "
                << "period: " << tickTimePeriod << " s]");

    reader->close();

    decoder->start();

    enabled = true;
    return true;
}

SlotImpl(SkPsyVDec, decoderStarted)
{
    SilentSlotArgsWarning();
    FlatMessage("Decoder thread STARTED");
    Attach(decoder->getTicker(), decoded, this, loadDecoded, SkQueued);
}

void SkPsyVDec::close()
{
    decoder->quit();
    decoder->wait();

    enabled = false;
}

void SkPsyVDec::setData(SkDataBuffer &input)
{
    if (!decoder->isRunning())
        return;

    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return;
    }

    SkPsyVDecPacket *pck = new SkPsyVDecPacket;
    pck->encoded.setData(input);

    decoder->thEventLoop()->invokeSlot(decoder->getTicker()->decode_SLOT, decoder->getTicker(), pck);
}

bool SkPsyVDec::decode()
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    if (oPcks.isEmpty())
        return false;

    SkPsyVDecPacket *pck = oPcks.dequeue();

    if (!pck->frame.empty())
        pck->frame.copyTo(img);

    delete pck;
    return true;
}

SlotImpl(SkPsyVDec, loadDecoded)
{
    SilentSlotArgsWarning();

    SkPsyVDecPacket *pck = dynamic_cast<SkPsyVDecPacket *>(referer);
    oPcks.enqueue(pck);

    /*if (oPcks.count() > 1)
    {
        while(oPcks.count() > 1)
            delete oPcks.dequeue();
    }*/
}

Size SkPsyVDec::resolution()
{
    return img.size();
}

Mat &SkPsyVDec::frame_BGR()
{
    return img;
}

SkQueue<Rect> &SkPsyVDec::blockRegions()
{
    return blocks;
}

float SkPsyVDec::cpuLoadAvg()
{
    if (!decoder->isRunning())
        return 0.f;

    return jobTimeAvg() / decoder->thEventLoop()->getLastPulseElapsedTimeAverage();
}

float SkPsyVDec::jobTimeAvg()
{
    if (!decoder->isRunning())
        return 0.f;

    return decoder->thEventLoop()->getConsumedTimeAverage();
}

ULong SkPsyVDec::queuedPcks()
{
    return oPcks.count();
}

#endif
