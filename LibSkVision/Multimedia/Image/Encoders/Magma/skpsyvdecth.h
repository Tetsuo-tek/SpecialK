#ifndef SKPSYVDECTH_H
#define SKPSYVDECTH_H

#if defined(ENABLE_CV)

#include "skpsyvenc.h"

#include "Core/System/Thread/skthread.h"
#include "Multimedia/Magma/skabstractmagmadecoder.h"
#include "Multimedia/Image/skimageutils.h"
#include "Multimedia/Image/skimagedecoder.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkPsyVDecBlock
{
    Point origin;
    Size blockSz;
    Mat block;
    SkDataBuffer jpeg;
};

struct SkPsyVDecPacket extends SkFlatObject
{
    ULong pckID;
    SkDataBuffer encoded;
    Mat frame;
    Mat y;
    Mat u;
    Mat v;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyVDecTicker extends SkObject
{
    public:
        Constructor(SkPsyVDecTicker, SkObject);

        void init(SkPsyVCodecType codec, UInt encodingBits, Size resolution, UInt framesPerSecond);

        Slot(decode);
        Signal(decoded);

    private:
        SkPsyVCodecType cdc;
        int bits;
        Size res;
        UInt fps;

        Mat prevY;
        Mat prevU;
        Mat prevV;

        SkDataBuffer inBuf;
        SkPsyVEncPacketType lastPckType;

        SkBufferDevice *pckReader;

        void mergeYUV(SkPsyVDecPacket *pck);
        void readBlocks(Mat &channel);
        void readJPEG(Mat &i);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyVDecTh extends SkThread
{
    public:
        Constructor(SkPsyVDecTh, SkThread);

        void init(SkPsyVCodecType codec, UInt encodingBits, Size resolution, UInt framesPerSecond, float period);

        SkPsyVDecTicker *getTicker();

    private:
        SkPsyVDecTicker *ticker;
        SkPsyVCodecType cdc;
        int bits;
        Size res;
        UInt fps;

        bool customRunSetup()                                   override;
        void customClosing()                                    override;
    };

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKPSYVDECTH_H
