#ifndef SKIMAGEGRADIENT_H
#define SKIMAGEGRADIENT_H

#if defined(ENABLE_CV)

#include "skimageutils.h"

class SkImageGradient extends SkFlatObject
{
    public:
        SkImageGradient();

        bool setup(Mat *source, Mat *target, double alpha, double delta, int gradient);
        void tick();

    private:
        int grd;
        Mat *src;
        Mat *tgt;
        Mat normalized;
        Mat normalized8u;
        double normAlpha;
        double normDelta;
};

#endif

#endif // SKIMAGEGRADIENT_H
