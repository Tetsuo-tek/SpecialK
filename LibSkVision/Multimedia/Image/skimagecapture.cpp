#if defined(ENABLE_CV)

#include "skimagecapture.h"
#include <Core/App/skeventloop.h>
#include "opencv2/opencv.hpp"

ConstructorImpl(SkImageCapture, SkObject)
{
    dev = -1;

    StaticMessage("Using OpenCV version: " << CV_VERSION);

    /*props.enabled = false;
    props.resolution = Size(-1, -1);
    props.forceResolution = false;
    props.fps = 0;
    props.forceFps = false;
    props.realTimeMode = false;
    props.flipMode = SkImageFlippingMode::SK_NONE_FLIPPING;
    props.apiReference = VideoCaptureAPIs::CAP_V4L2;*/

    if (vc.getExceptionMode())
        vc.setExceptionMode(false);

    SlotSet(tick);
    SignalSet(captured);
}

void SkImageCapture::setup(int device, SkVideoCaptureProperties &cfg)
{
    if (vc.isOpened())
    {
        ObjectError("Cannot setup: capture is ALREADY open");
        return;
    }

    dev = device;
    source.clear();
    props = cfg;

    ObjectMessage("Setup for device-capture: " << dev);
}

void SkImageCapture::setup(CStr *path, SkVideoCaptureProperties &cfg)
{
    if (vc.isOpened())
    {
        ObjectError("Cannot setup: capture is ALREADY open");
        return;
    }

    source = path;
    dev = -1;
    props = cfg;

    ObjectMessage("Setup for source-grab: " << source);
}

bool SkImageCapture::setCustomCvProperty(int cvPropertyName, double value)
{
    return vc.set(cvPropertyName, value);
}

double SkImageCapture::getCustomCvProperty(int cvPropertyName)
{
    return vc.get(cvPropertyName);
}

void SkImageCapture::setEnabled(bool enable)
{
    props.enabled = enable;
}

bool SkImageCapture::isEnabled()
{
    return props.enabled;
}

bool SkImageCapture::open()
{
    if (vc.isOpened())
    {
        ObjectError("Capture is ALREADY open");
        return false;
    }

    if (!props.fourCC.isEmpty() && props.fourCC.size() != 4)
    {
        ObjectError("FourCC string MUST be long 4 characters: " << props.fourCC);
        return false;
    }

    if (dev > -1)
    {
        if (!vc.open(dev, props.apiReference))
        {
            ObjectError("Cannot open video device: " << dev);
            return false;
        }
    }

    else if (!source.isEmpty())
    {
        if (!vc.open(source, props.apiReference))
        {
            ObjectError("Cannot open video source: " << source);
            return false;
        }
    }

    else
    {
        ObjectError("Video source not defined");
        return false;
    }

    if (props.apiReference == CAP_V4L2)
    {
        if (!props.fourCC.isEmpty())
        {
            CStr *c = props.fourCC.c_str();
            vc.set(CAP_PROP_FOURCC, VideoWriter::fourcc(c[0], c[1], c[2], c[3]));
        }

        double defaultFourCC = vc.get(CAP_PROP_FOURCC);
        char fourCCCode[5];
        *((int*)fourCCCode) = (int)defaultFourCC;
        fourCCCode[4] = '\0';

        ObjectMessage("Using FourCC: : " << fourCCCode);
    }

    if (props.apiReference == CAP_V4L2 && props.fps > 0)
        vc.set(CAP_PROP_FPS, props.resolution.height);

    int fps = static_cast<int>(vc.get(CAP_PROP_FPS));

    if (fps > 0)
    {
        if (props.fps != fps)
            ObjectWarning("FPS value from device DIFFERS from selected: " << fps << " != " << props.fps);

        if (props.forceFps)
        {
            if (props.fps != fps)
                ObjectWarning("Ignoring FPS value AVAILABLE from device; forcing to: " << props.fps);
        }

        else
        {
            if (props.fps == fps)
                ObjectMessage("Selected FPS value is SUPPORTED from device: " << props.fps);

            else
            {
                ObjectWarning("Selected FPS value is NOT SUPPORTED from device: " << props.fps);
                props.fps = fps;
                ObjectWarning("Switched to FPS value AVAILABLE from device: " << props.fps);
            }
        }
    }

    if (props.apiReference == CAP_V4L2)
    {
        if (props.resolution.width > 0 && props.resolution.height > 0)
        {
            vc.set(CAP_PROP_FRAME_WIDTH, props.resolution.width);
            vc.set(CAP_PROP_FRAME_HEIGHT, props.resolution.height);
        }
    }

    int w = static_cast<int>(vc.get(CAP_PROP_FRAME_WIDTH));
    int h = static_cast<int>(vc.get(CAP_PROP_FRAME_HEIGHT));

    if (w > 0 && h > 0)
    {
        Size sz(w, h);

        if (props.resolution != sz)
            ObjectWarning("RESOLUTION value from device DIFFERS from selected: " << sz << " != " <<  props.resolution);

        if (props.forceResolution)
        {
            if (props.resolution != sz)
                ObjectWarning("Ignoring RESOLUTION value AVAILABLE from device; forcing to: " << props.resolution);
        }

        else
        {
            if (props.resolution == sz)
                ObjectMessage("Selected RESOLUTION value is SUPPORTED from device: " << props.resolution);

            else
            {
                ObjectWarning("Selected RESOLUTION value is NOT SUPPORTED from device: " << props.resolution);
                props.resolution = sz;
                ObjectWarning("Switched to RESOLUTION value AVAILABLE from device: " << props.resolution);
            }
        }
    }

    if (props.fps)
    {
        changeFps();
        Attach(eventLoop()->fastZone_SIG, pulse, this, tick, SkDirect);
    }

    return true;
}

bool SkImageCapture::isOpen()
{
    return vc.isOpened();
}

void SkImageCapture::changeFps()
{
    if (!vc.isOpened())
    {
        ObjectError("Capture is NOT open yet");
        return;
    }

    ulong interval_us = static_cast<ulong>((1.f / props.fps) * 1000000);

    if (eventLoop()->getFastInterval() != interval_us)
        eventLoop()->changeFastZone(interval_us);

    if (props.realTimeMode && eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
}

SlotImpl(SkImageCapture, tick)
{
    SilentSlotArgsWarning();

    capture();
}

void SkImageCapture::close()
{
    if (vc.isOpened())
    {
        vc.release();

        if (props.fps)
            Detach(eventLoop()->fastZone_SIG, pulse, this, tick);
    }

    else
        ObjectError("Capture is NOT open yet");
}

Size SkImageCapture::getResolution()
{
    return props.resolution;
}

int SkImageCapture::getWidth()
{
    return props.resolution.width;
}

int SkImageCapture::getHeight()
{
    return props.resolution.height;
}

int SkImageCapture::getFps()
{
    return props.fps;
}

Mat &SkImageCapture::getFrame()
{
    return frame;
}

bool SkImageCapture::capture()
{
    if (!vc.isOpened())
        return false;

    if (!props.enabled)
        return true;

    if (!vc.read(frame))
    {
        ObjectError("Cannot read captured frame");
        return false;
    }

    if (frame.size() != props.resolution)
        SkImageUtils::doResize(frame, frame, props.resolution);

    if (props.flipMode != SkImageFlippingMode::SK_NONE_FLIPPING)
        SkImageUtils::doFlip(frame, frame, props.flipMode);

    onTick();
    captured();

    return true;
}

void SkImageCapture::changeFlipMode(SkImageFlippingMode m)
{
    props.flipMode = m;
}

ulong SkImageCapture::getTickTimeMicros()
{
    if (props.fps)
        return static_cast<ulong>((1.f / props.fps) * 1000000);

    return 0;
}

SkString SkImageCapture::getBackendName()
{
    return vc.getBackendName();
}

#endif
