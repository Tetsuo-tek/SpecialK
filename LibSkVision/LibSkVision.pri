CONFIG += c++11
CONFIG -= qt

DEFINES += ENABLE_CV
#DEFINES += ENABLE_CUDA

LIBS += -lopencv_core
LIBS += -lopencv_videoio
LIBS += -lopencv_highgui
LIBS += -lopencv_imgproc
LIBS += -lopencv_imgcodecs
LIBS += -lopencv_xphoto
LIBS += -lopencv_face
LIBS += -lopencv_objdetect
LIBS += -lopencv_calib3d
LIBS += -lopencv_features2d
LIBS += -lzbar

