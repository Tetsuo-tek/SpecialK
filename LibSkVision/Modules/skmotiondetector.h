#ifndef SKMOTIONDETECTOR_H
#define SKMOTIONDETECTOR_H

#if defined(ENABLE_CV)

#include <Modules/skabstractmodule.h>
#include <Multimedia/Image/Detection/skmotiondetect.h>
#include <Multimedia/Image/skimagedecoder.h>
#include <Multimedia/Image/skmat.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkMotionDetectorCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkMotionDetectorCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkMotionDetector extends SkAbstractModule
{
    SkMat f;

    SkMotionDetect detector;
    Size currentResolution;
    Size blockSz;
    UInt minBlocks;
    float threshold;

    SkFlowChanID sourceChan;
    SkImageDecoder sourceDecoder;

    SkFlowChanID detectionCanvasChan;
    SkFlowChanID detectionBlockChan;
    SkFlowChanID detectionBlocksChan;
    SkFlowChanID jpegChan;

    SkElapsedTimeMillis chrono;
    uint32_t detectionIntervalMS;

    public:
        Constructor(SkMotionDetector, SkAbstractModule);

    private:
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;
        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;

        void onChannelAdded(SkFlowChanID chanID)                        override;
        //void onChannelRemoved(SkFlowChanID chanID)                      override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;
};

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKMOTIONDETECTOR_H
