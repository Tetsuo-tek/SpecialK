#ifndef SKABSTRACTVIDEOMODULE_H
#define SKABSTRACTVIDEOMODULE_H

#if defined(ENABLE_CV)

#include <Modules/skabstractmodule.h>
#include <Multimedia/Image/skimageencoder.h>
#include <Multimedia/Image/skwhitebalancer.h>
#include <Multimedia/Image/skmat.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractVideoModuleCfg extends SkAbstractModuleCfg
{
    protected:
        AbstractConstructor(SkAbstractVideoModuleCfg, SkAbstractModuleCfg);

        void addStandardResolution(CStr *key, CStr *label, bool isRequired, const SkVariant &defaultValue, CStr *description);
        void addJpegCompression(CStr *key, CStr *label, bool isRequired, const SkVariant &defaultValue, CStr *description);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractVideoModule extends SkAbstractModule
{
    public:
        Slot(flipFrame);
        Slot(whiteBalancer);
        Slot(toggleMonochromatic);

    protected:
        SkMat f;

        bool flipModeEnabled;
        SkImageFlippingMode flipMode;

        bool wbEnabled;
        SkWhiteBalancer wb;

        bool grayScale;

        SkImageEncoder enc;
        int compression;
        vector<uchar> encodedBuffer;
        SkFlowChanID jpegChan;
        bool jpegChanEnabled;

        AbstractConstructor(SkAbstractVideoModule, SkAbstractModule);

        void addFlipFrameControl(CStr *label, CStr *description);
        void addWhiteBalancerCommand(CStr *label, CStr *description);
        void addToggleMonochromaticControl(CStr *label, CStr *description);

        bool addJpegPublisher();

        void tick();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKABSTRACTVIDEOMODULE_H
