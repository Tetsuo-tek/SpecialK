#ifndef SKCODEDETECTOR_H
#define SKCODEDETECTOR_H

#if defined(ENABLE_CV)

#include <Modules/skabstractmodule.h>
#include <Multimedia/Image/Detection/skcodedetect.h>
#include <Multimedia/Image/skimagedecoder.h>
#include <Multimedia/Image/skmat.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkCodeDetectorCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkCodeDetectorCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkCodeDetector extends SkAbstractModule
{
    public:
        Constructor(SkCodeDetector, SkAbstractModule);

    private:
        SkFlowSync *sync;

        SkMat f;

        SkCodeDetect detector;

        SkFlowChanID sourceChan;
        SkImageDecoder sourceDecoder;

        SkFlowChanID detectionMeshChan;
        SkFlowChanID detectionMeshesChan;
        SkFlowChanID jpegChan;

        SkElapsedTimeMillis chrono;
        uint32_t detectionIntervalMS;

        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;
        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;

        void onChannelAdded(SkFlowChanID chanID)                        override;
        //void onChannelRemoved(SkFlowChanID chanID)                      override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;
};

#endif

#endif // SKCODEDETECTOR_H
