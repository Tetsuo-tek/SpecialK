#include "skvideocomposer.h"

#if defined(ENABLE_CV)

#if defined(UMBA)
ConstructorImpl(SkVideoComposer, SkAbstractModule)
{

}

void SkVideoComposer::allowedCfgKeysSetup()
{

}

void SkVideoComposer::allowedExtendedCmdsSetup()
{

}

bool SkVideoComposer::onSetup()
{
    return false;
}

void SkVideoComposer::onStart()
{

}

void SkVideoComposer::onStop()
{

}

void SkVideoComposer::onFastTick()
{

}

void SkVideoComposer::onSlowTick()
{
}

void SkVideoComposer::onOneSecTick()
{
}

#endif
#endif
