#include "skvideosource.h"

#if defined(ENABLE_CV)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkVideoSourceCfg, SkCameraCfg)
{
}

void SkVideoSourceCfg::allowedCfgKeysSetup()
{
    //ROCK
    //v4l2src device=/dev/video0 ! video/x-raw,format=NV12,width=640,height=480, framerate=20/1 ! xvimagesink
    //v4l2src device=/dev/video0 ! videoscale ! videorate ! video/x-raw, width=1920, height=1080, framerate=10/1 ! videoconvert ! appsink
    //v4l2src device=/dev/video0 ! videoscale ! video/x-raw, width=1920, height=1080, framerate=30/1 ! videoconvert ! appsink

    addParam("source", "Input source query", SkVariant_T::T_STRING, true, "", "Source query string (example: filePath, url, gstPipeline, ..");

    SkWorkerParam *p = addParam("compression", "Compression", SkVariant_T::T_UINT8, true, 50, "JPeg compression");
    p->setMin(10);
    p->setMax(100);
    p->setStep(10);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkVideoSource, SkCamera)
{}

bool SkVideoSource::onSetup()
{
    SkAbstractModuleCfg *cfg = config();
    props.apiReference = VideoCaptureAPIs::CAP_GSTREAMER;
    source = cfg->getParameter("source")->value().toString();
    return sourceValidate();
}

bool SkVideoSource::sourceValidate()
{
    ObjectWarning("VALIDATING source: " << source);

    VideoCapture vc;

    if (vc.getExceptionMode())
        vc.setExceptionMode(false);

    if (!vc.open(source, VideoCaptureAPIs::CAP_GSTREAMER))
    {
        ObjectError("Cannot validate video source: " << source);
        return true;
    }

    Mat frame;

    if (!vc.read(frame))
    {
        ObjectError("Cannot capture checking-frame: " << source);
        return true;
    }

    props.fps = vc.get(CAP_PROP_FPS);
    props.resolution.width = vc.get(CAP_PROP_FRAME_WIDTH);
    props.resolution.height = vc.get(CAP_PROP_FRAME_HEIGHT);

    AssertKiller(props.resolution.width <= 0);

    vc.release();
    ObjectWarning("Source is VALID: " << " - res: " << props.resolution << " - fps: " << props.fps);

    return true;
}

void SkVideoSource::onStart()
{
    vc = new SkImageCapture(this);
    vc->setObjectName(this, "ImageCapture");
    vc->setup(source.c_str(), props);

    setupPublisher(publisher);
    publisher->setObjectName(this, "Publisher");

    ObjectMessage("Capture INITIALIZED"
                  << " - src: " << source
                  << " - res: " << props.resolution
                  << " - fps: " << props.fps << " [" << vc->getTickTimeMicros() << " us]"
                  << " - frameRawSize: " << publisher->getRawFrameSize() << " B");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
