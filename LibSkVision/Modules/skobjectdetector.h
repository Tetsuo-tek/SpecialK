#ifndef SKOBJECTDETECTOR_H
#define SKOBJECTDETECTOR_H

#if defined(ENABLE_CV)

#include <Modules/skabstractmodule.h>
#include <Multimedia/Image/Detection/skobjectdetect.h>
#include <Multimedia/Image/skimagedecoder.h>
#include <Multimedia/Image/skmat.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkObjectDetectorCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkObjectDetectorCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkObjectDetector extends SkAbstractModule
{
    SkMat f;

    SkObjectDetect detector;

    SkString cascadePath;

    Size areaMin;
    Size areaMax;

    SkFlowChanID sourceChan;

    SkImageDecoder sourceDecoder;

    SkFlowChanID detectionBoxChan;
    SkFlowChanID detectionBoxesChan;
    SkFlowChanID jpegChan;

    SkElapsedTimeMillis chrono;
    uint32_t detectionIntervalMS;

    public:
        Constructor(SkObjectDetector, SkAbstractModule);

    private:
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;
        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;

        void onChannelAdded(SkFlowChanID chanID)                        override;
        //void onChannelRemoved(SkFlowChanID chanID)                      override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;
};

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKOBJECTDETECTOR_H
