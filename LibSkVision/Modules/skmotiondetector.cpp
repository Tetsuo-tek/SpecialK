#if defined(ENABLE_CV)

#include "skmotiondetector.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfsutils.h"
#include <Core/System/Network/FlowNetwork/skflowserver.h>
#include "Core/System/Network/FlowNetwork/skflowsync.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkMotionDetectorCfg, SkAbstractModuleCfg)
{
}

void SkMotionDetectorCfg::allowedCfgKeysSetup()
{
    addParam("blockSize", "Detection block size", T_STRING, true, "16x16", "Detection block size");

    SkWorkerParam *p = addParam("threshold", "MSE threshold", T_FLOAT, true, 0.15f, "MSE threshold (>) to rate a block as changed");
    p->setMax(1.f);
    p->setStep(0.01);

    addParam("minBlockChanged", "Minimum blocks changed", T_UINT32, true, 30, "Minimum blocks changed to trigger a detection");

#if defined(ENABLE_CUDA)
    addParam("detectionInterval", "Detection milliseconds interval", T_UINT32, true, 50, "Sets detection interval in milliseconds");

#else
    addParam("detectionInterval", "Detection milliseconds interval", T_UINT32, true, 200, "Sets detection interval in milliseconds");

#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkMotionDetector, SkAbstractModule)
{
    currentResolution = Size(-1, -1);
    minBlocks = 0;
    threshold = 0.f;

    sourceChan = -1;
    detectionCanvasChan = -1;
    detectionBlockChan = -1;
    detectionBlocksChan = -1;
    jpegChan = -1;
    detectionIntervalMS = 0;
}

bool SkMotionDetector::onSetup()
{
    SkAbstractModuleCfg *cfg = config();

    SkString s = cfg->getParameter("blockSize")->value().toString();

    if (!parseSize(objectName(), s.c_str(), &blockSz))
        return false;

    threshold = cfg->getParameter("threshold")->value().toFloat();
    minBlocks = cfg->getParameter("minBlockChanged")->value().toUInt32();
    detectionIntervalMS = cfg->getParameter("detectionInterval")->value().toUInt32();

    return addStreamingChannel(sourceChan, FT_VIDEO_DATA, T_BYTEARRAY, "Input")
           && addStreamingChannel(detectionCanvasChan, FT_CV_OBJECT_DETECTED_BOX, T_BYTEARRAY, "DetectionCanvas")
           && addStreamingChannel(detectionBlockChan, FT_CV_OBJECT_DETECTED_BOX, T_BYTEARRAY, "DetectionBlock")
           && addStreamingChannel(detectionBlocksChan, FT_CV_OBJECT_DETECTED_BOX, T_STRING, "DetectionBlocks", SkMimeType::getMimeType("json"))
           && addStreamingChannel(jpegChan, FT_CV_OBJECT_DETECTED_FRAME, T_BYTEARRAY, "JPeg", SkMimeType::getMimeType("jpeg"));
}

void SkMotionDetector::onStart()
{
    detector.setObjectName(this, "Detector");
    sourceDecoder.setup(&f.get());

    ObjectMessage("Motion detector INITIALIZED ["
                  << "threshold: " << threshold << "; "
                  << "minBlocks: " << minBlocks << "; "
                  << "blockSize: " << blockSz << "]");
}

void SkMotionDetector::onStop()
{}

void SkMotionDetector::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        eventLoop()->changeFastZone(skApp->getFastInterval());
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }

    else
    {
        if (detector.isInitialized())
        {
            currentResolution = Size(-1, -1);
            detector.release();
        }

        eventLoop()->changeFastZone(200000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }
}

void SkMotionDetector::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (ch->chanID == sourceChan)
        subscribeChannel(ch);//self-subscrbing (publishing on source-queue is possible by attaching another producer-channel)
}

/*void SkMotionDetector::onChannelRemoved(SkFlowChanID chanID)
{}*/

void SkMotionDetector::onChannelPublishStartRequest(SkFlowChanID chanID)
{
}

void SkMotionDetector::onChannelPublishStopRequest(SkFlowChanID chanID)
{
}

void SkMotionDetector::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.data.isEmpty() || !isEnabled() || chData.chanID != sourceChan)
        return;

    if (chrono.stop() < detectionIntervalMS)
        return;

    chrono.start();

    sourceDecoder.loadFromBuffer(chData.data.data(), chData.data.size());

    if (f.isEmpty())
        return;

    if (currentResolution != f.size())
    {
        if (detector.isInitialized())
            detector.release();

        currentResolution = f.size();
        detector.setup(currentResolution, blockSz, threshold);
    }

    detector.tick(f.get());

    if (detector.count() < minBlocks)
        return;

    SkList<Rect> &detectedBoxes = detector.getDetectedBlocks();
    SkVariantList blocks;

    SkAbstractListIterator<Rect> *itr = detectedBoxes.iterator();

    while(itr->next())
    {
        Rect &block = itr->item();

        UInt *r = new UInt [4];
        r[0] = block.x;
        r[1] = block.y;
        r[2] = block.width;
        r[3] = block.height;

        publish(detectionBlockChan, SkArrayCast::toChar(r), 4*sizeof(UInt));

        delete [] r;

        SkArgsMap m;
        m["x"] = block.x;
        m["y"] = block.y;
        m["w"] = block.width;
        m["h"] = block.height;

        blocks << m;
    }

    Rect &detectedCanvas = detector.getDetectedCanvas();

    SkArgsMap canvas;
    canvas["x"] = detectedCanvas.x;
    canvas["y"] = detectedCanvas.y;
    canvas["w"] = detectedCanvas.width;
    canvas["h"] = detectedCanvas.height;

    setVariable("canvas", canvas);
    setVariable("blocks", blocks);

    publish(detectionCanvasChan, canvas);
    publish(detectionBlocksChan, blocks);
    publish(jpegChan, chData.data.toVoid(), chData.data.size());
}

void SkMotionDetector::onFastTick()
{}

void SkMotionDetector::onSlowTick()
{}

void SkMotionDetector::onOneSecTick()
{}

#endif
