#ifndef SKFACERECOGNIZER_H
#define SKFACERECOGNIZER_H

#if defined(ENABLE_CV)

#include <Modules/skabstractmodule.h>
#include <Multimedia/Image/skimagedecoder.h>
#include <Multimedia/Image/skmat.h>
#include <opencv2/face.hpp>
#include <opencv2/face/facerec.hpp>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

typedef struct
{
    bool enabled;
    SkString cascadeForFace;
    SkString cascadeForEyes;
    SkString cascadeForMouth;
    Size areaMin;
    Size areaMax;
    ulong interval_MS;
    SkString trainingPath;
    Size trainingCanvas;
    SkString trainingImageFormat;
    int jpgQuality;
} FaceDtcTickerCfg;

typedef struct
{
    int64_t id;
    double confidence;
    SkString name;
    Rect faceCanvas;
} FaceDtcIdentity;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFaceRecognizerCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkFaceRecognizerCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFaceRecognizer extends SkAbstractModule
{
    SkMat f;

    SkImageDecoder sourceDecoder;

    SkString sourceForFramesChanName;
    SkString sourceForBoxesChanName;

    SkFlowChanID sourceForFramesChan;
    SkFlowChanID sourceForBoxesChan;
    SkFlowChanID jpegChan;
    SkFlowChanID faceChan;

    SkString trainingPath;
    Size trainingCanvas;
    SkString trainingImageFormat;

    Ptr<face::FaceRecognizer> model;
    vector<Rect> boxes;
    SkMat temporaryFaceRoiFrame;
    vector<Mat> facesIdentityImages;
    vector<int> facesIdentityIDs;
    SkStringList facesIdentityNames;

    public:
        Constructor(SkFaceRecognizer, SkAbstractModule);

    private:
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;
        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;

        void makeTraining();
        bool recognize(Mat &recognizingCanvas, Rect &canvas, FaceDtcIdentity &identity);
};

#endif

#endif // SKFACERECOGNIZER_H
