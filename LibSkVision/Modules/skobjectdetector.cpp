#if defined(ENABLE_CV)

#include "skobjectdetector.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfsutils.h"
#include <Core/System/Network/FlowNetwork/skflowserver.h>
#include "Core/System/Network/FlowNetwork/skflowsync.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkObjectDetectorCfg, SkAbstractModuleCfg)
{
}

void SkObjectDetectorCfg::allowedCfgKeysSetup()
{
    addParam("areaMin", "Minimum detect area", T_STRING, true, "80x80", "Minimum area for bounding-box");
    addParam("areaMax", "Maximum detect area", T_STRING, true, "500x500", "Maximum area for bounding-box");

#if defined(ENABLE_CUDA)
    addParam("cascadePath", "Xml cascade detection model", T_STRING, true, "lbpcascade_frontalface.xml", "File path of the xml Cascade detection model to grab bounding-boxes");
    addParam("detectionInterval", "Detection milliseconds interval", T_UINT32, true, 10, "Sets detection interval in milliseconds");

#else
    addParam("cascadePath", "Xml cascade detection model", T_STRING, true, "haarcascade_frontalface_alt2.xml", "File path of the xml Cascade detection model to grab bounding-boxes");
    addParam("detectionInterval", "Detection milliseconds interval", T_UINT32, true, 200, "Sets detection interval in milliseconds");

#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkObjectDetector, SkAbstractModule)
{
    //sync = nullptr;

    sourceChan = -1;
    detectionBoxChan = -1;
    detectionBoxesChan = -1;
    jpegChan = -1;
    detectionIntervalMS = 0;
}

bool SkObjectDetector::onSetup()
{
    SkAbstractModuleCfg *cfg = config();

    SkString s = cfg->getParameter("areaMin")->value().toString();

    if (!parseSize(objectName(), s.c_str(), &areaMin))
        return false;

    s = cfg->getParameter("areaMax")->value().toString();

    if (!parseSize(objectName(), s.c_str(), &areaMax))
        return false;

    cascadePath = workingDir();
    cascadePath.append(cfg->getParameter("cascadePath")->value().toString());

    if (!SkFsUtils::exists(cascadePath.c_str())
            || !SkFsUtils::isFile(cascadePath.c_str())
            || !SkFsUtils::isReadable(cascadePath.c_str()))
    {
        ObjectError("Cascade file NOT valid: " << cascadePath);
        return false;
    }

    detectionIntervalMS = cfg->getParameter("detectionInterval")->value().toUInt32();

    return addStreamingChannel(sourceChan, FT_VIDEO_DATA, T_BYTEARRAY, "Input")
           && addStreamingChannel(detectionBoxChan, FT_CV_OBJECT_DETECTED_BOX, T_BYTEARRAY, "DetectionBox")
           && addStreamingChannel(detectionBoxesChan, FT_CV_OBJECT_DETECTED_BOX, T_STRING, "DetectionBoxes", SkMimeType::getMimeType("json"))
           && addStreamingChannel(jpegChan, FT_CV_OBJECT_DETECTED_FRAME, T_BYTEARRAY, "JPeg", SkMimeType::getMimeType("jpeg"));
}

void SkObjectDetector::onStart()
{
    detector.setObjectName(this, "Detector");

    if (!detector.setup(&f.get(), cascadePath.c_str(), areaMin, areaMax))
        return;

    sourceDecoder.setup(&f.get());

    ObjectMessage("Cascade classifier INITIALIZED ["
              << "areaMin: " << areaMin.width << "x" << areaMin.height << "; "
              << "areaMax: " << areaMax.width << "x" << areaMax.height
              << "]: " << cascadePath);
}

void SkObjectDetector::onStop()
{}

void SkObjectDetector::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        eventLoop()->changeFastZone(skApp->getFastInterval());
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }

    else
    {
        eventLoop()->changeFastZone(200000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }
}

void SkObjectDetector::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (ch->chanID == sourceChan)
        subscribeChannel(ch);//self-subscrbing (publishing on source-queue is possible attaching another producer-channel)
}

/*void SkObjectDetector::onChannelRemoved(SkFlowChanID chanID)
{}*/

void SkObjectDetector::onChannelPublishStartRequest(SkFlowChanID chanID)
{
}

void SkObjectDetector::onChannelPublishStopRequest(SkFlowChanID chanID)
{
}

void SkObjectDetector::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.data.isEmpty() || !isEnabled() || chData.chanID != sourceChan)
        return;

    if (chrono.stop() < detectionIntervalMS)
        return;

    chrono.start();

    sourceDecoder.loadFromBuffer(chData.data.data(), chData.data.size());

    if (f.isEmpty())
        return;

    detector.tick();

    if (!detector.isEmpty())
    {
        vector<Rect> &detectedCanvas = detector.getDetectedCanvases();
        SkVariantList boxes;

        for(uint64_t i=0; i<detectedCanvas.size(); i++)
        {
            Rect &boundingBox = detectedCanvas[i];

            UInt *r = new UInt [4];
            r[0] = boundingBox.x;
            r[1] = boundingBox.y;
            r[2] = boundingBox.width;
            r[3] = boundingBox.height;

            publish(detectionBoxChan, SkArrayCast::toChar(r), 4*sizeof(UInt));

            delete [] r;

            SkArgsMap m;
            m["x"] = boundingBox.x;
            m["y"] = boundingBox.y;
            m["w"] = boundingBox.width;
            m["h"] = boundingBox.height;

            boxes << m;
        }

        setVariable("boxes", boxes);
        publish(detectionBoxesChan, boxes);
        publish(jpegChan, chData.data.toVoid(), chData.data.size());

    }
}

void SkObjectDetector::onFastTick()
{}

void SkObjectDetector::onSlowTick()
{}

void SkObjectDetector::onOneSecTick()
{}

#endif
