#if defined(ENABLE_CV)

#include "skcamera.h"
#include "Core/System/Filesystem/skmimetype.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkCameraCfg, SkAbstractModuleCfg)
{
}

void SkCameraCfg::allowedCfgKeysSetup()
{
    SkWorkerParam *p = nullptr;

    p = addParam("camID", "Capturing device", SkVariant_T::T_UINT8, true, 0, "Video device ID");
    p->setMin(0);

    p = addParam("fps", "Frames speed", SkVariant_T::T_UINT16, true, 10, "Capturing frames per second");
    p->setMin(1);

    p = addParam("resolution", "Resolution", SkVariant_T::T_STRING, true, "640x480", "Output frame resolution");
    p->addOption("240p (4:3) 320x240", "320x240");
    p->addOption("240p () 426x240", "426x240");
    p->addOption("320p () 480x320", "480x320");
    p->addOption("360p (16:9) 640x360", "640x360");
    p->addOption("480p (4:3) 640x480", "640x480");
    p->addOption("480p (16:9) 854x480", "854x480");
    p->addOption("720p (16:9) 1280x720", "1280x720");
    p->addOption("1080p (16:9) 1920x1080", "1920x1080");
    p->addOption("1440p (16:9) 2560x1440", "2560x1440");
    p->addOption("2k or 1080p (1:1.77) 2048x1080", "2048x1080");
    p->addOption("4k or 2160p (1:1.9) 3840x2160", "3840x2160");
    p->addOption("8k or 4320p (16:9) 7680x4320", "7680x4320");

    p = addParam("compression", "Compression", SkVariant_T::T_UINT8, true, 50, "JPeg compression");
    p->setMin(10);
    p->setMax(100);
    p->setStep(10);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkCamera, SkAbstractModule)
{
    vc = nullptr;
    publisher = new SkFlowVideoPublisher(this);

    capID = -1;

    props.enabled = false;
    props.realTimeMode = true;// COULD BE FALSE AS IN robot-video-capture
    props.forceFps = true;
    props.forceResolution = true;

    SlotSet(flipFrame);
    SlotSet(whiteBalancer);
    SlotSet(toggleMonochromatic);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkCamera::onInit()
{
    SkWorkerCommand *cmd = addCommand("White balancer", "Enables white balancer", whiteBalancer_SLOT);
    SkWorkerParam *p = cmd->addParam("enabled", "Enable", T_BOOL, true, false, "Enables white balancer activity");
    p = cmd->addParam("wbAlgo", "Algorithm", T_UINT8, true, SK_SIMPLE_WB, "White balancing algotithm");
    p->addOption("Simple", SK_SIMPLE_WB);
    p->addOption("Grayworld", SK_GRAYWORLD_WB);

    p = addSingleControl("Flip", T_UINT8, SK_NONE_FLIPPING, "Flip the frame", flipFrame_SLOT);
    p->addOption("None", SK_NONE_FLIPPING);
    p->addOption("Horizontal", SK_HORIZONTAL_FLIPPING);
    p->addOption("Vertical", SK_VERTICAL_FLIPPING);
    p->addOption("Horizontal/Vertical", SK_TOTAL_FLIPPING);

    addSingleControl("Monochromatic", T_BOOL, false, "Sets to monochromatic", toggleMonochromatic_SLOT);
}

bool SkCamera::onSetup()
{
    SkAbstractModuleCfg *cfg = config();

    props.apiReference = VideoCaptureAPIs::CAP_V4L2;
    props.fps = cfg->getParameter("fps")->value().toInt();
    capID = cfg->getParameter("camID")->value().toInt();

    if (!parseSize(objectName(), cfg->getParameter("resolution")->value().data(), &props.resolution))
        return false;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkCamera::onStart()
{
    vc = new SkImageCapture(this);
    vc->setObjectName(this, "ImageCapture");
    vc->setup(capID, props);

    setupPublisher(publisher);
    publisher->setObjectName(this, "Publisher");
}

void SkCamera::onStop()
{
    vc->destroyLater();
    vc = nullptr;

    capID = -1;
    //frameRawSize = 0;

    f.clear();

    ObjectMessage("Capture QUITTED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkCamera::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        if (!vc->open())
        {
            requestToEnable(false);//
            return;
        }

        publisher->setup(&f, props.resolution, props.fps, config()->getParameter("compression")->value().toInt());

        ObjectMessage("Capture INITIALIZED"
                      << " - ID: " << capID
                      << " - res: " << props.resolution
                      << " - fps: " << props.fps << " [" << vc->getTickTimeMicros() << " us]"
                      << " - frameRawSize: " << publisher->getRawFrameSize() << " B");

        publisher->start();

        eventLoop()->changeSlowZone(eventLoop()->getFastInterval()*4);
        ObjectMessage("Capture ENABLED");
        onFastTick();
    }

    else
    {
        if (props.enabled)
        {
            props.enabled = false;
            vc->setEnabled(props.enabled);
        }

        publisher->stop();

        if (vc->isOpen())
            vc->close();

        eventLoop()->changeFastZone(250000);
        eventLoop()->changeSlowZone(250000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

        ObjectMessage("Capture DISABLED");
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkCamera::onFastTick()
{
    if (!vc || !vc->isOpen() || !isEnabled())
        return;

    if (props.enabled)
    {
        if (!publisher->hasTargets())
        {
            ObjectMessage("Capture tick NOT-ACTIVE");
            props.enabled = false;
            vc->setEnabled(false);
            return;
        }
    }

    else
    {
        if (publisher->hasTargets())
        {
            ObjectMessage("Capture tick ACTIVE");
            props.enabled = true;
            vc->setEnabled(true);
        }

        else
            return;
    }

    f.set(vc->getFrame());
    publisher->tick();
}

void SkCamera::onSlowTick()
{
    if (!vc)
        return;
}

void SkCamera::onOneSecTick()
{
    if (!vc)
        return;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkCamera, whiteBalancer)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    bool enabled = cmdMap["enabled"].toBool();
    SkWbAlgorithm a = static_cast<SkWbAlgorithm>(cmdMap["wbAlgo"].toInt());

    if ((!publisher->isMonochromaticEnabled() || (publisher->isMonochromaticEnabled() && a == SK_SIMPLE_WB)) && analyzeTransaction(t, v))
    {
        if (enabled)
            publisher->enableWhiteBalancer(a);

        else
            publisher->disableWhiteBalancer();
    }

    else
    {
        if (publisher->isMonochromaticEnabled() && a == SK_GRAYWORLD_WB)
            t->setError("Monochromatic is ENABLED and it is NOT compatible with white balancer algorithm Grayworld");
        else
            t->setError("Command NOT valid");

        t->setResponse(cmdMap);
    }

    t->goBack();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkCamera, flipFrame)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
    {
        SkImageFlippingMode flipMode = static_cast<SkImageFlippingMode>(cmdMap["flipFrame"].toInt());
        publisher->setFlip(flipMode);
    }

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

SlotImpl(SkCamera, toggleMonochromatic)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (((publisher->isWhiteBalancerEnabled() && publisher->getWhiteBalancerAlgo() == SK_SIMPLE_WB) || !publisher->isWhiteBalancerEnabled())
        && analyzeTransaction(t, v))
    {
        publisher->setMonochromatic(cmdMap["toggleMonochromatic"].toBool());
    }

    else
    {
        if (publisher->isMonochromaticEnabled() && publisher->getWhiteBalancerAlgo() == SK_GRAYWORLD_WB)
            t->setError("Monochromatic is ENABLED and it is NOT compatible with white balancer algorithm Grayworld");
        else
            t->setError("Command NOT valid");

        t->setResponse(cmdMap);
    }

    t->goBack();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


#endif
