#if defined(ENABLE_CV)

#include "skabstractvideomodule.h"

#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/System/Filesystem/skmimetype.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractVideoModuleCfg, SkAbstractModuleCfg)
{}

void SkAbstractVideoModuleCfg::addStandardResolution(CStr *key, CStr *label, bool isRequired, const SkVariant &defaultValue, CStr *description)
{
    SkWorkerParam *p = nullptr;

    p = addParam(key, label, SkVariant_T::T_STRING, isRequired, defaultValue, description);
    p->addOption("240p (4:3) 320x240", "320x240");
    p->addOption("240p () 426x240", "426x240");
    p->addOption("320p () 480x320", "480x320");
    p->addOption("360p (16:9) 640x360", "640x360");
    p->addOption("480p (4:3) 640x480", "640x480");
    p->addOption("480p (16:9) 854x480", "854x480");
    p->addOption("720p (16:9) 1280x720", "1280x720");
    p->addOption("1080p (16:9) 1920x1080", "1920x1080");
    p->addOption("1440p (16:9) 2560x1440", "2560x1440");
    p->addOption("2k or 1080p (1:1.77) 2048x1080", "2048x1080");
    p->addOption("4k or 2160p (1:1.9) 3840x2160", "3840x2160");
    p->addOption("8k or 4320p (16:9) 7680x4320", "7680x4320");
}

void SkAbstractVideoModuleCfg::addJpegCompression(CStr *key, CStr *label, bool isRequired, const SkVariant &defaultValue, CStr *description)
{
    SkWorkerParam *p = nullptr;

    p = addParam(key, label, SkVariant_T::T_UINT8, isRequired, defaultValue, description);
    p->setMin(10);
    p->setMax(100);
    p->setStep(10);

}
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(SkAbstractVideoModule, SkAbstractModule)
{
    flipModeEnabled = false;
    flipMode = SK_NONE_FLIPPING;

    wbEnabled = false;

    compression = 70;
    jpegChan = -1;
    jpegChanEnabled = false;

    grayScale = false;

    SlotSet(flipFrame);
    SlotSet(whiteBalancer);
    SlotSet(toggleMonochromatic);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractVideoModule::tick()
{
    if (f.isEmpty())
        return;

    if (grayScale)
        f.toGrayScale();

    if (wbEnabled)
        wb.tick();

    if (flipModeEnabled)
        f.flip(flipMode);

    if (jpegChanEnabled)
    {
        enc.saveToBuffer(f.get(), encodedBuffer);
        publish(jpegChan, encodedBuffer.data(), encodedBuffer.size());
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractVideoModule::addFlipFrameControl(CStr *label, CStr *description)
{
    SkWorkerParam *p = addSingleControl(label, T_UINT8, SK_NONE_FLIPPING, description, flipFrame_SLOT);

    p->addOption("None", SK_NONE_FLIPPING);
    p->addOption("Horizontal", SK_HORIZONTAL_FLIPPING);
    p->addOption("Vertical", SK_VERTICAL_FLIPPING);
    p->addOption("Horizontal/Vertical", SK_TOTAL_FLIPPING);
}

void SkAbstractVideoModule::addWhiteBalancerCommand(CStr *label, CStr *description)
{
    SkWorkerCommand *cmd = nullptr;
    SkWorkerParam *p = nullptr;

    cmd = addCommand(label, description, whiteBalancer_SLOT);
    p = cmd->addParam("enabled", "Enable", T_BOOL, true, false, "Enables white balancer activity");
    p = cmd->addParam("wbAlgo", "Algorithm", T_UINT8, true, SK_SIMPLE_WB, "White balancing algotithm");
    p->addOption("Simple", SK_SIMPLE_WB);
    p->addOption("Grayworld", SK_GRAYWORLD_WB);
}

void SkAbstractVideoModule::addToggleMonochromaticControl(CStr *label, CStr *description)
{
    addSingleControl(label, T_BOOL, false, description, toggleMonochromatic_SLOT);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractVideoModule::addJpegPublisher()
{
    return addStreamingChannel(jpegChan, FT_VIDEO_DATA, T_BYTEARRAY, "JPeg", SkMimeType::getMimeType("jpeg"));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractVideoModule, whiteBalancer)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;

    v.copyToMap(cmdMap);

    SkWorkerCommand *cmd = commands()["whiteBalancer"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    bool enabled = cmdMap["enabled"].toBool();
    SkWbAlgorithm a = static_cast<SkWbAlgorithm>(cmdMap["wbAlgo"].toInt());

    if ((!grayScale || (grayScale && enabled && a == SK_SIMPLE_WB)) && cmd->load(cmdMap))
    {
        updateCommand(cmd);
        wbEnabled = enabled;

        if (wbEnabled)
            wb.setup(&f.get(), &f.get(), a);

        if (t)
        {
            t->setResponse(cmdMap);
            t->goBack();
        }
    }

    else
    {
        if (t)
        {
            if (grayScale)
                t->setError("Monochromatic is ENABLED and it is NOT compatible with white balancer algorithm Grayworld");
            else
                t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAbstractVideoModule, flipFrame)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;
    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["flipFrame"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (ctrl->load(cmdMap))
    {
        updateCommand(ctrl);

        flipMode = static_cast<SkImageFlippingMode>(cmdMap["flipFrame"].toInt());
        flipModeEnabled = (flipMode != SK_NONE_FLIPPING);
    }

    else
    {
        if (t)
        {
            t->setError("Command NOT valid");
            t->setResponse(cmdMap);
        }
    }

    if (t)
        t->goBack();
}

SlotImpl(SkAbstractVideoModule, toggleMonochromatic)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;

    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["toggleMonochromatic"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (((wbEnabled && wb.algorithm() == SK_SIMPLE_WB) || !wbEnabled) && ctrl->load(cmdMap))
    {
        updateCommand(ctrl);
        grayScale = cmdMap["toggleMonochromatic"].toBool();

        if (t)
        {
            //t->setResponse(cmdMap);
            t->goBack();
        }
    }

    else
    {
        if (t)
        {
            if (wbEnabled)
                t->setError("White balancer is ENABLED and it is NOT compatible with gray-scale frames");
            else
                t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
