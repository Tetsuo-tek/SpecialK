#if defined(ENABLE_CV)

#include "skfacerecognizer.h"
#include "Core/App/skapp.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Filesystem/skfileinfoslist.h"
#include "Core/System/Network/FlowNetwork/skflowsync.h"
#include "Core/System/Network/FlowNetwork/skflowserver.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFaceRecognizerCfg, SkAbstractModuleCfg)
{
}

void SkFaceRecognizerCfg::allowedCfgKeysSetup()
{
    addParam("detectorJpegSource", "Detector frames source", SkVariant_T::T_STRING, true, "FaceDetect.JPeg", "Detector module frames source");
    addParam("detectorBoxSource", "Detector boxes source", SkVariant_T::T_STRING, true, "FaceDetect.DetectionBox", "Detector module boxes source");
    addParam("trainingPath", "Training path", SkVariant_T::T_STRING, true, "faces", "Path containing training data");
    addParam("trainingCanvasSize", "Training canvas", SkVariant_T::T_STRING, true, "92x112", "Training canvas size");
    addParam("trainingImageFormat", "Training format",SkVariant_T::T_STRING, true, "pgm", "Training image file format");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFaceRecognizer, SkAbstractModule)
{
    sourceForFramesChan = -1;
    sourceForBoxesChan = -1;
    jpegChan = -1;
    faceChan = -1;
}

bool SkFaceRecognizer::onSetup()
{
    SkAbstractModuleCfg *cfg = config();

    sourceForFramesChanName = cfg->getParameter("detectorJpegSource")->value().toString();
    sourceForBoxesChanName = cfg->getParameter("detectorBoxSource")->value().toString();

    SkString s = cfg->getParameter("trainingCanvasSize")->value().toString();

    if (!parseSize(objectName(), s.c_str(), &trainingCanvas))
        return false;

    trainingPath = workingDir();
    trainingPath.append(cfg->getParameter("trainingPath")->value().toString());

    if (!SkFsUtils::exists(trainingPath.c_str())
            || !SkFsUtils::isDir(trainingPath.c_str())
            || !SkFsUtils::isReadable(trainingPath.c_str()))
    {
        ObjectError("Training directory NOT valid: " << trainingPath);
        return false;
    }

    trainingImageFormat = cfg->getParameter("trainingImageFormat")->value().toString();
    sourceDecoder.setup(&f.get());

    return addStreamingChannel(faceChan, FT_CV_OBJECT_RECOGNIZED_IDENTITY, T_STRING, "FaceID", SkMimeType::getMimeType("json"))
           && addStreamingChannel(jpegChan, FT_CV_OBJECT_DETECTED_FRAME, T_BYTEARRAY, "JPeg", SkMimeType::getMimeType("jpeg"));
}

void SkFaceRecognizer::onStart()
{
    makeTraining();
}

void SkFaceRecognizer::onStop()
{
}

void SkFaceRecognizer::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        ObjectMessage("Recognizing STARTED");

        eventLoop()->changeFastZone(skApp->getFastInterval());
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }

    else
    {
        eventLoop()->changeFastZone(200000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

        ObjectMessage("Recognizing STOPPED");
    }
}

/*void SkFaceRecognizer::onChannelAdded(SkFlowChanID chanID)
{}

void SkFaceRecognizer::onChannelRemoved(SkFlowChanID chanID)
{}*/

void SkFaceRecognizer::onChannelPublishStartRequest(SkFlowChanID chanID)
{
    if (chanID != faceChan && chanID != jpegChan)
        return;

    if (!isSubscribed(sourceForBoxesChan) && containsChannel(sourceForBoxesChanName.c_str()) && containsChannel(sourceForFramesChanName.c_str()))
    {
        subscribeChannel(sourceForBoxesChanName.c_str(), sourceForBoxesChan);
        subscribeChannel(sourceForFramesChanName.c_str(), sourceForFramesChan);
    }
}

void SkFaceRecognizer::onChannelPublishStopRequest(SkFlowChanID chanID)
{
    if (chanID != faceChan && chanID != jpegChan)
        return;

    if (isSubscribed(sourceForBoxesChan) && containsChannel(sourceForBoxesChanName.c_str()) && containsChannel(sourceForFramesChanName.c_str()))
    {
        unsubscribeChannel(sourceForBoxesChan);
        unsubscribeChannel(sourceForFramesChan);
    }
}

void SkFaceRecognizer::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.data.isEmpty() || !isEnabled())
        return;

    if (chData.chanID == sourceForBoxesChan)
    {
        uint32_t *r = SkArrayCast::toUInt32(chData.data.toVoid());

        Rect boundingBox;
        boundingBox.x = r[0];
        boundingBox.y = r[1];
        boundingBox.width = r[2];
        boundingBox.height = r[3];

        boxes.push_back(boundingBox);
    }

    else if (chData.chanID == sourceForFramesChan)
    {
        sourceDecoder.loadFromBuffer(chData.data.data(), chData.data.size());

        if (f.isEmpty())
            return;

        f.toGrayScale();
        SkVariantList faces;

        for(size_t i=0; i<boxes.size(); i++)
        {
            Rect &boundingBox = boxes[i];
            f.region(boundingBox, temporaryFaceRoiFrame.get());

            int side = trainingCanvas.width;

            if (trainingCanvas.height > side)
                side = trainingCanvas.height;

            Mat resized;
            Size rectSize(side,side);
            SkImageUtils::doResize(temporaryFaceRoiFrame.get(), resized, rectSize);

            int offsetW = abs(side - trainingCanvas.width) / 2;
            int offsetH = abs(side - trainingCanvas.height) / 2;

            Rect rect(offsetW, offsetH, trainingCanvas.width, trainingCanvas.height);
            Mat w = resized(rect);

            FaceDtcIdentity identity;

            if (recognize(w, boundingBox, identity))
            {
                SkVariantList box;

                box << boundingBox.x
                    << boundingBox.y
                    << boundingBox.width
                    << boundingBox.height;

                SkArgsMap m;
                m["id"] = identity.id;
                m["confidence"] = identity.confidence;
                m["name"] = identity.name;
                m["box"] = box;

                publish(faceChan, m);
                faces << m;
            }
        }

        if (!faces.isEmpty())
        {
            publish(jpegChan, chData.data.toVoid(), chData.data.size());
            setVariable("Faces", faces);
        }

        boxes.clear();
    }
}

void SkFaceRecognizer::onFastTick()
{}

void SkFaceRecognizer::onSlowTick()
{}

void SkFaceRecognizer::onOneSecTick()
{}

void SkFaceRecognizer::makeTraining()
{
    SkFileInfo info;

    SkFsUtils::fillFileInfo(trainingPath.c_str(), info);

    if (!info.isDir)
    {
        ObjectError("Training path is NOT valid: " << info.completeAbsolutePath);
        return;
    }

    SkString filePath = SkFsUtils::adjustPathEndSeparator(info.absoluteParentPath.c_str());
    filePath.append(info.completeName);

    ObjectMessage("Training path: " << filePath);

    SkFileInfosList *fl = new SkFileInfosList;

    SkFsUtils::ls(filePath.c_str(), fl, true);

    if (fl->isEmpty())
    {
        ObjectError("Training path is EMPTY: " << info.absoluteParentPath);
        fl->destroyLater();
        return;
    }

    if (fl->open())
    {
        int currID = -1;

        while(fl->next())
        {
            if (!fl->currentPathIsDir() && fl->currentPath().endsWith(trainingImageFormat.c_str()))
            {
                SkFsUtils::fillFileInfo(fl->currentPath().c_str(), info);

                filePath = SkFsUtils::adjustPathEndSeparator(info.absoluteParentPath.c_str());
                filePath.append(info.completeName);

                SkFsUtils::fillFileInfo(info.absoluteParentPath.c_str(), info);

                if (facesIdentityNames.contains(info.name))
                {
                    currID = static_cast<int>(facesIdentityNames.indexOf(info.name));
                    ObjectDebug("Adding new file to identity [ID: " << currID << "] -> " << info.name << " [" << filePath << "]");
                }

                else
                {
                    currID = static_cast<int>(facesIdentityNames.count());
                    facesIdentityNames << info.name;
                    ObjectDebug("Adding new identity directory [ID: " << currID << "] -> " << info.name);
                }

                try
                {
                    facesIdentityImages.push_back(imread(filePath, IMREAD_GRAYSCALE));
                }

                catch(cv::Exception &e)
                {
                    ObjectError(e.err);
                    fl->close();
                }

                facesIdentityIDs.push_back(currID);
            }
        }

        fl->close();
        fl->destroyLater();

        if (facesIdentityNames.count() < 2)
            ObjectWarning("Recognizer required at least 2 identities to perform recognition");

        else
        {
            ObjectMessage("Faces-indentity images resolution: " << trainingCanvas.width << "x" << trainingCanvas.height);

            for(uint64_t i=0; i<facesIdentityNames.count(); i++)
            {
                SkFsUtils::fillFileInfo(facesIdentityNames[i].c_str(), info);
                facesIdentityNames[i] = info.completeName;
            }

            ObjectMessage("There are " << facesIdentityNames.count() << " identities usable for training-phase: " << facesIdentityNames.join(", "));

            try
            {
                ObjectWarning("Training for face-identities started");
                SkElapsedTime processingTime;
                processingTime.start();
                model = face::LBPHFaceRecognizer::create();//createLBPHFaceRecognizer();//FisherFaceRecognizer::create();//createFisherFaceRecognizer();//EigenFaceRecognizer::create();//createEigenFaceRecognizer();
                model->train(facesIdentityImages, facesIdentityIDs);
                ObjectWarning("Training terminated successfully [" << processingTime.stop() << " s]");
            }

            catch(cv::Exception &e)
            {
                ObjectError(e.err);
            }
        }
    }
}

bool SkFaceRecognizer::recognize(Mat &recognizingCanvas, Rect &canvas, FaceDtcIdentity &identity)
{
    try
    {
        int prediction = -1;
        double confidence = -1;

        model->predict(recognizingCanvas, prediction, confidence);

        identity.id = static_cast<int64_t>(prediction);
        identity.confidence = confidence;

        if (identity.id >= 0)
            identity.name = facesIdentityNames[static_cast<uint64_t>(identity.id)];

        identity.faceCanvas = canvas;
    }

    catch(cv::Exception &e)
    {
        ObjectError(e.err);
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
