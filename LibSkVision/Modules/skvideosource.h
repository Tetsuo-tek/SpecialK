#ifndef SKVIDEOSOURCE_H
#define SKVIDEOSOURCE_H

#if defined(ENABLE_CV)

#include "skcamera.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkVideoSourceCfg extends SkCameraCfg
{
    public:
        Constructor(SkVideoSourceCfg, SkCameraCfg);

    private:
        void allowedCfgKeysSetup()                                      override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkVideoSource extends SkCamera
{
    public:
        Constructor(SkVideoSource, SkCamera);

        bool onSetup()                                                  override;
        void onStart()                                                  override;

    private:
        SkString source;

        bool sourceValidate();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKVIDEOSOURCE_H
