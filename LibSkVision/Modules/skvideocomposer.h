#ifndef SKVIDEOCOMPOSER_H
#define SKVIDEOCOMPOSER_H

#if defined(ENABLE_CV)

#if defined(UMBA)
#include <Modules/skabstractmodule.h>

class SkVideoComposer extends SkAbstractModule
{
    public:
        Constructor(SkVideoComposer, SkAbstractModule);

    private:
        void allowedCfgKeysSetup()              override;
        void allowedExtendedCmdsSetup()         override;

        bool onSetup()                          override;
        void onStart()                          override;
        void onStop()                           override;

        //void onSetEnabled(bool enabled)         override;

        void onFastTick()                       override;
        void onSlowTick()                       override;
        void onOneSecTick()                     override;
};
#endif

#endif
#endif // SKVIDEOCOMPOSER_H
