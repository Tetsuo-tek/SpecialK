#ifndef SKCAMERA_H
#define SKCAMERA_H

#if defined(ENABLE_CV)

#include "Core/System/Network/FlowNetwork/skflowvideopublisher.h"
#include "Modules/skabstractmodule.h"
#include "Multimedia/Image/skimagecapture.h"

/*
#include "Multimedia/Image/skimageencoder.h"
#include "Multimedia/Image/skwhitebalancer.h"
#include "Multimedia/Image/skmat.h"
#include "Multimedia/Magma/skmagmawriter.h"
#include "Multimedia/Image/Encoders/Magma/skpsyvenc.h"*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkCameraCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkCameraCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()                                      override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkCamera extends SkAbstractModule
{
    int capID;

    public:
        Constructor(SkCamera, SkAbstractModule);

        Slot(flipFrame);
        Slot(whiteBalancer);
        Slot(toggleMonochromatic);

    protected:
        SkImageCapture *vc;
        SkVideoCaptureProperties props;

        SkMat f;
        SkFlowVideoPublisher *publisher;

        void onInit()                                                   override;
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKCAMERA_H
