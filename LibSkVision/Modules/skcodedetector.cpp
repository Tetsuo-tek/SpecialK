#if defined(ENABLE_CV)

#include "skcodedetector.h"
#include "Core/App/skapp.h"
#include "Modules/skmodulesmanager.h"
#include "Core/Containers/skarraycast.h"
#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/System/Filesystem/skmimetype.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkCodeDetectorCfg, SkAbstractModuleCfg)
{
}

void SkCodeDetectorCfg::allowedCfgKeysSetup()
{
    addParam("detectionInterval", "Detection milliseconds interval", T_UINT32, true, 50, "Sets detection interval in milliseconds");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkCodeDetector, SkAbstractModule)
{
    sync = nullptr;

    sourceChan = -1;
    detectionMeshChan = -1;
    detectionMeshesChan = -1;
    jpegChan = -1;
    detectionIntervalMS = 0;
}

bool SkCodeDetector::onSetup()
{
    SkAbstractModuleCfg *cfg = config();
    detectionIntervalMS = cfg->getParameter("detectionInterval")->value().toUInt32();

    return addStreamingChannel(sourceChan, FT_VIDEO_DATA, T_BYTEARRAY, "Input")
           && addStreamingChannel(detectionMeshChan, FT_CV_OBJECT_DETECTED_BOX, T_BYTEARRAY, "DetectionMesh")
           && addStreamingChannel(detectionMeshesChan, FT_CV_OBJECT_DETECTED_BOX, T_STRING, "DetectionMeshes", SkMimeType::getMimeType("json"))
           && addStreamingChannel(jpegChan, FT_CV_OBJECT_DETECTED_FRAME, T_BYTEARRAY, "JPeg", SkMimeType::getMimeType("jpeg"));
}

void SkCodeDetector::onStart()
{
    detector.setObjectName(this, "Detector");

    if (!detector.setup(&f.get()))
        return;

    sourceDecoder.setup(&f.get());
    ObjectMessage("Code detector INITIALIZED");
}

void SkCodeDetector::onStop()
{}

void SkCodeDetector::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        eventLoop()->changeFastZone(skApp->getFastInterval());
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }

    else
    {
        eventLoop()->changeFastZone(200000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }
}

void SkCodeDetector::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (ch->chanID == sourceChan)
        subscribeChannel(ch);//self-subscrbing (publishing on source-queue is possible by attaching another producer-channel)
}

/*void SkCodeDetector::onChannelRemoved(SkFlowChanID chanID)
{}*/

void SkCodeDetector::onChannelPublishStartRequest(SkFlowChanID chanID)
{}

void SkCodeDetector::onChannelPublishStopRequest(SkFlowChanID chanID)
{}

void SkCodeDetector::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.data.isEmpty() || !isEnabled() || chData.chanID != sourceChan)
        return;

    if (chrono.stop() < detectionIntervalMS)
        return;

    chrono.start();

    sourceDecoder.loadFromBuffer(chData.data.data(), chData.data.size());

    if (f.isEmpty())
        return;

    detector.tick();

    if (!detector.isEmpty())
    {
        vector<SkDecodedCode> &detectedCodes = detector.getDetectedCodes();
        SkVariantList codes;

        for(uint64_t i=0; i<detectedCodes.size(); i++)
        {
            SkDecodedCode &code = detectedCodes[i];

            SkArgsMap m;
            m["type"] = code.type;
            m["data"] = code.data;

            SkVariantList polygon;
            SkVector<UInt> r;

            for(uint64_t z=0; z<code.location.size(); z++)
            {
                SkArgsMap point;
                point["x"] = code.location[z].x;
                point["y"] = code.location[z].y;
                polygon << point;

                r << code.location[z].x;
                r << code.location[z].y;
            }

            publish(detectionMeshChan, SkArrayCast::toChar(r.data()), r.count()*sizeof(UInt));

            m["polygon"] = polygon;
            codes << m;
        }

        setVariable("codes", codes);

        publish(detectionMeshesChan, codes);
        publish(jpegChan, chData.data.toVoid(), chData.data.size());
    }
}

void SkCodeDetector::onFastTick()
{}

void SkCodeDetector::onSlowTick()
{}

void SkCodeDetector::onOneSecTick()
{}

#endif
