HEADERS += \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideodetectpublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideopublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideosubscriber.h \
    $$PWD/Modules/skabstractvideomodule.h \
    $$PWD/Modules/skcamera.h \
    $$PWD/Modules/skcodedetector.h \
    $$PWD/Modules/skfacerecognizer.h \
    $$PWD/Modules/skmotiondetector.h \
    $$PWD/Modules/skobjectdetector.h \
    $$PWD/Modules/skvideocomposer.h \
    $$PWD/Modules/skvideosource.h \
    $$PWD/Multimedia/Image/Detection/skcodedetect.h \
    $$PWD/Multimedia/Image/Detection/skmotiondetect.h \
    $$PWD/Multimedia/Image/Detection/skobjectdetect.h \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvdec.h \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvdecth.h \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvenc.h \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvencth.h \
    $$PWD/Multimedia/Image/skmat.h \
    $$PWD/Multimedia/Image/skimagecapture.h \
    $$PWD/Multimedia/Image/skimagedecoder.h \
    $$PWD/Multimedia/Image/skimageencoder.h \
    $$PWD/Multimedia/Image/skimageutils.h \
    $$PWD/Multimedia/Image/skimagegradient.h \
    $$PWD/Multimedia/Image/skwhitebalancer.h

SOURCES += \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideodetectpublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideopublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowvideosubscriber.cpp \
    $$PWD/Modules/skabstractvideomodule.cpp \
    $$PWD/Modules/skcamera.cpp \
    $$PWD/Modules/skcodedetector.cpp \
    $$PWD/Modules/skfacerecognizer.cpp \
    $$PWD/Modules/skmotiondetector.cpp \
    $$PWD/Modules/skobjectdetector.cpp \
    $$PWD/Modules/skvideocomposer.cpp \
    $$PWD/Modules/skvideosource.cpp \
    $$PWD/Multimedia/Image/Detection/skcodedetect.cpp \
    $$PWD/Multimedia/Image/Detection/skmotiondetect.cpp \
    $$PWD/Multimedia/Image/Detection/skobjectdetect.cpp \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvdec.cpp \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvdecth.cpp \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvenc.cpp \
    $$PWD/Multimedia/Image/Encoders/Magma/skpsyvencth.cpp \
    $$PWD/Multimedia/Image/skmat.cpp \
    $$PWD/Multimedia/Image/skimagecapture.cpp \
    $$PWD/Multimedia/Image/skimagedecoder.cpp \
    $$PWD/Multimedia/Image/skimageencoder.cpp \
    $$PWD/Multimedia/Image/skimageutils.cpp \
    $$PWD/Multimedia/Image/skimagegradient.cpp \
    $$PWD/Multimedia/Image/skwhitebalancer.cpp

