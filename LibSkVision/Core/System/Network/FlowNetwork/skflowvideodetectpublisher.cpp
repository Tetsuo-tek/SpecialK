#include "skflowvideodetectpublisher.h"

#if defined(ENABLE_CV)

#include "Core/System/Filesystem/skfsutils.h"

ConstructorImpl(SkFlowVideoDetectPublisher, SkAbstractFlowPublisher)
{
    boxesPublisher = nullptr;
    framePublisher = nullptr;
    compression = 70;
    detectionIntervalMS = 50;
}

bool SkFlowVideoDetectPublisher::setup(SkMat *frame, Size resolution, SkString cascadePath, Size min, Size max, CStr *chNamePfx)
{
    f = frame;
    res = resolution;
    cascade = cascadePath;
    areaMin = min;
    areaMax = max;

    if (!SkString::isEmpty(chNamePfx))
        chNamePrefix = chNamePfx;

    if (!SkFsUtils::exists(cascade.c_str())
        || !SkFsUtils::isFile(cascade.c_str())
        || !SkFsUtils::isReadable(cascade.c_str()))
    {
        ObjectError("Cascade-model file NOT valid: " << cascade);
        return false;
    }

    if (!detector.setup(&f.get(), cascade.c_str(), areaMin, areaMax))
        return false;

    return true;
}

bool SkFlowVideoDetectPublisher::start()
{
    SkString chName = chNamePrefix;
    chName.append(".");
    chName.append("DetectionBoxes");

    boxesPublisher = DynCast(SkFlowGenericPublisher, createPublisher(new SkFlowGenericPublisher(this)));
    boxesPublisher->setup(chName.c_str(), FT_CV_OBJECT_DETECTED_BOX, T_STRING, "text/json", &props);
    boxesPublisher->start();

    framePublisher = DynCast(SkFlowVideoMJpegPublisher, createPublisher(new SkFlowVideoMJpegPublisher(this)));
    framePublisher->setup(f, res, 0, compression, chNamePrefix.c_str());
    framePublisher->start();

    return true;
}

void SkFlowVideoDetectPublisher::stop()
{
    boxesPublisher->stop();
    boxesPublisher->destroyLater();
    boxesPublisher = nullptr;

    framePublisher->stop();
    framePublisher->destroyLater();
    framePublisher = nullptr;
}

void SkFlowVideoDetectPublisher::tick()
{
    if (!hasTargets())
        return;

    detector.tick();

    if (detector.isEmpty())
        return;

    vector<Rect> &detectedCanvas = detector.getDetectedCanvases();
    SkVariantList boxes;

    for(uint64_t i=0; i<detectedCanvas.size(); i++)
    {
        Rect &boundingBox = detectedCanvas[i];

        SkArgsMap m;
        m["x"] = boundingBox.x;
        m["y"] = boundingBox.y;
        m["w"] = boundingBox.width;
        m["h"] = boundingBox.height;

        boxes << m;
    }

    if (framePublisher->hasTargets())
        framePublisher->tick();

    if (boxesPublisher->hasTargets())
    {
        SkString json;
        SkVariant v(boxes);
        v.toJson(json);

        boxesPublisher->tick(json.c_str(), json.size());
    }
}

bool SkFlowVideoDetectPublisher::hasTargets()
{
    return boxesPublisher->hasTargets() || framePublisher->hasTargets();
}

#endif
