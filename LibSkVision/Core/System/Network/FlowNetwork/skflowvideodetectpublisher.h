#ifndef SKFLOWVIDEODETECTPUBLISHER_H
#define SKFLOWVIDEODETECTPUBLISHER_H

#if defined(ENABLE_CV)

#include "Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.h"
#include "Core/System/Time/skelapsedtime.h"

#include <Multimedia/Image/skmat.h>
#include <Multimedia/Image/Detection/skobjectdetect.h>

class SkFlowVideoDetectPublisher extends SkAbstractFlowPublisher
{
    Size res;
    int compression;

    SkString chNamePrefix;

    SkFlowGenericPublisher *boxesPublisher;
    SkFlowVideoMJpegPublisher *framePublisher;

    SkString cascade;

    Size areaMin;
    Size areaMax;

    SkObjectDetect detector;

    SkElapsedTimeMillis chrono;
    UInt detectionIntervalMS;

    public:
        Constructor(SkFlowVideoDetectPublisher, SkAbstractFlowPublisher);

        bool setup(SkMat *frame, Size resolution, SkString cascadePath, Size min, Size max, CStr *chNamePfx="");

        bool start()                                                    override;
        void stop()                                                     override;

        void tick();

        bool hasTargets()                                               override;
};

#endif

#endif // SKFLOWVIDEODETECTPUBLISHER_H
