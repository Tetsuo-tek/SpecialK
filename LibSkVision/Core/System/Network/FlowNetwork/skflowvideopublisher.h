#ifndef SKFLOWVIDEOPUBLISHER_H
#define SKFLOWVIDEOPUBLISHER_H

#if defined(ENABLE_CV)

/*#include "Core/System/Network/FlowNetwork/skabstractflowpublisher.h"

#include <Multimedia/Image/skimagecapture.h>
#include <Multimedia/Image/skimageencoder.h>
#include <Multimedia/Image/skmat.h>
#include "Multimedia/Magma/skmagmawriter.h"
#include "Multimedia/Image/Encoders/Magma/skpsyvenc.h"*/

#include <Multimedia/Image/skwhitebalancer.h>

#include "skflowvideomjpegpublisher.h"
#include "skflowvideomagmapublisher.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowVideoPublisher extends SkAbstractFlowPublisher
{
    SkMat *f;

    Size res;
    int fps;
    ULong frameRawSize;

    SkString chNamePrefix;

    bool flipModeEnabled;
    SkImageFlippingMode flipMode;

    bool wbEnabled;
    SkWhiteBalancer wb;

    bool grayScale;

    SkFlowVideoMJpegPublisher *mjpegPublisher;
    SkFlowVideoMagmaPublisher *magmaPublisher;

    int compression;
    bool preview;

    public:
        Constructor(SkFlowVideoPublisher, SkAbstractFlowPublisher);

        void setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx="", bool isPreview=false);

        bool start()                                                    override;
        void stop()                                                     override;

        void setFlip(SkImageFlippingMode mode);

        void setMonochromatic(bool enable);

        void enableWhiteBalancer(SkWbAlgorithm a);
        void disableWhiteBalancer();

        void tick();

        bool isWhiteBalancerEnabled();
        SkWbAlgorithm getWhiteBalancerAlgo();
        bool isMonochromaticEnabled();
        SkImageFlippingMode getFlipMode();

        bool hasTargets()                                               override;

        ULong getRawFrameSize();

        SkFlowVideoMJpegPublisher *getMjpegPublisher();
        SkFlowVideoMagmaPublisher *getMagmaPublisher();
};

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLOWVIDEOPUBLISHER_H
