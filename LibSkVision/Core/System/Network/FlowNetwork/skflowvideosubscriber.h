#ifndef SKFLOWVIDEOSUBSCRIBER_H
#define SKFLOWVIDEOSUBSCRIBER_H

#include "Core/System/Network/FlowNetwork/skabstractflowsubscriber.h"

#include <Multimedia/Image/skimagedecoder.h>
#include <Multimedia/Image/skmat.h>
#include <Multimedia/Magma/skmagmareader.h>
#include <Multimedia/Image/Encoders/Magma/skpsyvdec.h>

#if defined(ENABLE_FLTK)
    #include <UI/FLTK/skfltkwidgetwrapper.h>
#endif

class SkMagmaReader;
class SkPsyVDec;

class SkFlowVideoSubscriber extends SkAbstractFlowSubscriber
{
    bool firstTick;

#if defined(ENABLE_FLTK)
    SkFltkImageBox *videoWidget;
    SkSize lastSize;
#endif

    SkMat *f;

    SkImageDecoder jpegDec;

    SkMagmaReader *mgm;
    SkPsyVDec *vDec;

    Size res;
    int fps;
    SkString streamFMT;
    SkString streamCDC;

    public:
        Constructor(SkFlowVideoSubscriber, SkAbstractFlowSubscriber);

        void setup(SkMat *frame);

#if defined(ENABLE_FLTK)
        void setup(SkFltkImageBox *w);
#endif

        void subscribe(CStr *videoChanName)                             override;
        void unsubscribe()                                              override;

        void close()                                                    override;

        Size resolution();
        int framesPerSecond();
        float tickTimePeriod();
        CStr *stream();
        CStr *codec();

    private:
        void reset();

        void onChannelAdded(SkFlowChanID chanID)                        override;
        void onChannelRemoved(SkFlowChanID chanID)                      override;

        void tick(SkFlowChannelData &chData)                            override;
};

#endif // SKFLOWVIDEOSUBSCRIBER_H
