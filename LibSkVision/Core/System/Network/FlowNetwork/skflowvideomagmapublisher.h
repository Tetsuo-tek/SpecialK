#ifndef SKFLOWVIDEOMAGMAPUBLISHER_H
#define SKFLOWVIDEOMAGMAPUBLISHER_H

#if defined(ENABLE_CV)

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include <Multimedia/Image/skmat.h>
#include "Multimedia/Magma/skmagmawriter.h"
#include "Multimedia/Image/Encoders/Magma/skpsyvenc.h"

class SkFlowVideoMagmaPublisher extends SkFlowGenericPublisher
{
    SkMat *f;
    int compression;
    Size res;
    int fps;
    bool preview;
    ULong frameRawSize;

    SkMagmaWriter *mgm;
    SkPsyVEnc *vEnc;
    SkDataBuffer hdr;

    public:
        Constructor(SkFlowVideoMagmaPublisher, SkFlowGenericPublisher);
        bool setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx="", bool isPreview=false);

        void tick();

    private:
        void onStart()                                                  override;
        void onStop()                                                   override;
        void onChannelReady()                                           override;
};

#endif

#endif // SKFLOWVIDEOMAGMAPUBLISHER_H
