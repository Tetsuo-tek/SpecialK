#include "skflowvideopublisher.h"

#if defined(ENABLE_CV)
#include "Core/App/skeventloop.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h"
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowVideoPublisher, SkAbstractFlowPublisher)
{
    f = nullptr;

    mjpegPublisher = nullptr;
    magmaPublisher = nullptr;

    preview = false;
    fps = 0;
    frameRawSize = 0;

    flipModeEnabled = false;
    flipMode = SK_NONE_FLIPPING;

    wbEnabled = false;
    grayScale = false;

    compression = 70;

    setObjectName("VideoMgmPublisher");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowVideoPublisher::setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx, bool isPreview)
{
    f = frame;
    preview = isPreview;
    res = resolution;
    fps = framesPerSecond;
    compression = jpegCompression;

    if (!SkString::isEmpty(chNamePfx))
        chNamePrefix = chNamePfx;

    frameRawSize = static_cast<ULong>(res.width * res.height * 3);

    mjpegPublisher = DynCast(SkFlowVideoMJpegPublisher, createPublisher(new SkFlowVideoMJpegPublisher(this)));
    mjpegPublisher->setup(f, res, fps, compression, chNamePrefix.c_str(), preview);

    magmaPublisher = DynCast(SkFlowVideoMagmaPublisher, createPublisher(new SkFlowVideoMagmaPublisher(this)));
    magmaPublisher->setup(f, res, fps, compression, chNamePrefix.c_str(), preview);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowVideoPublisher::start()
{
    mjpegPublisher->start();
    magmaPublisher->start();

    return true;
}

void SkFlowVideoPublisher::stop()
{
    mjpegPublisher->stop();
    mjpegPublisher->destroyLater();
    mjpegPublisher = nullptr;

    magmaPublisher->stop();
    magmaPublisher->destroyLater();
    magmaPublisher = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowVideoPublisher::setFlip(SkImageFlippingMode mode)
{
    flipMode = mode;
    flipModeEnabled = (flipMode != SK_NONE_FLIPPING);
}

void SkFlowVideoPublisher::setMonochromatic(bool enable)
{
    grayScale = enable;
}

void SkFlowVideoPublisher::enableWhiteBalancer(SkWbAlgorithm a)
{
    wb.setup(&f->get(), &f->get(), a);
    wbEnabled = true;
}

void SkFlowVideoPublisher::disableWhiteBalancer()
{
    wbEnabled = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowVideoPublisher::tick()
{
    if (f->isEmpty() || !hasTargets())
        return;

    if (grayScale)
        f->toGrayScale();

    if (wbEnabled)
        wb.tick();

    if (flipModeEnabled)
        f->flip(flipMode);

    if (f->size() != res)
        f->resize(res);

    if (mjpegPublisher->hasTargets())
        mjpegPublisher->tick();

    if (magmaPublisher->hasTargets())
        magmaPublisher->tick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowVideoPublisher::isWhiteBalancerEnabled()
{
    return wbEnabled;
}

SkWbAlgorithm SkFlowVideoPublisher::getWhiteBalancerAlgo()
{
    return wb.algorithm();
}

bool SkFlowVideoPublisher::isMonochromaticEnabled()
{
    return grayScale;
}

SkImageFlippingMode SkFlowVideoPublisher::getFlipMode()
{
    return flipMode;
}

bool SkFlowVideoPublisher::hasTargets()
{
    return mjpegPublisher->hasTargets() || magmaPublisher->hasTargets();
}

ULong SkFlowVideoPublisher::getRawFrameSize()
{
    return frameRawSize;
}

SkFlowVideoMJpegPublisher *SkFlowVideoPublisher::getMjpegPublisher()
{
    return mjpegPublisher;
}

SkFlowVideoMagmaPublisher *SkFlowVideoPublisher::getMagmaPublisher()
{
    return magmaPublisher;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
