#include "skflowvideosubscriber.h"
#include "Core/Containers/skarraycast.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkFlowVideoSubscriber, SkAbstractFlowSubscriber)
{
    f = nullptr;

    mgm = nullptr;
    vDec = nullptr;

    fps = 0;

#if defined(ENABLE_FLTK)
    videoWidget = nullptr;
#endif
}

void SkFlowVideoSubscriber::setup(SkMat *frame)
{
    f = frame;
    jpegDec.setup(&f->get());
}

#if defined(ENABLE_FLTK)
void SkFlowVideoSubscriber::setup(SkFltkImageBox *w)
{
    videoWidget = w;
}
#endif

void SkFlowVideoSubscriber::subscribe(CStr *videoChanName)
{
    setChannelName(videoChanName);
}

void SkFlowVideoSubscriber::unsubscribe()
{
    /*if (!isReady())
        return;*/

    async->unsubscribeChannel(source());
    reset();
}

void SkFlowVideoSubscriber::tick(SkFlowChannelData &chData)
{
    if (!isReady() || chData.data.isEmpty())
        return;

    if (chData.chanID == source()->chanID)
    {
        if (firstTick)
        {
            firstTick = false;
#if defined(ENABLE_FLTK)
            lastSize = videoWidget->size();
#endif
        }

        if (mgm)
        {
            if (!mgm->isOpen() || !vDec || !vDec->isEnabled())
                return;

            int streamID = mgm->tick(chData.data.toVoid(), chData.data.size());

            if (streamID == -1)
                return;

            if (mgm->streams()[streamID] == vDec)
            {
                if (!vDec->frame_BGR().empty())
                {
                    Mat fr;
                    vDec->frame_BGR().copyTo(fr);

                    while(!vDec->blockRegions().isEmpty())
                        rectangle(fr, vDec->blockRegions().dequeue(), Scalar(0, 100, 100));

                    if (f)
                        f->set(fr);

#if defined(ENABLE_FLTK)
                    else if (videoWidget)
                    {
                        SkSize currentSize = videoWidget->size();
                        bool unChanged = (lastSize.w == currentSize.w && lastSize.h == currentSize.h);

                        if (unChanged)
                            videoWidget->setImage(fr);

                        else
                            lastSize = currentSize;
                    }
#endif
                }
            }
        }

        else
        {
            if (f)
                jpegDec.loadFromBuffer(chData.data.data(), chData.data.size());

#if defined(ENABLE_FLTK)
            if (videoWidget)
            {
                SkSize currentSize = videoWidget->size();
                bool unChanged = (lastSize.w == currentSize.w && lastSize.h == currentSize.h);

                if (unChanged)
                    videoWidget->setImage(SkArrayCast::toUInt8(chData.data.toVoid()));

                else
                    lastSize = currentSize;
            }
#endif
        }
    }
}

void SkFlowVideoSubscriber::onChannelAdded(SkFlowChanID chanID)
{
    if (isReady())
        return;

    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (SkString::compare(ch->name.c_str(), chanName()))
    {
        AssertKiller(ch->flow_t != FT_VIDEO_DATA
                     && ch->flow_t != FT_VIDEO_PREVIEW_DATA
                     && ch->flow_t != FT_CV_OBJECT_DETECTED_FRAME);

        ObjectMessage("Source video-channel is READY");

        //  //  //  //  //
        SkFlowSync *sync = async->buildSyncClient();

        AssertKiller(!sync->existsOptionalPairDb(ch->name.c_str()));
        AssertKiller(!sync->setCurrentDbName(ch->name.c_str()));

        SkVariant v;

        SkString resolution;

        if (sync->getVariable("resolution", v))
        {
            resolution = v.toString();
            parseSize(objectName(), resolution.c_str(), &res);
        }

        if (sync->getVariable("fps", v))
            fps = v.toInt();

        if (sync->getVariable("stream", v))
            streamFMT = v.toString();

        if (sync->getVariable("codec", v))
            streamCDC = v.toString();

        if (streamFMT == "MULTIPART")
            AssertKiller(streamCDC != "JPEG");

        else if (streamFMT == "MAGMA")
        {
            AssertKiller(streamCDC != "PSYV");

            mgm = new SkMagmaReader(this);
            mgm->setObjectName(this, "Decoder");

            AssertKiller(!ch->hasHeader || !mgm->open(ch->header) || !mgm->count());
            vDec = dynamic_cast<SkPsyVDec *>(mgm->streams()[0]);
            AssertKiller(!vDec);
        }

        else
            AssertKiller(streamFMT != "MULTIPART" && streamFMT!= "MAGMA");

        //ulong tickTimeMicros = props["tickTimePeriod"].toFloat()*1000000;

        ulong tickIntervalUS = 0;

        if (sync->getVariable("tickTimePeriod", v))
            tickIntervalUS = v.toFloat()*1000000;

        if (tickIntervalUS)
        {
            if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
            {
                ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
                eventLoop()->changeFastZone(tickIntervalUS);
                eventLoop()->changeSlowZone(tickIntervalUS*4);
                eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
            }
        }

        /*if (eventLoop()->getFastInterval() > tickTimeMicros)
            eventLoop()->changeFastZone(tickTimeMicros);*/

        sync->close();
        sync->destroyLater();

        setSource(ch);
    }
}

void SkFlowVideoSubscriber::onChannelRemoved(SkFlowChanID chanID)
{
    if (!isReady())
        return;

    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (ch->chanID == source()->chanID)
        reset();
}

void SkFlowVideoSubscriber::close()
{
    reset();
}

void SkFlowVideoSubscriber::reset()
{
    firstTick = true;

    SkAbstractFlowSubscriber::reset();

    if (mgm)
    {
        if (mgm->isOpen())
            mgm->close();

        mgm->destroyLater();
    }

    if (f)
        f->clear();

    f = nullptr;

    mgm = nullptr;
    vDec = nullptr;

#if defined(ENABLE_FLTK)
    videoWidget = nullptr;
    lastSize.h = 0;
    lastSize.w = 0;
#endif
}

Size SkFlowVideoSubscriber::resolution()
{
    return res;
}

int SkFlowVideoSubscriber::framesPerSecond()
{
    return fps;
}

float SkFlowVideoSubscriber::tickTimePeriod()
{
    if (fps)
        return (1./fps);

    return -1;
}

CStr *SkFlowVideoSubscriber::stream()
{
    return streamFMT.c_str();
}

CStr *SkFlowVideoSubscriber::codec()
{
    return streamCDC.c_str();
}
