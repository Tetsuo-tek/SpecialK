#include "skflowvideomjpegpublisher.h"

#if defined(ENABLE_CV)
#include "Core/App/skeventloop.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h"
#endif

ConstructorImpl(SkFlowVideoMJpegPublisher, SkFlowGenericPublisher)
{
    f = nullptr;

    preview = false;
    fps = 0;
    frameRawSize = 0;

    setObjectName("MJpegPublisher");
}

bool SkFlowVideoMJpegPublisher::setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx, bool isPreview)
{
    f = frame;
    preview = isPreview;
    res = resolution;
    fps = framesPerSecond;
    frameRawSize = static_cast<uint64_t>(res.width * res.height * 3);
    compression = jpegCompression;
    enc.setup(SkEncoderFormat::SK_JPEG_ENC, compression);

    SkString chName;

    if (!SkString::isEmpty(chNamePfx))
    {
        chName = chNamePfx;
        chName.append(".");
    }

    chName.append("JPeg");

    if (preview)
        return SkFlowGenericPublisher::setup(chName.c_str(), FT_VIDEO_PREVIEW_DATA, T_BYTEARRAY, "image/jpeg", nullptr);

    return SkFlowGenericPublisher::setup(chName.c_str(), FT_VIDEO_DATA, T_BYTEARRAY, "image/jpeg", nullptr);
}

void SkFlowVideoMJpegPublisher::onStart()
{
    if (fps > 0)
    {
        ulong tickIntervalUS = static_cast<ulong>((1./fps) * 1000000.);

        if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
        {
            ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(tickIntervalUS*4);
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        }

        /*if (tickIntervalUS < eventLoop()->getFastInterval())
        {
            ObjectWarning("Setting required tick mode");

            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(eventLoop()->getFastInterval()*4);

            if (eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT && eventLoop()->getTimerMode() != SK_FREELOOP)
                eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        }*/
    }
}

void SkFlowVideoMJpegPublisher::tick()
{
    if (!hasTargets())
        return;

    enc.saveToBuffer(f->get(), encodedBuffer);

    if (SkAbstractFlowPublisher::hasTargets(channel))
        async->publish(channelID, encodedBuffer.data(), encodedBuffer.size());

#if defined(ENABLE_HTTP)
    if (SkAbstractFlowPublisher::hasTargets(mp))
        mp->send(SkArrayCast::toCStr(encodedBuffer.data()), encodedBuffer.size());
#endif
}

void SkFlowVideoMJpegPublisher::onChannelReady()
{
    SkArgsMap props;
    SkString resolution(res.width);
    resolution.append("x");
    resolution.concat(res.height);

    props["resolution"] = resolution;
    props["fps"] = fps;
    props["frameRawSize"] = frameRawSize;

    if (fps)
        props["tickTimePeriod"] = (1./fps);

    else
        props["tickTimePeriod"] = -1;

    props["stream"] = "MULTIPART";
    props["codec"] = "JPEG";

    async->setChannelProperties(channelID, props);
}

#endif
