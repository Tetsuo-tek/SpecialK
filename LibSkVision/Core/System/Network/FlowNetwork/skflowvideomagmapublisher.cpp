#include "skflowvideomagmapublisher.h"
#include "Core/App/skeventloop.h"

#if defined(ENABLE_CV)

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h"
#endif

ConstructorImpl(SkFlowVideoMagmaPublisher, SkFlowGenericPublisher)
{
    f = nullptr;
    mgm = nullptr;
    vEnc = nullptr;

    preview = false;
    fps = 0;
    frameRawSize = 0;
    compression = 0;
}

bool SkFlowVideoMagmaPublisher::setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx, bool isPreview)
{
    f = frame;
    preview = isPreview;
    res = resolution;
    fps = framesPerSecond;
    frameRawSize = static_cast<ULong>(res.width * res.height * 3);

    compression = jpegCompression;

    SkString chName;

    if (!SkString::isEmpty(chNamePfx))
    {
        chName = chNamePfx;
        chName.append(".");
    }

    chName.append("MGM");

    if (preview)
        return SkFlowGenericPublisher::setup(chName.c_str(), FT_VIDEO_PREVIEW_DATA, T_BYTEARRAY, nullptr, nullptr);

    return SkFlowGenericPublisher::setup(chName.c_str(), FT_VIDEO_DATA, T_BYTEARRAY, nullptr, nullptr);
}

void SkFlowVideoMagmaPublisher::onStart()
{
    mgm = new SkMagmaWriter(this);
    mgm->setObjectName(this, "Mgm");
    vEnc = new SkPsyVEnc(mgm);
    vEnc->setObjectName(this, "PsyVEnc");
    vEnc->setup(res, fps, YuvCodec, Yuv411, 8, compression/100.f);
    mgm->addStream(vEnc);

    if (mgm->open(hdr))
    {
        if (!hdr.isEmpty())
        {
            ObjectMessage("MGM-writer HEADER produced: " << hdr.size() << " B");
        }

        else
            ObjectError("MGM header NOT produced!");
    }

    else
        ObjectError("CANNOT open MGM-writer!");

    if (fps > 0)
    {
        ulong tickIntervalUS = static_cast<ulong>((1./fps) * 1000000.);

        if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
        {
            ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(tickIntervalUS*4);
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        }

        /*if (tickIntervalUS < eventLoop()->getFastInterval())
        {
            ObjectWarning("Setting required tick mode");

            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(eventLoop()->getFastInterval()*4);

            if (eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT && eventLoop()->getTimerMode() != SK_FREELOOP)
                eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        }*/
    }
}

void SkFlowVideoMagmaPublisher::onStop()
{
    if (mgm)
    {
        if (mgm->isOpen())
            mgm->close();

        mgm->destroyLater();
        mgm = nullptr;
        vEnc = nullptr;
    }
}

void SkFlowVideoMagmaPublisher::tick()
{
    if (!hasTargets())
        return;

    SkDataBuffer output;

    vEnc->setData(f->get().data);
    mgm->tick(output);

    if (!output.isEmpty())
    {
        if (SkAbstractFlowPublisher::hasTargets(channel))
            async->publish(channelID, output.toVoid(), output.size());

#if defined(ENABLE_HTTP)
        if (SkAbstractFlowPublisher::hasTargets(mp))
            mp->send(output.data(), output.size());
#endif
    }
}

void SkFlowVideoMagmaPublisher::onChannelReady()
{
    SkArgsMap props;
    SkString resolution(res.width);
    resolution.append("x");
    resolution.concat(res.height);

    props["resolution"] = resolution;
    props["fps"] = fps;
    props["frameRawSize"] = frameRawSize;

    if (fps)
        props["tickTimePeriod"] = (1./fps);

    else
        props["tickTimePeriod"] = -1;

    props["stream"] = "MAGMA";
    props["codec"] = "PSYV";

    async->setChannelProperties(channelID, props);
    async->setChannelHeader(channelID, hdr);
}

#endif
