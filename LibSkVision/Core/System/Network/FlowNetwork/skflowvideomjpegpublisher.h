#ifndef SKFLOWVIDEOMJPEGPUBLISHER_H
#define SKFLOWVIDEOMJPEGPUBLISHER_H

#if defined(ENABLE_CV)

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include <Multimedia/Image/skmat.h>
#include <Multimedia/Image/skimageencoder.h>

class SkFlowVideoMJpegPublisher extends SkFlowGenericPublisher
{
    SkMat *f;
    Size res;
    int fps;
    bool preview;
    ULong frameRawSize;

    SkImageEncoder enc;
    int compression;
    vector<uchar> encodedBuffer;

    public:
        Constructor(SkFlowVideoMJpegPublisher, SkFlowGenericPublisher);

        bool setup(SkMat *frame, Size resolution, int framesPerSecond, int jpegCompression, CStr *chNamePfx="", bool isPreview=false);

        void tick();

    private:
        void onStart()                                                  override;
        void onChannelReady()                                           override;
};

#endif

#endif // SKFLOWVIDEOMJPEGPUBLISHER_H
