TARGET = SkVision
TEMPLATE = lib

# insert sources

include(LibSkVision.pri)
include(module-src.pri)

DEFINES += SKVIDEO_LIBRARY

# include libSkCore

LIBS += -lSkCore
INCLUDEPATH += /usr/local/include/LibSkCore
include(/usr/local/include/LibSkCore/LibSkCore.pri)

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkVision/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkWeb.pri

headersDataFiles.path = /usr/local/include/LibSkVision/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles

