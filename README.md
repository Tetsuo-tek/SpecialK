# SpecialK

SpecialK (aka Sk) is a framework written in C++ that is fully compatible with the STL (Standard Template Libraries). SpecialK is an highly experimental project oriented towards: robotics, IoT, IIoT and domotics.

The framework has minimal dependencies on external libraries. The only significant and optional ones to consider are: OpenCV-4, PortAudio, FFTW3, OggVorbis, and FLTK, if GUI (Graphic User Interface) support is used. With compilation macros, it is possible to enable or disable all portions of code that make up the framework, and thus the associated dependencies as well.

Sk essentially imposes the following paradigms:

* asynchronous and recursive destruction of objects;
* Signal/Slot interactions between different and potentially unknown objects;
* events and tick manager whose type and frequency can be regulated.

These simple features allow for the development of very lightweight and precise time management applications, if required. In practice, it is almost never necessary to use mutexes or semaphores for communication between objects placed on different threads and/or processes that share data. The optional threads of an application can always collaborate asynchronously and without ever requesting locks from the process, much less wait-conditions. To make these statements true, it is necessary to follow the paradigms proposed in Sk. All of this can make this type of concurrent programming very efficient and less complicated.

Depending on the specific flow within the single Sk process, it is often possible to completely avoid the use of concurrent multi-thread programming. This possibility essentially stems from the Signal/Slot paradigm behaviour.

Since Sk is under continuous rolling evolution, it is preferable to include the framework artifacts directly in the program with the methods proposed also in the appendix; this significantly increases the initial compilation times but allows you to keep changes that develop in the underlying framework under control. For these reasons, the use of the framework in the form of a shared library is currently discouraged, although possible.

SpecialK is part of the [pck](https://gitlab.com/Tetsuo-tek/pck) base ecosystem.

## Getting started with a simple pulsing application

The `main.cpp` file where the backend activity of Sk is prepared.

```cpp
#include "apptest.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    skApp->init(5000, 70000, SkLoopTimerMode::SK_TIMEDLOOP_RT);

    AppTest *a = new AppTest;
    Attach(skApp->started_SIG, pulse, a, init, SkAttachMode::SkOneShotDirect);

    return skApp->exec();
}
```

An instance of `SkApp` is created, which corresponds to the tick manager of the main thread. The pointer to the command line parameters of the program is passed for the allocation of the object:

```cpp
skApp = new SkApp(argc, argv);
```

Only one instance of this object exists in the entire application because it represents the core of the application itself. Derived from `SkEventLoop`, `SkApp` adds public and private methods useful for managing the entire application and all instantiated objects. In `SkApp`, it is also possible to insert references of pointers and variables existing somewhere in the program to be shared with other parts unknown to the former; these represent global objects and data that all parts of the application can use as references for data and functionality.

The global pointer must necessarily be provided, otherwise, a compilation error will occur:

```cpp
SkApp *skApp = nullptr;
```

The reason for the error is that `skApp` is a pointer declared as `extern`, which must be declared within applications that use Sk.

Next is the timer setup:

```cpp
skApp->init(5000, 70000, SkLoopTimerMode::SK_TIMEDLOOP_RT);
```

The first argument (5000) indicates the FastTick interval in μs, therefore 5 ms or 0.005 s. The second argument (70000) indicates the SlowTick interval in μs, therefore 70 ms or 0.07 s. The third parameter indicates the type of tick to apply, which here indicates the use of pseudo-real-time type.

Then the main class for the program, `AppTest`, is initialized:

```cpp
AppTest *a = new AppTest;
Attach(skApp->started_SIG, pulse, a, init, SkAttachMode::SkOneShotDirect);
```

After creating the instance, the init Slot of the `AppTest` program is attached (`Attach`) to the ready state of `SkApp`; capturing this state is necessary to start the main class activities when all backend portions are ready.

As explained below, since it is not derived from `SkObject`, `SkApp` cannot be equipped with real Signals. `SkApp` is derived from `SkEventLoop`, which is derived from `SkFlatObject`, so it cannot incorporate a normal Signal construct.

To associate the ready state of `SkApp` with something similar to a Signal, the problem is solved by incorporating an instance of the type `SkStandaloneSignal` named `started_SIG` into the object. Through this class, the `pulse()` Signal present by default in the `SkObject` type (from which it is derived) is used as if it were a generic Signal that can exceptionally be positioned even in objects that theoretically could not have one.

To make the connection, the `skApp→started_SIG` object (a public property of the `SkApp` type) with its `pulse()` Signal must be attached to the target `AppTest` instance with its `init()` Slot. Note that using this start mode for the application is merely a formal matter, and there could be other ways to initialize the program that are not considered here. This mode, however, is the clearest and simplest.

Right after, the main application loop is entered through the `SkApp::exec()` method:

```cpp
return skApp->exec();
```

The header file for the `AppTest` class:

```cpp
#ifndef APPTEST_H
#define APPTEST_H

#include <Core/App/skapp.h>

class AppTest extends SkObject
{
    public:
        Constructor(AppTest, SkObject);

        Slot(init);
        Slot(tick);
        Slot(exit);
};

#endif // APPTEST_H
```

The implementation file for the `AppTest` class:

```cpp
#include "apptest.h"

ConstructorImpl(AppTest, SkObject)
{
    SlotSet(init);
    SlotSet(tick);
    SlotSet(exit);
}

SlotImpl(AppTest, init)
{
    SilentSlotArgsWarning();

    //THE skApp IS UP FROM HERE

    ObjectWarning("Object initialized! .. press CTRL-C to exit.");

    Attach(skApp->fastZone_SIG, pulse, this, tick, SkAttachMode::SkQueued);
    Attach(skApp->kernel_SIG, pulse, this, exit, SkAttachMode::SkOneShotQueued);

    ObjectMessage("SpecialK pseudo-real-time tick period");
}

SlotImpl(AppTest, tick)
{
    SilentSlotArgsWarning();

    cout << " -> "<< SkString::number(skApp->getLastPulseElapsedTime(), 9) << " s\r              ";
    cout.flush();
}

SlotImpl(AppTest, exit)
{
    SilentSlotArgsWarning();
    
    Int sig = Arg_Int;

    if (sig != SIGINT && sig != SIGTERM && sig != SIGQUIT)
        return;

    ObjectWarning("Forcing application exit with CTRL+C");
    skApp->quit();
}
```

The implementation of the class begins with that of the pseudo-constructor:

```cpp
ConstructorImpl(AppTest, SkObject)
```

The constructor macro, related to declaration and implementation, behaves as if it were unique but implements various constructors as well as performing backend operations to configure the object on the belonging manager and to set introspection and hierarchy values for the type.

Signals and Slots (in this case, only Slots are present) must always be initialized in the implementation of the pseudo-constructor:

```cpp
    SlotSet(init);
    SlotSet(tick);
    SlotSet(exit);
```

Signals, on the other hand, are initialized as follows:

```cpp
SignalSet(<SIGNAL_NAME>);
```

Initializing Signals and Slots in the pseudo-constructor is mandatory; if not done or if attempted elsewhere, problems will occur at runtime or during compilation.

Here is the implementation of the purpose of the Slot, in this case, `init`:

```cpp
SlotImpl(AppTest, init)
{
    (…)
}
```

This macro hides various structures and operations that occur at the origin of the program's activities before the `main(..)` function is actually executed and when the type is instantiated. The purpose contains the actual code to be executed following the invocation of the Slot.

In the Slot intended for the `init` phase, the most important lines are:

```cpp
Attach(skApp->fastZone_SIG, pulse, this, tick, SkAttachMode::SkQueued);
Attach(skApp->kernel_SIG, pulse, this, exit, SkAttachMode::SkQueued);
```

Here, we connect the two pseudo-signals `skApp→fastZone_SIG` and `skApp→kernel_SIG` of the `SkApp` to the respective Slots in the `AppTest` application. Without the first connection to get the fast tick, there would be no activity in the program; the application could only be terminated by pressing CTRL+C.

In the Slot related to pulsing `tick`, the average tick interval amount is simply displayed on a fixed text line for comparison with what was originally requested at startup.

Since the arguments of a Slot are hidden by the macro, they are not visible but they exist; if the arguments are not used within the purpose, a compiler warning is generated. For this reason, in each Slot, it is possible (though not necessary) to use the following macro to silence the messages:

```cpp
SilentSlotArgsWarning();
```

The retrieval of any arguments present in the triggering Signal can be captured with argument macros, as in the case of the `exit()` Slot where an integer value is retrieved:

```cpp
Int sig = Arg_Int;
```

In any case, it is necessary to know beforehand if the Signal carries arguments.

There are variations of this macro for every primitive type supported by the `SkVariant` class. If the Signal contains multiple arguments, they must be retrieved within the invoked Slot in the same order they were inserted into the Signal at the time of triggering. In this implementation macro for the Slot, where the arguments are variadic and structured during the source preprocessing phase.

Kernel signals are quite different from Sk Signals; in the application, the signal emitted by the kernel is converted into a trigger for a queued (asynchronous) Signal. Thanks to the connection with the `exit()` Slot, the kernel signal can be captured. If the emitted signal is the correct one, the application is set to close through the asynchronous call `SkApp::quit()`. If the Signal related to the signals retrieved from the kernel is not connected in the `init()` Slot, the launched application cannot close and must be terminated externally with:

```sh
kill -9 <pid>
```
Applications based on Sk are typically compiled using the support of [SkMake](https://gitlab.com/Tetsuo-tek/SkMake), which creates an appropriate Makefile and then compiles the application. It is not mandatory to use this tool, and it is possible to compile Sk-based applications using any standard method.

## Hierarchy and Inheritance

At the root of the derivative hierarchy of every object included in SpecialK, there is always a `SkFlatObject`. As the name suggests (flat object), it doesn't possess any specific functionality or characteristics except for the accessory one of allowing the setting of a name for the object instance. The formal task of the `SkFlatObject` type is to define a common and distinctive C++ standard base for all types included in Sk.

All data structures such as lists, vectors, maps, queues, stacks, buffers, and so on, are directly derived from `SkFlatObject`. An object of this type can be instantiated using the classic methods allowed by C++, i.e., on the stack directly with the declaration or on the heap through allocation with the `new` operator. They are generally convenient to use on the stack to avoid worrying about their destruction. In the case of heap allocation, resource release requires the use of the standard `delete` operator, leading to a normal synchronous destruction of the memory segment related to the object instance. An object directly derives from `SkFlatObject` when it does not need to use Signal/Slot or the asynchronous and recursive destruction system. Such derived types have no awareness of the `SkEventLoop` (event manager) running in the application, nor of the threads they operate on.

The `SkObject` type is also directly derived from `SkFlatObject`. It adds very particular characteristics to the subsequent hierarchy that go beyond the C++ standard but allow for a very efficient programming flow and, in some respects, a much simpler and ideal one. These characteristics are always implemented through more or less complex macros; the use of the C++ template paradigm remains relegated to simple uses dedicated to making the various flat containers present in Sk as multi-type. If a derivative of `SkObject` is allocated on the stack, thus without the use of the `new` operator, a runtime warning will be received because this is not a supported condition where the destruction of the object would be synchronous at the moment of the closing of the containing scope (function/method/class/struct scope). During their use, types derived from `SkObject` are potentially connected to the underlying ecosystem through Signal, Slot, and any parental relationships with other objects; these interactions are managed in the background through asynchronous methods.

The creation of an instance for a type derived from `SkObject` always and only occurs through the standard `new` operator, and its destruction is never synchronous through the `delete` operator: when you want to destroy an object, you can call its `destroyLater()` method, which will place the object in a temporary limbo (DeathReign) for two ticks of the event manager that governs it, before it is actually eliminated (Expiration) using the `delete` operator internally at the end of the process.

After calling the `destroyLater()` method, the object enters the `preparedToDie` state and still exists but is no longer active and usable; it will only terminate its relationships and ongoing activities before expiring.

If the `delete` operator is directly used on a derivative of `SkObject`, a runtime error will occur, almost certainly followed by a crash of the application.

By following the aforementioned legitimate way, the object will be destroyed as soon as possible during backend operations after all relationships with other objects and the ecosystem have been correctly terminated by the belonging event manager. It is important to note that this is not a garbage collector like those present in Java or Python, as the object is not automatically destroyed when the program flow is no longer in the declaration scope of the object's life. For efficiency reasons, there is no validity check of previously allocated pointers.

Only in two particular cases, the destruction of `SkObject` derivatives is automatic, but this does not involve any resource waste or additional checks to trigger, as these are steps that are part of the normal operational flow in the lifecycle of the `SkEventLoop`; the two situations are as follows:

* The object is set as a child of a parent object, and the parent is destroyed; this happens for all direct children but also recursively for all children of the children, following what can be defined as a destruction tree.
* The event manager under whose control the objects live terminates its activity; this happens when a thread is closed or, more simply, when the application is closed (the event manager of the main thread of the application closes, reproducing the situation in point 1).

Excluding these two examined cases, in all other situations, destruction is required with the `destroyLater()` method; otherwise, resources will not be released until the belonging `SkEventLoop` is closed.

## Event Manager with Pseudo-Real-Time Capabilities

The application developed on Sk (each of its threads) operates basing on tick pulsations; the tick is the atomic temporal event on which all activity is based. This method conceptually recalls the flow of an Arduino sketch where the program's activity depends on the regular and cyclic call of the `loop()` function. The inspiration is clear and is also fully reported in the Python sketches ([PySketch](https://gitlab.comn/Tetsuo-tek/PySketch)).

Each optional thread (`SkThread`) instantiated in the application (including the main application thread) is identifiable with the activity of an instance of the `SkEventLoop` type, which generates ticks in various configurable modes. On each optional thread, it is possible to set the mode and interval of ticks or use by default the speed and type parameters set on the main application thread.

Each loop manager cyclically emits 3 types of ticks with different speeds; the fastest, indivisible, and therefore atomic tick also marks the resolution in response time (lag) to external and internal solicitations:

* **FastTick** - the fastest tick provided by the manager, active or passive depending on the mode set, and represents the maximum processing speed in the thread where the manager lives;
* **SlowTick** - a passive tick with an interval >= the interval of FastTick;
* **OneSecTick** - a passive tick with an interval always equal to 1 second.

What I have defined as passive waiting for SlowTick and OneSecTick implies counting on the affected intervals using `SkElapsedTime`, a nanosecond-resolution chronometer. Instead, active waiting is the one that waits for a more or less precise interval, stopping the belonging thread with `usleep(..)` or `nanosec(...)`; this can only happen on FastTick relating to the selected tick mode.

Formally, the FastTick is used to give a computational processing cadence to the involved thread, while the SlowTick and OneSecTick are used to subject any monitoring, visualization, and control operations to a slower cycle. If not strictly necessary, it is not advisable to insert control operations in the fast tick as they could hinder all reception and responsiveness activities to events coming from the outside, for example from the network. In these cases, and when possible, control can be triggered in the appropriately configured SlowTick or even in the OneSecTick.

SlowTick and OneSecTick are always passive compared to FastTick, meaning they are chronometers on the fast pulsation time. The resolution (tolerance) of the SlowTick and OneSecTick trigger interval directly depending on the precision of the wait (timer type) and the size of the FastTick interval. In simple terms, the smaller and more regular the fast interval, the more precise the timing for soliciting SlowTick and OneSecTick. Conversely, if irregular modes are used, tolerance on SlowTick and OneSecTick can be more variable and non-deterministic.

The FastTick set in the regular modes listed below can assume values in the interval [0, 1000000] μs, i.e., between 0 and 1 second. Slower instantaneous ticks can only be achieved with irregular modes.

The different types of ticks and the interval width must always be evaluated based on the potential of the involved CPU, the processing times of the tick scope (job-time), as well as the purposes and requirements of the implementation in question.

When creating multiple threads, where some have the role of producer while others have the role of consumer, consumers must always tick at a speed >= that of the associated producer. If concurrent programming is performed and the exchange of shared data occurs through something like a queue, a stack or even a socket, this rule must always be strictly observed to avoid abnormal or inefficient situations such as uncontrolled growth of queues (or stacks) or deadlock conditions triggered through blocking sockets with a full buffer. If data sharing occurs through non-blocking ring structures such as the `SkRingBuffer` type container, the previously erroneous condition becomes correct and could be desired in this case by the developer, allowing fewer data acquisitions than produced with a representative sampling mode of the incoming data.

Another important rule to observe to ensure everything flows smoothly is that the job-time of the tick scope (the total computation time consumed following a tick call) must never be close to its upper limit, represented by the interval time set for FastTick; otherwise, the fast tick will be slowed down, and the application will have an average tick interval greater than desired, mainly depending on the excessive job-time.

The type of cadence for FastTick and, subsequently, for SlowTick and OneSecTick can follow different modes:

* **Coarse regular timing** - never equal to or below the required interval (therefore with time loss), used as default and very light as it calls `usleep()`, which puts the process in passive pause timed by the Kernel in this case;
* **Pseudo-real-time regular timing** - more than regular average time, slightly more CPU intensive as it calls `nanosec()`, which counts (in the process) nanoseconds based on elapsed machine cycles, therefore with the process always active in the time window assigned by the Kernel;
* **Irregular timing dictated by socket I/O activity** - CPU work can be very intensive if socket traffic is quite pronounced; in this case, the value set as the FastTick interval corresponds to the maximum wait time on the sockets to detect the presence of data (`select()`) and is an upper limit to the tick interval as it waits at most the proposed interval time for each active socket existing in the concerned thread;
* **Irregular timing dictated by GUI activity** - based on the FLTK library's event manager, always very light for the CPU (the GUI portion of Sk that allows graphical application development is not described in this document);
* **No timing** - requires a blocking call of any type within the tick scope, aimed at slowing down the latter as a blocking socket read connected without available data would do; if not slowed down, the generated loop is equivalent to a `while(1){..}`.

By configuring the tick types and intervals appropriately, developers can achieve the desired balance of performance and responsiveness suitable for their application's requirements.

## Signal/Slot Paradigm

The evolutionary precursor to this paradigm is associated with the well-known callback functions, conceptually derived from the C language and used in many other languages as well. These functions are used as parameters during initial setup sessions and then subsequently called by the backend upon specific events within the programming context in which they are subject. A simple case reproducible across all graphical interface frameworks is where pressing a button (widget) triggers a function associated with the button-click event during setup, allowing the program to respond to an external solicitation (mouse click event).

A **Signal** is a scopeless method consisting solely of a declaration in the header file; the **Slot**, on the other hand, appears almost like a normal method with its own scope and always returns a `void` type.

A Signal can be attached in various ways to one or more Slots of the same or different objects; Slots connected to a Signal can also belong to objects of different types. When the meta-connection between a Signal and a Slot is no longer necessary, they can be detached; temporally, one tick after detachment, calling the Signal will no longer invoke the Slot.

Signals and Slots must always be declared following the `public` access specifier because they must always be observable and controllable by the timing and event manager, including concerning the inheritance of derived types. An incorrect condition with protected/private Signals/Slots will manifest as a runtime error only at the time of the meta-connection request; this will be impossible to process because the constructs in question are not observale from the manager. For this reason, when deriving a type in Sk that might have Signals and Slots, the simple `extends` macro is used for convenience, eliminating the possibility of deriving in a protected/private manner. These are somewhat special methods incorporated into objects derived from the `SkObject` base type; this paradigm of interaction between object instances primarily allows synchronization of operations originating on even different types and possibly placed on different threads without needing mutual exclusion management through mutexes and wait-conditions.

Attach and Detach are asynchronous operations; in both cases, the operation is not performed immediately but is considered a commitment by the belonging loop manager to connect or disconnect the Signal and Slot as soon as possible (at the next tick). A consequence of asynchrony is that if the Signal is triggered immediately after the Attach line, the Slot, which is not yet effectively connected, will not be invoked; it is necessary to wait for the next tick for the connection to be active. For stable connections throughout the objects' lifetime, Attach usually occurs in the `ConstructorImpl(..)`, and Detach happens automatically when the owner object is destroyed.

A Signal is triggered like a normal method by simply calling it; if connections between the Signal and one or more Slots have been established, they will be invoked with the set mode. Triggering the Signal and invoking a Slot are conceptually distinct actions in Sk, where the former is always synchronous, and the latter is potentially asynchronous. The invoked Slots will execute the code in their scope. The connection mode between Signal and Slot can be of different types:

* **Direct** - Slots are invoked directly when the Signal is triggered as if calling the Slot method directly, executing the code in the thread where the code requesting the Signal trigger lives; it is important to note that the manual invocation of a Slot through the offered manager method is not direct and will always occur at the next tick asynchronously.
* **Queued** - Slots are queued for future execution in the loop manager at the next Slot invocation round and in the belonging thread flow, even when the Signal call comes from another manager, thus a different thread; triggering a Signal connected in queued mode is never blocking concerning the trigger call, even when Signal and Slot live in the same thread, as this still implies the asynchronous invocation of the connected Slot at the next tick when the triggering scope is already closed.
* **OneShot** (direct or queued) - Slots are invoked as just illustrated in the previous two points but only once, as facilitation; immediately after invocation, they are automatically disconnected.

A Signal can be simultaneously connected to multiple Slots in different modes but never more than once concurrently to the same.

Upon triggering, the Signal can carry a list of `SkVariant` type arguments, organized in a vector, which can be used by the invoked Slots; when the call is direct, the argument list contains pointers to the original list of values. If the call is queued, the Slots receive a copy of the arguments, avoiding critical section situations dependent on mutual exclusion.

As mentioned earlier, to avoid problematic uses of mutexes and wait-conditions when synchronized working is needed, it is good practice to use queued connections between Signals and Slots belonging to objects living in different threads, thereby avoiding probable undesirable deadlock situations or other operational inconsistencies, with a sure loss of efficiency mainly due to continuous micro-waits.

In conclusion, another important merit of this paradigm is to isolate the various operations on the various types (classes) involved in cooperation into compartments, without requiring mutual knowledge of the cooperating instances, while each remains influenced by the other through functional meta-connections.