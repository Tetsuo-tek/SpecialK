#ifndef SKFFTSPECTROGRAM_H
#define SKFFTSPECTROGRAM_H

#if defined(ENABLE_CV)

#include "Core/Containers/sklist.h"
#include "Core/System/Time/skelapsedtime.h"
#include "Multimedia/Image/skmat.h"
#include "Multimedia/Image/skimagegradient.h"

class SkFftSpectrogram extends SkFlatObject
{
    bool enabled;

    SkMat *f;

    uint bufferFrames;
    uint sampleRate;
    Size res;

    UInt fftSize;
    UInt fftRange;
    float freqInterval;

    int maximumFreqHz;
    int minimumFreqHz;
    int fromFreqId;
    int toFreqId;
    bool customRangeEnabled;

    SkImageGradient gradientBuilder;
    int gradient_t;

    Mat fftFrameRow;
    Mat spectralFrame;
    Mat spectralFrameRow;

    SkList<uchar *> fftValues;

    bool enableGridX;
    bool enableGridY;
    bool enableGridLabelX;
    bool enableGridLabelY;

    double scrollingIntervalSecs;
    SkElapsedTime chrono;

    public:
        SkFftSpectrogram();

        void setup(SkMat *frame, uint samples, uint rate, Size resolution);

        void enableGrid(bool enableX, bool enableY);
        void enableGridLabels(bool enableX, bool enableY);

        void changeColorMap(ColormapTypes gradient);

        void setCustomRange(UInt minHz, UInt maxHz);
        void enableCustomRange(bool enabled);

        void setScrollingInterval(UInt ms);

        void start();
        void stop();

        void tick(float *data);

        bool isEnabled();
};

#endif

#endif // SKFFTSPECTROGRAM_H
