#include "skdirectfft.h"
#include <Core/System/Thread/skmutexlocker.h>
#include <Core/sklogmachine.h>

#if defined(ENABLE_FFTW3LIB)

SkDirectFFT::SkDirectFFT()
{
    in = nullptr;
    bufferSamples = 0;
    freqInterval = 0.f;

    out = nullptr;
    fftSz = 0;

    db = nullptr;

    enableGaussianFilter = false;
}

SkDirectFFT::~SkDirectFFT()
{
    if (in)
    {
        fftw_destroy_plan(plan);
        fftw_free(in);
        fftw_free(out);
        delete [] db;
    }
}

void SkDirectFFT::setup(uint samples, uint rate, bool enableGaussianWindow, SkFftPlanMode m)
{
    bufferSamples = samples;
    fftSz = (bufferSamples/2) + 1;
    freqInterval = static_cast<float>((rate/2 + 1)/fftSz);
    enableGaussianFilter = enableGaussianWindow;

    if (in)
    {
        fftw_destroy_plan(plan);
        fftw_free(in);
        fftw_free(out);
        delete [] db;
    }

    in = static_cast<double *>(fftw_malloc(bufferSamples*sizeof(double)));
    out = static_cast<fftw_complex *>(fftw_malloc(fftSz*sizeof(fftw_complex)));
    db = new float [fftSz];

    uint flag = FFTW_ESTIMATE;

    if (m == SkFftPlanMode::AFFT_MEASURE)
        flag = FFTW_MEASURE;

    plan = fftw_plan_dft_r2c_1d(static_cast<int>(bufferSamples), in , out , flag);

    FlatMessage("Initialized -> "
                << "Fr: " << bufferSamples
                << "; Sr: " << rate
                << "; Interval[Hz]: " << freqInterval
                << "; count: " << fftSz);
}

void SkDirectFFT::calculate(double *buffer)
{
    //apply Hann Window to data to account for artifacts

    if (enableGaussianFilter)
    {
        for (uint64_t i=0; i<bufferSamples; i++)
        {
            in[i] = 0.5 * (1. - cos((2. * M_PI * static_cast<double>(i)) / (bufferSamples - 1)));
            in[i] *= buffer[i];
        }
    }

    else
        memcpy(in, buffer, bufferSamples*sizeof(double));
    /*{
        for (uint64_t i=0; i<bufferSamples; i++)
            in[i] = buffer[i];
    }*/

    fftw_execute(plan);

    for (uint64_t i=0 ; i<fftSz; i++)
    {
        double mag = sqrt((out[i][0]*out[i][0]) + (out[i][1]*out[i][1]));
        db[i] = 10. * log10(mag + 1.);
    }
}

uint64_t SkDirectFFT::count()
{
    return fftSz;
}

float *SkDirectFFT::getEnergies()
{
    return db;
}

float SkDirectFFT::getEnergy(uint64_t id)
{
    return db[id];
}

float SkDirectFFT::getFrequency(uint64_t id)
{
    return freqInterval*static_cast<float>(id);
}

float SkDirectFFT::getFrequencyInterval()
{
    return freqInterval;
}

#endif // SKAUDIOFFT_H
