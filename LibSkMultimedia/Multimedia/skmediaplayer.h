#ifndef SKMEDIAPLAYER_H
#define SKMEDIAPLAYER_H

#include "Core/Object/skobject.h"

#if defined(ENABLE_VLCLIBS)
#include <vlc/vlc.h>

class SkRingBuffer;

class SKMULTIMEDIA SkMediaPlayer extends SkObject
{
    public:
        Constructor(SkMediaPlayer, SkObject);

        void setMedia(CStr *m);
        CStr *getMedia();

        bool isPlaying();
        bool isSeekable();

        int getVolume();//0-100
        bool setVolume(int volume);//0-100

        int64_t getTime();//ms
        void setTime(int64_t time);//ms

        float getPosition();//0.0-1.0
        void setPosition(float pos);//0.0-1.0

        int64_t getLength();//ms

        bool takeSnapshot(CStr *outputFile, unsigned int width, unsigned int heigth);

        //bool hasVideo();
        bool setDrawableWidget(void *drawable);
        void getVideoSize(uint32_t *outWidth,  uint32_t *outHeight);
        void nextFrame();

        int getAudioTracksCount();
        int getVideoTracksCount();

        void setKeyInput(bool enabled);

        SkRingBuffer *getAudioProduction();

        Slot(play);
        Slot(pause);
        Slot(stop);

        Signal(started);
        Signal(paused);
        Signal(stopped);

        Signal(opening);
        Signal(buffering);//(float);
        Signal(playing);
        Signal(snapshotTaken);//(CStr *);
        Signal(timeChanged);//(int64_t );
        Signal(endReached);
        Signal(volumeChanged);//(int);
        Signal(positionChanged);//(float);
        Signal(lengthChanged);//(int64_t);
        Signal(seekableChanged);//(int)
        Signal(pausableChanged);//(int)
        Signal(titleChanged);//(int)
        Signal(forward);
        Signal(backward);
        Signal(corked);
        Signal(unCorked);
        Signal(muted);
        Signal(unMuted);
        Signal(audioDevice);//(CStr *);

        Signal(errorOccurred);

        //DO NOT CALL THIS METHs; IT IS USED INTERNALLY
        void cbcom(const libvlc_event_t *event);

    private:
        SkString media;

        libvlc_instance_t *inst;
        libvlc_media_player_t *mp;
        libvlc_media_t *m;
        libvlc_event_manager_t *em;

        SkRingBuffer *audioProduction;
};

#endif

#endif // SKMEDIAPLAYER_H
