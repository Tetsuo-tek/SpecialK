#include "skmediaplayer.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/Containers/skringbuffer.h"

#if defined(ENABLE_VLCLIBS)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void vlc_CB(const libvlc_event_t *event, void *opaque)
{
    reinterpret_cast<SkMediaPlayer*>(opaque)->cbcom(event);
}

/**
 * Callback prototype for audio playback.
 *
 * The LibVLC media player decodes and post-processes the audio signal
 * asynchronously (in an internal thread). Whenever audio samples are ready
 * to be queued to the output, this callback is invoked.
 *
 * The number of samples provided per invocation may depend on the file format,
 * the audio coding algorithm, the decoder plug-in, the post-processing
 * filters and timing. Application must not assume a certain number of samples.
 *
 * The exact format of audio samples is determined by libvlc_audio_set_format()
 * or libvlc_audio_set_format_callbacks() as is the channels layout.
 *
 * Note that the number of samples is per channel. For instance, if the audio
 * track sampling rate is 48000 Hz, then 1200 samples represent 25 milliseconds
 * of audio signal - regardless of the number of audio channels.
 *
 * \param data data pointer as passed to libvlc_audio_set_callbacks() [IN]
 * \param samples pointer to a table of audio samples to play back [IN]
 * \param count number of audio samples to play back
 * \param pts expected play time stamp (see libvlc_delay())
 */
void aPlay(void *opaque, CVoid *samples, unsigned count, int64_t /*pts*/)
{
    SkRingBuffer *b = reinterpret_cast<SkRingBuffer*>(opaque);
    b->addData(static_cast<CStr *>(samples), sizeof(float)*count);
}

/**
 * Callback prototype for audio pause.
 *
 * LibVLC invokes this callback to pause audio playback.
 *
 * \note The pause callback is never called if the audio is already paused.
 * \param data data pointer as passed to libvlc_audio_set_callbacks() [IN]
 * \param pts time stamp of the pause request (should be elapsed already)
 */
void aPause(void */*opaque*/, int64_t /*pts*/)
{
}

/**
 * Callback prototype for audio resumption.
 *
 * LibVLC invokes this callback to resume audio playback after it was
 * previously paused.
 *
 * \note The resume callback is never called if the audio is not paused.
 * \param data data pointer as passed to libvlc_audio_set_callbacks() [IN]
 * \param pts time stamp of the resumption request (should be elapsed already)
 */
void aResume(void */*opaque*/, int64_t /*pts*/)
{
}

/**
 * Callback prototype for audio buffer flush.
 *
 * LibVLC invokes this callback if it needs to discard all pending buffers and
 * stop playback as soon as possible. This typically occurs when the media is
 * stopped.
 *
 * \param data data pointer as passed to libvlc_audio_set_callbacks() [IN]
 */
void aFlush(void */*opaque*/, int64_t /*pts*/)
{
}

/**
 * Callback prototype for audio buffer drain.
 *
 * LibVLC may invoke this callback when the decoded audio track is ending.
 * There will be no further decoded samples for the track, but playback should
 * nevertheless continue until all already pending buffers are rendered.
 *
 * \param data data pointer as passed to libvlc_audio_set_callbacks() [IN]
 */
void aDrain(void */*opaque*/)
{
}

// //

/**
 * Sets a fixed decoded audio format.
 *
 * This only works in combination with libvlc_audio_set_callbacks(),
 * and is mutually exclusive with libvlc_audio_set_format_callbacks().
 *
 * \param mp the media player
 * \param format a four-characters string identifying the sample format
 *               (e.g. "S16N" or "f32l")
 * \param rate sample rate (expressed in Hz)
 * \param channels channels count
 * \version LibVLC 2.0.0 or later
 */
/*LIBVLC_API
void libvlc_audio_set_format( libvlc_media_player_t *mp, CStr *format,
                              unsigned rate, unsigned channels );*/

// //
/**
 * Sets decoded audio format via callbacks.
 *
 * This only works in combination with libvlc_audio_set_callbacks().
 *
 * \param mp the media player
 * \param setup callback to select the audio format (cannot be NULL)
 * \param cleanup callback to release any allocated resources (or NULL)
 * \version LibVLC 2.0.0 or later
 */
/*LIBVLC_API
void libvlc_audio_set_format_callbacks( libvlc_media_player_t *mp,
                                        libvlc_audio_setup_cb setup,
                                        libvlc_audio_cleanup_cb cleanup );*/
/**
 * Callback prototype to setup the audio playback.
 *
 * This is called when the media player needs to create a new audio output.
 * \param opaque pointer to the data pointer passed to
 *               libvlc_audio_set_callbacks() [IN/OUT]
 * \param format 4 bytes sample format [IN/OUT]
 * \param rate sample rate [IN/OUT]
 * \param channels channels count [IN/OUT]
 * \return 0 on success, anything else to skip audio playback
 */
/*void aFmtSetup(void **opaque, char *format, unsigned *rate, unsigned *channels)
{

}*/

/**
 * Callback prototype for audio playback cleanup.
 *
 * This is called when the media player no longer needs an audio output.
 * \param opaque data pointer as passed to libvlc_audio_set_callbacks() [IN]
 */
/*void aFmtCleanUp(void *opaque)
{

}*/

//STREAMING OUT
/*int main(int argc, char **argv) {
    libvlc_instance_t *vlc;
    CStr *url;
    //Send File
    //Transcode it. Video codec use x264. Audio codec use mpga.
    //Mux it to mpegts format.
    //And stream it to udp://233.233.233.233:6666

    //CStr *sout = "#transcode{vcodec=h264,fps=25,venc=x264{preset=ultrafast,"\
    //	"profile=main,tune=zerolatency},vb=512,scale=0.5,"                        \
    //	"acodec=mpa,aenc=ffmpeg,ab=64,channels=2}"                                \
    //":standard{access=udp,mux=ts,dst=233.233.233.233:6666}";

    //Send and playing at same time
    CStr *sout = "#transcode{vcodec=h264,fps=25,venc=x264{preset=ultrafast,"\
        "profile=baseline,tune=zerolatency},vb=512,"                              \
        "acodec=mpga,ab=64,channels=2}"                                           \
        ":duplicate{dst=display,dst=standard{access=udp,mux=ts,dst=233.233.233.233:6666}}";
    CStr *media_name = "Lei's test";

    //Screen Capture
    //url = "screen://";

    url = "cuc_ieschool.flv";

    vlc = libvlc_new(0, NULL);
    libvlc_vlm_add_broadcast(vlc, media_name, url, sout, 0, NULL, true, false);
    libvlc_vlm_play_media(vlc, media_name);

    //play 30s
    _sleep(30000);

    libvlc_vlm_stop_media(vlc, media_name);
    libvlc_vlm_release(vlc);
    return 0;
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkMediaPlayerClose(SkObject *obj)
{
    SkMediaPlayer *f = static_cast<SkMediaPlayer *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkMediaPlayerDestroyVlcInstance()");

    if (f->isPlaying())
        f->stop();
}

ConstructorImpl(SkMediaPlayer, SkObject)
{
    inst = nullptr;
    mp = nullptr;
    m = nullptr;
    em = nullptr;
    audioProduction = nullptr;

    SlotSet(play);
    SlotSet(pause);
    SlotSet(stop);

    SignalSet(started);
    SignalSet(paused);
    SignalSet(stopped);

    SignalSet(opening);
    SignalSet(buffering);
    SignalSet(playing);
    SignalSet(snapshotTaken);
    SignalSet(timeChanged);
    SignalSet(endReached);
    SignalSet(volumeChanged);
    SignalSet(positionChanged);
    SignalSet(lengthChanged);
    SignalSet(seekableChanged);
    SignalSet(pausableChanged);
    SignalSet(titleChanged);
    SignalSet(forward);
    SignalSet(backward);
    SignalSet(corked);
    SignalSet(unCorked);
    SignalSet(muted);
    SignalSet(unMuted);
    SignalSet(audioDevice);

    SignalSet(errorOccurred);

    addDtorCompanion(SkMediaPlayerClose);
}

void SkMediaPlayer::setMedia(CStr *m)
{
    if (isPlaying())
    {
        ObjectError("Cannot set new media because the player is ALREADY active");
        return;
    }

    media = m;
    ObjectDebug("New media: " << media);
}

CStr *SkMediaPlayer::getMedia()
{
    return media.c_str();
}

bool SkMediaPlayer::isPlaying()
{
    return (inst != nullptr && (libvlc_media_player_is_playing(mp) == 1));
}

bool SkMediaPlayer::isSeekable()
{
    return (libvlc_media_player_is_seekable(mp) == 1);
}

int SkMediaPlayer::getVolume()
{
    return libvlc_audio_get_volume(mp);
}

bool SkMediaPlayer::setVolume(int volume)
{
    ObjectDebug("Setting volume to: " << volume);
    return (libvlc_audio_set_volume(mp , volume) == 0);
}

int64_t  SkMediaPlayer::getTime()
{
    return libvlc_media_player_get_time(mp);
}

void SkMediaPlayer::setTime(int64_t time)
{
    if (!isSeekable())
    {
        ObjectError("Cannot set time position because the media is NOT seekable");
        return;
    }

    int64_t l = getLength();

    if (time >= l || time < 0)
    {
        ObjectError("Cannot set time position because the value is NOT valid [0 - " << l << " ms]: " << time << " ms");
        return;
    }

    ObjectDebug("Setting time position to: " << time << " ms");
    libvlc_media_player_set_time(mp, time);
}

float SkMediaPlayer::getPosition()
{
    return libvlc_media_player_get_position(mp);
}

void SkMediaPlayer::setPosition(float pos)
{
    if (!isSeekable())
    {
        ObjectError("Cannot set percentage position because the media is NOT seekable");
        return;
    }

    if (pos > 1 || pos < 0)
    {
        ObjectError("Cannot set percentage position because the value is NOT valid [0.0 - 1.0]: " << pos);
        return;
    }

    ObjectDebug("Setting percentage position to: " << pos);
    libvlc_media_player_set_position(mp, pos );
}

int64_t  SkMediaPlayer::getLength()
{
    //libvlc_media_get_duration
    return libvlc_media_player_get_length(mp);
}

bool SkMediaPlayer::takeSnapshot( const char* outputFile, unsigned int width, unsigned int heigth )
{
    ObjectDebug("Saving a snapshot [res->" << width << "x" << heigth <<"] to file: " << outputFile);
    return (libvlc_video_take_snapshot(mp, 0, outputFile, width, heigth) == 0);
}

/*bool SkMediaPlayer::hasVideo()
{
    return libvlc_media_player_has_vout(mp);
}*/

bool SkMediaPlayer::setDrawableWidget(void *drawable)
{
    ObjectDebug("Setting VideoOutput [" << drawable <<"]");
    libvlc_media_player_set_xwindow(mp, reinterpret_cast<intptr_t>(drawable));

/*#if defined(Q_WS_MAC)
    libvlc_media_player_set_nsobject(mp, drawable);
#elif defined(Q_OS_UNIX)
    libvlc_media_player_set_xwindow(mp, reinterpret_cast<intptr_t>(drawable));
#elif defined(Q_OS_WIN)
    libvlc_media_player_set_hwnd(mp, drawable);
#endif*/

    return true;
}

void SkMediaPlayer::getVideoSize( uint32_t *outWidth, uint32_t *outHeight )
{
    libvlc_video_get_size(mp, 0, outWidth, outHeight );
}

void SkMediaPlayer::nextFrame()
{
    libvlc_media_player_next_frame(mp);
}

int SkMediaPlayer::getAudioTracksCount()
{
    return libvlc_audio_get_track_count(mp);
}

int SkMediaPlayer::getVideoTracksCount()
{
    return libvlc_video_get_track_count(mp);
}

void SkMediaPlayer::setKeyInput( bool enabled )
{
    libvlc_video_set_key_input(mp, enabled );
}

SkRingBuffer *SkMediaPlayer::getAudioProduction()
{
    return audioProduction;
}

SlotImpl(SkMediaPlayer, play)
{
    SilentSlotArgsWarning();

    if (isPlaying())
    {
        ObjectError("Cannot start because the player is ALREADY active");
        return;
    }

    audioProduction = new SkRingBuffer;
    inst = libvlc_new(0, nullptr);

    //OTHER INSTANCING-MODE
    //vlc_declare_plugin(mp4);
    //vlc_declare_plugin(dummy);
    //CVoid **builtins = { vlc_plugin(mp4), vlc_plugin(dummy), NULL };
    //inst = libvlc_new_with_builtins(argc, argv, builtins);

    if (!inst)
    {
        ObjectError("Cannot create a new VideoLAN-library instance");
        errorOccurred();
        return;
    }

    ObjectDebug("New VideoLAN-library instance allocated successfully [version: " << libvlc_get_version() << "]");
    CStr *s = media.c_str();

    //Create a new item
    if (SkFsUtils::exists(s))
    {
        SkFileInfo info;
        SkFsUtils::fillFileInfo(s, info);

        if (info.isFile)
            m = libvlc_media_new_path(inst, info.completeAbsolutePath.c_str());

        else
        {
            libvlc_release (inst);
            inst = nullptr;

            ObjectError("Media is NOT a file: " << media);
        }
    }

    else
        m = libvlc_media_new_location(inst, s);

    if (!m)
    {
        ObjectError("Cannot create the new media");
        errorOccurred();
        return;
    }

    //Create a media player playing environement
   mp= libvlc_media_player_new_from_media(m);

    if (!mp)
    {
        ObjectError("Cannot create the new media-player");
        errorOccurred();
        return;
    }

    //No need to keep the media now
    libvlc_media_release(m);
    m = nullptr;

#if 0
    //This is a non working code that show how to hooks into a window,

    //if we have a window around
    libvlc_media_player_set_xwindow (mp, xid);

    //or on windows
    libvlc_media_player_set_hwnd (mp, hwnd);

    //or on mac os
    libvlc_media_player_set_nsobject (mp, view);
#endif

    em = libvlc_media_player_event_manager(mp);

    //Register the callback
    libvlc_event_attach(em, libvlc_MediaPlayerPositionChanged,  vlc_CB, this);
    libvlc_event_attach(em, libvlc_MediaPlayerSnapshotTaken,    vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerTimeChanged,      vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerPlaying,          vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerPaused,           vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerStopped,          vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerEndReached,       vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerPositionChanged,  vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerLengthChanged,    vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerEncounteredError, vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerPausableChanged,  vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerSeekableChanged,  vlc_CB, this );

    libvlc_event_attach(em, libvlc_MediaPlayerCorked,           vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerUncorked,         vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerMuted,            vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerUnmuted,          vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerAudioVolume,      vlc_CB, this );
    libvlc_event_attach(em, libvlc_MediaPlayerAudioDevice,      vlc_CB, this );

    //format a four-characters string identifying the sample format(e.g. "S16N" or "f32l")
    //libvlc_audio_set_format(mp, "f32l", 48000, 2);
    //libvlc_audio_set_callbacks(mp, aPlay, nullptr, nullptr, nullptr, nullptr, audioProduction);
    //libvlc_audio_set_callbacks(mp, aPlay, aPause, aResume, aFlush, aDrain, this);

    //play the media_player
    libvlc_media_player_play(mp);

    started();
}

SlotImpl(SkMediaPlayer, pause)
{
    SilentSlotArgsWarning();

    if (!isPlaying())
    {
        ObjectError("Cannot stop because the player is NOT active");
        errorOccurred();
        return;
    }

    //Pause playing
    libvlc_media_player_pause(mp);

    //THE SIGNAL IS EMITTED BY VLC-EV
}

SlotImpl(SkMediaPlayer, stop)
{
    SilentSlotArgsWarning();

    if (!isPlaying())
    {
        ObjectError("Cannot stop because the player is NOT active");
        errorOccurred();
        return;
    }

    //libvlc_event_detach(em, libvlc_MediaPlayerSnapshotTaken, vlc_CB, this);

    //Stop playing
    libvlc_media_player_stop(mp);

    //Free the media_player
    libvlc_media_player_release(mp);

    libvlc_release(inst);

    inst = nullptr;
    mp = nullptr;

    stopped();

    delete audioProduction;
}

#include "Core/Containers/skvariant.h"

void SkMediaPlayer::cbcom(const libvlc_event_t *event)
{
    if (isPreparedToDie())
        return;

    if (event->type == libvlc_MediaPlayerPlaying)
        playing();

    else if (event->type == libvlc_MediaPlayerPaused)
        paused();

    else if (event->type == libvlc_MediaPlayerStopped)
    {}//DOES NOTHING: SIG IS EMITTED ON stop() SLOT, WHEN THE player IS NULL

    else if (event->type == libvlc_MediaPlayerEndReached)
        endReached();

    else if (event->type == libvlc_MediaPlayerTimeChanged)
    {
        SkVariantVector p;
        p << event->u.media_player_time_changed.new_time;
        timeChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerPositionChanged)
    {
        if (!isSeekable())
            return;

        SkVariantVector p;
        p << event->u.media_player_position_changed.new_position;
        positionChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerLengthChanged)
    {
        SkVariantVector p;
        p << event->u.media_player_length_changed.new_length;
        lengthChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerSnapshotTaken)
    {
        SkVariantVector p;
        p << event->u.media_player_snapshot_taken.psz_filename;
        snapshotTaken(this, p);
    }

    else if (event->type == libvlc_MediaPlayerSeekableChanged)
    {
        SkVariantVector p;
        p << event->u.media_player_seekable_changed.new_seekable;
        seekableChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerPausableChanged)
    {
        SkVariantVector p;
        p << event->u.media_player_pausable_changed.new_pausable;
        pausableChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerTitleChanged)
    {
        SkVariantVector p;
        p << event->u.media_player_title_changed.new_title;
        titleChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerNothingSpecial)
    {}//PERHAPS IT HAS NO SENSE?

    else if (event->type == libvlc_MediaPlayerOpening)
        opening();

    else if (event->type == libvlc_MediaPlayerBuffering)
    {
        SkVariantVector p;
        p << event->u.media_player_buffering.new_cache;
        buffering(this, p);
    }

    else if (event->type == libvlc_MediaPlayerForward)
        forward();

    else if (event->type == libvlc_MediaPlayerBackward)
        backward();

    else if (event->type == libvlc_MediaPlayerCorked)
        corked();

    else if (event->type == libvlc_MediaPlayerUncorked)
        unCorked();

    else if (event->type == libvlc_MediaPlayerMuted)
        muted();

    else if (event->type == libvlc_MediaPlayerUnmuted)
        unMuted();

    else if (event->type == libvlc_MediaPlayerAudioVolume)
    {
        SkVariantVector p;
        p << event->u.media_player_audio_volume.volume;
        volumeChanged(this, p);
    }

    else if (event->type == libvlc_MediaPlayerAudioDevice)
    {
        SkVariantVector p;
        p << event->u.media_player_audio_device.device;
        audioDevice(this, p);
    }

    else if (event->type == libvlc_MediaPlayerEncounteredError)
    {
        ObjectError("RECEIVED libvlc_MediaPlayerEncounteredError from VideoLAN-library: " << libvlc_errmsg());

        SkVariantVector p;
        p << libvlc_errmsg();
        errorOccurred(this, p);
    }

    else
        ObjectWarning("VideoLAN-library UNKNOWN event");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
