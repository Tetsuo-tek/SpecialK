#include "skfftspectrogram.h"
#include "Core/App/skeventloop.h"
#include <Core/Containers/skarraycast.h>

#if defined(ENABLE_CV)

SkFftSpectrogram::SkFftSpectrogram()
{
    enabled = false;

    bufferFrames = 0;
    sampleRate = 0;

    fftSize = 0;
    fftRange = 0;
    freqInterval = 0.f;

    maximumFreqHz = 0;
    minimumFreqHz = 0;
    fromFreqId = -1;
    toFreqId = -1;
    customRangeEnabled = false;

    gradient_t = COLORMAP_MAGMA;

    enableGridX = false;
    enableGridY = false;
    enableGridLabelX = false;
    enableGridLabelY = false;

    scrollingIntervalSecs = 0.;
}

void SkFftSpectrogram::setup(SkMat *frame, uint samples, uint rate, Size resolution)
{
    f = frame;

    bufferFrames = samples;
    sampleRate = rate;
    fftSize = (bufferFrames/2) + 1;

    res = resolution;
}

void SkFftSpectrogram::enableGrid(bool enableX, bool enableY)
{
    enableGridX = enableX;
    enableGridY = enableY;
}

void SkFftSpectrogram::enableGridLabels(bool enableX, bool enableY)
{
    enableGridLabelX = enableX;
    enableGridLabelY = enableY;
}

void SkFftSpectrogram::changeColorMap(ColormapTypes t)
{
    gradient_t = t;
}

void SkFftSpectrogram::setCustomRange(UInt minHz, UInt maxHz)
{
    minimumFreqHz = minHz;
    maximumFreqHz = maxHz;
}

void SkFftSpectrogram::enableCustomRange(bool enabled)
{
    customRangeEnabled = enabled;
}

void SkFftSpectrogram::setScrollingInterval(UInt ms)
{
    scrollingIntervalSecs = ms/1000.;
}

void SkFftSpectrogram::start()
{
    fftRange = fftSize;
    freqInterval = (sampleRate/2.f + 1)/fftRange;

    if (customRangeEnabled)
    {
        if (maximumFreqHz > minimumFreqHz)
        {
            toFreqId = (maximumFreqHz/freqInterval);
            fftRange -= (fftSize-toFreqId);

            FlatMessage("Custom spectrum display to: " << maximumFreqHz << " Hz");
        }

        else
            toFreqId = -1;

        if (minimumFreqHz > 0)
        {
            fromFreqId = (minimumFreqHz/freqInterval);
            fftRange -= fromFreqId;
            FlatMessage("Custom spectrum display from: " << minimumFreqHz << " Hz");
        }

        else
            fromFreqId = -1;
    }

    else
    {
        fromFreqId = -1;
        toFreqId = -1;

        FlatMessage("Full spectrum display: [0, " << (fftSize*freqInterval) << "] Hz");
    }

    int fftFrame_W = fftRange/* * parameters.getChannels()*/;

    fftFrameRow = Mat(1, fftFrame_W, CV_32FC1, Scalar(0));
    spectralFrameRow = Mat(1, fftFrame_W, CV_8UC3, Scalar(0, 0, 0));
    spectralFrame = Mat(res.height, res.width, CV_8UC3, Scalar(0, 0, 0));

    gradientBuilder.setup(&fftFrameRow, &spectralFrameRow, 0., 1., gradient_t);

    enabled = true;
}

void SkFftSpectrogram::stop()
{
    if (!fftValues.isEmpty())
    {
        SkAbstractListIterator<uchar *> *itr = fftValues.iterator();

        while(itr->next())
            delete [] itr->item();

        delete itr;

        fftValues.clear();
    }

    enabled = false;
}

void SkFftSpectrogram::tick(float *data)
{
    if (!isEnabled())
        return;

    if (customRangeEnabled && (fromFreqId > -1 || toFreqId > -1))
    {
        int lenOneChan = fftRange*sizeof(float);
        int from = 0;

        if (fromFreqId > -1)
            from = fromFreqId;

        //ONLY ONE CHAN
        CStr *d = SkArrayCast::toCStr(&data[from]);
        memcpy(fftFrameRow.data, d, lenOneChan);
    }

    else
        //ONLY ONE CHAN
        memcpy(fftFrameRow.data, data, fftSize*sizeof(float));

    gradientBuilder.tick();

    Mat resizedRow(1, res.width, CV_8UC3);
    Size tempSz(res.width, 1);

    resize(spectralFrameRow, resizedRow, tempSz, 0, 0, INTER_LINEAR);

    ULong rowSz = res.width*3;
    uchar *rowBgr = new uchar [rowSz];
    memcpy(rowBgr, resizedRow.data, rowSz);

    fftValues.prepend(rowBgr);

    if (fftValues.count() == static_cast<ULong>(spectralFrame.size().height)+1)
        delete [] fftValues.removeLast();

    if (scrollingIntervalSecs > 0.)
    {
        if (chrono.stop() < scrollingIntervalSecs)
            return;

        chrono.start();
    }

    int fontFace = cv::FONT_HERSHEY_SIMPLEX;
    double fontScale = 0.4;
    int thickness = 1;

    float incY = ((float) sampleRate/bufferFrames)*5;//?

    SkAbstractListIterator<uchar *> *itr = fftValues.iterator();

    for(float y=0; y<spectralFrame.size().height; y+=incY)
    {
        int row = y;

        while(row < y+incY && itr->next())
            memcpy(spectralFrame.ptr<uchar>(row++), itr->item(), rowSz);

        if (y==0)
            continue;

        if (enableGridY)
        {
            Point startPoint(0, y);
            Point endPoint(spectralFrame.size().width-1, y);
            line(spectralFrame, startPoint, endPoint, Scalar(100, 100, 100), thickness);
        }

        if (enableGridLabelY)
        {
            float tickTimePeriod = 1.f / (sampleRate / bufferFrames);
            SkString txt(y*tickTimePeriod);
            txt.append(" s");

            int baseline = 0;
            Size txtSize = getTextSize(txt, fontFace, fontScale, thickness, &baseline);
            txtSize.width += 4;
            txtSize.height += 4;

            int txtY = txtSize.height/2;
            rectangle(spectralFrame, Rect(0, y-txtY, txtSize.width, txtSize.height), Scalar(0, 0, 0), cv::FILLED);

            Point txtPoint(2, y+txtY-1);
            putText(spectralFrame, txt, txtPoint, fontFace, fontScale, Scalar(255, 255, 255), thickness);
        }
    }

    delete itr;

    if (enableGridX || enableGridLabelX)
    {
        float incX = (float) res.width/(5);
        float mult = (float) fftFrameRow.size().width/res.width;

        for(float x=0; x<spectralFrame.size().width; x+=incX)
        {
            if (x==0)
                continue;

            if (enableGridX)
            {
                Point startPoint(x, 0);
                Point endPoint(x, spectralFrame.size().height-1);
                line(spectralFrame, startPoint, endPoint, Scalar(100, 100, 100), thickness);
            }

            if (enableGridLabelX)
            {
                // X X X z1 X X X X                  fftFrameRow.width   -> fftRange                                        [*channels]
                // X X X X X X X z2 X X X X X X X X  resolution.width    -> z1 = z2 * (fftFrameRow.width/resolution.width)

                float f = x;

                if (customRangeEnabled && fromFreqId > -1)
                {
                    //cout << "# " << f << " ";
                    f += (fromFreqId/mult);
                    //cout << f << "\n";
                }

                f *= ((freqInterval*mult)/1024);

                SkString txt(f, 1, true);
                txt.append(" kHz");

                int baseline = 0;
                Size txtSize = getTextSize(txt, fontFace, fontScale, thickness, &baseline);
                txtSize.width += 4;
                txtSize.height += 4;

                int txtX = txtSize.width/2;
                rectangle(spectralFrame, Rect(x-txtX, 0, txtSize.width, txtSize.height), Scalar(0, 0, 0), cv::FILLED);

                Point txtPoint(x-txtX+2, 3+txtSize.height/2);
                putText(spectralFrame, txt, txtPoint, fontFace, fontScale, Scalar(255, 255, 255), thickness);
            }
        }
    }

    f->set(spectralFrame);
}

bool SkFftSpectrogram::isEnabled()
{
    return enabled;
}

#endif
