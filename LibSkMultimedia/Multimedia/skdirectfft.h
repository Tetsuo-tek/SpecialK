/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SkDirectFFT_H
#define SkDirectFFT_H

#if defined(ENABLE_FFTW3LIB)

#include <Core/Object/skflatobject.h>
#include <fftw3.h>

enum SkFftPlanMode
{
    AFFT_ESTIMATE,
    AFFT_MEASURE
};

class SKMULTIMEDIA SkDirectFFT extends SkFlatObject
{
    public:
        SkDirectFFT();
        ~SkDirectFFT() override;

        void setup(uint samples, uint rate, bool enableGaussianWindow, SkFftPlanMode m=AFFT_ESTIMATE);
        void calculate(double *buffer);
        uint64_t count();
        float *getEnergies();
        float getEnergy(uint64_t id);
        float getFrequency(uint64_t id);
        float getFrequencyInterval();

    private:
        uint bufferSamples;
        float freqInterval;
        double *in;

        uint64_t fftSz;
        fftw_complex *out;
        float *db;

        fftw_plan plan;

        bool enableGaussianFilter;
};

#endif

#endif // SkDirectFFT_H
