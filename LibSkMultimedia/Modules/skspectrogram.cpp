#include "skspectrogram.h"
#include <Core/Containers/skarraycast.h>

#if defined(ENABLE_CV)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkSpectrogramCfg, SkAbstractModuleCfg)
{
}

void SkSpectrogramCfg::allowedCfgKeysSetup()
{
    addParam("source", "Fft source channel", T_STRING, true, "AudioCapture.FFT", "Audio fft (double) flow-channel name");

    addParam("enableGridX", "Enable Frequency grid", SkVariant_T::T_BOOL, false, false, "Enable Frequency grid (X-grid) lines");
    addParam("enableGridY", "Enable Time grid", SkVariant_T::T_BOOL, false, false, "Enable Time grid (Y-grid) lines");

    addParam("enableGridLabelX", "Enable Frequency labels", SkVariant_T::T_BOOL, false, false, "Enable Frequency labels on grid");
    addParam("enableGridLabelY", "Enable Time labels", SkVariant_T::T_BOOL, false, false, "Enable Time labels on grid");

    addParam("enableCustomRange", "Enable custom range", SkVariant_T::T_BOOL, false, false, "Enable custom range display");

    SkWorkerParam *p = nullptr;
    p = addParam("minHz", "Minimum Hz", T_INT32, true, -1, "Minimum frequency Hz");
    p->setMin(-1);

    p = addParam("maxHz", "Maximum Hz", T_INT32, true, -1, "Maximum frequency Hz");
    p->setMin(-1);

    p = addParam("scrollingIntervalMS", "Scrolling milliseconds interval", T_INT32, true, -1, "Scrolling-display milliseconds interval");
    p->setMin(-1);

    p = addParam("resolution", "Resolution", SkVariant_T::T_STRING, true, "640x480", "Output frame resolution");
    p->addOption("240p (4:3) 320x240", "320x240");
    p->addOption("240p () 426x240", "426x240");
    p->addOption("320p () 480x320", "480x320");
    p->addOption("360p (16:9) 640x360", "640x360");
    p->addOption("480p (4:3) 640x480", "640x480");
    p->addOption("480p (16:9) 854x480", "854x480");
    p->addOption("720p (16:9) 1280x720", "1280x720");
    p->addOption("1080p (16:9) 1920x1080", "1920x1080");
    p->addOption("1440p (16:9) 2560x1440", "2560x1440");
    p->addOption("2k or 1080p (1:1.77) 2048x1080", "2048x1080");
    p->addOption("4k or 2160p (1:1.9) 3840x2160", "3840x2160");
    p->addOption("8k or 4320p (16:9) 7680x4320", "7680x4320");

    p = addParam("compression", "Compression", SkVariant_T::T_UINT8, true, 50, "JPeg compression");
    p->setMin(10);
    p->setMax(100);
    p->setStep(10);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkSpectrogram, SkAbstractModule)
{
    publisher = new SkFlowVideoPublisher(this);

    sourceChan = -1;
    fps = 0;
    spectrogramEnabled = false;

    SlotSet(setFalseColorsMap);
}

bool SkSpectrogram::onSetup()
{
    SkAbstractModuleCfg *cfg = config();

    SkWorkerParam *p = nullptr;

    p = addSingleControl("False color map", T_UINT8, COLORMAP_MAGMA, "Select gradient of false colors to use for thermo image", setFalseColorsMap_SLOT);
    p->addOption("autumn", COLORMAP_AUTUMN);
    p->addOption("bone", COLORMAP_BONE);
    p->addOption("jet", COLORMAP_JET);
    p->addOption("winter", COLORMAP_WINTER);
    p->addOption("rainbow", COLORMAP_RAINBOW);
    p->addOption("ocean", COLORMAP_OCEAN);
    p->addOption("summer", COLORMAP_SUMMER);
    p->addOption("spring", COLORMAP_SPRING);
    p->addOption("cool", COLORMAP_COOL);
    p->addOption("hsv", COLORMAP_HSV);
    p->addOption("pink", COLORMAP_PINK);
    p->addOption("hot", COLORMAP_HOT);
    p->addOption("parula", COLORMAP_PARULA);
    p->addOption("magma", COLORMAP_MAGMA);
    p->addOption("inferno", COLORMAP_INFERNO);
    p->addOption("plasma", COLORMAP_PLASMA);
    p->addOption("viridis", COLORMAP_VIRIDIS);
    p->addOption("cividis", COLORMAP_CIVIDIS);
    p->addOption("twilight", COLORMAP_TWILIGHT);
    p->addOption("twilight shifted", COLORMAP_TWILIGHT_SHIFTED);
    p->addOption("turbo", COLORMAP_TURBO);

    sourceChanName = cfg->getParameter("source")->value().toString();

    return true;
}

void SkSpectrogram::onStart()
{
    setupPublisher(publisher);
    publisher->setObjectName(this, "Publisher");
}

void SkSpectrogram::onStop()
{}

void SkSpectrogram::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        SkAbstractModuleCfg *cfg = config();

        SkStringList nameParsed;
        sourceChanName.split(".", nameParsed);

        SkFlowSync *sync = buildSyncClient();
        AssertKiller(!sync);

        sync->setCurrentDbName(nameParsed.first().c_str());

        SkVariant paramsVal;

        //DEPRECATED
        SkString varName(sourceChanName);
        varName.append("_CHAN_PROPS");

        if (!sync->existsVariable(varName.c_str()))
        {
            requestToEnable(false);
            return;
        }

        sync->getVariable(varName.c_str(), paramsVal);
        SkArgsMap cfgMap;
        paramsVal.copyToMap(cfgMap);

        sync->close();
        sync->destroyLater();
        sync = nullptr;

        SkString tempRes = cfg->getParameter("resolution")->value().toString();

        if (!parseSize(objectName(), tempRes.c_str(), &resolution))
        {
            requestToEnable(false);
            return;
        }

        uint bufferFrames = cfgMap["bufferFrames"].toUInt();
        float sampleRate = cfgMap["sampleRate"].toFloat();

        fps = ceil(sampleRate / bufferFrames);
        publisher->setup(&f, resolution, fps, config()->getParameter("compression")->value().toInt());

        ObjectMessage("Spectrogram INITIALIZED"
                      << " - res: " << resolution
                      << " - frameRawSize: " << publisher->getRawFrameSize() << " B"
                      << " - fps: " << fps);

        spectrogram.setup(&f, cfgMap["bufferFrames"].toUInt(), cfgMap["sampleRate"].toUInt(), resolution);

        spectrogram.enableGrid(cfg->getParameter("enableGridX")->value().toBool(),
                               cfg->getParameter("enableGridY")->value().toBool());

        spectrogram.enableGridLabels(cfg->getParameter("enableGridLabelX")->value().toBool(),
                                     cfg->getParameter("enableGridLabelY")->value().toBool());


        spectrogram.setScrollingInterval(cfg->getParameter("scrollingIntervalMS")->value().toDouble()/1000);

        spectrogram.setCustomRange(cfg->getParameter("minHz")->value().toInt32(),
                                   cfg->getParameter("maxHz")->value().toInt32());

        spectrogram.enableCustomRange(cfg->getParameter("enableCustomRange")->value().toBool());


        spectrogram.start();
        publisher->start();

        eventLoop()->changeFastZone(static_cast<ulong>((1./fps)*1000000.));
        eventLoop()->changeSlowZone(eventLoop()->getFastInterval()*4);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);

        ObjectMessage("Spectrogram ENABLED");
    }

    else
    {
        spectrogram.stop();
        publisher->stop();

        eventLoop()->changeFastZone(250000);
        eventLoop()->changeSlowZone(250000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

        ObjectMessage("Spectrogram DISABLED");
    }
}

void SkSpectrogram::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.data.isEmpty() || !isEnabled())
        return;

    if (chData.chanID == sourceChan)
    {
        spectrogram.tick(SkArrayCast::toFloat(chData.data.toVoid()));
        publisher->tick();
    }
}

void SkSpectrogram::onFastTick()
{
    if (spectrogramEnabled)
    {
        if (!publisher->hasTargets())
        {
            ObjectMessage("Capture tick NOT-ACTIVE");
            spectrogramEnabled = false;
            unsubscribeChannel(sourceChan);
            return;
        }
    }

    else
    {
        if (publisher->hasTargets())
        {
            ObjectMessage("Capture tick ACTIVE");
            spectrogramEnabled = true;
            subscribeChannel(sourceChanName.c_str(), sourceChan);
        }

        else
            return;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkSpectrogram, setFalseColorsMap)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;

    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["setFalseColorsMap"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (ctrl->load(cmdMap))
    {
        updateCommand(ctrl);

        ColormapTypes gradient = static_cast<ColormapTypes>(cmdMap["setFalseColorsMap"].toInt());
        spectrogram.changeColorMap(gradient);

        if (t)
        {
            t->setResponse(cmdMap);
            t->goBack();
        }
    }

    else
    {
        if (t)
        {
            t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif
