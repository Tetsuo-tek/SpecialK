#ifndef SKSPECTROGRAM_H
#define SKSPECTROGRAM_H

#if defined(ENABLE_CV)

#include "Core/System/Network/FlowNetwork/skflowvideopublisher.h"

#include "Modules/skabstractmodule.h"
#include <Multimedia/Image/skimagegradient.h>
#include <Multimedia/Audio/skaudioparameters.h>
#include "Multimedia/skfftspectrogram.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkSpectrogramCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkSpectrogramCfg, SkAbstractVideoModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkSpectrogram extends SkAbstractModule
{
    SkFlowVideoPublisher *publisher;
    SkMat f;

    SkString sourceChanName;
    SkFlowChanID sourceChan;

    SkFftSpectrogram spectrogram;
    Size resolution;
    int fps;

    bool spectrogramEnabled;

    public:
        Constructor(SkSpectrogram, SkAbstractModule);

        Slot(setFalseColorsMap);

    private:

        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;
        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        void onFastTick()                                               override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKSPECTROGRAM_H
