TARGET = SkMultimedia
TEMPLATE = lib

# insert sources

include(LibSkMultimedia.pri)
include(module-src.pri)

DEFINES += SKMULTIMEDIA_LIBRARY

# include libSkCore

LIBS += -lSkCore
INCLUDEPATH += /usr/local/include/LibSkCore
include(/usr/local/include/LibSkCore/LibSkCore.pri)

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkMultimedia/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkAudio.pri

headersDataFiles.path = /usr/local/include/LibSkMultimedia/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles

