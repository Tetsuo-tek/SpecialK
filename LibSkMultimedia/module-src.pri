HEADERS += \
    $$PWD/Multimedia/skdirectfft.h \
    $$PWD/Multimedia/skfftspectrogram.h \
    $$PWD/Multimedia/skmediaplayer.h \
    $$PWD/Modules/skspectrogram.h

SOURCES += \
    $$PWD/Multimedia/skdirectfft.cpp \
    $$PWD/Multimedia/skfftspectrogram.cpp \
    $$PWD/Multimedia/skmediaplayer.cpp \
    $$PWD/Modules/skspectrogram.cpp

DISTFILES +=
