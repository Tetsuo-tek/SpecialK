
!android: {
    LIBS += -lpthread
    LIBS += -lz
}

LIBS += -lcrypto
LIBS += -lssl

HEADERS += \
    $$PWD/Core/Object/skflatsignal.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowcommon.h \
    $$PWD/Core/System/skcli.h \
    $$PWD/skdefines.h \
    $$PWD/Core/Object/skflatobject.h \
    $$PWD/Core/Containers/skbinaryheap.h \
    $$PWD/Core/Containers/skbinarynode.h \
    $$PWD/Core/Containers/skredblacktree.h \
    $$PWD/Core/Containers/skset.h \
    $$PWD/Core/Containers/sktreemap.h \
    $$PWD/Core/Containers/sktreenode.h \
    $$PWD/Core/Containers/abstract/skabstractbuffer.h \
    $$PWD/Core/Containers/abstract/skabstractpointer.h \
    $$PWD/Core/Containers/skcpointer.h \
    $$PWD/Core/Containers/sklinkedmap.h \
    $$PWD/Core/sklogmachine.h \
    $$PWD/Core/Containers/abstract/skabstractarray.h \
    $$PWD/Core/Containers/abstract/skabstractcontainer.h \
    $$PWD/Core/Containers/abstract/skabstractiterator.h \
    $$PWD/Core/Containers/abstract/skabstractlist.h \
    $$PWD/Core/Containers/abstract/skabstractmap.h \
    $$PWD/Core/Containers/skarray.h \
    $$PWD/Core/Containers/sklist.h \
    $$PWD/Core/Containers/skmap.h \
    $$PWD/Core/Containers/skmatrix.h \
    $$PWD/Core/Containers/skgraph.h \
    $$PWD/Core/Containers/skpair.h \
    $$PWD/Core/Containers/skqueue.h \
    $$PWD/Core/Containers/sksort.h \
    $$PWD/Core/Containers/skstack.h \
    $$PWD/Core/Containers/skvector.h \
    $$PWD/Core/Containers/skargsmap.h \
    $$PWD/Core/Containers/skarraycast.h \
    $$PWD/Core/Containers/skdatabuffer.h \
    $$PWD/Core/Containers/skringbuffer.h \
    $$PWD/Core/Containers/skstring.h \
    $$PWD/Core/Containers/skstringlist.h \
    $$PWD/Core/Containers/skvariant.h \
    $$PWD/Core/System/skdatacrypt.h \
    $$PWD/Core/System/Time/skdatetime.h \
    $$PWD/Core/System/Time/skelapsedtime.h \
    $$PWD/Core/System/skabstractflatdevice.h \
    $$PWD/Core/System/Filesystem/skflatfile.h \
    $$PWD/Core/System/Filesystem/skflattempfile.h \
    $$PWD/Core/System/Network/sknetutils.h \
    $$PWD/Core/System/Network/skipaddress.h \
    $$PWD/Core/System/Network/skabstractflatserver.h \
    $$PWD/Core/System/Network/skabstractflatsocket.h \
    $$PWD/Core/System/Network/TCP/skflattcpserver.h \
    $$PWD/Core/System/Network/TCP/skflattcpsocket.h \
    $$PWD/Core/System/Network/LOCAL/skflatlocalsocket.h \
    $$PWD/Core/System/Network/LOCAL/skflatlocalserver.h \
    $$PWD/Core/System/skosenv.h \
    $$PWD/Core/System/skprocfs.h \
    $$PWD/Core/System/sksystem.h \
    $$PWD/Core/System/Filesystem/skfsutils.h \
    $$PWD/Core/skmath.h

SOURCES += \
    $$PWD/Core/Object/skflatsignal.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowcommon.cpp \
    $$PWD/Core/System/skcli.cpp \
$$PWD/skdefines.cpp \
    $$PWD/Core/Object/skflatobject.cpp \
    $$PWD/Core/Containers/skargsmap.cpp \
    $$PWD/Core/Containers/skarraycast.cpp \
    $$PWD/Core/Containers/skdatabuffer.cpp \
    $$PWD/Core/Containers/skringbuffer.cpp \
    $$PWD/Core/Containers/skstring.cpp \
    $$PWD/Core/Containers/skstringlist.cpp \
    $$PWD/Core/Containers/skvariant.cpp \
    $$PWD/Core/System/skdatacrypt.cpp \
    $$PWD/Core/System/Time/skdatetime.cpp \
    $$PWD/Core/System/Time/skelapsedtime.cpp \
    $$PWD/Core/System/skabstractflatdevice.cpp \
    $$PWD/Core/System/Filesystem/skflatfile.cpp \
    $$PWD/Core/System/Filesystem/skflattempfile.cpp \
    $$PWD/Core/System/Network/sknetutils.cpp \
    $$PWD/Core/System/Network/skipaddress.cpp \
    $$PWD/Core/System/Network/skabstractflatserver.cpp \
    $$PWD/Core/System/Network/skabstractflatsocket.cpp \
    $$PWD/Core/System/Network/TCP/skflattcpserver.cpp \
    $$PWD/Core/System/Network/TCP/skflattcpsocket.cpp \
    $$PWD/Core/System/Network/LOCAL/skflatlocalsocket.cpp \
    $$PWD/Core/System/Network/LOCAL/skflatlocalserver.cpp \
    $$PWD/Core/sklogmachine.cpp \
    $$PWD/Core/System/skosenv.cpp \
    $$PWD/Core/System/skprocfs.cpp \
    $$PWD/Core/System/sksystem.cpp \
    $$PWD/Core/System/Filesystem/skfsutils.cpp \
    $$PWD/Core/skmath.cpp
