#ifndef SKFLATSIGNAL_H
#define SKFLATSIGNAL_H

#include "Core/Containers/sktreemap.h"

#define AllowThis(CB) \
    friend void CB(SkFlatObject *_this, SkFlatObject *referer)

typedef void (SkCbFunct) (SkFlatObject *, SkFlatObject *);

struct SkCallback
{
    CStr *cbName;
    SkCbFunct *cb;
    SkFlatObject *_this;
};

class SkFlatSignal extends SkFlatObject
{
    SkTreeMap<SkString, SkCallback *> callbacks;

    public:
        SkFlatSignal();

        void attach(SkFlatObject *_this, SkCbFunct *cb, CStr *cbName);
        void detach(CStr *cbName);

        void trigger(SkFlatObject *referer);
        ULong count();
};

#define FlatAttach(SIG, THIS, CB) \
    SIG.attach(THIS, CB, TypeName(CB))

#define FlatDetach(SIG, CB) \
    SIG.detach(TypeName(CB))

#endif // SKFLATSIGNAL_H

/*
    //DECL SIG inside flatclass.h
    SkFlatSignal sig;

    //DECL CB inside flatclass.h
    static void cb(SkFlatObject *_this, SkFlatObject *referer);
    AllowThis(cb);

    //ATTACH TRIGGER DETACH
    FlatAttach(sig, this, cb);
    sig.trigger(&sig);
    FlatDetach(sig, cb);
*/
