#include "skflatobject.h"
#include "Core/sklogmachine.h"
#include "Core/Containers/skarraycast.h"

// // // // // // // // // // // // // // // // // // // // //

#include "Core/Containers/skvariant.h"

DeclareWrapper(SkFlatObject);

DeclareMeth_INSTANCE_VOID(SkFlatObject, setObjectName, Arg_CStr)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkFlatObject, setObjectName, 1, Arg_Custom(SkFlatObject), Arg_CStr)
DeclareMeth_INSTANCE_RET(SkFlatObject, objectName, CStr *)
DeclareMeth_INSTANCE_RET(SkFlatObject, typeName, CStr *)
DeclareMeth_INSTANCE_RET(SkFlatObject, typeID, uint64_t)
DeclareMeth_INSTANCE_RET(SkFlatObject, isFlatObject, bool)

SetupClassWrapper(SkFlatObject)
{
    AddMeth_INSTANCE_VOID(SkFlatObject, setObjectName);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkFlatObject, setObjectName, 1);
    AddMeth_INSTANCE_RET(SkFlatObject, objectName);
    AddMeth_INSTANCE_RET(SkFlatObject, typeName);
    AddMeth_INSTANCE_RET(SkFlatObject, typeID);
    AddMeth_INSTANCE_RET(SkFlatObject, isFlatObject);
}

// // // // // // // // // // // // // // // // // // // // //

//#include "Core/System/Thread/skthread.h"

SkFlatObject::SkFlatObject()
{
    isFlat = true;
    tid = std::this_thread::get_id();
    objName = "";

    CreateClassWrapper(SkFlatObject);
}

SkFlatObject::~SkFlatObject()
{}

void SkFlatObject::setObjectName(CStr *name)
{
    if (SkString::isEmpty(name))
    {
        FlatError("CANNOT set object name with a string empty or NULL");
        return;
    }

    SkString lastName = objName;
    objName = name;

    if (lastName.isEmpty())
        FlatPlusDebug("Set object name to: " << name);
    else
        FlatPlusDebug("Changed object name: " << lastName << " -> " << name);
}

void SkFlatObject::setObjectName(SkFlatObject *objContainer, CStr *name, CStr *separator)
{
    SkString tempName = objContainer->objectName();
    tempName.append(separator);
    tempName.append(name);

    setObjectName(tempName.c_str());
}

CStr *SkFlatObject::objectName()
{
    return objName.c_str();
}

CStr *SkFlatObject::typeName()
{
    return classTypesPtr()[flatID]->name;
}

uint64_t SkFlatObject::typeID()
{
    return flatID;
}

SkClassType *SkFlatObject::type()
{
    return classTypesPtr()[flatID];
}

SkThID SkFlatObject::threadID()
{
    return tid;
}

bool SkFlatObject::isFlatObject()
{
    return isFlat;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

