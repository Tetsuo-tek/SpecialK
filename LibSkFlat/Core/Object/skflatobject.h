/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKFLATOBJECT_H
#define SKFLATOBJECT_H

#include "skdefines.h"

/**
 * Flat means standard (+ or
 *
 * @brief The SkFlatObject is the common superclass for all SpecialK objects,
 * excepts for some types, as for example SkString and others
 */
class SPECIALK SkFlatObject
{
    public:
        /**
         * @brief Constructor
         */
        SkFlatObject();

        /**
         * @brief Destructor
        */
        virtual ~SkFlatObject();

        /**
         * @brief Sets the object instance name
         * @param name the instance name
         */
        void setObjectName(CStr *name);

        /**
         * @brief Sets the object instance name as pseudo hierarchy-naming
         * @param objContainer the object containing
         * @param name the instance name
         * @param separator the separator of the hierarchy
         */
        void setObjectName(SkFlatObject *objContainer, CStr *name, CStr *separator=".");

        /**
         * @brief Gets the cstring for the instance name of this object
         * @return the cstring for the object name
         */
        CStr *objectName();

        /**
         * @brief Gets the cstring for the instance name of this object
         * @return the cstring for the object typename
         */
        CStr *typeName();

        /**
         * @brief Gets the int index for the ClassType of this object
         * @return the FlatID
         */
        uint64_t typeID();

        /**
         * @brief Gets the int the ClassType of this object
         * @return the SkClassType structure
         */
        SkClassType *type();

        /**
         * @brief Gets the threadID of the thread in which the object lives
         * @return the threadID
         */
        SkThID threadID();

        /**
         * @brief It specifies if this object has SkObject in the type hierarchy
         * @return true if its own subclassing hierarchy does NOT include SkObject
         */
        bool isFlatObject();

    protected:
        SkThID tid;
        uint64_t flatID;
        bool isFlat;
        string objName;

};

#endif // SKFLATOBJECT_H
