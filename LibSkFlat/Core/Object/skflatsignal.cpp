#include "skflatsignal.h"
#include "Core/sklogmachine.h"

SkFlatSignal::SkFlatSignal()
{}

void SkFlatSignal::attach(SkFlatObject *_this, SkCbFunct *cb, CStr *cbName)
{
    SkCallback *callback = new SkCallback;
    callback->cbName = cbName;
    callback->_this = _this;
    callback->cb = cb;

    callbacks[cbName] = callback;
}

void SkFlatSignal::detach(CStr *cbName)
{
    if (!callbacks.contains(cbName))
    {
        FlatError("CallBack NOT found: " << cbName);
        return;
    }

    delete callbacks.remove(cbName).value();
}

void SkFlatSignal::trigger(SkFlatObject *referer)
{
    SkBinaryTreeVisit<SkString, SkCallback *> *itr = callbacks.iterator();

    while(itr->next())
    {
        SkCallback *callback = itr->item().value();
        callback->cb(callback->_this, referer);
    }

    delete itr;
}

ULong SkFlatSignal::count()
{
    return callbacks.count();
}
