#ifndef SKQUEUE_H
#define SKQUEUE_H

#include "sklist.h"

template <typename T>
class SPECIALK SkQueue extends SkAbstractContainer
{
    public:
        SkQueue();
        SkQueue(const SkQueue<T> &source);
        ~SkQueue();

        T &first();
        T &last();

        void enqueue(const T &e);
        T dequeue();

        void clear();
        bool isEmpty();
        uint64_t count();
        uint64_t size();

        void swap(SkQueue<T> &other);

        typename SkList<T>::SkIterator *iterator();
        SkQueue<T> &operator = (const SkQueue<T> &source);

        SkList<T> &getInternalList();

    private:
        SkList<T> l;
};

template <typename T>
SkQueue<T>::SkQueue()
{
}

template <typename T>
SkQueue<T>::SkQueue(const SkQueue<T> &source)
{
    l = source.l;
}

template <typename T>
SkQueue<T>::~SkQueue()
{
}

template <typename T>
T &SkQueue<T>::first()
{
    return l.first();
}

template <typename T>
T &SkQueue<T>::last()
{
    return l.last();
}

template <typename T>
void SkQueue<T>::enqueue(const T &e)
{
    l.append(e);
}

template <typename T>
T SkQueue<T>::dequeue()
{
    return l.removeFirst();
}

template <typename T>
void SkQueue<T>::clear()
{
    l.clear();
}

template <typename T>
bool SkQueue<T>::isEmpty()
{
    return l.isEmpty();
}

template <typename T>
uint64_t SkQueue<T>::count()
{
    return l.count();
}

template <typename T>
uint64_t SkQueue<T>::size()
{
    return l.size();
}

template <typename T>
void SkQueue<T>::swap(SkQueue<T> &other)
{
    l.swap(other.l);
}

template <typename T>
typename SkList<T>::SkIterator *SkQueue<T>::iterator()
{
    return l.iterator();
}

template <typename T>
SkQueue<T> &SkQueue<T>::operator = (const SkQueue<T> &source)
{
    l = source.l;
    return *this;
}

template <typename T>
SkList<T> &SkQueue<T>::getInternalList()
{
    return l;
}

#endif // SKQUEUE_H
