#ifndef SKPAIR_H
#define SKPAIR_H

#include "Core/Object/skflatobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

template <typename K, typename V>
class SPECIALK SkPair extends SkFlatObject
{
    public:
        SkPair();
        SkPair(const SkPair<K, V> &pair);
        SkPair(const K &key, const V &value);
        ~SkPair();

        void set(const K &key, const V &value);

        void setKey(const K &key);
        K &key();

        void setValue(const V &value);
        V &value();

        void swap(SkPair<K, V> &other);

        SkPair<K, V> &operator  =  (const SkPair<K, V> &pair);

        bool operator           == (const K &key);

        //Default comparation run on value() content
        //Instead, use SkPairKeysComparator<K, V> to create a comparator on key()
        bool operator           == (const SkPair<K, V> &pair);
        bool operator           != (const SkPair<K, V> &pair);
        bool operator           <  (const SkPair<K, V> &pair);
        bool operator           <= (const SkPair<K, V> &pair);
        bool operator           >  (const SkPair<K, V> &pair);
        bool operator           >= (const SkPair<K, V> &pair);

    private:
        K k;
        V v;
};

template <typename K, typename V>
SkPair<K, V>::SkPair()
{}

template <typename K, typename V>
SkPair<K, V>::SkPair(const SkPair<K, V> &pair)
{
    k = pair.k;
    v = pair.v;
}

template <typename K, typename V>
SkPair<K, V>::SkPair(const K &key, const V &value)
{
    set(key, value);
}

template <typename K, typename V>
SkPair<K, V>::~SkPair()
{}

template <typename K, typename V>
void SkPair<K, V>::set(const K &key, const V &value)
{
    k = key;
    v = value;
}

template <typename K, typename V>
void SkPair<K, V>::setKey(const K &key)
{
    k = key;
}

template <typename K, typename V>
K &SkPair<K, V>::key()
{
    return k;
}

template <typename K, typename V>
void SkPair<K, V>::setValue(const V &value)
{
    v = value;
}

template <typename K, typename V>
V &SkPair<K, V>::value()
{
    return v;
}

template <typename K, typename V>
void SkPair<K, V>::swap(SkPair<K, V> &other)
{
    K k = this->k;
    V v = this->v;

    this->k = other.k;
    this->v = other.v;

    other.k = k;
    other.v = v;
}

template <typename K, typename V>
SkPair<K, V> &SkPair<K, V>::operator = (const SkPair<K, V> &pair)
{
    k = pair.k;
    v = pair.v;

    return *this;
}

template <typename K, typename V>
bool SkPair<K, V>::operator == (const K &key)
{
    return key == k;
}

template <typename K, typename V>
bool SkPair<K, V>::operator == (const SkPair<K, V> &pair)
{
    return v == pair.v;
}

template <typename K, typename V>
bool SkPair<K, V>::operator != (const SkPair<K, V> &pair)
{
    return v != pair.v;
}

template <typename K, typename V>
bool SkPair<K, V>::operator <  (const SkPair<K, V> &pair)
{
    return (v < pair.v);
}

template <typename K, typename V>
bool SkPair<K, V>::operator <= (const SkPair<K, V> &pair)
{
    return (v <= pair.v);
}

template <typename K, typename V>
bool SkPair<K, V>::operator >  (const SkPair<K, V> &pair)
{
    return (v > pair.v);
}

template <typename K, typename V>
bool SkPair<K, V>::operator >= (const SkPair<K, V> &pair)
{
    return (v >= pair.v);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/Containers/sksort.h"

template <typename K, typename V>
class SPECIALK SkPairKeysComparator extends Comparator<SkPair<K, V>>
{
    public:
        SkPairKeysComparator(){}

        bool lt(SkPair<K, V> &a, SkPair<K, V> &b);
        bool le(SkPair<K, V> &a, SkPair<K, V> &b);
        bool gt(SkPair<K, V> &a, SkPair<K, V> &b);
        bool ge(SkPair<K, V> &a, SkPair<K, V> &b);
};

template <typename K, typename V>
bool SkPairKeysComparator<K, V>::lt(SkPair<K, V> &a, SkPair<K, V> &b)
{
    return (a.key()<b.key());
}

template <typename K, typename V>
bool SkPairKeysComparator<K, V>::le(SkPair<K, V> &a, SkPair<K, V> &b)
{
    return (a.key()<=b.key());
}

template <typename K, typename V>
bool SkPairKeysComparator<K, V>::gt(SkPair<K, V> &a, SkPair<K, V> &b)
{
    return (a.key()>b.key());
}

template <typename K, typename V>
bool SkPairKeysComparator<K, V>::ge(SkPair<K, V> &a, SkPair<K, V> &b)
{
    return (a.key()>=b.key());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKPAIR_H
