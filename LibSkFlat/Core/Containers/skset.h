#ifndef SKSET_H
#define SKSET_H

#include "skredblacktree.h"

template <typename T>
class SPECIALK SkSet extends SkAbstractContainer
{
    public:
        SkSet();
        SkSet(const SkSet<T> &source);
        ~SkSet();

        bool add(const T &e);
        bool remove(const T &e);
        bool contains(const T &e);

        void clear();
        bool isEmpty();
        uint64_t count();
        uint64_t size();

        void swap(SkSet<T> &other);

        SkBinaryTreeVisit<T, nullptr_t> *iterator();
        SkSet<T> &operator = (const SkSet<T> &source);

        SkRedBlackTree<T, nullptr_t> &getInternalTree();

    private:
        SkRedBlackTree<T, nullptr_t> t;
};

template <typename T>
SkSet<T>::SkSet()
{

}

template <typename T>
SkSet<T>::SkSet(const SkSet<T> &source)
{
    t = source.t;
}

template <typename T>
SkSet<T>::~SkSet()
{
}

template <typename T>
bool SkSet<T>::add(const T &e)
{
    if (t.contains(e))
        return false;

    return t.add(e);
}

template <typename T>
bool SkSet<T>::remove(const T &e)
{
    return t.remove(e);
}

template <typename T>
bool SkSet<T>::contains(const T &e)
{
    return t.contains(e);
}

template <typename T>
void SkSet<T>::clear()
{
    t.clear();
}

template <typename T>
bool SkSet<T>::isEmpty()
{
    return t.isEmpty();
}

template <typename T>
uint64_t SkSet<T>::count()
{
    return t.count();
}

template <typename T>
uint64_t SkSet<T>::size()
{
    return t.size();
}

template <typename T>
void SkSet<T>::swap(SkSet<T> &other)
{
    t.swap(other.t);
}

template <typename T>
SkBinaryTreeVisit<T, nullptr_t> *SkSet<T>::iterator()
{
    return t.iterator();
}

template <typename T>
SkSet<T> &SkSet<T>::operator = (const SkSet<T> &source)
{
    t = source.t;
}

template <typename T>
SkRedBlackTree<T, nullptr_t> &SkSet<T>::getInternalTree()
{
    return t;
}

#endif // SKSET_H
