#ifndef SKSTACK_H
#define SKSTACK_H

#include "sklist.h"

template <typename T>
class SPECIALK SkStack extends SkAbstractContainer
{
    public:
        SkStack();
        SkStack(const SkStack<T> &source);
        ~SkStack();

        T &top();

        void push(const T &e);
        T pop();

        void clear();
        bool isEmpty();
        uint64_t count();
        uint64_t size();

        void swap(SkStack<T> &other);

        typename SkList<T>::SkIterator *iterator();
        SkStack<T> &operator = (const SkStack<T> &source);

        SkList<T> &getInternalList();

    private:
        SkList<T> l;
};

template <typename T>
SkStack<T>::SkStack()
{
}

template <typename T>
SkStack<T>::SkStack(const SkStack<T> &source)
{
    l = source.l;
}

template <typename T>
SkStack<T>::~SkStack()
{
}

template <typename T>
T &SkStack<T>::top()
{
    return l.first();
}

template <typename T>
void SkStack<T>::push(const T &e)
{
    l.prepend(e);
}

template <typename T>
T SkStack<T>::pop()
{
    return l.removeFirst();
}

template <typename T>
void SkStack<T>::clear()
{
    l.clear();
}

template <typename T>
bool SkStack<T>::isEmpty()
{
    return l.isEmpty();
}

template <typename T>
uint64_t SkStack<T>::count()
{
    return l.count();
}

template <typename T>
uint64_t SkStack<T>::size()
{
    return l.size();
}

template <typename T>
void SkStack<T>::swap(SkStack<T> &other)
{
    l.swap(other.l);
}

template <typename T>
typename SkList<T>::SkIterator *SkStack<T>::iterator()
{
    return l.iterator();
}

template <typename T>
SkStack<T> &SkStack<T>::operator = (const SkStack<T> &source)
{
    l = source.l;
    return *this;
}

template <typename T>
SkList<T> &SkStack<T>::getInternalList()
{
    return l;
}

#endif // SKSTACK_H
