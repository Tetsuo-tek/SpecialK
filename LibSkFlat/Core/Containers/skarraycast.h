/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKARRAYCAST_H
#define SKARRAYCAST_H

#include "Core/Object/skflatobject.h"

//THIS CLASS IS CONVENIENCE AGGREGATE TO PERFORM CAST FOR ARRAYs TO PRIMITIVE TYPEs

/**
  * @brief SkArrayCast
  * It is a convenience aggregate to simplify c-cast for arrays between primitive types
*/

class SPECIALK SkArrayCast extends SkFlatObject
{
    public:
        /**
         * @brief Constructor
        */
        SkArrayCast();

        /**
         * @brief Stores a pointer; the pointer life is not managed here
         * @param ptr the raw pointer
         * @param size the bytes size of the pointer
        */
        void set(void *ptr, uint64_t size);

        /**
         * @brief Resets the object, removing the pointer reference
        */
        void unset();

        /**
         * @brief Grabs the size of the stored pointer
         * @return the size of the pointer; if it has not a pointer return 0
        */
        uint64_t size();

        /**
         * @brief Calculates the number of elements, of a specified size, in the stored pointer
         * @return the number of elements; if it has not a pointer return 0
        */
        uint64_t count(uint64_t elementByteSize);

        /**
         * @brief Checks if exists a stored pointer of data
         * @return the number of elements; if it has not a pointer return 0
        */
        bool isEmpty();

        /**
         * @brief Casts the stored pointer to a bool pointer
         * @return bool pointer if exists data, otherwise a NULL pointer
        */
        bool *toBool();

        /**
         * @brief Casts the stored pointer to a char pointer
         * @return char pointer if exists data, otherwise a NULL pointer
        */
        char *toChar();

        /**
         * @brief Casts the stored pointer to a int8_t pointer
         * @return int8_t pointer if exists data, otherwise a NULL pointer
        */
        int8_t *toInt8();

        /**
         * @brief Casts the stored pointer to a uint8_t pointer
         * @return uint8_t pointer if exists data, otherwise a NULL pointer
        */
        uint8_t *toUInt8();

        /**
         * @brief Casts the stored pointer to a int16_t pointer
         * @return int16_t pointer if exists data, otherwise a NULL pointer
        */
        int16_t *toInt16();

        /**
         * @brief Casts the stored pointer to a uint16_t pointer
         * @return uint16_t pointer if exists data, otherwise a NULL pointer
        */
        uint16_t *toUInt16();

        /**
         * @brief Casts the stored pointer to a int32_t pointer
         * @return int32_t pointer if exists data, otherwise a NULL pointer
        */
        int32_t *toInt32();

        /**
         * @brief Casts the stored pointer to a uint32_t pointer
         * @return uint32_t pointer if exists data, otherwise a NULL pointer
        */
        uint32_t *toUInt32();

        /**
         * @brief Casts the stored pointer to a float pointer
         * @return float pointer if exists data, otherwise a NULL pointer
        */
        float *toFloat();

        /**
         * @brief Casts the stored pointer to a int64_t pointer
         * @return int64_t pointer if exists data, otherwise a NULL pointer
        */
        int64_t *toInt64();

        /**
         * @brief Casts the stored pointer to a uint64_t pointer
         * @return uint64_t pointer if exists data, otherwise a NULL pointer
        */
        uint64_t *toUInt64();

        /**
         * @brief asts the stored pointer to a double pointer
         * @return double pointer if exists data, otherwise a NULL pointer
        */
        double *toDouble();

        /**
         * @brief Gets the stored pointer as raw void data
         * @return void row pointer if exists data, otherwise a NULL pointer
        */
        void *data();

        /**
         * @brief Casts the stored pointer to a bool pointer
         * @param ptr the raw pointer to cast
         * @return bool pointer if exists data, otherwise a NULL pointer
        */
        static bool *toBool(void *ptr);

        /**
         * @brief Casts the stored pointer to a char pointer
         * @param ptr the raw pointer to cast
         * @return char pointer if exists data, otherwise a NULL pointer
        */
        static char *toChar(void *ptr);
        static CStr *toCStr(CVoid *ptr);

        /**
         * @brief Casts the stored pointer to a int8_t pointer
         * @param ptr the raw pointer to cast
         * @return int8_t pointer if exists data, otherwise a NULL pointer
        */
        static int8_t *toInt8(void *ptr);

        /**
         * @brief Casts the stored pointer to a uint8_t pointer
         * @param ptr the raw pointer to cast
         * @return uint8_t pointer if exists data, otherwise a NULL pointer
        */
        static uint8_t *toUInt8(void *ptr);

        /**
         * @brief Casts the stored pointer to a int16_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained int16_t elements
         * @return int16_t pointer if exists data, otherwise a NULL pointer
        */
        static int16_t *toInt16(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a uint16_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained uint16_t elements
         * @return uint16_t pointer if exists data, otherwise a NULL pointer
        */
        static uint16_t *toUInt16(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a int32_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained int32_t elements
         * @return int32_t pointer if exists data, otherwise a NULL pointer
        */
        static int32_t *toInt32(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a uint32_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained uint32_t elements
         * @return uint32_t pointer if exists data, otherwise a NULL pointer
        */
        static uint32_t *toUInt32(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a float pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained float elements
         * @return float pointer if exists data, otherwise a NULL pointer
        */
        static float *toFloat(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a int64_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained int64_t elements
         * @return int64_t pointer if exists data, otherwise a NULL pointer
        */
        static int64_t *toInt64(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a uint64_t pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained uint64_t elements
         * @return uint64_t pointer if exists data, otherwise a NULL pointer
        */
        static uint64_t *toUInt64(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a double pointer
         * @param ptr the raw pointer to cast
         * @param size the bytes size of the pointer
         * @param count the filling reference value of the calculated number of contained double elements
         * @return double pointer if exists data, otherwise a NULL pointer
        */
        static double *toDouble(void *ptr, uint64_t size, uint64_t &count);

        /**
         * @brief Casts the stored pointer to a int16_t pointer
         * @param ptr the raw pointer to cast
         * @return int16_t pointer if exists data, otherwise a NULL pointer
        */
        static int16_t *toInt16(void *ptr);

        /**
         * @brief Casts the stored pointer to a uint16_t pointer
         * @param ptr the raw pointer to cast
         * @return uint16_t pointer if exists data, otherwise a NULL pointer
        */
        static uint16_t *toUInt16(void *ptr);

        /**
         * @brief Casts the stored pointer to a int32_t pointer
         * @param ptr the raw pointer to cast
         * @return int32_t pointer if exists data, otherwise a NULL pointer
        */
        static int32_t *toInt32(void *ptr);

        /**
         * @brief Casts the stored pointer to a uint32_t pointer
         * @param ptr the raw pointer to cast
         * @return uint32_t pointer if exists data, otherwise a NULL pointer
        */
        static uint32_t *toUInt32(void *ptr);

        /**
         * @brief Casts the stored pointer to a float pointer
         * @param ptr the raw pointer to cast
         * @return float pointer if exists data, otherwise a NULL pointer
        */
        static float *toFloat(void *ptr);

        /**
         * @brief Casts the stored pointer to a int64_t pointer
         * @param ptr the raw pointer to cast
         * @return int64_t pointer if exists data, otherwise a NULL pointer
        */
        static int64_t *toInt64(void *ptr);

        /**
         * @brief Casts the stored pointer to a uint64_t pointer
         * @param ptr the raw pointer to cast
         * @return uint64_t pointer if exists data, otherwise a NULL pointer
        */
        static uint64_t *toUInt64(void *ptr);

        /**
         * @brief Casts the stored pointer to a double pointer
         * @param ptr the raw pointer to cast
         * @return double pointer if exists data, otherwise a NULL pointer
        */
        static double *toDouble(void *ptr);

    private:
        void *dataPtr;
        uint64_t sz;
};

#endif // SKARRAYCAST_H
