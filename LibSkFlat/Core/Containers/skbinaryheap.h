#ifndef SKBINARYHEAP_H
#define SKBINARYHEAP_H

#include "abstract/skabstractcontainer.h"
#include "skbinarynode.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

template <typename K, typename T>
class SPECIALK SkBinaryHeap extends SkAbstractContainer
{
    public:
        SkBinaryHeap();
        SkBinaryHeap(const SkBinaryHeap<K, T> &source);
        ~SkBinaryHeap();

        bool add(const K &key, const T &val, CStr *label=nullptr);
        bool add(const K &key, CStr *label=nullptr);

        bool isEmpty() override;
        uint64_t size() override;
        uint64_t count();
        void clear() override;

        void visitToList(SkAbstractList<SkBinaryNode<K, T> *> &v, SkTreeVisitMode mode=SkTreeVisitMode::BTV_DEPTH_SIMMETRIC);
        SkBinaryTreeVisit<K, T> *iterator();

    private:
        SkBinaryNode<K, T> *r;
        uint64_t sz;
        K nullKey;
        T nullVal;

        K &nk() {return nullKey = K();}
        T &nv() {return nullVal = T();}
};

template <typename K, typename T>
SkBinaryHeap<K, T>::SkBinaryHeap()
{
    sz = 0;
    r = new SkBinaryNode<K, T>();
}

template <typename K, typename T>
SkBinaryHeap<K, T>::SkBinaryHeap(const SkBinaryHeap<K, T> &source)
{
    sz = 0;
    r = new SkBinaryNode<K, T>();

    SkBinaryTreeVisit<K, T> *itr = source.iterator();

    while(itr->next())
        add(itr->item().key(), itr->item().value(), itr->label());

    delete itr;
}

template <typename K, typename T>
SkBinaryHeap<K, T>::~SkBinaryHeap()
{
    clear();
    delete r;
}

template <typename K, typename T>
bool SkBinaryHeap<K, T>::add(const K &key, const T &val, CStr *label)
{
    return false;
}

template <typename K, typename T>
bool SkBinaryHeap<K, T>::add(const K &key, CStr *label)
{
    return false;
}

template <typename K, typename T>
bool SkBinaryHeap<K, T>::isEmpty()
{
    return (sz == 0);
}

template <typename K, typename T>
uint64_t SkBinaryHeap<K, T>::size()
{
    return sz*(sizeof(K)+sizeof(T));
}

template <typename K, typename T>
uint64_t SkBinaryHeap<K, T>::count()
{
    return sz;
}

template <typename K, typename T>
void SkBinaryHeap<K, T>::clear()
{
    SkList<SkBinaryNode<K, T> *> l;
    visitToList(l);

    SkAbstractListIterator<SkBinaryNode<K, T> *> *itr = l.iterator();

    while(itr->next())
        delete itr->item();

    delete itr;

    sz = 0;
    r = new SkBinaryNode<K, T>();
}

template <typename K, typename T>
void SkBinaryHeap<K, T>::visitToList(SkAbstractList<SkBinaryNode<K, T> *> &l, SkTreeVisitMode mode)
{
    SkStack<SkBinaryNode<K, T> *> s;
    SkBinaryNode<K, T> *p = r;

    if (mode == SkTreeVisitMode::BTV_DEPTH_SIMMETRIC)
    {
        do
        {
            while (!p->isNil())
            {
                s.push(p);
                p = p->leftChild();
            }

            if (!s.isEmpty())
            {
                p = s.pop();

                if (!p->isNil())
                    l << p;

                p = p->rightChild();
            }

        } while (!s.isEmpty() || !p->isNil());
    }
}

template <typename K, typename T>
SkBinaryTreeVisit<K, T> *SkBinaryHeap<K, T>::iterator()
{
    return new SkBinaryTreeVisit<K, T>(r);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKBINARYHEAP_H
