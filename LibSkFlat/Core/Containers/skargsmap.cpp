#include "skargsmap.h"

#if defined(ENABLE_SKAPP)
    #include "Core/System/Filesystem/skfsutils.h"
#endif

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkArgsMap);

DeclareMeth_INSTANCE_RET(SkArgsMap, contains, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkArgsMap, insert, SkVariant&, Arg_CStr, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkArgsMap, insertMap, *Arg_Custom(SkArgsMap))
DeclareMeth_INSTANCE_RET(SkArgsMap, value, SkVariant&, Arg_CStr)
//DeclareMeth_INSTANCE_RET(SkArgsMap, key, SkString&, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkArgsMap, toString, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET(SkArgsMap, fromString, bool, Arg_CStr)

#if defined(ENABLE_SKAPP)
    DeclareMeth_INSTANCE_RET(SkArgsMap, fromFile, bool, Arg_CStr)
#endif

DeclareMeth_INSTANCE_VOID(SkArgsMap, clear)
DeclareMeth_INSTANCE_RET(SkArgsMap, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkArgsMap, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkArgsMap, size, uint64_t)

SetupClassWrapper(SkArgsMap)
{
    SetClassSuper(SkArgsMap, SkFlatObject);

    AddMeth_INSTANCE_RET(SkArgsMap, contains);
    AddMeth_INSTANCE_RET(SkArgsMap, insert);
    AddMeth_INSTANCE_VOID(SkArgsMap, insertMap);
    AddMeth_INSTANCE_RET(SkArgsMap, value);
    //AddMeth_INSTANCE_RET(SkArgsMap, key);
    AddMeth_INSTANCE_RET(SkArgsMap, toString);
    AddMeth_INSTANCE_RET(SkArgsMap, fromString);

#if defined(ENABLE_SKAPP)
    AddMeth_INSTANCE_RET(SkArgsMap, fromFile);
#endif

    AddMeth_INSTANCE_VOID(SkArgsMap, clear);
    AddMeth_INSTANCE_RET(SkArgsMap, isEmpty);
    AddMeth_INSTANCE_RET(SkArgsMap, count);
    AddMeth_INSTANCE_RET(SkArgsMap, size);
}

// // // // // // // // // // // // // // // // // // // // //

SkArgsMap::SkArgsMap()
{
    CreateClassWrapper(SkArgsMap);
}

SkArgsMap::SkArgsMap(const SkArgsMap &source)/* : SkMap<SkString, SkVariant>()*/
{
    CreateClassWrapper(SkArgsMap);
    subMap = source.subMap;
}

SkArgsMap::SkArgsMap(SkMap<SkString, SkVariant> &source)
{
    subMap = source;
}

/*bool SkArgsMap::contains(CStr *label)
{
    SkString s(label);
    return subMap.contains(s);
}*/

bool SkArgsMap::contains(const SkString &label)
{
    return subMap.contains(label);
}

SkVariant &SkArgsMap::insert(const SkString &label, const SkVariant &val)
{
    subMap[label] = val;
    return subMap[label];
}

SkVariant &SkArgsMap::insert(const SkString &label)
{
    const SkVariant &v = subMap.nv();
    subMap[label] = v;
    return subMap[label];
}

void SkArgsMap::insertMap(SkArgsMap &m)
{
    //subMap.add(m.subMap);
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
        subMap[itr->item().key()] = itr->item().value();

    delete itr;
}

SkVariant &SkArgsMap::value(const SkString &label)
{
    if (contains(label))
        return subMap.value(label);

    const SkVariant &v = subMap.nv();
    subMap.add(label, v);
    return subMap.value(label);
}

void SkArgsMap::values(SkAbstractList<SkVariant> &valuesList)
{
    subMap.values(valuesList);
}

/*SkString &SkArgsMap::key(const SkVariant &val)
{
    return subMap.key(val);
}*/

void SkArgsMap::keys(SkAbstractList<SkString> &keysList)
{
    subMap.keys(keysList);
}

bool SkArgsMap::toString(SkString &json, bool indented, uint8_t spaces)
{
    SkVariant v(subMap);
    uint16_t level = 0;
    return SkVariant::toJson(v, json, indented, level, spaces);
}

bool SkArgsMap::fromString(CStr *json)
{
    SkVariant v;
    bool ok = v.fromJson(json);
    v.copyToMap(subMap);
    return ok;
}

#if defined(ENABLE_SKAPP)
bool SkArgsMap::fromFile(CStr *filePath)
{
    SkString s;

    if (!SkFsUtils::readTEXT(filePath, s))
        return false;

    return fromString(s.c_str());
}

bool SkArgsMap::fromFile(SkString &filePath)
{
    return fromFile(filePath.c_str());
}
#endif

void SkArgsMap::add(const SkString &key, const SkVariant &value)
{
    subMap.add(key, value);
}

SkPair<SkString, SkVariant> SkArgsMap::remove(const SkString &key)
{
    return subMap.remove(key);
}

/*bool SkArgsMap::contains(const SkString &key)
{
    return subMap.contains(key);
}*/

/*void SkArgsMap::sort(SkOrderAlgo algo)
{
    subMap.sort(algo);
}*/

void SkArgsMap::reverse()
{
    subMap.reverse();
}

void SkArgsMap::clear()
{
    subMap.clear();
}

bool SkArgsMap::isEmpty()
{
    return subMap.isEmpty();
}

uint64_t SkArgsMap::count()
{
    return subMap.count();
}

uint64_t SkArgsMap::countValues(SkVariant &v)
{
    return subMap.countValues(v);
}

uint64_t SkArgsMap::size()
{
    return subMap.size();
}

void SkArgsMap::swap(SkArgsMap &other)
{
    subMap.swap(other.subMap);
}

typename SkVector<SkPair<SkString, SkVariant>>::SkIterator *SkArgsMap::iterator()
{
    return subMap.iterator();
}

/*SkVariant &SkArgsMap::operator [] (CStr *label)
{
    if (contains(label))
        return subMap.value(label);

    return insert(label);
}

SkVariant &SkArgsMap::operator [] (SkString &label)
{
    if (contains(label))
        return subMap.value(label);

    return insert(label.c_str());
}*/

SkArgsMap &SkArgsMap::operator = (const SkArgsMap &source)
{
    subMap = source.subMap;
    return *this;
}

bool SkArgsMap::operator == (const SkArgsMap &operand)
{
    return (subMap == operand.subMap);
}

SkVariant &SkArgsMap::operator [] (const SkString &key)
{
    return subMap[key];
}

SkMap<SkString, SkVariant> &SkArgsMap::getInternalMap()
{
    return subMap;
}

SkVector<SkPair<SkString, SkVariant>> &SkArgsMap::getInternalVector()
{
    return subMap.getInternalVector();
}

#if defined(UMBA)
SkArgsMap::SkArgsMap()
{

}

bool SkArgsMap::contains(CStr *label)
{
    SkString s(label);
    return SkVariantMap::contains(s);
}

bool SkArgsMap::contains(SkString &label)
{
    return SkVariantMap::contains(label);
}

SkVariant &SkArgsMap::insert(CStr *label, const SkVariant val)
{
    return SkVariantMap::insert(label, val);
}

SkVariant &SkArgsMap::insert(SkString &label, const SkVariant val)
{
    return SkVariantMap::insert(label, val);
}

SkVariant &SkArgsMap::insert(const SkVariant label, const SkVariant val)
{
    SkVariant indexVal(label);
    SkString s = indexVal.toString();
    return insert(s, val);
}

SkVariant &SkArgsMap::insert(const SkVariant label)
{
    SkVariant indexVal(label);
    SkString s = indexVal.toString();
    SkVariant v;
    return insert(s, v);
}

void SkArgsMap::insertMap(SkArgsMap &m)
{
    if (m.isEmpty())
        return;

    SkStringList l;
    m.keys(l);

    for(uint64_t i=0; i<l.count(); i++)
        (*this)[l[i]] = m[l[i]];
}

SkVariant &SkArgsMap::value(CStr *label)
{
    return SkVariantMap::value(label);
}

SkVariant &SkArgsMap::value(SkString &label)
{
    if (contains(label))
        return SkVariantMap::value(label);

    else
        return insert(label, SkVariant());
}

bool SkArgsMap::remove(CStr *label)
{
    SkString s(label);
    return SkVariantMap::remove(s);
}

bool SkArgsMap::remove(SkString &label)
{
    return SkVariantMap::remove(label);
}

SkString SkArgsMap::key(const SkVariant val)
{
    SkString s = SkVariantMap::key(val).toString();
    return s;
}

void SkArgsMap::keys(SkStringList &keysList)
{
    if (!keysList.isEmpty())
        keysList.clear();

    for(uint64_t i=0; i<k.count(); i++)
    {
        SkString s = k.at(i).toString();
        keysList.append(s);
    }
}

bool SkArgsMap::toString(SkString &json, bool indented, uint8_t spaces)
{
    SkVariant v(*this);
    uint16_t level = 0;
    return SkVariant::toJson(v, json, indented, level, spaces);
}

bool SkArgsMap::fromString(SkString &json)
{
    SkVariant v;
    bool ok = v.fromJson(json);
    v.toVariantMap(*this);
    return ok;
}

#if defined(SPECIALK_APPLICATION)
bool SkArgsMap::fromFile(CStr *filePath)
{
    SkString s;
    SkFsUtils::readTEXT(filePath, s);
    return fromString(s);
}

bool SkArgsMap::fromFile(SkString &filePath)
{
    return fromFile(filePath.c_str());
}
#endif

SkVariant &SkArgsMap::operator [] (CStr *label)
{
    if (contains(label))
        return SkVariantMap::value(label);

    return insert(label);
}

SkVariant &SkArgsMap::operator [] (SkString &label)
{
    if (contains(label))
        return SkVariantMap::value(label);

    return insert(label);
}
#endif
