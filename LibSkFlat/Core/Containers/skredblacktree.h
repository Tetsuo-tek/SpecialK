#ifndef SKTREE_H
#define SKTREE_H

#include "abstract/skabstractcontainer.h"
#include "skbinarynode.h"
#include "skstring.h"
#include "skstack.h"
#include "skpair.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkRbColor
{
    RB_BLACK,
    RB_DOUBLEBLACK,
    RB_RED
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

template <typename K, typename T>
class SkRbNode extends SkBinaryNode<K, T>
{
    public:
        SkRbNode<K, T>();

        uint64_t childrenCount() override;

        SkRbNode<K, T> *min();
        SkRbNode<K, T> *max();
        SkRbNode<K, T> *ancestor();
        SkRbNode<K, T> *successor();

        void setDoubleBlack();
        void setBlack();
        void setRed();

        bool isDoubleBlack();
        bool isBlack();
        bool isRed();

        void toggleRedBlack();

        SkRbColor color;
};

template <typename K, typename T>
SkRbNode<K, T>::SkRbNode() : SkBinaryNode<K, T>()
{
    color = RB_BLACK;
}

template <typename K, typename T>
uint64_t SkRbNode<K, T>::childrenCount()
{
    return (this->leftChild() && !this->leftChild()->isNil())
            + (this->rightChild() && !this->rightChild()->isNil());
}

template <typename K, typename T>
SkRbNode<K, T> *SkRbNode<K, T>::min()
{
    SkBinaryNode<K, T> *n = this;

    while(!n->isNil())
    {
        if (n->leftChild()->isNil())
            return static_cast<SkRbNode<K, T> *>(n);

        n = n->leftChild();
    }

    return static_cast<SkRbNode<K, T> *>(n);
}

template <typename K, typename T>
SkRbNode<K, T> *SkRbNode<K, T>::max()
{
    SkBinaryNode<K, T> *n = this;

    while(!n->isNil())
    {
        if (n->rightChild()->isNil())
            return static_cast<SkRbNode<K, T> *>(n);

        n = n->rightChild();
    }

    return static_cast<SkRbNode<K, T> *>(n);
}

template <typename K, typename T>
SkRbNode<K, T> *SkRbNode<K, T>::ancestor()
{
    if (!this->leftChild()->isNil())
        return static_cast<SkRbNode<K, T> *>(this->leftChild())->max();

    else
    {
        SkBinaryNode<K, T> *n = this;

        while(n)
        {
            if (!n->hasFather())
                return nullptr;

            if (n->isRightChild())
                return n;

            n = static_cast<SkRbNode<K, T> *>(n->father());
        }
    }

    return nullptr;
}

template <typename K, typename T>
SkRbNode<K, T> *SkRbNode<K, T>::successor()
{
    if (!this->rightChild()->isNil())
        return static_cast<SkRbNode<K, T> *>(this->rightChild())->min();

    else
    {
        SkRbNode<K, T> *n = this;

        while(n)
        {
            if (!n->hasFather())
                return nullptr;

            if (n->isLeftChild())
                return n;

            n = static_cast<SkRbNode<K, T> *>(n->father());
        }
    }

    return nullptr;
}

template <typename K, typename T>
bool SkRbNode<K, T>::isDoubleBlack()
{
    return (color == RB_DOUBLEBLACK);
}

template <typename K, typename T>
bool SkRbNode<K, T>::isBlack()
{
    return (color == RB_BLACK);
}

template <typename K, typename T>
bool SkRbNode<K, T>::isRed()
{
    return (color == RB_RED);
}

template <typename K, typename T>
void SkRbNode<K, T>::setDoubleBlack()
{
    color = RB_DOUBLEBLACK;
}

template <typename K, typename T>
void SkRbNode<K, T>::setBlack()
{
    color = RB_BLACK;
}

template <typename K, typename T>
void SkRbNode<K, T>::setRed()
{
    color = RB_RED;
}

template <typename K, typename T>
void SkRbNode<K, T>::toggleRedBlack()
{
    if (color == RB_BLACK)
        color = RB_RED;

    else
        color = RB_BLACK;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

// - It WILL not admit repetition (it is a set)

template <typename K, typename T>
class SPECIALK SkRedBlackTree extends SkAbstractContainer
{
    public:
        SkRedBlackTree();
        SkRedBlackTree(const SkRedBlackTree<K, T> &source);
        ~SkRedBlackTree();

        bool add(const K &key, const T &val, CStr *label=nullptr);
        bool add(const K &key, CStr *label=nullptr);
        bool remove(const K &key);
        bool contains(const K &key);
        T &value(const K &key);
        K &min();
        K &max();
        bool isEmpty() override;
        uint64_t size() override;
        uint64_t count();
        void clear() override;

        SkRedBlackTree<K, T> &operator =  (const SkRedBlackTree<K, T> &source);
        bool                 operator  == (const SkRedBlackTree<K, T> &operand);

        void visitToList(SkAbstractList<SkBinaryNode<K, T> *> &v, SkBinaryTreeVisitMode mode=SkBinaryTreeVisitMode::BTV_DEPTH_SIMMETRIC);
        SkBinaryTreeVisit<K, T> *iterator();

        void swap(SkRedBlackTree<K, T> &other);

    private:
        SkRbNode<K, T> *r;
        uint64_t sz;
        K nullKey;
        T nullVal;

        K &nk() {return nullKey = K();}
        T &nv() {return nullVal = T();}

        void prepareNode(SkRbNode<K, T> *n, CStr *label, SkRbColor color);
        void setNode(SkRbNode<K, T> *n, const K &key, const T &val, CStr *label, SkRbColor color);
        void setNode(SkRbNode<K, T> *n, const K &key, CStr *label, SkRbColor color);

        SkRbNode<K, T> *search(const K &key);

        void abscendingFixup(SkRbNode<K, T> *z);
        void fixup(SkRbNode<K, T> *n);
};

template <typename K, typename T>
SkRedBlackTree<K, T>::SkRedBlackTree()
{
    sz = 0;
    r = new SkRbNode<K, T>();
    //dbg << r;
}

template <typename K, typename T>
SkRedBlackTree<K, T>::SkRedBlackTree(const SkRedBlackTree<K, T> &source)
{
    sz = 0;
    r = new SkRbNode<K, T>();
    //dbg << r;

    SkBinaryTreeVisit<K, T> *itr = source.iterator();

    while(itr->next())
        add(itr->item().key(), itr->item().value(), itr->label());

    delete itr;
}

template <typename K, typename T>
SkRedBlackTree<K, T>::~SkRedBlackTree()
{
    clear();
    delete r;
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::prepareNode(SkRbNode<K, T> *n, CStr *label, SkRbColor color)
{
    if (n->isNil())
    {
        //nil leafs

        n->setLeftChild(new SkRbNode<K, T>());
        n->leftChild()->setFather(n);

        n->setRightChild(new SkRbNode<K, T>());
        n->rightChild()->setFather(n);

        //if it is not nil color will not be changed
        n->color = color;
    }

    if (label)
        n->setLabel(label);
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::setNode(SkRbNode<K, T> *n, const K &key, const T &val, CStr *label, SkRbColor color)
{
    prepareNode(n, label, color);
    n->set(key, val);
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::setNode(SkRbNode<K, T> *n, const K &key, CStr *label, SkRbColor color)
{
    prepareNode(n, label, color);
    n->set(key);
}

template <typename K, typename T>
SkRbNode<K, T> *SkRedBlackTree<K, T>::search(const K &key)
{
    if (isEmpty())
        return r;

    SkBinaryNode<K, T> *n = r;

    //IT IS A CHECK ON SIZE VALUE AND ROOT CONSISTENCY
    if (n->isNil())
        cout << "!!!!! SIZE ERROR !!!!!\n";

    while(!n->isNil())
    {
        if (key < n->key())
            n = n->leftChild();

        else if (key > n->key())
            n = n->rightChild();

        else if (key == n->key())
            return static_cast<SkRbNode<K, T> *>(n);
    }

    return static_cast<SkRbNode<K, T> *>(n);
}

//MUST REMOVE bool RETURN
template <typename K, typename T>
bool SkRedBlackTree<K, T>::add(const K &key, const T &val, CStr *label)
{
    if (r->isNil())
        setNode(r, key, val, label, RB_BLACK);

    else
    {
        SkRbNode<K, T> *z = search(key);

        if (!z->isNil())
        {
            z->setValue(val);
            return true;
        }

        setNode(z, key, val, label, RB_RED);
        abscendingFixup(z);
    }

    sz++;
    return true;
}

template <typename K, typename T>
bool SkRedBlackTree<K, T>::add(const K &key, CStr *label)
{
    return add(key, nv(), label);
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::abscendingFixup(SkRbNode<K, T> *z)
{
    bool isDoubleBlack = z->isDoubleBlack();

    if (isDoubleBlack)
        z->setBlack();

    while(z)
    {
        fixup(z);

        if (z->father())
            z = static_cast<SkRbNode<K, T> *>(z->father());

        else
            z = nullptr;

        if (z && isDoubleBlack && z->isRed())
        {
            z->setBlack();
            isDoubleBlack = false;
        }
    }
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::fixup(SkRbNode<K, T> *n)
{
    SkRbNode<K, T> *p = static_cast<SkRbNode<K, T> *>(n->father());
    SkRbNode<K, T> *g = static_cast<SkRbNode<K, T> *>(n->grandFather());
    SkRbNode<K, T> *u = static_cast<SkRbNode<K, T> *>(n->uncle());

    if (n->color == RB_RED && p->color == RB_RED)
    {
        //case 1
        if (u->color == RB_RED)
        {
            //cout << n->label << " - violation: case 1\n";

            p->toggleRedBlack();
            u->toggleRedBlack();
            g->toggleRedBlack();
        }

        else if (u->color == RB_BLACK)
        {
            //case 2
            if (n->isRightChild() && p->isLeftChild())
            {
                //cout << n->label << " - violation: case 2a\n";

                g->setLeftChild(n);
                n->setFather(g);
                p->setRightChild(n->leftChild());
                p->rightChild()->setFather(p);
                n->setLeftChild(p);
                p->setFather(n);

                fixup(p);
            }

            else if (n->isLeftChild() && p->isRightChild())
            {
                //cout << n->label << " - violation: case 2b\n";

                g->setRightChild(n);
                n->setFather(g);
                p->setLeftChild(n->rightChild());
                p->leftChild()->setFather(p);
                n->setRightChild(p);
                p->setFather(n);

                fixup(p);
            }

            //case 3
            else if (n->isLeftChild() && p->isLeftChild())
            {
                //cout << n->label << " - violation: case 3a\n";

                if (g->father())
                {
                    if (g->isRightChild())
                        static_cast<SkRbNode<K, T> *>(g->father())->setRightChild(p);
                    else
                        static_cast<SkRbNode<K, T> *>(g->father())->setLeftChild(p);
                }

                else
                    r = p;

                p->setFather(g->father());
                g->setFather(p);
                g->setLeftChild(p->rightChild());
                g->leftChild()->setFather(g);
                p->setRightChild(g);

                p->toggleRedBlack();
                g->toggleRedBlack();
            }

            else if (n->isRightChild() && p->isRightChild())
            {
                //cout << n->label << " - violation: case 3b\n";

                if (g->father())
                {
                    if (g->isRightChild())
                        static_cast<SkRbNode<K, T> *>(g->father())->setRightChild(p);
                    else
                        static_cast<SkRbNode<K, T> *>(g->father())->setLeftChild(p);
                }

                else
                    r = p;

                p->setFather(g->father());
                g->setFather(p);
                g->setRightChild(p->leftChild());
                g->rightChild()->setFather(g);
                p->setLeftChild(g);

                p->toggleRedBlack();
                g->toggleRedBlack();
            }
        }

        if (r->color == RB_RED)
            r->toggleRedBlack();
    }
}

template <typename K, typename T>
bool SkRedBlackTree<K, T>::remove(const K &key)
{
    if (isEmpty())
        return false;

    SkRbNode<K, T> *z = search(key);

    if (z->isNil())
        return false;

    if (z->childrenCount() == 2)
    {
        //cout << "!!!!!!!!!!!!!!! TWO CHILD\n";

        //Here successor CANNOT be NIL
        SkRbNode<K, T> *successor = z->successor();
        //z->swap(successor);

        K k = z->key();
        T v = z->value();

        z->set(successor->key(), successor->value());
        successor->set(k, v);

        z = successor;
        // GOT TO NEXT CASE (ONE CHILD OR NO CHILD)
    }

    if (z->childrenCount() == 1)
    {
        //cout << "!!!!!!!!!!!!!!! ONE CHILD\n";
        SkBinaryNode<K, T> *child = nullptr;

        if (z->rightChild()->isNil())
        {
            child = z->leftChild();
            delete z->rightChild();
        }

        else if (z->leftChild()->isNil())
        {
            child = z->rightChild();
            delete z->leftChild();
        }

        SkBinaryNode<K, T> *father = nullptr;

        if (z->hasFather())
        {
            father = static_cast<SkBinaryNode<K, T> *>(z->father());

            if (z->isLeftChild())
                father->setLeftChild(child);

            else if (z->isRightChild())
                father->setRightChild(child);
        }

        child->setFather(father);

        if (z == r)
        {
            r = static_cast<SkRbNode<K, T> *>(child);

            if (r->isRed())
                r->setBlack();
        }

        else
        {
            if (z->isRed())//NO POSSIBLE VIOLATIONs
            {}

            else if (z->isBlack())
            {
                SkRbNode<K, T> *ch = static_cast<SkRbNode<K, T> *>(child);

                if (ch->isRed())
                    ch->setBlack();

                else if (ch->isBlack())
                    ch->setDoubleBlack();

                abscendingFixup(ch);
            }
        }
    }

    else if (z->childrenCount() == 0)
    {
        //cout << "!!!!!!!!!!!!!!! NO CHILD\n";

        // CHILDREN ARE ALL NIL
        // arbitrary delete right NIL and substitute z with its left NIL
        delete z->rightChild();

        if (z->hasFather())
        {
            SkBinaryNode<K, T> *fth = static_cast<SkBinaryNode<K, T> *>(z->father());

            if (z->isLeftChild())
                fth->setLeftChild(z->leftChild());

            else if (z->isRightChild())
                fth->setRightChild(z->leftChild());

            z->leftChild()->setFather(fth);

            if (z->isRed()){}//NO POSSIBLE VIOLATIONs

            else if (z->isBlack())
            {
                /*SkRbNode<K, T> *father = static_cast<SkRbNode<K, T> *>(fth);
                SkRbNode<K, T> *brother = static_cast<SkRbNode<K, T> *>(z->brother());

                if (father->isBlack())
                    father->setDoubleBlack();

                if (!brother->isNil() && brother->isBlack())
                    brother->setRed();

                abscendingFixup(f);*/
            }
        }

        else
            delete z->leftChild();

        if (z == r)
            r = new SkRbNode<K, T>();
    }

    delete z;

    sz--;
    return true;
}

template <typename K, typename T>
bool SkRedBlackTree<K, T>::contains(const K &key)
{
    return !search(key)->isNil();
}

template <typename K, typename T>
T &SkRedBlackTree<K, T>::value(const K &key)
{
    SkRbNode<K, T> *z = search(key);

    if (z->isNil())
        return nv();

    return z->value();
}

template <typename K, typename T>
K &SkRedBlackTree<K, T>::min()
{
    SkRbNode<K, T> *z = r->min();

    /*if (!z)
        return nk();*/

    return z->key();
}

template <typename K, typename T>
K &SkRedBlackTree<K, T>::max()
{
    SkRbNode<K, T> *z = r->max();

    /*if (!z)
        return nk();*/

    return z->key();
}

template <typename K, typename T>
bool SkRedBlackTree<K, T>::isEmpty()
{
    return (sz == 0);
}

template <typename K, typename T>
uint64_t SkRedBlackTree<K, T>::size()
{
    return sz*(sizeof(K)+sizeof(T));
}

template <typename K, typename T>
uint64_t SkRedBlackTree<K, T>::count()
{
    return sz;
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::clear()
{
    if (isEmpty())
        return;

    SkList<SkBinaryNode<K, T> *> l;
    visitToList(l);

    SkAbstractListIterator<SkBinaryNode<K, T> *> *itr = l.iterator();

    while(itr->next())
    {
        SkBinaryNode<K, T> *n = itr->item();

        if (n->rightChild()->isNil())
            delete n->rightChild();

        if (n->leftChild()->isNil())
            delete n->leftChild();
    }

    itr->reset();

    while(itr->next())
        delete itr->item();

    delete itr;

    sz = 0;
    r = new SkRbNode<K, T>();
}

template <typename K, typename T>
SkRedBlackTree<K, T> &SkRedBlackTree<K, T>::operator = (const SkRedBlackTree<K, T> &source)
{
    clear();

    SkRedBlackTree<K, T> t;
    SkRbNode<K, T> *emptyRoot = t.r;

    t.r = source.r;
    t.sz = source.sz;

    SkBinaryTreeVisit<K, T> *itr = t.iterator();

    while(itr->next())
        add(itr->item().key(), itr->item().value(), itr->label());

    delete itr;

    t.r = emptyRoot;
    t.sz = 0;

    return *this;
}

template <typename K, typename T>
bool SkRedBlackTree<K, T>::operator == (const SkRedBlackTree<K, T> &operand)
{
    if (sz != operand.sz)
        return false;

    SkRedBlackTree<K, T> t;
    SkRbNode<K, T> *emptyRoot = t.r;

    t.r = operand.r;
    t.sz = operand.sz;

    SkBinaryTreeVisit<K, T> *itr1 = iterator();
    SkBinaryTreeVisit<K, T> *itr2 = t.iterator();

    while(itr1->next() && itr2->next())
    {
        if (itr1->item().key() != itr2->item().key()
                || itr1->item().value() != itr2->item().value()
                || itr1->label() != itr2->label())
        {
            delete itr1;
            delete itr2;

            t.r = emptyRoot;
            t.sz = 0;
            return false;
        }
    }

    delete itr1;
    delete itr2;

    t.r = emptyRoot;
    t.sz = 0;
    return true;
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::visitToList(SkAbstractList<SkBinaryNode<K, T> *> &l, SkBinaryTreeVisitMode mode)
{
    SkBinaryTreeVisit<K, T> v(r);
    v.setVisitMode(mode);
    v.visitToList(l);
}

template <typename K, typename T>
SkBinaryTreeVisit<K, T> *SkRedBlackTree<K, T>::iterator()
{
    return new SkBinaryTreeVisit<K, T>(r);
}

template <typename K, typename T>
void SkRedBlackTree<K, T>::swap(SkRedBlackTree<K, T> &other)
{
    SkRbNode<K, T> *root = r;
    uint64_t tot = sz;

    r = other.r;
    sz = other.sz;

    other.r = root;
    other.sz = tot;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
    RB props:
    1. Every node is either RED or BLACK;
    2. Every nil node is BLACK
    3. Every RED node has two BLACK child nodes
    4. Every path from root down to a leaf has the same number if BLACK nodes
    5. The root node is always BLACK

    Violations cases:
    1. uncle(x) are RED:
       invert colors of parent(x), uncle(x) and grandparent(x)

    2. uncle(x) is BLACK (and goes to case 3):
       a)                                        b)
                x is DX-ch                                 x is SX-ch
         parent(x)is SX-ch                          parent(x)is DX-ch
         SX_ROTATE(parent)                          DX_ROTATE(parent)
         parent(x) becomes SX-ch of x               parent(x) becomes DX-ch of x
         x becomes SX-ch of grandparent(x)          x becomes DX-ch of grandparent(x)

    3. uncle(x) is BLACK:
       a)                                         b)
                x is SX-ch                                 x is DX-ch
         parent(x)is SX-ch                          parent(x)is DX-ch
         DX_ROTATE(grandparent)                     SX_ROTATE(grandparent)
         grandparent(x) becomes DX-ch of parent(x)  grandparent(x) becomes SX-ch of parent(x)
                   >>invert colors of parent(x) and grandparent(x)<<
*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKTREE_H
