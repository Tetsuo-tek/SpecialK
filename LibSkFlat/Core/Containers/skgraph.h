#ifndef SKGRAPH_H
#define SKGRAPH_H

#include "Core/Containers/skargsmap.h"

//EFFICIENCY IS EVERYTHING
//THIS CLASS WILL NOT CONTROL ANYTHING OF INSERTED DATA - DUPLICATION AND
//NOT-CONSISTENT STATES ARE ALLOWED HERE - YOU HAVE BE WARNED!

template <typename T>
class SPECIALK SkGraphNode extends SkFlatObject
{
    public:
        SkGraphNode(CStr *label)
        {
            setObjectName(label);
            //value IS NOT initialized
        }

        SkGraphNode(CStr *label, const T &val)
        {
            setObjectName(label);
            value = val;
        }

        void setValue(const T &val)
        {
            value = val;
        }

        T &getValue()
        {
            return value;
        }

        void linkTo(SkGraphNode *node, double weight=0.)
        {
            neighborshood[node] = weight;
        }

        void unlinkFrom(SkGraphNode *node)
        {
            neighborshood.remove(node);
        }

        void neighbors(SkList<SkGraphNode *> &m)
        {
            neighborshood.keys(m);
        }

        void neighbors(SkMap<SkGraphNode *, double> &m)
        {
            m = neighborshood;
        }

        bool operator == (const SkGraphNode<T> *operand)
        {
            return (operand->value == value);
        }

        bool operator != (const SkGraphNode<T> *operand)
        {
            return (operand->value != value);
        }

        bool operator < (const SkGraphNode<T> *operand)
        {
            return (operand->value < value);
        }

        bool operator <= (const SkGraphNode<T> *operand)
        {
            return (operand->value <= value);
        }

        bool operator > (const SkGraphNode<T> *operand)
        {
            return (operand->value > value);
        }

        bool operator >= (const SkGraphNode<T> *operand)
        {
            return (operand->value >= value);
        }

    protected:
        T value;
        SkMap<SkGraphNode *, double> neighborshood;
};

#endif // SKGRAPH_H
