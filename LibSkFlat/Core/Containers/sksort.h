#ifndef SKSORT_H
#define SKSORT_H

#include "Core/Object/skflatobject.h"

template <typename T>
class Comparator extends SkFlatObject
{
    public:
        Comparator(){}

        virtual bool lt(T &a, T &b) = 0;
        virtual bool le(T &a, T &b) = 0;
        virtual bool gt(T &a, T &b) = 0;
        virtual bool ge(T &a, T &b) = 0;

        //1(>) -1(<) 0 (==)
        int compare(T &a, T &b)
        {
            if (gt(a, b))
                return 1;

            if (lt(a, b))
                return -1;

            return 0;
        }
};

typedef enum
{
    OA_BUBBLE,
    OA_SELECTION,
    OA_INSERTION,
    OA_MERGE,
    OA_QUICK,
    OA_HEAP
} SkOrderAlgo;

template <typename T>
class SkSort extends SkFlatObject
{
    public:
        SkSort();
        ~SkSort();

        //default is QUICKSORT, abscending=true, c=&base
        void setup(SkOrderAlgo a, Comparator<T> *c=nullptr);

        void sort(T *a, uint64_t n);

    private:
        class BaseComparator extends Comparator<T>
        {
            public:
                bool lt(T &a, T &b);
                bool le(T &a, T &b);
                bool gt(T &a, T &b);
                bool ge(T &a, T &b);
        };

        Comparator<T> *comparator;
        BaseComparator base;
        SkOrderAlgo algo;
        T *array;
        uint64_t count;

        //Bubble, Selection and Insertion are here ONLY for didactic purposes
        //complexity is Θ(n * n)
        void bubble();
        void selection();
        void insertion();//Best case is O(n)

        //complexity is Θ(n * log n)
        void merge();
        void mergeSub(T *aux, int low, int mid, int high);

        void quick();//Worst case is O(n * n)
        int partition(int left, int right);

        void heap();
        void heapify();
};

template <typename T>
SkSort<T>::SkSort()
{
    algo = SkOrderAlgo::OA_QUICK;
    comparator = &base;
}

template <typename T>
SkSort<T>::~SkSort()
{}

template <typename T>
void SkSort<T>::setup(SkOrderAlgo a, Comparator<T> *c)
{
    algo = a;

    if (c)
        comparator = c;
}

template <typename T>
void SkSort<T>::sort(T *a, uint64_t n)
{
    array = a;
    count = n;

    if (algo == OA_BUBBLE)
        bubble();

    else if (algo == OA_SELECTION)
        selection();

    else if (algo == OA_INSERTION)
        insertion();

    else if (algo == OA_MERGE)
        merge();

    else if (algo == OA_QUICK)
        quick();

    else if (algo == OA_HEAP)
        heap();
}

template <typename T>
bool SkSort<T>::BaseComparator::lt(T &a, T &b)
{
    return (a<b);
}

template <typename T>
bool SkSort<T>::BaseComparator::le(T &a, T &b)
{
    return (a<=b);
}

template <typename T>
bool SkSort<T>::BaseComparator::gt(T &a, T &b)
{
    return (a>b);
}

template <typename T>
bool SkSort<T>::BaseComparator::ge(T &a, T &b)
{
    return (a>=b);
}

template <typename T>
void SkSort<T>::bubble()
{
    int finalSwapPos = count-1;
    int swapPos = 0;

    while(finalSwapPos>0)
    {
        swapPos = 0;

        for(int i=0; i<finalSwapPos; i++)
        {
            if (comparator->gt(array[i], array[i+1]))
            {
                ::swap(array[i], array[i+1]);
                swapPos = i;
            }
        }

        finalSwapPos = swapPos;
    }
}

template <typename T>
void SkSort<T>::selection()
{
    int tempCount = count;

    // Make x[0] ... x[i] sorted and <= x[i + 1] ... x[x.length - 1]:
    for(int i=0; i<tempCount-1; i++)
    {
        int pos = i;

        for(int k=i+1; k<tempCount; k++)
            if (comparator->lt(array[k], array[pos]))
                pos = k;

        ::swap(array[i], array[pos]);
    }
}

template <typename T>
void SkSort<T>::insertion()
{
    int tempCount = count;

    for (int i=1; i < tempCount; i++)
        for (int k = i; k>0 && comparator->gt(array[k -1], array[k]); k--)
           ::swap(array[k], array[k-1]);
}

template <typename T>
void SkSort<T>::merge()
{
    int tempCount = count;

    T *aux = new T[tempCount];

    //mempcy on smarts
    for(int i=0; i<tempCount; i++)
        aux[i] = array[i];

    int i = 0;
    int dim = 1;
    int t1 = 0;
    int t2 = 0;

    for (; dim < tempCount; dim = dim+dim)
        for (i=0; i < tempCount-dim; i = i+dim+dim)
        {
            t1 = i+dim+dim-1;
            t2 = tempCount-1;

            mergeSub( aux, i, i+dim-1, t1<t2 ? t1 : t2);
        }

    delete [] aux;
}

template <typename T>
void SkSort<T>::mergeSub(T *aux, int low, int mid, int high)
{
    int i = 0;
    int j = 0;
    int k = 0;

    for (i=mid+1; i>low; i--)
        aux[i-1]=array[i-1];

    for (j=mid; j<high; j++)
        aux[high+mid-j] = array[j+1];// copy in inverse order

    for (k=low; k<=high; k++)// l'indice k scorre l'array x[]
    {
        if (comparator->lt(aux[j], aux[i]))
           array[k]=aux[j--];
       else
           array[k]=aux[i++];
    }
}

template <typename T>
void SkSort<T>::quick()
{
    // si inserisce nello stack prima il piu' grande dei sottoarray,
    // ossia si elabora prima l'array piu' piccolo
    // => massima dim dello stack: O lg(#el.da ordinare)

    int tempCount = count;

    int *stack = new int[2*tempCount];
    int stackTop = 0;

    stack[stackTop++] = 0;
    stack[stackTop++] = tempCount-1;

    while(stackTop > 0)
    {
        int right = stack[--stackTop];
        int left = stack[--stackTop];

        while(right > left)
        {
            int i = partition(left, right);

            if (right-i-1 < i-1-left)
            {
               if(i-1 > left)
               {
                   stack[stackTop++] = left;
                   stack[stackTop++] = i-1;
               }

               left = i+1;
            }

            else
            {
                if(right > i+1)
                {
                    stack[stackTop++] = i+1;
                    stack[stackTop++] = right;
                }

                right = i-1;
            }
        }
    }

    delete [] stack;
}

template <typename T>
int SkSort<T>::partition(int left, int right)
{
    if (left == right)
        return left;

    int i = left - 1;
    int j = right;

    while(true)
    {
        // find item on left to swap; a[right] acts as sentinel
        while (comparator->lt(array[++i], array[right])) ;

        // find item on right to swap
        while (comparator->lt(array[right], array[--j]))
            if (j == left)// don't go out-of-bounds
                break;

        // check if pointers cross
        if (i >= j)
            break;

        // swap two elements into place
        ::swap(array[i], array[j]);
    }

    // swap with partition element
    ::swap(array[i], array[right]);
    return i;
}

template <typename T>
void SkSort<T>::heap()
{
    heapify();

    int tempCount = count;

    for (int i=tempCount-1; i>0; i--)
    {
        // swap value of first indexed
        // with last indexed
        ::swap(array[0], array[i]);

        // maintaining heap property
        // after each swapping
        int j = 0;
        int index = 0;

        do
        {
            index = (2 * j + 1);

            if (index >= tempCount-1)
                break;

            // if left child is smaller than
            // right child point index variable
            // to right child
            if (comparator->lt(array[index], array[index + 1]) && index < (i - 1))
                index++;

            // if parent is smaller than child
            // then swapping parent with child
            // having higher value
            if (comparator->lt(array[j], array[index]) && index < i)
                ::swap(array[j], array[index]);

            j = index;

        } while (index < i);
    }
}
template <typename T>
void SkSort<T>::heapify()
{
    // function build Max Heap where value
    // of each child is always smaller
    // than value of their parent

    int tempCount = count;

    for (int i=1; i<tempCount; i++)
    {
        // if child is bigger than parent
        //if (array[i] > array[(i - 1) / 2])
        if (comparator->gt(array[i], array[(i-1)/2]))
        {
            int j = i;

            // swap child and parent until
            // parent is smaller
            while (comparator->gt(array[j], array[(j-1)/2]))
            {
                ::swap(array[j], array[(j-1)/2]);
                j = (j-1)/2;
            }
        }
    }
}

#endif // SKSORT_H
