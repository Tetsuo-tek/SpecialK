/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKRINGBUFFER_H
#define SKRINGBUFFER_H

#include "Core/Containers/skstring.h"

//COULD BE DYNAMIC
#define SK_DEFAULTCAPACITY              1000000

class SkDataBuffer;
class SkObject;

//IT IS A FIFO-RING AND IS TH-SAFE
//COULD BE AN SkObject BUT IT IS BETTER AS SkFlatObject

/**
  * @brief SkRingBuffer
  * It is a fixed FIFO ring-buffer; it is thread-safe
*/

class SPECIALK SkRingBuffer extends SkFlatObject
{
    public:
        /**
         * @brief Contructor for a starting size; all bytes is put to 0
         * @param capacity the size to allocate; default is SK_DEFAULTCAPACITY
         */
        SkRingBuffer(uint64_t capacity=SK_DEFAULTCAPACITY);

        /**
         * @brief Destructor; it will destroy data inside
        */
        ~SkRingBuffer();

        /**
         * @brief Change capacity of the internal data
         * @param capacity the new value
         */
        void setCapacity(uint64_t capacity);

        /**
         * @brief Gets the capacity of the internal data
         * @return the size of internal data pointer
         */
        uint64_t capacity();

        void setMaxGrow(uint64_t maxGrowingSize);
        uint64_t maxGrowingSize();

        /**
         * @brief Sets a fixed size to use in congiuntion with getCanonicalBuffer meth;
         * it is only a facilities for situations where the size to get is always the same
         * @param size the canonical size
         */
        void setCanonicalSize(uint64_t size);

        /**
         * @brief Gets the canonical size
         * @return the canonical size
         */
        uint64_t getCanonicalSize();

        /**
         * @brief Check if internal stored size has reached the canonical size
         * value; in this case it is possible get data with getCanonicalBuffer meth
         * @return true if the stored size is greater or equal than the canonical size
         */
        bool isCanonicalSizeReached();

        /**
         * @brief Appends new data to the buffer, using constant char data pointer as input
         * @param data constant char data pointer to use as input
         * @param size the size of the input to append
         * @return true if size is valid, otherwise false
         */
        bool addData(CStr *data, uint64_t size);

        /**
         * @brief Appends new data to the buffer, using char data pointer as input
         * @param data char data pointer to use as input
         * @param size the size of the input to append
         * @return true if size is valid, otherwise false
         */
        bool addData(char *data, uint64_t size);

        /**
         * @brief Appends a string to the buffer
         * @param s the string to append
         * @warning the string terminator will be considered
         */
        void addString(const SkString &s);

        //THESE METHs W/R PRIMITIVE C TYPES BINARY-RAW-CASTED TO/FROM LE-BYTEARRAY
        //add* METHs PRODUCE DATA
        //get* METHs CONSUME DATA
        //peek* METHs DOES NOT CONSUME DATA

        /**
         * @brief Appends a uint8_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addUInt8(uint8_t val);

        /**
         * @brief Gets a uint8_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint8_t getUInt8();

        /**
         * @brief Peek a uint8_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint8_t peekUInt8();

        /**
         * @brief Appends a int8_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addInt8(int8_t val);

        /**
         * @brief Gets a int8_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int8_t getInt8();

        /**
         * @brief Peeks a int8_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int8_t peekInt8();

        /**
         * @brief Appends a uint16_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addUInt16(uint16_t val);

        /**
         * @brief Gets a uint16_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint16_t getUInt16();

        /**
         * @brief Peeks a uint16_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint16_t peekUInt16();

        /**
         * @brief Appends a int16_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addInt16(int16_t val);

        /**
         * @brief Gets a int16_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int16_t getInt16();

        /**
         * @brief Peeks a int16_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int16_t peekInt16();

        /**
         * @brief Appends a uint32_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addUInt32(uint32_t val);

        /**
         * @brief Gets a uint32_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint32_t getUInt32();

        /**
         * @brief Peeks a uint32_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint32_t peekUInt32();

        /**
         * @brief Appends a int32_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addInt32(int32_t val);

        /**
         * @brief Gets a int32_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int32_t getInt32();

        /**
         * @brief Peeks a int32_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int32_t peekInt32();

        /**
         * @brief Appends a uint64_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addUInt64(uint64_t val);

        /**
         * @brief Gets a uint64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint64_t getUInt64();

        /**
         * @brief Peeks a uint64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint64_t peekUInt64();

        /**
         * @brief Appends a int64_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addInt64(int64_t val);

        /**
         * @brief Gets a int64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int64_t getInt64();

        /**
         * @brief Peeks a int64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int64_t peekInt64();

        /**
         * @brief Appends a uint64_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addSizeT(uint64_t val);

        /**
         * @brief Gets a uint64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint64_t getSizeT();

        /**
         * @brief Peeks a uint64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        uint64_t peekSizeT();

        /**
         * @brief Appends a int64_t value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addSSizeT(int64_t val);

        /**
         * @brief Gets a int64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int64_t getSSizeT();

        /**
         * @brief Peeks a int64_t value in Little-Endian mode from the buffer
         * @return the value
         */
        int64_t peekSSizeT();

        /**
         * @brief Appends a float value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addFloat(float val);

        /**
         * @brief Gets a float value in Little-Endian mode from the buffer
         * @return the value
         */
        float getFloat();

        /**
         * @brief Peeks a float value in Little-Endian mode from the buffer
         * @return the value
         */
        float peekFloat();

        /**
         * @brief Appends a double value in Little-Endian mode to the buffer
         * @param val the value to append
         */
        void addDouble(double val);

        /**
         * @brief Gets a double value in Little-Endian mode from the buffer
         * @return the value
         */
        double getDouble();

        /**
         * @brief Peeks a double value in Little-Endian mode from the buffer
         * @return the value
         */
        double peekDouble();

        /**
         * @brief Flushes a segment from the buffer without read it
         * @param sz the size to flush; if it is 0 all the buffer is flushed,
         * as making clear
         */
        void flush(uint64_t sz=0);


        //IT WILL RETURN A NEW POINTER WITH A COPY OF THE CONTENTS OF size BYTES,
        //SO YOU CAN (AND MUST) MANAGE ITS LIFE
        //size IS USED ALSO AS RETURN HERE (IT COULD BE CHANGED)
        //size WILL BE FILLED WITH THE EFFECTIVE CONSUMED STORED SIZE)
        /**
         * @brief Gets data from buffer; this method allocate a pointer containing
         * a copy of requested data; this buffer does not manage the pointer life;
         * it is better to use other meths getCustomBuffer and getCanonicalBuffer
         * if you don't want manage the pointer life
         * @param size the length of getting data
         * @param peekData if true data read will not consumed
         * @return a new allocated char pointer of data, or a NULL pointer if the
         * requested size is not valid
         */
        char *getBuffer(uint64_t &size, bool peekData=false);

        /**
         * @brief Gets a buffer copy from buffer
         * @param data the char data pointer where to copy requested data
         * @param size the length of getting data
         * @param peekData if true data read will not consumed
         * @return true if the requested size is valid, otherwise false
         */
        bool getCustomBuffer(char *data, uint64_t &size, bool peekData=false);

        /**
         * @brief Gets a buffer copy from buffer of canonical size bytes
         * @param data the char data pointer where to copy requested data
         * @param peekData if true data read will not consumed
         * @return true if the buffer length ha reached the canonical size, otherwise false
         */
        bool getCanonicalBuffer(char *data, bool peekData=false);

        /**
         * @brief Copy of data from buffer to another
         * @param other the target of the copy
         * @param size  the length of getting data; if it is 0 it will copy all buffer
         * @param peekData if true data read will not consumed
         * @return true if the buffer length ha reached the canonical size, otherwise false
         */
        bool copyTo(SkRingBuffer *other, uint64_t size=0, bool peekData=false);

        /**
         * @brief Copy of data from buffer to another
         * @param other the target of the copy
         * @param size  the length of getting data; if it is 0 it will copy all buffer
         * @param peekData if true data read will not consumed
         * @return true if the buffer length ha reached the canonical size, otherwise false
         */
        bool copyTo(SkDataBuffer &other, uint64_t size=0, bool peekData=false);

        /**
         * @brief Copy of data from buffer to a string
         * @param other the target of the copy
         * @param size  the length of getting data; if it is 0 it will copy all buffer
         * @param peekData if true data read will not consumed
         * @return true if the buffer length ha reached the canonical size, otherwise false
         */
        bool copyTo(SkString &other, uint64_t size=0, bool peekData=false);//len MUST NOT INCLUDE TERMINATOR '\0'

        //THE PASSING SIZE MUST NOT INCLUDE TERMINATOR '\0'
        //bool fillString(SkString &s, uint64_t len);

        /**
         * @brief Assign-operator from a ring-buffer
         * @param buf the ring-buffer pointer to assign
         * @return a reference to this buffer
         */
        SkRingBuffer &operator =  (SkRingBuffer *buf);

        /**
         * @brief Assign-operator from another buffer
         * @param buf the buffer reference to assign
         * @return a reference to this buffer
         */
        SkRingBuffer &operator =  (SkDataBuffer &buf);

        /**
         * @brief Append-operator of a ring-buffer
         * @param buf the ring-buffer pointer to append
         * @return a reference to this buffer
         */
        SkRingBuffer &operator << (SkRingBuffer *buf);

        /**
         * @brief Append-operator of another buffer
         * @param buffer the other buffer reference to append
         * @return a reference to this buffer
         */
        SkRingBuffer &operator << (SkDataBuffer &buf);

        /**
         * @brief Copy-operator to a ring-buffer
         * @param buf the ring-buffer pointer where to make copy
         * @return a reference to this buffer
         */
        SkRingBuffer &operator >> (SkRingBuffer *buf);

        /**
         * @brief Copy-operator to another buffer
         * @param buf the other buffer reference where to make copy
         * @return a reference to this buffer
         */
        SkRingBuffer &operator >> (SkDataBuffer &buf);

        /**
         * @brief Checks it the buffer is empty
         * @return true if it is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Gets the size of data stored in the buffer
         * @return the length of data stored
         */
        uint64_t size();

        //void swap(SkRingBuffer &);

        /**
         * @brief Clears the buffer contents
         */
        void clear();

        /**
         * @brief Gets the costant char pointer of data
         * @warning remember this is a ring-buffer, so making this could have
         * no sense
         * @return the costant char data pointer
         */
        CStr *getRawDataConst();

        /**
         * @brief Gets the char pointer of data
         * @warning remember this is a ring-buffer, so making this could have
         * no sense
         * @return the char data pointer
         */
        char *getRawData();

        /**
         * @brief Gets the position of contents start; reading contents, will
         * start from here
         * @return the position
         */
        const uint64_t &getFromCursor();

        /**
         * @brief Gets the position of contents end; adding contents, will proceed
         * from here
         * @return the position
         */
        const uint64_t &getToCursor();

    private:
        SkMutex bufferEx;
        char *buffer;

        uint64_t canonicalSize;
        uint64_t from;
        uint64_t to;
        uint64_t max;
        uint64_t maxGrowSz;

        uint64_t calculateSize();

        //IF data ARG IS NULL IT WILL ALLOCATE AND RETURN A NEW POINTER (AFTER, YOU WILL MANAGE ITS LIFE)
        //IF THERE IS data ARRAY VALID IT WILL FILL WITH REQUESTED DATA, ALSO RETURNING IT
        //IF RETURN IS NULL IT MEANs THAT THE OPERATION IS FAILED FOR SOME REASON
        //IF size REQUESTED IS MORE THAN EFFECTIVE STORED SIZE THE OPERATION  WILL FAIL
        //IF size REQUESTED IS 0 IT WILL GET ALL DATA IF THERE ARE BYTES
        //IF EFFECTIVE STORED SIZE IS 0 THE OPERATION WILL FAIL
        char *getData(char *data, uint64_t &size, bool peekData);

    protected:
        virtual void onDataLoaded(uint64_t &){}
        virtual void onDataGotten(uint64_t &){}
        virtual void onEmptyGotten(){}
};

#endif // SKRINGBUFFER_H
