#include "skstring.h"

#include "Core/Containers/skvariant.h"

/*DeclareWrapper(SkString);

DeclareMeth_STATIC_RET(SkString, buildFilled, static SkString, Arg_Char, Arg_UInt64 )
DeclareMeth_INSTANCE_VOID(SkString, concat, Arg_Char)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 1, Arg_CStr)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 2, Arg_UInt8)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 3, Arg_UInt8, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 4, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 5, Arg_Int, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 6, Arg_UInt)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 7, Arg_UInt, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 8, *Arg_Data(long))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 9, *Arg_Data(long), Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 10, *Arg_Data(unsigned long))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 11, *Arg_Data(unsigned long), Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 12, Arg_Int64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 13, Arg_Int64, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 14, Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 15, Arg_UInt64, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 16, Arg_Float, Arg_Int, Arg_Bool)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 17, Arg_Double)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 18, Arg_Double, Arg_Int)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 19, Arg_Double, Arg_Int, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkString, prepend, Arg_CStr)

DeclareMeth_INSTANCE_RET(SkString, startsWith, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkString, endsWith, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkString, equals, bool, Arg_CStr)

DeclareMeth_STATIC_RET(SkString, compare, static bool, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkString, isEmpty, bool)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, isEmpty, 1, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkString, trim, SkString&)
DeclareMeth_INSTANCE_RET(SkString, simplify, SkString&)

DeclareMeth_INSTANCE_VOID(SkString, split, Arg_Char, *Arg_Custom(SkAbstractList<SkString>))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 1, Arg_Char, *Arg_Custom(SkAbstractList<SkString>), Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 2, Arg_CStr, *Arg_Custom(SkAbstractList<SkString>))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 3, Arg_CStr, *Arg_Custom(SkAbstractList<SkString>), Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkString, parsePair, Arg_CStr, *Arg_Custom(SkAbstractList<SkString>))

DeclareMeth_INSTANCE_RET(SkString, toLowerCase, SkString&)
DeclareMeth_INSTANCE_RET(SkString, toUpperCase, SkString&)
DeclareMeth_INSTANCE_RET(SkString, capitalize, SkString&)
DeclareMeth_INSTANCE_RET(SkString, contains, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkString, indexOf, int64_t, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkString, indexOf, 1, int64_t, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkString, charAt, char, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkString, replace, SkString&, Arg_CStr, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkString, replace, 1, SkString&, Arg_CStr, Arg_CStr, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkString, chop, SkString&, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkString, toInt, int)
//DeclareMeth_INSTANCE_RET(SkString, toLong, long)
DeclareMeth_INSTANCE_RET(SkString, toLongLong, int64_t)
DeclareMeth_INSTANCE_RET(SkString, toFloat, float)
DeclareMeth_INSTANCE_RET(SkString, toDouble, double)
DeclareMeth_INSTANCE_VOID(SkString, toCharArray, Arg_Data(char))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkString, toCharArray, 1, Arg_Data(char), Arg_UInt64)

DeclareMeth_STATIC_RET(SkString, number, SkString, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 1, SkString, Arg_Int, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 2, SkString, Arg_UInt)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 3, SkString, Arg_UInt, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 4, SkString, *Arg_Data(long))
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 5, SkString, *Arg_Data(long), Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 6, SkString, *Arg_Data(unsigned long))
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 7, SkString, *Arg_Data(unsigned long), Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 8, SkString, Arg_Int64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 9, SkString, Arg_Int64, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 10, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 11, SkString, Arg_UInt64, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 12, SkString, Arg_Float)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 13, SkString, Arg_Float, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 14, SkString, Arg_Float, Arg_Int, Arg_Bool)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 15, SkString, Arg_Double)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 16, SkString, Arg_Double, Arg_Int)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, number, 17, SkString, Arg_Double, Arg_Int, Arg_Bool)

DeclareMeth_STATIC_RET(SkString, formatBytesSize, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, formatBytesSize, 1, SkString, Arg_UInt64, Arg_Bool)
DeclareMeth_STATIC_RET(SkString, formatBytesPerSec, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, formatBytesPerSec, 1, SkString, Arg_UInt64, Arg_Bool)
DeclareMeth_STATIC_RET(SkString, formatBitsSize, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, formatBitsSize, 1, SkString, Arg_UInt64, Arg_Bool)
DeclareMeth_STATIC_RET(SkString, formatBitsPerSec, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, formatBitsPerSec, 1, SkString, Arg_UInt64, Arg_Bool)
DeclareMeth_STATIC_RET(SkString, formatHz, SkString, Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkString, formatHz, 1, SkString, Arg_UInt64, Arg_Bool)
DeclareMeth_STATIC_RET(SkString, formatValuesWithUdm, SkString, Arg_UInt64, Arg_Bool, Arg_CStr, Arg_UInt64)

SetupClassWrapper(SkString)
{
    SetClassSuper(SkString, SkFlatObject);
    AddMeth_STATIC_RET(SkString, buildFilled);
    AddMeth_INSTANCE_VOID(SkString, concat);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 2);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 3);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 4);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 5);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 6);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 7);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 8);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 9);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 10);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 11);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 12);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 13);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 14);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 15);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 16);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 17);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 18);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, concat, 19);
    AddMeth_INSTANCE_VOID(SkString, prepend);

    AddMeth_INSTANCE_RET(SkString, startsWith);
    AddMeth_INSTANCE_RET(SkString, endsWith);
    AddMeth_INSTANCE_RET(SkString, equals);

    AddMeth_STATIC_RET(SkString, compare);
    AddMeth_INSTANCE_RET(SkString, isEmpty);
    AddMeth_STATIC_RET_OVERLOAD(SkString, isEmpty, 1);
    AddMeth_INSTANCE_RET(SkString, trim);
    AddMeth_INSTANCE_RET(SkString, simplify);

    AddMeth_INSTANCE_VOID(SkString, split);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 2);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, split, 3);
    AddMeth_INSTANCE_VOID(SkString, parsePair);

    AddMeth_INSTANCE_RET(SkString, toLowerCase);
    AddMeth_INSTANCE_RET(SkString, toUpperCase);
    AddMeth_INSTANCE_RET(SkString, capitalize);
    AddMeth_INSTANCE_RET(SkString, contains);
    AddMeth_INSTANCE_RET(SkString, indexOf);
    AddMeth_INSTANCE_RET_OVERLOAD(SkString, indexOf, 1);
    AddMeth_INSTANCE_RET(SkString, charAt);
    AddMeth_INSTANCE_RET(SkString, replace);
    AddMeth_INSTANCE_RET_OVERLOAD(SkString, replace, 1);
    AddMeth_INSTANCE_RET(SkString, chop);
    AddMeth_INSTANCE_RET(SkString, toInt);
    //AddMeth_INSTANCE_RET(SkString, toLong);
    AddMeth_INSTANCE_RET(SkString, toLongLong);
    AddMeth_INSTANCE_RET(SkString, toFloat);
    AddMeth_INSTANCE_RET(SkString, toDouble);
    AddMeth_INSTANCE_VOID(SkString, toCharArray);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkString, toCharArray, 1);

    AddMeth_STATIC_RET(SkString, number);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 1);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 2);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 3);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 4);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 5);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 6);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 7);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 8);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 9);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 10);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 11);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 12);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 13);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 14);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 15);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 16);
    AddMeth_STATIC_RET_OVERLOAD(SkString, number, 17);

    AddMeth_STATIC_RET(SkString, formatBytesSize);
    AddMeth_STATIC_RET_OVERLOAD(SkString, formatBytesSize, 1);
    AddMeth_STATIC_RET(SkString, formatBytesPerSec);
    AddMeth_STATIC_RET_OVERLOAD(SkString, formatBytesPerSec, 1);
    AddMeth_STATIC_RET(SkString, formatBitsSize);
    AddMeth_STATIC_RET_OVERLOAD(SkString, formatBitsSize, 1);
    AddMeth_STATIC_RET(SkString, formatBitsPerSec);
    AddMeth_STATIC_RET_OVERLOAD(SkString, formatBitsPerSec, 1);
    AddMeth_STATIC_RET(SkString, formatHz);
    AddMeth_STATIC_RET_OVERLOAD(SkString, formatHz, 1);
    AddMeth_STATIC_RET(SkString, formatValuesWithUdm);
}*/

SkString::SkString(char c) : string()
{
    //CreateClassWrapper(SkString);
    concat(c);
}

SkString::SkString(CStr *cstr) : string(cstr)
{
    //CreateClassWrapper(SkString);
}

SkString::SkString(char *cstr, int size): string(cstr, size)
{
    //CreateClassWrapper(SkString);
}

SkString::SkString(const SkString &str) : string(str)
{
    //CreateClassWrapper(SkString);
}

SkString::SkString(const string &str) : string(str)
{
    //CreateClassWrapper(SkString);
}

SkString::SkString(unsigned char value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(int value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(unsigned int value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(long value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(unsigned long value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(long long value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(unsigned long long value, int base) : string()
{
    //CreateClassWrapper(SkString);
    concat(value, base);
}

SkString::SkString(float value, int precision, bool fixed)
{
    //CreateClassWrapper(SkString);
    concat(value, precision, fixed);
}

SkString::SkString(double value, int precision, bool fixed)  : string()
{
    //CreateClassWrapper(SkString);
    concat(value, precision, fixed);
}

SkString &SkString::operator = (CStr *cstr)
{
    clear();
    append(cstr);
    return *this;
}

SkString &SkString::operator = (SkString str)
{
    clear();
    append(str.c_str());
    return *this;
}

SkString &SkString::operator = (string str)
{
    clear();
    append(str.c_str());
    return *this;
}

bool SkString::operator == (CStr *cstr) {return equals(cstr);}
bool SkString::operator == (SkString &str) {return equals(str);}

bool SkString::operator < (CStr *cstr) {return (string::compare(cstr) < 0);}
bool SkString::operator < (SkString &str) {return (string::compare(str) < 0);}

bool SkString::operator <= (CStr *cstr) {return (string::compare(cstr) <= 0);}
bool SkString::operator <= (SkString &str) {return (string::compare(str) < 0);}

bool SkString::operator > (CStr *cstr) {return (string::compare(cstr) > 0);}
bool SkString::operator > (SkString &str) {return (string::compare(str) > 0);}

bool SkString::operator >= (CStr *cstr) {return (string::compare(cstr) >= 0);}
bool SkString::operator >= (SkString &str) {return (string::compare(str) >= 0);}

SkString &SkString::operator += (char c) {concat(c); return (*this);}
SkString &SkString::operator += (CStr *cstr) {append(cstr); return (*this);}
SkString &SkString::operator += (unsigned char value) {concat(value); return (*this);}
SkString &SkString::operator += (int value) {concat(value); return (*this);}
SkString &SkString::operator += (unsigned int value) {concat(value); return (*this);}
SkString &SkString::operator += (long value) {concat(value); return (*this);}
SkString &SkString::operator += (unsigned long value) {concat(value); return (*this);}
SkString &SkString::operator += (long long value) {concat(value); return (*this);}
SkString &SkString::operator += (unsigned long long value) {concat(value); return (*this);}
SkString &SkString::operator += (double value) {concat(value); return (*this);}
SkString &SkString::operator += (SkString str) {concat(str); return (*this);}

void SkString::prepareStreamToAddInt(stringstream &stream, int base)
{
    if (base == DEC)
        stream << std::dec;

    else if (base == HEX)
        stream << std::hex;
}

SkString &SkString::fill(char c, uint64_t count)
{
    append(count, c);
    return *this;
}

SkString SkString::buildFilled(char c, uint64_t count)
{
    SkString s;
    s.fill(c, count);
    return s;
}

void SkString::concat(char c) {append(1, c);}
void SkString::concat(CStr *cstr) {append(cstr);}

void SkString::concat(unsigned char value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << static_cast<int> (value);
    append(stream.str());
}

void SkString::concat(int value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

void SkString::concat(unsigned int value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

void SkString::concat(long value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

void SkString::concat(unsigned long value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

void SkString::concat(long long value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

void SkString::concat(unsigned long long value, int base)
{
    stringstream stream;
    prepareStreamToAddInt(stream, base);
    stream << value;
    append(stream.str());
}

/*void SkString::concat(float value, int precision, bool fixed)
{
    concat(static_cast<double>(value), precision, fixed);
}*/

void SkString::concat(double value, int precision, bool fixed)
{
    stringstream stream;

    if (fixed)
        stream << std::fixed;

    /*else
        stream << std::defaultfloat;*/

    stream << setprecision(precision) << value;
    append(stream.str());
}

void SkString::concat(SkString str) {append(str);}

void SkString::prepend(CStr *cstr)
{
    insert(0, cstr);
}

bool SkString::startsWith(CStr *cstr)
{
    uint64_t l = strlen(cstr);

    if (length() < l)
        return false;

    return string::compare(0, l, cstr) == 0;
}

bool SkString::endsWith(CStr *cstr)
{
    if (length() < strlen(cstr))
        return false;

    uint64_t sz = strlen(cstr);
    return (memcmp(cstr, &c_str()[size() - strlen(cstr)], sz) == 0);
}

bool SkString::equals(CStr *cstr) {return (string::compare(cstr) == 0);}
bool SkString::equals(SkString &str) {return (string::compare(str) == 0);}
bool SkString::equals(string &str) {return (string::compare(str) == 0);}

bool SkString::compare(CStr *str1, CStr* str2, uint64_t sz)
{
    if (!str1 || !str2)
        return false;

    if (!sz)
    {
        sz = strlen(str1);

        if (sz != strlen(str2))
            return false;
    }

    return (memcmp(str1, str2, sz) == 0);
}

bool SkString::isEmpty() {return empty();}
bool SkString::isEmpty(CStr *cstr)
{
    return (cstr == nullptr || !strlen(cstr));
}

uint64_t SkString::size() {return SkString::size(c_str());}
uint64_t SkString::size(CStr *cstr)
{
    if (!cstr)
        return 0;

    return strlen(cstr);
}

uint64_t SkString::length() const {return SkString::length(c_str());}

#include <locale>
#include <codecvt>

uint64_t SkString::length(CStr *cstr)
{
    uint64_t sz = SkString::size(cstr);

    if (sz == 0)
        return 0;

    char d[sz+1];

    uint64_t i = 0;
    uint64_t t = 0;

    for(; i<sz; i++)
        if (cstr[i] == 0x1B)
            for(; i<sz-1 && !::isalpha(cstr[i]); i++);
        else
            d[t++] = cstr[i];

    d[t] = '\0';

    wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    wstring s;
    s = converter.from_bytes(d);
    return s.length();
}

SkString &SkString::trim()
{
    if (isEmpty())
        return *this;

    uint64_t pos = find_first_not_of(" \t\r\n\v\f\b\a");
    erase(0, pos);//prefixing spaces

    //COULD HAVE BECOME EMPTY
    if (isEmpty())
        return *this;

    pos = find_last_not_of(" \t\r\n\v\f\b\a");
    resize(pos+1);//suffixing spaces
    return *this;
}

SkString &SkString::simplify()
{
    if (isEmpty())
        return *this;

    //char *check = ;
    SkString s;
    uint64_t sz = size();
    CStr *str = c_str();

    for(uint64_t i=0; i<sz; i++)
    {
        const char &c = str[i];

        if (isspace(c))
        {
            if (i < sz-1 && !isspace(str[i+1]))
                s.concat(c);
        }

        else
            s.concat(c);
    }

    *this = s;
    return *this;
}

void SkString::split(char sep, SkAbstractList<SkString> &splitted, uint64_t max)
{
    if (sep == '\0')
        return;

    char separator[] = {sep, '\0'};

    uint64_t counter = 0;
    char *s = strdup(c_str());
    char *token = nullptr;
    char *savePtr = s;

    SkString a;

    while ((token = strtok_r(savePtr, separator, &savePtr)))
    {
        if (max && counter >= max)
            break;

        a = token;
        splitted << a;
        counter++;
    }

    free(s);
}

void SkString::split(CStr *sep, SkAbstractList<SkString> &splitted, uint64_t max)
{
    if (SkString::isEmpty(sep))
        return;

    uint64_t counter = 0;
    uint64_t start = 0U;
    uint64_t end = 0;
    uint64_t sepSize = strlen(sep);

    SkString s;

    while(end < size())
    {
        if (max && counter >= max)
            break;

        end = find(sep, start);
        s = substr(start, end - start);
        splitted << s;
        start = end + sepSize;
        counter++;
    }
}

#include "skstringlist.h"

void SkString::parsePair(CStr *sep, SkAbstractList<SkString> &splitted)
{
    split(sep, splitted, 2);

    SkString s;

    if (splitted.count() < 2)
        splitted.append(s);

    if (splitted.count() < 1)
        splitted.append(s);
}

SkString &SkString::toLowerCase()
{
    transform(begin(), end(), begin(), ::tolower);
    return *this;
}

SkString &SkString::toUpperCase()
{
    transform(begin(), end(), begin(), ::toupper);
    return *this;
}

SkString &SkString::capitalize()
{
    if (!isEmpty())
        transform(begin(), begin()+1, begin(), ::toupper);

    return *this;
}

bool SkString::contains(CStr *cstr)
{
    return (indexOf(cstr, 0) > -1);
}

int64_t SkString::indexOf(CStr *cstr, uint64_t from)
{
    uint64_t pos = this->find(cstr, from);

    if (pos != string::npos)
        return static_cast<int64_t>(pos);

    return -1;
}

char SkString::charAt(uint64_t index)
{
    if (index < size())
        return c_str()[index];

    return '\0';
}

SkString &SkString::replace(CStr *cstr, CStr *substitute, uint64_t from, bool onlyFirstOccurrence)
{
    size_t pos = from;
    uint64_t cstrLen = strlen(cstr);
    uint64_t substituteLen = strlen(substitute);

    while ((pos = find(cstr, pos)) != npos)
    {
        dynamic_cast<std::string *>(this)->replace(pos, cstrLen, substitute);

        if (onlyFirstOccurrence)
            break;

        pos += substituteLen;
    }

    return *this;
}

SkString &SkString::chop(uint64_t count)
{
    if (!count || size() <= count)
        return *this;

    uint64_t choppedSize = size()-count;
    char *s = new char[choppedSize+1];
    memcpy(s, c_str(), choppedSize);
    s[choppedSize] = '\0';

    *this = s;
    return *this;
}

int SkString::toInt()
{
    return atoi(c_str());
}

long SkString::toLong()
{
    return atol(c_str());
}

long long SkString::toLongLong()
{
    return atoll(c_str());
}

float SkString::toFloat()
{
    return toDouble();//static_cast<float>(toDouble());
}

double SkString::toDouble()
{
    setlocale(LC_ALL, "C");
    double v = atof(c_str());
    setlocale(LC_ALL, "");
    return v;
}

void SkString::toCharArray(char *data, uint64_t len)
{
    if (isEmpty())
        return;

    if (len == 0 || len > size())
        len = size();

    memcpy(data, c_str(), len);
}

SkString SkString::number(int value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(unsigned int value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(long value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(unsigned long value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(long long value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(unsigned long long value, int base)
{
    return SkString(value, base);
}

SkString SkString::number(float value, int precision, bool fixed)
{
    return SkString(value, precision, fixed);
}

SkString SkString::number(double value, int precision, bool fixed)
{
    return SkString(value, precision, fixed);
}

/*SkString SkString::localTime(time_t n)
{
    char ltbuf[256];
    struct tm *tm = localtime((const time_t *) &n);
    strftime(ltbuf, sizeof(ltbuf), "%c", tm);
    return SkString((CStr *ltbuf);
}*/

SkString SkString::formatBytesSize(uint64_t bytes, bool onlyLittleValue)
{
    return formatValuesWithUdm(bytes, onlyLittleValue, "B", 1024);
}

SkString SkString::formatBytesPerSec(uint64_t speed, bool onlyLittleValue)
{
    return formatValuesWithUdm(speed, onlyLittleValue, "B/s", 1024);
}

SkString SkString::formatBitsSize(uint64_t bits, bool onlyLittleValue)
{
    return formatValuesWithUdm(bits, onlyLittleValue, "b", 1024);
}

SkString SkString::formatBitsPerSec(uint64_t speed, bool onlyLittleValue)
{
    return formatValuesWithUdm(speed, onlyLittleValue, "b/s", 1024);
}

SkString SkString::formatHz(uint64_t frequency, bool onlyLittleValue)
{
    return formatValuesWithUdm(frequency, onlyLittleValue, "Hz", 1000);
}

SkString SkString::formatValuesWithUdm(uint64_t value,
                                       bool onlyLittleValue,
                                       CStr *udm,
                                       uint64_t multKilo)
{
    SkString formatted;

    if (value > 1000)
    {
        if (!onlyLittleValue)
        {
            formatted += SkString::number(value);
            formatted += " ";
            formatted += udm;
            formatted += " (";
        }

        if (value > multKilo*multKilo*multKilo*multKilo)
        {
            formatted += SkString::number(value / (multKilo*multKilo*multKilo*multKilo));
            formatted += " T";
            formatted += udm;
        }

        else if (value > multKilo*multKilo*multKilo)
        {
            formatted += SkString::number(value / (multKilo*multKilo*multKilo));
            formatted += " G";
        }

        else if (value > multKilo*multKilo)
        {
            formatted += SkString::number(value / (multKilo*multKilo));
            formatted += " M";
        }

        else if (value > multKilo)
        {
            formatted += SkString::number(value / multKilo);
            formatted += " k";
        }

        else
        {
            if (onlyLittleValue)
            {
                formatted += SkString::number(value);
                formatted += " ";
            }
        }

        formatted += udm;

        if (!onlyLittleValue)
            formatted += ")";
    }

    else
    {
        formatted += SkString::number(value);
        formatted += " ";
        formatted += udm;
    }

    return formatted;
}

/*int utf8_strlen(const string& str)
{
    int c,i,ix,q;
    for (q=0, i=0, ix=str.length(); i < ix; i++, q++)
    {
        c = (unsigned char) str[i];
        if      (c>=0   && c<=127) i+=0;
        else if ((c & 0xE0) == 0xC0) i+=1;
        else if ((c & 0xF0) == 0xE0) i+=2;
        else if ((c & 0xF8) == 0xF0) i+=3;
        //else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
        //else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else return 0;//invalid utf8
    }
    return q;
}*/
