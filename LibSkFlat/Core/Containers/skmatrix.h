#ifndef SKMATRIX_H
#define SKMATRIX_H

#include "skarray.h"

template <typename T>
class SPECIALK SkMatrix extends SkAbstractContainer
{
    public:
        SkMatrix(uint64_t cols, uint64_t rows);
        SkMatrix(const SkMatrix<T> &source);
        SkMatrix();

        bool setCell(uint64_t colID, uint64_t rowID, T &e);
        bool setRow(uint64_t rowID, const T *row, uint64_t count);
        bool setColumn(uint64_t colID, const T *col, uint64_t count);

        T &cell(uint64_t colID, uint64_t rowID);
        const T *row(uint64_t rowID);

        bool sum(SkMatrix<T> &other);
        bool sub(SkMatrix<T> &other);
        bool mul(SkMatrix<T> &other);

        bool rotate();
        bool mirror();

        void clear();
        bool isEmpty();
        uint64_t count();
        uint64_t size();

        static uint64_t calculateCellID(uint64_t cols, uint64_t colID, uint64_t rowID);

    private:
        SkArray<T> array;
        uint64_t r;
        uint64_t c;
};

template <typename T>
SkMatrix<T>::SkMatrix(uint64_t cols, uint64_t rows)
{
    r = rows;
    c = cols;
    array.setCapacity(rows*cols);
}

template <typename T>
SkMatrix<T>::SkMatrix(const SkMatrix<T> &source)
{
    r = source.r;
    c = source.c;
    array = source.array;
}

template <typename T>
SkMatrix<T>::SkMatrix()
{
    r = 0;
    c = 0;
}

template <typename T>
uint64_t SkMatrix<T>::calculateCellID(uint64_t cols, uint64_t colID, uint64_t rowID)
{
    return colID+(cols*rowID);
}

template <typename T>
bool SkMatrix<T>::setCell(uint64_t colID, uint64_t rowID, T &e)
{
    if (colID >= c || rowID >= r)
        return false;

    return array.set(e, calculateCellID(c, colID, rowID));
}

template <typename T>
bool SkMatrix<T>::setRow(uint64_t rowID, const T *row, uint64_t count)
{
    if (rowID >= r || count >= r)
        return false;

    return array.set(row, count, calculateCellID(c, 0, rowID));
}

template <typename T>
bool SkMatrix<T>::setColumn(uint64_t colID, const T *col, uint64_t count)
{
    if (colID >= c || count >= c)
        return false;

    for(uint64_t i=0; i<count; i++)
        array.set(col[i], calculateCellID(c, colID, i));

    return true;
}

template <typename T>
T &SkMatrix<T>::cell(uint64_t colID, uint64_t rowID)
{
    return array[calculateCellID(c, colID, rowID)];
}

template <typename T>
const T *SkMatrix<T>::row(uint64_t rowID)
{
    if (rowID >= r)
        return nullptr;

    return array.data()[calculateCellID(c, 0, rowID)];
}

template <typename T>
void SkMatrix<T>::clear()
{
    r = 0;
    c = 0;
    array.clear();
}

template <typename T>
bool SkMatrix<T>::isEmpty()
{
    return array.isEmpty();
}

template <typename T>
uint64_t SkMatrix<T>::count()
{
    return array.count();
}

template <typename T>
uint64_t SkMatrix<T>::size()
{
    return array.size();
}

#endif // SKMATRIX_H
