#ifndef SKTREENODE_H
#define SKTREENODE_H

#include "skstring.h"
#include "skstack.h"
#include "skpair.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

template <typename K, typename T>
class SkTreeNode extends SkFlatObject
{
    public:
        SkTreeNode(uint64_t maxDegree=0);
        ~SkTreeNode();

        void setLabel(CStr *label);
        CStr *label();

        void set(const K &key);
        void set(const K &key, const T &val);
        K& key();

        bool isNil();

        void setValue(const T &val);
        T& value();

        //IF maxDegree is enabled these meths do nothing
        bool addChild(SkTreeNode<K, T> *node);
        bool removeChild(SkTreeNode<K, T> *node);

        void setFather(SkTreeNode<K, T> *f);

        bool hasFather();
        SkTreeNode<K, T> *father();

        bool hasGrandFather();
        SkTreeNode<K, T> *grandFather();

        void hierarchy(SkVector<SkTreeNode<K, T> *> &l);

        SkTreeNode<K, T> **children();
        virtual uint64_t childrenCount();

        bool isRoot();
        bool isLeaf();
        uint64_t degree();
        uint64_t maxDegree();
        uint64_t depth();

        void swap(SkTreeNode<K, T> *n);

        void setVisited(bool v);
        bool isVisited();

    protected:
        SkVector<SkTreeNode<K, T> *> chld;

    private:
        SkString lbl;

        uint64_t d;
        SkTreeNode<K, T> *fth;

        bool visited;

        K k;
        T v;
        bool nullValue;
};

template <typename K, typename T>
SkTreeNode<K, T>::SkTreeNode(uint64_t maxDegree)
{
    d = maxDegree;
    fth = nullptr;
    nullValue = true;
    visited = false;

    if (d)
        for(uint64_t i=0; i<d; i++)
            this->chld << nullptr;
}

template <typename K, typename T>
SkTreeNode<K, T>::~SkTreeNode()
{
}

template <typename K, typename T>
void SkTreeNode<K, T>::setLabel(CStr *label)
{
    lbl = label;
}

template <typename K, typename T>
CStr *SkTreeNode<K, T>::label()
{
    return lbl.c_str();
}

template <typename K, typename T>
void SkTreeNode<K, T>::set(const K &key)
{
    k = key;

    if (nullValue)
        nullValue = false;
}

template <typename K, typename T>
void SkTreeNode<K, T>::set(const K &key, const T &val)
{
    k = key;
    v = val;

    if (nullValue)
        nullValue = false;
}

template <typename K, typename T>
K& SkTreeNode<K, T>::key() {return k;}

template <typename K, typename T>
bool SkTreeNode<K, T>::isNil() {return nullValue;}

template <typename K, typename T>
void SkTreeNode<K, T>::setValue(const T &val) {v = val;}

template <typename K, typename T>
T& SkTreeNode<K, T>::value() {return v;}

template <typename K, typename T>
bool SkTreeNode<K, T>::addChild(SkTreeNode<K, T> *node)
{
    if (d)
        return false;

    if (node->fth == this)
        return false;

    if (node->fth)
        node->fth->removeChild(node);

    node->fth = this;
    chld << node;
    return true;
}

template <typename K, typename T>
bool SkTreeNode<K, T>::removeChild(SkTreeNode<K, T> *node)
{
    if (d)
        return false;

    if (node->fth != this)
        return false;

    node->fth = nullptr;
    return (node == chld.remove(node));
}

template <typename K, typename T>
void SkTreeNode<K, T>::setFather(SkTreeNode<K, T> *f)
{
    fth = f;
}

template <typename K, typename T>
bool SkTreeNode<K, T>::hasFather()
{
    return (father() != nullptr);
}

template <typename K, typename T>
SkTreeNode<K, T> *SkTreeNode<K, T>::father()
{
    return fth;
}

template <typename K, typename T>
bool SkTreeNode<K, T>::hasGrandFather()
{
    return (grandFather() != nullptr);
}

template <typename K, typename T>
SkTreeNode<K, T> *SkTreeNode<K, T>::grandFather()
{
    if (fth)
        return fth->fth;

    return nullptr;
}

template <typename K, typename T>
void SkTreeNode<K, T>::hierarchy(SkVector<SkTreeNode<K, T> *> &l)
{
    SkTreeNode<K, T> *n = this;
    l << n;

    while(n->fth)
    {
        n = n->fth;
        l << n;
    }
}

template <typename K, typename T>
SkTreeNode<K, T> **SkTreeNode<K, T>::children()
{
    return chld.data();
}

template <typename K, typename T>
uint64_t SkTreeNode<K, T>::childrenCount()
{
    return chld.count();
}

template <typename K, typename T>
bool SkTreeNode<K, T>::isRoot()
{
    return (fth == nullptr);
}

template <typename K, typename T>
bool SkTreeNode<K, T>::isLeaf()
{
    return chld.isEmpty();
}

template <typename K, typename T>
uint64_t SkTreeNode<K, T>::degree()
{
    return chld.count();
}

template <typename K, typename T>
uint64_t SkTreeNode<K, T>::maxDegree()
{
    return d;
}

template <typename K, typename T>
uint64_t SkTreeNode<K, T>::depth()
{
    SkTreeNode<K, T> *n = this;
    uint64_t d = 0;

    while(n->fth)
    {
        n = n->fth;
        d++;
    }

    return d;
}

template <typename K, typename T>
void SkTreeNode<K, T>::swap(SkTreeNode<K, T> *n)
{
    SkString label = n->lbl;
    uint64_t degree = n->d;
    SkTreeNode<K, T> *father = n->fth;
    bool isVisited = n->visited;
    K key = n->k;
    T val = n->v;
    bool nv = n->nullValue;
    SkVector<SkTreeNode<K, T> *> children = n->chld;

    n->lbl = lbl;
    n->d = d;
    n->fth = fth;
    n->visited = visited;
    n->k = k;
    n->v = v;
    n->nullValue = nullValue;
    n->chld = chld;

    lbl = label;
    d = degree;
    fth = father;
    visited = isVisited;
    k = key;
    v = val;
    nullValue = nv;
    chld = children;
}

template <typename K, typename T>
void SkTreeNode<K, T>::setVisited(bool v)
{
    visited = v;
}

template <typename K, typename T>
bool SkTreeNode<K, T>::isVisited()
{
    return visited;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkTreeVisitMode
{
    TV_DEPTH_PRE,
    TV_DEPTH_POST,
    TV_BREADTH
};

template <typename K, typename T>
class SPECIALK SkTreeVisit extends SkAbstractIterator<SkPair<K, T>>
{
    public:
        SkTreeVisit(SkTreeNode<K, T> *root, SkTreeVisitMode mode);

        void setVisitMode(SkTreeVisitMode mode);
        bool isValid() override;
        void reset();
        bool hasNext() override;
        bool next() override;
        SkPair<K, T> &item() override;
        void setValue(T *val);
        void setLabel(CStr *label);
        CStr *label();
        bool isNil();
        void visitToList(SkAbstractList<SkTreeNode<K, T> *> &l);
        SkTreeVisit<K, T> &operator ++ () override;

    private:
        SkTreeVisitMode m;
        SkPair<K, T> nullPair;
        SkPair<K, T> currentPair;
        SkTreeNode<K, T> *current;
        SkTreeNode<K, T> *r;
        SkStack<SkTreeNode<K, T> *> s;
        SkQueue<SkTreeNode<K, T> *> q;

        SkPair<K, T> &np() {return nullPair = SkPair<K, T>();}
};

template <typename K, typename T>
SkTreeVisit<K, T>::SkTreeVisit(SkTreeNode<K, T> *root, SkTreeVisitMode mode)
{
    r = root;
    setVisitMode(mode);
}

template <typename K, typename T>
void SkTreeVisit<K, T>::setVisitMode(SkTreeVisitMode mode)
{
    m = mode;
    reset();
}

template <typename K, typename T>
bool SkTreeVisit<K, T>::isValid()
{
    return (current != nullptr);
}

template <typename K, typename T>
void SkTreeVisit<K, T>::reset()
{
    current = nullptr;

    s.clear();
    q.clear();

    if (m == SkTreeVisitMode::TV_DEPTH_PRE)
        s.push(r);

    else if (m == SkTreeVisitMode::TV_DEPTH_POST)
    {
        SkTreeVisit<K, T> v(r, SkTreeVisitMode::TV_BREADTH);

        while(v.next())
            v.current->setVisited(false);

        s.push(r);
    }

    else if (m == SkTreeVisitMode::TV_BREADTH)
        q.enqueue(r);
}

template <typename K, typename T>
bool SkTreeVisit<K, T>::hasNext()
{
    if (m == SkTreeVisitMode::TV_BREADTH)
        return !q.isEmpty();

    return !s.isEmpty();
}

template <typename K, typename T>
bool SkTreeVisit<K, T>::next()
{
    if (!hasNext())
        return false;

    if (m == SkTreeVisitMode::TV_DEPTH_PRE)
    {
        current = s.pop();
        SkTreeNode<K, T> *currChild  = nullptr;

        for(int64_t i=current->childrenCount()-1; i>=0; i--)
        {
            currChild = current->children()[i];

            if (currChild)
                s.push(currChild);
        }
    }

    else if (m == SkTreeVisitMode::TV_DEPTH_POST)
    {
        if (s.top()->isVisited())
            current = s.pop();

        else
        {
            while(s.top()->degree() > 0)
            {
                SkTreeNode<K, T> *curr = s.top();
                curr->setVisited(true);

                SkTreeNode<K, T> *currChild  = nullptr;

                for(int64_t i=curr->childrenCount()-1; i>=0; i--)
                {
                    currChild = curr->children()[i];

                    if (currChild)
                        s.push(currChild);
                }
            }

            current = s.pop();
        }
    }

    else if (m == SkTreeVisitMode::TV_BREADTH)
    {
        current = q.dequeue();
        SkTreeNode<K, T> *currChild  = nullptr;

        for(uint64_t i=0; i<current->childrenCount(); i++)
        {
            currChild = current->children()[i];

            if (currChild)
                q.enqueue(currChild);
        }
    }

    return true;
}

template <typename K, typename T>
SkPair<K, T> &SkTreeVisit<K, T>::item()
{
    if (!isValid())
        return np();

    currentPair.set(current->key(), current->value());
    return currentPair;
}

template <typename K, typename T>
void SkTreeVisit<K, T>::setValue(T *val)
{
    if (!isValid())
        return;

    current->setValue(val);
}

template <typename K, typename T>
void SkTreeVisit<K, T>::setLabel(CStr *label)
{
    if (!isValid())
        return;

    current->setLabel(label);
}

template <typename K, typename T>
CStr *SkTreeVisit<K, T>::label()
{
    if (!isValid())
        return nullptr;

    return current->label();
}

template <typename K, typename T>
bool SkTreeVisit<K, T>::isNil()
{
    if (!isValid())
        return false;

    return current->isNil();
}

template <typename K, typename T>
void SkTreeVisit<K, T>::visitToList(SkAbstractList<SkTreeNode<K, T> *> &l)
{
    while(next())
        l << current;
}

template <typename K, typename T>
SkTreeVisit<K, T> &SkTreeVisit<K, T>::operator ++ ()
{
    next();
    return *this;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKTREENODE_H
