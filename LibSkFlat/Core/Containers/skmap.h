#ifndef SKMAP_H
#define SKMAP_H

#include "skvector.h"
#include "abstract/skabstractmap.h"

template <typename K, typename V>
class SPECIALK SkMap extends SkAbstractMap<K, V>
{
    public:
        SkMap();
        SkMap(const SkMap<K, V> &source);
        ~SkMap();

        //add will add key without controls if it already exists
        //so, using only add and NOT [] to insert data, it will be also a MULTIMAP
        void add(const K &key, const V &value) override;
        void add(SkMap<K, V> &m);
        SkPair<K, V> remove(const K &key) override;

        bool contains(const K &key) override;

        //K &key(const V &value) override;
        void keys(SkAbstractList<K> &l) override;

        V &value(const K &key) override;
        void values(SkAbstractList<V> &l) override;

        void reverse();

        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t countValues(V &v) override;
        uint64_t size() override;

        void swap(SkMap<K, V> &other);

        SkPair<K, V> *data();

        SkMap<K, V> &operator =  (const SkMap<K, V> &source);
        bool        operator  == (const SkMap<K, V> &operand);
        V           &operator [] (const K &key) override;

        typename SkVector<SkPair<K, V>>::SkIterator *iterator() override;

        SkVector<SkPair<K, V>> &getInternalVector();

        K &nk()
        {return nullKey = K();}

        V &nv()
        {return nullValue = V();}

    private:
        SkVector<SkPair<K, V>> map;
        K nullKey;
        V nullValue;
};

template <typename K, typename V>
SkMap<K, V>::SkMap()
{}

template <typename K, typename V>
SkMap<K, V>::SkMap(const SkMap<K, V> &source)
{
    map = source.map;
}

template <typename K, typename V>
SkMap<K, V>::~SkMap()
{}

template <typename K, typename V>
void SkMap<K, V>::add(const K &key, const V &value)
{
    SkPair<K, V> p(key, value);
    map.append(p);
}

template <typename K, typename V>
void SkMap<K, V>::add(SkMap<K, V> &m)
{
    map.append(m.map);
}

template <typename K, typename V>
SkPair<K, V> SkMap<K, V>::remove(const K &key)
{
    for(uint64_t i=0; i<map.count(); i++)
        if (map[i].key() == key)
        {
            SkPair<K, V> p = map[i];
            map.removeAt(i);
            return p;
        }

    return SkPair<K, V>();
}

template <typename K, typename V>
bool SkMap<K, V>::contains(const K &key)
{
    for(uint64_t i=0; i<map.count(); i++)
        if (map[i].key() == key)
            return true;

    return false;
}

/*template <typename K, typename V>
K &SkMap<K, V>::key(const V &value)
{
    for(uint64_t i=0; i<map.count(); i++)
        if (map[i].value() == value)
            return map[i].key();

    return nk();
}*/

template <typename K, typename V>
void SkMap<K, V>::keys(SkAbstractList<K> &l)
{
    for(uint64_t i=0; i<map.count(); i++)
        l.append(map[i].key());
}

template <typename K, typename V>
V &SkMap<K, V>::value(const K &key)
{
    for(uint64_t i=0; i<map.count(); i++)
        if (map[i].key() == key)
            return map[i].value();

    return nv();
}

template <typename K, typename V>
void SkMap<K, V>::values(SkAbstractList<V> &l)
{
    for(uint64_t i=0; i<map.count(); i++)
        l.append(map[i].value());
}

/*template <typename K, typename V>
void SkMap<K, V>::sort(SkOrderAlgo algo)
{
    KeyComparator keyComparator;
    map.sort(algo, &keyComparator);
}*/

template <typename K, typename V>
void SkMap<K, V>::reverse()
{
    map.reverse();
}

template <typename K, typename V>
void SkMap<K, V>::clear()
{
    map.clear();
}

template <typename K, typename V>
bool SkMap<K, V>::isEmpty()
{
    return map.isEmpty();
}

template <typename K, typename V>
uint64_t SkMap<K, V>::count()
{
    return map.count();
}

template <typename K, typename V>
uint64_t SkMap<K, V>::countValues(V &v)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        if (itr->item().value() == v)
            i++;

    delete itr;
    return i;
}

template <typename K, typename V>
uint64_t SkMap<K, V>::size()
{
    return map.size();
}

template <typename K, typename V>
void SkMap<K, V>::swap(SkMap<K, V> &other)
{
    map.swap(other.map);
}

template <typename K, typename V>
SkPair<K, V> *SkMap<K, V>::data()
{
    return map.data();
}

template <typename K, typename V>
SkMap<K, V> &SkMap<K, V>::operator = (const SkMap<K, V> &source)
{
    map = source.map;
    return *this;
}

template <typename K, typename V>
bool SkMap<K, V>::operator == (const SkMap<K, V> &operand)
{
    return (map == operand.map);
}

template <typename K, typename V>
V &SkMap<K, V>::operator [] (const K &key)
{
    for(uint64_t i=0; i<map.count(); i++)
        if (map[i].key() == key)
            return map[i].value();

    add(key, nv());
    return map.last().value();
}

template <typename K, typename V>
typename SkVector<SkPair<K, V>>::SkIterator *SkMap<K, V>::iterator()
{
    return map.iterator();
}

template <typename K, typename V>
SkVector<SkPair<K, V>> &SkMap<K, V>::getInternalVector()
{
    return map;
}

#endif // SKMAP_H
