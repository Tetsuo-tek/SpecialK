#include "skdatabuffer.h"
#include "skringbuffer.h"
#include "skvariant.h"
#include "skarraycast.h"
#include "Core/sklogmachine.h"

DeclareWrapper(SkDataBuffer);

// // // // // // // // // // // // // // // // // // // // //

//DeclareMeth_INSTANCE_RET(SkDataBuffer, setRawData, bool, Arg_Custom(void), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkDataBuffer, setData, bool, Arg_Custom(void), Arg_UInt64, Arg_Bool)//
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 1, bool, Arg_StringRef, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 2, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 3, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkDataBuffer, set, bool, Arg_Char)
DeclareMeth_INSTANCE_RET(SkDataBuffer, setZero, bool)
DeclareMeth_INSTANCE_RET(SkDataBuffer, prepend, bool, Arg_CStr, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 1, bool, Arg_StringRef, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 2, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 3, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_Bool)

DeclareMeth_INSTANCE_RET(SkDataBuffer, insert, bool, Arg_CStr, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 1, bool, Arg_CStr, Arg_UInt64, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 2, bool, Arg_StringRef, Arg_UInt64, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 3, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 4, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkDataBuffer, append, bool, Arg_CStr, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 1, bool, Arg_StringRef, Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 2, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 3, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkDataBuffer, fill, Arg_Char, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, indexOf, int64_t, Arg_Char, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, indexOf, 1, int64_t, Arg_CStr, Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, indexOf, 2, int64_t, Arg_StringRef, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, substitute, bool, Arg_Char, Arg_Char, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, substitute, 1, bool, Arg_CStr, Arg_UInt64, Arg_CStr, Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, substitute, 2, bool, Arg_StringRef, Arg_StringRef, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, remove, bool, Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, copyTo, bool, Arg_Data(char), Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 1, bool, Arg_Data(char), Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 2, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 3, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 4, bool, Arg_StringRef, Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, left, bool, Arg_Data(char), Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, right, bool, Arg_Data(char), Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, compare, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, compare, 1, bool, Arg_CStr, Arg_UInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, startsWith, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, startsWith, 1, bool, Arg_StringRef)
DeclareMeth_INSTANCE_RET(SkDataBuffer, endsWith, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, endsWith, 1, bool, Arg_StringRef)
//DeclareMeth_INSTANCE_RET(SkDataBuffer, at, bool, Arg_Char_REAL, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, at, 1, char, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkDataBuffer, data, CStr*)
DeclareMeth_INSTANCE_RET(SkDataBuffer, toVoid, void*)
DeclareMeth_INSTANCE_RET(SkDataBuffer, toString, SkString)
DeclareMeth_INSTANCE_VOID(SkDataBuffer, toStringHash, Arg_StringRef, Arg_Enum(SkHashMode))
DeclareMeth_INSTANCE_VOID(SkDataBuffer, toBinaryHash, *Arg_Custom(SkDataBuffer), Arg_Enum(SkHashMode))
DeclareMeth_INSTANCE_VOID(SkDataBuffer, toBase64, *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_VOID(SkDataBuffer, fromBase64, *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET(SkDataBuffer, compress, bool, *Arg_Custom(SkDataBuffer), Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkDataBuffer, decompress, bool, *Arg_Custom(SkDataBuffer))
DeclareMeth_STATIC_RET_OVERLOAD(SkDataBuffer, compress, 1, bool, *Arg_Custom(SkDataBuffer), *Arg_Custom(SkDataBuffer), Arg_UInt8)
DeclareMeth_STATIC_RET_OVERLOAD(SkDataBuffer, decompress, 2, bool, *Arg_Custom(SkDataBuffer), *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_RET(SkDataBuffer, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkDataBuffer, size, uint64_t)
DeclareMeth_INSTANCE_VOID(SkDataBuffer, swap, *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_VOID(SkDataBuffer, clear)

// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkDataBuffer)
{
    SetClassSuper(SkDataBuffer, SkFlatObject);

    //AddMeth_INSTANCE_RET(SkDataBuffer, setRawData);
    AddMeth_INSTANCE_RET(SkDataBuffer, setData);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 2);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, setData, 3);
    AddMeth_INSTANCE_RET(SkDataBuffer, set);
    AddMeth_INSTANCE_RET(SkDataBuffer, setZero);
    AddMeth_INSTANCE_RET(SkDataBuffer, prepend);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 2);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, prepend, 3);
    AddMeth_INSTANCE_RET(SkDataBuffer, insert);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 2);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 3);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, insert, 4);
    AddMeth_INSTANCE_RET(SkDataBuffer, append);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 2);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, append, 3);
    AddMeth_INSTANCE_VOID(SkDataBuffer, fill);
    AddMeth_INSTANCE_RET(SkDataBuffer, indexOf);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, indexOf, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, indexOf, 2);
    AddMeth_INSTANCE_RET(SkDataBuffer, substitute);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, substitute, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, substitute, 2);
    AddMeth_INSTANCE_RET(SkDataBuffer, remove);
    AddMeth_INSTANCE_RET(SkDataBuffer, copyTo);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 2);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 3);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, copyTo, 4);
    AddMeth_INSTANCE_RET(SkDataBuffer, left);
    AddMeth_INSTANCE_RET(SkDataBuffer, right);
    AddMeth_INSTANCE_RET(SkDataBuffer, compare);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, compare, 1);
    AddMeth_INSTANCE_RET(SkDataBuffer, startsWith);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, startsWith, 1);
    AddMeth_INSTANCE_RET(SkDataBuffer, endsWith);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, endsWith, 1);
    //AddMeth_INSTANCE_RET(SkDataBuffer, at);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataBuffer, at, 1);
    AddMeth_INSTANCE_RET(SkDataBuffer, data);
    AddMeth_INSTANCE_RET(SkDataBuffer, toVoid);
    AddMeth_INSTANCE_RET(SkDataBuffer, toString);
    AddMeth_INSTANCE_VOID(SkDataBuffer, toStringHash);
    AddMeth_INSTANCE_VOID(SkDataBuffer, toBinaryHash);
    AddMeth_INSTANCE_VOID(SkDataBuffer, toBase64);
    AddMeth_INSTANCE_VOID(SkDataBuffer, fromBase64);
    AddMeth_INSTANCE_RET(SkDataBuffer, compress);
    AddMeth_INSTANCE_RET(SkDataBuffer, decompress);
    AddMeth_STATIC_RET_OVERLOAD(SkDataBuffer, compress, 1);
    AddMeth_STATIC_RET_OVERLOAD(SkDataBuffer, decompress, 2);
    AddMeth_INSTANCE_RET(SkDataBuffer, isEmpty);
    AddMeth_INSTANCE_RET(SkDataBuffer, size);
    AddMeth_INSTANCE_VOID(SkDataBuffer, swap);
    AddMeth_INSTANCE_VOID(SkDataBuffer, clear);
}

// // // // // // // // // // // // // // // // // // // // //

SkDataBuffer::SkDataBuffer()
{
    CreateClassWrapper(SkDataBuffer);

    buffer = nullptr;
    sz = 0;
    defaultAtPosErr = '\0';
}

SkDataBuffer::SkDataBuffer(uint64_t bytes)
{
    buffer = nullptr;
    sz = 0;

    defaultAtPosErr = '\0';

    if (bytes > 0)
    {
        sz = bytes;
        buffer = new char [sz];
        setZero();
    }
}

SkDataBuffer::SkDataBuffer(SkDataBuffer &other)
{
    buffer = nullptr;
    sz = 0;

    defaultAtPosErr = '\0';

    if (!other.isEmpty())
        setData(other.buffer, other.size(), false);
}

SkDataBuffer::~SkDataBuffer()
{
    if (sz > 0)
        delete [] buffer;
}

bool SkDataBuffer::setRawData(void *data, uint64_t size, bool notifyLoad)
{
    if (size == 0)
    {
        FlatError("You are setting nothing to this buffer");
        return false;
    }

    if (sz > 0)
        delete [] buffer;

    sz = size;
    buffer = SkArrayCast::toChar(data);

    if (notifyLoad)
        onDataLoaded(size);

    return true;
}

bool SkDataBuffer::setData(CVoid *data, uint64_t size, bool notifyLoad)
{
    char *d = new char [size];
    memcpy(d, data, size);
    return setRawData(d, size, notifyLoad);
}

bool SkDataBuffer::setData(SkString &s, bool notifyLoad)
{
    return setData(s.c_str(), s.size(), notifyLoad);
}

bool SkDataBuffer::setData(SkDataBuffer &source, uint64_t size, bool notifyLoad)
{
    if (source.isEmpty())
        return false;

    if (size == 0)
        size = source.size();

    return setData(source.data(), size, notifyLoad);
}

bool SkDataBuffer::setData(SkRingBuffer *source, uint64_t size, bool notifyLoad)
{
    if (source->isEmpty())
        return false;

    if (size == 0)
        size = source->size();

    char *d = new char [size];
    source->getCustomBuffer(d, size);
    setData(d, size, notifyLoad);
    delete [] d;

    return true;
}

bool SkDataBuffer::set(char c)
{
    if (sz == 0)
        return false;

    memset(buffer, c, sz);
    return true;
}

bool SkDataBuffer::setZero()
{
    return set('\0');
}

bool SkDataBuffer::prepend(CStr *data, uint64_t size, bool notifyLoad)
{
    if (size == 0)
    {
        FlatError("You are prepending anything to this buffer");
        return false;
    }

    char *tempData = new char [size+sz];
    memcpy(tempData, data, size);

    if (sz > 0)
    {
        memcpy(&tempData[size], buffer, sz);
        delete [] buffer;
    }

    buffer = tempData;
    sz += size;

    if (notifyLoad)
        onDataLoaded(size);

    return true;
}

bool SkDataBuffer::insert(CStr *data, uint64_t size, bool notifyLoad)
{
    return insert(data, 0, size, notifyLoad);
}

bool SkDataBuffer::insert(CStr *data, uint64_t from, uint64_t size, bool notifyLoad)
{
    if (size == 0)
    {
        FlatError("You are inserting anything to this buffer");
        return false;
    }

    if (from == 0)
        return prepend(data, size, notifyLoad);

    if (from >= sz)
    {
        FlatError("You are making a buffer-overflow [From: " << from << " B; Size: " << sz << "]");
        return false;
    }

    if (sz == 0)
        return setData(data, size, false);

    else if (from == sz-1)
        return append(data, size, false);

    char *tempData = new char [sz+size];
    memcpy(tempData, buffer, from);
    memcpy(&tempData[from], data, size);
    memcpy(&tempData[from+size], &buffer[from], sz-from);
    delete [] buffer;
    buffer = tempData;
    sz += size;

    if (notifyLoad)
        onDataLoaded(size);

    return true;
}

bool SkDataBuffer::append(CStr *data, uint64_t size, bool notifyLoad)
{
    if (size == 0)
    {
        FlatError("You are adding anything to this buffer");
        return false;
    }

    char *tempData = new char [sz+size];

    if (sz > 0)
    {
        memcpy(tempData, buffer, sz);
        delete [] buffer;
    }

    memcpy(&tempData[sz], data, size);
    buffer = tempData;
    sz += size;

    if (notifyLoad)
        onDataLoaded(size);

    return true;
}

bool SkDataBuffer::prepend(SkString &s, uint64_t size, bool notifyLoad)
{
    if (size == 0)
        size = s.size();

    if (size > s.size())
    {
        FlatError("You are adding more data than existing in the string");
        return false;
    }

    return prepend(s.c_str(), size, notifyLoad);
}

bool SkDataBuffer::insert(SkString &s, uint64_t from, uint64_t size, bool notifyLoad)
{
    if (size == 0)
        size = s.size();

    if (size > s.size())
    {
        FlatError("You are adding more data than existing in the string");
        return false;
    }

    return insert(s.c_str(), from, size, notifyLoad);
}

bool SkDataBuffer::append(SkString &s, uint64_t size, bool notifyLoad)
{
    if (size == 0)
        size = s.size();

    if (size > s.size())
    {
        FlatError("You are adding more data than existing in the string");
        return false;
    }

    return append(s.c_str(), size, notifyLoad);
}

bool SkDataBuffer::prepend(SkDataBuffer &buf, uint64_t size, bool notifyLoad)
{
    if (buf.isEmpty())
    {
        FlatError("You are prepending anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf.size();

    else if (buf.size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to prepend");
        return false;
    }

    return prepend(buf.data(), size, notifyLoad);
}

bool SkDataBuffer::insert(SkDataBuffer &buf, uint64_t from, uint64_t size, bool notifyLoad)
{
    if (buf.isEmpty())
    {
        FlatError("You are inserting anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf.size();

    else if (buf.size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to insert");
        return false;
    }

    return insert(buf.data(), from, size, notifyLoad);
}

bool SkDataBuffer::append(SkDataBuffer &buf, uint64_t size, bool notifyLoad)
{
    if (buf.isEmpty())
    {
        FlatError("You are appending anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf.size();

    else if (buf.size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to append");
        return false;
    }

    return append(buf.data(), size, notifyLoad);
}

bool SkDataBuffer::prepend(SkRingBuffer *buf, uint64_t size, bool notifyLoad)
{
    if (buf->isEmpty())
    {
        FlatError("You are prepending anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf->size();

    else if (buf->size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to prepend");
        return false;
    }

    char *tempData = new char [size];

    if (!buf->getCustomBuffer(tempData, size))
    {
        delete [] tempData;
        return false;
    }

    bool ok = prepend(tempData, size, notifyLoad);

    delete [] tempData;
    return ok;
}

bool SkDataBuffer::insert(SkRingBuffer *buf, uint64_t from, uint64_t size, bool notifyLoad)
{
    if (buf->isEmpty())
    {
        FlatError("You are inserting anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf->size();

    else if (buf->size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to insert");
        return false;
    }

    char *tempData = new char [size];

    if (!buf->getCustomBuffer(tempData, size))
    {
        delete [] tempData;
        return false;
    }

    bool ok = insert(tempData, from, size, notifyLoad);

    delete [] tempData;
    return ok;
}

bool SkDataBuffer::append(SkRingBuffer *buf, uint64_t size, bool notifyLoad)
{
    if (buf->isEmpty())
    {
        FlatError("You are appending anything to this buffer");
        return false;
    }

    if (size == 0)
        size = buf->size();

    else if (buf->size() < size)
    {
        FlatError("Source DataBuffer NOT reach requested size to append");
        return false;
    }

    char *tempData = new char [size];

    if (!buf->getCustomBuffer(tempData, size))
    {
        delete [] tempData;
        return false;
    }

    bool ok = append(tempData, size, notifyLoad);

    delete [] tempData;
    return ok;
}

void SkDataBuffer::fill(char c, uint64_t count)
{
    if (count == 0)
        return;

    if (count <= sz)
        memset(buffer, c, count);

    else
    {
        if (buffer)
            delete [] buffer;

        sz = count;
        buffer = new char [sz];
        memset(buffer, c, sz);
    }
}

int64_t SkDataBuffer::indexOf(char c, uint64_t from)
{
    if (from >= sz)
        return -1;

    for(uint64_t i=from; i<sz; i++)
        if (buffer[i] == c)
            return static_cast<int64_t>(i);

    return -1;
}

int64_t SkDataBuffer::indexOf(CStr *d, uint64_t size, uint64_t from)
{
    if (from+size >= sz)
        return -1;

    for(uint64_t i=(from); i<sz-size; i++)
        if (memcmp(&d[i], d, size) == 0)
            return static_cast<int64_t>(i);

    return -1;
}

int64_t SkDataBuffer::indexOf(SkString &s, uint64_t from)
{
    return indexOf(s.c_str(), s.size(), from);
}

bool SkDataBuffer::substitute(char oldCh, char newCh, uint64_t from)
{
    int64_t index = indexOf(oldCh, from);

    if (index > -1)
    {
        buffer[index] = newCh;
        return true;
    }

    return false;
}

bool SkDataBuffer::substitute(CStr *oldData, uint64_t oldDataSz, CStr *newData, uint64_t newDataSz, uint64_t from)
{
    int64_t index = indexOf(oldData, oldDataSz, from);

    if (index > -1)
    {
        uint64_t newSz = sz-oldDataSz+newDataSz;
        char *tempData = new char [newSz];

        memcpy(tempData, buffer, index);
        memcpy(&tempData[index], newData, newDataSz);
        uint64_t holeOffset = static_cast<uint64_t>(index)+oldDataSz;
        memcpy(&tempData[static_cast<uint64_t>(index)+newDataSz], &buffer[holeOffset], sz-holeOffset);

        delete [] buffer;
        buffer = tempData;
        sz = newSz;

        return true;
    }

    return false;
}

bool SkDataBuffer::substitute(SkString &oldStr, SkString &newStr, uint64_t from)
{
    return substitute(oldStr.c_str(), oldStr.size(), newStr.c_str(), newStr.size(), from);
}

bool SkDataBuffer::remove(uint64_t from, uint64_t size)
{
    if (from+size >= sz)
        return false;

    uint64_t newSz = sz-size;

    if (size == 0)
        return false;

    char *tempData = new char [newSz];

    if (from > 0)
        memcpy(tempData, buffer, from);

    uint64_t holeOffset = from+size;
    memcpy(tempData, &buffer[holeOffset], sz-holeOffset);

    delete [] buffer;
    buffer = tempData;
    sz = newSz;

    return true;
}

//IF SIZE REQUESTED IS NOT READY IT WILL RETURN AN EMPTY BUFFER (NOT NULL but set to 0)
bool SkDataBuffer::copyTo(char *data, uint64_t size)
{
    if (size > sz)
        return false;

    if (data)
        memcpy(data, buffer, size);

    return true;
}

bool SkDataBuffer::copyTo(char *data, uint64_t from, uint64_t size)
{
    if (from + size > sz)
        return false;

    if (data)
        memcpy(data, &buffer[from], size);

    return true;
}

bool SkDataBuffer::copyTo(SkDataBuffer &target, uint64_t from, uint64_t size)
{
    if (sz == 0)
    {
        FlatError("You are adding nothing to DataBuffer target");
        return false;
    }

    if (size == 0)
        size = sz;

    if (from+size > sz)
    {
        FlatError("Requested 'from' + 'size' is greater than buffer size");
        return false;
    }

    target.append(&buffer[from], size);
    return true;
}

bool SkDataBuffer::copyTo(SkRingBuffer *target, uint64_t from, uint64_t size)
{
    if (sz == 0)
    {
        FlatWarning("You are adding nothing to RingBuffer target");
        return false;
    }

    if (size == 0)
        size = sz;

    if (from+size > sz)
    {
        FlatError("Requested 'from' + 'size' is greater than buffer size");
        return false;
    }

    target->addData(&buffer[from], size);
    return true;
}

bool SkDataBuffer::copyTo(SkString &target, uint64_t from, uint64_t size)
{
    if (sz == 0)
    {
        FlatWarning("You are adding nothing to RingBuffer target");
        return false;
    }

    if (size == 0)
        size = sz;

    if (from+size > sz)
    {
        FlatError("Requested 'from' + 'size' is greater than buffer size");
        return false;
    }

    char *tempData = new char [size+1];
    memcpy(tempData, &buffer[from], size);
    tempData[size] = '\0';

    target = tempData;
    delete [] tempData;

    return true;
}

bool SkDataBuffer::left(char *data, uint64_t size)
{
    return copyTo(data, 0, size);
}

bool SkDataBuffer::right(char *data, uint64_t size)
{
    return copyTo(data, sz-size-1, size);
}

bool SkDataBuffer::compare(CStr *data)
{
    return compare(data, 0, sz);
}

bool SkDataBuffer::compare(CStr *data, uint64_t from, uint64_t size)
{
    if (size == 0)
        return false;

    if (from+size > sz)
        return false;

    return (memcmp(&buffer[from], data, size) == 0);
}

bool SkDataBuffer::startsWith(CStr *data, uint64_t size)
{
    if (size > sz)
    {
        FlatError("You are comparing data [startsWith] having size > of this buffer");
        return false;
    }

    return compare(data, 0, size);
}

bool SkDataBuffer::startsWith(SkString &s)
{
    return startsWith(s.c_str(), s.size());
}

bool SkDataBuffer::endsWith(CStr *data, uint64_t size)
{
    if (size > sz)
    {
        FlatError("You are comparing data [endsWith] having size > of this buffer");
        return false;
    }

    return compare(data, sz-size-1, size);
}

bool SkDataBuffer::endsWith(SkString &s)
{
    return endsWith(s.c_str(), s.size());
}

bool SkDataBuffer::at(char &element, uint64_t index)
{
    if (index >= sz)
        return false;

    element = buffer[index];
    return true;
}

char SkDataBuffer::at(uint64_t index)
{
    char c = -1;
    at(c, index);
    return c;
}

CStr *SkDataBuffer::data()
{
    return buffer;
}

void *SkDataBuffer::toVoid()
{
    return buffer;
}

SkString SkDataBuffer::toString()
{
    if (isEmpty())
        return "";

    SkString s;
    s.append(buffer, sz);
    return s;
}

void SkDataBuffer::toStringHash(SkString &output, SkHashMode mode)
{
    if (isEmpty())
        output = "";

    output = bufferHash(buffer, sz, mode);
}

void SkDataBuffer::toBinaryHash(SkDataBuffer &output, SkHashMode mode)
{
    if (isEmpty())
        return;

    binaryHash(buffer, sz, output, mode);
}

void SkDataBuffer::toBase64(SkDataBuffer &output)
{
    b64enc(buffer, sz, output);
}

void SkDataBuffer::fromBase64(SkDataBuffer &input)
{
    b64dec(input.buffer, input.sz, *this);
}

bool SkDataBuffer::compress(SkDataBuffer &output, uint8_t level)
{
    return gzip(buffer, sz, output, level);
}

bool SkDataBuffer::decompress(SkDataBuffer &output)
{
    return ungzip(buffer, sz, output);
}

bool SkDataBuffer::isEmpty()
{
    return (sz == 0);
}

uint64_t SkDataBuffer::size()
{
    return sz;
}

void SkDataBuffer::swap(SkDataBuffer &other)
{
    uint64_t tempSZ = other.sz;
    char *tempBUFFER = other.buffer;

    other.buffer = buffer;
    other.sz = sz;

    sz = tempSZ;
    buffer = tempBUFFER;
}

void SkDataBuffer::clear()
{
    if (sz > 0)
    {
        delete [] buffer;
        buffer = nullptr;
        sz = 0;
    }
}

SkDataBuffer &SkDataBuffer::operator = (SkString &s)
{
    if (s.isEmpty())
        return *this;

    setData(s.c_str(), s.size());
    return *this;
}

SkDataBuffer &SkDataBuffer::operator = (SkDataBuffer &buf)
{
    if (buf.isEmpty())
        return *this;

    clear();
    buf.copyTo(*this);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator = (SkRingBuffer *buf)
{
    if (buf->isEmpty())
        return *this;

    clear();
    buf->copyTo(*this);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator << (SkString &s)
{
    if (s.isEmpty())
        return *this;

    append(s.c_str(), s.size());
    return *this;
}

SkDataBuffer &SkDataBuffer::operator << (SkDataBuffer &buf)
{
    if (buf.isEmpty())
        return *this;

    buf.copyTo(*this);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator << (SkRingBuffer *buf)
{
    if (buf->isEmpty())
        return *this;

    buf->copyTo(*this);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator >> (SkString &s)
{
    if (isEmpty())
        return *this;

    s.append(buffer, sz);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator >> (SkDataBuffer &buf)
{
    if (isEmpty())
        return *this;

    copyTo(buf);
    return *this;
}

SkDataBuffer &SkDataBuffer::operator >> (SkRingBuffer *buf)
{
    if (isEmpty())
        return *this;

    copyTo(buf);
    return *this;
}

char &SkDataBuffer::operator [] (uint64_t index)
{
    if (index >= sz)
    {
        FlatError("RETURNING 0 ON BUFFER-OVERFLOW, (READING WITH [] OP); INDEX: " << index << "; SZ: " << sz << " B");
        return defaultAtPosErr;
    }

    return buffer[index];
}

bool SkDataBuffer::compress(SkDataBuffer &input, SkDataBuffer &output, uint8_t level)
{
    return input.compress(output, level);
}

bool SkDataBuffer::decompress(SkDataBuffer &input, SkDataBuffer &output)
{
    return input.decompress(output);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <zlib.h>
#include "Core/skmath.h"

#define GZIP_WINDOWS_BIT 15 + 16
#define GZIP_CHUNK_SIZE 32 * 1024

bool gzip(void *input, uint64_t size, SkDataBuffer &output, uint8_t level)
{
    if (size)
    {
        int flush = 0;

        // Prepare deflater status
        z_stream strm;
        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        strm.avail_in = 0;
        strm.next_in = Z_NULL;

        // Initialize deflater
        int ret = deflateInit2(&strm,
                               SkMath::max(-1, level),
                               Z_DEFLATED,
                               GZIP_WINDOWS_BIT,
                               8,
                               Z_DEFAULT_STRATEGY);

        if (ret != Z_OK)
            return(false);

        // Extract pointer to input data
        char *input_data = static_cast<char *>(input);
        uint64_t input_data_left = size;

        // Compress data until available
        do
        {
            // Determine current chunk size
            uint chunk_size =
                    static_cast<uint>
                    (SkMath::min(GZIP_CHUNK_SIZE, static_cast<double>(input_data_left)));

            // Set deflater references
            strm.next_in = (unsigned char*)input_data;
            strm.avail_in = chunk_size;

            // Update interval variables
            input_data += chunk_size;
            input_data_left -= chunk_size;

            // Determine if it is the last chunk
            flush = (input_data_left <= 0 ? Z_FINISH : Z_NO_FLUSH);

            // Deflate chunk and cumulate output
            do
            {
                // Declare vars
                char out[GZIP_CHUNK_SIZE];

                // Set deflater references
                strm.next_out = (unsigned char *) out;
                strm.avail_out = GZIP_CHUNK_SIZE;

                // Try to deflate chunk
                ret = deflate(&strm, flush);

                // Check errors
                if (ret == Z_STREAM_ERROR)
                {
                    // Clean-up
                    deflateEnd(&strm);

                    return false;
                }

                // Determine compressed size
                uint have = (GZIP_CHUNK_SIZE - strm.avail_out);

                // Cumulate result
                if (have > 0)
                    output.append(out, have);

            }

            while (strm.avail_out == 0);
        }

        while (flush != Z_FINISH);

        // Clean-up
        (void) deflateEnd(&strm);

        // Return
        return (ret == Z_STREAM_END);
    }

    return false;
}

bool ungzip(void *input, uint64_t size, SkDataBuffer &output)
{
    if (size)
    {
        // Prepare inflater status
        z_stream strm;
        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        strm.avail_in = 0;
        strm.next_in = Z_NULL;

        // Initialize inflater
        int ret = inflateInit2(&strm, GZIP_WINDOWS_BIT);

        if (ret != Z_OK)
        {
            StaticError("Z_OK is FALSE");
            return false;
        }

        // Extract pointer to input data
        char *input_data = static_cast<char *>(input);
        uint64_t input_data_left = size;

        // Decompress data until available
        do
        {
            // Determine current chunk size
            uint chunk_size =
                    static_cast<uint>
                    (SkMath::min(GZIP_CHUNK_SIZE, static_cast<double>(input_data_left)));

            // Check for termination
            if (chunk_size <= 0)
                break;

            // Set inflater references
            strm.next_in = (unsigned char*) input_data;
            strm.avail_in = chunk_size;

            // Update interval variables
            input_data += chunk_size;
            input_data_left -= chunk_size;

            // Inflate chunk and cumulate output
            do
            {

                // Declare vars
                char out[GZIP_CHUNK_SIZE];

                // Set inflater references
                strm.next_out = (unsigned char*) out;
                strm.avail_out = GZIP_CHUNK_SIZE;

                // Try to inflate chunk
                ret = inflate(&strm, Z_NO_FLUSH);

                if ((ret == Z_NEED_DICT)
                        || (ret == Z_DATA_ERROR)
                        || (ret == Z_MEM_ERROR)
                        || (ret == Z_STREAM_ERROR))
                {
                    inflateEnd(&strm);
                    StaticError("CANNOT inflate");
                    return false ;
                }

                // Determine decompressed size
                uint have = (GZIP_CHUNK_SIZE - strm.avail_out);

                // Cumulate result
                if (have > 0)
                    output.append(out, have);

            }

            while (strm.avail_out == 0);
        }

        while (ret != Z_STREAM_END);

        // Clean-up
        inflateEnd(&strm);

        if (ret == Z_STREAM_END)
            return true;

        else
        {
            StaticError("Z_STREAM_END NOT found");
            return false;
        }
    }

    StaticError("There is NOT data (size = 0)");
    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif*/
#define SET_BINARY_MODE(file)
#define CHUNK 16384

#include <assert.h>

/* Compress from file source to file dest until EOF on source.
   def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files. */
int def(FILE *source, FILE *dest, int level)
{
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inf(FILE *source, FILE *dest)
{
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/* report a zlib or i/o error */
void zerr(int ret)
{
    fputs("zpipe: ", stderr);
    switch (ret) {
    case Z_ERRNO:
        if (ferror(stdin))
            fputs("error reading stdin\n", stderr);
        if (ferror(stdout))
            fputs("error writing stdout\n", stderr);
        break;
    case Z_STREAM_ERROR:
        fputs("invalid compression level\n", stderr);
        break;
    case Z_DATA_ERROR:
        fputs("invalid or incomplete deflate data\n", stderr);
        break;
    case Z_MEM_ERROR:
        fputs("out of memory\n", stderr);
        break;
    case Z_VERSION_ERROR:
        fputs("zlib version mismatch!\n", stderr);
    }
}

bool inflate(void *input, uint64_t size, SkDataBuffer &output)
{
    int inputPos = 0;

    int ret;
    unsigned have;
    z_stream strm;

    uint8_t *in = SkArrayCast::toUInt8(input);
    uint8_t out[CHUNK];

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;

    //ret = inflateInit(&strm);
    int windowBits = -15; // valore negativo per indicare il formato raw Deflate
    ret = inflateInit2(&strm, windowBits);

    if (ret != Z_OK)
    {
        StaticError("CANNOT init zlib");
        return false;
    }

    do {
        int chunkSz = CHUNK;
        int remain = size-inputPos;

        if (remain < CHUNK)
            chunkSz = remain;

        strm.avail_in = chunkSz;

        if (strm.avail_in == 0)
            break;

        strm.next_in = &in[inputPos];

        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            //assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                StaticError("CANNOT inflate: " << ret);
                (void)inflateEnd(&strm);
                return false;
            }
            have = CHUNK - strm.avail_out;
            output.append(SkArrayCast::toCStr(out), have);
        } while (strm.avail_out == 0);

        inputPos += chunkSz;

        if (inputPos >= size)
            break;

    } while (ret != Z_STREAM_END);

    (void)inflateEnd(&strm);

    if (ret == Z_STREAM_END)
        StaticWarning("ret = " << ret);

    return true;
}
