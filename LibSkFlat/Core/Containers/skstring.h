/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSTRING_H
#define SKSTRING_H

#include "skdefines.h"
#include "skvector.h"

#define DEC             10
#define HEX             16

/**
 * @brief The SkString
 * It is a derived class of std::string, thinked to have facilities meths that
 * emulates QString from Qt and String from Arduino
 */

#define Stringify(STRING, STREAM) \
    FakeSingleLine( \
        stringstream stream; \
        stream << STREAM; \
        STRING.append(stream.str().data());\
    )

class SPECIALK SkString extends string/*, public SkFlatObject*/
{
    public:
        /**
         * @brief Constructs from cstring
         * @param CStr *the cstring to insert
         */
        SkString(CStr *cstr = "");

        /**
         * @brief Constructs from single character
         * @param c the character to insert
         */
        SkString(char c);

        /**
         * @brief Constructs from cstring segment
         * @param CStr *the cstring to insert
         * @param size the length of the cstring
         */
        SkString(char *cstr, int size);

        /**
         * @brief Contruct from another string
         * @param str the string to insert
         */
        SkString(const SkString &str);

        /**
         * @brief Contructs from a std::string
         * @param str the string to insert
         */
        SkString(const string &str);

        /**
         * @brief Constructs from arithmetic value
         * @param value the unsigned char number
         * @param base number representation (DEC/HEX)
         */
        SkString(unsigned char value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the integer number
         * @param base number representation (DEC/HEX)
         */
        SkString(int value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the unsigned integer number
         * @param base number representation (DEC/HEX)
         */
        SkString(unsigned int value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the long number
         * @param base number representation (DEC/HEX)
         */
        SkString(long value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the unsigned long number
         * @param base number representation (DEC/HEX)
         */
        SkString(unsigned long value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the long long number
         * @param base number representation (DEC/HEX)
         */
        SkString(long long value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the unsigned long long number
         * @param base number representation (DEC/HEX)
         */
        SkString(unsigned long long value, int base=DEC);

        /**
         * @brief Constructs from arithmetic value
         * @param value the float with single precision number
         * @param precision the number of decimals considered
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         */
        SkString(float value, int precision=3, bool fixed=false);

        /**
         * @brief Constructs from arithmetic value
         * @param value the float with double precision number
         * @param precision the number of decimals considered
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         */
        SkString(double value, int precision=3, bool fixed=false);

        /**
         * @brief Assign-operator from cstring
         * @param CStr *the string to insert
         * @return this string
         */
        SkString &operator = (CStr *cstr);

        /**
         * @brief Assign-operator from string
         * @param str the string to insert
         * @return this string
         */
        SkString &operator = (SkString str);

        /**
         * @brief Assign-operator from std::string
         * @param str the string to insert
         * @return this string
         */
        SkString &operator = (string str);

        /**
         * @brief Equal-operator from cstring
         * @param CStr *the string to compare
         * @return true if contents are equal, otherwise false
         */
        bool operator == (CStr *cstr);

        /**
         * @brief Equal-operator from string
         * @param str the string to compare
         * @return true if contents are equal, otherwise false
         */
        bool operator == (SkString &str);

        /**
         * @brief LessThan-operator from cstring
         * @param str the string to compare
         * @return true if contents are LessThan, otherwise false
         */
        bool operator < (CStr *cstr);

        /**
         * @brief LessThan-operator from string
         * @param str the string to compare
         * @return true if contents are LessThan, otherwise false
         */
        bool operator < (SkString &str);

        /**
         * @brief LessThanOrEqual-operator from cstring
         * @param str the string to compare
         * @return true if contents are LessThanOrEqual, otherwise false
         */
        bool operator <= (CStr *);

        /**
         * @brief LessThanOrEqual-operator from string
         * @param str the string to compare
         * @return true if contents are LessThanOrEqual, otherwise false
         */
        bool operator <= (SkString &str);

        /**
         * @brief GreaterThan-operator from cstring
         * @param str the string to compare
         * @return true if contents are GreaterThan, otherwise false
         */
        bool operator > (CStr *);

        /**
         * @brief GreaterThan-operator from string
         * @param str the string to compare
         * @return true if contents are GreaterThan, otherwise false
         */
        bool operator > (SkString &str);

        /**
         * @brief GreaterThanOrEqual-operator from cstring
         * @param str the string to compare
         * @return true if contents are GreaterThanOrEqual, otherwise false
         */
        bool operator >= (CStr *);

        /**
         * @brief GreaterThanOrEqual-operator from string
         * @param str the string to compare
         * @return true if contents are GreaterThanOrEqual, otherwise false
         */
        bool operator >= (SkString &str);

        /**
         * @brief Append-operator of a character
         * @param c the character to append
         * @return this string
         */
        SkString &operator += (char c);

        /**
         * @brief Append-operator of a cstring
         * @param CStr *the string to append
         * @return this string
         */
        SkString &operator += (CStr *);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the unsigned char number
         * @return this string
         */
        SkString &operator += (unsigned char value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the integer number
         * @return this string
         */
        SkString &operator += (int value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the unsigned integer number
         * @return this string
         */
        SkString &operator += (unsigned int value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the long number
         * @return this string
         */
        SkString &operator += (long value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the unsigned long number
         * @return this string
         */
        SkString &operator += (unsigned long value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the long long number
         * @return this string
         */
        SkString &operator += (long long value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the unsigned long long number
         * @return this string
         */
        SkString &operator += (unsigned long long value);

        /**
         * @brief Append-operator of an arithmetic value converted to a string
         * @param value the double number
         * @return this string
         */
        SkString &operator += (double value);

        /**
         * @brief Append-operator of another string
         * @param str the string to append
         * @return this string
         */
        SkString &operator += (SkString str);

        /**
         * @brief Fills the string with specified character
         * @param c the filling character
         * @param count the size to fill
         */
        SkString &fill(char c, uint64_t count);

        /**
         * @brief Creates a string filled with specified character
         * @param c the filling character
         * @param count the size to fill
         * @return the create string
         */
        static SkString buildFilled(char c, uint64_t count);

        //"append" methods are defined inside std::string and they will NOT be substituted
        //"concat" methods numenclature is oriented to Arduino-String compatibility

        /**
         * @brief Appends a character
         * @param c the character to append
         */
        void concat(char c);

        /**
         * @brief Appends a cstring
         * @param CStr *the string to append
         */
        void concat(CStr *cstr);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the unsigned char number
         * @param base number representation (DEC/HEX)
         */
        void concat(unsigned char value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the integer number
         * @param base number representation (DEC/HEX)
         */
        void concat(int value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the unsigned integer number
         * @param base number representation (DEC/HEX)
         */
        void concat(unsigned int value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the long number
         * @param base number representation (DEC/HEX)
         */
        void concat(long value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the unsigned long number
         * @param base number representation (DEC/HEX)
         */
        void concat(unsigned long value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the long long number
         * @param base number representation (DEC/HEX)
         */
        void concat(long long value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the unsigned long long number
         * @param base number representation (DEC/HEX)
         */
        void concat(unsigned long long value, int base=DEC);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the float number
         * @param base number representation (DEC/HEX)
         * @param precision the number of decimals considered
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         */
        //void concat(float value, int precision, bool fixed);

        /**
         * @brief Appends of an arithmetic value converted to a string
         * @param value the double number
         * @param base number representation (DEC/HEX)
         * @param precision the number of decimals considered
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         */
        void concat(double value, int precision=3, bool fixed=true);

        /**
         * @brief Appends
         * @param str
         */
        void concat(SkString str);

        /**
         * @brief Prepends characters before this string
         * @param CStr *the cstring to prepend
         */
        void prepend(CStr *cstr);

        /**
         * @brief Checks if the this string starts with comparing string
         * @param CStr *the source of the compare action
         * @return true if the string is equal to considered segment, otherwise false
         */
        bool startsWith(CStr *cstr);

        /**
         * @brief Checks if the this string ends with comparing string
         * @param CStr *the source of the compare action
         * @return true if the string is equal to considered segment, otherwise false
         */
        bool endsWith(CStr *cstr);

        /**
         * @brief Compares with a cstring
         * @param CStr *the string to compare
         * @return true if string are equal, otherwise false
         */
        bool equals(CStr *cstr);

        /**
         * @brief Compares with a string
         * @param str the string to compare
         * @return true if string are equal, otherwise false
         */
        bool equals(SkString &str);

        /**
         * @brief Compares with a std::string
         * @param str the string to compare
         * @return true if strings are equal, otherwise false
         */
        bool equals(string &str);

        static bool compare(CStr *str1, CStr* str2, uint64_t sz=0);

        /**
         * @brief Checks if the string is empty
         * @return true if the string is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Checks if the sstring is empty or NULL
         * @CStr *the string to check
         * @return true if the string is empty, otherwise false
         */
        static bool isEmpty(CStr *cstr);

        /**
         * @brief Return the string bytes-size
         * @return the string bytes-size
         */
        uint64_t size();

        /**
         * @brief Return the string bytes-size
         * @CStr *the string to check
         * @return the string bytes-size
         */
        static uint64_t size(CStr *cstr);

        /**
         * @brief Return the wide-characters lenght (supporting unicode and ASCII)
         * @warning unicode symbols and escape sequence lens are respectively 1 and 0
         * @return the valid character number
         */
        uint64_t length() const ;

        /**
         * @brief Return the wide-characters lenght (supporting unicode and ASCII)
         * @warning unicode symbols and escape sequence lens are respectively 1 and 0
         * @CStr *the string to calculate from
         * @return the valid character number
         */
        static uint64_t length(CStr *cstr);

        /**
         * @brief Removes spaces and special characters from start and end of the string
         */
        SkString &trim();

        /**
         * @brief Removes spaces duplications and special characters from the string
         */
        SkString &simplify();

        /**
         * @brief Splits a string using a character as separator
         * @param sep the separator character
         * @param splitted the reference of the strings container to fill
         */
        //FAILS ON GRABBING EMPTY LINEs; MUST REVIEW
        void split(char sep, SkAbstractList<SkString> &splitted, uint64_t max=0);

        /**
         * @brief Splits a string using a character as separator
         * @param sep the separator string
         * @param splitted the reference of the strings container to fill
         */
        //FAILS ON GRABBING ONE LINEs OVER THE REAL COUNT; MUST REVIEW
        void split(CStr *sep, SkAbstractList<SkString> &splitted, uint64_t max=0);

        /**
         * @brief Splits a string using a character as separator
         * @param sep the separator string
         * @param splitted the reference of the strings container to fill
         */
        void parsePair(CStr *sep, SkAbstractList<SkString> &splitted);

        /**
         * @brief Converts the string to lower-case
         */
        SkString &toLowerCase();

        /**
         * @brief Converts the string to upper-case
         */
        SkString &toUpperCase();

        /**
         * @brief Set the first character to upper-case
         */
        SkString &capitalize();

        /**
         * @brief Checks if the string contains a cstring
         * @param CStr *the string to check
         * @return true if the string is contained in this string
         */
        bool contains(CStr *cstr);

        /**
         * @brief Searchs for a cstring
         * @param CStr *the string to search
         * @param from the position to start from
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(CStr *cstr, uint64_t from=0);

        /**
         * @brief Gets the character at some position
         * @param index the position to use
         * @return the selected character, or 0 ('\0') if the position is not valid
         */
        char charAt(uint64_t index);

        /**
         * @brief Replaces occurrences of a cstring inside this string
         * @param CStr *the string to replace
         * @param substitute the substitute string
         * @param from the position to start from
         * @param onlyFirstOccurrence if true replaces only the first found occurrence
         */
        SkString &replace(CStr *cstr, CStr *substitute, uint64_t from=0, bool onlyFirstOccurrence=false);

        /**
         * @brief Removes characters from the end
         * @param count the number of characters to remove
         */
        SkString &chop(uint64_t count);

        /**
         * @brief Converts the string to an integer value
         * @return the integer value, or 0 if the string is not a number
         */
        int toInt();

        /**
         * @brief Converts the string to an long value
         * @return the long value, or 0 if the string is not a number
         */
        long toLong();

        /**
         * @brief Converts the string to an long long value
         * @return the long long value, or 0 if the string is not a number
         */
        long long toLongLong();

        /**
         * @brief Converts the string to a float value
         * @return the float value, or 0 if the string is not a number
         */
        float toFloat();

        /**
         * @brief Converts the string to a double value
         * @return the double value, or 0 if the string is not a number
         */
        double toDouble();

        /**
         * @brief Copies the string to a characters pointer
         * @param data character pointer of the right length
         * @param len the length to copy; if it is 0 all the content will be copied
         */
        void toCharArray(char *data, uint64_t len=0);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the integer number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(int value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the unsigned integer number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(unsigned int value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the long number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(long value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the unsigned long number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(unsigned long value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the long long number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(long long value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the unsigned long long number
         * @param base number representation (DEC/HEX)
         * @return the built string
         */
        static SkString number(unsigned long long value, int base=DEC);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the float number
         * @param base number representation (DEC/HEX)
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         * @return the built string
         */
        static SkString number(float value, int precision=3, bool fixed=false);

        /**
         * @brief Builds a string from arithmetic value
         * @param value the double number
         * @param base number representation (DEC/HEX)
         * @param fixed if true the representation will show all precision decimals
         * also if they are 0
         * @return the built string
         */
        static SkString number(double value, int precision=3, bool fixed=true);

        //static SkString localTime(time_t n);

        // !!! ALL FORMATTING STATIC METHs MUST BE REVISIONED !!!

        /**
         * @brief Facility meth to build a formatted string based on a byte size with units and its multiplier
         * @param bytes the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value
         * the format is "value (littleValue)" -> ex: 1024 B (1 kB)
         * @return the built string with formatted value
         */
        static SkString formatBytesSize(uint64_t bytes, bool onlyLittleValue=true);

        /**
         * @brief Facility meth to build a formatted string based on a bytes-speed with units and its multiplier
         * @param speed the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value;
         * the format is "value (littleValue)" -> ex: 1024 B/s (1 kB/s)
         * @return the built string with formatted value
         */
        static SkString formatBytesPerSec(uint64_t speed, bool onlyLittleValue=true);

        /**
         * @brief Facility meth to build a formatted string based on a bit size with unit and its multiplier
         * @param bits the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value
         * the format is "value (littleValue)" -> ex: 1024 b (1 kb)
         * @return the built string with formatted value
         */
        static SkString formatBitsSize(uint64_t bits, bool onlyLittleValue=true);

        /**
         * @brief Facility meth to build a formatted string based on a baud with unit and its multiplier
         * @param speed the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value
         * the format is "value (littleValue)" -> ex: 1024 b/s (1 kb/s)
         * @return the built string with formatted value
         */
        static SkString formatBitsPerSec(uint64_t speed, bool onlyLittleValue=true);

        /**
         * @brief Facility meth to build a formatted string based on a frequency with unit and its multiplier
         * @param frequency the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value
         * the format is "value (littleValue)" -> ex: 1000 Hz (1 kHz)
         * @return the built string with formatted value
         */
        static SkString formatHz(uint64_t frequency, bool onlyLittleValue=true);

        /**
         * @brief Facility meth to build a formatted string based on a value with unit and its multiplier
         * @param value the quantity to format
         * @param onlyLittleValue if true it will contain only the little-value
         * @param udm the unit string to use
         * @param multKilo the multiplier
         * @return the built string with formatted value
         */
        static SkString formatValuesWithUdm(uint64_t value, bool onlyLittleValue, CStr *udm, uint64_t multKilo);

    private:
        void prepareStreamToAddInt(stringstream &stream, int base);
};

#endif // SKSTRING_H
