#ifndef SKBINARYNODE_H
#define SKBINARYNODE_H

#include "skstack.h"
#include "skpair.h"
#include "sktreenode.h"

template <typename K, typename T>
class SkBinaryNode extends SkTreeNode<K, T>
{
    public:
        SkBinaryNode();
        ~SkBinaryNode();

        void setLeftChild(SkBinaryNode<K, T> *z);
        void setRightChild(SkBinaryNode<K, T> *z);

        bool isLeftChild();
        bool isRightChild();

        SkBinaryNode<K, T> *leftChild();
        SkBinaryNode<K, T> *rightChild();

        SkBinaryNode<K, T> *brother();
        SkBinaryNode<K, T> *uncle();
};

template <typename K, typename T>
SkBinaryNode<K, T>::SkBinaryNode() : SkTreeNode<K, T>(2)
{
}

template <typename K, typename T>
SkBinaryNode<K, T>::~SkBinaryNode()
{
}

template <typename K, typename T>
void SkBinaryNode<K, T>::setLeftChild(SkBinaryNode<K, T> *z)
{
    this->chld.first() = z;
}

template <typename K, typename T>
void SkBinaryNode<K, T>::setRightChild(SkBinaryNode<K, T> *z)
{
    this->chld.last() = z;
}

template <typename K, typename T>
bool SkBinaryNode<K, T>::isLeftChild()
{
    if (this->father())
        return static_cast<SkBinaryNode<K, T> *>(this->father())->leftChild() == this;

    return false;
}

template <typename K, typename T>
bool SkBinaryNode<K, T>::isRightChild()
{
    if (this->father())
        return static_cast<SkBinaryNode<K, T> *>(this->father())->rightChild() == this;

    return false;
}

template <typename K, typename T>
SkBinaryNode<K, T> *SkBinaryNode<K, T>::leftChild()
{
    if (!this->chld.first())
        return nullptr;

    return static_cast<SkBinaryNode<K, T> *>(this->chld.first());
}

template <typename K, typename T>
SkBinaryNode<K, T> *SkBinaryNode<K, T>::rightChild()
{
    if (!this->chld.last())
        return nullptr;

    return static_cast<SkBinaryNode<K, T> *>(this->chld.last());
}

template <typename K, typename T>
SkBinaryNode<K, T> *SkBinaryNode<K, T>::brother()
{
    if (this->father())
    {
        if (isLeftChild())
            return static_cast<SkBinaryNode<K, T> *>(this->father())->rightChild();

        return static_cast<SkBinaryNode<K, T> *>(this->father())->leftChild();
    }

    return nullptr;
}

template <typename K, typename T>
SkBinaryNode<K, T> *SkBinaryNode<K, T>::uncle()
{
    SkBinaryNode<K, T> *g = static_cast<SkBinaryNode<K, T> *>(this->grandFather());

    if (!g)
        return nullptr;

    if (static_cast<SkBinaryNode<K, T> *>(this->father())->isLeftChild())
        return g->rightChild();

    return g->leftChild();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkBinaryTreeVisitMode
{
    BTV_DEPTH_PRE,
    BTV_DEPTH_SIMMETRIC,
    BTV_DEPTH_POST,
    BTV_BREADTH
};

template <typename K, typename T>
class SPECIALK SkBinaryTreeVisit extends SkAbstractIterator<SkPair<K, T>>
{
    public:
        SkBinaryTreeVisit(SkBinaryNode<K, T> *root, SkBinaryTreeVisitMode mode=SkBinaryTreeVisitMode::BTV_DEPTH_SIMMETRIC);

        void setVisitMode(SkBinaryTreeVisitMode mode);
        bool isValid() override;
        void reset() override;
        bool hasNext() override;
        bool next() override;
        SkPair<K, T> &item() override;
        void setValue(T *val);
        void setLabel(CStr *label);
        CStr *label();
        bool isNil();
        void visitToList(SkAbstractList<SkBinaryNode<K, T> *> &l);
        SkBinaryTreeVisit<K, T> &operator ++ () override;

    private:
        SkBinaryTreeVisitMode m;
        SkPair<K, T> nullPair;
        SkPair<K, T> currentPair;
        SkBinaryNode<K, T> *current;
        SkBinaryNode<K, T> *r;
        SkStack<SkBinaryNode<K, T> *> s;
        SkQueue<SkBinaryNode<K, T> *> q;

        SkPair<K, T> &np() {return nullPair = SkPair<K, T>();}
};

template <typename K, typename T>
SkBinaryTreeVisit<K, T>::SkBinaryTreeVisit(SkBinaryNode<K, T> *root, SkBinaryTreeVisitMode mode)
{
    r = root;
    setVisitMode(mode);
}

template <typename K, typename T>
void SkBinaryTreeVisit<K, T>::setVisitMode(SkBinaryTreeVisitMode mode)
{
    m = mode;
    reset();
}

template <typename K, typename T>
bool SkBinaryTreeVisit<K, T>::isValid()
{
    return (current != nullptr);
}

template <typename K, typename T>
void SkBinaryTreeVisit<K, T>::reset()
{
    current = nullptr;

    s.clear();
    q.clear();

    if (m == SkBinaryTreeVisitMode::BTV_DEPTH_SIMMETRIC)
    {
        current = r;

        while(!current->isNil())
        {
            s.push(current);
            current = current->leftChild();
        }
    }

    else if (m == SkBinaryTreeVisitMode::BTV_DEPTH_PRE)
        s.push(r);

    else if (m == SkBinaryTreeVisitMode::BTV_DEPTH_POST)
    {
        SkBinaryTreeVisit<K, T> v(r, SkBinaryTreeVisitMode::BTV_BREADTH);

        while(v.next())
            v.current->setVisited(false);

        s.push(r);
    }

    else if (m == SkBinaryTreeVisitMode::BTV_BREADTH)
        q.enqueue(r);
}

template <typename K, typename T>
bool SkBinaryTreeVisit<K, T>::hasNext()
{
    if (m == SkBinaryTreeVisitMode::BTV_BREADTH)
        return !q.isEmpty();

    return !s.isEmpty();
}

template <typename K, typename T>
bool SkBinaryTreeVisit<K, T>::next()
{
    if (!hasNext())
    {
        current = nullptr;
        return false;
    }

    if (m == SkBinaryTreeVisitMode::BTV_DEPTH_SIMMETRIC)
    {
        current = s.pop();

        if (!current->rightChild()->isNil())
        {
            SkBinaryNode<K, T> *p = current->rightChild();

            while(!p->isNil())
            {
                s.push(p);
                p = p->leftChild();
            }
        }
    }

    else if (m == SkBinaryTreeVisitMode::BTV_DEPTH_PRE)
    {
        current = s.pop();

        if (!current->rightChild()->isNil())
            s.push(current->rightChild());

        if (!current->leftChild()->isNil())
            s.push(current->leftChild());
    }

    else if (m == SkBinaryTreeVisitMode::BTV_DEPTH_POST)
    {
        if (s.top()->isVisited())
            current = s.pop();

        else
        {
            while(!s.top()->rightChild()->isNil() || !s.top()->leftChild()->isNil())
            {
                SkBinaryNode<K, T> *curr = s.top();
                curr->setVisited(true);

                if (!curr->rightChild()->isNil())
                    s.push(curr->rightChild());

                if (!curr->leftChild()->isNil())
                    s.push(curr->leftChild());
            }

            current = s.pop();
        }
    }

    else if (m == SkBinaryTreeVisitMode::BTV_BREADTH)
    {
        current = q.dequeue();

        SkBinaryNode<K, T> *n = nullptr;

        if (!current->leftChild()->isNil())
            q.enqueue(n = current->leftChild());

        if (!current->rightChild()->isNil())
            q.enqueue(n = current->rightChild());
    }

    return true;
}

template <typename K, typename T>
SkPair<K, T> &SkBinaryTreeVisit<K, T>::item()
{
    if (!isValid())
        return np();

    currentPair.set(current->key(), current->value());
    return currentPair;
}

template <typename K, typename T>
void SkBinaryTreeVisit<K, T>::setValue(T *val)
{
    if (!isValid())
        return;

    current->setValue(val);
}

template <typename K, typename T>
void SkBinaryTreeVisit<K, T>::setLabel(CStr *label)
{
    if (!isValid())
        return;

    current->label = label;
}

template <typename K, typename T>
CStr *SkBinaryTreeVisit<K, T>::label()
{
    if (!isValid())
        return nullptr;

    return current->label();
}

template <typename K, typename T>
bool SkBinaryTreeVisit<K, T>::isNil()
{
    if (!isValid())
        return false;

    return current->isNil();
}

template <typename K, typename T>
void SkBinaryTreeVisit<K, T>::visitToList(SkAbstractList<SkBinaryNode<K, T> *> &l)
{
    while(next())
        l << current;
}

template <typename K, typename T>
SkBinaryTreeVisit<K, T> &SkBinaryTreeVisit<K, T>::operator ++ ()
{
    next();
    return *this;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKBINARYNODE_H
