#include "skarraycast.h"

SkArrayCast::SkArrayCast()
{
    unset();
}

void SkArrayCast::set(void *ptr, uint64_t size)
{
    dataPtr = ptr;
    sz = size;
}

void SkArrayCast::unset()
{
    dataPtr = nullptr;
    sz = 0;
}

uint64_t SkArrayCast::size()
{
    return sz;
}

uint64_t SkArrayCast::count(uint64_t elementByteSize)
{
    if (elementByteSize == 0 || sz == 0 || sz % elementByteSize > 0)
        return 0;

    return sz/elementByteSize;
}

bool SkArrayCast::isEmpty()
{
    return (sz == 0);
}

bool *SkArrayCast::toBool()
{
    return SkArrayCast::toBool(dataPtr);
}

char *SkArrayCast::toChar()
{
    return SkArrayCast::toChar(dataPtr);
}

int8_t *SkArrayCast::toInt8()
{
    return SkArrayCast::toInt8(dataPtr);
}

uint8_t *SkArrayCast::toUInt8()
{
    return SkArrayCast::toUInt8(dataPtr);
}

int16_t *SkArrayCast::toInt16()
{
    return SkArrayCast::toInt16(dataPtr);
}

uint16_t *SkArrayCast::toUInt16()
{
    return SkArrayCast::toUInt16(dataPtr);
}

int32_t *SkArrayCast::toInt32()
{
    return SkArrayCast::toInt32(dataPtr);
}

uint32_t *SkArrayCast::toUInt32()
{
    return SkArrayCast::toUInt32(dataPtr);
}

float *SkArrayCast::toFloat()
{
    return SkArrayCast::toFloat(dataPtr);
}

int64_t *SkArrayCast::toInt64()
{
    return SkArrayCast::toInt64(dataPtr);
}

uint64_t *SkArrayCast::toUInt64()
{
    return SkArrayCast::toUInt64(dataPtr);
}

double *SkArrayCast::toDouble()
{
    return SkArrayCast::toDouble(dataPtr);
}

void *SkArrayCast::data()
{
    return dataPtr;
}

bool *SkArrayCast::toBool(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<bool *>(ptr);
}

char *SkArrayCast::toChar(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<char *>(ptr);
}

CStr *SkArrayCast::toCStr(CVoid *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<CStr *>(ptr);
}

int8_t *SkArrayCast::toInt8(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<int8_t *>(ptr);
}

uint8_t *SkArrayCast::toUInt8(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<uint8_t *>(ptr);
}

int16_t *SkArrayCast::toInt16(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/2;
    return SkArrayCast::toInt16(ptr);
}

uint16_t *SkArrayCast::toUInt16(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/2;
    return SkArrayCast::toUInt16(ptr);
}

int32_t *SkArrayCast::toInt32(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/4;
    return SkArrayCast::toInt32(ptr);
}

uint32_t *SkArrayCast::toUInt32(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/4;
    return SkArrayCast::toUInt32(ptr);
}

float *SkArrayCast::toFloat(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/4;
    return SkArrayCast::toFloat(ptr);
}

int64_t *SkArrayCast::toInt64(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/8;
    return SkArrayCast::toInt64(ptr);
}

uint64_t *SkArrayCast::toUInt64(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/8;
    return SkArrayCast::toUInt64(ptr);
}

double *SkArrayCast::toDouble(void *ptr, uint64_t size, uint64_t &count)
{
    if (!ptr)
    {
        count = 0;
        return nullptr;
    }

    count = size/8;
    return SkArrayCast::toDouble(ptr);
}

int16_t *SkArrayCast::toInt16(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<int16_t *>(ptr);
}

uint16_t *SkArrayCast::toUInt16(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<uint16_t *>(ptr);
}

int32_t *SkArrayCast::toInt32(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<int32_t *>(ptr);
}

uint32_t *SkArrayCast::toUInt32(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<uint32_t *>(ptr);
}

float *SkArrayCast::toFloat(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<float *>(ptr);
}

int64_t *SkArrayCast::toInt64(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<int64_t *>(ptr);
}

uint64_t *SkArrayCast::toUInt64(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<uint64_t *>(ptr);
}

double *SkArrayCast::toDouble(void *ptr)
{
    if (!ptr)
        return nullptr;

    return static_cast<double *>(ptr);
}
