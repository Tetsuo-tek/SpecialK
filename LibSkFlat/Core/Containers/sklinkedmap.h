#ifndef SKLINKEDMAP_H
#define SKLINKEDMAP_H

#include "sklist.h"
#include "abstract/skabstractmap.h"

template <typename K, typename V>
class SPECIALK SkLinkedMap extends SkAbstractMap<K, V>
{
    public:
        SkLinkedMap();
        SkLinkedMap(const SkLinkedMap<K, V> &source);
        ~SkLinkedMap();

        void add(const K &key, const V &value) override;
        void add(SkLinkedMap<K, V> &m);
        SkPair<K, V> remove(const K &key) override;

        bool contains(const K &key) override;

        //K &key(const V &value) override;
        void keys(SkAbstractList<K> &l) override;

        V &value(const K &key) override;
        void values(SkAbstractList<V> &l) override;

        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t countValues(V &v) override;
        uint64_t size() override;

        void swap(SkLinkedMap<K, V> &other);

        SkLinkedMap<K, V> &operator =  (const SkLinkedMap<K, V> &source);
        bool              operator  == (const SkLinkedMap<K, V> &operand);
        V                 &operator [] (const K &key) override;

        typename SkList<SkPair<K, V>>::SkIterator *iterator() override;

        SkList<SkPair<K, V>> &getInternalList();

        K &nk()
        {return nullKey = K();}

        V &nv()
        {return nullValue = V();}

    private:
        SkList<SkPair<K, V>> map;
        K nullKey;
        V nullValue;
};

template <typename K, typename V>
SkLinkedMap<K, V>::SkLinkedMap()
{}

template <typename K, typename V>
SkLinkedMap<K, V>::SkLinkedMap(const SkLinkedMap<K, V> &source)
{
    map = source.map;
}

template <typename K, typename V>
SkLinkedMap<K, V>::~SkLinkedMap()
{}

template <typename K, typename V>
void SkLinkedMap<K, V>::add(const K &key, const V &value)
{
    SkPair<K, V> p(key, value);
    map.append(p);
}

template <typename K, typename V>
void SkLinkedMap<K, V>::add(SkLinkedMap<K, V> &m)
{
    map.append(m.map);
}

template <typename K, typename V>
SkPair<K, V> SkLinkedMap<K, V>::remove(const K &key)
{
    SkAbstractListIterator<SkPair<K,V>> *itr = map.iterator();
    SkPair<K, V> p;

    while(itr->next())
        if (itr->item().key() == key)
        {
            p = itr->item();
            itr->remove();
            break;
        }

    delete itr;
    return p;
}

template <typename K, typename V>
bool SkLinkedMap<K, V>::contains(const K &key)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();
    bool ok = false;

    while(itr->next())
        if (itr->item().key() == key)
        {
            ok = true;
            break;
        }

    delete itr;
    return ok;
}

/*template <typename K, typename V>
K &SkLinkedMap<K, V>::key(const V &value)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();

    while(itr->next())
        if (itr->item().value() == value)
        {
            K &k =itr->item().key();
            delete itr;
            return k;
        }

    delete itr;
    return nk();
}*/

template <typename K, typename V>
void SkLinkedMap<K, V>::keys(SkAbstractList<K> &l)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();

    while(itr->next())
        l.append(itr->item().key());

    delete itr;
}

template <typename K, typename V>
V &SkLinkedMap<K, V>::value(const K &key)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();

    while(itr->next())
        if (itr->item().key() == key)
        {
            V &v =itr->item().value();
            delete itr;
            return v;
        }

    delete itr;
    return nv();
}

template <typename K, typename V>
void SkLinkedMap<K, V>::values(SkAbstractList<V> &l)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();

    while(itr->next())
        l.append(itr->item().value());

    delete itr;
}

template <typename K, typename V>
void SkLinkedMap<K, V>::clear()
{
    map.clear();
}

template <typename K, typename V>
bool SkLinkedMap<K, V>::isEmpty()
{
    return map.isEmpty();
}

template <typename K, typename V>
uint64_t SkLinkedMap<K, V>::count()
{
    return map.count();
}

template <typename K, typename V>
uint64_t SkLinkedMap<K, V>::countValues(V &v)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        if (itr->item().value() == v)
            i++;

    delete itr;
    return i;
}

template <typename K, typename V>
uint64_t SkLinkedMap<K, V>::size()
{
    return map.size();
}

template <typename K, typename V>
void SkLinkedMap<K, V>::swap(SkLinkedMap<K, V> &other)
{
    map.swap(other.map);
}

template <typename K, typename V>
SkLinkedMap<K, V> &SkLinkedMap<K, V>::operator = (const SkLinkedMap<K, V> &source)
{
    map = source.map;
}

template <typename K, typename V>
bool SkLinkedMap<K, V>::operator == (const SkLinkedMap<K, V> &operand)
{
    return (map == operand.map);
}

template <typename K, typename V>
V &SkLinkedMap<K, V>::operator [] (const K &key)
{
    SkAbstractListIterator<SkPair<K, V>> *itr = iterator();

    while(itr->next())
        if (itr->item().key() == key)
        {
            V &v = itr->item().value();
            delete itr;
            return v;
        }

    delete itr;

    add(key, nv());
    return map.last().value();
}

template <typename K, typename V>
typename SkList<SkPair<K, V>>::SkIterator *SkLinkedMap<K, V>::iterator()
{
    return map.iterator();
}

template <typename K, typename V>
SkList<SkPair<K, V>> &SkLinkedMap<K, V>::getInternalList()
{
    return map;
}

#endif // SKLINKEDMAP_H
