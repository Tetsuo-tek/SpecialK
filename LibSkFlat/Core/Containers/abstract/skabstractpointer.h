#ifndef SKABSTRACTPOINTER_H
#define SKABSTRACTPOINTER_H

#include "skabstractcontainer.h"

template <typename T>
class SPECIALK SkAbstractPointer extends SkAbstractContainer
{
    public:
        virtual const T *data() = 0;
        virtual bool destroy() = 0;
        virtual uint64_t count() = 0;
};

#endif // SKABSTRACTPOINTER_H
