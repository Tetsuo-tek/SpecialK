#ifndef SKABSTRACTARRAY_H
#define SKABSTRACTARRAY_H

#include "skabstractlist.h"
#include "Core/Containers/sksort.h"

template <typename T>
class SPECIALK SkAbstractArray extends SkAbstractList<T>
{
    public:
        //virtual void sort(SkOrderAlgo algo=SkOrderAlgo::OA_QUICK, Comparator<T> *c=nullptr) = 0;

        virtual void setCapacity(uint64_t count) = 0;
        virtual uint64_t capacity() = 0;
        virtual uint64_t capacitySize() = 0;

        //virtual bool set(const T *, uint64_t, uint64_t) = 0;

        virtual void reverse();

        virtual T *data() = 0;

        virtual T                   &operator [] (uint64_t index) = 0;
        virtual SkAbstractList<T>   &operator << (const T &item) = 0;
};

template <typename T>
void SkAbstractArray<T>::reverse()
{
    T *p = data();
    uint64_t middle = floor(this->count()/2);
    T left;

    for(uint64_t i=0; i<=middle; i++)
    {
        left = p[i];
        p[i] = p[this->count()-1-i];
        p[this->count()-1-i] = left;
    }
}

#endif // SKABSTRACTARRAY_H
