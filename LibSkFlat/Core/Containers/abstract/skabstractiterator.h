#ifndef SKABSTRACTITERATOR_H
#define SKABSTRACTITERATOR_H

#include "Core/Object/skflatobject.h"

template <typename T>
class SPECIALK SkAbstractIterator extends SkFlatObject
{
    public:
        virtual bool isValid() = 0;

        virtual bool hasNext() = 0;
        virtual bool next() = 0;

        virtual T &item() = 0;

        virtual void reset() = 0;

        virtual SkAbstractIterator &operator ++ () = 0;
};

#endif // SKABSTRACTITERATOR_H
