#ifndef SKABSTRACTCONTAINER_H
#define SKABSTRACTCONTAINER_H

#include "Core/Object/skflatobject.h"

class SPECIALK SkAbstractContainer extends SkFlatObject
{
    public:
        virtual void clear() = 0;
        virtual bool isEmpty() = 0;
        virtual uint64_t size() = 0;
};

#endif // SKABSTRACTCONTAINER_H
