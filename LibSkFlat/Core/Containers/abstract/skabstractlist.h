#ifndef SKABSTRACTLIST_H
#define SKABSTRACTLIST_H

#include "skabstractcontainer.h"
#include "skabstractiterator.h"

template <typename T>
class SPECIALK SkAbstractListIterator extends SkAbstractIterator<T>
{
    public:
        virtual int64_t index() = 0;
        virtual bool insert(const T &e) = 0;//if the list is empty it will perform append
        virtual T remove() = 0;

        virtual bool hasPrev() = 0;
        virtual bool prev() = 0;

        virtual bool set(const T &e) = 0;

        virtual bool goToBegin() = 0;
        virtual bool goToEnd() = 0;

        virtual bool atBegin() = 0;
        virtual bool atEnd() = 0;

        virtual SkAbstractListIterator &operator ++ () = 0;
        virtual SkAbstractListIterator &operator -- () = 0;
};

template <typename T>
class SPECIALK SkAbstractList extends SkAbstractContainer
{
    public:
        virtual void append(const T &) = 0;
        virtual void append(SkAbstractList<T> &, uint64_t=0) = 0;

        virtual void prepend(const T &) = 0;
        virtual void prepend(SkAbstractList<T> &, uint64_t=0) = 0;

        virtual bool insert(const T &, uint64_t) = 0;
        virtual bool insert(SkAbstractList<T> &, uint64_t, uint64_t=0) = 0;

        virtual bool contains(const T &) = 0;
        virtual int64_t indexOf(const T &, uint64_t=0) = 0;

        virtual bool set(const T &, uint64_t) = 0;
        virtual T &get(uint64_t) = 0;
        T &at(uint64_t index) {return get(index);}
        virtual T &first() = 0;
        virtual T &last() = 0;

        //return removing item
        virtual T remove(const T &) = 0;
        virtual T removeAt(uint64_t) = 0;
        virtual T removeFirst() = 0;
        virtual T removeLast() = 0;
        virtual uint64_t remove(SkAbstractList<T> &) = 0;
        virtual uint64_t remove(T *, uint64_t) = 0; //T[] array, elements-count (NOT SIZE)

        virtual void toArray(T *array) = 0;
        virtual void toList(SkAbstractList<T> &l) = 0;

        virtual SkAbstractListIterator<T> *iterator(int64_t=-1) = 0;//starting pos

        virtual uint64_t count() = 0;
        virtual uint64_t count(const T &) = 0;

        virtual T                   &operator [] (uint64_t index) = 0;
        virtual SkAbstractList<T>   &operator << (const T &item) = 0;
};

#endif // SKABSTRACTLIST_H
