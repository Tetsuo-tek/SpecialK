#ifndef SKABSTRACTMAP_H
#define SKABSTRACTMAP_H

#include "Core/Containers/skpair.h"
#include "skabstractcontainer.h"
#include "skabstractiterator.h"
#include "skabstractlist.h"

template <typename K, typename V>
class SPECIALK SkAbstractMap extends SkAbstractContainer
{
    public:
        virtual void add(const K &key, const V &value) = 0;
        virtual SkPair<K, V> remove(const K &key) = 0;

        virtual bool contains(const K &key) = 0;

        //virtual K &key(const V &value) = 0;
        virtual void keys(SkAbstractList<K> &l) = 0;

        virtual V &value(const K &key) = 0;
        virtual void values(SkAbstractList<V> &l) = 0;

        virtual SkAbstractIterator<SkPair<K, V>> *iterator() = 0;

        virtual uint64_t count() = 0;
        virtual uint64_t countValues(V &) = 0;

        virtual V &operator [] (const K &key) = 0;
};

#endif // SKABSTRACTMAP_H
