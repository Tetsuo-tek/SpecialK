/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKARGSMAP_H
#define SKARGSMAP_H

#include "skvariant.h"
#include "skmap.h"
#include "skstringlist.h"

/**
  * @brief SkArgsMap
  * it is a specialized version of SkVariantMap used with string keys;
  * it has also JSON capabilities
*/
class SPECIALK SkArgsMap extends SkAbstractMap<SkString, SkVariant>
{
    public:
        /**
         * @brief Constructor
        */
        SkArgsMap();
        SkArgsMap(const SkArgsMap &source);
        SkArgsMap(SkMap<SkString, SkVariant> &source);

        /**
         * @brief Checks if exists a key in the map
         * @param label the key name to check
         * @return true if it exists, otherwise false
        */
        bool contains(const SkString &label) override;

        /**
         * @brief Inserts new pair into the map; if the key already exists
         * the value will be substituted
         * @param label the key name (as string) to insert
         * @param val the variable to insert
         * @return the inserted value reference
        */
        SkVariant &insert(const SkString &label, const SkVariant &val);

        /**
         * @brief Inserts new pair into the map, with an empty value
         * @param label the key name to insert
         * @return the inserted empty value reference
        */
        SkVariant &insert(const SkString &label);

        /**
         * @brief Adds the content of another map
         * @param m the adding map
        */
        void insertMap(SkArgsMap &m);

        /**
         * @brief Gets the the value associated to a key
         * @param label the key name to grabbing value
         * @return the grabbed value reference, otherwise an empty variable
        */
        SkVariant &value(const SkString &label) override;

        /**
         * @brief Fills the string list with all values stored in the map
         * @param valuesListList the list to fill with existing keys
        */
        void values(SkAbstractList<SkVariant> &valuesList) override;

        /**
         * @brief Removes the the pair associated to a key
         * @param val the value you want to grab associated key
         * @return the key of the value if it exists, otherwise an empty string
        */
        //SkString &key(const SkVariant &val) override;

        /**
         * @brief Fills the string list with all keys stored in the map
         * @param keysList the list to fill with existing keys
        */
        void keys(SkAbstractList<SkString> &keysList) override;

        /**
         * @brief Converts the map content to a JSON document
         * @param json the string to fill with JSON document
         * @param indented true for an human readable document
         * @param spaces spaces for indentation
         * @return true if there are not errors, otherwise false
        */
        bool toString(SkString &json, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Converts a JSON document to a map
         * @param json the JSON document to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromString(CStr *json);

    #if defined(ENABLE_SKAPP)
        /**
         * @brief Converts a JSON file to a map
         * @param filePath the JSON file to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromFile(SkString &filePath);

        /**
         * @brief Converts a JSON file to a map
         * @param filePath the JSON file to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromFile(CStr *filePath);
    #endif

        // // From SkMap
        void add(const SkString &key, const SkVariant &value) override;
        SkPair<SkString, SkVariant> remove(const SkString &key) override;

        void reverse();

        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t countValues(SkVariant &v) override;
        uint64_t size() override;

        void swap(SkArgsMap &other);
        typename SkVector<SkPair<SkString, SkVariant>>::SkIterator *iterator() override;
        // //

        /**
         * @brief Index operator to simplify the map content access, for insert or getting variable
         * @param label the JSON file to convert in a map
         * @return the grabbed, if the label exists, or inserted variable reference
        */
        //SkVariant &operator [] (CStr *label);

        /**
         * @brief Index operator to simplify the map content access, for insert or getting variable
         * @param label the JSON file to convert in a map
         * @return the grabbed, if the label exists, or inserted variable reference
        */
       // SkVariant &operator [] (SkString &label);

        SkArgsMap &operator =  (const SkArgsMap &source);
        bool      operator  == (const SkArgsMap &operand);
        SkVariant &operator [] (const SkString &key) override;

        SkMap<SkString, SkVariant> &getInternalMap();
        SkVector<SkPair<SkString, SkVariant>> &getInternalVector();

        SkString &nk()
        {return subMap.nk();}

        SkVariant &nv()
        {return subMap.nv();}

    private:
        SkMap<SkString, SkVariant> subMap;
};


//DEPRECATED!

#if defined(UMBA)
class SPECIALK SkArgsMap extends SkVariantMap
{
    public:
        /**
         * @brief Constructor
        */
        SkArgsMap();

        /**
         * @brief Checks if exists a key in the map
         * @param label the key name to check
         * @return true if it exists, otherwise false
        */
        bool contains(CStr *label);

        /**
         * @brief Checks if exists a key in the map
         * @param label the key name to check
         * @return true if it exists, otherwise false
        */
        bool contains(SkString &label);

        /**
         * @brief Inserts new pair into the map; if the key already exists
         * the value will be substituted
         * @param label the key name (as string) to insert
         * @param val the variable to insert
         * @return the inserted value reference
        */
        SkVariant &insert(CStr *label, const SkVariant val);

        /**
         * @brief Inserts new pair into the map; if the key already exists
         * the value will be substituted
         * @param label the key name (as string) to insert
         * @param val the variable to insert
         * @return the inserted value reference
        */
        SkVariant &insert(SkString &label, const SkVariant val);

        /**
         * @brief Inserts new pair into the map; if the key already exists
         * the value will be substituted
         * @param label the key name (as generic variable) to insert
         * @param val the variable to insert
         * @return the inserted value reference
        */
        SkVariant &insert(const SkVariant label, const SkVariant val) override;

        /**
         * @brief Inserts new pair into the map, with an empty value
         * @param label the key name (as generic variable) to insert
         * @return the inserted empty value reference
        */
        SkVariant &insert(const SkVariant label);

        /**
         * @brief Adds the content of another map
         * @param m the adding map
        */
        void insertMap(SkArgsMap &m);

        /**
         * @brief Gets the the value associated to a key
         * @param label the key name to grabbing value
         * @return the grabbed value reference, otherwise an empty variable
        */
        SkVariant &value(CStr *label);

        /**
         * @brief Gets the the value associated to a key
         * @param label the key name to grabbing value
         * @return the grabbed value reference, otherwise an empty variable
        */
        SkVariant &value(SkString &label);

        /**
         * @brief Removes the the pair associated to a key
         * @param label the key name of the pair to remove
         * @return true if it exists (it's removable), otherwise false
        */
        bool remove(CStr *label);

        /**
         * @brief Removes the the pair associated to a key
         * @param label the key name of the pair to remove
         * @return true if it exists (it's removable), otherwise false
        */
        bool remove(SkString &label);

        /**
         * @brief Removes the the pair associated to a key
         * @param val the value you want to grab associated key
         * @return the key of the value if it exists, otherwise an empty string
        */
        SkString key(const SkVariant val);

        /**
         * @brief Fills the string list with all keys stored in the map
         * @param keysList the list to fill with existing keys
        */
        void keys(SkStringList &keysList);

        /**
         * @brief Converts the map content to a JSON document
         * @param json the string to fill with JSON document
         * @param indented true for an human readable document
         * @param spaces spaces for indentation
         * @return true if there are not errors, otherwise false
        */
        bool toString(SkString &json, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Converts a JSON document to a map
         * @param json the JSON document to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromString(SkString &json);

#if defined(SPECIALK_APPLICATION)
        /**
         * @brief Converts a JSON file to a map
         * @param filePath the JSON file to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromFile(SkString &filePath);

        /**
         * @brief Converts a JSON file to a map
         * @param filePath the JSON file to convert in a map
         * @return true if there are not errors, otherwise false
        */
        bool fromFile(CStr *filePath);
#endif

        /**
         * @brief Index operator to simplify the map content access, for insert or getting variable
         * @param label the JSON file to convert in a map
         * @return the grabbed, if the label exists, or inserted variable reference
        */
        SkVariant &operator [] (CStr *label);

        /**
         * @brief Index operator to simplify the map content access, for insert or getting variable
         * @param label the JSON file to convert in a map
         * @return the grabbed, if the label exists, or inserted variable reference
        */
        SkVariant &operator [] (SkString &label);
};

#endif

#endif // SKARGSMAP_H
