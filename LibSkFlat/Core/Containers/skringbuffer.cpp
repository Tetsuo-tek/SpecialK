#include "skringbuffer.h"
#include "skdatabuffer.h"
#include "skvariant.h"
#include "Core/sklogmachine.h"

DeclareWrapper(SkRingBuffer);

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkRingBuffer, setCapacity, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkRingBuffer, capacity, uint64_t)
DeclareMeth_INSTANCE_VOID(SkRingBuffer, setCanonicalSize, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getCanonicalSize, uint64_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, isCanonicalSizeReached, bool)

DeclareMeth_INSTANCE_RET(SkRingBuffer, addData, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, addData, 1, bool, Arg_Data(char), Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkRingBuffer, addString, Arg_StringRef)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addUInt8, Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getUInt8, uint8_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekUInt8, uint8_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addInt8, Arg_Int8)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getInt8, int8_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekInt8, int8_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addUInt16, Arg_UInt16)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getUInt16, uint16_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekUInt16, uint16_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addInt16, Arg_Int16)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getInt16, int16_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekInt16, int16_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addUInt32, Arg_UInt32)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getUInt32, uint32_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekUInt32, uint32_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addInt32, Arg_Int32)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getInt32, int32_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekInt32, int32_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addUInt64, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getUInt64, uint64_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekUInt64, uint64_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addInt64, Arg_Int64)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getInt64, int64_t)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekInt64, int64_t)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addFloat, Arg_Float)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getFloat, float)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekFloat, float)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, addDouble, Arg_Double)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getDouble, double)
DeclareMeth_INSTANCE_RET(SkRingBuffer, peekDouble, double)

DeclareMeth_INSTANCE_VOID(SkRingBuffer, flush, Arg_UInt64)

/*DeclareMeth_INSTANCE_RET(SkRingBuffer, getBuffer, char*, Arg_UInt64_REAL, Arg_Bool)
DeclareMeth_INSTANCE_RET(SkRingBuffer, getCustomBuffer, bool, Arg_Data(char), Arg_UInt64_REAL, Arg_Bool)*/

DeclareMeth_INSTANCE_RET(SkRingBuffer, getCanonicalBuffer, bool, Arg_Data(char), Arg_Bool)
DeclareMeth_INSTANCE_RET(SkRingBuffer, copyTo, bool, Arg_Custom(SkRingBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, copyTo, 1, bool, *Arg_Custom(SkDataBuffer), Arg_UInt64, Arg_Bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, copyTo, 2, bool, Arg_StringRef, Arg_UInt64, Arg_Bool)

// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkRingBuffer)
{
    SetClassSuper(SkRingBuffer, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkRingBuffer, setCapacity);
    AddMeth_INSTANCE_RET(SkRingBuffer, capacity);
    AddMeth_INSTANCE_VOID(SkRingBuffer, setCanonicalSize);
    AddMeth_INSTANCE_RET(SkRingBuffer, getCanonicalSize);
    AddMeth_INSTANCE_RET(SkRingBuffer, isCanonicalSizeReached);

    AddMeth_INSTANCE_RET(SkRingBuffer, addData);
    AddMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, addData, 1);
    AddMeth_INSTANCE_VOID(SkRingBuffer, addString);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addUInt8);
    AddMeth_INSTANCE_RET(SkRingBuffer, getUInt8);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekUInt8);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addInt8);
    AddMeth_INSTANCE_RET(SkRingBuffer, getInt8);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekInt8);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addUInt16);
    AddMeth_INSTANCE_RET(SkRingBuffer, getUInt16);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekUInt16);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addInt16);
    AddMeth_INSTANCE_RET(SkRingBuffer, getInt16);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekInt16);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addUInt32);
    AddMeth_INSTANCE_RET(SkRingBuffer, getUInt32);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekUInt32);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addInt32);
    AddMeth_INSTANCE_RET(SkRingBuffer, getInt32);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekInt32);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addUInt64);
    AddMeth_INSTANCE_RET(SkRingBuffer, getUInt64);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekUInt64);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addInt64);
    AddMeth_INSTANCE_RET(SkRingBuffer, getInt64);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekInt64);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addFloat);
    AddMeth_INSTANCE_RET(SkRingBuffer, getFloat);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekFloat);

    AddMeth_INSTANCE_VOID(SkRingBuffer, addDouble);
    AddMeth_INSTANCE_RET(SkRingBuffer, getDouble);
    AddMeth_INSTANCE_RET(SkRingBuffer, peekDouble);

    AddMeth_INSTANCE_VOID(SkRingBuffer, flush);
    /*AddMeth_INSTANCE_RET(SkRingBuffer, getBuffer);
    AddMeth_INSTANCE_RET(SkRingBuffer, getCustomBuffer);*/
    AddMeth_INSTANCE_RET(SkRingBuffer, getCanonicalBuffer);
    AddMeth_INSTANCE_RET(SkRingBuffer, copyTo);
    AddMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, copyTo, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkRingBuffer, copyTo, 2);
}

// // // // // // // // // // // // // // // // // // // // //

SkRingBuffer::SkRingBuffer(uint64_t capacity)
{
    CreateClassWrapper(SkRingBuffer);

    from = 0;
    to = 0;
    max = capacity;
    buffer = new char [max];
    canonicalSize = 0;
    maxGrowSz = capacity*50;
}

SkRingBuffer::~SkRingBuffer()
{
    delete [] buffer;
}

void SkRingBuffer::setCapacity(uint64_t capacity)
{
    bufferEx.lock();
    from = 0;
    to = 0;
    max = capacity;
    delete [] buffer;
    buffer = new char [max];
    canonicalSize = 0;
    bufferEx.unlock();
}

uint64_t SkRingBuffer::capacity()
{
    return max;
}

void SkRingBuffer::setMaxGrow(uint64_t maxGrowingSize)
{
    maxGrowSz = maxGrowingSize;
}

uint64_t SkRingBuffer::maxGrowingSize()
{
    return maxGrowSz;
}

void SkRingBuffer::setCanonicalSize(uint64_t size)
{
    canonicalSize = size;

    if (canonicalSize == 0)
        FlatWarning("SETTING CANONICAL-SZ ON 0");
}

uint64_t SkRingBuffer::getCanonicalSize()
{
    return canonicalSize;
}

bool SkRingBuffer::isCanonicalSizeReached()
{
    bool ok = false;

    bufferEx.lock();
    ok = (canonicalSize > 0 && calculateSize() >= canonicalSize);
    bufferEx.unlock();

    return ok;
}

bool SkRingBuffer::addData(CStr *data, uint64_t size)
{
    if (size == 0)
    {
        FlatWarning("ADDING DATA size ZERO");
        return false;
    }

    bool ok = false;

    bufferEx.lock();

    uint64_t curSz = calculateSize();
    uint64_t newSz = curSz + size;

    if (newSz > max && newSz <= maxGrowSz)
    {
        newSz *= 2;
        char *newBuffer = new char [newSz];

        if (curSz)
            getData(newBuffer, curSz, true);

        FlatWarning("Internal buffer has grown: " << max << "->" << newSz);

        from = 0;
        to = curSz;
        delete [] buffer;
        buffer = newBuffer;
        max = newSz;
    }

    uint64_t toInc = (to+size);

    if (toInc <= max)
    {
        memcpy(&buffer[to], data, size);

        if (from > to && from <= toInc)
            from = toInc + 1;
    }

    else
    {
        uint64_t partialSz = size - (toInc - max);
        memcpy(&buffer[to], data, partialSz);

        uint64_t remainSz = size - partialSz;
        memcpy(&buffer[0], &data[partialSz], remainSz);

        if (from <= remainSz)
            from = remainSz + 1;

        toInc = remainSz;
    }

    to = toInc;
    ok = true;

    bufferEx.unlock();

    onDataLoaded(size);

    return ok;
}

bool SkRingBuffer::addData(char *data, uint64_t size)
{
    return addData(static_cast<CStr *>(data), size);
}

void SkRingBuffer::addString(const SkString &s)
{
    addData(s.c_str(), s.length());
}

#include "Core/Containers/skarraycast.h"
#define CASTEDVALUES_DEF(SUFFIX, TYPE) \
    void SkRingBuffer::add ## SUFFIX (TYPE val) \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = val; \
        addData(SkArrayCast::toChar(dataVal), sizeof(TYPE)); \
    } \
    \
    TYPE SkRingBuffer::get ## SUFFIX () \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = 0; \
    \
        uint64_t sz = sizeof(TYPE);\
        if (getCustomBuffer(SkArrayCast::toChar(dataVal), sz, false)) \
            return dataVal[0]; \
     \
        return 0; \
    }\
    TYPE SkRingBuffer::peek ## SUFFIX () \
    { \
        TYPE dataVal[1]; \
        dataVal[0] = 0; \
    \
        uint64_t sz = sizeof(TYPE);\
        if (getCustomBuffer(SkArrayCast::toChar(dataVal), sz, true)) \
            return dataVal[0]; \
     \
        return 0; \
    }

CASTEDVALUES_DEF(UInt8, uint8_t)
CASTEDVALUES_DEF(Int8, int8_t)
CASTEDVALUES_DEF(UInt16, uint16_t)
CASTEDVALUES_DEF(Int16, int16_t)
CASTEDVALUES_DEF(UInt32, uint32_t)
CASTEDVALUES_DEF(Int32, int32_t)
CASTEDVALUES_DEF(UInt64, uint64_t)
CASTEDVALUES_DEF(Int64, int64_t)
CASTEDVALUES_DEF(SizeT, uint64_t)
CASTEDVALUES_DEF(SSizeT, int64_t)
CASTEDVALUES_DEF(Float, float)
CASTEDVALUES_DEF(Double, double)

void SkRingBuffer::flush(uint64_t sz)
{
    if (sz == 0 || sz > size())
        sz = size();

    delete [] getBuffer(sz);
}

char *SkRingBuffer::getBuffer(uint64_t &size, bool peekData)
{
    bufferEx.lock();
    char *d = getData(nullptr, size, peekData);
    bufferEx.unlock();
    return d;
}

bool SkRingBuffer::getCustomBuffer(char *data, uint64_t &size, bool peekData)
{
    if (!data)
        return false;

    bufferEx.lock();
    char *d = getData(data, size, peekData);
    bufferEx.unlock();
    return (d!=nullptr);
}

bool SkRingBuffer::getCanonicalBuffer(char *data, bool peekData)
{
    if (!data)
        return false;

    if (!canonicalSize)
        return false;

    uint64_t csz = canonicalSize;
    return getCustomBuffer(data, csz, peekData);
}

bool SkRingBuffer::copyTo(SkRingBuffer *other, uint64_t size, bool peekData)
{
    bufferEx.lock();

    if (!size)
        size = calculateSize();

    char *d = getData(nullptr, size, peekData);

    bufferEx.unlock();

    other->addData(d, size);
    delete [] d;
    return (d!=nullptr);
}

bool SkRingBuffer::copyTo(SkDataBuffer &other, uint64_t size, bool peekData)
{
    bufferEx.lock();

    if (!size)
        size = calculateSize();

    char *d = getData(nullptr, size, peekData);

    bufferEx.unlock();

    other.append(d, size);
    delete [] d;
    return (d!=nullptr);
}

bool SkRingBuffer::copyTo(SkString &other, uint64_t size, bool peekData)
{
    bufferEx.lock();

    if (!size)
        size = calculateSize();

    char *d = getData(nullptr, size, peekData);

    bufferEx.unlock();

    other.append(d, size);
    delete [] d;
    return (d!=nullptr);
}

char *SkRingBuffer::getData(char *data, uint64_t &size, bool peekData)
{
    uint64_t sz = calculateSize();

    if (sz == 0)
    {
        FlatWarning("GETTING BUFFER EMPTY");
        return nullptr;
    }

    if (size == 0)
    {
        FlatWarning("SETTING requested size (was 0) TO " << sz);
        size = sz;
    }

    if (size == 0)
    {
        FlatWarning("GETTING size ZERO");
        return nullptr;
    }

    if (size > sz)
    {
        FlatWarning("NOT ENOUGH DATA TO GET");
        return nullptr;
    }

    if (!data)
        data = new char [size];

    uint64_t partialLen = max-from;

    if (to > from || (to < from && size <= partialLen))
    {
        memcpy(data, &buffer[from], size);

        if (!peekData)
           from += size;

        if (!peekData)
            onDataGotten(size);

        return data;
    }

    else if (to < from)
    {
        memcpy(data, &buffer[from], partialLen);

        FlatPlusDebug("GETTING OF FRAGMENTED DATA");

        uint64_t remain =  size-partialLen;
        memcpy(&data[partialLen], buffer, remain);

        if (!peekData)
            from = remain;

        if (!peekData)
            onDataGotten(size);

        return data;
    }

    onEmptyGotten();

    delete [] data;
    FlatWarning("CANNOT get data, current size: " << sz << " B; REQUESTED-SZ: " << size << " B");
    return nullptr;
}

SkRingBuffer &SkRingBuffer::operator = (SkRingBuffer *buf)
{
    if (buf->isEmpty())
        return *this;

    clear();
    buf->copyTo(this);
    return *this;
}

SkRingBuffer &SkRingBuffer::operator = (SkDataBuffer &buf)
{
    if (buf.isEmpty())
        return *this;

    clear();
    buf.copyTo(this);
    return *this;
}

SkRingBuffer &SkRingBuffer::operator << (SkRingBuffer *buf)
{
    if (buf->isEmpty())
        return *this;

    buf->copyTo(this);
    return *this;
}

SkRingBuffer &SkRingBuffer::operator << (SkDataBuffer &buf)
{
    if (buf.isEmpty())
        return *this;

    buf.copyTo(this);
    return *this;
}

SkRingBuffer &SkRingBuffer::operator >> (SkDataBuffer &buf)
{
    if (isEmpty())
        return *this;

    copyTo(buf);
    return *this;
}

SkRingBuffer &SkRingBuffer::operator >> (SkRingBuffer *buf)
{
    if (isEmpty())
        return *this;

    copyTo(buf);
    return *this;
}

bool SkRingBuffer::isEmpty()
{
    return (size() == 0);
}

uint64_t SkRingBuffer::calculateSize()
{
    if (to >= from)
        return to-from;

    return (max-from) + to;
}

uint64_t SkRingBuffer::size()
{
    uint64_t currentSize = 0;

    bufferEx.lock();
    currentSize = calculateSize();
    bufferEx.unlock();

    return currentSize;
}

void SkRingBuffer::clear()
{
    bufferEx.lock();
    from = 0;
    to = 0;
    bufferEx.unlock();
}

CStr *SkRingBuffer::getRawDataConst()
{
    return buffer;
}

char *SkRingBuffer::getRawData()
{
    return buffer;
}

const uint64_t &SkRingBuffer::getFromCursor()
{
    return from;
}

const uint64_t &SkRingBuffer::getToCursor()
{
    return to;
}

