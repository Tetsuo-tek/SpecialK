#ifndef SKVECTOR_H
#define SKVECTOR_H

#define SK_DEFAULT_VECTOR_CAPACITY      1

#include "abstract/skabstractarray.h"
#include "Core/sklogmachine.h"

//IT CAN USE CLASSEs AND C/C++-STRUCTs, OTHER THAN PRIMITIVE TYPEs AND POINTERs
//TO ALLOCATE/DEALLOCATE MEMORY IT USES new AND delete []
//ITS EFFICIENCY IS LESS THAN SkArray

template <typename T>
class SPECIALK SkVector extends SkAbstractArray<T>
{
    public:
        SkVector(/*uint64_t defaultCapacity=SK_DEFAULT_VECTOR_CAPACITY*/);
        SkVector(const SkVector<T> &source);
        ~SkVector();

        void setCapacity(uint64_t count) override;

        //these meths preserve the position-order by definition
        void append(const T &e) override;
        void append(SkAbstractList<T> &other, uint64_t count=0) override;

        void prepend(const T &e) override;
        void prepend(SkAbstractList<T> &other, uint64_t count=0) override;

        bool insert(const T &e, uint64_t index) override;
        bool insert(SkAbstractList<T> &other, uint64_t index, uint64_t count=0) override;

        bool contains(const T &e) override;
        int64_t indexOf(const T &e, uint64_t from=0) override;

        bool set(const T &e, uint64_t index) override;
        //bool set(const T *array, uint64_t count, uint64_t index) override;
        T &get(uint64_t index) override;
        T &first() override;
        T &last() override;

        //if the preserveOrder(..) is NOT set on true
        //these meths creates disorder between remaining element
        T remove(const T &e) override;
        T removeAt(uint64_t index) override;
        T removeFirst() override;
        T removeLast() override;
        uint64_t remove(SkAbstractList<T> &other) override;
        uint64_t remove(T *array, uint64_t count) override;

        //void sort(SkOrderAlgo algo=SkOrderAlgo::OA_QUICK, Comparator<T> *c=nullptr) override;

        //logic size becomes 0
        //but anything is deallocated until the next remove*(..) will be called
        void clear() override;

        bool isEmpty() override;
        uint64_t count() override;
        uint64_t count(const T &e) override;
        uint64_t size() override;
        uint64_t capacity() override;
        uint64_t capacitySize() override;

        void toArray(T *array) override;
        void toList(SkAbstractList<T> &l) override;
        void swap(SkVector<T> &other);

        T *data() override;

        class SPECIALK SkIterator extends SkAbstractListIterator<T>
        {
            public:
                SkIterator(SkVector<T> &l, int64_t startFrom);

                bool isValid() override;

                bool hasNext() override;
                bool next() override;

                bool hasPrev() override;
                bool prev() override;

                T &item() override;
                int64_t index() override;

                bool set(const T &e) override;
                bool insert(const T &e) override;
                T remove() override;

                void reset() override;

                bool goToBegin() override;
                bool goToEnd() override;

                bool atBegin() override;
                bool atEnd() override;

                SkIterator &operator ++ () override;
                SkIterator &operator -- () override;

            private:
                int64_t currentIndex;
                SkVector<T> *ownerList;
        };

        SkVector<T> &operator =  (const SkVector<T> &source);
        bool        operator  == (const SkVector<T> &operand);
        T           &operator [] (uint64_t index) override;
        SkVector<T> &operator << (const T &item) override;

        SkIterator *iterator(int64_t startFrom=-1) override;

        T &nv()
        {return nullValue = T();}

    private:
        uint64_t defaultCountCapacity;
        uint64_t maxCountCapacity;
        uint64_t elemsCount;
        T *ptr;
        uint64_t elemSz;
        T nullValue;

    protected:
        //these meths will NOT update elemsCount
        void updateCapacity(int delta);
        void refactorize(uint64_t oldCapacity, uint64_t newCapacity);

        void move(T &src, T &tgt);
        void shiftBlock(uint64_t index, int64_t offset, uint64_t count);
};

//

template <typename T>
SkVector<T>::SkVector(/*uint64_t defaultCapacity*/)
{
    //defaultCountCapacity = defaultCapacity;

    //if (defaultCountCapacity == 0)
        defaultCountCapacity = 2;

    /*if (defaultCountCapacity % 2 != 0)
        defaultCountCapacity++;*/

    maxCountCapacity = defaultCountCapacity;

    elemSz = sizeof(T);
    elemsCount = 0;

    ptr = new T [maxCountCapacity];
}

template <typename T>
SkVector<T>::SkVector(const SkVector<T> &source)
{
    defaultCountCapacity = source.defaultCountCapacity;
    maxCountCapacity = source.maxCountCapacity;
    elemsCount = source.elemsCount;
    elemSz = source.elemSz;
    ptr = new T [maxCountCapacity];

    for(uint64_t i=0; i<elemsCount; i++)
        ptr[i] = source.ptr[i];
}

template <typename T>
SkVector<T>::~SkVector()
{
    delete [] ptr;
}

template <typename T>
void SkVector<T>::move(T &src, T &tgt)
{
    tgt = src;
}

template <typename T>
void SkVector<T>::shiftBlock(uint64_t index, int64_t offset, uint64_t count)
{
    //cout << "!!! " << index << " " << offset << " " << count << "\n";

    if (offset < 0)
    {
        //<- (-)
        // i-1<<i [index+1; <count]
        for(uint64_t i=index; i<index+count; i++)
        {
            //cout << "i: " << i << "; i-o: " << i+offset << "; o: " << offset << "; c: " << maxCountCapacity << "\n";
            //cout << "A[i" << offset << "] (&" << (i+offset) << ") =" << data[i+offset] << " <- A[i] (&" << i << ") =" << data[i] << "\n\n";

            //cout << data[i+offset] << " < " << data[i] << "; ";
            ptr[i+offset] = ptr[i];
        }

        //cout << "\n";
    }

    else if (offset > 0)
    {
        // -> (+)
        // i+1<<i [count+index-1; >=index]
        int64_t i= static_cast<int64_t>(index) + static_cast<int64_t>(count) - 1;

        //cout << "maxCountCapacity: " << maxCountCapacity << "\n";

        for(; i>=static_cast<int64_t>(index); i--)
        {
            /*cout << "i: " << i << "; i+o: " << i+offset << "; c: " << maxCountCapacity << "\n";
            cout << "A[i+" << offset << "] (&" << (i+offset) << ") =" << data[i+offset] << " <- A[i] (&" << i << ") =" << data[i] << "\n\n";*/
            //cout << ptr[i] << " (&" << (i) << ") > " << ptr[i+offset] << " (&" << (i+offset) << "); \n";
            ptr[i+offset] = ptr[i];
        }

        //cout << "\n";
    }
}


template <typename T>
void SkVector<T>::setCapacity(uint64_t count)
{
    uint64_t currentCapacity = maxCountCapacity;
    uint64_t newMaxCountCapacity = 0;

    if (!count)
    {
        newMaxCountCapacity = defaultCountCapacity;
        elemsCount = 0;
    }

    else
    {
        if (count % 2 != 0)
            count++;

        newMaxCountCapacity = count;

        if (elemsCount > count)
            elemsCount = count;
    }

    refactorize(currentCapacity, newMaxCountCapacity);
}

template <typename T>
void SkVector<T>::refactorize(uint64_t oldCapacity, uint64_t newCapacity)
{
    if (oldCapacity == newCapacity)
        return;

    maxCountCapacity = newCapacity;

    if (maxCountCapacity == 0)
        maxCountCapacity = 2;

    T *d = new T [maxCountCapacity];

    // !!!

    uint64_t max = elemsCount;

    if (maxCountCapacity < max)
        max = maxCountCapacity;

    for(uint64_t i=0; i<max; i++)
        d[i] = ptr[i];
    // !!!

    delete [] ptr;
    ptr = d;

    //IN ONE CASE IT MAKE A STRAGE CRASH (STARTING ls.skasm WITH MIMESI)
    FlatPlusDebug("changed capacity: " << oldCapacity << "->" << maxCountCapacity << " (" << maxCountCapacity*elemSz << " B)");
}

template <typename T>
void SkVector<T>::updateCapacity(int delta)
{
    uint64_t currentCapacity = maxCountCapacity;
    uint64_t newMaxCountCapacity = maxCountCapacity;
    uint64_t newCount = elemsCount + delta;
    //cout << "* check: " << newCount << " = " << elemsCount << " + (" << delta << ") [" << maxCountCapacity << " / " << (maxCountCapacity >> 1) << "]\n";

    if (delta > 0 && newCount > (newMaxCountCapacity >> 1))
        (newMaxCountCapacity <<= 1)+=newCount; // *= 2

    //NOT happens when we call clear() until some remove action is performed
    else if (delta < 0 && newCount <= (newMaxCountCapacity >> 3))// /4)
        newMaxCountCapacity >>= 1; // /= 2

    //else
    if (delta == 0)
    {
        FlatError("Capacity NOT changed: " << maxCountCapacity << " -> " << currentCapacity);
        return;
    }

    refactorize(currentCapacity, newMaxCountCapacity);
}

template <typename T>
void SkVector<T>::append(const T &e)
{
    updateCapacity(+1);
    ptr[elemsCount++] = e;
}

template <typename T>
void SkVector<T>::append(SkAbstractList<T> &other, uint64_t count)
{
    if (other.isEmpty() || count >= other.count())
        return;

    if (count == 0)
        count = other.count();

    if (count == 1)
    {
        append(other.first());
        return;
    }

    updateCapacity(+count);

    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t i=elemsCount;
    uint64_t limit = elemsCount + count;

    while(itr->next() && i<limit)
        ptr[i++] = itr->item();

    delete itr;
    elemsCount += count;
}

template <typename T>
void SkVector<T>::prepend(const T &e)
{
    updateCapacity(+1);
    shiftBlock(0, +1, elemsCount);
    ptr[0] = e;
    elemsCount++;
}

template <typename T>
void SkVector<T>::prepend(SkAbstractList<T> &other, uint64_t count)
{
    if (isEmpty())
        append(other, count);

    insert(other, 0, count);
}

template <typename T>
bool SkVector<T>::insert(const T &e, uint64_t index)
{
    if (index >= elemsCount)
        return false;

    updateCapacity(+1);
    shiftBlock(index, +1, elemsCount-index);
    ptr[index] = e;
    elemsCount++;
    return true;
}

template <typename T>
bool SkVector<T>::insert(SkAbstractList<T> &other, uint64_t index, uint64_t count)
{
    if (index >= elemsCount || other.isEmpty() || count >= other.count())
        return false;

    if (count == 0)
        count = other.count();

    if (count == 1)
        return insert(other.first(), index);

    updateCapacity(+count);
    shiftBlock(index, +count, elemsCount-index);

    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t i = index;
    uint64_t limit = index + count;

    while(itr->next() && i<limit)
        ptr[i++] = itr->item();

    delete itr;
    elemsCount += count;

    return true;
}

template <typename T>
bool SkVector<T>::contains(const T &e)
{
    return (indexOf(e) > -1);
}

template <typename T>
int64_t SkVector<T>::indexOf(const T &e, uint64_t from)
{
    for(uint64_t i=from; i<elemsCount; i++)
        if (ptr[i] == e)
            return static_cast<int64_t>(i);

    return -1;
}

template <typename T>
bool SkVector<T>::set(const T &e, uint64_t index)
{
    if (index >= elemsCount)
        return false;

    ptr[index] = e;
    return true;
}

/*template <typename T>
bool SkVector<T>::set(const T *array, uint64_t count, uint64_t index)
{
    if (index >= elemsCount || index + count >= elemsCount)
        return false;

    std::copy(array, array + count, &ptr[index]);

    //if (array >= ptr && array+count<=ptr+count)
        //memmove(&ptr[index], array, count*elemSz);
    //else
        //memcpy(&ptr[index], array, count*elemSz);

    return true;
}*/

template <typename T>
T &SkVector<T>::get(uint64_t index)
{
    if (index >= elemsCount)
        return nv();

    return ptr[index];
}

template <typename T>
T &SkVector<T>::first()
{
    if (!elemsCount)
        return nv();

    return ptr[0];
}

template <typename T>
T &SkVector<T>::last()
{
    if (!elemsCount)
        return nv();

    return ptr[elemsCount-1];
}

template <typename T>
T SkVector<T>::remove(const T &e)
{
    for(uint64_t i=0; i<elemsCount; i++)
    {
        if (ptr[i] == e)
            return removeAt(i);
    }

    return nv();
}

template <typename T>
T SkVector<T>::removeAt(uint64_t index)
{
    if (index >= elemsCount)
        return nv();

    T removingElement = ptr[index];
    shiftBlock(index+1, -1, elemsCount-(index+1));
    updateCapacity(-1);
    elemsCount--;
    return removingElement;
}

template <typename T>
T SkVector<T>::removeFirst()
{
    if (!elemsCount)
        return nv();

    return removeAt(0);
}

template <typename T>
T SkVector<T>::removeLast()
{
    if (!elemsCount)
        return nv();

    return removeAt(elemsCount-1);
}

template <typename T>
uint64_t SkVector<T>::remove(SkAbstractList<T> &other)
{
    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t removed = 0;
    uint64_t c = elemsCount;

    while(itr->next())
    {
        T &e = itr->item();

        for(uint64_t i=0; i<c; i++)
            if (e == ptr[i])
            {
                shiftBlock(i+1, -1, c-(i+1));
                c--;
                removed++;
                break;
            }

    }

    delete itr;
    elemsCount = c;
    updateCapacity(-removed);
    return removed;
}

template <typename T>
uint64_t SkVector<T>::remove(T *array, uint64_t count)
{
    uint64_t removed = 0;
    uint64_t c = elemsCount;

    for(uint64_t z=0; z<count /*&& c>=0*/; z++)
    {
        T &e = array[z];

        for(uint64_t i=0; i<elemsCount; i++)
            if (e == ptr[i])
            {
                shiftBlock(i+1, i, c-(i+1));
                c--;
                removed++;
                break;
            }
    }

    elemsCount = c;
    updateCapacity(-removed);
    return removed;
}

/*template <typename T>
void SkVector<T>::sort(SkOrderAlgo algo, Comparator<T> *c)
{
    SkSort<T> s;
    s.setup(algo, c);
    s.sort(ptr, elemsCount);
}*/

template <typename T>
void SkVector<T>::clear()
{
    elemsCount = 0;
}

template <typename T>
bool SkVector<T>::isEmpty()
{
    return (elemsCount == 0);
}

template <typename T>
uint64_t SkVector<T>::count()
{
    return elemsCount;
}

template <typename T>
uint64_t SkVector<T>::count(const T &e)
{
    SkIterator *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        if (itr->item() == e)
            i++;

    delete itr;
    return i;
}

template <typename T>
uint64_t SkVector<T>::size()
{
    return elemsCount*elemSz;
}

template <typename T>
uint64_t SkVector<T>::capacity()
{
    return maxCountCapacity;
}

template <typename T>
uint64_t SkVector<T>::capacitySize()
{
    return maxCountCapacity*elemSz;
}

template <typename T>
void SkVector<T>::toArray(T *array)
{
    //memcpy(array, d, elemsCount*elemSz);
    for(uint64_t i=0; i<elemsCount; i++)
        array[i] = ptr[i];
}

template <typename T>
void SkVector<T>::toList(SkAbstractList<T> &l)
{
    for(uint64_t i=0; i<elemsCount; i++)
        l.append(ptr[i]);
}

template <typename T>
void SkVector<T>::swap(SkVector<T> &other)
{
    uint64_t defaultCountCapacity = this->defaultCountCapacity;
    uint64_t maxCountCapacity = this->maxCountCapacity;
    uint64_t elemsCount = this->elemsCount;
    T *ptr = this->ptr;

    this->defaultCountCapacity = other.defaultCountCapacity;
    this->maxCountCapacity = other.maxCountCapacity;
    this->elemsCount = other.elemsCount;
    this->ptr = other.ptr;

    other.defaultCountCapacity = defaultCountCapacity;
    other.maxCountCapacity = maxCountCapacity;
    other.elemsCount = elemsCount;
    other.ptr = ptr;
}

template <typename T>
T *SkVector<T>::data()
{
    return ptr;
}

template <typename T>
SkVector<T> &SkVector<T>::operator = (const SkVector<T> &source)
{
    defaultCountCapacity = source.defaultCountCapacity;
    maxCountCapacity = source.maxCountCapacity;
    elemsCount = source.elemsCount;

    delete [] ptr;
    ptr = new T [maxCountCapacity];

    for(uint64_t i=0; i<maxCountCapacity; i++)
        ptr[i] = source.ptr[i];

    return *this;
}

template <typename T>
bool SkVector<T>::operator == (const SkVector<T> &operand)
{
    return (elemsCount == operand.elemsCount
            && memcmp(ptr, operand.ptr, elemsCount*elemSz) == 0);
}

template <typename T>
T &SkVector<T>::operator [] (uint64_t index)
{
    return get(index);
}

template <typename T>
SkVector<T> &SkVector<T>::operator << (const T &item)
{
    append(item);
    return *this;
}

template <typename T>
typename SkVector<T>::SkIterator *SkVector<T>::iterator(int64_t startFrom)
{
    return new SkIterator(*this, startFrom);
}

// ITERATOR

template <typename T>
SkVector<T>::SkIterator::SkIterator(SkVector<T> &l, int64_t startFrom)
{
    if (startFrom >= 0 && l.elemsCount && static_cast<uint64_t>(startFrom) < l.elemsCount)
        currentIndex = startFrom;
    else
        currentIndex = -1;

    ownerList = &l;
}

template <typename T>
bool SkVector<T>::SkIterator::isValid()
{
    return currentIndex != -1;
}

template <typename T>
bool SkVector<T>::SkIterator::hasNext()
{
    return (/*(currentIndex >= -1) &&*/ (currentIndex < static_cast<int64_t>(ownerList->elemsCount-1)));
}

template <typename T>
bool SkVector<T>::SkIterator::next()
{
    if (!hasNext())
        return false;

    currentIndex++;
    return true;
}

template <typename T>
bool SkVector<T>::SkIterator::hasPrev()
{
    return ((currentIndex > 0) && (currentIndex < static_cast<int64_t>(ownerList->elemsCount)));
}

template <typename T>
bool SkVector<T>::SkIterator::prev()
{
    if (!hasPrev())
        return false;

    currentIndex--;
    return true;
}

template <typename T>
T &SkVector<T>::SkIterator::item()
{
    if (!isValid())
        return ownerList->nv();

    return ownerList->ptr[currentIndex];
}

template <typename T>
int64_t SkVector<T>::SkIterator::index()
{
    return currentIndex;
}

template <typename T>
bool SkVector<T>::SkIterator::set(const T &e)
{
    if (!isValid())
        return false;

    ownerList->ptr[currentIndex] = e;
    return true;
}

template <typename T>
bool SkVector<T>::SkIterator::insert(const T &e)
{
    if (!isValid())
    {
        ownerList->append(e);
        currentIndex = 0;
        return true;
    }

    return ownerList->insert(e, currentIndex);
}

template <typename T>
T SkVector<T>::SkIterator::remove()
{
    if (!isValid())
        return ownerList->nv();

    T removing = ownerList->ptr[currentIndex];
    ownerList->removeAt(static_cast<uint64_t>(currentIndex));    
    currentIndex--;

    return removing;
}

template <typename T>
void SkVector<T>::SkIterator::reset()
{
    currentIndex = -1;
}

template <typename T>
bool SkVector<T>::SkIterator::goToBegin()
{
    if (!isValid())
        return false;

    currentIndex = 0;
    return true;
}

template <typename T>
bool SkVector<T>::SkIterator:: goToEnd()
{
    if (!isValid())
        return false;

    currentIndex = ownerList->elemsCount-1;
    return true;
}

template <typename T>
bool SkVector<T>::SkIterator::atBegin()
{
    return currentIndex == 0;
}

template <typename T>
bool SkVector<T>::SkIterator::atEnd()
{
    return currentIndex == static_cast<int64_t>(ownerList->elemsCount)-1;
}

template <typename T>
typename SkVector<T>::SkIterator &SkVector<T>::SkIterator::operator ++ ()
{
    next();
    return *this;
}

template <typename T>
typename SkVector<T>::SkIterator &SkVector<T>::SkIterator::operator -- ()
{
    prev();
    return *this;
}

#endif // SKVECTOR_H
