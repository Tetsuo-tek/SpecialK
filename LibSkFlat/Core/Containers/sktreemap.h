#ifndef SKSORTEDMAP_H
#define SKSORTEDMAP_H

#include "skredblacktree.h"
#include "abstract/skabstractmap.h"

//all keys inserted will be ordered

template <typename K, typename V>
class SPECIALK SkTreeMap extends SkAbstractMap<K, V>
{
    public:
        SkTreeMap();
        SkTreeMap(const SkTreeMap<K, V> &source);
        ~SkTreeMap();

        void add(const K &key, const V &value) override;
        SkPair<K, V> remove(const K &key) override;

        bool contains(const K &key) override;
        void keys(SkAbstractList<K> &l) override;

        V &value(const K &key) override;
        void values(SkAbstractList<V> &l) override;

        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t countValues(V &v) override;
        uint64_t size() override;

        void swap(SkTreeMap<K, V> &other);

        SkTreeMap<K, V> &operator =  (const SkTreeMap<K, V> &source);
        bool            operator  == (const SkTreeMap<K, V> &operand);
        V               &operator [] (const K &key) override;

        SkBinaryTreeVisit<K, V> *iterator() override;

        SkRedBlackTree<K, V> &getInternalTree();

        K &nk()
        {return nullKey = K();}

        V &nv()
        {return nullValue = V();}

    private:
        SkRedBlackTree<K, V> tree;
        K nullKey;
        V nullValue;
};

template <typename K, typename V>
SkTreeMap<K, V>::SkTreeMap()
{}

template <typename K, typename V>
SkTreeMap<K, V>::SkTreeMap(const SkTreeMap<K, V> &source)
{
    tree = source.tree;
}

template <typename K, typename V>
SkTreeMap<K, V>::~SkTreeMap()
{}

template <typename K, typename V>
void SkTreeMap<K, V>::add(const K &key, const V &value)
{
    tree.add(key, value);
}

template <typename K, typename V>
SkPair<K, V> SkTreeMap<K, V>::remove(const K &key)
{
    SkPair<K, V> pair;
    pair.set(key, value(key));
    tree.remove(key);
    return pair;
}

template <typename K, typename V>
bool SkTreeMap<K, V>::contains(const K &key)
{
    return tree.contains(key);
}

template <typename K, typename V>
void SkTreeMap<K, V>::keys(SkAbstractList<K> &l)
{
    SkBinaryTreeVisit<K, V> *itr = tree.iterator();

    while(itr->next())
        l << itr->item().key();

    delete itr;
}

template <typename K, typename V>
V &SkTreeMap<K, V>::value(const K &key)
{
    return tree.value(key);
}

template <typename K, typename V>
void SkTreeMap<K, V>::values(SkAbstractList<V> &l)
{
    SkBinaryTreeVisit<K, V> *itr = tree.iterator();

    while(itr->next())
        l << itr->item().value();

    delete itr;
}

template <typename K, typename V>
void SkTreeMap<K, V>::clear()
{
    tree.clear();
}

template <typename K, typename V>
bool SkTreeMap<K, V>::isEmpty()
{
    return tree.isEmpty();
}

template <typename K, typename V>
uint64_t SkTreeMap<K, V>::count()
{
    return tree.count();
}

template <typename K, typename V>
uint64_t SkTreeMap<K, V>::countValues(V &v)
{
    uint64_t c = 0;
    SkBinaryTreeVisit<K, V> *itr = tree.iterator();

    while(itr->next())
        if (itr->item().value() == v)
            c++;

    delete itr;
    return c;
}

template <typename K, typename V>
uint64_t SkTreeMap<K, V>::size()
{
    return tree.size();
}

template <typename K, typename V>
void SkTreeMap<K, V>::swap(SkTreeMap<K, V> &other)
{
    tree.swap(other.tree);
}

template <typename K, typename V>
SkTreeMap<K, V> &SkTreeMap<K, V>::operator = (const SkTreeMap<K, V> &source)
{
    //map = source.map;
    tree = source.tree;
    return *this;
}

template <typename K, typename V>
bool SkTreeMap<K, V>::operator == (const SkTreeMap<K, V> &operand)
{
    return (tree == operand.tree);
}

template <typename K, typename V>
V &SkTreeMap<K, V>::operator [] (const K &key)
{
    if (!tree.contains(key))
        tree.add(key);

    return tree.value(key);
}

template <typename K, typename V>
SkBinaryTreeVisit<K, V> *SkTreeMap<K, V>::iterator()
{
    return tree.iterator();
}

template <typename K, typename V>
SkRedBlackTree<K, V> &SkTreeMap<K, V>::getInternalTree()
{
    return tree;
}

#endif // SKSORTEDMAP_H
