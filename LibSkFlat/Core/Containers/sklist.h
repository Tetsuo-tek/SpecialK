#ifndef SKLIST_H
#define SKLIST_H

#include "abstract/skabstractlist.h"

template <typename T>
class SPECIALK SkList extends SkAbstractList<T>
{
    class Node
    {
        public:
            Node(const T &e, Node *p=nullptr, Node *n=nullptr);
            ~Node();

            void link(Node *p, Node *n);
            void unlink();

            void set(const T &e, Node *p=nullptr, Node *n=nullptr);
            T &get();

            bool hasPrev();
            Node *prevNode();
            bool hasNext();
            Node *nextNode();

            static Node *newInstance(const T &e, Node *p, Node *n);

        private:
            friend class SkList;

            Node *prev;
            Node *next;
            T elem;
    };

    public:
        SkList();
        SkList(const SkList<T> &source);
        ~SkList();

        void append(const T &e) override;
        void append(SkAbstractList<T> &other, uint64_t count=0) override;

        void prepend(const T &e) override;
        void prepend(SkAbstractList<T> &other, uint64_t count=0) override;

        bool insert(const T &e, uint64_t index) override;
        bool insert(SkAbstractList<T> &other, uint64_t index, uint64_t count=0) override;

        bool contains(const T &e) override;
        int64_t indexOf(const T &e, uint64_t from=0) override;

        bool set(const T &e, uint64_t index) override;
        T &get(uint64_t index) override;
        T &first() override;
        T &last() override;

        T remove(const T &e) override;
        T removeAt(uint64_t index) override;
        T removeFirst() override;
        T removeLast() override;
        uint64_t remove(SkAbstractList<T> &other) override;
        uint64_t remove(T *array, uint64_t count) override;

        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t count(const T &e) override;
        uint64_t size() override;

        void toArray(T *array) override;
        void toList(SkAbstractList<T> &l) override;
        virtual void swap(SkList<T> &other);

        class SPECIALK SkIterator extends SkAbstractListIterator<T>
        {
            public:
                SkIterator(SkList<T> &l, int64_t startFrom);

                bool isValid() override;

                bool hasNext() override;
                bool next() override;

                bool hasPrev() override;
                bool prev() override;

                T &item() override;
                int64_t index() override;

                bool set(const T &e) override;
                bool insert(const T &e) override;
                T remove() override;

                void reset() override;

                bool goToBegin() override;
                bool goToEnd() override;

                bool atBegin() override;
                bool atEnd() override;

                SkIterator &operator ++ () override;
                SkIterator &operator -- () override;

            private:
                Node *current;
                int64_t currentIndex;
                SkList<T> *ownerList;
        };

        SkList<T>   &operator =  (const SkList<T> &source);
        bool        operator  == (const SkList<T> &operand);
        T           &operator [] (uint64_t index) override;
        SkList<T>   &operator << (const T &item) override;

        SkIterator *iterator(int64_t startFrom=-1) override;

        T &nv()
        {return nullValue = T();}

    private:
        Node *head;
        Node *tail;

        uint64_t elemsCount;
        uint64_t elemSz;
        T nullValue;

        Node *getNodeByIndex(uint64_t index);
        Node *getNodeByValue(const T &e);
        T removeNode(Node *n);
};

template <typename T>
SkList<T>::Node::Node(const T &e, Node *p, Node *n)
{
    elem = e;

    link(p, n);
}

template <typename T>
SkList<T>::Node::~Node()
{}

template <typename T>
void SkList<T>::Node::link(Node *p, Node *n)
{
    prev = p;

    if (prev)
        prev->next = this;

    next = n;

    if (next)
        next->prev = this;
}

template <typename T>
void SkList<T>::Node::unlink()
{
    if (prev)
        prev->next = next;

    if (next)
        next->prev = prev;

    prev = nullptr;
    next = nullptr;
}

template <typename T>
void SkList<T>::Node::set(const T &e, Node *p, Node *n)
{
    elem = e;

    if (p && prev)
        delete prev;

    if (n && next)
        delete next;

    link(p, n);
}

template <typename T>
T &SkList<T>::Node::get()
{
    return elem;
}

template <typename T>
bool SkList<T>::Node::hasPrev()
{
    return (prev != nullptr);
}

template <typename T>
typename SkList<T>::Node *SkList<T>::Node::prevNode()
{
    return prev;
}

template <typename T>
bool SkList<T>::Node::hasNext()
{
    return (next != nullptr);
}

template <typename T>
typename SkList<T>::Node *SkList<T>::Node::nextNode()
{
    return next;
}

template <typename T>
typename SkList<T>::Node *SkList<T>::Node::newInstance(const T &e, Node *p, Node *n)
{
    Node *node = new Node(e);
    node->link(p, n);
    return node;
}

template <typename T>
SkList<T>::SkList()
{
    elemSz = sizeof(T);
    elemsCount = 0;

    head = nullptr;
    tail = nullptr;
}

template <typename T>
SkList<T>::SkList(const SkList<T> &source)
{
    elemSz = sizeof(T);
    elemsCount = 0;

    head = nullptr;
    tail = nullptr;

    *this = source;
}

template <typename T>
SkList<T>::~SkList()
{
    clear();
}

template <typename T>
typename SkList<T>::Node *SkList<T>::getNodeByIndex(uint64_t index)
{
    if (isEmpty() || index >= elemsCount)
        return nullptr;

    if (index == 0)
        return head;

    if (index == elemsCount-1)
        return tail;

    if (!head->hasNext())
        return nullptr;

    Node *n = head->nextNode();
    uint64_t i = 1;

    while(i<index && n && n != tail)
    {
        if (i == index)
            break;

        n = n->nextNode();
        i++;
    }

    return n;
}

template <typename T>
typename SkList<T>::Node *SkList<T>::getNodeByValue(const T &e)
{
    if (isEmpty())
        return nullptr;

    if (head->get() == e)
        return head;

    if (tail->get() == e)
        return tail;

    if (!head->hasNext())
        return nullptr;

    Node *n = head->nextNode();

    do
    {
        if (n->get() == e)
            break;

        n = n->nextNode();
    } while(n && n != tail);

    return n;
}

template <typename T>
T SkList<T>::removeNode(Node *n)
{
    if (!n)
        return nv();

    T removingElement = n->get();

    if (head && head == tail)
        head = tail = nullptr;

    else if (n == head)
        head = head->nextNode();

    else if (n == tail)
        tail = tail->prevNode();

    n->unlink();
    delete n;
    elemsCount--;

    return removingElement;
}

template <typename T>
void SkList<T>::append(const T &e)
{
    tail = Node::newInstance(e, tail, nullptr);

    if (head == nullptr)
        head = tail;

    elemsCount++;
}

template <typename T>
void SkList<T>::append(SkAbstractList<T> &other, uint64_t count)
{
    if (other.isEmpty() || count >= other.count())
        return;

    if (count == 0)
        count = other.count();

    if (count == 1)
    {
        append(other.first());
        return;
    }

    SkAbstractListIterator<T> *itr = other.iterator();
    itr->goToBegin();

    if (head == nullptr)
        head = tail = Node::newInstance(itr->item(), nullptr, nullptr);
    else
        tail = Node::newInstance(itr->item(), tail, nullptr);

    while(itr->next())
        tail = Node::newInstance(itr->item(), tail, nullptr);

    elemsCount += count;
    delete itr;
}

template <typename T>
void SkList<T>::prepend(const T &e)
{
    head = Node::newInstance(e, nullptr, head);

    if (tail == nullptr)
        tail = head;

    elemsCount++;
}

template <typename T>
void SkList<T>::prepend(SkAbstractList<T> &other, uint64_t count)
{
    if (isEmpty())
        append(other, count);

    insert(other, 0, count);
}

template <typename T>
bool SkList<T>::insert(const T &e, uint64_t index)
{
    Node *n = getNodeByIndex(index);

    if (!n)
        return false;

    Node *insertingNode = Node::newInstance(e, n->prevNode(), n);

    if (n == head)
        head = insertingNode;

    elemsCount++;
    return true;
}

template <typename T>
bool SkList<T>::insert(SkAbstractList<T> &other, uint64_t index, uint64_t count)
{
    if (index >= elemsCount || other.isEmpty() || count >= other.count())
        return false;

    if (count == 0)
        count = other.count();

    if (count == 1)
        return insert(other.first(), index);

    Node *n = getNodeByIndex(index);
    Node *movingNode = n;
    SkAbstractListIterator<T> *itr = other.iterator();
    itr->goToBegin();

    Node *prev = nullptr;
    Node *next = nullptr;

    if (n == head)
    {
        head = Node::newInstance(itr->item(), nullptr, head);
        n = head;
    }

    else
    {
        prev = n->prevNode();
        next = n;
        n = Node::newInstance(itr->item(), prev, next);
    }

    while(itr->next())
    {
        prev = n;
        next = movingNode;
        n = Node::newInstance(itr->item(), prev, next);
    }

    elemsCount += count;
    delete itr;

    return true;
}

template <typename T>
bool SkList<T>::contains(const T &e)
{
    return (indexOf(e) > -1);
}

template <typename T>
int64_t SkList<T>::indexOf(const T &e, uint64_t from)
{
    if (isEmpty())
        return -1;

    Node *n = head;
    uint64_t i = 0;

    do
    {
        if (n->get() == e && i >= from)
            return static_cast<int64_t>(i);

        n = n->nextNode();
        i++;
    } while(n);

    return -1;
}

template <typename T>
bool SkList<T>::set(const T &e, uint64_t index)
{
    Node *n = getNodeByIndex(index);

    if (!n)
        return false;

    n->set(e);
    return true;
}

template <typename T>
T &SkList<T>::get(uint64_t index)
{
    Node *n = getNodeByIndex(index);

    if (!n)
        return nv();

    return n->get();
}

template <typename T>
T &SkList<T>::first()
{
    if (isEmpty())
        return nv();

    return head->get();
}

template <typename T>
T &SkList<T>::last()
{
    if (isEmpty())
        return nv();

    return tail->get();
}

template <typename T>
T SkList<T>::remove(const T &e)
{
    return removeNode(getNodeByValue(e));
}

template <typename T>
T SkList<T>::removeAt(uint64_t index)
{
    return removeNode(getNodeByIndex(index));
}

template <typename T>
T SkList<T>::removeFirst()
{
    return removeNode(head);
}

template <typename T>
T SkList<T>::removeLast()
{
    return removeNode(tail);
}

template <typename T>
uint64_t SkList<T>::remove(SkAbstractList<T> &other)
{
    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t removed = 0;

    while(itr->next())
    {
        Node *n = getNodeByValue(itr->item());

        if (n)
        {
            removed++;
            removeNode(n);
        }

     }

    delete itr;
    return removed;
}

template <typename T>
uint64_t SkList<T>::remove(T *array, uint64_t count)
{
    uint64_t removed = 0;

    for(uint64_t z=0; z<count && !isEmpty(); z++)
    {
        Node *n = getNodeByValue(array[z]);

        if (n)
        {
            removed++;
            removeNode(n);
        }
    }

    return removed;
}

template <typename T>
void SkList<T>::clear()
{
    if (isEmpty())
        return;

    Node *n = head;
    Node *tempNode = nullptr;

    while (n)
    {
        tempNode = n;
        n = n->nextNode();

        delete tempNode;
    }

    head = nullptr;
    tail = nullptr;
    elemsCount = 0;
}

template <typename T>
bool SkList<T>::isEmpty()
{
    return (elemsCount == 0);
}

template <typename T>
uint64_t SkList<T>::count()
{
    return elemsCount;
}

template <typename T>
uint64_t SkList<T>::count(const T &e)
{
    SkIterator *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        if (itr->item() == e)
            i++;

    delete itr;
    return i;
}

template <typename T>
uint64_t SkList<T>::size()
{
    return elemsCount*elemSz;
}

template <typename T>
void SkList<T>::toArray(T *array)
{
    SkIterator *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        array[i++] = itr->item();

    delete itr;
}

template <typename T>
void SkList<T>::toList(SkAbstractList<T> &l)
{
    SkIterator *itr = iterator();

    while(itr->next())
        l.append(itr->item());

    delete itr;
}

template <typename T>
void SkList<T>::swap(SkList<T> &other)
{
    Node *head = this->head;
    Node *tail = this->tail;
    uint64_t elemsCount = this->elemsCount;

    this->head = other.head;
    this->tail = other.tail;
    this->elemsCount = other.elemsCount;

    other.head = head;
    other.tail = tail;
    other.elemsCount = elemsCount;
}

template <typename T>
SkList<T> &SkList<T>::operator = (const SkList<T> &source)
{
    clear();

    SkList<T> l;
    l.head = source.head;
    l.tail = source.tail;
    l.elemsCount = source.elemsCount;

    SkAbstractListIterator<T> *itr = l.iterator();

    while(itr->next())
        append(itr->item());

    delete itr;

    //l will not fermorm any clear(); source is not touched
    l.head = nullptr;
    l.tail = nullptr;
    l.elemsCount = 0;
    return *this;
}

template <typename T>
bool SkList<T>::operator == (const SkList<T> &operand)
{
    if (elemsCount != operand.elemsCount)
        return false;

    SkAbstractListIterator<T> *itr = iterator();
    SkAbstractListIterator<T> *itrOperand = static_cast<SkList<T>>(operand).iterator();//ROBA MALE

    bool ok = true;

    while(ok && itr->next() && itrOperand->next() )
        if (!(itr->item() == itrOperand->item()))
            ok = false;

    delete itr;
    delete itrOperand;
    return ok;
}

template <typename T>
T &SkList<T>::operator [] (uint64_t index)
{
    return get(index);
}

template <typename T>
SkList<T> &SkList<T>::operator << (const T &item)
{
    append(item);
    return *this;
}

template <typename T>
typename SkList<T>::SkIterator *SkList<T>::iterator(int64_t startFrom)
{
    return new SkIterator(*this, startFrom);
}

// ITERATOR

template <typename T>
SkList<T>::SkIterator::SkIterator(SkList<T> &l, int64_t startFrom)
{
    ownerList = &l;

    if (startFrom >= 0 && ownerList->elemsCount && startFrom < static_cast<int64_t>(ownerList->elemsCount))
    {
        currentIndex = startFrom;
        current = ownerList->getNodeByIndex(currentIndex);
        //cout << "====== "<< current->get() << " " << currentIndex << "\n";
    }
    else
    {
        currentIndex = -1;
        current = nullptr;
    }
}

template <typename T>
bool SkList<T>::SkIterator::isValid()
{
    return currentIndex != -1;
}

template <typename T>
bool SkList<T>::SkIterator::hasNext()
{
    if (!current &&
            (!ownerList->isEmpty()
             && (currentIndex < static_cast<int64_t>(ownerList->elemsCount-1))))
    {
        return true;
    }

    return (current && current->hasNext());
}

template <typename T>
bool SkList<T>::SkIterator::next()
{
    if (!hasNext())
        return false;

    currentIndex++;

    if (!current && !ownerList->isEmpty())
        current = ownerList->head;

    else
        current = current->nextNode();

    return true;
}

template <typename T>
bool SkList<T>::SkIterator::hasPrev()
{
    if (!current &&
            (!ownerList->isEmpty()
             && (currentIndex > static_cast<int64_t>(ownerList->elemsCount-1))))
    {
        return true;
    }

    return (current && current->hasPrev());
}

template <typename T>
bool SkList<T>::SkIterator::prev()
{
    if (!hasPrev())
        return false;

    currentIndex--;

    if (!current && !ownerList->isEmpty())
        current = ownerList->tail;

    else
        current = current->prevNode();

    return true;
}

template <typename T>
T &SkList<T>::SkIterator::item()
{
    if (!isValid())
        return ownerList->nv();

    return current->get();
}

template <typename T>
int64_t SkList<T>::SkIterator::index()
{
    return currentIndex;
}

template <typename T>
bool SkList<T>::SkIterator::set(const T &e)
{
    if (!isValid())
        return false;

    current->set(e);
    return true;
}

template <typename T>
bool SkList<T>::SkIterator::insert(const T &e)
{
    if (!isValid())
    {
        //cout << "@@\n";
        ownerList->append(e);
        current = ownerList->tail;
        currentIndex = ownerList->elemsCount-1;
        return true;
    }

    //cout << "!!!!!!! id: " << currentIndex << " current: " << current->get() << " val: " << e << "\n";
    Node *n = Node::newInstance(e, current->prevNode(), current);

    if (current == ownerList->head)
        ownerList->head = n;

    current = n;

    //cout << "@@@@@@@ id: " << currentIndex << " current: " << current->get() << " val: " << e << "\n";
    ownerList->elemsCount++;
    return true;
}

template <typename T>
T SkList<T>::SkIterator::remove()
{
    if (!isValid())
        return ownerList->nv();

    T removing = current->get();
    Node *substitute = current->prevNode();
    ownerList->removeNode(current);

    current = substitute;
    currentIndex--;

    return removing;
}

template <typename T>
void SkList<T>::SkIterator::reset()
{
    currentIndex = -1;
    current = nullptr;
}

template <typename T>
bool SkList<T>::SkIterator::goToBegin()
{
    if (ownerList->isEmpty())
        return false;

    currentIndex = 0;
    current = ownerList->head;
    return true;
}

template <typename T>
bool SkList<T>::SkIterator:: goToEnd()
{
    if (ownerList->isEmpty())
        return false;

    currentIndex = ownerList->elemsCount-1;
    current = ownerList->tail;
    return true;
}

template <typename T>
bool SkList<T>::SkIterator::atBegin()
{
    return current == ownerList->head;
}

template <typename T>
bool SkList<T>::SkIterator::atEnd()
{
    return current == ownerList->tail;
}

template <typename T>
typename SkList<T>::SkIterator &SkList<T>::SkIterator::operator ++ ()
{
    next();
    return *this;
}

template <typename T>
typename SkList<T>::SkIterator &SkList<T>::SkIterator::operator -- ()
{
    prev();
    return *this;
}

#endif // SKLIST_H
