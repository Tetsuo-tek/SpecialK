/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSTRINGLIST_H
#define SKSTRINGLIST_H

#include "skstring.h"

class SPECIALK SkStringList extends SkAbstractList<SkString>
{
    public:
        /**
         * @brief Constructor
         */
        SkStringList();
        SkStringList(const SkStringList &other);
        SkStringList(const SkVector<SkString> &other);
        SkStringList(SkAbstractList<SkString> &other);

        /**
         * @brief Appends a new element to the container
         * @param text the string to append
         */
        void append(const SkString &e) override;
        void append(SkAbstractList<SkString> &other, uint64_t count=0) override;

        void prepend(const SkString &e) override;
        void prepend(SkAbstractList<SkString> &other, uint64_t count=0) override;

        bool insert(const SkString &e, uint64_t index) override;
        bool insert(SkAbstractList<SkString> &other, uint64_t index, uint64_t count=0) override;

        /**
         * @brief Checks if the list contains a cstring
         * @param text the string to search
         * @return true if the string is contained in the container, otherwise false
         */
        bool contains(const SkString &e) override;

        /**
         * @brief Searches for a cstring inside the container
         * @param text the string to search
         * @param from the position to start from
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(const SkString &e, uint64_t from=0) override;

        bool set(const SkString &e, uint64_t index) override;
        SkString &get(uint64_t index) override;
        SkString &first() override;
        SkString &last() override;

        /**
         * @brief Removes a cstring from the container
         * @param text the removing string
         * @return true if the string exists, otherwise false
         */
        SkString remove(const SkString &e) override;

        SkString removeAt(uint64_t index) override;
        SkString removeFirst() override;
        SkString removeLast() override;
        uint64_t remove(SkAbstractList<SkString> &other) override;
        uint64_t remove(SkString *array, uint64_t count) override;

        /**
         * @brief Merges all strings in the container to make a single string
         * @param sep the string to insert between merging strings
         * @return the built string
         */
        SkString join(CStr *sep);

        void reverse();

        void clear() override;
        bool isEmpty() override;

        /**
         * @brief Gets the stored elements count
         * @return the count number
         */
        uint64_t count() override;

        /**
         * @brief Gets the stored elements count for a specified string
         * @param text the string to count
         * @return the count number
         */
        uint64_t count(const SkString &text) override;

        uint64_t size() override;

        void toArray(SkString *array) override;
        void toList(SkAbstractList<SkString> &l) override;
        void swap(SkVector<SkString> &other);
        void swap(SkStringList &other);

        SkString *data();

        /**
         * @brief Equal-operator from another container
         * @param operand the container to compare
         * @return true if containers are equal
         */
        SkStringList    &operator =  (const SkStringList &source);
        SkStringList    &operator =  (const SkVector<SkString> &source);
        SkStringList    &operator =  (SkAbstractList<SkString> &source);
        bool            operator  == (const SkStringList &operand);
        SkString        &operator [] (uint64_t index)                     override;
        SkStringList    &operator << (const SkString &item)             override;

        SkVector<SkString>::SkIterator *iterator(int64_t startFrom=-1)  override;

        SkVector<SkString> &getInternalVector();

    private:
        SkVector<SkString> subList;
};

//DEPRECATED!

#if defined(UMBA)
class SPECIALK SkStringList extends SkTemplatedVector<SkString>
{
    public:
        /**
         * @brief Constructor
         */
        SkStringList();

        /**
         * @brief Gets the stored elements count
         * @return the count number
         */
        uint64_t count();

        /**
         * @brief Gets the stored elements count for a specified cstring
         * @param text the string to count
         * @return the count number
         */
        uint64_t count(CStr *text);

        /**
         * @brief Gets the stored elements count for a specified string
         * @param text the string to count
         * @return the count number
         */
        uint64_t count(const SkString &text);

        /**
         * @brief Checks if the list contains a cstring
         * @param text the string to search
         * @return true if the string is contained in the container, otherwise false
         */
        bool contains(CStr *text);

        /**
         * @brief Checks if the list contains a string
         * @param text the string to search
         * @return true if the string is contained in the container, otherwise false
         */
        bool contains(const SkString &text);

        /**
         * @brief Searches for a cstring inside the container
         * @param text the string to search
         * @param from the position to start from
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(CStr *text, uint64_t from=0);

        /**
         * @brief Searches for a string inside the container
         * @param text the string to search
         * @param from the position to start from
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(const SkString &text, uint64_t from=0);

        /**
         * @brief Merges all strings in the container to make a single string
         * @param sep the string to insert between merging strings
         * @return the built string
         */
        SkString join(CStr *sep);

        /**
         * @brief Appends a new element to the container
         * @param text the string to append
         */
        void append(CStr *text);

        /**
         * @brief Appends a new element to the container
         * @param text the string to append
         */
        void append(const SkString &text);

        /**
         * @brief Removes a cstring from the container
         * @param text the removing string
         * @return true if the string exists, otherwise false
         */
        bool remove(CStr *text);

        /**
         * @brief Removes a string from the container
         * @param text the removing string
         * @return true if the string exists, otherwise false
         */
        bool remove(const SkString &text);

        /**
         * @brief Equal-operator from another container
         * @param operand the container to compare
         * @return true if containers are equal
         */
        bool operator == (const SkStringList operand);

        /**
         * @brief NotEqual-operator from another container
         * @param operand the container to compare
         * @return true if containers are not equal
         */
        bool operator != (const SkStringList operand);

        /**
         * @brief operator Append-operator of a string
         * @param text the string to append
         * @return this container
         */
        SkStringList &operator << (SkString text);

        /**
         * @brief []-operator for read/write on referred element
         * @param index the index of the element
         * @return the element reference
         */
        SkString &operator [] (uint64_t index);
};
#endif
#endif // SKSTRINGLIST_H
