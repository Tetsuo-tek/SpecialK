#include "skstringlist.h"
#include "skvariant.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkStringList);

DeclareMeth_INSTANCE_VOID(SkStringList, append, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkStringList, prepend, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkStringList, insert, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkStringList, contains, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkStringList, indexOf, int64_t, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkStringList, set, bool, Arg_CStr, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkStringList, get, SkString&, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkStringList, first, SkString&)
DeclareMeth_INSTANCE_RET(SkStringList, last, SkString&)
DeclareMeth_INSTANCE_RET(SkStringList, remove, SkString, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkStringList, removeAt, SkString, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkStringList, removeFirst, SkString)
DeclareMeth_INSTANCE_RET(SkStringList, removeLast, SkString)
DeclareMeth_INSTANCE_RET(SkStringList, join, SkString, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkStringList, clear)
DeclareMeth_INSTANCE_RET(SkStringList, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkStringList, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkStringList, size, uint64_t)

SetupClassWrapper(SkStringList)
{
    SetClassSuper(SkStringList, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkStringList, append);
    AddMeth_INSTANCE_VOID(SkStringList, prepend);
    AddMeth_INSTANCE_RET(SkStringList, insert);
    AddMeth_INSTANCE_RET(SkStringList, contains);
    AddMeth_INSTANCE_RET(SkStringList, indexOf);
    AddMeth_INSTANCE_RET(SkStringList, set);
    AddMeth_INSTANCE_RET(SkStringList, get);
    AddMeth_INSTANCE_RET(SkStringList, first);
    AddMeth_INSTANCE_RET(SkStringList, last);
    AddMeth_INSTANCE_RET(SkStringList, remove);
    AddMeth_INSTANCE_RET(SkStringList, removeAt);
    AddMeth_INSTANCE_RET(SkStringList, removeFirst);
    AddMeth_INSTANCE_RET(SkStringList, removeLast);
    AddMeth_INSTANCE_RET(SkStringList, join);
    AddMeth_INSTANCE_VOID(SkStringList, clear);
    AddMeth_INSTANCE_RET(SkStringList, isEmpty);
    AddMeth_INSTANCE_RET(SkStringList, count);
    AddMeth_INSTANCE_RET(SkStringList, size);
}

// // // // // // // // // // // // // // // // // // // // //

SkStringList::SkStringList()
{
    CreateClassWrapper(SkStringList);
}

SkStringList::SkStringList(const SkStringList &other)
{
    CreateClassWrapper(SkStringList);
    subList = other.subList;
}

SkStringList::SkStringList(const SkVector<SkString> &other)
{
    CreateClassWrapper(SkStringList);
    subList = other;
}

SkStringList::SkStringList(SkAbstractList<SkString> &other)
{
    CreateClassWrapper(SkStringList);
    other.toList(subList);
}

void SkStringList::append(const SkString &e)
{
    subList.append(e);
}

void SkStringList::append(SkAbstractList<SkString> &other, uint64_t count)
{
    subList.append(other, count);
}

void SkStringList::prepend(const SkString &e)
{
    subList.prepend(e);
}

void SkStringList::prepend(SkAbstractList<SkString> &other, uint64_t count)
{
    subList.prepend(other, count);
}

bool SkStringList::insert(const SkString &e, uint64_t index)
{
    return subList.insert(e, index);
}

bool SkStringList::insert(SkAbstractList<SkString> &other, uint64_t index, uint64_t count)
{
    return subList.insert(other, index, count);
}

bool SkStringList::contains(const SkString &e)
{
    return subList.contains(e);
}

int64_t SkStringList::indexOf(const SkString &e, uint64_t from)
{
    return subList.indexOf(e, from);
}

bool SkStringList::set(const SkString &e, uint64_t index)
{
    return subList.set(e, index);
}

SkString &SkStringList::get(uint64_t index)
{
    return subList.get(index);
}

SkString &SkStringList::first()
{
    return subList.first();
}

SkString &SkStringList::last()
{
    return subList.last();
}

SkString SkStringList::remove(const SkString &e)
{
    return subList.remove(e);
}

SkString SkStringList::removeAt(uint64_t index)
{
    return subList.removeAt(index);
}

SkString SkStringList::removeFirst()
{
    return subList.removeFirst();
}

SkString SkStringList::removeLast()
{
    return subList.removeLast();
}

uint64_t SkStringList::remove(SkAbstractList<SkString> &other)
{
    return subList.remove(other);
}

uint64_t SkStringList::remove(SkString *array, uint64_t count)
{
    return subList.remove(array, count);
}

SkString SkStringList::join(CStr *sep)
{
    SkString joined;

    SkVector<SkString>::SkIterator *itr = iterator();

    while(itr->next())
    {
        joined += itr->item();

        if (itr->hasNext())
            joined += sep;
    }

    delete itr;
    return joined;
}

void SkStringList::reverse()
{
    subList.reverse();
}

void SkStringList::clear()
{
    subList.clear();
}

bool SkStringList::isEmpty()
{
    return subList.isEmpty();
}

uint64_t SkStringList::count()
{
    return subList.count();
}

uint64_t SkStringList::count(const SkString &text)
{
    return subList.count(text);
}

uint64_t SkStringList::size()
{
    return subList.size();
}

void SkStringList::toArray(SkString *array)
{
    return subList.toArray(array);
}

void SkStringList::toList(SkAbstractList<SkString> &l)
{
    subList.toList(l);
}

void SkStringList::swap(SkVector<SkString> &other)
{
    subList.swap(other);
}

void SkStringList::swap(SkStringList &other)
{
    subList.swap(other.subList);
}

SkString *SkStringList::data()
{
    return subList.data();
}

SkStringList &SkStringList::operator = (const SkStringList &source)
{
    subList = source.subList;
    return *this;
}

SkStringList &SkStringList::operator = (const SkVector<SkString> &source)
{
    subList = source;
    return *this;
}

SkStringList &SkStringList::operator = (SkAbstractList<SkString> &source)
{
    clear();
    source.toList(subList);
    return *this;
}

bool SkStringList::operator == (const SkStringList &operand)
{
    return (subList == operand.subList);
}

SkString &SkStringList::operator [] (uint64_t index)
{
    return get(index);
}

SkStringList &SkStringList::operator << (const SkString &item)
{
    append(item);
    return *this;
}

/*SkStringList &SkStringList::operator << (CStr *item)
{
    if (!SkString::isEmpty(item))
        append(item);

    return *this;
}*/

SkVector<SkString>::SkIterator *SkStringList::iterator(int64_t startFrom)
{
    return subList.iterator(startFrom);
}

SkVector<SkString> &SkStringList::getInternalVector()
{
    return subList;
}

#if defined(UMBA)
SkStringList::SkStringList() : SkTemplatedVector<SkString>()
{

}

uint64_t SkStringList::count() {return SkTemplatedVector<SkString>::count();}

uint64_t SkStringList::count(CStr *text)
{
    SkString s(text);
    return SkTemplatedVector<SkString>::count(s);
}

uint64_t SkStringList::count(const SkString &text) {return SkTemplatedVector<SkString>::count(text);}

bool SkStringList::contains(CStr *text)
{
    SkString s(text);
    return SkTemplatedVector<SkString>::contains(s);
}

bool SkStringList::contains(const SkString &text) {return SkTemplatedVector<SkString>::contains(text);}

int64_t SkStringList::indexOf(CStr *text, uint64_t from)
{
    const SkString s(text);
    return SkTemplatedVector<SkString>::indexOf(s, from);
}

int64_t SkStringList::indexOf(const SkString &text, uint64_t from) {return SkTemplatedVector<SkString>::indexOf(text, from);}

SkString SkStringList::join(CStr *sep)
{
    SkString joined;

    for(uint64_t i=0; i<count(); i++)
    {
        joined += at(i);

        if (i < count()-1)
            joined += sep;
    }

    return joined;
}

void SkStringList::append(CStr *text)
{
    SkString s(text);
    SkTemplatedVector<SkString>::append(s);
}

void SkStringList::append(const SkString &text) {return SkTemplatedVector<SkString>::append(text);}

bool SkStringList::remove(CStr *text)
{
    const SkString s(text);
    return SkTemplatedVector<SkString>::remove(s);
}

bool SkStringList::remove(const SkString &text) {return SkTemplatedVector<SkString>::remove(text);}

bool SkStringList::operator == (const SkStringList operand)
{
    return isEqualTo(operand);
}

bool SkStringList::operator != (const SkStringList operand)
{
    return !isEqualTo(operand);
}

SkStringList &SkStringList::operator << (SkString text)
{
    if (!text.isEmpty())
        append(text);

    return *this;
}

SkString &SkStringList::operator [] (uint64_t index)
{
    return at(index);
}
#endif
