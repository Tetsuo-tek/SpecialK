#include "skvariant.h"
#include "Core/Containers/skringbuffer.h"
#include "Core/Containers/skargsmap.h"
#include "Core/Containers/skarraycast.h"
#include "Core/sklogmachine.h"
#include "Core/Containers/sklist.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkVariant);

DeclareMeth_STATIC_RET(SkVariant, isNull, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isBoolean, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isChar, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isNumber, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isInteger, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isReal, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isByteArray, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isString, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isList, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isPair, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isMap, bool, Arg_Enum(SkVariant_T))
DeclareMeth_STATIC_RET(SkVariant, isRawPointer, bool, Arg_Enum(SkVariant_T))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isNull, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isBoolean, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isChar, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isNumber, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isInteger, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isReal, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isByteArray, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isString, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isList, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isPair, 1,  bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isMap, 1, bool)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, isRawPointer, 1, bool)
DeclareMeth_INSTANCE_RET(SkVariant, isEmpty, bool)
//DeclareMeth_INSTANCE_RET(SkVariant, isStringList, bool)
//DeclareMeth_INSTANCE_RET(SkVariant, isStack, bool)
//DeclareMeth_INSTANCE_RET(SkVariant, isQueue, bool)
DeclareMeth_INSTANCE_RET(SkVariant, canConvertTo, bool, Arg_Enum(SkVariant_T))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, canConvertTo, 1, bool, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, variantTypeName, CStr*, Arg_Enum(SkVariant_T))
DeclareMeth_INSTANCE_RET(SkVariant, variantType, SkVariant_T)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkVariant, variantTypeName, 1, CStr*)
DeclareMeth_INSTANCE_RET(SkVariant, size, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariant, data, CStr*)
DeclareMeth_INSTANCE_VOID(SkVariant, setVal, Arg_Bool)
//DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 1, Arg_Char)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 2, Arg_Int8)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 3, Arg_UInt8)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 4, Arg_Int16)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 5, Arg_UInt16)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 6, Arg_Int32)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 7, Arg_UInt32)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 8, Arg_Int64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 9, Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 10, Arg_Float)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 11, Arg_Double)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 12, Arg_Custom(void), Arg_UInt64)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 13, Arg_StringRef)
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 14, *Arg_Custom(SkStringList))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 15, *Arg_Custom(SkStack<SkVariant>))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 16, *Arg_Custom(SkQueue<SkVariant>))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 17, *Arg_Custom(SkAbstractList<SkVariant>))
//DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 18, *Arg_Custom(SkPair<SkVariant, SkVariant>))
//DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 19, *Arg_Custom(SkAbstractMap<SkVariant, SkVariant>))
//DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 20, *Arg_Custom(SkAbstractMap<SkString, SkVariant>))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 21, Arg_Custom(void))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 22, *Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 23, *Arg_Custom(SkVariantData))
DeclareMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 24, Arg_Custom(SkVariantData))
DeclareMeth_INSTANCE_VOID(SkVariant, swap, *Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_VOID(SkVariant, nullify)
DeclareMeth_INSTANCE_VOID(SkVariant, clear)
DeclareMeth_INSTANCE_RET(SkVariant, toBool, bool)
DeclareMeth_INSTANCE_RET(SkVariant, toChar, char)
DeclareMeth_INSTANCE_RET(SkVariant, toInt, int)
DeclareMeth_INSTANCE_RET(SkVariant, toUInt, uint)
DeclareMeth_INSTANCE_RET(SkVariant, toInt8, int8_t)
DeclareMeth_INSTANCE_RET(SkVariant, toUInt8, uint8_t)
DeclareMeth_INSTANCE_RET(SkVariant, toInt16, int16_t)
DeclareMeth_INSTANCE_RET(SkVariant, toUInt16, uint16_t)
DeclareMeth_INSTANCE_RET(SkVariant, toInt32, int32_t)
DeclareMeth_INSTANCE_RET(SkVariant, toUInt32, uint32_t)
DeclareMeth_INSTANCE_RET(SkVariant, toInt64, int64_t)
DeclareMeth_INSTANCE_RET(SkVariant, toUInt64, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariant, toFloat, float)
DeclareMeth_INSTANCE_RET(SkVariant, toDouble, double)
DeclareMeth_INSTANCE_RET(SkVariant, toString, SkString)
DeclareMeth_INSTANCE_RET(SkVariant, toCustom, void*, Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_RET(SkVariant, toCustomString, SkString, Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_RET(SkVariant, toVoid, void*)
DeclareMeth_INSTANCE_RET(SkVariant, toStringList, SkStringList)
DeclareMeth_INSTANCE_RET(SkVariant, toStack, SkStack<SkVariant>)
DeclareMeth_INSTANCE_RET(SkVariant, toQueue, SkQueue<SkVariant>)
DeclareMeth_INSTANCE_RET(SkVariant, toList, SkList<SkVariant>)
//DeclareMeth_INSTANCE_RET(SkVariant, toMap, SkMap<SkString, SkVariant>)
//DeclareMeth_INSTANCE_RET(SkVariant, toVariantMap, SkMap<SkVariant, SkVariant>)
DeclareMeth_INSTANCE_RET(SkVariant, toStringRef, SkString&)
DeclareMeth_INSTANCE_RET(SkVariant, updateOnFromStringRef, bool)
DeclareMeth_INSTANCE_VOID(SkVariant, copyToString, Arg_StringRef)
DeclareMeth_INSTANCE_VOID(SkVariant, copyToStringList, *Arg_Custom(SkStringList))
DeclareMeth_INSTANCE_VOID(SkVariant, copyToBuffer, *Arg_Custom(SkDataBuffer))
DeclareMeth_INSTANCE_VOID(SkVariant, copyToStack, *Arg_Custom(SkStack<SkVariant> ))
DeclareMeth_INSTANCE_VOID(SkVariant, copyToQueue, *Arg_Custom(SkQueue<SkVariant>))
DeclareMeth_INSTANCE_VOID(SkVariant, copyToList, *Arg_Custom(SkAbstractList<SkVariant>))
//DeclareMeth_INSTANCE_VOID(SkVariant, copyToPair, *Arg_Custom(SkPair<SkVariant, SkVariant>))
//DeclareMeth_INSTANCE_VOID(SkVariant, copyToMap, *Arg_Custom(SkAbstractMap<SkString, SkVariant>))
//DeclareMeth_INSTANCE_VOID(SkVariant, copyToVariantMap, *Arg_Custom(SkAbstractMap<SkVariant, SkVariant>))
DeclareMeth_INSTANCE_RET(SkVariant, toJson, bool, Arg_StringRef, Arg_Bool, Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkVariant, fromJson, bool, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkVariant, toData, Arg_Custom(SkRingBuffer))
DeclareMeth_STATIC_VOID_OVERLOAD(SkVariant, toData, 1, Arg_Custom(SkRingBuffer), *Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_VOID(SkVariant, fromData, Arg_Custom(SkRingBuffer))
DeclareMeth_STATIC_VOID_OVERLOAD(SkVariant, fromData, 1, Arg_Custom(SkRingBuffer), *Arg_Custom(SkVariant))
DeclareMeth_INSTANCE_RET(SkVariant, realBool, bool&)
DeclareMeth_INSTANCE_RET(SkVariant, realChar, char&)
DeclareMeth_INSTANCE_RET(SkVariant, realInt8, int8_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realUInt8, uint8_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realInt16, int16_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realUInt16, uint16_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realInt32, int32_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realUInt32, uint32_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realInt64, int64_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realUInt64, uint64_t&)
DeclareMeth_INSTANCE_RET(SkVariant, realFloat, float&)
DeclareMeth_INSTANCE_RET(SkVariant, realDouble, double&)
DeclareMeth_STATIC_RET_OVERLOAD(SkVariant, toJson, 1, bool, *Arg_Custom(SkVariant), Arg_StringRef, Arg_Bool_REAL, Arg_UInt16_REAL, Arg_UInt8_REAL)
DeclareMeth_STATIC_VOID(SkVariant, listToJson, *Arg_Custom(SkVariant), Arg_StringRef, Arg_Bool_REAL, Arg_UInt16_REAL, Arg_UInt8_REAL)
DeclareMeth_STATIC_VOID(SkVariant, mapToJson, *Arg_Custom(SkVariant), Arg_StringRef, Arg_Bool_REAL, Arg_UInt16_REAL, Arg_UInt8_REAL)
DeclareMeth_STATIC_VOID(SkVariant, byteArrayToJson, *Arg_Custom(SkVariant), Arg_StringRef, Arg_Bool_REAL, Arg_UInt16_REAL, Arg_UInt8_REAL)
DeclareMeth_STATIC_VOID(SkVariant, stringToJson, *Arg_Custom(SkVariant), Arg_StringRef)
DeclareMeth_STATIC_VOID(SkVariant, charToJson, *Arg_Custom(SkVariant), Arg_StringRef)
DeclareMeth_STATIC_RET(SkVariant, convertSpecialCharToString, bool, Arg_Char, Arg_StringRef)
DeclareMeth_STATIC_VOID(SkVariant, valueToJsonString, *Arg_Custom(SkVariant), Arg_StringRef, Arg_Bool_REAL, Arg_UInt16_REAL, Arg_UInt8_REAL)
DeclareMeth_STATIC_RET_OVERLOAD(SkVariant, fromJson, 1, bool, Arg_CStr, *Arg_Custom(SkVariant))

/*DeclareMeth_STATIC_RET(SkVariant, parseJsonValue, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, parseJsonMap, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, parseJsonList, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, parseJsonString, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, parseJsonNumber, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))
DeclareMeth_STATIC_RET(SkVariant, parseJsonBoolean, bool, Arg_CStr, Arg_UInt64_REAL, *Arg_Custom(SkVariant))*/

DeclareMeth_STATIC_RET(SkVariant, boolToString, CStr*, Arg_Bool)

// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkVariant)
{
    SetClassSuper(SkVariant, SkFlatObject);

    AddMeth_STATIC_RET(SkVariant, isNull);
    AddMeth_STATIC_RET(SkVariant, isBoolean);
    AddMeth_STATIC_RET(SkVariant, isChar);
    AddMeth_STATIC_RET(SkVariant, isNumber);
    AddMeth_STATIC_RET(SkVariant, isInteger);
    AddMeth_STATIC_RET(SkVariant, isReal);
    AddMeth_STATIC_RET(SkVariant, isByteArray);
    AddMeth_STATIC_RET(SkVariant, isString);
    AddMeth_STATIC_RET(SkVariant, isList);
    AddMeth_STATIC_RET(SkVariant, isPair);
    AddMeth_STATIC_RET(SkVariant, isMap);
    AddMeth_STATIC_RET(SkVariant, isRawPointer);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isNull, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isBoolean, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isChar, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isNumber, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isInteger, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isReal, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isByteArray, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isString, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isList, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isPair, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isMap, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, isRawPointer, 1);
    AddMeth_INSTANCE_RET(SkVariant, isEmpty);
//    AddMeth_INSTANCE_RET(SkVariant, isStringList);
//    AddMeth_INSTANCE_RET(SkVariant, isStack);
//    AddMeth_INSTANCE_RET(SkVariant, isQueue);
    AddMeth_INSTANCE_RET(SkVariant, canConvertTo);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, canConvertTo, 1);
    AddMeth_STATIC_RET(SkVariant, variantTypeName);
    AddMeth_INSTANCE_RET(SkVariant, variantType);
    AddMeth_INSTANCE_RET_OVERLOAD(SkVariant, variantTypeName, 1);
    AddMeth_INSTANCE_RET(SkVariant, size);
    AddMeth_INSTANCE_RET(SkVariant, data);
    AddMeth_INSTANCE_VOID(SkVariant, setVal);
    //AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 2);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 3);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 4);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 5);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 6);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 7);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 8);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 9);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 10);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 11);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 12);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 13);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 14);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 15);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 16);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 17);
    //AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 18);
    //AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 19);
    //AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 20);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 21);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 22);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 23);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkVariant, setVal, 24);
    AddMeth_INSTANCE_VOID(SkVariant, swap);
    AddMeth_INSTANCE_VOID(SkVariant, nullify);
    AddMeth_INSTANCE_VOID(SkVariant, clear);
    AddMeth_INSTANCE_RET(SkVariant, toBool);
    AddMeth_INSTANCE_RET(SkVariant, toChar);
    AddMeth_INSTANCE_RET(SkVariant, toInt);
    AddMeth_INSTANCE_RET(SkVariant, toUInt);
    AddMeth_INSTANCE_RET(SkVariant, toInt8);
    AddMeth_INSTANCE_RET(SkVariant, toUInt8);
    AddMeth_INSTANCE_RET(SkVariant, toInt16);
    AddMeth_INSTANCE_RET(SkVariant, toUInt16);
    AddMeth_INSTANCE_RET(SkVariant, toInt32);
    AddMeth_INSTANCE_RET(SkVariant, toUInt32);
    AddMeth_INSTANCE_RET(SkVariant, toInt64);
    AddMeth_INSTANCE_RET(SkVariant, toUInt64);
    AddMeth_INSTANCE_RET(SkVariant, toFloat);
    AddMeth_INSTANCE_RET(SkVariant, toDouble);
    AddMeth_INSTANCE_RET(SkVariant, toString);
    AddMeth_INSTANCE_RET(SkVariant, toCustom);
    AddMeth_INSTANCE_RET(SkVariant, toCustomString);
    AddMeth_INSTANCE_RET(SkVariant, toVoid);
    AddMeth_INSTANCE_RET(SkVariant, toStringList);
    AddMeth_INSTANCE_RET(SkVariant, toStack);
    AddMeth_INSTANCE_RET(SkVariant, toQueue);
    AddMeth_INSTANCE_RET(SkVariant, toList);
    //AddMeth_INSTANCE_RET(SkVariant, toMap);
    //AddMeth_INSTANCE_RET(SkVariant, toVariantMap);
    AddMeth_INSTANCE_RET(SkVariant, toStringRef);
    AddMeth_INSTANCE_RET(SkVariant, updateOnFromStringRef);
    AddMeth_INSTANCE_VOID(SkVariant, copyToString);
    AddMeth_INSTANCE_VOID(SkVariant, copyToStringList);
    AddMeth_INSTANCE_VOID(SkVariant, copyToBuffer);
    AddMeth_INSTANCE_VOID(SkVariant, copyToStack);
    AddMeth_INSTANCE_VOID(SkVariant, copyToQueue);
    AddMeth_INSTANCE_VOID(SkVariant, copyToList);
    //AddMeth_INSTANCE_VOID(SkVariant, copyToPair);
    //AddMeth_INSTANCE_VOID(SkVariant, copyToMap);
    //AddMeth_INSTANCE_VOID(SkVariant, copyToVariantMap);
    AddMeth_INSTANCE_RET(SkVariant, toJson);
    AddMeth_INSTANCE_RET(SkVariant, fromJson);
    AddMeth_INSTANCE_VOID(SkVariant, toData);
    AddMeth_STATIC_VOID_OVERLOAD(SkVariant, toData, 1);
    AddMeth_INSTANCE_VOID(SkVariant, fromData);
    AddMeth_STATIC_VOID_OVERLOAD(SkVariant, fromData, 1);
    AddMeth_INSTANCE_RET(SkVariant, realBool);
    AddMeth_INSTANCE_RET(SkVariant, realChar);
    AddMeth_INSTANCE_RET(SkVariant, realInt8);
    AddMeth_INSTANCE_RET(SkVariant, realUInt8);
    AddMeth_INSTANCE_RET(SkVariant, realInt16);
    AddMeth_INSTANCE_RET(SkVariant, realUInt16);
    AddMeth_INSTANCE_RET(SkVariant, realInt32);
    AddMeth_INSTANCE_RET(SkVariant, realUInt32);
    AddMeth_INSTANCE_RET(SkVariant, realInt64);
    AddMeth_INSTANCE_RET(SkVariant, realUInt64);
    AddMeth_INSTANCE_RET(SkVariant, realFloat);
    AddMeth_INSTANCE_RET(SkVariant, realDouble);
    AddMeth_STATIC_RET_OVERLOAD(SkVariant, toJson, 1);
    AddMeth_STATIC_VOID(SkVariant, listToJson);
    AddMeth_STATIC_VOID(SkVariant, mapToJson);
    AddMeth_STATIC_VOID(SkVariant, byteArrayToJson);
    AddMeth_STATIC_VOID(SkVariant, stringToJson);
    AddMeth_STATIC_VOID(SkVariant, charToJson);
    AddMeth_STATIC_RET(SkVariant, convertSpecialCharToString);
    AddMeth_STATIC_VOID(SkVariant, valueToJsonString);
    AddMeth_STATIC_RET_OVERLOAD(SkVariant, fromJson, 1);
    /*AddMeth_STATIC_RET(SkVariant, parseJsonValue);
    AddMeth_STATIC_RET(SkVariant, parseJsonMap);
    AddMeth_STATIC_RET(SkVariant, parseJsonList);
    AddMeth_STATIC_RET(SkVariant, parseJsonString);
    AddMeth_STATIC_RET(SkVariant, parseJsonNumber);
    AddMeth_STATIC_RET(SkVariant, parseJsonBoolean);*/
    AddMeth_STATIC_RET(SkVariant, boolToString);
}

// // // // // // // // // // // // // // // // // // // // //

SkVariant::SkVariant()
{
    CreateClassWrapper(SkVariant);
    cTorReset();
}

SkVariant::SkVariant(bool v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

/*SkVariant::SkVariant(char v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}*/

SkVariant::SkVariant(int8_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(uint8_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(int16_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(uint16_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(int32_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(uint32_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(int64_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(uint64_t v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(float v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(double v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(void *ptr, uint64_t size)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(ptr, size);
}

SkVariant::SkVariant(CVoid *ptr, uint64_t size)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(ptr, size);
}

SkVariant::SkVariant(CVoid *ptr, uint64_t size, SkVariant_T t)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(ptr, size, t);
}

SkVariant::SkVariant(CStr *cstr)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(cstr);
}

SkVariant::SkVariant(SkString &s)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(s);
}

SkVariant::SkVariant(SkStringList &l)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(l);
}

SkVariant::SkVariant(SkStack<SkVariant> &s)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(s);
}

SkVariant::SkVariant(SkQueue<SkVariant> &q)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(q);
}

SkVariant::SkVariant(SkAbstractList<SkVariant> &l)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(l);
}

SkVariant::SkVariant(SkAbstractMap<SkString, SkVariant> &m)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(m);
}

SkVariant::SkVariant(SkPair<SkVariant, SkVariant> &p)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(p);
}

SkVariant::SkVariant(SkAbstractMap<SkVariant, SkVariant> &m)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(m);
}

SkVariant::SkVariant(SkFlatObject &o) : SkVariant(static_cast<void *>(&o))
{}

SkVariant::SkVariant(void *ptr)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(ptr);
}

SkVariant::SkVariant(const SkVariant &v)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(v);
}

SkVariant::SkVariant(SkVariantData &raw)
{
    CreateClassWrapper(SkVariant);
    cTorReset();
    setVal(raw);
}

void SkVariant::cTorReset()
{
    toCustom = nullptr;
    toCustomString = nullptr;

    props.t = SkVariant_T::T_NULL;
    props.sz = 0;
    props.valData = nullptr;

    stringRef = nullptr;
}

SkVariant::~SkVariant()
{
    if (props.valData && props.t != SkVariant_T::T_RAWPOINTER)
    {
        deleteVariantContainer();

        //cout << SkVariant::typeName(props.t) << " " << props.sz << " " << props.valData << "\n";
        delete [] castToCharData();
    }
}

bool SkVariant::isNull(const SkVariant_T t)
{
    return (t == SkVariant_T::T_NULL);
}

bool SkVariant::isBoolean(const SkVariant_T t)
{
    return (t == SkVariant_T::T_BOOL);
}

bool SkVariant::isChar(const SkVariant_T t)
{
    return (t == SkVariant_T::T_INT8);
}

bool SkVariant::isNumber(const SkVariant_T t)
{
    return (isReal(t) || isInteger(t));
}

bool SkVariant::isInteger(const SkVariant_T t)
{
    return (t == SkVariant_T::T_INT8
            || t == SkVariant_T::T_UINT8
            || t == SkVariant_T::T_INT16
            || t == SkVariant_T::T_UINT16
            || t == SkVariant_T::T_INT32
            || t == SkVariant_T::T_UINT32
            || t == SkVariant_T::T_INT64
            || t == SkVariant_T::T_UINT64);
}

bool SkVariant::isReal(const SkVariant_T t)
{
    return (t == SkVariant_T::T_FLOAT || t == SkVariant_T::T_DOUBLE);
}

bool SkVariant::isByteArray(const SkVariant_T t)
{
    return (t == SkVariant_T::T_BYTEARRAY);
}

bool SkVariant::isString(const SkVariant_T t)
{
    return (t == SkVariant_T::T_STRING);
}

bool SkVariant::isList(const SkVariant_T t)
{
    return (t == SkVariant_T::T_LIST);
}

bool SkVariant::isPair(const SkVariant_T t)
{
    return (t == SkVariant_T::T_PAIR);
}

bool SkVariant::isMap(const SkVariant_T t)
{
    return (t == SkVariant_T::T_MAP);
}

bool SkVariant::isRawPointer(const SkVariant_T t)
{
    return (t == SkVariant_T::T_RAWPOINTER);
}

bool SkVariant::isNull()
{
    return SkVariant::isNull(props.t);
}

bool SkVariant::isEmpty()
{
    return (props.sz == 0);
}

bool SkVariant::isBoolean()
{
    return SkVariant::isBoolean(props.t);
}

bool SkVariant::isChar()
{
    return SkVariant::isChar(props.t);
}

bool SkVariant::isNumber()
{
    return SkVariant::isNumber(props.t);
}

bool SkVariant::isInteger()
{
    return SkVariant::isInteger(props.t);
}

bool SkVariant::isReal()
{
    return SkVariant::isReal(props.t);
}

bool SkVariant::isByteArray()
{
    return SkVariant::isByteArray(props.t);
}

bool SkVariant::isString()
{
    return SkVariant::isString(props.t);
}

bool SkVariant::isList()
{
    return SkVariant::isList(props.t);
}

bool SkVariant::isPair()
{
    return SkVariant::isPair(props.t);
}

bool SkVariant::isMap()
{
    return SkVariant::isMap(props.t);
}

bool SkVariant::isRawPointer()
{
    return SkVariant::isRawPointer(props.t);
}

bool SkVariant::canConvertTo(const SkVariant_T targetType)
{
    if (targetType == SkVariant_T::T_NULL || targetType == SkVariant_T::T_RAWPOINTER)
        return false;

    if (props.t == targetType)
        return true;

    //MAPs AND ARRAYs CANNOT CONVERT TO/FROM ANYTHING
    //MUST REVIEW WITH NEW TYPES
    return (isNumber() && SkVariant::isNumber(targetType))
            || (isNumber() && SkVariant::isString(targetType))
            || (isBoolean() && SkVariant::isNumber(targetType))
            || (isNumber() && SkVariant::isBoolean(targetType))
            || (isString() && SkVariant::isNumber(targetType))
            || (isString() && SkVariant::isByteArray(targetType))
            || (isBoolean() && SkVariant::isString(targetType))
            || (isString() && SkVariant::isBoolean(targetType))
            || (isList() && SkVariant::isString(targetType))
            || (isMap() && SkVariant::isString(targetType));
}

bool SkVariant::canConvertTo(SkVariant &target)
{
    return canConvertTo(target.variantType());
}

CStr *SkVariant::variantTypeName(SkVariant_T t)
{
    if (t == SkVariant_T::T_BOOL)
        return "T_BOOL";

    else if (t == SkVariant_T::T_INT8)
        return "T_INT8";

    else if (t == SkVariant_T::T_UINT8)
        return "T_UINT8";

    else if (t == SkVariant_T::T_INT16)
        return "T_INT16";

    else if (t == SkVariant_T::T_UINT16)
        return "T_UINT16";

    else if (t == SkVariant_T::T_INT32)
        return "T_INT32";

    else if (t == SkVariant_T::T_UINT32)
        return "T_UINT32";

    else if (t == SkVariant_T::T_INT64)
        return "T_INT64";

    else if (t == SkVariant_T::T_UINT64)
        return "T_UINT64";

    else if (t == SkVariant_T::T_FLOAT)
        return "T_FLOAT";

    else if (t == SkVariant_T::T_DOUBLE)
        return "T_DOUBLE";

    else if (t == SkVariant_T::T_STRING)
        return "T_STRING";

    else if (t == SkVariant_T::T_BYTEARRAY)
        return "T_BYTEARRAY";

    else if (t == SkVariant_T::T_LIST)
        return "T_LIST";

    else if (t == SkVariant_T::T_PAIR)
        return "T_PAIR";

    else if (t == SkVariant_T::T_MAP)
        return "T_MAP";

    else if (t == SkVariant_T::T_RAWPOINTER)
        return "T_RAWPOINTER";

    return "T_NULL";
}

SkVariant_T SkVariant::variantTypeFromName(CStr * t)
{
    if (SkString::compare(t, "T_BOOL2"))
        return T_BOOL;

    else if (SkString::compare(t, "T_INT8"))
        return T_INT8;

    else if (SkString::compare(t, "T_UINT8"))
        return T_UINT8;

    else if (SkString::compare(t, "T_INT16"))
        return T_INT16;

    else if (SkString::compare(t, "T_UINT16"))
        return T_UINT16;

    else if (SkString::compare(t, "T_INT32"))
        return T_INT32;

    else if (SkString::compare(t, "T_UINT32"))
        return T_UINT32;

    else if (SkString::compare(t, "T_INT64"))
        return T_INT64;

    else if (SkString::compare(t, "T_UINT64"))
        return T_UINT64;

    else if (SkString::compare(t, "T_FLOAT"))
        return T_FLOAT;

    else if (SkString::compare(t, "T_DOUBLE"))
        return T_DOUBLE;

    else if (SkString::compare(t, "T_STRING"))
        return T_STRING;

    else if (SkString::compare(t, "T_BYTEARRAY"))
        return T_BYTEARRAY;

    else if (SkString::compare(t, "T_LIST"))
        return T_LIST;

    else if (SkString::compare(t, "T_PAIR"))
        return T_PAIR;

    else if (SkString::compare(t, "T_MAP"))
        return T_MAP;

    else if (SkString::compare(t, "T_RAWPOINTER"))
        return T_RAWPOINTER;

    return T_NULL;
}

CStr *SkVariant::variantTypeName()
{
    return variantTypeName(props.t);
}

SkVariant_T SkVariant::variantType()
{
    return props.t;
}

uint64_t SkVariant::size()
{
    return props.sz;
}

CStr *SkVariant::data()
{
    return static_cast<CStr *>(props.valData);
}

void SkVariant::deleteVariantContainer()
{
    if (stringRef)
    {
        delete stringRef;
        stringRef = nullptr;
    }
}

char *SkVariant::castToCharData()
{
    return static_cast<char *>(props.valData);
}

void SkVariant::setVal(bool v)
{
    convertValData(SkVariant_T::T_BOOL, sizeof(bool));
    static_cast<bool *>(props.valData)[0] = v;
}

/*void SkVariant::setVal(char v)
{
    convertValData(SkVariant_T::T_CHAR, sizeof(char));
    static_cast<char *>(props.valData)[0] = v;
}*/

void SkVariant::setVal(int8_t v)
{
    convertValData(SkVariant_T::T_INT8, sizeof(int8_t));
    static_cast<int8_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(uint8_t v)
{
    convertValData(SkVariant_T::T_UINT8, sizeof(uint8_t));
    static_cast<uint8_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(int16_t v)
{
    convertValData(SkVariant_T::T_INT16, sizeof(int16_t));
    static_cast<int16_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(uint16_t v)
{
    convertValData(SkVariant_T::T_UINT16, sizeof(uint16_t));
    static_cast<uint16_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(int32_t v)
{
    convertValData(SkVariant_T::T_INT32, sizeof(int32_t));
    static_cast<int32_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(uint32_t v)
{
    convertValData(SkVariant_T::T_UINT32, sizeof(uint32_t));
    static_cast<uint32_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(int64_t v)
{
    convertValData(SkVariant_T::T_INT64, sizeof(int64_t));
    static_cast<int64_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(uint64_t v)
{
    convertValData(SkVariant_T::T_UINT64, sizeof(uint64_t));
    static_cast<uint64_t *>(props.valData)[0] = v;
}

void SkVariant::setVal(float v)
{
    convertValData(SkVariant_T::T_FLOAT, sizeof(float));
    static_cast<float *>(props.valData)[0] = v;
}

void SkVariant::setVal(double v)
{
    convertValData(SkVariant_T::T_DOUBLE, sizeof(double));
    static_cast<double *>(props.valData)[0] = v;
}

void SkVariant::setVal(void *ptr, uint64_t size)
{
    if (!ptr || !size)
        return;

    convertValData(SkVariant_T::T_BYTEARRAY, size);

    if (props.sz)
        memcpy(props.valData, ptr, props.sz);
}

void SkVariant::setVal(CVoid *ptr, uint64_t size, SkVariant_T t)
{
    if (!ptr || !size)
        return;

    convertValData(t, size);

    if (props.sz)
        memcpy(props.valData, ptr, props.sz);
}

void SkVariant::setVal(CVoid *ptr, uint64_t size)
{
    if (!ptr || !size)
        return;

    convertValData(SkVariant_T::T_BYTEARRAY, size);

    if (props.sz)
        memcpy(props.valData, ptr, props.sz);
}

void SkVariant::setVal(CStr *cstr)
{
    if (!cstr)
    {
        FlatError("String is NULL");
        return;
    }

    uint64_t sz = strlen(cstr);

    convertValData(SkVariant_T::T_STRING, sz + 1);

    if (sz)
        memcpy(props.valData, cstr, sz);

    castToCharData()[sz] = '\0';
}

void SkVariant::setVal(SkString &s)
{
    convertValData(SkVariant_T::T_STRING, s.size() + 1);

    if (!s.isEmpty())
        memcpy(props.valData, s.c_str(), s.size());

    castToCharData()[s.size()] = '\0';
}

void SkVariant::setVal(SkStringList &l)
{
    SkRingBuffer r(1000000);
    r.setObjectName("SkVariant::setVal");

    for(uint64_t i=0; i<l.count(); i++)
    {
        SkVariant val(l.at(i));
        val.toData(&r);
    }

    convertValData(SkVariant_T::T_LIST, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkStack<SkVariant> &s)
{
    SkRingBuffer r;
    r.setObjectName("SkVariant::setVal");

    SkAbstractListIterator<SkVariant> *itr = s.iterator();

    while(itr->next())
        itr->item().toData(&r);

    delete itr;
    convertValData(SkVariant_T::T_LIST, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkQueue<SkVariant> &q)
{
    SkRingBuffer r;

    SkAbstractListIterator<SkVariant> *itr = q.iterator();

    while(itr->next())
        itr->item().toData(&r);

    delete itr;
    convertValData(SkVariant_T::T_LIST, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkAbstractList<SkVariant> &l)
{
    SkRingBuffer r;

    SkAbstractListIterator<SkVariant> *itr = l.iterator();

    while(itr->next())
        itr->item().toData(&r);

    delete itr;
    convertValData(SkVariant_T::T_LIST, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkPair<SkVariant, SkVariant> &p)
{
    SkRingBuffer r;
    p.key().toData(&r);
    p.value().toData(&r);

    convertValData(SkVariant_T::T_PAIR, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkAbstractMap<SkVariant, SkVariant> &m)
{
    SkRingBuffer r;
    SkAbstractIterator<SkPair<SkVariant, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        itr->item().key().toData(&r);
        itr->item().value().toData(&r);
    }

    delete itr;
    convertValData(SkVariant_T::T_MAP, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(SkAbstractMap<SkString, SkVariant> &m)
{
    SkRingBuffer r;
    SkAbstractIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    while(itr->next())
    {
        r.addUInt8(SkVariant_T::T_STRING);
        r.addUInt32(static_cast<uint32_t>(itr->item().key().size()+1));
        r.addData(itr->item().key().c_str(), itr->item().key().size());
        r.addInt8('\0');

        itr->item().value().toData(&r);
    }

    delete itr;
    convertValData(SkVariant_T::T_MAP, r.size());

    if (!r.isEmpty())
        r.getCustomBuffer(castToCharData(), props.sz);
}

void SkVariant::setVal(const SkVariant &other)
{
    /*if (!other.props.sz)
    {
        nullify();
        return;
    }*/

    /*SkString name = other.objName;

    if (name.isEmpty())
        FlatMessage("Copying from: NO_NAME [" << SkVariant::variantTypeName(other.props.t)
                   << "; " << other.props.sz << " B" << "]");
    else
        FlatMessage("Copying from: " << name << " [" << SkVariant::variantTypeName(other.props.t)
                   << "; " << other.props.sz << " B" << "]");*/

    toCustom = other.toCustom;
    toCustomString = other.toCustomString;

    if (other.props.t == SkVariant_T::T_RAWPOINTER)
        setVal(other.props.valData);

    else
    {
        convertValData(other.props.t, other.props.sz);

        if (other.props.sz)
            memcpy(props.valData, other.props.valData, props.sz);
    }

    props.originalRealType = props.originalRealType.c_str();
}

void SkVariant::setVal(SkVariantData &raw)
{
    nullify();
    props = raw;
}

void SkVariant::setVal(void *ptr)
{
    if (!ptr)
        return;

    nullify();

    props.sz = sizeof(int64_t);
    props.t = SkVariant_T::T_RAWPOINTER;
    props.valData = ptr;
}

void SkVariant::setOriginalRealType(CStr *realType)
{
    props.originalRealType = realType;
}

CStr *SkVariant::getOriginalRealType()
{
    return props.originalRealType.c_str();
}

void SkVariant::swap(SkVariant &other)
{
    void *thisData = props.valData;
    SkVariant_T thisT = props.t;
    uint64_t thisSize = props.sz;
    void *(*thisToCustom)(SkVariant *) = toCustom;
    SkString (*thisToCustomString)(SkVariant *) = toCustomString;

    props.sz = other.props.sz;
    props.t = other.props.t;
    props.valData = other.props.valData;
    toCustom = other.toCustom;
    toCustomString = other.toCustomString;

    other.props.sz = thisSize;
    other.props.t = thisT;
    other.props.valData = thisData;
    other.toCustom = thisToCustom;
    other.toCustomString = thisToCustomString;
}

void SkVariant::convertValData(SkVariant_T type, uint64_t size)
{
    nullify();

    props.t = type;
    props.sz = size;

    //REDUNDANT
    if (!props.sz)
        return;

    if (props.sz)
        props.valData = new char [props.sz];
}

void SkVariant::nullify()
{
    if (props.valData)
    {
        props.sz = 0;

        if (props.t != SkVariant_T::T_RAWPOINTER)
            delete [] castToCharData();

        props.valData = nullptr;
        props.t = SkVariant_T::T_NULL;
    }
}

void SkVariant::clear()
{
    nullify();
}

#define RETCASTED(TO, NULLVAL) \
    if (isNull() || !props.sz) \
        return NULLVAL; \
    \
    if (props.t == SkVariant_T::T_BOOL) \
        return static_cast<TO> (realBool()); \
    \
    /*if (props.t == SkVariant_T::T_CHAR)*/ \
        /*return static_cast<TO> (realChar());*/ \
    \
    if (props.t == SkVariant_T::T_INT8) \
        return static_cast<TO> (realInt8()); \
    \
    if (props.t == SkVariant_T::T_UINT8) \
        return static_cast<TO> (realUInt8()); \
    \
    if (props.t == SkVariant_T::T_INT16) \
        return static_cast<TO> (realInt16()); \
    \
    if (props.t == SkVariant_T::T_UINT16) \
        return static_cast<TO> (realUInt16()); \
    \
    if (props.t == SkVariant_T::T_INT32) \
        return static_cast<TO> (realInt32()); \
    \
    if (props.t == SkVariant_T::T_UINT32) \
        return static_cast<TO> (realUInt32()); \
    \
    if (props.t == SkVariant_T::T_INT64) \
        return static_cast<TO> (realInt64()); \
    \
    if (props.t == SkVariant_T::T_UINT64) \
        return static_cast<TO> (realUInt64()); \
    \
    if (props.t == SkVariant_T::T_FLOAT) \
        return static_cast<TO> (realFloat()); \
    \
    if (props.t == SkVariant_T::T_DOUBLE) \
        return static_cast<TO> (realDouble()); \
    \
    return NULLVAL

bool SkVariant::toBool()
{
    if (props.t == SkVariant_T::T_STRING)
        return (memcmp(props.valData, "true", 4) == 0);

    RETCASTED(bool, false);
}

char SkVariant::toChar()
{
    if (props.t == SkVariant_T::T_STRING)
        return castToCharData()[0];

    RETCASTED(char, '\0');
}

int SkVariant::toInt()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return s.toInt();
    }

    RETCASTED(int, 0);
}

uint SkVariant::toUInt()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<uint>(s.toInt());
    }

    RETCASTED(uint, 0);
}

int8_t SkVariant::toInt8()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<int8_t>(s.toInt());
    }

    RETCASTED(int8_t, 0);
}

uint8_t SkVariant::toUInt8()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<uint8_t>(s.toInt());
    }

    RETCASTED(uint8_t, 0);
}

int16_t SkVariant::toInt16()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<int16_t>(s.toInt());
    }

    RETCASTED(int16_t, 0);
}

uint16_t SkVariant::toUInt16()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<uint16_t>(s.toInt());
    }

    RETCASTED(uint16_t, 0);
}

int32_t SkVariant::toInt32()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<int32_t>(s.toInt());
    }

    RETCASTED(int32_t, 0);
}

uint32_t SkVariant::toUInt32()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        return static_cast<uint32_t>(s.toInt());
    }

    RETCASTED(uint32_t, 0);
}

int64_t SkVariant::toInt64()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        //HERE THE VALUE COULD BE CUTTED
        return static_cast<int64_t>(s.toInt());
    }

    RETCASTED(int64_t, 0);
}

uint64_t SkVariant::toUInt64()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s = castToCharData();
        //HERE THE VALUE COULD BE CUTTED
        return static_cast<uint64_t>(s.toInt());
    }

    RETCASTED(uint64_t, 0);
}

float SkVariant::toFloat()
{
    if (props.t == SkVariant_T::T_STRING)
    {
        SkString s;// = castToCharData();
        copyToString(s);
        //HERE THE VALUE COULD BE CUTTED
        return s.toFloat();
    }

    RETCASTED(float, 0);
}

double SkVariant::toDouble()
{
    if (props.t == SkVariant_T::T_STRING)
        return toFloat();

    RETCASTED(double, 0);
}

bool &SkVariant::realBool()         {return ((bool *)       props.valData)[0];}
char &SkVariant::realChar()         {return ((char *)       props.valData)[0];}
int8_t &SkVariant::realInt8()       {return ((int8_t *)     props.valData)[0];}
uint8_t &SkVariant::realUInt8()     {return ((uint8_t *)    props.valData)[0];}
int16_t &SkVariant::realInt16()     {return ((int16_t *)    props.valData)[0];}
uint16_t &SkVariant::realUInt16()   {return ((uint16_t *)   props.valData)[0];}
int32_t &SkVariant::realInt32()     {return ((int32_t *)    props.valData)[0];}
uint32_t &SkVariant::realUInt32()   {return ((uint32_t *)   props.valData)[0];}
int64_t &SkVariant::realInt64()     {return ((int64_t *)    props.valData)[0];}
uint64_t &SkVariant::realUInt64()   {return ((uint64_t *)   props.valData)[0];}
float &SkVariant::realFloat()       {return ((float *)      props.valData)[0];}
double &SkVariant::realDouble()     {return ((double *)     props.valData)[0];}

void *SkVariant::toVoid()
{
    return props.valData;
}

SkString SkVariant::toString()
{
    SkString s;
    copyToString(s);
    return s.c_str();
}

SkStringList SkVariant::toStringList()
{
    SkStringList l;
    copyToStringList(l);
    return l;
}

SkStack<SkVariant> SkVariant::toStack()
{
    SkStack<SkVariant> l;
    copyToStack(l);
    return l;
}

SkQueue<SkVariant> SkVariant::toQueue()
{
    SkQueue<SkVariant> l;
    copyToQueue(l);
    return l;
}

SkList<SkVariant> SkVariant::toList()
{
    SkList<SkVariant> l;
    copyToList(l);
    return l;
}

SkMap<SkString, SkVariant> SkVariant::toMap()
{
    SkMap<SkString, SkVariant> m;
    copyToMap(m);
    return m;
}

SkMap<SkVariant, SkVariant> SkVariant::toVariantMap()
{
    SkMap<SkVariant, SkVariant> m;
    copyToVariantMap(m);
    return m;
}

void SkVariant::copyToString(SkString &str)
{
    if (isNull())
        return;

    if (toCustomString)
    {
        str = toCustomString(this);
        return;
    }

    /*if (props.t == SkVariant_T::T_PAIR)
    {
        SkVariantMap m;
        m[]
        SkVariant v()
    }*/

    //here it is NOT required to have a valid size
    if (props.t == SkVariant_T::T_LIST || props.t == SkVariant_T::T_MAP)
    {
        toJson(str, false);
        return;
    }

    if (!canConvertTo(SkVariant_T::T_STRING))
    {
        FlatError("Cannot convert data from type [" << SkVariant::variantTypeName(props.t) << "] to T_STRING");
        return;
    }

    if (props.t == SkVariant_T::T_BOOL)
    {
        str = SkVariant::boolToString(realBool());
        return;
    }

    /*if (props.t == SkVariant_T::T_CHAR)
    {
        SkString s;
        s.concat(toChar());
        return;
    }*/

    if (props.t == SkVariant_T::T_INT8)
    {
        str = SkString(static_cast<int64_t>(realInt8()));
        return;
    }

    if (props.t == SkVariant_T::T_UINT8)
    {
        str = SkString(static_cast<uint64_t>(realUInt8()));
        return;
    }

    if (props.t == SkVariant_T::T_INT16)
    {
        str = SkString(static_cast<int64_t>(realInt16()));
        return;
    }

    if (props.t == SkVariant_T::T_UINT16)
    {
        str = SkString(static_cast<uint64_t>(realUInt16()));
        return;
    }

    if (props.t == SkVariant_T::T_INT32)
    {
        str = SkString(static_cast<int64_t>(realInt32()));
        return;
    }

    if (props.t == SkVariant_T::T_UINT32)
    {
        str = SkString(static_cast<uint64_t>(realUInt32()));
        return;
    }

    if (props.t == SkVariant_T::T_INT64)
    {
        str = SkString(static_cast<int64_t>(realInt64()));
        return;
    }

    if (props.t == SkVariant_T::T_UINT64)
    {
        str = SkString(static_cast<uint64_t>(realUInt64()));
        return;
    }

    if (props.t == SkVariant_T::T_FLOAT)
    {
        str = SkString(static_cast<double>(realFloat()), 3, true);
        return;
    }

    if (props.t == SkVariant_T::T_DOUBLE)
    {
        str = SkString(realDouble(), 3, true);
        return;
    }

    if (props.t == SkVariant_T::T_STRING && props.sz > 0)
    {
        str = data();
        return;
    }

    if (props.t == SkVariant_T::T_RAWPOINTER)
    {
        std::ostringstream ss;
        ss << hex << SkArrayCast::toUInt64(props.valData);
        str = ss.str();
    }
}

void SkVariant::copyToStringList(SkStringList &l)
{
    l.clear();

    if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_LIST)
    {
        SkString s = toString();
        l.append(s);
        return;
    }

    SkRingBuffer r(1000000);
    r.addData(castToCharData(), props.sz);

    SkVariant v;
    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, v);
        SkString s = v.toString();
        l.append(s);
        v.nullify();
    }
}

void SkVariant::copyToBuffer(SkDataBuffer &b)
{
    b.setData(props.valData, props.sz);
}

void SkVariant::copyToStack(SkStack<SkVariant> &s)
{
    s.clear();
    copyToList(s.getInternalList());

    /*if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_LIST)
    {
        FlatWarning("Copying this content as the only item, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_LIST)
                  << "]: " << variantTypeName());

        s.push(*this);
        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant v;
    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, v);
        s.push(v);
        v.nullify();
    }*/
}

void SkVariant::copyToQueue(SkQueue<SkVariant> &q)
{
    q.clear();
    copyToList(q.getInternalList());

    /*if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_LIST)
    {
        FlatWarning("Copying this content as the only item, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_LIST)
                  << "]: " << variantTypeName())
;
        q.enqueue(*this);
        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant v;
    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, v);
        q.enqueue(v);
        v.nullify();
    }*/
}

void SkVariant::copyToList(SkAbstractList<SkVariant> &l)
{
    l.clear();

    if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_LIST)
    {
        FlatWarning("Copying this content as the only item, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_LIST)
                  << "]: " << variantTypeName());

        l.append(*this);
        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant v;

    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, v);
        l.append(v);
        v.nullify();
    }
}

void SkVariant::copyToPair(SkPair<SkVariant, SkVariant> &p)
{
    if (isNull() || !props.sz)
        return;


    if (props.t != SkVariant_T::T_PAIR)
    {
        FlatError("Cannot copy to pair, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_PAIR)
                  << "]: " << variantTypeName());

        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant k;
    SkVariant v;

    SkVariant::fromData(&r, k);
    SkVariant::fromData(&r, v);

    p.setKey(k);
    p.setValue(v);
}

void SkVariant::copyToMap(SkAbstractMap<SkString, SkVariant> &m)
{
    m.clear();

    if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_MAP)
    {
        FlatError("Cannot copy to map, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_MAP)
                  << "]: " << variantTypeName());

        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant k;
    SkVariant v;

    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, k);
        SkVariant::fromData(&r, v);

        m.add(k.data(), v);

        k.nullify();
        v.nullify();
    }
}

void SkVariant::copyToVariantMap(SkAbstractMap<SkVariant, SkVariant> &m)
{
    m.clear();

    if (isNull() || !props.sz)
        return;

    if (props.t != SkVariant_T::T_MAP)
    {
        FlatError("Cannot copy to map, because the this Variant_T is wrong [requested: "
                  << SkVariant::variantTypeName(SkVariant_T::T_MAP)
                  << "]: " << variantTypeName());

        return;
    }

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant k;
    SkVariant v;

    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, k);
        SkVariant::fromData(&r, v);

        m.add(k, v);

        k.nullify();
        v.nullify();
    }
}

bool SkVariant::toJson(SkString &json, bool indented, uint8_t spaces)
{
    uint16_t level = 0;
    return SkVariant::toJson(*this, json, indented, level, spaces);
}

bool SkVariant::toJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces)
{
    if (v.isList())
        listToJson(v, json, indented, level, spaces);

    else if (v.isMap())
        mapToJson(v, json, indented, level, spaces);

    else
        valueToJsonString(v, json, indented, level, spaces);

    return true;
}

void SkVariant::valueToJsonString(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces)
{
    if (v.isList() || v.isMap() || v.isByteArray())
        SkVariant::toJson(v, json, indented, level, spaces);

    else
    {
        if (v.isChar())
            SkVariant::charToJson(v, json);

        else if (v.isString())
            SkVariant::stringToJson(v, json);

        else if (v.isByteArray())
            SkVariant::byteArrayToJson(v, json, indented, level, spaces);

        else if (v.isNull() || v.isEmpty())
            json += "\"\"";

        else
            json += v.toString();
    }
}

void SkVariant::listToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces)
{
    if (indented)
    {
        json += "[\n";
        level++;
    }

    else
        json += '[';

    SkVector<SkVariant> *lst = new SkVector<SkVariant>();
    SkVector<SkVariant> &l = *lst;

    v.copyToList(l);

    for(uint64_t i=0; i<l.count(); i++)
    {
        if (indented)
            json += SkString::buildFilled(' ', level*spaces);

        SkVariant::valueToJsonString(l.at(i), json, indented, level, spaces);

        if (i < l.count()-1)
            json += ',';

        if (indented)
            json += '\n';
    }

    if (indented)
    {
        level--;
        json += SkString::buildFilled(' ', level*spaces);
        json += "]";
    }

    else
        json += ']';

    delete lst;
}

void SkVariant::mapToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces)
{
    if (indented)
    {
        json += "{\n";
        level++;
    }

    else
        json += '{';

    SkMap<SkVariant, SkVariant> *mp = new SkMap<SkVariant, SkVariant>();
    SkMap<SkVariant, SkVariant> &m = *mp;

    v.copyToVariantMap(m);

    SkVector<SkVariant> *lst = new SkVector<SkVariant>();
    SkVector<SkVariant> &k = *lst;

    m.keys(k);

    for(uint64_t i=0; i<k.count(); i++)
    {
        if (indented)
            json += SkString::buildFilled(' ', level*spaces);

        SkVariant::valueToJsonString(k.at(i), json, indented, level, spaces);

        if (indented)
            json += " : ";
        else
            json += ':';

        SkVariant::valueToJsonString(m.value(k.at(i)), json, indented, level, spaces);

        if (i < k.count()-1)
            json += ',';

        if (indented)
            json += '\n';
    }

    if (indented)
    {
        level--;
        json += SkString::buildFilled(' ', level*spaces);
        json += "}";
    }

    else
        json += '}';

    delete mp;
    delete lst;
}

void SkVariant::byteArrayToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces)
{
    if (indented)
    {
        json += "[\n";
        level++;
    }

    else
        json += '[';

    char *d = v.castToCharData();

    for(uint64_t b=0; b<v.props.sz; b++)
    {
        if (indented)
            json += SkString::buildFilled(' ', level*spaces);

        json.concat(static_cast<int8_t>(d[b]));

        if (b < v.props.sz-1)
            json += ',';

        if (indented)
            json += '\n';
    }

    if (indented)
    {
        level--;
        json += SkString::buildFilled(' ', level*spaces);
        json += "]";
    }

    else
        json += ']';
}

void SkVariant::stringToJson(SkVariant &v, SkString &json)
{
    json += '"';

    SkString src = v.toString();
    CStr *d = src.c_str();

    for(uint64_t i=0; i<src.size(); i++)
        if (!SkVariant::convertSpecialCharToString(d[i], json))
            json += d[i];

    json += '"';
}

void SkVariant::charToJson(SkVariant &v, SkString &json)
{
    json += '"';
    json += v.toString();
    json += '"';
}

bool SkVariant::convertSpecialCharToString(const char c, SkString &output)
{
    /*
        Backspace is replaced with \b.
        Form feed is replaced with \f.
        Newline is replaced with \n.
        Carriage return is replaced with \r.
        Tab is replaced with \t.
        \\Double quote is replaced with \"
        Backslash is replaced with \\
    */

    if (c == '\b')
    {
        output.concat("\\");
        output += 'b';
    }

    else if (c == '\f')
    {
        output.concat("\\");
        output += 'f';
    }

    else if (c == '\n')
    {
        output.concat("\\");
        output += 'n';
    }

    else if (c == '\r')
    {
        output.concat("\\");
        output += 'r';
    }

    else if (c == '\t')
    {
        output.concat("\\");
        output += 't';
    }

    else if (c == '\a')
    {
        output.concat("\\");
        output += 'a';
    }

    else if (c == '\e')
    {
        output.concat("\\");
        output += 'e';
    }

    /*else if (c == '\u')
    {
        output.concat("\\");
        output += 'u';
    }*/

    else if (c == '\"')
    {
        output.concat("\\");
        output += '"';
    }

    else if (c == '\\')
    {
        output.concat("\\");
        output += '\\';
    }

    else
        return false;

    return true;
}

bool SkVariant::fromJson(CStr *json)
{
    return fromJson(json, *this);
}

bool SkVariant::fromJson(CStr *json, SkVariant &v)
{
    uint64_t len;
    return SkVariant::parseJsonValue(json, len, v);
}

bool SkVariant::parseJsonValue(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    if (SkString::isEmpty(s))
    {
        valueLen = 0;
        return false;
    }

    int64_t sz = static_cast<int64_t>(strlen(s));
    int64_t i;

    for(i=0; i<static_cast<int64_t>(sz); i++) if (::isprint(s[i]) && !::isspace(s[i])) break;

    if (i >= sz)
    {
        StaticError("JSON-Text is empty");
        return false;
    }

    if (s[i] == '{')
        return SkVariant::parseJsonMap(&s[i], valueLen, v);

    else if (s[i] == '[')
        return SkVariant::parseJsonList(&s[i], valueLen, v);

    else if (s[i] == '"')
        return SkVariant::parseJsonString(&s[i], valueLen, v);

    //NO HEX HERE
    else if (::isdigit(s[i]) || s[i] == '-' || s[i] == '+')
        return SkVariant::parseJsonNumber(&s[i], valueLen, v);

    else if (s[i] == 't' || s[i] == 'f')
        return SkVariant::parseJsonBoolean(&s[i], valueLen, v);

    StaticError("Value UNKNOWN: " << s);
    return false;
}

bool SkVariant::parseJsonMap(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    StaticPlusDebug("Parsing Json Map");

    uint64_t sz = strlen(s);
    uint64_t i;
    SkMap<SkVariant, SkVariant> m;
    bool isTerminated = false;
    SkVariant keyItemVal;
    bool isKeyExpected = true;
    bool isValueExpected = false;

    for(i=1; i<sz && !isTerminated; i++)
    {
        if (s[i] == '}')
            isTerminated = true;

        else if (s[i] == ':')
        {
            if (isValueExpected)
            {
                StaticError("Json Map expects a value, but it has found a double-points [key: " << keyItemVal << "]");
                return false;
            }

            else
            {
                isKeyExpected = false;
                isValueExpected = !isKeyExpected;
            }
        }

        else if (s[i] == ',')
        {
            if (isKeyExpected)
            {
                StaticError("Json Map expects a value, but it has found a comma [key: " << keyItemVal << "]");
                return false;
            }

            else
            {
                isKeyExpected = true;
                isValueExpected = !isKeyExpected;
            }
        }

        else if (::isspace(s[i]))
        {}

        else
        {
            if (isKeyExpected)
            {
                keyItemVal.nullify();
                uint64_t len;

                if (!SkVariant::parseJsonValue(&s[i], len, keyItemVal))
                    return false;

                i += (len-1);
                isKeyExpected = false;
            }

            else if (isValueExpected)
            {
                SkVariant valueItemVal;
                valueItemVal.nullify();
                uint64_t len;

                if (!SkVariant::parseJsonValue(&s[i], len, valueItemVal))
                    return false;

                i += (len-1);
                m[keyItemVal] = valueItemVal;
                isValueExpected = false;
            }

            else
            {
                StaticError("Json Map does NOT expect a value, but it has found a value");
                return false;
            }
        }
    }

    if (isTerminated)
    {
        valueLen = i;
        v.setVal(m);
    }

    else
        StaticError("Json Map is NOT correctly terminated");

    return isTerminated;
}

bool SkVariant::parseJsonList(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    StaticPlusDebug("Parsing Json List");

    uint64_t sz = strlen(s);
    uint64_t i;
    SkVector<SkVariant> l;
    bool isTerminated = false;

    SkVariant listItemVal;
    bool isValueExpected = true;

    for(i=1; i<sz && !isTerminated; i++)
    {
        if (s[i] == ']')
            isTerminated = true;

        else if (s[i] == ',')
        {
            if (isValueExpected)
            {
                StaticError("Json List expects a value, but it has found a comma");
                return false;
            }

            else
                isValueExpected = true;
        }

        else if (::isspace(s[i]))
        {}

        else
        {
            if (isValueExpected)
            {
                listItemVal.nullify();
                uint64_t len;

                if (!SkVariant::parseJsonValue(&s[i], len, listItemVal))
                    return false;

                i += (len-1);
                l << listItemVal;
                isValueExpected = false;
            }

            else
            {
                StaticError("Json List does NOT expect a value, but it has found a value");
                return false;
            }
        }
    }

    if (isTerminated)
    {
        valueLen = i;
        v.setVal(l);
    }

    else
        StaticError("Json List is NOT correctly terminated");

    return isTerminated;
}
#include <iostream>
#include <string>
#include <locale>
#include <codecvt>
bool SkVariant::parseJsonString(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    StaticPlusDebug("Parsing Json String");

    uint64_t sz = strlen(s);
    uint64_t i;
    SkString str;
    bool isTerminated = false;

    wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;

    for(i=1; i<sz && !isTerminated; i++)
    {
        if (i<sz-1 && s[i] == '\\')
        {
            if (s[i+1] == '"')        str += '"';
            else if (s[i+1] == 'b')   str += '\b';
            else if (s[i+1] == 'f')   str += '\f';
            else if (s[i+1] == 'n')   str += '\n';
            else if (s[i+1] == 'r')   str += '\r';
            else if (s[i+1] == 't')   str += '\t';
            else if (s[i+1] == 'a')   str += '\a';
            else if (s[i+1] == 'e')   str += '\e';

            else if (s[i+1] == 'u')
            {
                bool check = true;
                SkString utf8 = "\\";
                i++;

                for(; i<sz && check;)
                {
                    if (isalnum(s[i]))
                    {
                        utf8 += s[i];
                        i++;
                    }

                    else
                    {
                        i--;
                        i--;
                        check = false;
                    }
                }

                //string utf8 = "\\u00e8";
                int unicode = std::stoi(utf8.substr(2), nullptr, 16);
                str += converter.to_bytes(std::u32string(1, unicode));
            }

            else if (s[i+1] == '\\')  str += '\\';
            i++;
        }

        else if (s[i] == '"')
            isTerminated = true;

        else
            str.concat(s[i]);
    }

    //cout << "!!!! " << str << "\n";

    if (isTerminated)
    {
        valueLen = i;
        v.setVal(str);

        //cout << "____ " << str << "\n";
    }

    else
        StaticError("Json String is NOT correctly terminated");

    return isTerminated;
}


/*std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
std::string a = "\\u00e8";
std::cout << "\n\n" << a << "\n";
int unicode = std::stoi(a.substr(2), nullptr, 16);
std::string utf8_char = converter.to_bytes(std::u32string(1, unicode));
std::cout << "\n!!! " << utf8_char << std::endl;*/

/* std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
const char *s = "è";\\u00e8";
// Converti il carattere UTF-8 in un valore Unicode
std::u32string unicode = converter.from_bytes(s);
// Converti il valore Unicode in una sequenza di escape Unicode
std::stringstream ss;
ss << "\\u" << std::hex << std::setw(4) << std::setfill('0') << unicode[0];
std::string escape_sequence = ss.str();
std::cout << "!!! " << escape_sequence << std::endl;*/

bool SkVariant::parseJsonNumber(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    StaticPlusDebug("Parsing Json Number");

    SkString str;
    str += s[0];

    uint64_t sz = strlen(s);
    uint64_t i;
    bool isFloating = false;
    bool isTerminated = false;

    for(i=1; i<sz && !isTerminated; i++)
    {
        if (::isdigit(s[i]))
            str += s[i];

        else if (s[i] == '.')
        {
            if (isFloating)
            {
                StaticError("Floating numbers have ONLY ONE '.'");
                return false;
            }

            isFloating = true;
            str += s[i];
        }

        else
        {
            isTerminated = true;
            break;
        }
    }

    valueLen = i;

    if (isFloating)
        v = str.toDouble();

    else
        v = static_cast<int64_t>(str.toLongLong());

    return true;
}

bool SkVariant::parseJsonBoolean(CStr *s, uint64_t &valueLen, SkVariant &v)
{
    StaticPlusDebug("Parsing Json Boolean");

    uint64_t sz = strlen(s);

    if (sz >= 4)
    {
        if (memcmp(s, "true", 4) == 0)
        {
            v.setVal(true);
            valueLen = 4;
        }

        else if (memcmp(s, "false", 5) == 0)
        {
            v.setVal(false);
            valueLen = 5;
        }

        return true;
    }

    return false;
}

void SkVariant::toData(SkRingBuffer *buf)
{
    SkVariant::toData(buf, *this);
}

void SkVariant::toData(SkRingBuffer *buf, SkVariant &v)
{
    buf->addUInt8(v.props.t);
    buf->addUInt32(static_cast<uint32_t>(v.props.sz));

    if (v.props.sz)
        buf->addData(v.data(), v.props.sz);
}

void SkVariant::fromData(SkRingBuffer *buf)
{
    SkVariant::fromData(buf, *this);
}

void SkVariant::fromData(SkRingBuffer *buf, SkVariant &v)
{
    v.nullify();

    v.props.t = static_cast<SkVariant_T>(buf->getUInt8());
    v.props.sz = buf->getUInt32();

    if (v.props.sz)
    {
        v.props.valData = new char [v.props.sz];
        buf->getCustomBuffer(v.castToCharData(), v.props.sz);
    }
}

CStr *SkVariant::boolToString(bool v)
{
    return (v ? "true" : "false");
}

SkVariant &SkVariant::operator = (const SkVariant &operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (bool operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (int8_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (uint8_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (int16_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (uint16_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (int32_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (uint32_t operand)
{
    setVal(operand);
    return *this;
}
SkVariant &SkVariant::operator =  (int64_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (uint64_t operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (float operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (double operand)
{
    setVal(operand);
    return *this;
}

SkVariant &SkVariant::operator =  (CStr *cstr)
{
    setVal(cstr);
    return *this;
}

SkVariant &SkVariant::operator =  (SkString &s)
{
    setVal(s);
    return *this;
}

SkVariant &SkVariant::operator =  (SkStringList &l)
{
    setVal(l);
    return *this;
}

SkVariant &SkVariant::operator =  (SkStack<SkVariant> &s)
{
    setVal(s);
    return *this;
}

SkVariant &SkVariant::operator =  (SkQueue<SkVariant> &q)
{
    setVal(q);
    return *this;
}

SkVariant &SkVariant::operator =  (SkAbstractList<SkVariant> &l)
{
    setVal(l);
    return *this;
}

SkVariant &SkVariant::operator =  (SkPair<SkVariant, SkVariant> &p)
{
    setVal(p);
    return *this;
}

SkVariant &SkVariant::operator =  (SkAbstractMap<SkString, SkVariant> &m)
{
    setVal(m);
    return *this;
}

SkVariant &SkVariant::operator =  (SkAbstractMap<SkVariant, SkVariant> &m)
{
    setVal(m);
    return *this;
}

SkVariant &SkVariant::operator =  (void *ptr)
{
    setVal(ptr);
    return *this;
}

#define CalculateExpression(SYMBOL, T, REALFUNCT, CASTFUNCT) \
    { \
        if (v.props.t == T) \
            return REALFUNCT SYMBOL v.REALFUNCT; \
        else \
            return REALFUNCT SYMBOL v.CASTFUNCT; \
    }

#define CastedOpExp(SYMBOL, DFLT) \
    { \
        if (props.t == SkVariant_T::T_BOOL) \
            CalculateExpression(SYMBOL, SkVariant_T::T_BOOL, realBool(), toBool()) \
        \
        /*else if (props.t == SkVariant_T::T_CHAR)*/ \
            /*CalculateExpression(SYMBOL, SkVariant_T::T_CHAR, realChar(), toChar())*/ \
        \
        else if (props.t == SkVariant_T::T_INT8) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT8, realInt8(), toInt8()) \
        \
        else if (props.t == SkVariant_T::T_UINT8) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT8, realUInt8(), toUInt8()) \
        \
        else if (props.t == SkVariant_T::T_INT16) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT16, realInt16(), toInt16()) \
        \
        else if (props.t == SkVariant_T::T_UINT16) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT16, realUInt16(), toUInt16()) \
        \
        else if (props.t == SkVariant_T::T_INT32) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT32, realInt32(), toInt32()) \
        \
        else if (props.t == SkVariant_T::T_UINT32) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT32, realUInt32(), toUInt32()) \
        \
        else if (props.t == SkVariant_T::T_INT64) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT64, realInt64(), toInt64()) \
        \
        else if (props.t == SkVariant_T::T_UINT64) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT64, realUInt64(), toUInt64()) \
        \
        else if (props.t == SkVariant_T::T_FLOAT) \
            CalculateExpression(SYMBOL, SkVariant_T::T_FLOAT, realFloat(), toFloat()) \
        \
        else if (props.t == SkVariant_T::T_DOUBLE) \
            CalculateExpression(SYMBOL, SkVariant_T::T_DOUBLE, realDouble(), toDouble()) \
        \
        else \
            return DFLT; \
    }

#define CastedOpExpOnlyInteger(SYMBOL, DFLT) \
    { \
        if (props.t == SkVariant_T::T_BOOL) \
            CalculateExpression(SYMBOL, SkVariant_T::T_BOOL, realBool(), toBool()) \
        \
        /*else if (props.t == SkVariant_T::T_CHAR)*/ \
            /*CalculateExpression(SYMBOL, SkVariant_T::T_CHAR, realChar(), toChar())*/ \
        \
        else if (props.t == SkVariant_T::T_INT8) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT8, realInt8(), toInt8()) \
        \
        else if (props.t == SkVariant_T::T_UINT8) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT8, realUInt8(), toUInt8()) \
        \
        else if (props.t == SkVariant_T::T_INT16) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT16, realInt16(), toInt16()) \
        \
        else if (props.t == SkVariant_T::T_UINT16) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT16, realUInt16(), toUInt16()) \
        \
        else if (props.t == SkVariant_T::T_INT32) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT32, realInt32(), toInt32()) \
        \
        else if (props.t == SkVariant_T::T_UINT32) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT32, realUInt32(), toUInt32()) \
        \
        else if (props.t == SkVariant_T::T_INT64) \
            CalculateExpression(SYMBOL, SkVariant_T::T_INT64, realInt64(), toInt64()) \
        \
        else if (props.t == SkVariant_T::T_UINT64) \
            CalculateExpression(SYMBOL, SkVariant_T::T_UINT64, realUInt64(), toUInt64()) \
        \
        else \
            return DFLT; \
    }

#define CalculateSideExpression(SYMBOL, T, REALFUNCT, CASTFUNCT) \
    { \
        if (v.props.t == T) \
            REALFUNCT SYMBOL v.REALFUNCT; \
        else \
            REALFUNCT SYMBOL v.CASTFUNCT; \
    }

#define CastedOpSideExp(SYMBOL) \
    { \
        if (props.t == SkVariant_T::T_BOOL) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_BOOL, realBool(), toBool()) \
        \
        /*else if (props.t == SkVariant_T::T_CHAR)*/ \
            /*CalculateSideExpression(SYMBOL, SkVariant_T::T_CHAR, realChar(), toChar())*/ \
        \
        else if (props.t == SkVariant_T::T_INT8) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT8, realInt8(), toInt8()) \
        \
        else if (props.t == SkVariant_T::T_UINT8) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT8, realUInt8(), toUInt8()) \
        \
        else if (props.t == SkVariant_T::T_INT16) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT16, realInt16(), toInt16()) \
        \
        else if (props.t == SkVariant_T::T_UINT16) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT16, realUInt16(), toUInt16()) \
        \
        else if (props.t == SkVariant_T::T_INT32) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT32, realInt32(), toInt32()) \
        \
        else if (props.t == SkVariant_T::T_UINT32) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT32, realUInt32(), toUInt32()) \
        \
        else if (props.t == SkVariant_T::T_INT64) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT64, realInt64(), toInt64()) \
        \
        else if (props.t == SkVariant_T::T_UINT64) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT64, realUInt64(), toUInt64()) \
        \
        else if (props.t == SkVariant_T::T_FLOAT) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_FLOAT, realFloat(), toFloat()) \
        \
        else if (props.t == SkVariant_T::T_DOUBLE) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_DOUBLE, realDouble(), toDouble()) \
    }


#define CastedOpSideExpOnlyInteger(SYMBOL) \
    { \
        if (props.t == SkVariant_T::T_BOOL) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_BOOL, realBool(), toBool()) \
        \
        /*else if (props.t == SkVariant_T::T_CHAR)*/ \
            /*CalculateSideExpression(SYMBOL, SkVariant_T::T_CHAR, realChar(), toChar())*/ \
        \
        else if (props.t == SkVariant_T::T_INT8) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT8, realInt8(), toInt8()) \
        \
        else if (props.t == SkVariant_T::T_UINT8) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT8, realUInt8(), toUInt8()) \
        \
        else if (props.t == SkVariant_T::T_INT16) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT16, realInt16(), toInt16()) \
        \
        else if (props.t == SkVariant_T::T_UINT16) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT16, realUInt16(), toUInt16()) \
        \
        else if (props.t == SkVariant_T::T_INT32) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT32, realInt32(), toInt32()) \
        \
        else if (props.t == SkVariant_T::T_UINT32) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT32, realUInt32(), toUInt32()) \
        \
        else if (props.t == SkVariant_T::T_INT64) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_INT64, realInt64(), toInt64()) \
        \
        else if (props.t == SkVariant_T::T_UINT64) \
            CalculateSideExpression(SYMBOL, SkVariant_T::T_UINT64, realUInt64(), toUInt64()) \
    }

#define CastedOpSideIncDec(SYMBOL) \
    { \
        /*if (props.t == SkVariant_T::T_CHAR)*/ \
            realChar() SYMBOL; \
        \
        /*else */if (props.t == SkVariant_T::T_INT8) \
            realInt8() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_UINT8) \
            realUInt8() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_INT16) \
            realInt16() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_UINT16) \
            realUInt16() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_INT32) \
            realInt32() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_UINT32) \
            realUInt32() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_INT64) \
            realInt64() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_UINT64) \
            realUInt64() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_FLOAT) \
            realFloat() SYMBOL; \
        \
        else if (props.t == SkVariant_T::T_DOUBLE) \
            realDouble() SYMBOL; \
    }

bool SkVariant::operator == (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(==, false)

    else
    {
        if (props.sz != operand.props.sz)
        {
            SkVariant op(operand);
            return op.toString() == toString();
        }

        return (memcmp(props.valData, v.props.valData, props.sz) == 0);
    }

    return false;
}

bool SkVariant::operator != (const SkVariant &operand)
{
    return !(*this==operand);
}

bool SkVariant::operator < (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(<, false)

    return props.sz < v.props.sz;
}

bool SkVariant::operator <= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(<=, false)

    return props.sz <= v.props.sz;
}

bool SkVariant::operator > (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(>, false)

    return props.sz > v.props.sz;
}

bool SkVariant::operator >= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(>=, false)

    return props.sz >= v.props.sz;
}

SkVariant SkVariant::operator + (const SkVariant &operand)
{
    SkVariant v(operand);

    if (v.isEmpty())
    {
        FlatError("v2 is empty");
        return 0;
    }

    if (isNumber() && v.isNumber())
        CastedOpExp(+, 0)

    else if (isString() && (v.isString() || v.isByteArray()))
    {
        uint64_t v1Size = props.sz-1;
        uint64_t v2Size = v.props.sz;
        uint64_t totSize = v1Size + v2Size;

        bool hasTerminator = (SkArrayCast::toChar(v.props.valData)[v2Size-1] == '\0');

        if (!hasTerminator)
            totSize++;

        char *newValData = new char [totSize];
        memcpy(newValData, props.valData, v1Size);
        memcpy(&newValData[v1Size], v.props.valData, v2Size);

        //INC
        /*delete [] castToCharData();
        props.valData = newValData;
        props.sz = totSize;*/

        if (!hasTerminator)
            newValData[totSize-1] = '\0';

        SkVariantData tempProps;
        tempProps.valData = newValData;
        tempProps.sz = totSize;
        tempProps.t = props.t;

        return tempProps;
    }

    else if (isString() && v.isNumber())
    {

    }

    else if (isString() && v.isNumber())
    {

    }

    else if (isByteArray())
    {

    }

    else
        FlatError("Cannot make sum on type: " << variantTypeName());


    return 0;
}

SkVariant &SkVariant::operator += (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpSideExp(+=)

    return *this;
}

/*SkVariant &SkVariant::operator ++ ()
{
    if (isNumber())
    {
        if (isBoolean())
            FlatError("Cannot increment a boolean value");

        else
            CastedOpSideIncDec(++)
    }

    return *this;
}*/

SkVariant SkVariant::operator - (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(-, 0)

    return 0;
}

SkVariant &SkVariant::operator -= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpSideExp(-=)

    return *this;
}

/*SkVariant &SkVariant::operator -- ()
{
    if (isNumber())
    {
        if (isBoolean())
            FlatError("Cannot decrement a boolean value");

        else
            CastedOpSideIncDec(--)
    }

    return *this;
}*/

SkVariant SkVariant::operator * (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpExp(*, 0)

    return 0;
}

SkVariant &SkVariant::operator *= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
        CastedOpSideExp(*=)

    return *this;
}

SkVariant SkVariant::operator ^ (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (!isInteger() || !v.isInteger())
            FlatError("Power is valid ONLY for integers");

        else
            CastedOpExpOnlyInteger(^, 0)
    }

    return 0;
}

SkVariant &SkVariant::operator ^= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (!isInteger() || !v.isInteger())
            FlatError("Power is valid ONLY for integers");

        else
            CastedOpSideExpOnlyInteger(^=)
    }

    return *this;
}

SkVariant SkVariant::operator / (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (v == 0)
            FlatError("Division by zero");

        else
           CastedOpExp(/, 0)
    }

    return 0;
}

SkVariant &SkVariant::operator /= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (v == 0)
            FlatError("Division by zero");

        else
            CastedOpSideExp(/=)
    }

    return *this;
}

SkVariant SkVariant::operator % (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (!isInteger() || !v.isInteger())
            FlatError("Remainder is valid ONLY for integers");

        else
        {
            if (v == 0)
                FlatError("Division by zero");

            else
                CastedOpExpOnlyInteger(%, 0)
        }
    }

    return 0;
}

SkVariant &SkVariant::operator %= (const SkVariant &operand)
{
    SkVariant v(operand);

    if (isNumber() && v.isNumber())
    {
        if (!isInteger() || !v.isInteger())
            FlatError("Remainder is valid ONLY for integers");

        else
        {
            if (v == 0)
                FlatError("Division by zero");

            else
                CastedOpSideExpOnlyInteger(%=)
        }
    }

    return *this;
}

SkString &SkVariant::toStringRef()
{
    if (!isString())
        FlatError("Getting StringRef from Variant of WRONG type: " << variantTypeName());

    if (!stringRef)
    {
        stringRef = new SkString;
        FlatPlusDebug("Created StringRef: [" << variantTypeName() << "]");
    }

    else if (isEmpty())
        return *stringRef;

    *stringRef = data();
    return *stringRef;
}

bool SkVariant::updateOnFromStringRef()
{
    FlatPlusDebug("Checking Referenced-parameter [" << variantTypeName()  <<"] ..");

    if (props.t == SkVariant_T::T_STRING)
    {
        //str is allocated ONLY if used! it is not required to check
        if (stringRef)
        {
            FlatPlusDebug("Referenced-parameter for StringRef Changed [" << props.sz << " -> "
                       << stringRef->size() << " B]");

            setVal(*stringRef);
            stringRef->clear();
            return true;
        }
    }

    return false;
}

bool SkVariant::getMapProperty(SkVariant &propName, SkVariant &val)
{
    if (!isMap())
        return false;

    if (!val.isNull())
        val.nullify();

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    SkVariant k;
    SkVariant v;
    bool ok = false;

    while(!r.isEmpty())
    {
        SkVariant::fromData(&r, k);

        if (k == propName)
        {
            ok = true;
            SkVariant::fromData(&r, val);
            break;
        }

        else
            SkVariant::fromData(&r, v);
    }

    return ok;
}

bool SkVariant::getListItem(uint64_t index, SkVariant &val)
{
    if (!isList())
        return false;

    if (!val.isNull())
        val.nullify();

    SkRingBuffer r;
    r.addData(castToCharData(), props.sz);

    uint64_t counter = 0;
    SkVariant v;
    bool ok = false;

    while(!r.isEmpty())
    {
        if (counter == index)
        {
            ok = true;
            SkVariant::fromData(&r, val);
            break;
        }

        else
            SkVariant::fromData(&r, v);

        counter++;
    }

    return ok;
}

// // // // // // // // // // // // // // // // // // // // //

#include "skvariant.h"

DeclareWrapper(SkVariantVector);

DeclareMeth_INSTANCE_VOID(SkVariantVector, setCapacity, Arg_UInt64)
DeclareMeth_INSTANCE_VOID(SkVariantVector, append, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkVariantVector, prepend, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkVariantVector, insert, Arg_AbstractVariadic, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkVariantVector, contains, bool, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantVector, indexOf, int64_t, Arg_AbstractVariadic, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkVariantVector, set, bool, Arg_AbstractVariadic, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkVariantVector, get, SkVariant&, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkVariantVector, first, SkVariant&)
DeclareMeth_INSTANCE_RET(SkVariantVector, last, SkVariant&)
DeclareMeth_INSTANCE_RET(SkVariantVector, remove, SkVariant, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantVector, removeAt, SkVariant, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkVariantVector, removeFirst, SkVariant)
DeclareMeth_INSTANCE_RET(SkVariantVector, removeLast, SkVariant)
DeclareMeth_INSTANCE_VOID(SkVariantVector, clear)
DeclareMeth_INSTANCE_RET(SkVariantVector, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkVariantVector, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantVector, size, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantVector, capacity, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantVector, capacitySize, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantVector, data, SkVariant *)

SetupClassWrapper(SkVariantVector)
{
    SetClassSuper(SkVariantVector, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkVariantVector, setCapacity);
    AddMeth_INSTANCE_VOID(SkVariantVector, append);
    AddMeth_INSTANCE_VOID(SkVariantVector, prepend);
    AddMeth_INSTANCE_VOID(SkVariantVector, insert);
    AddMeth_INSTANCE_RET(SkVariantVector, contains);
    AddMeth_INSTANCE_RET(SkVariantVector, indexOf);
    AddMeth_INSTANCE_RET(SkVariantVector, set);
    AddMeth_INSTANCE_RET(SkVariantVector, get);
    AddMeth_INSTANCE_RET(SkVariantVector, first);
    AddMeth_INSTANCE_RET(SkVariantVector, last);
    AddMeth_INSTANCE_RET(SkVariantVector, remove);
    AddMeth_INSTANCE_RET(SkVariantVector, removeAt);
    AddMeth_INSTANCE_RET(SkVariantVector, removeFirst);
    AddMeth_INSTANCE_RET(SkVariantVector, removeLast);
    AddMeth_INSTANCE_VOID(SkVariantVector, clear);
    AddMeth_INSTANCE_RET(SkVariantVector, isEmpty);
    AddMeth_INSTANCE_RET(SkVariantVector, count);
    AddMeth_INSTANCE_RET(SkVariantVector, size);
    AddMeth_INSTANCE_RET(SkVariantVector, capacity);
    AddMeth_INSTANCE_RET(SkVariantVector, capacitySize);
    AddMeth_INSTANCE_RET(SkVariantVector, data);
}

// // // // // // // // // // // // // // // // // // // // //

SkVariantVector::SkVariantVector() : SkVector<SkVariant>()
{
    CreateClassWrapper(SkVariantVector);
}

SkVariantVector::SkVariantVector(const SkVector<SkVariant> &source) : SkVector<SkVariant>(source)
{
    CreateClassWrapper(SkVariantVector);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkVariantList);

SetupClassWrapper(SkVariantList)
{
    SetClassSuper(SkVariantList, SkFlatObject);
}

// // // // // // // // // // // // // // // // // // // // //

SkVariantList::SkVariantList() : SkList<SkVariant>()
{
    CreateClassWrapper(SkVariantList);
}

SkVariantList::SkVariantList(const SkList<SkVariant> &source) : SkList<SkVariant>(source)
{
    CreateClassWrapper(SkVariantList);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkVariantStack);

DeclareMeth_INSTANCE_VOID(SkVariantStack, push, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantStack, pop, SkVariant)
DeclareMeth_INSTANCE_VOID(SkVariantStack, clear)
DeclareMeth_INSTANCE_RET(SkVariantStack, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkVariantStack, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantStack, size, uint64_t)

SetupClassWrapper(SkVariantStack)
{
    SetClassSuper(SkVariantStack, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkVariantStack, push);
    AddMeth_INSTANCE_RET(SkVariantStack, pop);
    AddMeth_INSTANCE_VOID(SkVariantStack, clear);
    AddMeth_INSTANCE_RET(SkVariantStack, isEmpty);
    AddMeth_INSTANCE_RET(SkVariantStack, count);
    AddMeth_INSTANCE_RET(SkVariantStack, size);
}

// // // // // // // // // // // // // // // // // // // // //

SkVariantStack::SkVariantStack() : SkStack<SkVariant>()
{
    CreateClassWrapper(SkVariantStack);
}

SkVariantStack::SkVariantStack(const SkVariantStack &source) : SkStack<SkVariant>(source)
{
    CreateClassWrapper(SkVariantStack);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkVariantQueue);

DeclareMeth_INSTANCE_VOID(SkVariantQueue, enqueue, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantQueue, dequeue, SkVariant)
DeclareMeth_INSTANCE_VOID(SkVariantQueue, clear)
DeclareMeth_INSTANCE_RET(SkVariantQueue, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkVariantQueue, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantQueue, size, uint64_t)

SetupClassWrapper(SkVariantQueue)
{
    SetClassSuper(SkVariantQueue, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkVariantQueue, enqueue);
    AddMeth_INSTANCE_RET(SkVariantQueue, dequeue);
    AddMeth_INSTANCE_VOID(SkVariantQueue, clear);
    AddMeth_INSTANCE_RET(SkVariantQueue, isEmpty);
    AddMeth_INSTANCE_RET(SkVariantQueue, count);
    AddMeth_INSTANCE_RET(SkVariantQueue, size);
}

// // // // // // // // // // // // // // // // // // // // //

SkVariantQueue::SkVariantQueue() : SkQueue<SkVariant>()
{
    CreateClassWrapper(SkVariantQueue);
}

SkVariantQueue::SkVariantQueue(const SkQueue<SkVariant> &source) : SkQueue<SkVariant>(source)
{
    CreateClassWrapper(SkVariantQueue);
}

// // // // // // // // // // // // // // // // // // // // //
DeclareWrapper(SkVariantPair);

DeclareMeth_INSTANCE_VOID(SkVariantPair, set, Arg_AbstractVariadic, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkVariantPair, setKey, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantPair, key, SkVariant &)
DeclareMeth_INSTANCE_VOID(SkVariantPair, setValue, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantPair, value, SkVariant &)
DeclareMeth_INSTANCE_VOID(SkVariantPair, swap, *Arg_Custom(SkVariantPair))

SetupClassWrapper(SkVariantPair)
{
    AddMeth_INSTANCE_VOID(SkVariantPair, set);
    AddMeth_INSTANCE_VOID(SkVariantPair, setKey);
    AddMeth_INSTANCE_RET(SkVariantPair, key);
    AddMeth_INSTANCE_VOID(SkVariantPair, setValue);
    AddMeth_INSTANCE_RET(SkVariantPair, value);
    AddMeth_INSTANCE_VOID(SkVariantPair, swap);
}

SkVariantPair::SkVariantPair()
{
    CreateClassWrapper(SkVariantPair);
}

SkVariantPair::SkVariantPair(const SkPair<SkVariant, SkVariant> &source) : SkPair<SkVariant, SkVariant>(source)
{
    CreateClassWrapper(SkVariantPair);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkVariantMap);

DeclareMeth_INSTANCE_VOID(SkVariantMap, add, Arg_AbstractVariadic, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantMap, remove, SkVariantPair, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantMap, contains, bool, Arg_AbstractVariadic)
//DeclareMeth_INSTANCE_RET(SkVariantMap, key, SkVariant &, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkVariantMap, keys, *Arg_Custom(SkVariantList))
DeclareMeth_INSTANCE_RET(SkVariantMap, value, SkVariant &, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_VOID(SkVariantMap, values, *Arg_Custom(SkVariantList))
DeclareMeth_INSTANCE_VOID(SkVariantMap, clear)
DeclareMeth_INSTANCE_RET(SkVariantMap, isEmpty, bool)
DeclareMeth_INSTANCE_RET(SkVariantMap, count, uint64_t)
DeclareMeth_INSTANCE_RET(SkVariantMap, countValues, uint64_t, Arg_AbstractVariadic)
DeclareMeth_INSTANCE_RET(SkVariantMap, size, uint64_t)

SetupClassWrapper(SkVariantMap)
{
    SetClassSuper(SkVariantMap, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkVariantMap, add);
    AddMeth_INSTANCE_RET(SkVariantMap, remove);
    AddMeth_INSTANCE_RET(SkVariantMap, contains);
    //AddMeth_INSTANCE_RET(SkVariantMap, key);
    AddMeth_INSTANCE_VOID(SkVariantMap, keys);
    AddMeth_INSTANCE_RET(SkVariantMap, value);
    AddMeth_INSTANCE_VOID(SkVariantMap, values);
    AddMeth_INSTANCE_VOID(SkVariantMap, clear);
    AddMeth_INSTANCE_RET(SkVariantMap, isEmpty);
    AddMeth_INSTANCE_RET(SkVariantMap, count);
    AddMeth_INSTANCE_RET(SkVariantMap, countValues);
    AddMeth_INSTANCE_RET(SkVariantMap, size);
}

SkVariantMap::SkVariantMap() : SkMap<SkVariant, SkVariant>()
{
    CreateClassWrapper(SkVariantQueue);
}

SkVariantMap::SkVariantMap(const SkMap<SkVariant, SkVariant> &source) : SkMap<SkVariant, SkVariant>(source)
{
    CreateClassWrapper(SkVariantMap);
}

// // // // // // // // // // // // // // // // // // // // //

#define DeclarePrimitiveCPointer(CLASS, TYPE, REAL_ARG) \
    DeclareWrapper(CLASS); \
    DeclareMeth_INSTANCE_RET(CLASS, allocate, bool, Arg_UInt64) \
    DeclareMeth_INSTANCE_RET(CLASS, reallocate, bool, Arg_UInt64) \
    DeclareMeth_INSTANCE_VOID(CLASS, set, Arg_UInt64, REAL_ARG) \
    DeclareMeth_INSTANCE_RET(CLASS, get, TYPE &, Arg_UInt64) \
    DeclareMeth_INSTANCE_RET(CLASS, destroy, bool) \
    DeclareMeth_INSTANCE_VOID(CLASS, clear) \
    DeclareMeth_INSTANCE_RET(CLASS, isEmpty, bool) \
    DeclareMeth_INSTANCE_RET(CLASS, size, uint64_t) \
    SetupClassWrapper(CLASS) \
    { \
        SetClassSuper(CLASS, SkFlatobject); \
        AddMeth_INSTANCE_RET(CLASS, allocate); \
        AddMeth_INSTANCE_RET(CLASS, reallocate); \
        AddMeth_INSTANCE_VOID(CLASS, set); \
        AddMeth_INSTANCE_RET(CLASS, get); \
        AddMeth_INSTANCE_RET(CLASS, destroy); \
        AddMeth_INSTANCE_VOID(CLASS, clear); \
        AddMeth_INSTANCE_RET(CLASS, isEmpty); \
        AddMeth_INSTANCE_RET(CLASS, size); \
    }

DeclarePrimitiveCPointer(SkInt8Ptr,     int8_t,     Arg_Int8_REAL)
DeclarePrimitiveCPointer(SkUInt8Ptr,    uint8_t,    Arg_UInt8_REAL)
DeclarePrimitiveCPointer(SkInt16Ptr,    int16_t,    Arg_Int16_REAL)
DeclarePrimitiveCPointer(SkUInt16Ptr,   uint16_t,   Arg_UInt16_REAL)
DeclarePrimitiveCPointer(SkInt32Ptr,    int32_t,    Arg_Int32_REAL)
DeclarePrimitiveCPointer(SkUInt32Ptr,   uint32_t,   Arg_UInt32_REAL)
DeclarePrimitiveCPointer(SkInt64Ptr,    int64_t,    Arg_Int64_REAL)
DeclarePrimitiveCPointer(SkUInt64Ptr,   uint64_t,   Arg_UInt64_REAL)
DeclarePrimitiveCPointer(SkFloatPtr,    float,      Arg_Float_REAL)
DeclarePrimitiveCPointer(SkDoublePtr,   double,     Arg_Double_REAL)

// // // // // // // // // // // // // // // // // // // // //

SkInt8Ptr::SkInt8Ptr() : SkCPointer<int8_t>(){CreateClassWrapper(SkInt8Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkUInt8Ptr::SkUInt8Ptr() : SkCPointer<uint8_t>(){CreateClassWrapper(SkUInt8Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkInt16Ptr::SkInt16Ptr() : SkCPointer<int16_t>(){CreateClassWrapper(SkInt16Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkUInt16Ptr::SkUInt16Ptr() : SkCPointer<uint16_t>(){CreateClassWrapper(SkUInt16Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkInt32Ptr::SkInt32Ptr() : SkCPointer<int32_t>(){CreateClassWrapper(SkInt32Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkUInt32Ptr::SkUInt32Ptr() : SkCPointer<uint32_t>(){CreateClassWrapper(SkUInt32Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkInt64Ptr::SkInt64Ptr() : SkCPointer<int64_t>(){CreateClassWrapper(SkInt64Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkUInt64Ptr::SkUInt64Ptr() : SkCPointer<uint64_t>(){CreateClassWrapper(SkUInt64Ptr);}

// // // // // // // // // // // // // // // // // // // // //

SkFloatPtr::SkFloatPtr() : SkCPointer<float>(){CreateClassWrapper(SkFloatPtr);}

// // // // // // // // // // // // // // // // // // // // //

SkDoublePtr::SkDoublePtr() : SkCPointer<double>(){CreateClassWrapper(SkDoublePtr);}

// // // // // // // // // // // // // // // // // // // // //

void *variantToCustom(SkVariant *v)
{
    if (!v->toCustom)
    {
        StaticError("toCustom callback is NOT valid");
        return nullptr;
    }

    StaticPlusDebug("Executinging toCustom callback");
    return v->toCustom(v);
}
