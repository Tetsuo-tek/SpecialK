/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKDATABUFFER_H
#define SKDATABUFFER_H

#include "Core/Containers/skstring.h"
#include "Core/System/skdatacrypt.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkRingBuffer;
class SkObject;

//IT IS NOT A RING AND IS NOT TH-SAFE

/**
  * @brief SkDataBuffer
  * It is a growable buffer; it is not thread-safe
*/

class SPECIALK SkDataBuffer extends SkFlatObject
{
    public:
        /**
         * @brief Simple constructor
        */
        SkDataBuffer();

        /**
         * @brief Contructor for a starting size; all bytes is put to 0
         * @param bytes the size to allocate
        */
        SkDataBuffer(uint64_t bytes);

        /**
         * @brief Contructor that initializes from another buffer, copying it
         * @param bytes the size to allocate
        */
        SkDataBuffer(SkDataBuffer &other);

        /**
         * @brief Destructor; it will destroy data inside
        */
        ~SkDataBuffer();

        //IT WILL NOT COPY, BUT IT ASSIGNs AND OWNs THE POINTER
        bool setRawData(void *data, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Sets internal data with a copy of a raw pointer
         * @param data the raw data to store
         * @param size the bytes size to store, could be less than the real size of original data
         * @param notifyLoad if true there will be a notify from internal standalone signal
         * @return true if size is valid, otherwise false
        */
        bool setData(CVoid *data, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Sets internal data with a copy of a string
         * @param s the string to store
         * @warning the string terminator will not be considered
         * @param notifyLoad if true there will be a notify from internal standalone signal
         * @return true if size is valid, otherwise false
        */
        bool setData(SkString &s, bool notifyLoad=false);

        /**
         * @brief Sets internal data with a copy of an existing buffer
         * @param source the buffer to store
         * @param notifyLoad if true there will be a notify from internal standalone signal
         * @return true if size is valid, otherwise false
        */
        bool setData(SkDataBuffer &source, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Sets internal data with a copy of an existing ring-buffer
         * @param source the ring-buffer to store
         * @param notifyLoad if true there will be a notify from internal standalone signal
         * @return true if size is valid, otherwise false
        */
        bool setData(SkRingBuffer *source, uint64_t size=0, bool notifyLoad=false);

        //THIS REPLACE EACH EXISTING BYTE WITH THE C VALUE

        /**
         * @brief Sets all bytes to argument value
         * @param c the char value
         * @return true if size is valid, otherwise false
        */
        bool set(char c);

        /**
         * @brief setZero Sets all bytes to 0
         * @return true if size is valid, otherwise false
        */
        bool setZero();

        //'from' PARAMETER INSIDE THESE METHs, MEANS THE LOCATION IN this BUFFER
        //WHERE TO START THE INSERTION OF THE DATA

        /**
         * @brief Prepends data to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param data the prepending data
         * @param size the size of prepending data
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool prepend(CStr *data, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Makes exatly the same of prepend
        */
        bool insert(CStr *data, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Inserts data to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param data the inserting data
         * @param from the position where to insert; the existing data starting
         * from this position will be shifted of size length
         * @warning from must be less than the buffer size
         * @param size the size of inserting data
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool insert(CStr *data, uint64_t from, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Appends data to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param data the appending data
         * @param size the size of appending data
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool append(CStr *data, uint64_t size, bool notifyLoad=false);

        /**
         * @brief Prepends string to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the prepending string
         * @param size the size of prepending string; if size is 0, it will be used
         * the entire string size
         * @warning the string terminator will not be considered
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool prepend(SkString &s, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Inserts string to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the inserting string
         * @param from the position where to insert; the existing data starting
         * from this position will be shifted of size length
         * @warning from must be less than the buffer size
         * @param size the size of inserting string; if size is 0, it will be
         * used the entire string size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool insert(SkString &s, uint64_t from=0, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Appends string to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the appending string
         * @param size the size of appending string; if size is 0, it will be
         * used the entire string size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool append(SkString &s, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Prepends buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the prepending buffer
         * @param size the size of prepending buffer; if size is 0, it will be used
         * the entire buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool prepend(SkDataBuffer &buf, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Inserts buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the inserting buffer
         * @param from the position where to insert; the existing data starting
         * from this position will be shifted of size length
         * @warning from must be less than the buffer size
         * @param size the size of inserting buffer; if size is 0, it will be
         * used the entire buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool insert(SkDataBuffer &buf, uint64_t from=0, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Appends buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the appending buffer
         * @param size the size of appending buffer; if size is 0, it will be
         * used the entire buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool append(SkDataBuffer &buf, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Prepends ring-buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the prepending ring-buffer
         * @param size the size of prepending ring-buffer; if size is 0, it will be used
         * the entire ring-buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool prepend(SkRingBuffer *buf, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief ring-buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the inserting ring-buffer
         * @param from the position where to insert; the existing data starting
         * from this position will be shifted of size length
         * @warning from must be less than the buffer size
         * @param size the size of inserting ring-buffer; if size is 0, it will be
         * used the entire ring-buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool insert(SkRingBuffer *buf, uint64_t from=0, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Appends ring-buffer to the buffer; if buffer is empty its behaviour is
         * equal to setData
         * @param s the appending ring-buffer
         * @param size the size of appending ring-buffer; if size is 0, it will be
         * used the entire ring-buffer size
         * @param notifyLoad if true there will be a notify from internal
         * standalone signal
         * @return true if size is valid, otherwise false
        */
        bool append(SkRingBuffer *buf, uint64_t size=0, bool notifyLoad=false);

        /**
         * @brief Fills the buffer with specified value
         * @param c the filling value
         * @param count the size to fill; can be different from buffer size
         */
        void fill(char c, uint64_t count);

        /**
         * @brief Search the index of some char in the buffer
         * @param c the char to search
         * @param from the starting position where to search
         * @return the first position of the char if it exists, otherwise -1
         */
        int64_t indexOf(char c, uint64_t from=0);

        /**
         * @brief Search the index of some char sequence in the buffer
         * @param d the char sequence to search
         * @param size the char sequence length
         * @param from the starting position where to search
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(CStr *d, uint64_t size, uint64_t from=0);

        /**
         * @brief Search the index of some string in the buffer
         * @param s the stringe to search
         * @param from the starting position where to search
         * @return the first position of the char sequence if it exists, otherwise -1
         */
        int64_t indexOf(SkString &s, uint64_t from=0);

        /**
         * @brief Substitutes one char with another in the buffer
         * @param oldCh the char to substitute
         * @param newCh the substituting char
         * @param from the starting position where to init the substitution
         * @return true if the char to substitute exists, otherwise false
         */
        bool substitute(char oldCh, char newCh, uint64_t from=0);

        /**
         * @brief Substitutes one char sequence with another in the buffer
         * @param oldData the char sequence to substitute
         * @param oldDataSz the char sequence size
         * @param newData the substituting char sequence
         * @param newDataSz the substituting char sequence size
         * @param from the starting position where to init the substitution
         * @return true if the char sequence to substitute exists, otherwise false
         */
        bool substitute(CStr *oldData, uint64_t oldDataSz, CStr *newData, uint64_t newDataSz, uint64_t from=0);

        /**
         * @brief Substitutes one string with another in the buffer
         * @param oldStr the string to substitute
         * @param newStr the substituting string
         * @param from the starting position where to init the substitution
         * @return true if the newStr to substitute exists, otherwise false
         */
        bool substitute(SkString &oldStr, SkString &newStr, uint64_t from=0);

        /**
         * @brief Removes a segment of the buffer
         * @param from the starting position where to init the deletion
         * @param size the length of the deletion
         * @return true if from and size parameters are valid, otherwise false
         */
        bool remove(uint64_t from, uint64_t size);

        /**
         * @brief Copies data to a pointer from the buffer
         * @param data the pointer where to copy data
         * @param size the size of data to copy
         * @return true if the buffer contains the requested size, otherwise false
         */
        bool copyTo(char *data, uint64_t size);

        /**
         * @brief Copies data to a pointer from the buffer
         * @param data the pointer where to copy data
         * @param from the starting position in the buffer where to init the copy
         * @param size the size of data to copy
         * @return true if from and size parameters are valid, otherwise false
         */
        bool copyTo(char *data, uint64_t from, uint64_t size);

        //IF size ARGS IS 0 IT WILL USE this.size()
        //copyTo MEANS APPEND-TO

        /**
         * @brief Copies data to another buffer, appending them
         * @param target the target buffer
         * @param from the starting position in the buffer where to init the copy
         * @param size the size of data to copy; if it is 0 the entire buffer size
         * is copied
         * @return true if from and size parameters are valid, otherwise false
         */
        bool copyTo(SkDataBuffer &target, uint64_t from=0, uint64_t size=0);

        /**
         * @brief Copies data to a ring-buffer, appending them
         * @param target the target ring-buffer
         * @param from the starting position in the buffer where to init the copy
         * @param size the size of data to copy; if it is 0 the entire buffer size
         * is copied
         * @return true if from and size parameters are valid, otherwise false
         */
        bool copyTo(SkRingBuffer *target, uint64_t from=0, uint64_t size=0);

        /**
         * @brief Copies data to a string, appending them
         * @param target the target string
         * @param from the starting position in the buffer where to init the copy
         * @param size the size of data to copy; if it is 0 the entire buffer size
         * is copied
         * @return true if from and size parameters are valid, otherwise false
         */
        bool copyTo(SkString &target, uint64_t from=0, uint64_t size=0);

        /**
         * @brief Copies data starting from the start of the buffer
         * @param data the pointer where to copy data
         * @param size the size of data to copy
         * @return true if the requested size is valid, otherwise false
         */
        bool left(char *data, uint64_t size);

        /**
         * @brief Copies data starting from the end of the buffer
         * @param data the pointer where to copy data
         * @param size the size of data to copy
         * @return true if the requested size is valid, otherwise false
         */
        bool right(char *data, uint64_t size);

        /**
         * @brief Compare data with the buffer, meaning for the buffer size
         * @param data the source of the compare action
         * @return true if data is equal to the buffer, otherwise false
         */
        bool compare(CStr *data);

        /**
         * @brief Compare data with the buffer
         * @param data the source of the compare action
         * @param from the starting position in the buffer where to init the compare action
         * @param size the size of the buffer to compare
         * @return true if from and size parameters are valid and if data is equal
         * to the buffer segment, otherwise false
         */
        bool compare(CStr *data, uint64_t from, uint64_t size);

        /**
         * @brief Checks if the the buffer starts with comparing data
         * @param data the source of the compare action
         * @param size the size of data to compare
         * @return true if size is valid and if data is equal to the buffer
         * segment, otherwise false
         */
        bool startsWith(CStr *data, uint64_t size);

        /**
         * @brief Checks if the the buffer starts with comparing string
         * @param s the source string to compare
         * @return true if size of the string is valid for the buffer and if string
         * is equal to the buffer segment, otherwise false
         */
        bool startsWith(SkString &s);

        /**
         * @brief Checks if the the buffer ends with comparing data
         * @param data the source of the compare action
         * @param size the size of data to compare
         * @return true if size is valid and if data is equal to the buffer
         * segment, otherwise false
         */
        bool endsWith(CStr *data, uint64_t size);

        /**
         * @brief Checks if the the buffer ends with comparing string
         * @param s the source string to compare
         * @return true if size of the string is valid for the buffer and if string
         * is equal to the buffer segment, otherwise false
         */
        bool endsWith(SkString &s);

        /**
         * @brief Grabs the char at the specified position
         * @param element the char reference to fill with value
         * @param index the position of the char to grab
         * @return true if the index is valid, otherwise false
         */
        bool at(char &element, uint64_t index);

        /**
         * @brief Gets the char at the specified position
         * @param index the position of the char to grab
         * @return the char at requested potition, if it is valid, otherwise return 0 (or'\0')
         */
        char at(uint64_t index);

        /**
         * @brief Get constant char pointer of the internal data
         * @return the internal constant char pointer
         */
        CStr *data();

        /**
         * @brief Get raw pointer of the internal data
         * @warning destroying the pointer will make unconsistent the buffer
         * @return the raw writable pointer
         */
        void *toVoid();

        /**
         * @brief Get a string version of the internal data, adding the string terminator
         * @warning it is intended that the data contained from the buffer are
         * valide characters
         * @return a string with the buffer data; if the buffer is empty return an
         * empty string
         */
        SkString toString();

        /**
         * @brief Makes a string based (Base64) hash of the buffer data
         * @param output the string reference where to put the hash
         * @param mode the SkHashMode enum value for the requested hash type
         */
        void toStringHash(SkString &output, SkHashMode mode=SkHashMode::HM_MD5);

        /**
         * @brief Makes a binary hash of the buffer data
         * @param output the buffer reference where to put the hash
         * @param mode the SkHashMode enum value for the requested hash type
         */
        void toBinaryHash(SkDataBuffer &output, SkHashMode mode=SkHashMode::HM_MD5);

        /**
         * @brief Encodes the buffer content to Base64
         * @param output the other buffer reference where to put the encoded data
         */
        void toBase64(SkDataBuffer &output);

        /**
         * @brief Decodes the other buffer content from Base64
         * @param input the other buffer reference with data to decode
         */
        void fromBase64(SkDataBuffer &input);

        //THESE METHs USE THE GZIP COMPRESSION

        /**
         * @brief Compresses the buffer with GZip algorithm
         * @param output the other buffer reference where to put the compressed data
         * @param level the level of the GZip compression [0..9]
         * @return true if there are not errors, otherwise false
         */
        bool compress(SkDataBuffer &output, uint8_t level=9);

        /**
         * @brief Decompresses the buffer with UnGZip algorithm
         * @param output the other buffer reference where to put the compressed data
         * @return true if there are not errors, otherwise false
         */
        bool decompress(SkDataBuffer &output);

        /**
         * @brief Compresses the input buffer with GZip algorithm to the output buffer
         * @param input the source buffer reference to compress
         * @param output the target buffer reference where to put the compressed data
         * @param level the level of the GZip compression [0..9]
         * @return true if there are not errors, otherwise false
         */
        static bool compress(SkDataBuffer &input, SkDataBuffer &output, uint8_t level);


        /**
         * @brief Decompresses the input  buffer with UnGZip algorithm to the output buffer
         * @param input the source buffer reference to decompress
         * @param output the other buffer reference where to put the decompressed data
         * @return true if there are not errors, otherwise false
         */
        static bool decompress(SkDataBuffer &input, SkDataBuffer &output);

        /**
         * @brief Checks if the buffer is empty
         * @return true if the buffer is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Gets the buffer stored size
         * @return the buffer stored size
         */
        uint64_t size();

        /**
         * @brief Swaps the buffer content with another buffer
         * @param other the other buffer to swap
         */
        void swap(SkDataBuffer &other);

        /**
         * @brief Clear the buffer, releasing allocated data
         */
        void clear();

        /**
         * @brief Assign-operator from a string
         * @param s the string reference to assign
         * @return a reference to this buffer
         */
        SkDataBuffer &operator =  (SkString &s);

        /**
         * @brief Assign-operator from another buffer
         * @param buf the buffer reference to assign
         * @return a reference to this buffer
         */
        SkDataBuffer &operator =  (SkDataBuffer &buf);

        /**
         * @brief Assign-operator from a ring-buffer
         * @param buf the ring-buffer pointer to assign
         * @return a reference to this buffer
         */
        SkDataBuffer &operator =  (SkRingBuffer *buf);

        /**
         * @brief Append-operator of a string
         * @param s the string reference to append
         * @return a reference to this buffer
         */
        SkDataBuffer &operator << (SkString &s);

        /**
         * @brief Append-operator of another buffer
         * @param buffer the other buffer reference to append
         * @return a reference to this buffer
         */
        SkDataBuffer &operator << (SkDataBuffer &buf);

        /**
         * @brief Append-operator of a ring-buffer
         * @param buf the ring-buffer pointer to append
         * @return a reference to this buffer
         */
        SkDataBuffer &operator << (SkRingBuffer *buf);

        /**
         * @brief Copy-operator to a string
         * @param s the string reference where to make copy
         * @return a reference to this buffer
         */
        SkDataBuffer &operator >> (SkString &s);

        /**
         * @brief Copy-operator to another buffer
         * @param buf the other buffer reference where to make copy
         * @return a reference to this buffer
         */
        SkDataBuffer &operator >> (SkDataBuffer &buf);

        /**
         * @brief Copy-operator to a ring-buffer
         * @param buf the ring-buffer pointer where to make copy
         * @return a reference to this buffer
         */
        SkDataBuffer &operator >> (SkRingBuffer *buf);

        //ON BUFFER-OVERFLOW IT IS PROTECTED AND RETURN '\0' (or 0)
        /**
         * @brief []-operator for read/write on referred value
         * @param index the position to use
         * @return the reference to the reuested value, or 0 ('\0') if the index
         * is not valid
         */
        char &operator [] (uint64_t index);

    private:
        char *buffer;
        char defaultAtPosErr;
        uint64_t sz;

    protected:
        //IT IS TRIGGERED ONLY IF IT HAS bool
        //      notifyLoad=true ON WRITING METHs
        virtual void onDataLoaded(uint64_t &){}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/**
 * @brief Compresses the input raw data with GZip algorithm to the output buffer
 * @param input the raw data source
 * @param size the size of data to compress
 * @param output the target buffer reference where to put the compressed data
 * @param level the level of the GZip compression [0..9]
 * @return true if there are not errors, otherwise false
 */
bool gzip(void *input, uint64_t size, SkDataBuffer &output, uint8_t level=9);

/**
 * @brief Uncompresses the input raw data with UnGZip algorithm to the output buffer
 * @param input the raw data source
 * @param size the size of data to uncompress
 * @param output the target buffer reference where to put the uncompressed data
 * @return true if there are not errors, otherwise false
 */
bool ungzip(void *input, uint64_t size, SkDataBuffer &output);

//TESTING
bool inflate(void *input, uint64_t size, SkDataBuffer &output);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKDATABUFFER_H
