#ifndef SKCPOINTER_H
#define SKCPOINTER_H

#include "abstract/skabstractpointer.h"

//IT USES malloc AND readlloc

template <typename T>
class SPECIALK SkCPointer extends SkAbstractPointer<T>
{
    public:
        SkCPointer(T *data, uint64_t count, bool canDestroyFromHere=false);
        SkCPointer(uint64_t count=0);
        ~SkCPointer();

        bool allocate(uint64_t count);
        bool reallocate(uint64_t count);

        //THIS OBJECT DOES NOT CONTROL FOR OVERFLOW CONDITIONs
        void set(uint64_t index, T &value);
        T &get(uint64_t index);

        const T *data()     override;
        bool destroy()      override;
        void clear()        override;
        bool isEmpty()      override;
        uint64_t count()      override;
        uint64_t size()       override;

        SkCPointer<T> &operator   = (const SkCPointer<T> &source);
        bool           operator  == (const SkCPointer<T> &operand);

        //THIS OBJECT DOES NOT CONTROL FOR OVERFLOW CONDITIONs
        T             &operator  [] (uint64_t index);

    private:
        T *ptr;
        bool canDestroy;
        uint64_t elemSz;
        uint64_t elemsCount;
};

template <typename T>
SkCPointer<T>::SkCPointer(T *data, uint64_t count, bool canDestroyFromHere)
{
    ptr = data;
    elemSz = sizeof(T);
    elemsCount = count;
    canDestroy = canDestroyFromHere;
}

template <typename T>
SkCPointer<T>::SkCPointer(uint64_t count)
{
    ptr = nullptr;
    elemSz = sizeof(T);
    elemsCount = 0;
    canDestroy = true;

    if (count)
        allocate(count);
}

template <typename T>
SkCPointer<T>::~SkCPointer()
{
    if (ptr && canDestroy)
        destroy();
}

template <typename T>
bool SkCPointer<T>::allocate(uint64_t count)
{
    if (ptr)
    {
        FlatError("RawPointer is ALREADY allocated, use 'reallocate' instead");
        return false;
    }

    ptr = static_cast<T *>(malloc(count*elemSz));
    elemsCount = count;
    return (ptr != nullptr);
}

template <typename T>
bool SkCPointer<T>::reallocate(uint64_t count)
{
    ptr = static_cast<T *>(realloc(ptr, count*elemSz));
    elemsCount = count;
    return (ptr != nullptr);
}

template <typename T>
void SkCPointer<T>::set(uint64_t index, T &value)
{
    if (!ptr)
    {
        FlatError("RawPointer is NOT allocated, allocate it first");
        return;
    }

    ptr[index] = value;
}

template <typename T>
T &SkCPointer<T>::get(uint64_t index)
{
    return ptr[index];
}

template <typename T>
const T *SkCPointer<T>::data()
{
    return ptr;
}

template <typename T>
bool SkCPointer<T>::destroy()
{
    if (!ptr)
    {
        FlatError("RawPointer is NOT allocated, allocate it first");
        return false;
    }

    free(ptr);
    ptr = nullptr;
    return true;
}

template <typename T>
SkCPointer<T> &SkCPointer<T>::operator = (const SkCPointer<T> &source)
{
    reallocate(elemsCount);
    memcpy(ptr, source.ptr, elemsCount*elemSz);
    return *this;
}

template <typename T>
void SkCPointer<T>::clear()
{

}

template <typename T>
bool SkCPointer<T>::isEmpty()
{
    return (elemsCount == 0);
}

template <typename T>
uint64_t SkCPointer<T>::count()
{
    return elemsCount;
}

template <typename T>
uint64_t SkCPointer<T>::size()
{
    return elemsCount*elemSz;
}

template <typename T>
bool SkCPointer<T>::operator == (const SkCPointer<T> &operand)
{
    return (elemsCount == operand.elemsCount
            && memcmp(ptr, operand.ptr, elemsCount*elemSz) == 0);
}

template <typename T>
T &SkCPointer<T>::operator [] (uint64_t index)
{
    return get(index);
}

#endif // SKCPOINTER_H
