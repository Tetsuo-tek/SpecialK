#ifndef SKARRAY_H
#define SKARRAY_H

#define SK_DEFAULT_ARRAY_CAPACITY      1

#include "abstract/skabstractarray.h"
#include "Core/sklogmachine.h"

//IT CAN' USE CLASSEs, BUT ONLY PRIMITIVE TYPEs AND POINTERs
//ABOUT STRUCTs IT ALLOWs ONLY C-STRUCTs

//IF USE THIS CLASS WITH NOT ALLOWED TYPEs THE COMPILER DOES NOT NOTIFY ANY ERROR,
//BUT MISTERIOUS THINGs COULD HAPPEN, SO TAKE CARE OF ITS USE-CASEs

//TO ALLOCATE/DEALLOCATE MEMORY IT USES malloc, realloc AND free
//BLOCK ARE MOVED WITH memmove
//MEMORY IS CLEARED WITH bzero

//ITS EFFICIENCY IS GREATER THAN SkVector

template <typename T>
class SPECIALK SkArray extends SkAbstractArray<T>
{
    public:
        SkArray(uint64_t defaultCapacity=SK_DEFAULT_ARRAY_CAPACITY);
        SkArray(const SkArray<T> &source);
        ~SkArray();

        void setCapacity(uint64_t count) override;

        //these meths preserve the position-order by definition
        void append(const T &e) override;
        void append(SkAbstractList<T> &other, uint64_t count=0) override;

        void prepend(const T &e) override;
        void prepend(SkAbstractList<T> &other, uint64_t count=0) override;

        bool insert(const T &e, uint64_t index) override;
        bool insert(SkAbstractList<T> &other, uint64_t index, uint64_t count=0) override;

        bool contains(const T &e) override;
        int64_t indexOf(const T &e, uint64_t from=0) override;

        bool set(const T &e, uint64_t index) override;
        //bool set(const T *array, uint64_t count, uint64_t index) override;
        T &get(uint64_t index) override;
        T &first() override;
        T &last() override;

        //if the preserveOrder(..) is NOT set on true
        //these meths creates disorder between remaining element
        T remove(const T &e) override;
        T removeAt(uint64_t index) override;
        T removeFirst() override;
        T removeLast() override;
        uint64_t remove(SkAbstractList<T> &other) override;
        uint64_t remove(T *array, uint64_t count) override;

        //void sort(SkOrderAlgo algo=SkOrderAlgo::OA_QUICK, Comparator<T> *c=nullptr) override;

        //logic size becomes 0
        //but anything is deallocated until the next remove*(..) will be called
        void clear() override;
        bool isEmpty() override;
        uint64_t count() override;
        uint64_t count(const T &e) override;
        uint64_t size() override;

        uint64_t capacity() override;
        uint64_t capacitySize() override;

        void toArray(T *array) override;
        void toList(SkAbstractList<T> &l) override;
        void swap(SkArray<T> &other);
        T *data() override;

        class SPECIALK SkIterator extends SkAbstractListIterator<T>
        {
            public:
                SkIterator(SkArray<T> &l, int64_t startFrom);

                bool isValid() override;

                bool hasNext() override;
                bool next() override;

                bool hasPrev() override;
                bool prev() override;

                T &item() override;
                int64_t index() override;

                bool set(const T &e) override;
                bool insert(const T &e) override;
                T remove() override;

                void reset() override;

                bool goToBegin() override;
                bool goToEnd() override;

                bool atBegin() override;
                bool atEnd() override;

                SkIterator &operator ++ () override;
                SkIterator &operator -- () override;

            private:
                int64_t currentIndex;
                SkArray<T> *ownerList;
        };

        SkArray<T> &operator =  (const SkArray<T> &source);
        bool        operator == (const SkArray<T> operand);
        T          &operator [] (uint64_t index) override;
        SkArray<T> &operator << (const T &item) override;

        SkIterator *iterator(int64_t startFrom=-1) override;

        T &nv()
        {return nullValue = T();}

    private:
        uint64_t defaultCountCapacity;
        uint64_t maxCountCapacity;
        uint64_t elemsCount;
        T *ptr;
        uint64_t elemSz;
        T nullValue;

    protected:
        void updateCapacity(int delta);

        bool isEqual(const T &a, const T &b);
        void move(const T &src, T &tgt);
        void shiftBlock(uint64_t index, int64_t offset, uint64_t count);
};

//

template <typename T>
SkArray<T>::SkArray(uint64_t defaultCapacity)
{
    defaultCountCapacity = defaultCapacity;

    if (defaultCountCapacity == 0)
        defaultCountCapacity = 2;

    if (defaultCountCapacity % 2 != 0)
        defaultCountCapacity++;

    maxCountCapacity = defaultCountCapacity;

    elemSz = sizeof(T);
    elemsCount = 0;

    ptr = static_cast<T *>(malloc(maxCountCapacity*elemSz));
}

template <typename T>
SkArray<T>::SkArray(const SkArray<T> &source)
{
    defaultCountCapacity = source.defaultCountCapacity;
    maxCountCapacity = source.maxCountCapacity;
    elemsCount = source.elemsCount;
    elemSz = source.elemSz;
    ptr = static_cast<T *>(malloc(maxCountCapacity*elemSz));
    memcpy(ptr, source.ptr, maxCountCapacity*elemSz);
}

template <typename T>
SkArray<T>::~SkArray()
{
    free(ptr);
}

template <typename T>
bool SkArray<T>::isEqual(const T &a, const T &b)
{
    return (memcmp(&a, &b, elemSz) == 0);
}

template <typename T>
void SkArray<T>::move(const T &src, T &tgt)
{
    memcpy(&tgt, &src, elemSz);
}

template <typename T>
void SkArray<T>::shiftBlock(uint64_t index, int64_t offset, uint64_t count)
{
    memmove(&ptr[index+offset], &ptr[index], count*elemSz);
}

template <typename T>
void SkArray<T>::setCapacity(uint64_t count)
{
    if (!count)
    {
        maxCountCapacity = defaultCountCapacity;
        elemsCount = 0;
    }

    else
    {
        if (count % 2 != 0)
            count++;

        maxCountCapacity = count;

        if (elemsCount > count)
            elemsCount = count;
    }

    ptr = static_cast<T *>(realloc(ptr, maxCountCapacity*elemSz));
}

template <typename T>
void SkArray<T>::updateCapacity(int delta)
{
    uint64_t currentCapacity = maxCountCapacity;
    uint64_t newCount = elemsCount + delta;
    //cout << "* check: " << newCount << " = " << elemsCount << " + (" << delta << ") [" << maxCountCapacity << " / " << (maxCountCapacity >> 1) << "]\n";

    if (delta > 0 && newCount > (maxCountCapacity >> 1))
        (maxCountCapacity <<= 1)+=newCount; // *= 2

    //NOT happens when we call clear() until some remove action is performed
    else if (delta < 0 && newCount <= (maxCountCapacity >> 3))// /4)
        maxCountCapacity >>= 1; // /= 2

    else
    {
        //cout << "! capacity NOT changed: " << maxCountCapacity << " - " << currentCapacity << "\n";
        return;
    }

    ptr = static_cast<T *>(realloc(ptr, maxCountCapacity*elemSz));
    FlatPlusDebug("* capacity changed: " << currentCapacity << "->" << maxCountCapacity << " (" << maxCountCapacity*elemSz << " B)");
}

template <typename T>
void SkArray<T>::append(const T &e)
{
    updateCapacity(+1);
    ptr[elemsCount++] = e;
}

template <typename T>
void SkArray<T>::append(SkAbstractList<T> &other, uint64_t count)
{
    if (other.isEmpty() || count >= other.count())
        return;

    if (count == 0)
        count = other.count();

    if (count == 1)
    {
        append(other.first());
        return;
    }

    updateCapacity(+count);

    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t i=elemsCount;
    uint64_t limit = elemsCount + count;

    while(itr->next() && i < limit)
    {
        //cout << i << " " << itr->item() << " " << elemsCount << " " << maxCountCapacity << "\n";
        move(itr->item(), ptr[i++]);
    }

    delete itr;
    elemsCount += count;
}

template <typename T>
void SkArray<T>::prepend(const T &e)
{
    updateCapacity(+1);
    shiftBlock(0, +1, elemsCount);
    move(e, ptr[0]);
    elemsCount++;
}

template <typename T>
void SkArray<T>::prepend(SkAbstractList<T> &other, uint64_t count)
{
    if (isEmpty())
        append(other, count);

    insert(other, 0, count);
}

template <typename T>
bool SkArray<T>::insert(const T &e, uint64_t index)
{
    if (index >= elemsCount)
        return false;

    updateCapacity(+1);
    shiftBlock(index, +1, elemsCount-index);
    move(e, ptr[index]);
    elemsCount++;
    return true;
}

template <typename T>
bool SkArray<T>::insert(SkAbstractList<T> &other, uint64_t index, uint64_t count)
{
    if (index >= elemsCount || other.isEmpty() || count >= other.count())
        return false;

    if (count == 0)
        count = other.count();

    if (count == 1)
        return insert(other.first(), index);

    updateCapacity(+count);
    shiftBlock(index, +count, elemsCount-index);

    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t i = index;
    uint64_t limit = index + count;

    while(itr->next() && i < limit)
    {
        move(itr->item(), ptr[i++]);
        //cout << i << " " << itr->item() << " " << ptr[i-1]  << "\n";
    }

    delete itr;
    elemsCount += count;

    return true;
}

template <typename T>
bool SkArray<T>::contains(const T &e)
{
    return (indexOf(e) > -1);
}

template <typename T>
int64_t SkArray<T>::indexOf(const T &e, uint64_t from)
{
    for(uint64_t i=from; i<elemsCount; i++)
        if (isEqual(e, ptr[i]))
            return static_cast<int64_t>(i);

    return -1;
}

template <typename T>
bool SkArray<T>::set(const T &e, uint64_t index)
{
    if (index >= elemsCount)
        return false;

    move(e, ptr[index]);
    return true;
}

/*template <typename T>
bool SkArray<T>::set(const T *array, uint64_t count, uint64_t index)
{
    if (index >= elemsCount || index + count >= elemsCount)
        return false;

    if (array >= ptr && array+count<=ptr+count)
        memmove(&ptr[index], array, count*elemSz);
    else
        memcpy(&ptr[index], array, count*elemSz);

    return true;
}*/

template <typename T>
T &SkArray<T>::get(uint64_t index)
{
    if (index >= elemsCount)
        return nv();

    return ptr[index];
}

template <typename T>
T &SkArray<T>::first()
{
    if (!elemsCount)
        return nv();

    return ptr[0];
}

template <typename T>
T &SkArray<T>::last()
{
    if (!elemsCount)
        return nv();

    return ptr[elemsCount-1];
}

template <typename T>
T SkArray<T>::remove(const T &e)
{
    T removingElement;

    for(uint64_t i=0; i<elemsCount; i++)
    {
        if (isEqual(ptr[i], e))
        {
            removingElement = removeAt(i);
            break;
        }
    }

    return removingElement;
}

template <typename T>
T SkArray<T>::removeAt(uint64_t index)
{
    if (index >= elemsCount)
        return nv();

    T removingElement;
    move(ptr[index], removingElement);
    shiftBlock(index+1, -1, elemsCount-(index+1));
    updateCapacity(-1);
    elemsCount--;
    return removingElement;
}

template <typename T>
T SkArray<T>::removeFirst()
{
    if (!elemsCount)
        return nv();

    return removeAt(0);
}

template <typename T>
T SkArray<T>::removeLast()
{
    if (!elemsCount)
        return nv();

    return removeAt(elemsCount-1);
}

template <typename T>
uint64_t SkArray<T>::remove(SkAbstractList<T> &other)
{
    SkAbstractIterator<T> *itr = other.iterator();
    uint64_t removed = 0;
    uint64_t c = elemsCount;

    while(itr->next())
    {
        T &e = itr->item();

        for(uint64_t i=0; i<c; i++)
            if (isEqual(e, ptr[i]))
            {
                shiftBlock(i+1, -1, c-(i+1));
                c--;
                removed++;
                break;
            }
    }

    delete itr;
    elemsCount = c;
    updateCapacity(-removed);
    return removed;
}

template <typename T>
uint64_t SkArray<T>::remove(T *array, uint64_t count)
{
    uint64_t removed = 0;
    uint64_t c = elemsCount;

    for(uint64_t z=0; z<count/* && c>=0*/; z++)
    {
        const T &e = array[z];

        for(uint64_t i=0; i<elemsCount; i++)
            if (isEqual(e, ptr[i]))
            {
                shiftBlock(i+1, i, c-(i+1));
                c--;
                removed++;
                break;
            }
    }

    elemsCount = c;
    updateCapacity(-removed);
    return removed;
}

/*template <typename T>
void SkArray<T>::sort(SkOrderAlgo algo, Comparator<T> *c)
{
    SkSort<T> s;
    s.setup(algo, c);
    s.sort(ptr, elemsCount);
}*/

template <typename T>
void SkArray<T>::clear()
{
    elemsCount = 0;
}

template <typename T>
bool SkArray<T>::isEmpty()
{
    return (elemsCount == 0);
}

template <typename T>
uint64_t SkArray<T>::count()
{
    return elemsCount;
}

template <typename T>
uint64_t SkArray<T>::count(const T &e)
{
    SkIterator *itr = iterator();
    uint64_t i=0;

    while(itr->next())
        if (isEqual(e, itr->item()))//(itr->item() == e)
            i++;

    delete itr;
    return i;
}

template <typename T>
uint64_t SkArray<T>::size()
{
    return elemsCount*elemSz;
}

template <typename T>
uint64_t SkArray<T>::capacity()
{
    return maxCountCapacity;
}

template <typename T>
uint64_t SkArray<T>::capacitySize()
{
    return maxCountCapacity*elemSz;
}

template <typename T>
void SkArray<T>::toArray(T *array)
{
    memcpy(array, ptr, elemsCount*elemSz);
}

template <typename T>
void SkArray<T>::toList(SkAbstractList<T> &l)
{
    for(uint64_t i=0; i<elemsCount; i++)
        l.append(ptr[i]);
}

template <typename T>
void SkArray<T>::swap(SkArray<T> &other)
{
    uint64_t defaultCountCapacity = this->defaultCountCapacity;
    uint64_t maxCountCapacity = this->maxCountCapacity;
    uint64_t elemsCount = this->elemsCount;
    T *ptr = this->ptr;

    this->defaultCountCapacity = other.defaultCountCapacity;
    this->maxCountCapacity = other.maxCountCapacity;
    this->elemsCount = other.elemsCount;
    this->ptr = other.ptr;

    other.defaultCountCapacity = defaultCountCapacity;
    other.maxCountCapacity = maxCountCapacity;
    other.elemsCount = elemsCount;
    other.ptr = ptr;
}

template <typename T>
T *SkArray<T>::data()
{
    return ptr;
}

template <typename T>
SkArray<T> &SkArray<T>::operator = (const SkArray<T> &source)
{
    defaultCountCapacity = source.defaultCountCapacity;
    maxCountCapacity = source.maxCountCapacity;
    elemsCount = source.elemsCount;
    ptr = static_cast<T *>(realloc(ptr, maxCountCapacity*elemSz));
    memcpy(ptr, source.ptr, maxCountCapacity*elemSz);
    return *this;
}

template <typename T>
bool SkArray<T>::operator == (const SkArray<T> operand)
{
    return (elemsCount == operand.elemsCount
            && memcmp(ptr, operand.ptr, elemsCount*elemSz) == 0);
}

template <typename T>
T &SkArray<T>::operator [] (uint64_t index)
{
    return get(index);
}

template <typename T>
SkArray<T> &SkArray<T>::operator << (const T &item)
{
    append(item);
    return *this;
}

template <typename T>
typename SkArray<T>::SkIterator *SkArray<T>::iterator(int64_t startFrom)
{
    return new SkIterator(*this, startFrom);
}

// ITERATOR

template <typename T>
SkArray<T>::SkIterator::SkIterator(SkArray<T> &l, int64_t startFrom)
{
    if (startFrom >= 0 && l.elemsCount && startFrom < static_cast<int64_t>(l.elemsCount))
        currentIndex = startFrom;
    else
        currentIndex = -1;

    ownerList = &l;
}

template <typename T>
bool SkArray<T>::SkIterator::isValid()
{
    return currentIndex != -1;
}

template <typename T>
bool SkArray<T>::SkIterator::hasNext()
{
    return (/*(currentIndex >= -1) &&*/ (currentIndex < static_cast<int64_t>(ownerList->elemsCount-1)));
}

template <typename T>
bool SkArray<T>::SkIterator::next()
{
    if (!hasNext())
        return false;

    currentIndex++;
    return true;
}

template <typename T>
bool SkArray<T>::SkIterator::hasPrev()
{
    return ((currentIndex > 0) && (currentIndex < static_cast<int64_t>(ownerList->elemsCount)));
}

template <typename T>
bool SkArray<T>::SkIterator::prev()
{
    if (!hasPrev())
        return false;

    currentIndex--;
    return true;
}

template <typename T>
T &SkArray<T>::SkIterator::item()
{
    if (!isValid())
        return ownerList->nv();

    return ownerList->ptr[currentIndex];
}

template <typename T>
int64_t SkArray<T>::SkIterator::index()
{
    return currentIndex;
}

template <typename T>
bool SkArray<T>::SkIterator::set(const T &e)
{
    if (!isValid())
        return false;

    ownerList->ptr[currentIndex] = e;
    return true;
}

template <typename T>
bool SkArray<T>::SkIterator::insert(const T &e)
{
    if (!isValid())
    {
        ownerList->append(e);
        currentIndex = 0;
        return true;
    }

    return ownerList->insert(e, currentIndex);
}

template <typename T>
T SkArray<T>::SkIterator::remove()
{
    if (!isValid())
        return ownerList->nv();

    T removing = ownerList->ptr[currentIndex];
    ownerList->removeAt(static_cast<uint64_t>(currentIndex));
    currentIndex--;

    return removing;
}

template <typename T>
void SkArray<T>::SkIterator::reset()
{
    currentIndex = -1;
}

template <typename T>
bool SkArray<T>::SkIterator::goToBegin()
{
    if (!isValid())
        return false;

    currentIndex = 0;
    return true;
}

template <typename T>
bool SkArray<T>::SkIterator:: goToEnd()
{
    if (!isValid())
        return false;

    currentIndex = static_cast<int64_t>(ownerList->elemsCount)-1;
    return true;
}

template <typename T>
bool SkArray<T>::SkIterator::atBegin()
{
    return currentIndex == 0;
}

template <typename T>
bool SkArray<T>::SkIterator::atEnd()
{
    return currentIndex == static_cast<int64_t>(ownerList->elemsCount)-1;
}

template <typename T>
typename SkArray<T>::SkIterator &SkArray<T>::SkIterator::operator ++ ()
{
    next();
    return *this;
}

template <typename T>
typename SkArray<T>::SkIterator &SkArray<T>::SkIterator::operator -- ()
{
    prev();
    return *this;
}

#endif // SKARRAY_H
