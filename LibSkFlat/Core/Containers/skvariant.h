/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKVARIANT_H
#define SKVARIANT_H

#include "skstring.h"
#include "skstack.h"
#include "skqueue.h"
#include "abstract/skabstractlist.h"
#include "skdatabuffer.h"
#include "skcpointer.h"
#include "skmap.h"

class SkRingBuffer;
class SkArgsMap;
class SkStringList;

//SkVariant_T::T_POINTER IS THE ONLY VALUE TYPE THAT IS NOT BE MANAGED FROM THIS
//CLASS; IT MEANS THAT, ON RESET (OR DESTROY), THE STORED POINTER WILL NOT BE
//RELEASED

typedef struct
{
    SkVariant_T t;
    uint64_t sz;
    void *valData;
    SkString originalRealType;
} SkVariantData;

class SPECIALK SkVariant extends SkFlatObject
{
    public:
        /**
         * @brief Simple constructor
         */
        SkVariant();

        /**
         * @brief Constructs the variant from a bool value
         * @param v the value to copy inside
         */
        SkVariant(bool v);

        /**
         * @brief Constructs the variant from a char value
         * @param v the value to copy inside
         */
        //SkVariant(char v);

        /**
         * @brief Constructs the variant from a int8_t value
         * @param v the value to copy inside
         */
        SkVariant(int8_t v);

        /**
         * @brief Constructs the variant from a uint8_t value
         * @param v the value to copy inside
         */
        SkVariant(uint8_t v);

        /**
         * @brief Constructs the variant from a int16_t value
         * @param v the value to copy inside
         */
        SkVariant(int16_t v);

        /**
         * @brief Constructs the variant from a uint16_t value
         * @param v the value to copy inside
         */
        SkVariant(uint16_t v);

        /**
         * @brief Constructs the variant from a int32_t value
         * @param v the value to copy inside
         */
        SkVariant(int32_t v);

        /**
         * @brief Constructs the variant from a uint32_t value
         * @param v the value to copy inside
         */
        SkVariant(uint32_t v);

        /**
         * @brief Constructs the variant from a int64_t value
         * @param v the value to copy inside
         */
        SkVariant(int64_t v);

        /**
         * @brief Constructs the variant from a uint64_t value
         * @param v the value to copy inside
         */
        SkVariant(uint64_t v);

        /**
         * @brief Constructs the variant from a float value
         * @param v the value to copy inside
         */
        SkVariant(float v);

        /**
         * @brief Constructs the variant from a double value
         * @param v the value to copy inside
         */
        SkVariant(double v);

        /**
         * @brief Constructs the variant from a copy of a raw array pointer
         * @param array the raw array pointer to copy inside
         */
        SkVariant(void *ptr, uint64_t size);

        /**
         * @brief Constructs the variant from a copy of a const raw array pointer
         * @param array the const raw array pointer to copy inside
         */
        SkVariant(CVoid *ptr, uint64_t size);

        /**
         * @brief Constructs the variant from a raw internal data
         */
        SkVariant(CVoid *ptr, uint64_t size, SkVariant_T t);

        /**
         * @brief Constructs the variant from a copy of a cstring;
         * it will consider th '\0' terminator
         * @param CStr *the string to copy inside
         */
        SkVariant(CStr *cstr);

        /**
         * @brief Constructs the variant from a copy of a string;
         * it will consider th '\0' terminator
         * @param s the string to copy inside
         */
        SkVariant(SkString &s);

        /**
         * @brief Constructs the variant from a copy of a string-list
         * @param l the string-list to copy inside
         */
        SkVariant(SkStringList &l);

        /**
         * @brief Constructs the variant from a copy of a variant-stack
         * @param l the variant-stack to copy inside
         */
        SkVariant(SkStack<SkVariant> &s);

        /**
         * @brief Constructs the variant from a copy of a variant-queue
         * @param l the variant-queue to copy inside
         */
        SkVariant(SkQueue<SkVariant> &q);

        /**
         * @brief Constructs the variant from a copy of a variant-list/vector
         * @param l the variant-list to copy inside
         */
        SkVariant(SkAbstractList<SkVariant> &l);

        /**
         * @brief Constructs the variant from a copy of a variant-map/linkedMap
         * @param m the variant-map to copy inside
         */
        SkVariant(SkAbstractMap<SkString, SkVariant> &m);

        /**
         * @brief Constructs the variant from a copy of a variant-pair
         * @param m the variant-pair to copy inside
         */
        SkVariant(SkPair<SkVariant, SkVariant> &p);

        /**
         * @brief Constructs the variant from a copy of a variant-map/linkedMap
         * @param m the variant-map to copy inside
         */
        SkVariant(SkAbstractMap<SkVariant, SkVariant> &m);

        /**
         * @brief Constructs the variant from a raw pointer; it will not be managed (deleted) from here
         * @param ptr the raw pointer
         */
        SkVariant(SkFlatObject &o);

        /**
         * @brief Constructs the variant from a raw pointer; it will not be managed (deleted) from here
         * @param ptr the raw pointer
         */
        SkVariant(void *ptr);

        /**
         * @brief Constructs the variant from another constant variant copying it
         * @param v the other constant variant to copy
         */
        SkVariant(const SkVariant &v);

        /**
         * @brief Constructs the variant from a copy of another internal variant-data
         * @param raw the other internal variant-data
         */
        SkVariant(SkVariantData &raw);

        /**
         * @brief Destructor
        */
        ~SkVariant();

        /**
         * @brief Checks if the type is T_NULL
         * @param t the type to check
         * @return true if the type is T_NULL, otherwise false
         */
        static bool isNull(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_BOOL
         * @param t the type to check
         * @return true if the type is T_BOOL, otherwise false
         */
        static bool isBoolean(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_INT8
         * @param t the type to check
         * @return true if the type is T_INT8, otherwise false
         */
        static bool isChar(const SkVariant_T t);

        /**
         * @brief Checks if the type is an integer or a float number
         * @param t the type to check
         * @return true if the type is an integer or a float number, otherwise false
         */
        static bool isNumber(const SkVariant_T t);

        /**
         * @brief Checks if the type is an integer number
         * @param t the type to check
         * @return true if the type is an integer number, otherwise false
         */
        static bool isInteger(const SkVariant_T t);

        /**
         * @brief Checks if the type is a float number
         * @param t the type to check
         * @return true if the type is an float number, otherwise false
         */
        static bool isReal(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_BYTEARRAY
         * @param t the type to check
         * @return true if the type is T_BYTEARRAY, otherwise false
         */
        static bool isByteArray(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_STRING
         * @param t the type to check
         * @return true if the type is T_STRING, otherwise false
         */
        static bool isString(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_LIST
         * @param t the type to check
         * @return true if the type is T_LIST, otherwise false
         */
        static bool isList(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_PAIR
         * @param t the type to check
         * @return true if the type is T_PAIR, otherwise false
         */
        static bool isPair(const SkVariant_T t);
        /**
         * @brief Checks if the type is T_MAP
         * @param t the type to check
         * @return true if the type is T_MAP, otherwise false
         */
        static bool isMap(const SkVariant_T t);

        /**
         * @brief Checks if the type is T_RAWPOINTER
         * @param t the type to check
         * @return true if the type is T_RAWPOINTER, otherwise false
         */
        static bool isRawPointer(const SkVariant_T t);

        //NULL MEANS WITHOUT TYPE
        //IF THIS VAL IS NULL IT IS SURELY EMPTY
        /**
         * @brief Checks if the type of this variant is T_NULL
         * @return true if the type is T_NULL, otherwise false
         */
        bool isNull();

        //EMPTY MEANS WITH DATA-SIZE = 0
        //IF THIS VAL IS EMPTY IT COULD BE NULL OR NOT-NULL (isNull() == false)
        /**
         * @brief Checks if this variant is empty (size == 0)
         * @return true if this variant is empty, otherwise false
         */
        bool isEmpty();

        /**
         * @brief Checks if the type of this variant is T_BOOL
         * @return true if the type is T_BOOL, otherwise false
         */
        bool isBoolean();

        /**
         * @brief Checks if the type of this variant is T_INT8
         * @return true if the type is T_INT8, otherwise false
         */
        bool isChar();

        /**
         * @brief Checks if the type of this variant is an integer or a float number
         * @return true if the type of this variant is an integer or a float number, otherwise false
         */
        bool isNumber();

        /**
         * @brief Checks if the type of this variant is an integer number
         * @return true if the type of this variant is an integer number, otherwise false
         */
        bool isInteger();

        /**
         * @brief Checks if the type of this variant is a float number
         * @return true if the type of this variant is a float number, otherwise false
         */
        bool isReal();

        /**
         * @brief Checks if the type of this variant is T_BYTEARRAY
         * @return true if the type of this variant is T_BYTEARRAY, otherwise false
         */
        bool isByteArray();

        /**
         * @brief Checks if the type of this variant is T_STRING
         * @return true if the type of this variant is T_STRING, otherwise false
         */
        bool isString();

        /**
         * @brief Checks if the type of this variant is T_STRINGSLIST
         * @return true if the type of this variant is T_STRINGSLIST, otherwise false
         */
        bool isStringList();

        /**
         * @brief Checks if the type of this variant is T_VARIANTSTACK
         * @return true if the type of this variant is T_VARIANTSTACK, otherwise false
         */
        bool isStack();
        /**

         * @brief Checks if the type of this variant is T_VARIANTQUEUE
         * @return true if the type of this variant is T_VARIANTQUEUE, otherwise false
         */
        bool isQueue();

        /**
         * @brief Checks if the type of this variant is T_LIST
         * @return true if the type of this variant is T_LIST, otherwise false
         */
        bool isList();

        /**
         * @brief Checks if the type of this variant is T_LIST
         * @return true if the type of this variant is T_LIST, otherwise false
         */
        bool isPair();

        /**
         * @brief Checks if the type of this variant is T_MAP
         * @return true if the type of this variant is T_MAP, otherwise false
         */
        bool isMap();

        /**
         * @brief Checks if the type of this variant is T_RAWPOINTER
         * @return true if the type of this variant is T_RAWPOINTER, otherwise false
         */
        bool isRawPointer();

        /**
         * @brief Checks if this variant is convertible to another type
         * @param targetType the target of the conversion
         * @return true if this variant is convertible
         */
        bool canConvertTo(const SkVariant_T targetType);

        /**
         * @brief Checks if this variant is convertible to another type
         * @param targetType the constant target of the conversion
         * @return true if this variant is convertible
         */
        bool canConvertTo(SkVariant &target);

        /**
         * @brief Gets the cstring name of a variant type
         * @param t the variant type
         * @return the name of the variant type
         */
        static CStr *variantTypeName(SkVariant_T t);

        static SkVariant_T variantTypeFromName(CStr * t);

        /**
         * @brief Gets the type of this variant
         * @return the variant type
         */
        SkVariant_T variantType();

        /**
         * @brief  Gets the cstring name of this variant type
         * @return the name of this variant type
         */
        CStr *variantTypeName();

        /**
         * @brief Gets this variant stored bytes size
         * @return the size
         */
        uint64_t size();

        /**
         * @brief The internal stored data pointer for this variant
         * @return internal stored data pointer
         */
        CStr *data();

        /**
         * @brief Sets this variant from a bool value
         * @param v the value to copy inside
         */
        void setVal(bool v);

        /**
         * @brief Sets this variant from a char value
         * @param v the value to copy inside
         */
        //void setVal(char v);

        /**
         * @brief Sets this variant from a int8_t value
         * @param v the value to copy inside
         */
        void setVal(int8_t v);

        /**
         * @brief Sets this variant from a uint8_t value
         * @param v the value to copy inside
         */
        void setVal(uint8_t v);

        /**
         * @brief Sets this variant from a int16_t value
         * @param v the value to copy inside
         */
        void setVal(int16_t v);

        /**
         * @brief Sets this variant from a uint16_t value
         * @param v the value to copy inside
         */
        void setVal(uint16_t v);

        /**
         * @brief Sets this variant from a int32_t value
         * @param v the value to copy inside
         */
        void setVal(int32_t v);

        /**
         * @brief Sets this variant from a uint32_t value
         * @param v the value to copy inside
         */
        void setVal(uint32_t v);

        /**
         * @brief Sets this variant from a int64_t value
         * @param v the value to copy inside
         */
        void setVal(int64_t v);

        /**
         * @brief Sets this variant from a uint64_t value
         * @param v the value to copy inside
         */
        void setVal(uint64_t v);

        /**
         * @brief Sets this variant from a float value
         * @param v the value to copy inside
         */
        void setVal(float v);

        /**
         * @brief Sets this variant from a double value
         * @param v the value to copy inside
         */
        void setVal(double v);

        /**
         * @brief Sets this the variant from a copy of a raw array pointer
         * @param array the raw array pointer to copy inside
         */
        void setVal(void *ptr, uint64_t size);

        void setVal(CVoid *ptr, uint64_t size, SkVariant_T t);

        /**
         * @brief Sets this the variant from a copy of const a raw array pointer
         * @param array the const raw array pointer to copy inside
         */
        void setVal(CVoid *ptr, uint64_t size);

        /**
         * @brief Sets this variant from a copy of a cstring; it will consider th '\0' terminator
         * @param CStr *the string to copy inside
         */
        void setVal(CStr *cstr);

        /**
         * @brief Sets this variant from a copy of a string; it will consider th '\0' terminator
         * @param s the string to copy inside
         */
        void setVal(SkString &s);

        /**
         * @brief Sets this variant from a copy of a string-list
         * @param l the string-list to copy inside
         */
        void setVal(SkStringList &l);

        /**
         * @brief Sets this variant from a copy of a variant-stack/vector
         * @param l the variant-stack to copy inside
         */
        void setVal(SkStack<SkVariant> &s);

        /**
         * @brief Sets this variant from a copy of a variant-queue/vector
         * @param l the variant-queue to copy inside
         */
        void setVal(SkQueue<SkVariant> &q);

        /**
         * @brief Sets this variant from a copy of a variant-list/vector
         * @param l the variant-list to copy inside
         */
        void setVal(SkAbstractList<SkVariant> &l);

        /**
         * @brief Sets this variant from a copy of a variant-map/linkedMap
         * @param m the variant-map to copy inside
         */
        void setVal(SkPair<SkVariant, SkVariant> &p);

        /**
         * @brief Sets this variant from a copy of a variant-map/linkedMap
         * @param m the variant-map to copy inside
         */
        void setVal(SkAbstractMap<SkVariant, SkVariant> &m);

        /**
         * @brief Sets this variant from a copy of a variant-map/linkedMap
         * @param m the variant-map to copy inside
         */
        void setVal(SkAbstractMap<SkString, SkVariant> &m);

        /**
         * @brief Sets this variant from another constant variant copying it
         * @param v the other constant variant to copy
         */
        void setVal(const SkVariant &other);

        /**
         * @brief Sets this variant from a copy of another internal variant-data
         * @param raw the other internal variant-data
         */
        void setVal(SkVariantData &raw);

        /**
         * @brief Sets this variant from a raw pointer; it will not be managed (deleted) from here
         * @param ptr the raw pointer
         */
        void setVal(void *ptr);

        void setOriginalRealType(CStr *realType);
        CStr *getOriginalRealType();

        /**
         * @brief Swaps the contents between this variant and another one
         * @param other the other swapping container
         */
        void swap(SkVariant &other);

        //IT IS A RESET
        /**
         * @brief Sets this variant to T_NULL clearing it
         */
        void nullify();
        void clear();

        /**
         * @brief Tries to cast the internal stored value to a bool type;
         * @return the bool value
         */
        bool toBool();

        /**
         * @brief Tries to cast the internal stored value to a char type;
         * @return the char value
         */
        char toChar();

        /**
         * @brief Tries to cast the internal stored value to an int type;
         * @return the int value
         */
        int toInt();

        /**
         * @brief Tries to cast the internal stored value to an unsigned int type;
         * @return the int value
         */
        uint toUInt();

        /**
         * @brief Tries to cast the internal stored value to an int8_t type;
         * @return the int8_t value
         */
        int8_t toInt8();

        /**
         * @brief Tries to cast the internal stored value to an uint8_t type;
         * @return the uint8_t value
         */
        uint8_t toUInt8();

        /**
         * @brief Tries to cast the internal stored value to an int16_t type;
         * @return the int16_t value
         */
        int16_t toInt16();

        /**
         * @brief Tries to cast the internal stored value to an uint16_t type;
         * @return the uint16_t value
         */
        uint16_t toUInt16();

        /**
         * @brief Tries to cast the internal stored value to an int32_t type;
         * @return the int32_t value
         */
        int32_t toInt32();

        /**
         * @brief Tries to cast the internal stored value to an uint32_t type;
         * @return the uint32_t value
         */
        uint32_t toUInt32();

        /**
         * @brief Tries to cast the internal stored value to an int64_t type;
         * @return the int64_t value
         */
        int64_t toInt64();

        /**
         * @brief Tries to cast the internal stored value to an uint64_t type;
         * @return the uint64_t value
         */
        uint64_t toUInt64();

        /**
         * @brief Tries to cast the internal stored value to a float type;
         * @return the float value
         */
        float toFloat();

        /**
         * @brief Tries to cast the internal stored value to a double type;
         * @return the double value
         */
        double toDouble();

        /**
         * @brief Converts the value to a string
         * @return the string representation of value
         */
        SkString toString();

        void *(*toCustom)(SkVariant *);
        SkString (*toCustomString)(SkVariant *);

        //USE WITH CARE
        /**
         * @brief Gets the internal raw pointer; use it with care
         * @return the internal raw pointer
         */
        void *toVoid();

        SkStringList toStringList();
        SkStack<SkVariant> toStack();
        SkQueue<SkVariant> toQueue();
        SkList<SkVariant> toList();
        SkMap<SkString, SkVariant> toMap();
        SkMap<SkVariant, SkVariant> toVariantMap();

        SkString &toStringRef();
        //if stringRef is created and modified from extern with toStringRef()
        //this meth will update the variant content with toStringRef-data
        bool updateOnFromStringRef();

        /**
         * @brief Fills the string with its contents
         * @param l the filling string-list; if this variant is not a T_STRING
         * it will be add its string representation
         */
        void copyToString(SkString &str);

        /**
         * @brief Fills the string-list with its contents
         * @param l the filling string-list; if this variant is not a T_STRINGLIST
         * it will be add only one item to the list, with its string representation
         */
        void copyToStringList(SkStringList &l);

        /**
         * @brief Fills the buffer with its contents
         * @param l the filling string-list; if this variant is not a T_BYTEARRAY
         * it will be add only one item to the list, with its string representation
         */
        void copyToBuffer(SkDataBuffer &b);

        /**
         * @brief Fills the variant-stack/vector with its contents
         * @param l the filling variant-stack; if this variant is not a T_VARIANTSTACK
         * it will be add only one item to the list
         */
        void copyToStack(SkStack<SkVariant> &s);

        /**
         * @brief Fills the variant-queue/vector with its contents
         * @param l the filling variant-queue; if this variant is not a T_VARIANTQUEUE
         * it will be add only one item to the list
         */
        void copyToQueue(SkQueue<SkVariant> &q);

        /**
         * @brief Fills the variant-list/vector with its contents
         * @param l the filling variant-list; if this variant is not a T_LIST
         * it will be add only one item to the list
         */
        void copyToList(SkAbstractList<SkVariant> &l);

        /**
         * @brief Fills the variant-pair with its contents
         * @param l the filling variant-pair; if this variant is not a T_PAIR
         * it will not add anything
         */
        void copyToPair(SkPair<SkVariant, SkVariant> &p);

        /**
         * @brief Fills the variant-map/linkedMap with its contents
         * @param l the filling variant-map; if this variant is not a T_MAP
         * it will not add anything
         */
        void copyToMap(SkAbstractMap<SkString, SkVariant> &m);

        /**
         * @brief Fills the variant-map/linkedMap with its contents
         * @param l the filling variant-map; if this variant is not a T_MAP
         * it will not add anything
         */
        void copyToVariantMap(SkAbstractMap<SkVariant, SkVariant> &m);

        //WE CAN CREATE JSON ONLY FROM CONTAINERS:
        //  isList() OR isMap() OR isByteArray()
        //OTHERWISE IT WILL NOT CREATE ANYTHING
        /**
         * @brief Fills the string with a JSON document from the stored value;
         * it could be possible only if the stored type is T_MAP,
         * T_LIST or T_BYTEARRAY
         * @param json the string to fill with document
         * @param indented permits to write indented document
         * @param spaces the number of spaces used for the indentation
         * @return true if the document is created, otherwise false
         */
        bool toJson(SkString &json, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Converts the JSON document string to a variant
         * @param json the JSON document string
         * @return true if there are not errors, otherwise false
         */
        bool fromJson(CStr *json);

        /**
         * @brief Writes the content of this variant to a ring-buffer, saving
         * the type (as uint8_t), the size (as uint32_t) and finally the data
         * @param buf the ring-buffer pointer where to save the variant data
         */
        void toData(SkRingBuffer *buf);

        /**
         * @brief Writes the content of a variant to a ring-buffer, saving
         * the type (as uint8_t), the size (as uint32_t) and finally the data
         * @param buf the ring-buffer pointer where to save the variant data
         * @param v the variant reference to save
         */
        static void toData(SkRingBuffer *buf, SkVariant &v);

        /**
         * @brief Restores the content of this variant from a ring-buffer, reading
         * the type (as uint8_t), the size (as uint32_t) and finally the data
         * @param buf the ring-buffer pointer to read from the variant data
         */
        void fromData(SkRingBuffer *buf);

        /**
         * @brief Restores the content of a variant from a ring-buffer, reading
         * the type (as uint8_t), the size (as uint32_t) and finally the data
         * @param buf the ring-buffer pointer to read from the variant data
         * @param v the variant reference to restore
         */
        static void fromData(SkRingBuffer *buf, SkVariant &v);

        //RETRIEVING REAL-TYPE VAL IS MORE EFFICIENT BECAUSE IT WILL NOT COPY
        //ANYTHING
        //IT IS ALSO POSSIBLE, FOR THESE TYPES, CHANGE THE VALUE DIRECTLY BY THE
        //RETURNED VALUE REF
        //BUT, RETRIEVING VALUEs FROM REAL-FUNCTs ON TYPEs NOT CASTABLE COULD
        //MAKE STRANGE AND MISTERIOUS THINGs

        /**
         * @brief Gets the internal bool value reference; if the internal size
         * corresponds to the sizeof of the requested value, it will results as normal cast;
         * if the internal size is greater than the sizeof of the requested value,
         * if will result as simple truncation (LE)
         * @return the bool value reference
         */
        bool &realBool();

        /**
         * @brief Gets the internal char value reference; if the internal size
         * corresponds to the sizeof of the requested value, it will results as normal cast;
         * if the internal size is greater than the sizeof of the requested value,
         * if will result as simple truncation (LE)
         * @return the char value reference
         */
        char &realChar();

        /**
        * @brief Gets the internal int8_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the int8_t value reference
        */
        int8_t &realInt8();

        /**
        * @brief Gets the internal uint8_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the uint8_t value reference
        */
        uint8_t &realUInt8();

        /**
        * @brief Gets the internal int16_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the int16_t value reference
        */
        int16_t &realInt16();

        /**
        * @brief Gets the internal uint16_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the uint16_t value reference
        */
        uint16_t &realUInt16();

        /**
        * @brief Gets the internal int32_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the int32_t value reference
        */
        int32_t &realInt32();

        /**
        * @brief Gets the internal uint32_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the uint32_t value reference
        */
        uint32_t &realUInt32();

        /**
        * @brief Gets the internal int64_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the int64_t value reference
        */
        int64_t &realInt64();

        /**
        * @brief Gets the internal uint64_t value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the uint64_t value reference
        */
        uint64_t &realUInt64();

        /**
        * @brief Gets the internal float value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the float value reference
        */
        float &realFloat();

        /**
        * @brief Gets the internal double value reference; if the internal size
        * corresponds to the sizeof of the requested value, it will results as normal cast;
        * if the internal size is greater than the sizeof of the requested value,
        * if will result as simple truncation (LE); if the internal size is less than
        * the sizeof of the requested value, something of strange and unpredicable
        * could happen
        * @return the double value reference
        */
        double &realDouble();

        /**
         * @brief Assign-operator from another variant
         * @param source the other variant to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (const SkVariant &source);

        /**
         * @brief Assign-operator from a bool value
         * @param v the bool to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (bool v);

        /**
         * @brief Assign-operator from a int8_t value
         * @param v the int8_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (int8_t v);

        /**
         * @brief Assign-operator from a uint8_t value
         * @param v the uint8_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (uint8_t v);

        /**
         * @brief Assign-operator from a int16_t value
         * @param v the int16_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (int16_t v);

        /**
         * @brief Assign-operator from a uint16_t value
         * @param v the uint16_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (uint16_t v);

        /**
         * @brief Assign-operator from a int32_t value
         * @param v the int32_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (int32_t v);

        /**
         * @brief Assign-operator from a uint32_t value
         * @param v the uint32_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (uint32_t v);

        /**
         * @brief Assign-operator from a int64_t value
         * @param v the int64_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (int64_t v);

        /**
         * @brief Assign-operator from a uint64_t value
         * @param v the uint64_t to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (uint64_t v);

        /**
         * @brief Assign-operator from a float value
         * @param v the float to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (float v);

        /**
         * @brief Assign-operator from a double value
         * @param v the double to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (double v);

        /**
         * @brief Assign-operator from a cstring; it will consider th '\0' terminator
         * @param v the cstring to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (CStr *cstr);

        /**
         * @brief Assign-operator from a string; it will consider th '\0' terminator
         * @param v the string to copy on this variant
         * @return this variant reference
         */
        SkVariant &operator =  (SkString &s);

        /**
         * @brief Assign-operator from a string-list
         * @param l the string-list to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkStringList &l);

        /**
         * @brief Assign-operator from a variant-list
         * @param l the variant-stack to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkStack<SkVariant> &s);

        /**
         * @brief Assign-operator from a variant-queue
         * @param m the variant-map to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkQueue<SkVariant> &q);

        /**
         * @brief Assign-operator from a variant-list
         * @param m the variant-map to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkAbstractList<SkVariant> &l);

        /**
         * @brief Assign-operator from a variant-pair
         * @param m the variant-pair to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkPair<SkVariant, SkVariant> &p);

        /**
         * @brief Assign-operator from a variant-list
         * @param m the variant-map to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkAbstractMap<SkString, SkVariant> &m);

        /**
         * @brief Assign-operator from a variant-list
         * @param m the variant-map to copy inside
         * @return this variant reference
         */
        SkVariant &operator =  (SkAbstractMap<SkVariant, SkVariant> &m);

        /**
         * @brief Assign-operator from a variant-map
         * @param ptr the raw pointer to set without managing its life
         * @return this variant reference
         */
        SkVariant &operator =  (void *ptr);

        /**
         * @brief Equal-operator from another variant
         * @param operand the other variant to compare
         * @return true if contents are equal, otherwise false
         */
        bool       operator == (const SkVariant &operand);

        /**
         * @brief NotEqual-operator from another variant
         * @param operand the other variant to compare
         * @return true if contents are not equal, otherwise false
         */
        bool       operator != (const SkVariant &operand);

        /**
         * @brief LessThan-operator of another variant
         * @param operand the other variant to compare
         * @return true if contents are less than, otherwise false
         */
        bool       operator <  (const SkVariant &operand);

        /**
         * @brief LessThanEqual-operator of another variant
         * @param operand the other variant to compare
         * @return true if contents are less than or equal, otherwise false
         */
        bool       operator <= (const SkVariant &operand);

        /**
         * @brief GreaterThan-operator of another variant
         * @param operand the other variant to compare
         * @return true if contents are greater than, otherwise false
         */
        bool       operator >  (const SkVariant &operand);

        /**
         * @brief GreaterThanEqual-operator of another variant
         * @param operand the other variant to compare
         * @return true if contents are greater than or equal, otherwise false
         */
        bool       operator >= (const SkVariant &operand);

        /**
         * @brief Sum-operator between this variant and another
         * @param operand the other variant to sum
         * @return a variant representing the sum
         */
        SkVariant  operator +  (const SkVariant &operand);

        /**
         * @brief Increment-operator for this variant of another
         * @param operand the other variant to sum
         * @return this variant reference
         */
        SkVariant &operator += (const SkVariant &operand);
        //SkVariant &operator ++ ();

        /**
         * @brief Subtraction-operator between this variant and another
         * @param operand the other variant to subtract
         * @return a variant representing the subtraction
         */
        SkVariant  operator -  (const SkVariant &operand);

        /**
         * @brief Decrement-operator for this variant of another
         * @param operand the other variant to subtract
         * @return this variant reference
         */
        SkVariant &operator -= (const SkVariant &operand);
        //SkVariant &operator -- ();

        /**
         * @brief Multiplication-operator between this variant and another
         * @param operand the other variant to multiplicate
         * @return a variant representing the multiplication
         */
        SkVariant  operator *  (const SkVariant &operand);

        /**
         * @brief AssignMultiplication-operator for this variant of another
         * @param operand the other variant to multiplicate
         * @return this variant reference
         */
        SkVariant &operator *= (const SkVariant &operand);

        /**
         * @brief Power-operator between this variant and another
         * @param operand the other variant as the power
         * @return a variant representing the result
         */
        SkVariant  operator ^  (const SkVariant &operand);

        /**
         * @brief AssignPower-operator for this variant of another
         * @param operand the other variant as the power
         * @return this variant reference
         */
        SkVariant &operator ^= (const SkVariant &operand);

        /**
         * @brief Division-operator between this variant and another
         * @param operand the other variant as the divisor
         * @return a variant representing the division
         */
        SkVariant  operator /  (const SkVariant &operand);

        /**
         * @brief AssignDivision-operator for this variant of another
         * @param operand the other variant as the divisor
         * @return this variant reference
         */
        SkVariant &operator /= (const SkVariant &operand);

        /**
         * @brief Remainder-operator between this variant and another
         * @param operand the other variant as the divisor
         * @return a variant representing the division remainder
         */
        SkVariant  operator %  (const SkVariant &operand);

        /**
         * @brief AssignRemainder-operator for this variant of another
         * @param operand the other variant as the divisor
         * @return this variant reference
         */
        SkVariant &operator %= (const SkVariant &operand);

        friend ostream &operator<<(std::ostream& os, SkVariant &v)
        {return os << v.toString();}

        /**
         * @brief Used internally, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string
         * @param v the variant of type T_MAP, T_LIST or T_BYTEARRAY
         * @param json the string to fill with document
         * @param indented permits to write indented document
         * @param level a variable to store the current indentation
         * @param spaces the number of spaces used for the indentation
         * @return true if the document is created, otherwise false
         */
        static bool toJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string
         * @param v the variant of type T_LIST
         * @param json the string to fill with JSON representation
         * @param indented permits to write indented document
         * @param level a variable to store the current indentation
         * @param spaces the number of spaces used for the indentation
         */
        static void listToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string
         * @param v the variant of type T_MAP
         * @param json the string to fill with JSON representation
         * @param indented permits to write indented document
         * @param level a variable to store the current indentation
         * @param spaces the number of spaces used for the indentation
         */
        static void mapToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string
         * @param v the variant of type T_BYTEARRAY
         * @param json the string to fill with JSON representation
         * @param indented permits to write indented document
         * @param level a variable to store the current indentation
         * @param spaces the number of spaces used for the indentation
         */
        static void byteArrayToJson(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string
         * @param v the variant of type T_STRING
         * @param json the string to fill with JSON representation
         */
        static void stringToJson(SkVariant &v, SkString &json);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant to its JSON representation, filling a string;
         * JSON represent a single char as a string with only one char
         * @param v the variant of type T_INT8
         * @param json the string to fill with JSON representation
         */
        static void charToJson(SkVariant &v, SkString &json);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a special character to its JSON representation, filling a string,
         * for example '\n' becomes "\\n";
         * @param c the special character to convert
         * @param output the string to fill
         * @return true if it is a valid special character,
         */
        static bool convertSpecialCharToString(const char c, SkString &output);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts a variant value to its JSON representation, filling a string
         * @param v the variant of type supported from JSON
         * @param json he string to fill with JSON representation
         * @param indented permits to write indented document
         * @param level a variable to store the current indentation
         * @param spaces the number of spaces used for the indentation
         */
        static void valueToJsonString(SkVariant &v, SkString &json, bool &indented, uint16_t &level, uint8_t &spaces);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON document cstring to a variant
         * @param s the json document as cstring
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool fromJson(CStr *json, SkVariant &v);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON document-part cstring to a variant
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonValue(CStr *s, uint64_t &valueLen, SkVariant &v);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON object to a variant
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonMap(CStr *s, uint64_t &valueLen, SkVariant &v);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON list to a variant
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonList(CStr *s, uint64_t &valueLen, SkVariant &v);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON string-value to a variant
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonString(CStr *s, uint64_t &valueLen, SkVariant &v);

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON decimal representation number-value to a variant;
         * an integer becomes T_INT64 and a float becomes T_DOUBLE
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonNumber(CStr *s, uint64_t &valueLen, SkVariant &v);//NO HEX HERE

        /**
         * @brief Internally used, but usable also for other scopes;
         * converts the JSON boolean-value to a variant
         * @param s the json document-part as cstring
         * @param valueLen the parsed length
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool parseJsonBoolean(CStr *s, uint64_t &valueLen, SkVariant &v);

        /**
         * @brief Simple facilties to convert a bool value to its string representation
         * @param v the bool value
         * @return the cstring with representation of the bool
         */
        static CStr *boolToString(bool v);

        bool getMapProperty(SkVariant &propName, SkVariant &val);
        bool getListItem(uint64_t index, SkVariant &val);

    private:
        SkVariantData props;
        SkString *stringRef;

        void cTorReset();
        void deleteVariantContainer();
        char *castToCharData();
        void convertValData(SkVariant_T variantType, uint64_t size);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantVector extends SkVector<SkVariant>
{
    public:
        SkVariantVector();
        SkVariantVector(const SkVector<SkVariant> &source);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantList extends SkList<SkVariant>
{
    public:
        SkVariantList();
        SkVariantList(const SkList<SkVariant> &source);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantStack extends SkStack<SkVariant>
{
    public:
        SkVariantStack();
        SkVariantStack(const SkVariantStack &source);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantQueue extends SkQueue<SkVariant>
{
    public:
        SkVariantQueue();
        SkVariantQueue(const SkQueue<SkVariant> &source);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantPair extends SkPair<SkVariant, SkVariant>
{
    public:
        SkVariantPair();
        SkVariantPair(const SkPair<SkVariant, SkVariant> &source);
};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkVariantMap extends SkMap<SkVariant, SkVariant>
{
    public:
        SkVariantMap();
        SkVariantMap(const SkMap<SkVariant, SkVariant> &source);
};

// // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkInt8Ptr extends SkCPointer<int8_t>
{public: SkInt8Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkUInt8Ptr extends SkCPointer<uint8_t>
{public: SkUInt8Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkInt16Ptr extends SkCPointer<int16_t>{public: SkInt16Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkUInt16Ptr extends SkCPointer<uint16_t>{public: SkUInt16Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkInt32Ptr extends SkCPointer<int32_t>{public: SkInt32Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkUInt32Ptr extends SkCPointer<uint32_t>{public: SkUInt32Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkInt64Ptr extends SkCPointer<int64_t>{public: SkInt64Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkUInt64Ptr extends SkCPointer<uint64_t>{public: SkUInt64Ptr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkFloatPtr extends SkCPointer<float>{public: SkFloatPtr();};

// // // // // // // // // // // // // // // // // // // // //

class SPECIALK SkDoublePtr extends SkCPointer<double>{public: SkDoublePtr();};

// // // // // // // // // // // // // // // // // // // // //

void *variantToCustom(SkVariant *);

#endif // SKVARIANT_H
