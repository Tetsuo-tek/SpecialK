#include "skmath.h"

SkMath::SkMath()
{

}

double SkMath::max(double val1, double val2)
{
    if (val1 >= val2)
        return val1;

    return val2;
}

double SkMath::min(double val1, double val2)
{
    if (val1 <= val2)
        return val1;

    return val2;
}

float SkMath::percent(double max, double val)
{
    if (val == 0. || max == 0.)
        return 0.;

    return (val / max) * 100;
}
