#include "sklogmachine.h"
#include "Containers/skvariant.h"

#if defined(SPECIALK_APPLICATION)
    #include "Core/App/skapp.h"
    #include "Core/System/Thread/skthread.h"
    #include "Core/Object/skobject.h"
#else
    SkLogMachine *logger=nullptr;
    StaticAutoLaunch(SkLogMachineInit, logInit)
    {new SkLogMachine;}
#endif

#include <syslog.h>

#include <unistd.h>

#define SKWHITE_COL     "\033[1;97m"
#define SKRED_COL       "\033[1;31m"

#define SKAPP_COL       "\033[1;35m"
#define SKLOOP_COL      "\033[1;36m"//
#define SKFLT_COL       "\033[1;34m"
#define SKSTC_COL       "\033[1;94m"

#define SKMSG_COL       "\033[1;32m"//
#define SKWRN_COL       "\033[1;33m"//
#define SKERR_COL       SKRED_COL
#define SKDBG_COL       "\033[1;34m"
#define SKPDBG_COL      "\033[1;90m"

#define SKBOLD_MODE     "\033[1m"
#define SKRESETBOLD     "\033[0m"

#define SKDIM_MODE      "\033[2m"
#define SKRESETDIM      "\033[22m"

/*
31: Red (don't use with green background)
32: Green
33: Yellow
34: Blue
35: Magenta/Purple
36: Cyan
37: Light Gray
90: Dark Gray
91: Light Red
92: Light Green
93: Light Yellow
94: Light Blue
95: Light Magenta/Pink
96: Light Cyan
97: White
*/

#include "Core/System/Time/skdatetime.h"
#include "Core/Containers/skargsmap.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkLogType)
{
    SetupEnumWrapper(SkLogType);

    SetEnumItem(SkLogType::Msg);
    SetEnumItem(SkLogType::Wrn);
    SetEnumItem(SkLogType::Err);
    SetEnumItem(SkLogType::Dbg);
    SetEnumItem(SkLogType::PlusDbg);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkLogMachine);

DeclareMeth_INSTANCE_VOID(SkLogMachine, enable, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enableDateTime, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enableSrcFileName, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enablePrettyName, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enablePID, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enableDebug, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enablePlusDebug, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkLogMachine, enableEscapes, Arg_Bool)

DeclareMeth_INSTANCE_VOID(SkLogMachine, stcMsg, Arg_Enum(SkLogType), Arg_CStr, Arg_CStr, Arg_CStr, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkLogMachine, fltMsg, Arg_Enum(SkLogType), Arg_CStr, Arg_CStr, Arg_CStr, Arg_Int, Arg_Custom(SkFlatObject))

#if defined(ENABLE_SKAPP)
    DeclareMeth_INSTANCE_VOID(SkLogMachine, objMsg, Arg_Enum(SkLogType), Arg_CStr, Arg_CStr, Arg_CStr, Arg_Int, Arg_Custom(SkObject))
#endif

DeclareMeth_INSTANCE_RET(SkLogMachine, isEnabled, bool)
DeclareMeth_INSTANCE_RET(SkLogMachine, isDateTimeEnabled, bool)
DeclareMeth_INSTANCE_RET(SkLogMachine, isPrettyNameEnabled, bool)
DeclareMeth_INSTANCE_RET(SkLogMachine, isDebugEnabled, bool)
DeclareMeth_INSTANCE_RET(SkLogMachine, isPlusDebugEnabled, bool)
DeclareMeth_INSTANCE_RET(SkLogMachine, isEscapesEnabled, bool)

SetupClassWrapper(SkLogMachine)
{
    SetClassSuper(SkLogMachine, SkFlatObject);

    AddEnumType(SkLogType);

    AddMeth_INSTANCE_VOID(SkLogMachine, enable);
    AddMeth_INSTANCE_VOID(SkLogMachine, enableDateTime);
    AddMeth_INSTANCE_VOID(SkLogMachine, enableSrcFileName);
    AddMeth_INSTANCE_VOID(SkLogMachine, enablePrettyName);
    AddMeth_INSTANCE_VOID(SkLogMachine, enablePID);
    AddMeth_INSTANCE_VOID(SkLogMachine, enableDebug);
    AddMeth_INSTANCE_VOID(SkLogMachine, enablePlusDebug);
    AddMeth_INSTANCE_VOID(SkLogMachine, enableEscapes);
    AddMeth_INSTANCE_VOID(SkLogMachine, stcMsg);
    AddMeth_INSTANCE_VOID(SkLogMachine, fltMsg);

#if defined(ENABLE_SKAPP)
    AddMeth_INSTANCE_VOID(SkLogMachine, objMsg);
#endif

    AddMeth_INSTANCE_RET(SkLogMachine, isEnabled);
    AddMeth_INSTANCE_RET(SkLogMachine, isDateTimeEnabled);
    AddMeth_INSTANCE_RET(SkLogMachine, isPrettyNameEnabled);
    AddMeth_INSTANCE_RET(SkLogMachine, isDebugEnabled);
    AddMeth_INSTANCE_RET(SkLogMachine, isPlusDebugEnabled);
    AddMeth_INSTANCE_RET(SkLogMachine, isEscapesEnabled);
}

// // // // // // // // // // // // // // // // // // // // //

SkLogMachine::SkLogMachine()
{
    CreateClassWrapper(SkLogMachine);

    logs = nullptr;
    sysLogEnabled = false;

    active = true;

    logOwnerName = true;
    withEscapes =  true;

    logDateTime = false;
    logSrcCodeFile = false;
    logPretty = false;
    logPID = false;
    logDebug = false;
    logPlusDebug = false;

    logger = this;
    setObjectName("LogMachine");
}

void SkLogMachine::enable(bool enabled)
{
    active = enabled;
    FlatDebug("Changed LOGGER state [active: "<< SkVariant::boolToString(active) << "]");
}

void SkLogMachine::enableSysLog(CStr *identifier)
{
    loggerEx.lock();
    sysLogEnabled = true;
    openlog(identifier, LOG_PID | LOG_CONS, LOG_USER);
    loggerEx.unlock();

    FlatDebug("Enabled SysLog [identifier: " << identifier << "]");
}

void SkLogMachine::disableSysLog()
{
    if (!sysLogEnabled)
        return;

    sysLogEnabled = false;
    closelog();
}

void SkLogMachine::enableOwnerName(bool enabled)
{
    logOwnerName = enabled;
    FlatDebug("Enabled logOwnerName on LOGGER [enabled: "<< SkVariant::boolToString(logOwnerName) << "]");
}

void SkLogMachine::enableDateTime(bool enabled)
{
    logDateTime = enabled;
    FlatDebug("Enabled logDateTime on LOGGER [enabled: "<< SkVariant::boolToString(logDateTime) << "]");
}

void SkLogMachine::enableSrcFileName(bool enabled)
{
    logSrcCodeFile = enabled;
    FlatDebug("Enabled logSrcCodeFile on LOGGER [enabled: "<< SkVariant::boolToString(logSrcCodeFile) << "]");
}

void SkLogMachine::enablePrettyName(bool enabled)
{
    logPretty = enabled;
    FlatDebug("Enabled logPretty on LOGGER [enabled: "<< SkVariant::boolToString(logPretty) << "]");
}

void SkLogMachine::enablePID(bool enabled)
{
    logPID = enabled;
    FlatDebug("Enabled logPID on LOGGER [enabled: "<< SkVariant::boolToString(logPID) << "]");
}

void SkLogMachine::enableDebug(bool enabled)
{
    logDebug = enabled;
    FlatDebug("Enabled logDebug on LOGGER [enabled: " << SkVariant::boolToString(logDebug) << "]");
}

void SkLogMachine::enablePlusDebug(bool enabled)
{
    logPlusDebug = enabled;
    FlatDebug("Enabled logPlusDebug on LOGGER [enabled: "<< SkVariant::boolToString(logPlusDebug) << "]");
}

void SkLogMachine::enableEscapes(bool enabled)
{
    withEscapes = enabled;
    FlatDebug("Enabled escapes on LOGGER [enabled: "<< SkVariant::boolToString(withEscapes) << "]");
}

bool SkLogMachine::isEnabled()
{
    return active;
}

bool SkLogMachine::isSysLogEnabled()
{
    return sysLogEnabled;
}

bool SkLogMachine::isOwnerNameEnabled()
{
    return logOwnerName;
}

bool SkLogMachine::isDateTimeEnabled()
{
    return logDateTime;
}

bool SkLogMachine::isPrettyNameEnabled()
{
    return logPretty;
}

bool SkLogMachine::isDebugEnabled()
{
    return logDebug;
}

bool SkLogMachine::isPlusDebugEnabled()
{
    return logPlusDebug;
}

bool SkLogMachine::isEscapesEnabled()
{
    return withEscapes;
}

SkMutex &SkLogMachine::getMutexEx()
{
    return loggerEx;
}

void SkLogMachine::stcMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber)
{
    internalMSG(SKSTC_COL, "[STC]", type, text, srcFile, pretty, lineNumber);
}

void SkLogMachine::fltMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkFlatObject *obj)
{
    if (!SkString::isEmpty(obj->objectName()))
        internalMSG(SKFLT_COL, obj->objectName(), type, text, srcFile, pretty, lineNumber);
    else
        internalMSG(SKFLT_COL, "[FLT]", type, text, srcFile, pretty, lineNumber);
}

#if defined(ENABLE_SKAPP)
void SkLogMachine::objMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkObject *obj)
{
    internalMSG(SKWHITE_COL, obj->objectName(), type, text, srcFile, pretty, lineNumber);
}

void SkLogMachine::loopMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkEventLoop *loop)
{
    if (loop->threadID() == tid)
        internalMSG(SKAPP_COL, loop->objectName(), type, text, srcFile, pretty, lineNumber);

    else
        internalMSG(SKLOOP_COL, loop->objectName(), type, text, srcFile, pretty, lineNumber);
}
#endif

#if defined(ENABLE_QTAPP)
#include <QObject>

void SkLogMachine::qobjMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, QObject *obj)
{
    SkString name = obj->objectName().toStdString();
    internalMSG(SKSTC_COL, name.c_str(), type, text, srcFile, pretty, lineNumber);
}

#endif

void SkLogMachine::internalMSG(CStr *objColor, CStr *name, SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber)
{
    /*if (type != SkLogType::Err && (!active || (!logDebug && type == SkLogType::Dbg) || (!logPlusDebug && type == SkLogType::PlusDbg)))
        return;*/

    loggerEx.lock();

    if (sysLogEnabled && (type == SkLogType::Dbg || type == SkLogType::PlusDbg))
            return;

    if (logs)
    {
        SkLog *l = new SkLog;
        l->dateTime = SkDateTime::currentDateTimeISOString();
        l->t = type;
        l->srcFileName = srcFile;
        l->prettyName = pretty;
        l->descr = text;

        logs->enqueue(l);
    }

    SkString s;

    if (logDateTime)
    {
        s += SkDateTime::currentDateTimeISOString();
        s += " ";
    }

    if (logPID)
    {
        if (!sysLogEnabled && withEscapes)
        {
            s += SKDIM_MODE;
            s += SKERR_COL;
        }

        s += "#";
        s += SkString::number(::getpid());
        s += " ";

        if (!sysLogEnabled && withEscapes)
            s += SKRESETDIM;
    }

    if (logOwnerName)
    {
        if (!sysLogEnabled && withEscapes)
        {
            s += SKBOLD_MODE;
            s += objColor;//SKWHITE_COL;
        }
        s += "{";

        uint64_t sz = strlen(name);

        if (sz <= 50)
            s += name;

        else
        {
            s += "(..)";
            s += &name[sz-30];
        }

        s += "}";

        if (!sysLogEnabled && withEscapes)
            s += SKRESETBOLD;
    }

    if (logSrcCodeFile || type == SkLogType::Err)
    {
        s += " ";

        if (!sysLogEnabled && withEscapes)
            s += SKDIM_MODE;

        if (CStr *p = ::strrchr(srcFile, '/'))
            s += p+1;
        else
            s += srcFile;

        if (!sysLogEnabled && withEscapes)
            s += SKRESETDIM;
    }

    if (logPretty || type == SkLogType::Err)
    {
        if (logSrcCodeFile)
            s += ":";

        s += " ";

        if (!sysLogEnabled && withEscapes)
            s += SKDIM_MODE;

        s += pretty;

        if (!sysLogEnabled && withEscapes)
            s += SKRESETDIM;

        s += " [L: ";
        s.concat(lineNumber);
        s += "]";
    }

    if (!sysLogEnabled && withEscapes)
    {
        if (type == SkLogType::Msg)
            s += SKMSG_COL;

        else if (type == SkLogType::Wrn)
            s += SKWRN_COL;

        else if (type == SkLogType::Err)
            s += SKERR_COL;

        else if (type == SkLogType::Dbg)
            s += SKDBG_COL;

        else if (type == SkLogType::PlusDbg)
            s += SKPDBG_COL;
    }

    if (logDateTime || logPID || logOwnerName || logSrcCodeFile || logPretty || type == SkLogType::Err)
        s += " -> ";

    s += text;

    if (!sysLogEnabled && withEscapes)
        s += "\033[0m";

    if (sysLogEnabled)
    {
        if (type == SkLogType::Msg)
            syslog(LOG_INFO, s.c_str());

        else if (type == SkLogType::Wrn)
            syslog(LOG_WARNING, s.c_str());

        else if (type == SkLogType::Err)
            syslog(LOG_ERR, s.c_str());
    }

    else
    {

        if (type == SkLogType::Err)
#if defined(ENABLE_QTAPP)
            qDebug() << s;
#else
            cerr << s << "\n";
#endif

        else
#if defined(ENABLE_QTAPP)
            qDebug() << s;
#else
            cout << s << "\n";
#endif
    }

    loggerEx.unlock();
}

void SkLogMachine::setAsLogQueue(SkQueue<SkLog *> *logsList)
{
    loggerEx.lock();
    logs = logsList;
    loggerEx.unlock();
}

void SkLogMachine::disableLogQueue()
{
    loggerEx.lock();
    logs = nullptr;
    loggerEx.unlock();
}

CStr *SkLogMachine::logTypeName(SkLogType t)
{
    if (t == SkLogType::Msg)
        return "MSG";

    else if (t == SkLogType::Wrn)
        return "WRN";

    else if (t == SkLogType::Err)
        return "ERR";

    else if (t == SkLogType::Dbg)
        return "DBG";

    else if (t == SkLogType::PlusDbg)
        return "PDBG";

    return "NULL";
}

