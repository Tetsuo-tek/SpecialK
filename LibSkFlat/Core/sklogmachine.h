/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKLOGMACHINE_H
#define SKLOGMACHINE_H

/**
 * @file skflatlogmachine.h
*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <sys/ioctl.h>
#include <stdio.h>

#include "Core/Containers/skqueue.h"

#if defined(ENABLE_QTAPP)
    #include <QDebug>
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkLogType
{
    Msg,
    Wrn,
    Err,
    Dbg,
    PlusDbg
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkLog
{
    SkLogType t;
    string srcFileName;
    string prettyName;
    string dateTime;
    string descr;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkArgsMap;

#if defined(ENABLE_SKAPP)
    class SkObject;
    class SkEventLoop;
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/**
  * @brief SkLogMachine
  *  
  *
  * It is a unique, static and global instance of the log service that you can
  * take advantage of.
  *
  * It is created during the 'skApp' startup and is reachable from any portion
  * of the code, including this file, through the 'logger' pointer; in truth it
  * will be used only through facilitating macros.
  *
  * Logs can be of 4 types:
  *
  * * notification;
  *
  * * Warning;
  *
  * * Error;
  *
  * * Debug (normal debug);
  *
  * * PlusDebug (deep debug).
  *
  * The log produced is affected, by its shape, by the context and the object in
  * which it is created; in fact it will be different if it comes from a SkEventLoop,
  * a SkObject or a simple SkFlatObject.
  *
  * If the log is produced by a SkEventLoop or a derivative of SkObject, the object
  * instance name will also be used, while if it is a SkFlatObject, which does
  * not have an instance name, the fixed name '{ FLT} '.
  *
  * Other informations, which can all be activated / deactivated, common to all
  * types of product logs are:
  *
  * * datetime of the message, in ISO format;
  *
  * * name of the file from which the message comes;
  *
  * * prototype of the method or function from which the message comes;
  *
  * * colored message according to the type.
  *
*/

class SPECIALK SkLogMachine extends SkFlatObject
{
    public:
        /**
         * @brief Constructor (Internally used)
        */
        SkLogMachine();

        /**
         * @brief Enables/Disables the LogMachine
         * @param enabled value
        */
        void enable(bool enabled);

        void enableSysLog(CStr *identifier);
        void disableSysLog();

        /**
         * @brief Enables/Disables owner name inside the logs
         * @param enabled value
        */
        void enableOwnerName(bool enabled);

        /**
         * @brief Enables/Disables ISO DateTime inside the logs
         * @param enabled value
        */
        void enableDateTime(bool enabled);

        /**
         * @brief Enables/Disables the name of the file containing the log call
         * @param enabled value
        */
        void enableSrcFileName(bool enabled);

        /**
         * @brief Enables/Disables the signature of the function/meth containing the
         * log call
         * @param enabled value
        */
        void enablePrettyName(bool enabled);

        /**
         * @brief Enables/Disables the process-id inside the logs
         * @param enabled value
        */
        void enablePID(bool enabled);

        /**
         * @brief Enables/Disables DEBUG logs
         * @param enabled value
        */
        void enableDebug(bool enabled);

        /**
         * @brief Enables/Disables PLUS-DEBUG (DEEP) logs
         * @param enabled value
        */
        void enablePlusDebug(bool enabled);

        /**
         * @brief Enables/Disables escapes (formatting and colors) inside the logs
         * @param enabled value
        */
        void enableEscapes(bool enabled);

        /**
         * @brief Builds a log message from a static context
         * @param type the type of the building log
         * @param text the log text
         * @param srcFile the name of the file containing the log-call
         * @param pretty the static function/meth containing the log-call
        */
        void stcMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber);

        /**
         * @brief Builds a log message from a generic SkFlatObject
         * @param type the type of the building log
         * @param text the log text
         * @param srcFile the name of the file containing the log-call
         * @param pretty the meth containing the log-call
         * @param obj the object emitting the log
        */
        void fltMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkFlatObject *obj);

#if defined(ENABLE_SKAPP)
        /**
         * @brief Builds a log message from a SkObject derivate
         * @param type the type of the building log
         * @param text the log text
         * @param srcFile the name of the file containing the log-call
         * @param pretty the meth containing the log-call
         * @param obj the object emitting the log
        */
        void objMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkObject *obj);

        /**
         * @brief Builds a log message from a specific SkEventLoop and its derivates as SkApp
         * @param type the type of the building log
         * @param text the log text
         * @param srcFile the name of the file containing the log-call
         * @param pretty the function/meth containing the log-call
         * @param obj the object emitting the log
        */
        void loopMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, SkEventLoop *loop);
#endif

#if defined(ENABLE_QTAPP)
        void qobjMsg(SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber, QObject *obj);
#endif
        /**
         * @brief Check if the LogMachine is enabled
         * @return true if enabled, otherwise false
         */
        bool isEnabled();

        bool isSysLogEnabled();

        /**
         * @brief Check if logs contain the owner name part
         * @return true if enabled, otherwise false
         */
        bool isOwnerNameEnabled();

        /**
         * @brief Check if logs contain the ISO DateTime part
         * @return true if enabled, otherwise false
         */
        bool isDateTimeEnabled();

        /**
         * @brief Check if logs contain the function/meth signature
         * @return true if enabled, otherwise false
         */
        bool isPrettyNameEnabled();

        /**
         * @brief Check if DEBUG logs are enabled
         * @return true if enabled, otherwise false
         */
        bool isDebugEnabled();

        /**
         * @brief Check if PLUS-DEBUG (DEEP) logs are enabled
         * @return true if enabled, otherwise false
         */
        bool isPlusDebugEnabled();

        /**
         * @brief Check if escapes (formatting and colors) inside logs are enabled
         * @return true if enabled, otherwise false
         */
        bool isEscapesEnabled();

        /**
         * @brief Gets the logger messages mutex
         * @return the mutex
         */
        SkMutex &getMutexEx();

        void setAsLogQueue(SkQueue<SkLog *> *logsList);
        void disableLogQueue();

        static CStr *logTypeName(SkLogType t);

    private:
        SkMutex loggerEx;

        bool active;

        bool sysLogEnabled;

        bool logOwnerName;
        bool logDateTime;
        bool logSrcCodeFile;
        bool logPretty;
        bool logPID;
        bool logDebug;
        bool logPlusDebug;
        bool withEscapes;

        SkQueue<SkLog *> *logs;

    protected:
        void internalMSG(CStr *objColor, CStr *name, SkLogType type, CStr *text, CStr *srcFile, CStr *pretty, int lineNumber);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

extern SkLogMachine *logger;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//LogMachine COMMONs

#define SK_LOG_CONDITION(TYPE) \
    logger != nullptr && logger->isEnabled() \
    && !(TYPE == SkLogType::Dbg && !logger->isDebugEnabled()) \
    && !(TYPE == SkLogType::PlusDbg && !logger->isPlusDebugEnabled())

#define SK_STCLOG(TYPE, LOGITEM) \
    FakeSingleLine( \
        if (SK_LOG_CONDITION(TYPE)) \
        { \
            stringstream stream; \
            stream << LOGITEM; \
            logger->stcMsg(TYPE, stream.str().data(), __FILE__, __PRETTY_FUNCTION__, __LINE__); \
        } \
    )

#define SK_LOG(TYPE, LOGITEM, OBJ, METH) \
    FakeSingleLine( \
        if (SK_LOG_CONDITION(TYPE)) \
        { \
            stringstream stream; \
            stream << LOGITEM; \
            logger->METH(TYPE, stream.str().data(), __FILE__, __PRETTY_FUNCTION__, __LINE__, OBJ); \
        } \
    )

#define StaticMessage(LOGITEM)              SK_STCLOG(SkLogType::Msg,       LOGITEM)
#define StaticWarning(LOGITEM)              SK_STCLOG(SkLogType::Wrn,       LOGITEM)
#define StaticError(LOGITEM)                SK_STCLOG(SkLogType::Err,       LOGITEM)
#define StaticDebug(LOGITEM)                SK_STCLOG(SkLogType::Dbg,       LOGITEM)
#define StaticPlusDebug(LOGITEM)            SK_STCLOG(SkLogType::PlusDbg,   LOGITEM)

#define FlatMessage(LOGITEM)                SK_LOG(SkLogType::Msg,          LOGITEM, this, fltMsg)
#define FlatWarning(LOGITEM)                SK_LOG(SkLogType::Wrn,          LOGITEM, this, fltMsg)
#define FlatError(LOGITEM)                  SK_LOG(SkLogType::Err,          LOGITEM, this, fltMsg)
#define FlatDebug(LOGITEM)                  SK_LOG(SkLogType::Dbg,          LOGITEM, this, fltMsg)
#define FlatPlusDebug(LOGITEM)              SK_LOG(SkLogType::PlusDbg,      LOGITEM, this, fltMsg)

#define FlatMessage_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Msg,          LOGITEM, OBJ, fltMsg)
#define FlatWarning_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Wrn,          LOGITEM, OBJ, fltMsg)
#define FlatError_EXT(OBJ, LOGITEM)         SK_LOG(SkLogType::Err,          LOGITEM, OBJ, fltMsg)
#define FlatDebug_EXT(OBJ, LOGITEM)         SK_LOG(SkLogType::Dbg,          LOGITEM, OBJ, fltMsg)
#define FlatPlusDebug_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::PlusDbg,      LOGITEM, OBJ, fltMsg)

#define ObjectMessage(LOGITEM)              SK_LOG(SkLogType::Msg,          LOGITEM, this, objMsg)
#define ObjectWarning(LOGITEM)              SK_LOG(SkLogType::Wrn,          LOGITEM, this, objMsg)
#define ObjectError(LOGITEM)                SK_LOG(SkLogType::Err,          LOGITEM, this, objMsg)
#define ObjectDebug(LOGITEM)                SK_LOG(SkLogType::Dbg,          LOGITEM, this, objMsg)
#define ObjectPlusDebug(LOGITEM)            SK_LOG(SkLogType::PlusDbg,      LOGITEM, this, objMsg)

#define ObjectMessage_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::Msg,          LOGITEM, OBJ, objMsg)
#define ObjectWarning_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::Wrn,          LOGITEM, OBJ, objMsg)
#define ObjectError_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Err,          LOGITEM, OBJ, objMsg)
#define ObjectDebug_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Dbg,          LOGITEM, OBJ, objMsg)
#define ObjectPlusDebug_EXT(OBJ, LOGITEM)   SK_LOG(SkLogType::PlusDbg,      LOGITEM, OBJ, objMsg)

#define LoopMessage(LOGITEM)                SK_LOG(SkLogType::Msg,          LOGITEM, this, loopMsg)
#define LoopWarning(LOGITEM)                SK_LOG(SkLogType::Wrn,          LOGITEM, this, loopMsg)
#define LoopError(LOGITEM)                  SK_LOG(SkLogType::Err,          LOGITEM, this, loopMsg)
#define LoopDebug(LOGITEM)                  SK_LOG(SkLogType::Dbg,          LOGITEM, this, loopMsg)
#define LoopPlusDebug(LOGITEM)              SK_LOG(SkLogType::PlusDbg,      LOGITEM, this, loopMsg)

#define LoopMessage_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Msg,          LOGITEM, OBJ, loopMsg)
#define LoopWarning_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Wrn,          LOGITEM, OBJ, loopMsg)
#define LoopError_EXT(OBJ, LOGITEM)         SK_LOG(SkLogType::Err,          LOGITEM, OBJ, loopMsg)
#define LoopDebug_EXT(OBJ, LOGITEM)         SK_LOG(SkLogType::Dbg,          LOGITEM, OBJ, loopMsg)
#define LoopPlusDebug_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::PlusDbg,      LOGITEM, OBJ, loopMsg)

#endif // SKLOGMACHINE_H
