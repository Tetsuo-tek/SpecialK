#ifndef SKABSTRACTFLATDEVICE_H
#define SKABSTRACTFLATDEVICE_H

#include "Core/Containers/skstring.h"
#include "Core/Object/skflatsignal.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define SKMAXKREADLINE_LEN                                      8192
#define MAXKSENDDATABLOCK_LEN                                   8192

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// THESE enums COULD GO INSIDE ABSTRACT DEVICE

enum SkDeviceState
{
    Open,
    Closed
};

enum SkDeviceError
{
    NoErrorOnDevice,
    CannotOpen,
    CannotSeekDevice,
    CannotReadFromDevice,
    CannotWriteToDevice,
    DeviceAlreadyOpen,
    CannotUseClosedDevice,
    TargetNotValid,
    DeviceInternalError
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkDataBuffer;
class SkRingBuffer;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAbstractFlatDevice extends SkFlatObject
{
    SkDeviceState state;
    SkDeviceError err;

    ULong txCounter;
    ULong rxCounter;

    public:
        virtual Long pos()                                      =0;
        virtual bool rewind()                                   =0;
        virtual bool seek(Long)                                 =0;
        virtual bool atEof()                                    =0;
        virtual Long canReadLine()                              =0;
        virtual bool flush()                                    =0;
        virtual Long size()                                     =0;
        virtual Long bytesAvailable()                           =0;
        virtual void close()                                    {}

        bool readFromWriteHere(SkAbstractFlatDevice *dev, Long &len);
        bool readHereWriteTo(SkAbstractFlatDevice *dev, Long len=0);

        bool read(char &c, bool peek=false);
        bool read(char *data, Long &len, bool peek=false);

        bool read(SkString &s, Long len=0, bool peek=false);
        bool read(SkDataBuffer *buf, Long len=0, bool peek=false);
        bool read(SkRingBuffer *buf, Long len=0, bool peek=false);

        bool readAll(SkString &s, bool peek=false);
        bool readAll(SkDataBuffer *buf, bool peek=false);
        bool readAll(SkRingBuffer *buf, bool peek=false);

        bool write(char c);
        bool write(SkString &s);

        bool write(CStr *data, Long len);
        bool write(SkDataBuffer *buf, Long len=0);
        bool write(SkRingBuffer *buf, Long len=0);

        bool writeUInt8(uint8_t val);
        uint8_t readUInt8(bool peek=false);

        bool writeInt8(int8_t val);
        int8_t readInt8(bool peek=false);

        bool writeUInt16(uint16_t val);
        uint16_t readUInt16(bool peek=false);

        bool writeInt16(int16_t val);
        int16_t readInt16(bool peek=false);

        bool writeUInt32(uint32_t val);
        uint32_t readUInt32(bool peek=false);

        bool writeInt32(int32_t val);
        int32_t readInt32(bool peek=false);

        bool writeUInt64(uint64_t val);
        uint64_t readUInt64(bool peek=false);

        bool writeInt64(int64_t val);
        int64_t readInt64(bool peek=false);

        bool writeFloat(float val);
        float readFloat(bool peek=false);

        bool writeDouble(double val);
        double readDouble(bool peek=false);

        uint64_t getTxDataCounter();
        void resetTxDataCounter();

        uint64_t getRxDataCounter();
        void resetRxDataCounter();

        void resetAllDataCounters();

        bool isOpen();

        SkDeviceState currentState();
        CStr *currentStateString();

        SkDeviceError lastError();
        CStr *lastErrorString();

    protected:
        SkAbstractFlatDevice();
        ~SkAbstractFlatDevice();

        void notifyRead(Long sz);
        void notifyWrite(Long sz);
        void notifyState(SkDeviceState st);
        void notifyError(SkDeviceError e, CStr *prettyFrom, CStr *comment=nullptr);

        virtual bool readInternal(char *, Long &, bool)         =0;
        virtual bool writeInternal(CStr *, Long)                =0;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKABSTRACTFLATDEVICE_H
