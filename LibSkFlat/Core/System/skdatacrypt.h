/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKDATACRYPT_H
#define SKDATACRYPT_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"

/**
 * @file skdatacrypt.h
*/

#if defined(ENABLE_AES)
    #include <openssl/evp.h>
    typedef EVP_CIPHER_CTX SkCryptContext;
#endif

/**
 * @brief The SkHashMode enum
 */
enum SkHashMode
{
    HM_MD4,
    HM_MD5,
    HM_SHA1,
    HM_SHA224,
    HM_SHA256,
    HM_SHA384,
    HM_SHA512,
    HM_RIPEMD160,
    HM_WHIRLPOOL

    /* NOT IMPLEMENTED YET
    HM_DSS,
    HM_DSS1,
    HM_MD5SHA1,
    HM_ECDSA,
    HM_SM3,
    HM_GOST341194,
    HM_GOST2814789IMIT,
    HM_STREEBOG256,
    HM_STREEBOG512*/
};

/**
 * @brief The SkCryptMode enum
 */
enum SkCryptMode
{
    CM_AES128_ECB,
    CM_AES128_CBC,
    CM_AES128_CFB1,
    CM_AES128_CFB8,
    CM_AES128_CFB128,
    CM_AES128_OFB,
    CM_AES128_CTR,
    CM_AES128_CCM,
    CM_AES128_GCM,
    CM_AES128_WRAP,
    CM_AES128_XTS,
    CM_AES192_ECB,
    CM_AES192_CBC,
    CM_AES192_CFB1,
    CM_AES192_CFB8,
    CM_AES192_CFB128,
    CM_AES192_OFB,
    CM_AES192_CTR,
    CM_AES192_CCM,
    CM_AES192_GCM,
    CM_AES192_WRAP,
    CM_AES256_ECB,
    CM_AES256_CBC,
    CM_AES256_CFB1,
    CM_AES256_CFB8,
    CM_AES256_CFB128,
    CM_AES256_OFB,
    CM_AES256_CTR,
    CM_AES256_CCM,
    CM_AES256_GCM,
    CM_AES256_WRAP,
    CM_AES256_XTS,
    CM_AES128_CBC_HMAC_SHA1,
    CM_AES256_CBC_HMAC_SHA1

    /* NOT IMPLEMENTED YET
    CM_DES_ECB,
    CM_DES_EDE,
    CM_DES_EDE3,
    CM_DES_EDEECB,
    CM_DES_EDE3ECB,
    CM_DES_CFB1,
    CM_DES_CFB8,
    CM_DES_CFB64,
    CM_DES_EDECFB64,
    CM_DES_EDE3CFB1,
    CM_DES_EDE3CFB8,
    CM_DES_EDE3CFB64,
    CM_DES_OFB,
    CM_DES_EDEOFB,
    CM_DES_EDE3OFB,
    CM_DES_CBC,
    CM_DES_EDECBC,
    CM_DES_EDE3CBC,
    CM_DESX_CBC,
    CM_RC4,
    CM_RC4_40,
    CM_RC4_HMAC_MD5,
    CM_IDEA_ECB,
    CM_IDEA_CFB64,
    CM_IDEA_OFB,
    CM_IDEA_CBC,
    CM_RC2_ECB,
    CM_RC2_CBC,
    CM_RC2_40_CBC,
    CM_RC2_64_CBC,
    CM_RC2_CFB64,
    CM_RC2_OFB,
    CM_CAST5_ECB,
    CM_CAST5_CBC,
    CM_CAST5_CFB64,
    CM_CAST5_OFB,
    CM_CAMELIA_128_ECB,
    CM_CAMELIA_128_CBC,
    CM_CAMELIA_128_CFB1,
    CM_CAMELIA_128_CFB8,
    CM_CAMELIA_128_CFB128,
    CM_CAMELIA_128_OFB,
    CM_CAMELIA_192_ECB,
    CM_CAMELIA_192_CBC,
    CM_CAMELIA_192_CFB1,
    CM_CAMELIA_192_CFB8,
    CM_CAMELIA_192_CFB128,
    CM_CAMELIA_192_OFB,
    CM_CAMELIA_256_ECB,
    CM_CAMELIA_256_CBC,
    CM_CAMELIA_256_CFB1,
    CM_CAMELIA_256_CFB8,
    CM_CAMELIA_256_CFB128,
    CM_CAMELIA_256_OFB,
    CM_CHACHA20,
    CM_GOST2814789_ECB,
    CM_GOST2814789_CFB64,
    CM_GOST2814789_CNT,
    CM_SM4_ECB,
    CM_SM4_CBC,
    CM_SM4_CFB128,
    CM_SM4_OFB,
    CM_SM4_CTR*/
};

/**
 * @brief The SkKeySalt struct
 */
struct SkKeySalt
{
    uint32_t hi;
    uint32_t lo;
};

class SkDataBuffer;

class SPECIALK SkDataCrypt extends SkFlatObject
{
    public:
        /**
         * @brief Simple constructor
         */
        SkDataCrypt();

#if defined(ENABLE_AES)
        /**
         * @brief Opens the encryption/decryption session
         * @param salt the SkSaltKey reference to use
         * @param key the encrypting/decrypting key
         * @param cryptMode the encrypting/decrypting mode to use
         * @param hashMode the hashing mode to use
         * @return true if there are not errors, otherwise false
         */
        bool open(SkKeySalt &salt,
                  SkString &key,
                  SkCryptMode cryptMode=SkCryptMode::CM_AES256_CBC,
                  SkHashMode hashMode=SkHashMode::HM_SHA1);

        /**
         * @brief Encrypts a raw array data pointer
         * @param input the raw array data pointer to encrypt
         * @param size the size of data
         * @param output the buffer to fill with the encrypted data
         */
        void encrypt(void *input, uint64_t size, SkDataBuffer &output);

        /**
         * @brief Encrypts a string
         * @param input the string to encrypt
         * @param output the buffer to fill with the encrypted data
         */
        void encrypt(SkString &input, SkDataBuffer &output);

        /**
         * @brief Encrypts a string
         * @param input the buffer to encrypt
         * @param output the buffer to fill with the encrypted data
         */
        void encrypt(SkDataBuffer &input, SkDataBuffer &output);

        /**
         * @brief Decrypts a raw array data pointer to a string
         * @param input the raw array data pointer to decrypt
         * @param size the size of data
         * @param output the string to fill with decrypted data
         */
        void decrypt(void *input, uint64_t size, SkString &output);

        /**
         * @brief Decrypts a data-buffer to a string
         * @param input the buffer to decrypt
         * @param output the string to fill with decrypted data
         */
        void decrypt(SkDataBuffer &input, SkString &output);

        /**
         * @brief Decrypts a raw array data pointer to a data-buffer
         * @param input the raw array data pointer to decrypt
         * @param size the size of data
         * @param output the buffer to fill with the decrypted data
         */
        void decrypt(void *input, uint64_t size, SkDataBuffer &output);

        /**
         * @brief Decrypts a data-buffer to a data-buffer
         * @param input the buffer to decrypt
         * @param output the buffer to fill with the decrypted data
         */
        void decrypt(SkDataBuffer &input, SkDataBuffer &output);

        /**
         * @brief Close the encryption/decryption session
         */
        void close();
#endif
        /**
         * @brief Makes hash of a raw array data pointer to a string
         * @param input the raw array data pointer to hash
         * @param size the size of data
         * @param mode the hashing mode
         * @return the hash string
         */
        static SkString hash(void *input, uint64_t size, SkHashMode mode=SkHashMode::HM_MD5);

        /**
         * @brief Makes hash of a string to a string
         * @param input the string to hash
         * @param mode the hashing mode
         * @return the hash string
         */
        static SkString hash(SkString &input, SkHashMode mode=SkHashMode::HM_MD5);

        /**
         * @brief Makes hash of a data-buffer to a string
         * @param input the data-buffer to hash
         * @param mode the hashing mode
         * @return the hash string
         */
        static SkString hash(SkDataBuffer &input, SkHashMode mode=SkHashMode::HM_MD5);

    private:

#if defined(ENABLE_AES)
        bool enabled;

        SkCryptContext *encryptor;
        SkCryptContext *decryptor;
#endif
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/**
 * @brief Makes binary hash of a raw array data pointer to a buffer
 * @param input the raw array data pointer to hash
 * @param size the size of data
 * @param output the buffer to fill with the binary hash
 * @param mode the hashing mode
 */
void binaryHash(void *buffer, uint64_t size, SkDataBuffer &output, SkHashMode mode=SkHashMode::HM_MD5);

/**
 * @brief Makes hash of a raw array data pointer to a string
 * @param buffer the raw array data pointer to hash
 * @param size the size of data
 * @param mode the hashing mode
 * @return the hash string
 */
SkString bufferHash(void *buffer, uint64_t size, SkHashMode mode=SkHashMode::HM_MD5);

/**
 * @brief Makes hash of a string to a string
 * @param s the string to hash
 * @param mode the hashing mode
 * @return the hash string
 */
SkString stringHash(SkString &s, SkHashMode mode=SkHashMode::HM_MD5);

SkString uniqueID(SkHashMode mode=SkHashMode::HM_MD5, CStr *surplus="");

/**
 * @brief Encodes of a raw array data pointer as Base64 to a data-buffer
 * @param input the raw array data pointer to encode
 * @param size the size of data
 * @param output the data-buffer filled with the Base64 encoded data
 */
void b64enc(void *input, uint64_t size, SkDataBuffer &output);

/**
 * @brief Decodes of a raw array data pointer as Base64 to a data-buffer
 * @param input the raw array data pointer to decode
 * @param size the size of data
 * @param output the data-buffer filled with the Base64 decoded data
 */
void b64dec (void *input, uint64_t size, SkDataBuffer &output);

/**
 * @brief Encodes of a data-buffer as Base64 to a data-buffer
 * @param input the data-buffer to encode
 * @param output the data-buffer filled with the Base64 encoded data
 */
void b64enc(SkDataBuffer &input, SkDataBuffer &output);

/**
 * @brief Decodes of a data-buffer as Base64 to a data-buffer
 * @param input the data-buffer to decode
 * @param output the data-buffer filled with the Base64 decoded data
 */
void b64dec(SkDataBuffer &input, SkDataBuffer &output);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKDATACRYPT_H
