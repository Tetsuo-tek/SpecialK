#include "skcli.h"
#include "Core/sklogmachine.h"
#include "Core/System/Filesystem/skfsutils.h"

SkCli::SkCli(int argc, char *argv[])
{
    setObjectName("CLI");

    SkPathInfo info;
    SkFsUtils::fillPathInfo(argv[0], info);
    executableName = info.completeName;

    for(int i=1; i<argc; i++)
        appCli << argv[i];

    add("--help",               "-h", "",   "Print this output and exit");
    add("--disable-verbose",    "",   "",   "Disable logs on stdout");
    add("--enable-syslog",      "",   "",   "Enable standard messages on SysLog");
}

bool SkCli::add(CStr *label, CStr *abbr, CStr *defaultValue, CStr *desc)
{
    if (label[0] != '-' && label[1] != '-')
    {
        FlatError("Parameter label MUST have '--' prefix: " << label);
        return false;
    }

    if (allowedCli.contains(label))
    {
        FlatError("Parameter label ALREADY exists: " << label);
        return false;
    }

    SkCliParameter *p = new SkCliParameter;

    p->used = false;
    p->label = label;
    p->defaultValue = defaultValue;
    p->desc = desc;

    allowedCli[p->label] = p;

    if (!SkString::isEmpty(abbr))
    {
        if (strlen(abbr) < 2 || abbr[0] != '-' || abbr[1] == '-')
        {
            FlatError("Parameter label-abbreviation MUST have '-' prefix: " << abbr);
            return false;
        }

        p->abbr = abbr;
        allowedCli[p->abbr] = p;
    }

    return true;
}

void SkCli::setValue(CStr *label, CStr *val)
{
    if (!allowedCli.contains(label))
        return;

    SkCliParameter *p = allowedCli[label];
    p->used = true;
    p->value = val;
}

void SkCli::resetValue(CStr *label)
{
    if (!allowedCli.contains(label))
        return;

    SkCliParameter *p = allowedCli[label];
    p->used = false;
    p->value.nullify();
}

bool SkCli::isUsed(CStr *label)
{
    if (!allowedCli.contains(label))
        return false;

    return allowedCli[label]->used;
}

SkVariant &SkCli::value(CStr *label)
{
    if (!allowedCli.contains(label))
    {
        FlatError("Label UNKNOWN: " << label);
        return nv;
    }

    SkCliParameter *p = allowedCli[label];

    if (p->value.isEmpty())
        return p->defaultValue;

    return p->value;
}

SkVariant &SkCli::floatingValue(ULong floatingID)
{
    if (floatingID >= floatingArgs.count())
    {
        FlatError("Floating-index UNKNOWN: " << floatingID);
        return nv;
    }

    return floatingArgs[floatingID];
}

bool SkCli::check()
{
    if (appCli.isEmpty())
        return true;

    for(ULong i=0; i<appCli.count(); i++)
    {
        SkString &label = appCli[i];
        SkString val;

        if (label.startsWith("-"))
        {
            if (i < appCli.count()-1)
            {
                if (appCli[i+1].at(0) != '-')
                    val = appCli[i+1];
            }

            if (allowedCli.contains(label))
            {
                SkCliParameter *p = allowedCli[label];
                p->value = val;
                p->used = true;
            }

            else
            {
                FlatError("UNKONWN cli-argument: " << label);
                return false;
            }
        }

        else
        {
            if (i == 1 || (i < appCli.count() && appCli[i-1].at(0) != '-'))
                floatingArgs << label;
        }
    }

    if (isUsed("--help"))
    {
        logger->enable(false);
        cout << help();
        exit(0);
    }

    else if (isUsed("--enable-syslog"))
        logger->enableSysLog(executableName.c_str());

    return true;
}

SkStringList &SkCli::userAppCli()
{
    return appCli;
}

SkString SkCli::help()
{
    cout << "\033[0m";

    SkString help;

    if (!allowedCli.isEmpty())
    {
        help = "\nHELP command-line arguments\n\n";

        SkBinaryTreeVisit<SkString, SkCliParameter *> *itr = allowedCli.iterator();

        while(itr->next())
        {
            SkString &key = itr->item().key();

            if (!key.startsWith("--"))
                continue;

            SkCliParameter *p = itr->item().value();

            help += p->label;

            if (!p->abbr.isEmpty())
            {
                help += " OR ";
                help += p->abbr;
            }

            // \0 is contained inside Variant for empty string
            if (p->defaultValue.size() > 1)
            {
                help += " [default -> ";
                help += p->defaultValue.toString();
                help += "]";
            }

            help += ":\n";
            help += p->desc;
            help += "\n\n";
        }

        delete itr;

        help += "\n";
    }

    return help;
}

ULong SkCli::floatingParametersCount()
{
    return floatingArgs.count();
}

void SkCli::labels(SkStringList &l)
{
    allowedCli.keys(l);
}
