#include "skosenv.h"

#if defined(SPECIALK_APPLICATION)
    #include "Core/App/skapp.h"
#else
    SkOsEnv *osEnvironment=nullptr;
    StaticAutoLaunch(SkOsEnvInit, osEnvInit)
    {new SkOsEnv;}
#endif

#if !defined(PATH_MAX)
    #define PATH_MAX   1024
#endif

#include "Core/Containers/skstring.h"
#include "Core/Containers/skargsmap.h"
#include "Core/System/Filesystem/skfsutils.h"

#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <limits.h>

extern char **environ;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkOsUser::SkOsUser()
{
    isValid = false;
}

void SkOsUser::toMap(SkArgsMap &map)
{
    if (!isValid)
        return;

    map["uid"] =  uid;
    map["gid"] =  gid;
    map["name"] =  name;
    map["passwd"] =  passwd;
    map["infos"] =  infos;
    map["home"] =  home;
    map["shell"] =  shell;
}

SkOsGroup::SkOsGroup()
{
    isValid = false;
}

void SkOsGroup::toMap(SkArgsMap &map)
{
    if (!isValid)
        return;

    map["gid"] =  gid;
    map["name"] =  name;
    map["passwd"] =  passwd;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkOsEnv::SkOsEnv()
{
    argsCount = 0;
    argsVector = nullptr;
    //nullCounter = 0;
    osEnvironment = this;
}

void SkOsEnv::init(int argc, char *argv[])
{
    envVars.setObjectName(this, "EnvVars");
    //appArguments.getInternalList().setObjectName(this, "AppArguments");
    //cliHelp.getInternalList().setObjectName(this, "CliHelp");
    knownPaths.getInternalVector().setObjectName(this, "KnownPaths");

    argsCount = argc;
    argsVector = argv;

    grabExePath();
    parseOsEnvironments();

    SkString pathsKey("PATH");

    if (envVars.contains(pathsKey))
        parseOsPaths(envVars.value(pathsKey).c_str());

    //parseCommandLine(argc, argv);
}

void SkOsEnv::grabExePath()
{
    SkPathInfo info;
    SkFsUtils::fillPathInfo(argsVector[0], info);
    appPath = info.absoluteParentPath;
    executableName = info.completeName;
}

void SkOsEnv::parseOsEnvironments()
{
    char **tempEnv = environ;

    SkVector<SkString> splitted;
    splitted.setObjectName(this, "tempEnvParse");

    for (; *tempEnv != nullptr; tempEnv++)
    {
        SkString envPair = *tempEnv;
        envPair.parsePair("=", splitted);
        envVars.add(splitted.at(0), splitted.at(1));
        splitted.clear();
        FlatPlusDebug("ENV-PAIR -> " << envPair);
    }
}

void SkOsEnv::parseOsPaths(CStr *paths)
{
    SkString envPair(paths);
    envPair.split(":", knownPaths);
}

/*void SkOsEnv::parseCommandLine(int argc, char *argv[])
{
    if (argc == 1)
        return;

    SkString val;

    for(int i=1; i<argc; i++)
    {
        SkString tempArgument = argv[i];

        if (tempArgument.startsWith("-"))
        {
            val.clear();

            if (i < argc-1)
            {
                if (argv[i+1][0] != '-')
                    val = argv[i+1];
            }

            appArguments[tempArgument] = val;
            val.clear();
        }

        else
        {
            if (i == 1 || (i < argc && argv[i-1][0] != '-'))
            {
                val = tempArgument;
                tempArgument = SkString::number(nullCounter);
                appArguments[tempArgument] = val;
                nullCounter++;
                val.clear();
            }
        }
    }
}*/

/*char **SkOsEnv::getArgsVector()
{
    return argsVector;
}

int SkOsEnv::getArgsCount()
{
    return argsCount;
}

int SkOsEnv::getNullArgsCount()
{
    return nullCounter;
}

bool SkOsEnv::existsAppArgument(CStr *name)
{
    return appArguments.contains(name);
}

SkTreeMap<SkString, SkString> &SkOsEnv::getAppArguments()
{
    return appArguments;
}

void SkOsEnv::getAppArgumentsList(SkStringList &names)
{
    appArguments.keys(names);
}

bool SkOsEnv::removeAppArgument(CStr *name)
{
    return (appArguments.remove(name).value() != appArguments.nv());
}

SkString &SkOsEnv::getAppArgument(CStr *name)
{
    return appArguments.value(name);
}

bool SkOsEnv::checkAllowedAppArguments(SkStringList &acceptedKeys)
{
    SkStringList keys;
    appArguments.keys(keys);

    for(uint64_t i=0; i < keys.count(); i++)
    {
        SkString &key = keys.at(i);

        if (!acceptedKeys.contains(key))
        {
            FlatError("Command-line argument '" + key + "' NOT RECOGNIZED!");
            cout << cmdLineHelp();

            return false;
        }
    }

    return true;
}

void SkOsEnv::addCliAppHelp(CStr *argument, CStr *description)
{
    if (!cliHelp.contains(argument))
        cliHelp[argument] = description;
}

SkString SkOsEnv::cmdLineHelp()
{
    cout << "\033[0m";

    SkString help;

    if (!cliHelp.isEmpty())
    {
        help = "\nHELP command-line arguments\n\n";

        SkStringList keys;
        cliHelp.keys(keys);

        for(uint64_t i=0; i<keys.count(); i++)
        {
            SkString &key = keys.at(i);

            help += " ";
            help += key;
            help += " : ";
            help += cliHelp.value(key);

            if (i < keys.count()-1)
                help += ";\n";

            else
                help += ".\n\n";
        }
    }

    return help.c_str();
}*/

bool SkOsEnv::existsEnvironmentVar(CStr *name)
{
    return envVars.contains(name);
}

SkTreeMap<SkString, SkString> &SkOsEnv::getEnvironment()
{
    return envVars;
}

void SkOsEnv::getEnvironmentVarsList(SkStringList &names)
{
    envVars.keys(names);
}

SkString &SkOsEnv::getEnvironmentVar(CStr *name)
{
    return envVars[name];
}

SkString &SkOsEnv::getExecutableName()
{
    return executableName;
}

SkString &SkOsEnv::getAppPath()
{
    return appPath;
}

SkString SkOsEnv::getWorkingDir()
{
    char currPath[PATH_MAX];
    ::getcwd(currPath, sizeof(currPath));
    return SkFsUtils::adjustPathEndSeparator(currPath);
}

SkStringList &SkOsEnv::getSystemPaths()
{
    return knownPaths;
}

SkString SkOsEnv::getSystemTempPath()
{
    return SkFsUtils::adjustPathEndSeparator(P_tmpdir);
}

pid_t SkOsEnv::currentPID()
{
    return ::getpid();
}

pid_t SkOsEnv::getCurrentParentPID()
{
    return ::getppid();
}

uid_t SkOsEnv::getCurrentUID()
{
    return ::getuid();
}

uid_t SkOsEnv::getCurrentEffectiveUID()
{
    return ::geteuid();
}

gid_t SkOsEnv::getCurrentGID()
{
    return ::getgid();
}

gid_t SkOsEnv::getCurrentEffectiveGID()
{
    return ::getegid();
}

/*struct passwd {
    char   *pw_name;       // username
    char   *pw_passwd;     // user password
    uid_t   pw_uid;        // user ID
    gid_t   pw_gid;        // group ID
    char   *pw_gecos;      // user information
    char   *pw_dir;        // home directory
    char   *pw_shell;      // shell program
};*/

bool SkOsEnv::userInfos(uid_t uid, SkOsUser &props)
{
    struct passwd *pwuser = nullptr;

    if (!(pwuser = ::getpwuid(uid)))
    {
        props.isValid = false;
        return false;
    }

    props.isValid = true;

    props.uid = uid;
    props.gid = pwuser->pw_gid;
    props.name = pwuser->pw_name;
    props.passwd = pwuser->pw_passwd;
    props.infos = pwuser->pw_gecos;
    props.home = pwuser->pw_dir;
    props.shell = pwuser->pw_shell;

    return true;
}

bool SkOsEnv::userInfos(CStr *name, SkOsUser &props)
{
    struct passwd *pwuser = nullptr;

    if (!(pwuser = ::getpwnam(name)))
    {
        props.isValid = false;
        return false;
    }

    props.isValid = true;

    props.uid = pwuser->pw_uid;
    props.gid = pwuser->pw_gid;
    props.name = name;
    props.passwd = pwuser->pw_passwd;
    props.infos = pwuser->pw_gecos;
    props.home = pwuser->pw_dir;
    props.shell = pwuser->pw_shell;

    return true;
}

/*struct group {
    char   *gr_name;       // group name
    char   *gr_passwd;     // group password
    gid_t   gr_gid;        // group ID
    char  **gr_mem;        // group members
};*/

bool SkOsEnv::groupInfos(gid_t gid, SkOsGroup &props)
{
    struct group *grpnam = nullptr;

    if (!(grpnam = ::getgrgid(gid)))
    {
        props.isValid = false;
        return false;
    }

    props.isValid = true;

    props.gid = gid;
    props.name = grpnam->gr_name;
    props.passwd = grpnam->gr_passwd;

    return true;
}

bool SkOsEnv::groupInfos(CStr *name, SkOsGroup &props)
{
    struct group *grpnam = nullptr;

    if (!(grpnam = ::getgrnam(name)))
    {
        props.isValid = false;
        return false;
    }

    props.isValid = true;

    props.gid = grpnam->gr_gid;
    props.name = name;
    props.passwd = grpnam->gr_passwd;

    return true;
}

bool SkOsEnv::groupMembers(gid_t gid, SkStringList &members)
{
    struct group *grpnam;

    if (!(grpnam = ::getgrgid(gid)))
        return false;

    for (uint64_t i=0; grpnam->gr_mem[i] != nullptr; i++)
        members.append(grpnam->gr_mem[i]);

    return true;
}

bool SkOsEnv::groupMembers(CStr *name, SkStringList &members)
{
    struct group *grpnam;

    if (!(grpnam = ::getgrnam(name)))
        return false;

    for (uint64_t i=0; grpnam->gr_mem[i] != nullptr; i++)
        members.append(grpnam->gr_mem[i]);

    return true;
}

