#include "skdatetime.h"
#include "Core/sklogmachine.h"

#include "Core/Containers/skvariant.h"


DeclareWrapper(SkDateTime);

// // // // // // // // // // // // // // // // // // // // //

DeclareMeth_INSTANCE_VOID(SkDateTime, setAsLocal)
DeclareMeth_INSTANCE_RET(SkDateTime, isLocal, bool)
DeclareMeth_INSTANCE_VOID(SkDateTime, setAsUTC)
DeclareMeth_INSTANCE_RET(SkDateTime, isUTC, bool)
DeclareMeth_INSTANCE_VOID(SkDateTime,setToCurrent)
DeclareMeth_INSTANCE_VOID(SkDateTime,setFromUnixTime, Arg_Int64)
DeclareMeth_INSTANCE_VOID(SkDateTime,setYear, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, setMonth, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, setDay, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, setHour, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, setMinute, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, setSecond, Arg_UInt)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumYears, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumMonths, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumDays, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumHours, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumMinutes, Arg_Int)
DeclareMeth_INSTANCE_VOID(SkDateTime, sumSeconds, Arg_Int)
DeclareMeth_INSTANCE_RET(SkDateTime, getYear, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getMonth, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getDayOfTheYear, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getDayOfTheMonth, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getDayOfTheWeek, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getHour, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getMinutes, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, getSeconds, uint)
DeclareMeth_INSTANCE_RET(SkDateTime, isDayLightSavingTime, bool)
DeclareMeth_INSTANCE_RET(SkDateTime, getTimeZone, CStr*)

//DeclareMeth_INSTANCE_RET(SkDateTime, getGmtHoursOffset, long)
//DeclareMeth_INSTANCE_RET(SkDateTime, toUnixTime, time_t)
DeclareMeth_INSTANCE_RET(SkDateTime, toString, SkString, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkDateTime, toISO, SkString)
//DeclareMeth_STATIC_RET(SkDateTime, currentDateTime, SkDateTime, Arg_Bool)
DeclareMeth_STATIC_RET(SkDateTime, currentDateTimeISOString, SkString, Arg_Bool)
//DeclareMeth_STATIC_RET(SkDateTime, currentUnixTime, time_t)

// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkDateTime)
{
    SetClassSuper(SkDateTime, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkDateTime, setAsLocal);
    AddMeth_INSTANCE_RET(SkDateTime, isLocal);
    AddMeth_INSTANCE_VOID(SkDateTime, setAsUTC);
    AddMeth_INSTANCE_RET(SkDateTime, isUTC);
    AddMeth_INSTANCE_VOID(SkDateTime,setToCurrent);
    AddMeth_INSTANCE_VOID(SkDateTime, setFromUnixTime);
    AddMeth_INSTANCE_VOID(SkDateTime,setYear);
    AddMeth_INSTANCE_VOID(SkDateTime, setMonth);
    AddMeth_INSTANCE_VOID(SkDateTime, setDay);
    AddMeth_INSTANCE_VOID(SkDateTime, setHour);
    AddMeth_INSTANCE_VOID(SkDateTime, setMinute);
    AddMeth_INSTANCE_VOID(SkDateTime, setSecond);
    AddMeth_INSTANCE_VOID(SkDateTime, sumYears);
    AddMeth_INSTANCE_VOID(SkDateTime, sumMonths);
    AddMeth_INSTANCE_VOID(SkDateTime, sumDays);
    AddMeth_INSTANCE_VOID(SkDateTime, sumHours);
    AddMeth_INSTANCE_VOID(SkDateTime, sumMinutes);
    AddMeth_INSTANCE_VOID(SkDateTime, sumSeconds);
    AddMeth_INSTANCE_RET(SkDateTime, getYear);
    AddMeth_INSTANCE_RET(SkDateTime, getMonth);
    AddMeth_INSTANCE_RET(SkDateTime, getDayOfTheYear);
    AddMeth_INSTANCE_RET(SkDateTime, getDayOfTheMonth);
    AddMeth_INSTANCE_RET(SkDateTime, getDayOfTheWeek);
    AddMeth_INSTANCE_RET(SkDateTime, getHour);
    AddMeth_INSTANCE_RET(SkDateTime, getMinutes);
    AddMeth_INSTANCE_RET(SkDateTime, getSeconds);
    AddMeth_INSTANCE_RET(SkDateTime, getTimeZone);
    //AddMeth_INSTANCE_RET(SkDateTime, getGmtHoursOffset);
    //AddMeth_INSTANCE_RET(SkDateTime, toUnixTime);
    AddMeth_INSTANCE_RET(SkDateTime, toString);
    AddMeth_INSTANCE_RET(SkDateTime, toISO);
    //AddMeth_STATIC_RET(SkDateTime, currentDateTime);
    AddMeth_STATIC_RET(SkDateTime, currentDateTimeISOString);
    //AddMeth_STATIC_RET(SkDateTime, currentUnixTime);
}

// // // // // // // // // // // // // // // // // // // // //

SkDateTime::SkDateTime(bool asUTC)
{
    CreateClassWrapper(SkDateTime);

    unixTime = 0;
    asUtcTime = asUTC;
    updateTimeRapresentation();
}

void SkDateTime::setAsLocal()
{
    asUtcTime = false;
    updateTimeRapresentation();
}

bool SkDateTime::isLocal()
{
    return !asUtcTime;
}

void SkDateTime::setAsUTC()
{
    asUtcTime = true;
    updateTimeRapresentation();
}

bool SkDateTime::isUTC()
{
    return asUtcTime;
}

void SkDateTime::setToCurrent()
{
    //The Unix time is the number of seconds since the Unix epoch. The time()
    //function returns the value of time in seconds since 0 hours, 0 minutes,
    //0 seconds, January 1, 1970, Coordinated Universal Time.
    //If an error occurs, it returns -1.

    unixTime = ::time(nullptr);
    updateTimeRapresentation();
}

void SkDateTime::setFromUnixTime(time_t t)
{
    unixTime = t;
    updateTimeRapresentation();
}

//seconds after the minute – [0, 61](until C++11) / [0, 60] (since C++11)[note 1]
//minutes after the hour – [0, 59]
//hours since midnight – [0, 23]
//day of the month – [1, 31]
//months since January – [0, 11]

void SkDateTime::setYear(uint value)
{
    if (value < 1970 || value > 2038)
    {
        FlatError("Yera value MUST be in the interval [1970, 2038]");
        return;
    }

    timeInfo.tm_year = static_cast<int>(value)-1900;
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::setMonth(uint value)
{
    if (value < 1 || value > 12)
    {
        FlatError("Mount value MUST be in the interval [1, 12]");
        return;
    }

    timeInfo.tm_mon = static_cast<int>(value) - 1;
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::setDay(uint value)
{
    if (value < 1)
    {
        FlatError("Day of the mount value MUST be in the interval [1, 31]");
        return;
    }

    timeInfo.tm_mday = static_cast<int>(value);
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::setHour(uint value)
{
    if (value > 23)
    {
        FlatError("Hour value MUST be in the interval [0, 23]");
        return;
    }

    timeInfo.tm_hour = static_cast<int>(value);
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::setMinute(uint value)
{
    if (value > 59)
    {
        FlatError("Minute value MUST be in the interval [0, 59]");
        return;
    }

    timeInfo.tm_min = static_cast<int>(value);
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::setSecond(uint value)
{
    if (value > 59)
    {
        FlatError("Second value MUST be in the interval [0, 59]");
        return;
    }

    timeInfo.tm_sec = static_cast<int>(value);
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::sumYears(int value)
{
    timeInfo.tm_year += value;
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::sumMonths(int value)
{
    timeInfo.tm_mon += value;
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::sumDays(int value)
{
    timeInfo.tm_mday += value;
    unixTime = ::mktime(&timeInfo);
}

void SkDateTime::sumHours(int value)
{
    sumMinutes(value*60);
}

void SkDateTime::sumMinutes(int value)
{
    sumSeconds(value*60);
}

void SkDateTime::sumSeconds(int value)
{
    timeInfo.tm_sec += value;
    unixTime = ::mktime(&timeInfo);
}

uint SkDateTime::getYear()
{
    return static_cast<uint>(timeInfo.tm_year);
}

uint SkDateTime::getMonth()
{
    return static_cast<uint>(timeInfo.tm_mon)+1;
}

uint SkDateTime::getDayOfTheYear()
{
    return static_cast<uint>(timeInfo.tm_yday);
}

uint SkDateTime::getDayOfTheMonth()
{
    return static_cast<uint>(timeInfo.tm_mday);
}

uint SkDateTime::getDayOfTheWeek()
{
    return static_cast<uint>(timeInfo.tm_wday);
}

uint SkDateTime::getHour()
{
    return static_cast<uint>(timeInfo.tm_hour);
}

uint SkDateTime::getMinutes()
{
    return static_cast<uint>(timeInfo.tm_min);
}

uint SkDateTime::getSeconds()
{
    return static_cast<uint>(timeInfo.tm_sec);
}

void SkDateTime::updateTimeRapresentation()
{
    if (asUtcTime)
        ::gmtime_r(&unixTime, &timeInfo);

    else
        ::localtime_r(&unixTime, &timeInfo);
}

bool SkDateTime::isDayLightSavingTime()
{
    return (timeInfo.tm_isdst == 1);
}

CStr *SkDateTime::getTimeZone()
{
    return timeInfo.tm_zone;
}

long SkDateTime::getGmtHoursOffset()
{
    return timeInfo.tm_gmtoff/60/60;
}

time_t SkDateTime::toUnixTime()
{
    return unixTime;
}

SkString SkDateTime::toString(CStr *format)
{
    char *text = new char [256];

    SkString s;

    if (format)
    {
        strftime(text, 256, format, &timeInfo);
        s = text;
    }

    else
        s = asctime_r(&timeInfo, text);

    s.trim();
    delete [] text;
    return s;
}

SkString SkDateTime::toISO()
{
    if (asUtcTime)
        return toString("%FT%TZ");

    return toString("%FT%T");
}

SkDateTime SkDateTime::currentDateTime(bool asUTC)
{
    SkDateTime dt(asUTC);
    dt.setToCurrent();
    return dt;
}

SkString SkDateTime::currentDateTimeISOString(bool asUTC)
{
    SkDateTime dt = SkDateTime::currentDateTime();

    if (asUTC)
        dt.setAsUTC();

    return dt.toISO();
    /*SkDateTimeInfo timeInfo;
    time_t unixTime = SkDateTime::currentUnixTime();

    if (asUTC)
        ::gmtime_r(&unixTime, &timeInfo);

    else
        ::localtime_r(&unixTime, &timeInfo);

    char *text = new char [20];
    uint64_t wrote = 0;

    if (asUTC)
        strftime(text, 20, "%FT%TZ", &timeInfo);

    else
        strftime(text, 20, "%FT%T", &timeInfo);

    SkString s;

    if (wrote)
        s = text;

    delete [] text;
    return s;*/
}

time_t SkDateTime::currentUnixTime()
{
    time_t unixTime;
    ::time(&unixTime);
    return unixTime;
}
