#include "skrttimer.h"

/*SK_METATYPE(SkRtTimer)

SkRtTimer::SkRtTimer(SkObject *parent, bool inheriting) : SkObject(parent, true)
{
    enabled = false;

    nsecs = 0;
    startTime = 0;
    virtualTime = 0;
    realTime = 0;
    delta = 0;
    tickNumber = 0;

    ObjSetup(SkRtTimer);
    
}

void SkRtTimer::setInterval(unsigned long tempNsecs)
{
    if (tempNsecs == 0 || tempNsecs > 999999999)
    {
        SK_OBJERR("RtTimer CANNOT setup interval over 1 second")
        return;
    }

    SkMutexLocker locker(&timerEx);
    nsecs = tempNsecs;
    tickNumber = 0;
    delta = 0;

    SK_OBJDBG("Setup timer interval " << tempNsecs << " ns")
}

unsigned long SkRtTimer::getInterval()
{
    return nsecs;
}*/

/*void SkRtTimer::start()
{
    {
        SkMutexLocker locker(&timerEx);

        if (enabled || nsecs == 0)
            return;

        else
            enabled = true;

        tickNumber = 0;
    }

    KTimeSpec temporaryClock;

    while(true)
    {
        {
            timerEx.lock();

            if (!enabled)
            {
                timerEx.unlock();
                break;
            }

            timerEx.unlock();
        }

        tick(&temporaryClock);
    }
}*/

/*#include "Core/App/skapp.h"
void SkRtTimer::tick(KTimeSpec *temporaryClock)
{
    if (!enabled)
        return;

    if (tickNumber == 0)
    {
        clock_gettime(CLOCK_MONOTONIC, temporaryClock);
        startTime = temporaryClock->tv_sec +
                (((double) temporaryClock->tv_nsec) / 1000000000.);
    }

    clock_gettime(CLOCK_MONOTONIC, temporaryClock);

    realTime = temporaryClock->tv_sec +
            (((double) temporaryClock->tv_nsec) / 1000000000.);

    {
        timerEx.lock();
        virtualTime = startTime +
                (((double) tickNumber) * (((double) nsecs) / 1000000000.));

        delta = (realTime - virtualTime) * 1000000000.;

        temporaryClock->tv_nsec = nsecs - (delta);

        tickNumber++;
        timerEx.unlock();
    }

    temporaryClock->tv_sec = 0;
    ::nanosleep(temporaryClock, nullptr);

    timeout();
}

void SkRtTimer::setEnabled(bool value)
{
    SkMutexLocker locker(&timerEx);
    enabled = value;
}

bool SkRtTimer::isActive()
{
    SkMutexLocker locker(&timerEx);
    return enabled;
}*/

/*void SkRtTimer::stop()
{
    SkMutexLocker locker(&timerEx);
    enabled = false;
}*/

