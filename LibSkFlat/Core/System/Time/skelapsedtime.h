/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKELAPSEDTIME_H
#define SKELAPSEDTIME_H

#include "Core/Object/skflatobject.h"
#include "Core/Containers/skstring.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkChrono
{
    uint hours;
    uint minutes;
    uint seconds;
    uint millis;
};

class SkElapsedTime extends SkFlatObject
{
    public:
        SkElapsedTime();

        void start();
        double stop();
        double stop(SkChrono &chrono);
        bool check(double interval);

        SkString toString(bool ms=true);
        static SkString toString(SkChrono &chrono, bool ms=true);

    private:
        SkTimeSpec startTimeStruct;
        double startTime;
        SkTimeSpec endTimeStruct;
        double endTime;
        double elapsedTime;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkElapsedTimeMicros extends SkElapsedTime
{
    public:
        SkElapsedTimeMicros();
        unsigned long stop();
        bool check(unsigned long interval);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkElapsedTimeMillis extends SkElapsedTime
{
    public:
        SkElapsedTimeMillis();
        unsigned long stop();
        bool check(unsigned long interval);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKELAPSEDTIME_H
