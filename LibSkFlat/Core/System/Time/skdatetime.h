/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKDATETIME_H
#define SKDATETIME_H

#include "Core/Containers/skstring.h"
#include "Core/Object/skflatobject.h"

typedef struct tm SkDateTimeInfo;

class SkDateTime extends SkFlatObject
{
    public:
        SkDateTime(bool asUTC=false);

        void setAsLocal();
        bool isLocal();

        void setAsUTC();
        bool isUTC();

        void setToCurrent();
        void setFromUnixTime(time_t t);

        void setYear(uint value);
        void setMonth(uint value);
        void setDay(uint value);
        void setHour(uint value);
        void setMinute(uint value);
        void setSecond(uint value);

        //THESE WILL MAKE ALGEBRIC SUM: WITH NEGATIVE NUMBERs IT WILL MAKE A SUBTRACTION
        void sumYears(int value);
        void sumMonths(int value);
        void sumDays(int value);
        void sumHours(int value);
        void sumMinutes(int value);
        void sumSeconds(int value);

        uint getYear();
        uint getMonth();
        uint getDayOfTheYear();
        uint getDayOfTheMonth();
        uint getDayOfTheWeek();
        uint getHour();
        uint getMinutes();
        uint getSeconds();

        bool isDayLightSavingTime();
        CStr *getTimeZone();
        long getGmtHoursOffset();

        time_t toUnixTime();
        SkString toString(CStr *format=nullptr);//man strftime options
        SkString toISO();

        static SkDateTime currentDateTime(bool asUTC=false);
        static SkString currentDateTimeISOString(bool asUTC=false);
        static time_t currentUnixTime();

    private:
        //DEFAULT IS NOW
        bool asUtcTime;
        time_t unixTime;
        SkDateTimeInfo timeInfo;

        void updateTimeRapresentation();
};

#endif // SKDATETIME_H
