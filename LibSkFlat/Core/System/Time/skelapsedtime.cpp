#include "skelapsedtime.h"
#include <ctime>

// // // // // // // // // // // // // // // // // // // // //

#include "Core/Containers/skvariant.h"

DeclareWrapper(SkElapsedTime);

DeclareMeth_INSTANCE_VOID(SkElapsedTime, start)
DeclareMeth_INSTANCE_RET(SkElapsedTime, stop, double)
DeclareMeth_INSTANCE_RET(SkElapsedTime, check, bool, Arg_Double)

SetupClassWrapper(SkElapsedTime)
{
    SetClassSuper(SkElapsedTime, SkFlatObject);

    AddMeth_INSTANCE_VOID(SkElapsedTime, start);
    AddMeth_INSTANCE_RET(SkElapsedTime, stop);
    AddMeth_INSTANCE_RET(SkElapsedTime, check);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkElapsedTimeMicros);

DeclareMeth_INSTANCE_VOID(SkElapsedTimeMicros, start)
//DeclareMeth_INSTANCE_RET(SkElapsedTimeMicros, stop, ulong)
DeclareMeth_INSTANCE_RET(SkElapsedTimeMicros, check, bool, Arg_UInt64)

SetupClassWrapper(SkElapsedTimeMicros)
{
    SetClassSuper(SkElapsedTimeMicros, SkElapsedTime);

    AddMeth_INSTANCE_VOID(SkElapsedTimeMicros, start);
    //AddMeth_INSTANCE_RET(SkElapsedTimeMicros, stop);
    AddMeth_INSTANCE_RET(SkElapsedTimeMicros, check);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkElapsedTimeMillis);

DeclareMeth_INSTANCE_VOID(SkElapsedTimeMillis, start)
//DeclareMeth_INSTANCE_RET(SkElapsedTimeMillis, stop, ulong)
DeclareMeth_INSTANCE_RET(SkElapsedTimeMillis, check, bool, Arg_UInt64)

SetupClassWrapper(SkElapsedTimeMillis)
{
    SetClassSuper(SkElapsedTimeMillis, SkElapsedTime);

    AddMeth_INSTANCE_VOID(SkElapsedTimeMillis, start);
    //AddMeth_INSTANCE_RET(SkElapsedTimeMillis, stop);
    AddMeth_INSTANCE_RET(SkElapsedTimeMillis, check);
}

// // // // // // // // // // // // // // // // // // // // //

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkElapsedTime::SkElapsedTime()
{
    CreateClassWrapper(SkElapsedTime);

    startTime = 0;
    endTime = 0;
    elapsedTime = 0;

    start();
}

void SkElapsedTime::start()
{
    clock_gettime(CLOCK_MONOTONIC, &startTimeStruct);
    startTime = startTimeStruct.tv_sec + (static_cast<double>(startTimeStruct.tv_nsec) / 1000000000.);
}

double SkElapsedTime::stop()
{
    clock_gettime(CLOCK_MONOTONIC, &endTimeStruct);
    endTime = endTimeStruct.tv_sec + (static_cast<double>(endTimeStruct.tv_nsec) / 1000000000.);
    elapsedTime = endTime - startTime;

    return elapsedTime;
}

double SkElapsedTime::stop(SkChrono &chrono)
{
    stop();

    chrono.seconds = static_cast<uint>(elapsedTime);
    double decimals = elapsedTime - chrono.seconds;
    chrono.minutes = chrono.seconds / 60;
    chrono.seconds %= 60;
    chrono.hours = chrono.minutes / 60;
    chrono.minutes %= 60;
    decimals *= 1000;
    chrono.millis = static_cast<uint>(decimals);

    return elapsedTime;
}

bool SkElapsedTime::check(double interval)
{
    return (SkElapsedTime::stop() >= interval);
}

SkString SkElapsedTime::toString(bool ms)
{
    SkChrono chronoTime;
    stop(chronoTime);

    return SkElapsedTime::toString(chronoTime, ms);
}

SkString SkElapsedTime::toString(SkChrono &chrono, bool ms)
{
    stringstream strStream;

    strStream << setw(2) << setfill('0') << chrono.hours << ":"
              << setw(2) << setfill('0') << chrono.minutes << ":"
              << setw(2) << setfill('0') << chrono.seconds;

    if (ms)
        strStream << "." << setw(3) << setfill('0') << chrono.millis;

    return strStream.str().data();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkElapsedTimeMicros::SkElapsedTimeMicros()
{
    CreateClassWrapper(SkElapsedTimeMicros);
}

unsigned long SkElapsedTimeMicros::stop(){return static_cast<unsigned long>(SkElapsedTime::stop() * 1000000UL);}
bool SkElapsedTimeMicros::check(unsigned long interval){return (stop() * 1000000UL) >= interval;}
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkElapsedTimeMillis::SkElapsedTimeMillis()
{
    CreateClassWrapper(SkElapsedTimeMillis);
}

unsigned long SkElapsedTimeMillis::stop(){return static_cast<unsigned long>(SkElapsedTime::stop() * 1000UL);}
bool SkElapsedTimeMillis::check(unsigned long interval){return (stop() * 1000UL) >= interval;}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
