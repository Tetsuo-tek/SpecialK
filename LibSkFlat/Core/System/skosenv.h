/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKOSENV_H
#define SKOSENV_H

#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skstringlist.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkArgsMap;

struct SkOsUser
{
    bool isValid;

    uid_t uid;
    gid_t gid;

    SkString name;
    SkString passwd;
    SkString infos;
    SkString home;
    SkString shell;

    SkOsUser();
    void toMap(SkArgsMap &map);
};

struct SkOsGroup
{
    bool isValid;

    gid_t gid;

    SkString name;
    SkString passwd;

    SkOsGroup();
    void toMap(SkArgsMap &map);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//HERE WE GRAB OS-SYSTEM INFOs ABOUT CLI, PROCESS, USERs AND GROUPs

class SPECIALK SkOsEnv extends SkFlatObject
{
    SkString appPath;
    SkString executableName;

    SkStringList knownPaths;
    SkTreeMap<SkString, SkString> envVars;
    //SkTreeMap<SkString, SkString> appArguments;
    //SkTreeMap<SkString, SkString> cliHelp;
    //SkTreeMap<SkString, SkCliParameter *> cli;

    //int nullCounter;
    int argsCount;
    char **argsVector;

    public:
        SkOsEnv();

        void init(int argc, char *argv[]);

        /*char **getArgsVector();
        int getArgsCount();
        int getNullArgsCount();
        bool existsAppArgument(CStr *name);
        SkTreeMap<SkString, SkString> &getAppArguments();
        void getAppArgumentsList(SkStringList &names);
        bool removeAppArgument(CStr *name);
        SkString &getAppArgument(CStr *name);
        bool checkAllowedAppArguments(SkStringList &acceptedKeys);
        void addCliAppHelp(CStr *argument, CStr *description);
        SkString cmdLineHelp();*/

        bool existsEnvironmentVar(CStr *name);
        SkTreeMap<SkString, SkString> &getEnvironment();
        void getEnvironmentVarsList(SkStringList &names);
        SkString &getEnvironmentVar(CStr *name);
        SkString &getExecutableName();

        SkString &getAppPath();
        SkString getWorkingDir();

        SkStringList &getSystemPaths();
        SkString getSystemTempPath();

        pid_t currentPID();
        pid_t getCurrentParentPID();
        uid_t getCurrentUID();
        uid_t getCurrentEffectiveUID();
        gid_t getCurrentGID();
        gid_t getCurrentEffectiveGID();

        static bool userInfos(uid_t uid, SkOsUser &props);
        static bool userInfos(CStr *name, SkOsUser &props);
        static bool groupInfos(gid_t gid, SkOsGroup &props);
        static bool groupInfos(CStr *name, SkOsGroup &props);
        static bool groupMembers(gid_t gid, SkStringList &members);
        static bool groupMembers(CStr *name, SkStringList &members);

    private:
        void grabExePath();

        void parseOsEnvironments();
        void parseOsPaths(CStr *paths);

        void parseCommandLine(int argc, char *argv[]);
};

extern SkOsEnv *osEnvironment;

#endif // SKOSENV_H
