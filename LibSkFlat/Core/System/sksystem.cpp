#include "sksystem.h"
#include <unistd.h>

SkSysConf::SkSysConf()
{

}

void SkSysConf::getMemoryInfo(SkRamInfo &info)
{
    info.pages = sysconf(_SC_PHYS_PAGES);
    info.page_size = sysconf(_SC_PAGE_SIZE);

    info.total = info.pages * info.page_size;
    info.free = info.page_size * sysconf(_SC_AVPHYS_PAGES);
}

long SkSysConf::getTickSpeed()
{
    return sysconf(_SC_CLK_TCK);
}

long SkSysConf::getProcessorsCount()
{
    return sysconf(_SC_NPROCESSORS_CONF);
}

long SkSysConf::getOnlineProcessorsCount()
{
    return sysconf(_SC_NPROCESSORS_ONLN);
}
