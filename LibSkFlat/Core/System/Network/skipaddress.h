/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKIPADDRESS_H
#define SKIPADDRESS_H

#include "Core/Containers/skstring.h"

class SPECIALK SkIPAddress extends SkFlatObject
{
    public:
        /**
         * @brief Simple constructor
         */
        SkIPAddress();

        /**
         * @brief Constructor that resolves an hostname as cstring
         * @param hostName the hostname cstring
         */
        SkIPAddress(CStr *hostName);

        /**
         * @brief Constructor that resolves an hostname as string
         * @param hostName the hostname string
         */
        SkIPAddress(SkString &hostName);

        /**
         * @brief Constructor that initialize the IP from an array pointer of 4
         * uint8_t elements
         * @param ipV4Bytes the array pointer with 4 uint8_t elements
         */
        SkIPAddress(uint8_t *ipV4Bytes);

        /**
         * @brief Constructor that initialize the IP from 4 uint8_t values
         * @param byte0 I octect (more significative)
         * @param byte1 II octect
         * @param byte2 III octect
         * @param byte3 iV octect
         */
        SkIPAddress(uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3);

        /**
         * @brief Sets IP from an array pointer of 4 uint8_t elements
         * @param ipV4Bytes the array pointer with 4 uint8_t elements
         */
        void setByIpV4(uint8_t *ipV4Bytes);

        /**
         * @brief Sets IP from an hostName, resolving it
         * @param hostName the hostName cstring
         * @return true whether the address could be resolved, otherwise false
         */
        bool setByHostName(CStr *hostName);

        /**
         * @brief Assign-operator from an array pointer of 4 uint8_t elements
         * @param address the assigning array pointer with 4 uint8_t elements
         * @return this object reference
         */
        SkIPAddress &operator = (const uint8_t *ipV4Bytes);

        /**
         * @brief Equal-operator from an array pointer of 4 uint8_t elements
         * @param ipV4Bytes the comparing array pointer with 4 uint8_t elements
         * @return true if IP addresses are equal, otherwise false
         */
        bool operator == (const uint8_t *ipV4Bytes) const;

        /**
         * @brief Equal-operator from another SkIPAddress reference
         * @param addr the other SkIPAddress
         * @return true if IP addresses are equal, otherwise false
         */
        bool operator == (SkIPAddress &addr) const;

        /**
         * @brief []-operator for read/write on referred value
         * @param index the position to use
         * @return the reference to the reuested value, or 0 if the index
         * is not valid
         */
        uint8_t operator [] (uint64_t index) const;

        /**
         * @brief Tries to make reverse dns lookup on the stored IP
         * @return the hostName reversed
         */
        SkString reverseLookup();

        /**
         * @brief Represents the IP address as a string
         * @return the string representation
         */
        SkString toString();

    private:
        uint8_t bytes[4];
};

#endif // SKIPADDRESS_H

