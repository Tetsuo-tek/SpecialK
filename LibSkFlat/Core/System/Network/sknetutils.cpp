#include "sknetutils.h"
#include "Core/Containers/skstringlist.h"
#include "Core/sklogmachine.h"
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>

/*SkNetUtils::SkNetUtils()
{

}*/
// // // // // // // // // // // // // // // // // // // // //
#include "Core/Containers/skvariant.h"

DeclareWrapper_STATIC(SkNetUtils);

DeclareMeth_STATIC_RET(SkNetUtils, dnsDirectLookup, bool, Arg_CStr, *Arg_Custom(SkIPAddress))
DeclareMeth_STATIC_RET_OVERLOAD(SkNetUtils, dnsDirectLookup, 1, bool, Arg_CStr, *Arg_Custom(SkIPAddress))
DeclareMeth_STATIC_RET(SkNetUtils, dnsReverseLookup, bool, Arg_CStr, Arg_StringRef)

SetupClassWrapper(SkNetUtils)
{
    AddMeth_STATIC_RET(SkNetUtils, dnsDirectLookup);
    AddMeth_STATIC_RET_OVERLOAD(SkNetUtils, dnsDirectLookup, 1);
    AddMeth_STATIC_RET(SkNetUtils, dnsReverseLookup);
}
// // // // // // // // // // // // // // // // // // // // //

bool SkNetUtils::setSocketBlocking(int fd, bool blocking)
{
    if (fd < 0) return false;

    #ifdef _WIN32
       unsigned long mode = blocking ? 0 : 1;
       return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
    #else
       int flags = fcntl(fd, F_GETFL, 0);
       if (flags == -1) return false;
       flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
       return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
    #endif
}

bool SkNetUtils::dnsDirectLookup(CStr *hostName, SkIPAddress &ipV4addr)
{
    uint8_t bytes[4];

    struct hostent *host = gethostbyname(hostName);

    if (!host)
    {
        StaticError("Cannot make DNS-DIRECT-LOOKUP of: " << hostName);
        return false;
    }

    SkString ipStr = inet_ntoa(*((struct in_addr **) host->h_addr_list)[0]);

    SkStringList prsd;
    ipStr.split(".", prsd);

    if (prsd.count() != 4)
        return false;

    bytes[0] = static_cast<uint8_t>(prsd.at(0).toInt());
    bytes[1] = static_cast<uint8_t>(prsd.at(1).toInt());
    bytes[2] = static_cast<uint8_t>(prsd.at(2).toInt());
    bytes[3] = static_cast<uint8_t>(prsd.at(3).toInt());

    ipV4addr.setByIpV4(bytes);

    return true;
}

#define PORT_NO     0

bool SkNetUtils::dnsDirectLookup(CStr *hostName, SkString &ip, SkSckAddr *addr)
{
    struct hostent *host = gethostbyname(hostName);

    if (!host)
    {
        StaticError("Cannot make DNS-DIRECT-LOOKUP of: " << hostName);
        return false;
    }

    ip = inet_ntoa(*((struct in_addr **) host->h_addr_list)[0]);

    if (addr)
    {
        addr->sin_family = static_cast<uint16_t>(host->h_addrtype);
        addr->sin_port = htons(PORT_NO);
        addr->sin_addr.s_addr = * (long*) host->h_addr;
    }

    return true;
}

bool SkNetUtils::dnsReverseLookup(CStr *ipV4addr, SkString &hostName)
{
    struct sockaddr_in temp_addr;
    socklen_t len;
    char buf[NI_MAXHOST];

    temp_addr.sin_family = AF_INET;
    temp_addr.sin_addr.s_addr = inet_addr(ipV4addr);
    len = sizeof(struct sockaddr_in);

    if (getnameinfo((struct sockaddr *) &temp_addr, len, buf, sizeof(buf), nullptr, 0, NI_NAMEREQD))
    {
        StaticError("Cannot make DNS-REVERSE-LOOKUP of: " << ipV4addr);
        return false;
    }

    hostName = buf;
    return true;
}
