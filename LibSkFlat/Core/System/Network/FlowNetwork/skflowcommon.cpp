#include "skflowcommon.h"

static CStr *txtCommands[] =
{
    "NOCMD",

    "FCMD_GET_LOGIN_SEED",
    "LOGIN",

    "SET_ASYNC",
    "CHK_SERVICE",

    "EXISTS_DATABASE",
    "ADD_DATABASE",
    "DEL_DATABASE",
    "LOAD_DATABASE",
    "SAVE_DATABASE",
    "EXISTS_DATABASE_PROPERTY",
    "SET_DATABASE_PROPERTY",
    "SET_DATABASE_PROPERTY_JSON",
    "GET_DATABASE_PROPERTY",
    "GET_DATABASE_PROPERTY_JSON",
    "DEL_DATABASE_PROPERTY",
    "SET_DATABASE",
    "GET_DATABASE",
    "GET_VARIABLES_KEYS",
    "GET_VARIABLES",
    "GET_VARIABLES_JSON",
    "EXISTS_VARIABLE",
    "SET_VARIABLE",
    "SET_VARIABLE_JSON",
    "GET_VARIABLE",
    "GET_VARIABLE_JSON",
    "DEL_VARIABLE",
    "FLUSHALL",

    "GET_CHANS_LIST",
    "GET_CHAN_PROPS",
    "GET_CHAN_HEADER",
    "SET_CHAN_HEADER",

    "ADD_STREAMING_CHAN",
    "ADD_SERVICE_CHAN",
    "ADD_BLOB_CHAN",

    "DEL_CHAN",

    "ATTACH_CHAN",
    "DETACH_CHAN",

    "EXEC_SERVICE_REQUEST",
    "RETURN_SERVICE_RESPONSE",

    "GRAB_REGISTER_CHAN",
    "GRAB_UNREGISTER_CHAN",
    "GRAB_LASTDATA_CHAN",

    "PUBLISH",

    "SUBSCRIBE",
    "UNSUBSCRIBE",

    "ADD_USER",
    "DEL_USER",
    "SET_USER_PERMISSION",
    "GET_USER_PERMISSIONS",

    "KILL_USER",
    "KILL_CONNECTION",

    "QUIT"
};

static CStr *txtResponses[] =
{
    "NORSP",

    "CHK_FLOW",

    "CURRENTDB_CHANGED",

    "CHANNEL_ADDED",
    "CHANNEL_REMOVED",
    "CHANNEL_HEADER",

    "SEND_REQUEST_TO_SERVICE",
    "SEND_RESPONSE_TO_REQUESTER",

    "CHANNEL_PUBLISH_START",
    "CHANNEL_PUBLISH_STOP",

    "SUBSCRIBED_DATA",
    //"SUBSCRIBED_DATA_FRAGMENT",

    "OK",
    "KO"
};

static CStr *txtFlow_t[] =
{
    "BLOB",

    "TICK",
    "EVENTS",
    "PAIRS",
    "LOGS",
    "DATETIME_CLOCK",

    "SYSTEM_PARAMETER",
    "SYSTEM_SHELL",

    "GPIO_PIN",
    "GPIO_SENSOR",

    "CTRL_PULSE",
    "CTRL_SWITCH",
    "CTRL_VALUE",

    "AUDIO_DATA",
    "AUDIO_PREVIEW_DATA",
    "AUDIO_FFT",
    "AUDIO_VUMETER",
    "AUDIO_CLIPPING",

    "SPEECH_TO_TEXT",
    "TEXT_TO_SPEECH",

    "VIDEO_DATA",
    "VIDEO_PREVIEW_DATA",

    "CV_MOVEMENT",
    "CV_EDGES",
    "CV_GESTURES",
    "CV_POSTURE",
    "CV_BONES",
    "CV_CLOUD_3DPOINTS",
    "CV_OBJECT_DETECTED_FRAME",
    "CV_OBJECT_DETECTED_BOX",
    "CV_OBJECT_RECOGNIZED_IDENTITY",

    "MULTIMEDIA_DATA",
    "MULTIMEDIA_PREVIEW_DATA",
    "MULTIMEDIA_CTRL"
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static SkTreeMap<SkString, SkFlowCommand> cmdTxtToBin;
static SkTreeMap<SkString, SkFlowResponse> rspTxtToBin;
static SkTreeMap<SkString, SkFlow_T> flowTxtToBin;

SkTreeMap<SkString, SkFlowCommand> &getCmdTxtToBin()
{return cmdTxtToBin;}

SkTreeMap<SkString, SkFlowResponse> &getRspTxtToBin()
{return rspTxtToBin;}

SkTreeMap<SkString, SkFlow_T> &getFlowTxtToBin()
{return flowTxtToBin;}

CStr **getTxtCommands()
{return txtCommands;}

CStr **getTxtResponses()
{return txtResponses;}

CStr **getTxtFlow_t()
{return txtFlow_t;}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

StaticAutoLaunch(SkFlowProtoInit, protoInit)
{
    int max = FCMD_QUIT+1;

    for(int cmd=0; cmd<max; cmd++)
        cmdTxtToBin[txtCommands[cmd]] = static_cast<SkFlowCommand>(cmd);

    max = FRSP_KO+1;

    for(int rsp=0; rsp<max; rsp++)
        rspTxtToBin[txtResponses[rsp]] = static_cast<SkFlowResponse>(rsp);

    max = FT_MULTIMEDIA_PREVIEW_DATA+1;

    for(int ft=0; ft<max; ft++)
        flowTxtToBin[txtFlow_t[ft]] = static_cast<SkFlow_T>(ft);

    if (cmdTxtToBin.count() != FCMD_QUIT+1)
    {
        cout << "\033[1;31m\n!!!Flow PROTOCOL commands NOT valid! "
             << "[bins: " << (FCMD_QUIT+1) << "; txt: " << cmdTxtToBin.count() <<"]\n\033[0m";
        KillApp();
    }

    if (rspTxtToBin.count() != FRSP_KO+1)
    {
        cout << "\033[1;31m\n!!!Flow PROTOCOL responses NOT valid! "
             << "[bins: " << (FRSP_KO+1) << "; txt: " << rspTxtToBin.count() <<"]\n\033[0m";
        KillApp();
    }

    if (flowTxtToBin.count() != FT_MULTIMEDIA_PREVIEW_DATA+1)
    {
        cout << "\033[1;31m\n!!!Flow_T NOT valid! "
             << "[bins: " << (FT_MULTIMEDIA_PREVIEW_DATA+1) << "; txt: " << flowTxtToBin.count() <<"]\n\033[0m";
        KillApp();
    }
}
