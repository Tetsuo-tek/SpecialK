#ifndef SKFLOWCOMMON_H
#define SKFLOWCOMMON_H

#include "Core/Containers/sktreemap.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

typedef Short SkFlowChanID;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
 AUTH:

    SERVICE KNOWS SHA512 HASHED PASSWORD
    HASHED-PASSWORD = HASH((HASH(seed, SHA512) + HASH(password, SHA512)), SHA512)

 LOGIN TRANS ON SERVICE:

    User                                Service
    ================================    =====================
    -> getSeed()
                                        <- sendSeed(uuid())
    -> login(userName, hashedPasswd)
                                        <- authenticate()
    -> useService()

 TRANS-LOGIN ON SERVICE, THROUGH ANOTHER SAT:

    User                                Sat                                 Service
    ================================    =====================               =====================
    -> getSeed()
                                        -> getSeed()
                                                                            <- sendSeed(uuid())
                                        <- sendSeed()
    -> login(userName, hashedPasswd)
                                        -> login(userName, hashedPasswd)
                                                                            <- authenticate()
                                        <- authenticate()
    -> useSat()
*/

//FROM CLIENT TO THE SERVER
enum SkFlowCommand //<CMD:16>
{
    FCMD_NOCMD,

    FCMD_GET_LOGIN_SEED,                //ONLY SYNC
    FCMD_LOGIN,                         //ONLY SYNC

    FCMD_SET_ASYNC,                     //ONLY SYNC (ASYNC STARTs ALWAYS AS SYNC)
    FCMD_CHK_SERVICE,                   //ONLY ASYNC

    FCMD_EXISTS_DATABASE,               //ONLY SYNC
    FCMD_ADD_DATABASE,                  //ONLY SYNC
    FCMD_DEL_DATABASE,                  //ONLY SYNC
    FCMD_LOAD_DATABASE,                 //ONLY SYNC
    FCMD_SAVE_DATABASE,                 //ONLY SYNC
    FCMD_EXISTS_DATABASE_PROPERTY,      //ONLY SYNC
    FCMD_SET_DATABASE_PROPERTY,         //*
    FCMD_SET_DATABASE_PROPERTY_JSON,    //*
    FCMD_GET_DATABASE_PROPERTY,         //ONLY SYNC
    FCMD_GET_DATABASE_PROPERTY_JSON,    //ONLY SYNC
    FCMD_DEL_DATABASE_PROPERTY,         //*
    FCMD_SET_DATABASE,                  //*
    FCMD_GET_DATABASE,                  //ONLY SYNC
    FCMD_GET_VARIABLES_KEYS,            //ONLY SYNC
    FCMD_GET_VARIABLES,                 //ONLY SYNC
    FCMD_GET_VARIABLES_JSON,            //ONLY SYNC
    FCMD_EXISTS_VARIABLE,               //ONLY SYNC
    FCMD_SET_VARIABLE,                  //*
    FCMD_SET_VARIABLE_JSON,             //*
    FCMD_GET_VARIABLE,                  //ONLY SYNC
    FCMD_GET_VARIABLE_JSON,             //ONLY SYNC
    FCMD_DEL_VARIABLE,                  //*
    FCMD_FLUSHALL,                      //*

    FCMD_GET_CHANS_LIST,                //ONLY SYNC
    FCMD_GET_CHAN_PROPS,                //ONLY SYNC
    FCMD_GET_CHAN_HEADER,               //ONLY SYNC
    FCMD_SET_CHAN_HEADER,               //ONLY ASYNC

    FCMD_ADD_STREAMING_CHAN,            //ONLY ASYNC
    FCMD_ADD_SERVICE_CHAN,              //ONLY ASYNC
    FCMD_ADD_BLOB_CHAN,                 //ONLY ASYNC

    FCMD_DEL_CHAN,                      //ONLY ASYNC

    FCMD_ATTACH_CHAN,                   //ONLY ASYNC
    FCMD_DETACH_CHAN,                   //ONLY ASYNC

    FCMD_EXEC_SERVICE_REQUEST,          //*
    FCMD_RETURN_SERVICE_RESPONSE,       //ONLY_ASYNC

    FCMD_GRAB_REGISTER_CHAN,            //ONLY SYNC
    FCMD_GRAB_UNREGISTER_CHAN,          //ONLY SYNC
    FCMD_GRAB_LASTDATA_CHAN,            //ONLY SYNC

    FCMD_PUBLISH,                       //ONLY ASYNC

    FCMD_SUBSCRIBE_CHAN,                //ONLY ASYNC
    FCMD_UNSUBSCRIBE_CHAN,              //ONLY ASYNC

    FCMD_ADD_USER,                      //ONLY SYNC (ADMIN)
    FCMD_DEL_USER,                      //ONLY SYNC (ADMIN)
    FCMD_SET_USER_PERMISSION,           //ONLY SYNC (ADMIN)
    FCMD_GET_USER_PERMISSIONS,          //ONLY SYNC

    FCMD_KILL_USER,                     //ONLY SYNC (ADMIN)
    FCMD_KILL_CONNECTION,               //ONLY SYNC (ADMIN)

    FCMD_QUIT
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//FROM SERVER TO THE CLIENT
enum SkFlowResponse //<RSP:16>
{
    FRSP_NORSP,

    FRSP_CHK_FLOW,

    //SENT ONLY TO THE CONNECTION THAT CHANGED DB
    FRSP_CURRENTDB_CHANGED,

    //SENT ONLY TO ALL
    FRSP_CHANNEL_ADDED,
    FRSP_CHANNEL_REMOVED,
    FRSP_CHANNEL_HEADER,

    //SENT ONLY TO THE SERVICE
    FRSP_SEND_REQUEST_TO_SERVICE,

    //SENT ONLY TO THE REQUESTER
    FRSP_SEND_RESPONSE_TO_REQUESTER,

    //SENT ONLY TO THE CHANNEL-OWNER
    FRSP_CHANNEL_PUBLISH_START,
    FRSP_CHANNEL_PUBLISH_STOP,

    //SENT ONLY TO ALL SUBSCRIBERS
    FRSP_SUBSCRIBED_DATA,           // ON FRAGMENTED REDISTR, IT IS THE LAST FRAME FOR A DATA-PACK
    //FRSP_SUBSCRIBED_DATA_FRAGMENT,  // DATA-FRAGMENTATION: > 180 kB ON LOCAL; > 900 kB ON TCP

    FRSP_OK,
    FRSP_KO
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkFlowChannel_T
{
    ChannelNotValid,
    StreamingChannel,
    ServiceChannel,
    BlobChannel
};

enum SkFlow_T
{
    FT_BLOB,

    FT_TICK,
    FT_EVENTS,
    FT_PAIRS,
    FT_LOGS,
    FT_DATETIME_CLOCK,

    FT_SYSTEM_PARAMETER,
    FT_SYSTEM_SHELL,

    FT_GPIO_PIN,
    FT_SENSOR,

    FT_CTRL_PULSE,
    FT_CTRL_SWITCH,
    FT_CTRL_VALUE,

    /*FT_GEOMETRY_POINT,
    FT_GEOMETRY_LINE,
    FT_GEOMETRY_TRIANGLE,
    FT_GEOMETRY_RECTANGLE,
    FT_GEOMETRY_POLYLINE,
    FT_GEOMETRY_POLYGON,
    FT_GEOMETRY_CIRCLE,
    FT_GEOMETRY_ELLIPSE,*/

    FT_AUDIO_DATA,
    FT_AUDIO_PREVIEW_DATA,
    FT_AUDIO_FFT,
    FT_AUDIO_VUMETER,
    FT_AUDIO_CLIPPING,

    FT_SPEECH_TO_TEXT,
    FT_TEXT_TO_SPEECH,

    FT_VIDEO_DATA,
    FT_VIDEO_PREVIEW_DATA,

    FT_CV_MOVEMENT,
    FT_CV_EDGES,
    FT_CV_GESTURES,
    FT_CV_POSTURE,
    FT_CV_BONES,

    //
    FT_CV_CLOUD_3DPOINTS,
    FT_CV_OBJECT_DETECTED_FRAME,
    FT_CV_OBJECT_DETECTED_BOX,
    FT_CV_OBJECT_RECOGNIZED_IDENTITY,
    //

    FT_MULTIMEDIA_DATA,
    FT_MULTIMEDIA_PREVIEW_DATA
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkTreeMap<SkString, SkFlowCommand> &getCmdTxtToBin();
SkTreeMap<SkString, SkFlowResponse> &getRspTxtToBin();
SkTreeMap<SkString, SkFlow_T> &getFlowTxtToBin();

CStr **getTxtCommands();
CStr **getTxtFlow_t();
CStr **getTxtResponses();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLOWCOMMON_H
