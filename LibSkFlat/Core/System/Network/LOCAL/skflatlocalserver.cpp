#include "skflatlocalserver.h"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

SkFlatLocalServer::SkFlatLocalServer()
{
    sckPerm.owner = SkFilePermItem::PM_RW_;
    sckPerm.group = SkFilePermItem::PM_NOPERM;
    sckPerm.other = SkFilePermItem::PM_NOPERM;
}

void SkFlatLocalServer::setSocketPermission(SkFilePerm &perm)
{
    sckPerm = perm;
}

bool SkFlatLocalServer::start(CStr *path, uint64_t maxQueuedConnections)
{
    if (svrSckFD != -1)
    {
        notifyError(SkServerError::ServerAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if ((svrSckFD = ::socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    {
        notifyError(SkServerError::CannotCreateServer, __PRETTY_FUNCTION__);
        return false;
    }

    ::unlink(path);

    svr.sun_family = AF_UNIX;
    ::strcpy(svr.sun_path, path);

    socklen_t t = sizeof(sockaddr_un);

    /*************************************************************/
    /* Set socket to be nonblocking. All of the sockets for      */
    /* the incoming connections will also be nonblocking since   */
    /* they will inherit that state from the listening socket.   */
    /*************************************************************/

    int rc = 0; //ioctl(socketFD, FIONBIO, (char *)&on);

    if (rc < 0 || bind(svrSckFD, (const struct sockaddr *) &svr, t) == -1)
    {
        stop();
        notifyError(SkServerError::CannotBind, __PRETTY_FUNCTION__);
        return false;
    }

    svrPath = path;
    SkFsUtils::chmod(svrPath.c_str(), sckPerm);

    bool ok = enableLinistening(maxQueuedConnections);

    if (!ok)
        stop();

    return ok;
}

bool SkFlatLocalServer::tryToAcceptSD(int &sd)
{
    sockaddr_un remote;
    socklen_t t = sizeof(remote);
    return ((sd = ::accept(svrSckFD, (sockaddr *) &remote, &t)) > -1);
}

void SkFlatLocalServer::stop()
{
    if (svrSckFD != -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;

        ::unlink(svrPath.c_str());
        svrPath.clear();
    }
}

CStr *SkFlatLocalServer::getServerPath()
{
    return svrPath.c_str();
}
