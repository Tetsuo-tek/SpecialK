/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKFLATLOCALSERVER_H
#define SKFLATLOCALSERVER_H

#include "Core/System/Network/skabstractflatserver.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "skflatlocalsocket.h"

class SPECIALK SkFlatLocalServer extends SkAbstractFlatServer
{
    public:
        SkFlatLocalServer();

        void setSocketPermission(SkFilePerm &perm);
        bool start(CStr *path, uint64_t maxQueuedConnections);
        void stop()                                                     override;

        CStr *getServerPath();

    private:
        struct sockaddr_un svr;
        SkString svrPath;
        SkFilePerm sckPerm;

    protected:
        bool tryToAcceptSD(int &sd)                                     override;
        void onTick()                                                   override {}
};

#endif // SKFLATLOCALSERVER_H
