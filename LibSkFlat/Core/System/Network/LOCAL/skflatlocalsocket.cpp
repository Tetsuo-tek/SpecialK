#include "skflatlocalsocket.h"
#include <unistd.h>
#include <netdb.h>

SkFlatLocalSocket::SkFlatLocalSocket()
{}

bool SkFlatLocalSocket::connect(CStr *path)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (SkString::isEmpty(path))
    {
        FlatError("Socket-path is NOT valid");
        return false;
    }

    FlatDebug("Connecting to TCP target: " << path);
    int socketFD = ::socket(AF_UNIX, SOCK_STREAM, 0);

    if (socketFD == -1)
    {
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, path);
        return false;
    }

    clnt.sun_family = AF_UNIX;
    strcpy(clnt.sun_path, path);

    bool ok = (::connect(socketFD, (const struct sockaddr *) &clnt, sizeof(sockaddr_un)) == 0);

    if (ok)
    {
        setSocket(socketFD);
        isServerSck = false;
        return true;
    }

    ::close(socketFD);

    notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__, path);
    notifyState(SkDeviceState::Closed);

    return false;
}

bool SkFlatLocalSocket::accept(int sd)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    isServerSck = true;
    return setSocket(sd);
}
