/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKNETUTILS_H
#define SKNETUTILS_H

#include "skipaddress.h"

typedef struct sockaddr_in SkSckAddr;

/**
 * @brief The SkNetUtils class is internally used, but it will not be documented
 * yet because it primordial and incomplete
 */
class SPECIALK SkNetUtils
{
    public:
        //SkNetUtils();

        static bool setSocketBlocking(int fd, bool blocking);

        static bool dnsDirectLookup(CStr *hostName, SkIPAddress &ipV4addr);
        static bool dnsDirectLookup(CStr *hostName, SkString &ip, SkSckAddr *addr=nullptr);
        static bool dnsReverseLookup(CStr *ipV4addr, SkString &hostName);
};

#endif // SKNETUTILS_H
