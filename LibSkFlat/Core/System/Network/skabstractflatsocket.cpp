#include "skabstractflatsocket.h"
#include "Core/Containers/skringbuffer.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include <netdb.h>

// // // // // // // // // // // // // // // // // // // // //

SkAbstractFlatSocket::SkAbstractFlatSocket()
{
    socketFD = -1;
    isServerSck = false;
}

bool SkAbstractFlatSocket::setSocket(int sd)
{
    if (socketFD == -1)
    {
        socketFD = sd;
        notifyState(Open);
        return true;
    }

    notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
    return false;
}

int SkAbstractFlatSocket::socket()
{
    return socketFD;
}

bool SkAbstractFlatSocket::isServerConnection()
{
    return isServerSck;
}

void SkAbstractFlatSocket::close()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    ::close(socketFD);
    socketFD = -1;
    notifyState(Closed);
}

void SkAbstractFlatSocket::disconnect()
{
    close();
}

bool SkAbstractFlatSocket::isConnected()
{
    return isOpen();
}

bool SkAbstractFlatSocket::waitForData(Long bytes)
{
    if (bytes < 1)
        return false;

    char c;
    return read(&c, bytes, true);
}

bool SkAbstractFlatSocket::atEof()
{
    return !isConnected();
}

Long SkAbstractFlatSocket::canReadLine()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    Long lineLength = 0;
    Long sz = bytesAvailable();

    if (sz)
    {
        char *d = new char [sz];

        if (read(d, sz, true))
        {
            for(Long i=0; i<sz; i++)
                if (d[i] == '\n')
                {
                    lineLength = i+1;
                    delete [] d;
                    return lineLength;
                }
        }

        delete [] d;
    }

    return 0;
}

bool SkAbstractFlatSocket::canWrite(Long microseconds)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    fd_set writeSet;
    FD_ZERO(&writeSet);

    struct timeval selectTimeout;
    selectTimeout.tv_sec = 0;
    selectTimeout.tv_usec = microseconds;

    FD_SET(socketFD, &writeSet);

    int activity = select(socketFD+1, nullptr, &writeSet, nullptr, &selectTimeout);

    if (activity < 0)
        return false;

    return FD_ISSET(socketFD , &writeSet);
}

bool SkAbstractFlatSocket::canRead(Long microseconds)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    fd_set readSet;
    FD_ZERO(&readSet);

    struct timeval selectTimeout;
    selectTimeout.tv_sec = 0;
    selectTimeout.tv_usec = microseconds;

    FD_SET(socketFD, &readSet);

    int activity = select(socketFD+1, &readSet, nullptr, nullptr, &selectTimeout);

    if (activity < 0)
        return false;

    return FD_ISSET(socketFD , &readSet);
}

bool SkAbstractFlatSocket::tick()
{
    onTick();

    if (socketFD == -1)
        return false;

    char data;
    int64_t ret;

    //if ((ret = ::write(socketFD, &data, 0)) < 0) //it will disconnect too easly
    if ((ret = ::recv(socketFD, &data, 1, MSG_PEEK | MSG_DONTWAIT)) == 0 && (errno != EAGAIN ))//&& errno != EWOULDBLOCK)))
    {
        disconnect();
        return false;
    }

    return true;
}

Long SkAbstractFlatSocket::bytesAvailable()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    Long count;

    if (ioctl(socketFD, FIONREAD, &count) < 0)
    {
        disconnect();
        return 0;
    }

    return count;
}

bool SkAbstractFlatSocket::flush()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return flushWriteBuffer() && flushReadBuffer();
}

bool SkAbstractFlatSocket::flushReadBuffer()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    Long sz = bytesAvailable();

    if (sz == 0)
        return false;

    char *fakeBuffer = new char [sz];
    bool ok = read(fakeBuffer, sz);
    delete [] fakeBuffer;
    return ok;
}

bool SkAbstractFlatSocket::flushWriteBuffer()
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return (fsync(socketFD) != 0);
}


bool SkAbstractFlatSocket::readInternal(char *data, Long &len, bool peek)
{
    if (!isConnected())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (len == 0)
        len = bytesAvailable();

    int flags = 0;

    if (peek)
        flags = MSG_PEEK | MSG_NOSIGNAL;

    else
        flags = MSG_NOSIGNAL;

    //* could be too large to read
    //* 'readSize' could be less than total size to write
    Long readSize = recv(socketFD, data, len, flags);

    if (readSize > 0)
        len = readSize;

    else
    {
        len = 0;
        disconnect();
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__, "recv error");
    }

    if (len > 0)
    {
        notifyRead(len);
        return true;
    }

    return false;
}

bool SkAbstractFlatSocket::writeInternal(CStr *data, Long len)
{
    if (!isConnected())
    {
        //notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    //* could be too large to read
    //* 'sentSize' could be less than total size to write
    Long sentSize = send(socketFD, data, len, MSG_NOSIGNAL);

    if (sentSize < 0)
    {
        disconnect();
        return false;
    }

    notifyWrite(sentSize);
    return true;
}

