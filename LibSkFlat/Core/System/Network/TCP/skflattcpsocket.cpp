#include "skflattcpsocket.h"

#include <unistd.h>
#include <sys/ioctl.h>
#include <netdb.h>

SkFlatTcpSocket::SkFlatTcpSocket()
{
    host = nullptr;
}

bool SkFlatTcpSocket::connect(CStr *hostName, UShort port)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (SkString::isEmpty(hostName))
    {
        FlatError("HostName is NOT valid");
        return false;
    }

    clnt.sin_family = AF_INET;
    clnt.sin_port = htons(port);

    host = gethostbyname(hostName);
    struct in_addr **addr_list = (struct in_addr **) host->h_addr_list;

    SkString addrNameStr(host->h_name);

    if (addr_list[0])
        peerAddrIP = inet_ntoa(*addr_list[0]);

    else
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    FlatDebug("Connecting to: " << addrNameStr << ":" << port << " [" << peerAddrIP << "]");

    if (!host)
    {
        notifyError(SkDeviceError::TargetNotValid, __PRETTY_FUNCTION__);
        return false;
    }

    int socketFD = ::socket(AF_INET, SOCK_STREAM, 0);

    if (socketFD == -1)
    {
        notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
        return false;
    }

    bcopy(host->h_addr, &clnt.sin_addr, static_cast<uint64_t>(host->h_length));
    bool ok = (::connect(socketFD, (const struct sockaddr *) &clnt, sizeof(sockaddr_in)) == 0);

    if (ok)
    {
        hostNameStr = hostName;
        setSocket(socketFD);
        isServerSck = false;
        return true;
    }

    ::close(socketFD);

    notifyError(SkDeviceError::CannotOpen, __PRETTY_FUNCTION__);
    notifyState(SkDeviceState::Closed);

    return false;
}

bool SkFlatTcpSocket::accept(int sd)
{
    if (isConnected())
    {
        notifyError(SkDeviceError::DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    socklen_t addr_size = sizeof(struct sockaddr_in);

    if (getpeername(socketFD, (struct sockaddr *) &clnt, &addr_size) != 0)
    {
        FlatError("Cannot grab peer-address from tcp-socket");
        return false;
    }

    peerAddrIP = ::inet_ntoa(clnt.sin_addr);

    FlatDebug("Grabbed peer address: " << peerAddrIP);
    isServerSck = true;

    return setSocket(sd);
}

CStr *SkFlatTcpSocket::peerAddress()
{
    return peerAddrIP.c_str();
}

CStr *SkFlatTcpSocket::peerHostName()
{
    return hostNameStr.c_str();
}

