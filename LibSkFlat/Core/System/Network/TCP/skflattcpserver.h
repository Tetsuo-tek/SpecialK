#ifndef SKFLATTCPSERVER_H
#define SKFLATTCPSERVER_H

#include "Core/System/Network/skabstractflatserver.h"
#include <arpa/inet.h>

class SkFlatTcpServer extends SkAbstractFlatServer
{
    struct sockaddr_in svr;
    SkString svrAddress;
    UShort svrPort;

    public:
        SkFlatTcpServer();

        bool start(CStr *address, uint16_t port, ULong maxQueuedConnections);
        void stop()                                                     override;

        CStr *address();
        uint16_t port();

    private:
        bool tryToAcceptSD(int &sd)                                     override;
        void onTick()                                                   override {}
};

#endif // SKFLATTCPSERVER_H
