#include "skflattcpserver.h"

#include <unistd.h>
#include <netinet/in.h>

SkFlatTcpServer::SkFlatTcpServer()
{
    svrPort = 0;
}

bool SkFlatTcpServer::start(CStr *address, uint16_t port, ULong maxQueuedConnections)
{
    if (isListening())
    {
        notifyError(SkServerError::ServerAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if ((svrSckFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        notifyError(SkServerError::CannotCreateServer, __PRETTY_FUNCTION__);
        return false;
    }

    svr.sin_family = AF_INET;
    svrAddress = address;

    if (address && memcmp(address, "0.0.0.0", strlen(address)) != 0)
        svr.sin_addr.s_addr = inet_addr(address);

    else
        svr.sin_addr.s_addr = htonl(INADDR_ANY);

    svrPort = port;
    svr.sin_port = htons(svrPort);

    socklen_t t = sizeof(sockaddr_in);

    int a = 1;

    if (::setsockopt(svrSckFD, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int)) == -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
        svrAddress.clear();
        return false;
    }

    if (::bind(svrSckFD, (const struct sockaddr *) &svr, t) == -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
        svrAddress.clear();

        notifyError(SkServerError::CannotBind, __PRETTY_FUNCTION__);
        return false;
    }

    bool ok = enableLinistening(maxQueuedConnections);

    if (!ok)
    {
        ::close(svrSckFD);

        svrSckFD = -1;
        svrAddress.clear();
    }

    return ok;
}

void SkFlatTcpServer::stop()
{
    if (svrSckFD != -1)
    {
        ::close(svrSckFD);
        svrSckFD = -1;
    }
}

bool SkFlatTcpServer::tryToAcceptSD(int &sd)
{
    sockaddr_in remote;
    socklen_t t = sizeof(remote);
    return ((sd = accept(svrSckFD, (sockaddr *) &remote, &t)) > -1);
}

CStr *SkFlatTcpServer::address()
{
    return svrAddress.c_str();
}

uint16_t SkFlatTcpServer::port()
{
    return svrPort;
}
