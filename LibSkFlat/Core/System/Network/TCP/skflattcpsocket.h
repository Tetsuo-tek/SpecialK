#ifndef SKFLATTCPSOCKET_H
#define SKFLATTCPSOCKET_H

#include "Core/System/Network/skabstractflatsocket.h"
#include <arpa/inet.h>

class SkFlatTcpSocket extends SkAbstractFlatSocket
{
    SkString hostNameStr;
    SkString peerAddrIP;
    struct sockaddr_in clnt;
    struct hostent *host;

    public:
        SkFlatTcpSocket();

        bool connect(CStr *hostName, UShort port);
        bool accept(int sd)                                             override;

        CStr *peerAddress();
        CStr *peerHostName();
};

#endif // SKFLATTCPSOCKET_H
