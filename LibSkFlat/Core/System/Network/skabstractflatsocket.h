/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTFLATSOCKET_H
#define SKABSTRACTFLATSOCKET_H

#include "Core/System/skabstractflatdevice.h"

class SPECIALK SkAbstractFlatSocket extends SkAbstractFlatDevice
{

    public:
        int socket();
        void disconnect();

        virtual bool canWrite(Long microseconds=10000);
        virtual bool canRead(Long microseconds=10000);
        virtual bool flushReadBuffer();
        virtual bool flushWriteBuffer();
        virtual bool waitForData(Long bytes=1);
        virtual bool isConnected();
        virtual bool accept(int)                        =0;
        virtual bool tick();

        Long pos()                                      override {return -1;}
        bool rewind()                                   override {return false;}
        bool seek(Long)                                 override {return false;}
        bool atEof()                                    override;
        Long canReadLine()                              override;
        bool flush()                                    override;
        Long size()                                     override {return -1;}
        Long bytesAvailable()                           override;
        void close()                                    override;

        bool isServerConnection();

    protected:
        int socketFD;
        bool isServerSck;

        SkAbstractFlatSocket();

        bool setSocket(int sd);
        void unSetSocket();

        bool readInternal(char *data, Long &len, bool peek=false)   override;
        bool writeInternal(CStr *data, Long len)                    override;

        virtual void onTick()                           =0;
};

#endif // SKABSTRACTFLATSOCKET_H
