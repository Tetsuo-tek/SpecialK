#include "skipaddress.h"
#include "sknetutils.h"

#include "Core/Containers/skvariant.h"

DeclareWrapper(SkIPAddress);

// // // // // // // // // // // // // // // // // // // // //

//DeclareMeth_INSTANCE_VOID(SkIPAddress, setByIpV4, Arg_UInt8)
DeclareMeth_INSTANCE_RET(SkIPAddress, setByHostName, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkIPAddress, reverseLookup, SkString)
DeclareMeth_INSTANCE_RET(SkIPAddress, toString, SkString)


// // // // // // // // // // // // // // // // // // // // //

SetupClassWrapper(SkIPAddress)
{
    SetClassSuper(SkIPAddress, SkFlatObject);

    //AddMeth_INSTANCE_VOID(SkIPAddress, setByIpV4);
    AddMeth_INSTANCE_RET(SkIPAddress, setByHostName);
    AddMeth_INSTANCE_RET(SkIPAddress, reverseLookup);
    AddMeth_INSTANCE_RET(SkIPAddress, toString);
}

// // // // // // // // // // // // // // // // // // // // //

SkIPAddress::SkIPAddress()
{
    CreateClassWrapper(SkIPAddress);

    memset(bytes, 0, 4);
}

SkIPAddress::SkIPAddress(CStr *hostName)
{
    CreateClassWrapper(SkIPAddress);

    setByHostName(hostName);
}

SkIPAddress::SkIPAddress(uint8_t *ipV4Bytes)
{
    CreateClassWrapper(SkIPAddress);

    memcpy(bytes, ipV4Bytes, 4);
}

SkIPAddress::SkIPAddress(uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3)
{
    CreateClassWrapper(SkIPAddress);

    bytes[0] = byte0;
    bytes[1] = byte1;
    bytes[2] = byte2;
    bytes[3] = byte3;
}

void SkIPAddress::setByIpV4(uint8_t *ipV4Bytes)
{
    memcpy(bytes, ipV4Bytes, 4);
}

bool SkIPAddress::setByHostName(CStr *hostName)
{
    return SkNetUtils::dnsDirectLookup(hostName, *this);
}

SkIPAddress& SkIPAddress::operator = (const uint8_t *ipV4Bytes)
{
    memcpy(bytes, ipV4Bytes, 4);
    return *this;
}

bool SkIPAddress::operator == (const uint8_t *ipV4Bytes) const
{
    return (memcmp(ipV4Bytes, bytes, 4) == 0);
}

bool SkIPAddress::operator == (SkIPAddress &addr) const
{
    return (memcmp(addr.bytes, bytes, 4) == 0);
}

uint8_t SkIPAddress::operator [] (uint64_t index) const
{
    if (index > 3)
        return 0;

    return bytes[index];
}

SkString SkIPAddress::reverseLookup()
{
    SkString ip = toString();
    SkString h;
    SkNetUtils::dnsReverseLookup(ip.c_str(), h);
    return h;
}

SkString SkIPAddress::toString()
{
    SkString tempIP;

    tempIP.concat(bytes[0], 10);
    tempIP.concat('.');
    tempIP.concat(bytes[1], 10);
    tempIP.concat('.');
    tempIP.concat(bytes[2], 10);
    tempIP.concat('.');
    tempIP.concat(bytes[3], 10);

    return tempIP;
}
