#include "skabstractflatserver.h"

#include <unistd.h>
#include <sys/fcntl.h>
#include <netdb.h>

static CStr *stateNames[] = {
    "Listening",
    "Closed"};

static CStr *errorNames[] = {
    "NoError on server",
    "Cannot create server",
    "Cannot bind",
    "Cannot listen",
    "Server is ALREADY open",
    "Target is NOT valid"
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractFlatServer::SkAbstractFlatServer()
{
    svrSckFD = -1;
    maxQueuedSD = 1;

    err = SkServerError::NoServerError;
    state = SkDeviceState::Closed;
}

SkAbstractFlatServer::~SkAbstractFlatServer()
{
    if (isListening())
        stop();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlatServer::enableLinistening(uint64_t sdQueueMax)
{
    if (sdQueueMax < 1)
        sdQueueMax = 10;

    maxQueuedSD = sdQueueMax;

    if (listen(svrSckFD, static_cast<int>(maxQueuedSD)) == -1)
    {
        notifyError(CannotListen, __PRETTY_FUNCTION__);
        return false;
    }

    int flags = ::fcntl(svrSckFD, F_GETFL);

    if (flags < 0)
    {
        notifyError(CannotGetSckParameter, __PRETTY_FUNCTION__);
        return false;
    }

    if (fcntl(svrSckFD, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        notifyError(CannotSetSckParameter, __PRETTY_FUNCTION__);
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlatServer::tick()
{
    onTick();

    if (state != Open)
        return false;

    int newClient = -1;

    while (tryToAcceptSD(newClient))
    {
        if (sdQueue.count() > maxQueuedSD)
        {
            FlatWarning("Killing SD cached [MaxQueuedSD: " << maxQueuedSD <<  "]: " << newClient);
            ::close(newClient);
        }

        else
            sdQueue.enqueue(newClient);
    }

    return sdQueue.isEmpty();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlatServer::dequeueSD(int &sd)
{
    if (sdQueue.isEmpty())
    {
        sd = -1;
        return false;
    }

    sd = sdQueue.dequeue();
    return true;
}

bool SkAbstractFlatServer::hasQueuedSockets()
{
    return !sdQueue.isEmpty();
}

uint64_t SkAbstractFlatServer::queuedSocketsCount()
{
    return sdQueue.count();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkAbstractFlatServer::isListening()
{
    return (state == SkDeviceState::Open);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkDeviceState SkAbstractFlatServer::currentState()
{
    return state;
}

CStr *SkAbstractFlatServer::currentStateString()
{
    return stateNames[state];
}

SkServerError SkAbstractFlatServer::lastError()
{
    return err;
}

CStr *SkAbstractFlatServer::lastErrorString()
{
    return errorNames[err];
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkAbstractFlatServer::notifyState(SkDeviceState st)
{
    state = st;
    FlatDebug("STATE changed to: " << currentStateString());
}

void SkAbstractFlatServer::notifyError(SkServerError e, CStr *prettyFrom, CStr *comment)
{
    err = e;

    if (comment)
        FlatError(lastErrorString() << ": " << comment << " [from: " << prettyFrom << "]");
    else
        FlatError(lastErrorString() << " [from: " << prettyFrom << "]");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


