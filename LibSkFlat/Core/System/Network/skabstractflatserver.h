/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKABSTRACTFLATSERVER_H
#define SKABSTRACTFLATSERVER_H

#include "skabstractflatsocket.h"

#include "Core/Containers/skqueue.h"

enum SkServerError
{
    NoServerError,
    CannotCreateServer,
    CannotBind,
    CannotListen,
    CannotGetSckParameter,
    CannotSetSckParameter,
    ServerAlreadyOpen,
    ServerNotValid
};

class SkAbstractFlatSocket;

class SPECIALK SkAbstractFlatServer extends SkFlatObject
{
    SkDeviceState state;
    SkServerError err;

    ULong maxQueuedSD;
    SkQueue<int> sdQueue;

    public:
        virtual void stop()                                     {}
        virtual bool tick();

        bool hasQueuedSockets();
        uint64_t queuedSocketsCount();
        bool dequeueSD(int &sd);

        bool isListening();

        SkDeviceState currentState();
        CStr *currentStateString();

        SkServerError lastError();
        CStr *lastErrorString();

    protected:
        int svrSckFD;

        SkAbstractFlatServer();
        ~SkAbstractFlatServer();

        bool enableLinistening(uint64_t sdQueueMax);

        void notifyState(SkDeviceState st);
        void notifyError(SkServerError e, CStr *prettyFrom, CStr *comment=nullptr);

        virtual bool tryToAcceptSD(int &)                       =0;
        virtual void onTick()                                   =0;
};

#endif // SKABSTRACTFLATSERVER_H
