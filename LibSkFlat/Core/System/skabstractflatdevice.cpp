#include "skabstractflatdevice.h"

#include "Core/Containers/skarraycast.h"
#include "Core/Containers/skringbuffer.h"
#include "Core/Containers/skdatabuffer.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define CASTEDVALUES_DEF(SUFFIX, TYPE) \
bool SkAbstractFlatDevice::write ## SUFFIX (TYPE val) \
{ \
        TYPE dataVal[1]; \
        dataVal[0] = val; \
        return write(SkArrayCast::toChar(dataVal), sizeof(TYPE)); \
} \
\
TYPE SkAbstractFlatDevice::read ## SUFFIX (bool peek) \
{ \
        TYPE dataVal[1]; \
        dataVal[0] = 0; \
\
        Long sz = sizeof(TYPE);\
        if (read(SkArrayCast::toChar(dataVal), sz, peek)) \
            return dataVal[0]; \
\
        return 0; \
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static CStr *stateNames[] = {
    "Open",
    "Closed"};

static CStr *errorNames[] = {
    "NoError on device",
    "Cannot open device",
    "Cannot seek device",
    "Cannot read from device",
    "Cannot write on device",
    "Device is already open",
    "Cannot use closed device",
    "Target is not valid",
    "Device internal error"
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkAbstractFlatDevice::SkAbstractFlatDevice()
{
    state = SkDeviceState::Closed;
    txCounter = 0;
    rxCounter = 0;
}

SkAbstractFlatDevice::~SkAbstractFlatDevice()
{
    if (isOpen())
        close();
}

bool SkAbstractFlatDevice::readFromWriteHere(SkAbstractFlatDevice *dev, Long &len)
{
    if (len == 0)
        len = dev->bytesAvailable();

    if (len == 0)
    {
        FlatError("There are NOT data to read");
        return false;
    }

    char *b = new char [len];
    bool ok = dev->read(b, len);

    if (ok)
        ok = write(b, len);

    delete [] b;
    return ok;
}

bool SkAbstractFlatDevice::readHereWriteTo(SkAbstractFlatDevice *dev, Long len)
{
    if (len == 0)
        len = bytesAvailable();

    if (len == 0)
    {
        FlatError("There are NOT data to read");
        return false;
    }

    char *b = new char [len];
    bool ok = read(b, len);

    if (ok)
        ok = dev->write(b, len);

    delete [] b;
    return ok;
}

bool SkAbstractFlatDevice::read(char &c, bool peek)
{
    Long sz = 1;
    return readInternal(&c, sz, peek);
}

bool SkAbstractFlatDevice::read(char *data, Long &len, bool peek)
{
    return readInternal(data, len, peek);
}

bool SkAbstractFlatDevice::read(SkString &s, Long len, bool peek)
{
    Long sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    if (len == 0)
    {
        FlatError("There are NOT data to read");
        return false;
    }

    char *d = new char [len];
    bool ok = this->readInternal(d, len, peek);

    if (ok)
        s.append(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractFlatDevice::read(SkDataBuffer *buf, Long len, bool peek)
{
    Long sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    if (len == 0)
    {
        FlatError("There are NOT data to read");
        return false;
    }

    char *d = new char [len];
    bool ok = this->readInternal(d, len, peek);

    if (ok)
        buf->append(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractFlatDevice::read(SkRingBuffer *buf, Long len, bool peek)
{
    Long sz = bytesAvailable();

    if (len > sz || len == 0)
        len = sz;

    if (len == 0)
    {
        FlatError("There are NOT data to read");
        return false;
    }

    char *d = new char [len];
    bool ok = readInternal(d, len, peek);

    if (ok)
        buf->addData(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractFlatDevice::readAll(SkString &s, bool peek)
{
    int64_t lastPos = pos();

    if (lastPos > -1)
        seek(0);

    Long len = 0;
    bool ok = read(s, len, peek);

    if (lastPos > -1)
        seek(lastPos);

    return ok;
}

bool SkAbstractFlatDevice::readAll(SkDataBuffer *buf, bool peek)
{
    int64_t lastPos = pos();

    if (lastPos > -1)
        seek(0);

    Long len = 0;
    bool ok = read(buf, len, peek);

    if (lastPos > -1)
        seek(lastPos);

    return ok;
}

bool SkAbstractFlatDevice::readAll(SkRingBuffer *buf, bool peek)
{
    int64_t lastPos = pos();

    if (lastPos > -1)
        seek(0);

    Long len = 0;
    bool ok = read(buf, len, peek);

    if (lastPos > -1)
        seek(lastPos);

    return ok;
}

bool SkAbstractFlatDevice::write(char c)
{
    return writeInternal(&c, 1);
}

bool SkAbstractFlatDevice::write(SkString &s)
{
    if (s.isEmpty())
    {
        FlatError("You are writing nothing to the device, from String");
        return false;
    }

    Long sz = s.size();
    return writeInternal(s.c_str(), sz);
}

bool SkAbstractFlatDevice::write(CStr *data, Long len)
{
    return writeInternal(data, len);
}

bool SkAbstractFlatDevice::write(SkDataBuffer *buf, Long len)
{
    if (len == 0)
        len = buf->size();

    if (len == 0)
    {
        FlatError("You are writing nothing to the device, from DataBuffer");
        return false;
    }

    char *d = new char [len];
    bool ok = buf->copyTo(d, len);

    if (ok)
        writeInternal(d, len);

    delete [] d;
    return ok;
}

bool SkAbstractFlatDevice::write(SkRingBuffer *buf, Long len)
{
    if (len == 0)
        len = buf->size();

    if (len == 0)
    {
        FlatError("You are writing nothing to the device, from RingBuffer");
        return false;
    }

    char *d = new char [len];
    ULong sz = len;
    bool ok = buf->getCustomBuffer(d, sz);

    if (ok)
        writeInternal(d, len);

    delete [] d;
    return ok;
}

CASTEDVALUES_DEF(UInt8, uint8_t)
CASTEDVALUES_DEF(Int8, int8_t)
CASTEDVALUES_DEF(UInt16, uint16_t)
CASTEDVALUES_DEF(Int16, int16_t)
CASTEDVALUES_DEF(UInt32, uint32_t)
CASTEDVALUES_DEF(Int32, int32_t)
CASTEDVALUES_DEF(UInt64, uint64_t)
CASTEDVALUES_DEF(Int64, int64_t)
CASTEDVALUES_DEF(Float, float)
CASTEDVALUES_DEF(Double, double)

uint64_t SkAbstractFlatDevice::getTxDataCounter()
{
    return txCounter;
}

void SkAbstractFlatDevice::resetTxDataCounter()
{
    txCounter = 0;
}

uint64_t SkAbstractFlatDevice::getRxDataCounter()
{
    return rxCounter;
}

void SkAbstractFlatDevice::resetRxDataCounter()
{    
    rxCounter = 0;
}

void SkAbstractFlatDevice::resetAllDataCounters()
{
    resetRxDataCounter();
    resetTxDataCounter();
}

bool SkAbstractFlatDevice::isOpen()
{
    return (state == SkDeviceState::Open);
}

SkDeviceState SkAbstractFlatDevice::currentState()
{
    return state;
}

CStr *SkAbstractFlatDevice::currentStateString()
{
    return stateNames[state];
}

SkDeviceError SkAbstractFlatDevice::lastError()
{
    return err;
}

CStr *SkAbstractFlatDevice::lastErrorString()
{
    return errorNames[err];
}

void SkAbstractFlatDevice::notifyRead(Long sz)
{
    rxCounter += sz;
}

void SkAbstractFlatDevice::notifyWrite(Long sz)
{
    txCounter += sz;
}

void SkAbstractFlatDevice::notifyState(SkDeviceState st)
{
    state = st;
    FlatDebug("STATE changed to: " << currentStateString());
}

void SkAbstractFlatDevice::notifyError(SkDeviceError e, CStr *prettyFrom, CStr *comment)
{
    err = e;

    if (comment)
        FlatError(lastErrorString() << ": " << comment << " [from: " << prettyFrom << "]");
    else
        FlatError(lastErrorString() << " [from: " << prettyFrom << "]");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

