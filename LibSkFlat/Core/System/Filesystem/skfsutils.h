/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKFSUTILS_H
#define SKFSUTILS_H

#include "Core/System/skdatacrypt.h"
#include "Core/System/skosenv.h"

#if defined(SPECIALK_APPLICATION)
    class SkFileInfosList;

#else
    #include "skflatfile.h"

#endif

#define SK_PATH_SEPARATOR        "/"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkArgsMap;
class SkVariantList;

struct SPECIALK SkPathInfo
{
    bool exists;
    SkString completeAbsolutePath;
    SkString absoluteParentPath;
    SkString completeName;
    SkString name;
    SkString suffix;

    SkPathInfo();
    void toMap(SkArgsMap &map);
    SkString toString(bool indented=false, uint8_t spaces=4);
};

struct SPECIALK SkFileInfo extends SkPathInfo
{
    int64_t size;

    SkString mimeType;

    bool isDir;
    bool isFile;
    bool isRegularFile;
    bool isSymLink;
    SkString symLinkTarget;
    bool isFifo;
    bool isCharDevice;
    bool isBlockDevice;
    bool isSocket;
    bool isHidden;
    bool isReadable;
    bool isWritable;
    bool isExecutable;

    uint permRawMode;
    SkString permissions;

    SkOsUser user;
    SkOsGroup group;

    SkString created;
    SkString lastModified;
    SkString lastRead;

    SkString checkSum;

    dev_t device;
    dev_t serialNO;
    nlink_t hLinks;
    blksize_t blockSize;
    blkcnt_t blocks;

    SkFileInfo();
    void toMap(SkArgsMap &map);
    SkString toString(bool indented=false, uint8_t spaces=4);
};

/*
    0: No permissions ("-")
    1: Esecuzione ("--x")
    2: Scrittura ("-w-")
    3: Scrittura ed esecuzione ("-wx")
    4: Lettura ("r--")
    5: Lettura ed esecuzione ("r-x")
    6: Lettura e scrittura ("rw-")
    7: Lettura, scrittura ed esecuzione ("rwx")
*/

enum SkFilePermItem
{
    PM_NOPERM,
    PM___X,
    PM__W_,
    PM__WX,
    PM_R__,
    PM_R_X,
    PM_RW_,
    PM_RWX
};

struct SPECIALK SkFilePerm
{
    SkFilePermItem owner;
    SkFilePermItem group;
    SkFilePermItem other;

    SkFilePerm();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

typedef struct stat SkInodeStat;

class SPECIALK SkFsUtils extends SkFlatObject
{
    public:
        //SkFsUtils();

        /**
         * @brief Adjusts the path name adding the separator to the end if it
         * has not and if it is a directory
         * @param target the path directory name
         * @return the adjusted path name
         */
        static SkString adjustPathEndSeparator(CStr *target);

        /**
         * @brief Gets the real path for an symlink
         * @param target the symlink
         * @return the real path name
         */
        static SkString resolveLinkTargetPath(CStr *target);

        static SkString relativePath(CStr *fromAbsPath, CStr *target);

        /**
         * @brief Grabs generic informations about a path name
         * @param target the target path name
         * @param info the struct to fill
         * @return true if the target is not empty, otherwise false
         */
        static bool fillPathInfo(CStr *target, SkPathInfo &info);

        /**
         * @brief Grabs deep informations about a path name
         * @param target the target path name
         * @param info the struct to fill
         * @param calculateCheckSum if true it will calculate che hash
         * @param hm the hash mode to use
         * @return true if the target is not empty, otherwise false
         */
        static bool fillFileInfo(CStr *target,
                                 SkFileInfo &info,
                                 bool calculateCheckSum=false,
                                 SkHashMode hm=SkHashMode::HM_MD5);

        /**
         * @brief Checks if the path name exists
         * @param target the target path name
         * @return true if the file exists, otherwise false
         */
        static bool exists(CStr *target);

        /**
         * @brief Gets the size of the path name
         * @param target the target path name
         * @return the calculate size if there are not errors, otherwise < 0
         */
        static int64_t size(CStr *target);

        /**
         * @brief Checks if the path name is a generic file
         * @param target the target path name
         * @warning a generic file could be a symlink, a fifo, a character device,
         * a block devices and, finally, a local socket, in addition to a
         * regular file
         * @return true if it is a generic file, otherwise false
         */
        static bool isFile(CStr *target);

        /**
         * @brief Checks if the path name is a regular file
         * @param target the target path name
         * @return true if it is a regular file, otherwise false
         */
        static bool isRegularFile(CStr *target);

        /**
         * @brief Checks if the path name is a directory
         * @param target the target path name
         * @return true if it is a directory, otherwise false
         */
        static bool isDir(CStr *target);

        /**
         * @brief Checks if the path name is a character device
         * @param target the target path name
         * @return true if it is a character device, otherwise false
         */
        static bool isCharacterDevice(CStr *target);

        /**
         * @brief Checks if the path name is a block device
         * @param target the target path name
         * @return true if it is a block device, otherwise false
         */
        static bool isBlockDevice(CStr *target);

        /**
         * @brief Checks if the path name is a fifo
         * @param target the target path name
         * @return true if it is a fifo, otherwise false
         */
        static bool isFifo(CStr *target);

        /**
         * @brief Checks if the path name is a symlink
         * @param target the target path name
         * @return true if it is a symlink, otherwise false
         */
        static bool isSymLink(CStr *target);

        /**
         * @brief Checks if the path name is a local socket
         * @param target the target path name
         * @return true if it is a local socket, otherwise false
         */
        static bool isSocket(CStr *target);

        /**
         * @brief Gets the userID owner of a path name
         * @param target the target path name
         * @return the userID
         */
        static uid_t userID(CStr *target);

        /**
         * @brief Gets the groupID owner of a path name
         * @param target the target path name
         * @return the groupID
         */
        static gid_t groupID(CStr *target);

        /**
         * @brief Checks if a SkInodeState is a generic file
         * @param status the SkInodeState pointer relative to a path
         * @warning a generic file could be a symlink, a fifo, a character device,
         * a block devices and, finally, a local socket, in addition to a
         * regular file
         * @return true if it is a generic file, otherwise false
         */
        static bool isFile(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a regular file
         * @param status the SkInodeState pointer relative to a path
         * @return true if it is a regular file, otherwise false
         */
        static bool isRegularFile(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a directory
         * @param status the SkInodeState pointer relative to a path
         * @return true if it is a directory, otherwise false
         */
        static bool isDir(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a character device
         * @param status the SkInodeState pointer relative to a path
         * @return true if it is a character device, otherwise false
         */
        static bool isCharacterDevice(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a block device
         * @param status the SkInodeState pointer relative to a path
         * @return true if it is a block device, otherwise false
         */
        static bool isBlockDevice(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a fifo
         * @param status the SkInodeState relative to a path
         * @return true if it is a fifo, otherwise false
         */
        static bool isFifo(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a symlink
         * @param status the SkInodeState relative to a path
         * @return true if it is a symlink, otherwise false
         */
        static bool isSymLink(SkInodeStat *status);

        /**
         * @brief Checks if a SkInodeState is a local socket
         * @param status the SkInodeState relative to a path
         * @return true if it is a local socket, otherwise false
         */
        static bool isSocket(SkInodeStat *status);

        /**
         * @brief Gets the userID owner of a path name
         * @param status the SkInodeState relative to a path
         * @return the userID
         */
        static uid_t userID(SkInodeStat *status);

        /**
         * @brief Gets the groupID owner of a path name
         * @param status the SkInodeState relative to a path
         * @return the groupID
         */
        static gid_t groupID(SkInodeStat *status);

        /**
         * @brief Checks if a path name is readable
         * @param target the target path name
         * @return true if it is a readable, otherwise false
         */
        static bool isReadable(CStr *target);

        /**
         * @brief Checks if a path name is writable
         * @param target the target path name
         * @return true if it is a writable, otherwise false
         */
        static bool isWritable(CStr *target);

        /**
         * @brief Checks if a path name is executable
         * @param target the target path name
         * @return true if it is a executable, otherwise false
         */
        static bool isExecutable(CStr *target);

        /**
         * @brief Changes permissions to a path name
         * @param target the target path name
         * @param perm the permission reference to set
         * @return true if there are not errors, otherwise false
         */
        static bool chmod(CStr *target, SkFilePerm &perm);

        //THIS METH USE chown(..) syscall FOR EACH EXISTING FS ITEM,
        //BUT USE lchown(..) ONLY ON SYMLINK IF doNotResolveLink==true

        /**
         * @brief Changes the owner of a path name
         * @param target the target path name
         * @param owner the owner user by cstring
         * @param group the owner group by cstring
         * @param resolveLinkToItsTarget if true the real path will be used
         * @return true if there are not errors, otherwise false
         */
        static bool chown(CStr *target, CStr *owner, CStr *group, bool resolveLinkToItsTarget=true);

        /**
         * @brief Changes the owner of a path name
         * @param target the target path name
         * @param owner the owner user by uid_t
         * @param group the owner group by gid_t
         * @param resolveLinkToItsTarget if true the real path will be used
         * @return true if there are not errors, otherwise false
         */
        static bool chown(CStr *target, uid_t ownerID, gid_t groupID, bool resolveLinkToItsTarget=true);

        /**
         * @brief Changes the owner of a path name
         * @param target the target path name
         * @param owner the owner user by a SkOsUser reference (from SkOsEnv)
         * @param group the owner group by a SkOsGroup reference (from SkOsEnv)
         * @param resolveLinkToItsTarget if true the real path will be used
         * @return true if there are not errors, otherwise false
         */
        static bool chown(CStr *target, SkOsUser &owner, SkOsGroup &group, bool resolveLinkToItsTarget=true);

        /**
         * @brief Represents the unix permission as a string
         * @param target the target path name
         * @return the permission representation as string
         */
        static SkString unixPermToString(CStr *target);

        //FOR cp AND mv METHs, IF THE target EXISTS IT WILL BE DESTROYED

        /**
         * @brief Copies a file using path names
         * @param source the source path name
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool cp(CStr *source, CStr *target);

        /**
         * @brief Copies a file using path SkFileInfos
         * @param srcInfo the source infos
         * @param tgtInfo the target infos
         * @return true if there are not errors, otherwise false
         */
        static bool cp(SkFileInfo &srcInfo, SkFileInfo &tgtInfo);

        //IF source AND target STAY ON THE SAME DEV IT WILL BE A TRUE ATOMIC MOVE
        //OTHERWISE IT WILL BE A NORMAL copy, FOLLOWED BY rm OF THE SOURCE

        /**
         * @brief Moves a path using names
         * @param source the source path name
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool mv(CStr *source, CStr *target);

        /**
         * @brief Moves a file using path SkFileInfos
         * @param srcInfo the source infos
         * @param tgtInfo the target infos
         * @return true if there are not errors, otherwise false
         */
        static bool mv(SkFileInfo &srcInfo, SkFileInfo &tgtInfo);

        //mv AND rename MAKE THE SAME OPERATIONs

        /**
         * @brief Renames a path using names
         * @param source the source path name
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool rename(CStr *oldName, CStr *newName);

        /**
         * @brief Renames a file using path SkFileInfos
         * @param srcInfo the source infos
         * @param tgtInfo the target infos
         * @return true if there are not errors, otherwise false
         */
        static bool rename(SkFileInfo &oldInfo, SkFileInfo &newInfo);

        /**
         * @brief Creates a symlink of a path name
         * @param target the target path name
         * @param linkName the symlink path name
         * @return true if there are not errors, otherwise false
         */
        static bool ln(CStr *target, CStr *linkName);

        /**
         * @brief Creates a file from a path name
         * @param target the target path name
         * @param size the size of the file to create (all bytes will be set to 0)
         * @return true if there are not errors, otherwise false
         */
        static bool touch(CStr *target, uint64_t size=0);

        /**
         * @brief Resizes a file, truncating it or adding zeros
         * @param target the target path name
         * @param newSize the new size for the file
         * @return true if there are not errors, otherwise false
         */
        static bool resize(CStr *target, uint64_t newSize);

        /**
         * @brief Creates a fifo
         * @param target the target path name
         * @param perm the permission reference to set
         * @return true if there are not errors, otherwise false
         */
        static bool mkfifo(CStr *target, SkFilePerm &perm);

        //REMOVE FILE OR EMPTY-DIR
        /**
         * @brief Removes a generic file or an empty directory
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool rm(CStr *target);

        /**
         * @brief Creates a directory
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool mkdir(CStr *target);

        /**
         * @brief Creates a directory tree
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool mkdirhier(CStr *target);

#if defined(SPECIALK_APPLICATION)
        //REMOVE RECURSIVELY DIR CONTENTS
        /**
         * @brief Removes recursively a directory
         * @param target the target path name
         * @param removeRecursiveContents
         * @return true if there are not errors, otherwise false
         */
        static bool rmdir(CStr *target, bool removeRecursiveContents=false);
#endif

        /**
         * @brief Changes directory path for this process
         * @param target the target path name
         * @return true if there are not errors, otherwise false
         */
        static bool chdir(CStr *target);

        /**
         * @brief Gets the current directory for this process
         * @return the current directory as string
         */
        static SkString pwd();

#if defined(SPECIALK_APPLICATION)
        /**
         * @brief List of a directory content, with or without recursive feature
         * @param target the target path name
         * @param results the SkFileInfosList pointer
         * @param recursive if true the search will be recursive inside subdirectories
         * @return true if there are not errors, otherwise false
         */
        static bool ls(CStr *target,
                       SkFileInfosList *results,
                       //SkStringList nameFilters=SkStringList("*"),
                       bool recursive=true);
#endif


        //CONVENIENCE METHs TO R/W BYTEs FROM/TO FILEs USING SkFile INTERNNALLY
        //USE THESE METHs CONSIDERING ALL OPERATIONs WILL STORE DATA INSIDE RAM

        /**
         * @brief Facilities to write text on a file
         * @param target the target path name
         * @param s the string to write
         * @return true if there are not errors, otherwise false
         */
        static bool writeTEXT(CStr *target, CStr *s);

        /**
         * @brief Facilities to read text from a file
         * @param source the source path name
         * @param s the string to fill
         * @return true if there are not errors, otherwise false
         */
        static bool readTEXT(CStr *source, SkString &text);

        /**
         * @brief Facilities to write a JSON document on a file
         * @param target the target path name
         * @param map the variant-map to write
         * @return true if there are not errors, otherwise false
         */
        static bool writeJSON(CStr *target, SkArgsMap &m, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Facilities to write a JSON document on a file
         * @param target the target path name
         * @param l the variant-list to write
         * @return true if there are not errors, otherwise false
         */
        static bool writeJSON(CStr *target, SkVariantList &l, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Facilities to write a JSON document on a file
         * @param target the target path name
         * @param v the variant to write
         * @return true if there are not errors, otherwise false
         */
        static bool writeJSON(CStr *target, SkVariant &v, bool indented=false, uint8_t spaces=4);

        /**
         * @brief Facilities to read a JSON document from a file
         * @param target the target path name
         * @param map the variant-map to fill
         * @return true if there are not errors, otherwise false
         */
        static bool readJSON(CStr *source, SkArgsMap &m);

        /**
         * @brief Facilities to read a JSON document from a file
         * @param target the target path name
         * @param l the variant-list to fill
         * @return true if there are not errors, otherwise false
         */
        static bool readJSON(CStr *source, SkVariantList &l);

        /**
         * @brief Facilities to read a JSON document from a file
         * @param target the target path name
         * @param v the variant to fill
         * @return true if there are not errors, otherwise false
         */
        static bool readJSON(CStr *source, SkVariant &v);
        /**
         * @brief Facilities to write an array data pointer on a file
         * @param target the target path name
         * @param b the array data pointer to write
         * @return true if there are not errors, otherwise false
         */
        static bool writeDATA(CStr *target, SkDataBuffer &b);
        static bool writeDATA(CStr *target, CStr *data, ULong size);

        /**
         * @brief Facilities to read an array data pointer from a file
         * @param target the target path name
         * @param map the array data pointer to fill
         * @return true if there are not errors, otherwise false
         */
        static bool readDATA(CStr *source, SkDataBuffer &b);

        static void copyCodeHeaderFs(CVoid *array, unsigned count, CStr *target, bool asBinary);

    private:
        static void unixPermToString(SkString &permissions, SkInodeStat *status);
        static mode_t convertFilePermToMode(SkFilePerm &perm);
        static mode_t convertFilePermToMode(SkFilePerm &perm, SkString &s);

        static bool prepareCopyTarget(SkFileInfo &srcInfo, SkFileInfo &tgtInfo);

        //RETURNING strlen OF target
        static uint64_t checkEmptyPathError(CStr *target, CStr *errMessage);

#if defined(SPECIALK_APPLICATION)
        static bool lsPriv(CStr *target,
                           SkFileInfosList *results,
                           //QStringList nameFilters,
                           SkStringList *subDirs);
#endif

        //static bool removeSingleEmptyDirectory(CStr *target);
};

#endif // SKFSUTILS_H
