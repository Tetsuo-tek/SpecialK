#ifndef SKFLATTEMPFILE_H
#define SKFLATTEMPFILE_H

#include "skflatfile.h"

class SkFlatTempFile extends SkFlatFile
{
    bool autoremoveOnExit;

    public:
        SkFlatTempFile();
        ~SkFlatTempFile();

        void setAutoRemove(bool enabled);
        bool autoRemove();
};

#endif // SKFLATTEMPFILE_H
