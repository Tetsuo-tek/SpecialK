#include "skflatfile.h"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

static CStr *modeNames[] = {
    "NoMode",
    "OnlyRead",
    "OnlyWrite",
    "OnlyWriteAppending",
    "ReadWrite",
    "ReadWriteAppending"
};

SkFlatFile::SkFlatFile()
{
    file = nullptr;
    openMode = SkFileMode::FLM_NOMODE;
}

void SkFlatFile::setFilePath(CStr *path)
{
    filePath = path;
    setObjectName(filePath.c_str());
}

bool SkFlatFile::open(SkFileMode mode)
{
    if (isOpen())
    {
        notifyError(DeviceAlreadyOpen, __PRETTY_FUNCTION__);
        return false;
    }

    if (filePath.isEmpty())
    {
        notifyError(TargetNotValid, __PRETTY_FUNCTION__, "Cannot open file device with an EMPTY name");
        return false;
    }

    // 085 20 58 336

    if (mode == SkFileMode::FLM_ONLYREAD)
    {
        if (!exists())
        {
            notifyError(TargetNotValid, __PRETTY_FUNCTION__, "File NOT found");
            return false;
        }

        file = ::fopen(filePath.c_str(), "r");
    }

    else if (mode == FLM_ONLYWRITE)
        file = ::fopen(filePath.c_str(), "w");

    else if (mode == FLM_ONLYWRITE_APPENDING)
        file = ::fopen(filePath.c_str(), "a");

    else if (mode == FLM_READWRITE)
        file = ::fopen(filePath.c_str(), "r+");

    else if (mode == FLM_READWRITE_APPENDING)
        file = ::fopen(filePath.c_str(), "a+");

    if (file)
    {
        openMode = mode;
        FlatDebug("File device is OPEN [mode: " << currentModeString() << "]");
        notifyState(SkDeviceState::Open);
        return true;
    }

    notifyError(CannotOpen, __PRETTY_FUNCTION__);
    file = nullptr;

    return false;
}

Long SkFlatFile::pos()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return -1;
    }

    Long p = ::ftell(file);

    if (p < 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__);
        p = -1;
    }

    return p;
}

bool SkFlatFile::rewind()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return seek(0);
}

bool SkFlatFile::seek(Long position)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (::fseek(file, position, SEEK_SET) != 0)
    {
        notifyError(SkDeviceError::CannotSeekDevice, __PRETTY_FUNCTION__);
        return false;
    }

    return true;
}

bool SkFlatFile::atEof()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return true;
    }

    return (bytesAvailable()==0);
}

Long SkFlatFile::canReadLine()
{
    Long lineLength = 0;
    Long currentPos = pos();

    if (currentPos == -1)
        return false;

    while(!atEof())
    {
        lineLength++;

        if (::fgetc(file) == '\n' || atEof())
        {
            seek(currentPos);
            return lineLength;
        }
    }

    return 0;
}

bool SkFlatFile::flush()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    if (::fflush(file) != 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__, "Cannot flush file");
        return false;
    }

    if (::fsync(fileno(file)) != 0)
    {
        notifyError(SkDeviceError::DeviceInternalError, __PRETTY_FUNCTION__, "Cannot synchronize file");
        return false;
    }

    return true;
}

Long SkFlatFile::size()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    flush();

    struct stat st;
    ::stat(filePath.c_str(), &st);
    return st.st_size;
}

Long SkFlatFile::bytesAvailable()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    flush();

    Long sz = size();
    Long p = pos();

    if (sz <= 0 || p < 0)
        return 0;

    return sz - pos();
}

void SkFlatFile::close()
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return;
    }

    flush();
    ::fclose(file);
    file = nullptr;

    openMode = SkFileMode::FLM_NOMODE;

    notifyState(SkDeviceState::Closed);
}

bool SkFlatFile::exists()
{
    return (::access(filePath.c_str(), F_OK) == 0);
}

bool SkFlatFile::remove()
{
    if (filePath.isEmpty())
        return false;

    if (!exists())
    {
        notifyError(TargetNotValid, __PRETTY_FUNCTION__, "File NOT found");
        return false;
    }

    if (isOpen())
        this->close();

    return (::remove(filePath.c_str()) == 0);
}

bool SkFlatFile::readInternal(char *data, Long &len, bool peek)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    len = ::fread(data, sizeof(char), len, file);

    if (peek)
    {
        Long currentPos = pos();
        currentPos -= len;
        seek(currentPos);
    }

    if (len > 0)
        notifyRead(len);

    bool error = (::ferror(file) != 0);

    if (error)
        notifyError(SkDeviceError::CannotReadFromDevice, __PRETTY_FUNCTION__);

    return !error;
}

bool SkFlatFile::writeInternal(CStr *data, Long len)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return false;
    }

    Long sent = ::fwrite(data, sizeof(char), len, file);

    if (sent > 0)
    {
        len = sent;
        notifyWrite(len);
    }

    bool error = (::ferror(file) != 0);

    if (error)
        notifyError(SkDeviceError::CannotWriteToDevice, __PRETTY_FUNCTION__);

    return !error;
}

SkFileMode SkFlatFile::currentMode()
{
    return openMode;
}

CStr *SkFlatFile::currentModeString()
{
    return modeNames[openMode];
}

CStr *SkFlatFile::path()
{
    return filePath.c_str();
}

SkString SkFlatFile::hash(SkHashMode mode)
{
    if (!isOpen())
    {
        notifyError(SkDeviceError::CannotUseClosedDevice, __PRETTY_FUNCTION__);
        return 0;
    }

    flush();

    return fileHash(filePath.c_str(), mode);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <sys/mman.h>

SkString fileHash(CStr *path, SkHashMode mode)
{
    int fd = ::open(path, O_RDONLY);

    if(fd < 0)
        return "-";

    struct stat statbuf;

    if(::fstat(fd, &statbuf) < 0)
        return "-";

    uint64_t sz = static_cast<uint64_t>(statbuf.st_size);

    void *fileBuffer = ::mmap(nullptr, sz, PROT_READ, MAP_SHARED, fd, 0);
    SkString hashStr = bufferHash(fileBuffer, sz, mode);
    ::munmap(fileBuffer, sz);

    ::close(fd);
    return hashStr.c_str();
}
