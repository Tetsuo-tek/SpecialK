#include "skflattempfile.h"
#include "skfsutils.h"
#include "Core/System/skosenv.h"

SkFlatTempFile::SkFlatTempFile()
{
    autoremoveOnExit = true;

    SkString p = SkFsUtils::adjustPathEndSeparator(osEnvironment->getSystemTempPath().c_str());
    p.concat(osEnvironment->getExecutableName());
    p.concat("-");
    p.append(uniqueID());
    p.concat(".tmp");

    setFilePath(p.c_str());
}

SkFlatTempFile::~SkFlatTempFile()
{
    if (autoremoveOnExit)
        remove();
}

void SkFlatTempFile::setAutoRemove(bool enabled)
{
    autoremoveOnExit = enabled;
}

bool SkFlatTempFile::autoRemove()
{
    return autoremoveOnExit;
}
