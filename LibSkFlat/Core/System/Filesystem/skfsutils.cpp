#include "skfsutils.h"

#if defined(SPECIALK_APPLICATION)
    #include "Core/App/skapp.h"
    #include "Core/System/Filesystem/skfile.h"
    #include "Core/System/Filesystem/skfileinfoslist.h"
#endif

#if !defined(PATH_MAX)
#define PATH_MAX   1024
#endif

#include "Core/Containers/skargsmap.h"
#include "Core/Containers/skarraycast.h"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
//#include <sys/sendfile.h>
#include <dirent.h>

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkFilePermItem)
{
    SetupEnumWrapper(SkFilePermItem);

    SetEnumItem(SkFilePermItem::PM_NOPERM);
    SetEnumItem(SkFilePermItem::PM___X);
    SetEnumItem(SkFilePermItem::PM__W_);
    SetEnumItem(SkFilePermItem::PM__WX);
    SetEnumItem(SkFilePermItem::PM_R__);
    SetEnumItem(SkFilePermItem::PM_R_X);
    SetEnumItem(SkFilePermItem::PM_RW_);
    SetEnumItem(SkFilePermItem::PM_RWX);
}

DeclareWrapper_STRUCT_TOMAP(SkFilePerm)
{
    SetupStructWrapper(SkFilePerm);

    StructToMapProperty(owner);
    StructToMapProperty(group);
    StructToMapProperty(other);
}

DeclareWrapper_STRUCT_FROMMAP(SkFilePerm)
{
    MapToStructPropertyCasted(owner, SkFilePermItem, toInt);
    MapToStructPropertyCasted(group, SkFilePermItem, toInt);
    MapToStructPropertyCasted(other, SkFilePermItem, toInt);
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STRUCT_TOMAP(SkPathInfo)
{
    SetupStructWrapper(SkPathInfo);

    StructToMapProperty(exists);
    StructToMapProperty(completeAbsolutePath);
    StructToMapProperty(absoluteParentPath);
    StructToMapProperty(completeName);
    StructToMapProperty(name);
    StructToMapProperty(suffix);
}

DeclareWrapper_STRUCT_FROMMAP(SkPathInfo)
{
    MapToStructProperty(exists).toBool();
    MapToStructProperty(completeAbsolutePath).data();
    MapToStructProperty(absoluteParentPath).data();
    MapToStructProperty(completeName).data();
    MapToStructProperty(name).data();
    MapToStructProperty(suffix).data();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STRUCT_TOMAP(SkFileInfo)
{
    SetupStructWrapper(SkFileInfo);

    StructToMapProperty(size);
    StructToMapProperty(mimeType);
    StructToMapProperty(isDir);
    StructToMapProperty(isFile);
    StructToMapProperty(isRegularFile);
    StructToMapProperty(isSymLink);
    StructToMapProperty(symLinkTarget);
    StructToMapProperty(isFifo);
    StructToMapProperty(isCharDevice);
    StructToMapProperty(isBlockDevice);
    StructToMapProperty(isSocket);
    StructToMapProperty(isHidden);
    StructToMapProperty(isReadable);
    StructToMapProperty(isWritable);
    StructToMapProperty(isExecutable);
    StructToMapProperty(permRawMode);
    StructToMapProperty(permissions);
    //StructToMapProperty(user);
    //StructToMapProperty(group);
    StructToMapProperty(created);
    StructToMapProperty(lastModified);
    StructToMapProperty(lastRead);
    StructToMapProperty(checkSum);
    StructToMapProperty(device);
    StructToMapProperty(serialNO);
    StructToMapProperty(hLinks);
    //StructToMapProperty(blockSize);
    //StructToMapProperty(blocks);
}

DeclareWrapper_STRUCT_FROMMAP(SkFileInfo)
{
    MapToStructProperty(size).toUInt64();
    MapToStructProperty(mimeType).data();
    MapToStructProperty(isDir).toBool();
    MapToStructProperty(isFile).toBool();
    MapToStructProperty(isRegularFile).toBool();
    MapToStructProperty(isSymLink).toBool();
    MapToStructProperty(symLinkTarget).data();
    MapToStructProperty(isFifo).toBool();
    MapToStructProperty(isCharDevice).toBool();
    MapToStructProperty(isBlockDevice).toBool();
    MapToStructProperty(isSocket).toBool();
    MapToStructProperty(isHidden).toBool();
    MapToStructProperty(isReadable).toBool();
    MapToStructProperty(isWritable).toBool();
    MapToStructProperty(isExecutable).toBool();
    MapToStructProperty(permRawMode).toUInt32();
    MapToStructProperty(permissions).data();
    //MapToStructProperty(user);
    //MapToStructProperty(group);
    MapToStructProperty(created).data();
    MapToStructProperty(lastModified).data();
    MapToStructProperty(lastRead).data();
    MapToStructProperty(checkSum).data();
    MapToStructProperty(device).toInt32();
    MapToStructProperty(serialNO).toInt64();
    MapToStructProperty(hLinks).toInt32();
    MapToStructProperty(blockSize).toInt32();
    MapToStructProperty(blocks).toInt32();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STATIC(SkFsUtils);

DeclareMeth_STATIC_RET(SkFsUtils, adjustPathEndSeparator, SkString, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, resolveLinkTargetPath, SkString, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, fillPathInfo, bool, Arg_CStr, *Arg_Custom(SkPathInfo))
DeclareMeth_STATIC_RET(SkFsUtils, fillFileInfo, bool, Arg_CStr, *Arg_Custom(SkFileInfo), Arg_Bool)
DeclareMeth_STATIC_RET(SkFsUtils, exists, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, size, int64_t, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isFile, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isRegularFile, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isDir, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isCharacterDevice, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isBlockDevice, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isFifo, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isSymLink, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isSocket, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, userID, uid_t, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, groupID, gid_t, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isReadable, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isWritable, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, isExecutable, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, chmod, bool, Arg_CStr, *Arg_Custom(SkFilePerm))
DeclareMeth_STATIC_RET(SkFsUtils, chown, bool, Arg_CStr, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, unixPermToString, SkString, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, cp, bool, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, mv, bool, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, rename, bool, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, ln, bool, Arg_CStr, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, touch, bool, Arg_CStr, Arg_Int64)
DeclareMeth_STATIC_RET(SkFsUtils, resize, bool, Arg_CStr, Arg_Int64)
DeclareMeth_STATIC_RET(SkFsUtils, mkfifo, bool, Arg_CStr, *Arg_Custom(SkFilePerm))
DeclareMeth_STATIC_RET(SkFsUtils, rm, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, mkdir, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, mkdirhier, bool, Arg_CStr)

#if defined(SPECIALK_APPLICATION)
DeclareMeth_STATIC_RET(SkFsUtils, rmdir, bool, Arg_CStr)
#endif

DeclareMeth_STATIC_RET(SkFsUtils, chdir, bool, Arg_CStr)
DeclareMeth_STATIC_RET(SkFsUtils, pwd, SkString)

#if defined(SPECIALK_APPLICATION)
    DeclareMeth_STATIC_RET(SkFsUtils, ls, bool, Arg_CStr, Arg_Custom(SkFileInfosList))
    DeclareMeth_STATIC_RET_OVERLOAD(SkFsUtils, ls, 1, bool, Arg_CStr, Arg_Custom(SkFileInfosList), Arg_Bool)
    DeclareMeth_STATIC_RET(SkFsUtils, writeTEXT, bool, Arg_CStr, Arg_CStr)
    DeclareMeth_STATIC_RET(SkFsUtils, readTEXT, bool, Arg_CStr, Arg_StringRef)
    DeclareMeth_STATIC_RET(SkFsUtils, writeJSON, bool, Arg_CStr, *Arg_Custom(SkArgsMap))
    DeclareMeth_STATIC_RET(SkFsUtils, readJSON, bool, Arg_CStr, *Arg_Custom(SkArgsMap))
    DeclareMeth_STATIC_RET(SkFsUtils, writeDATA, bool, Arg_CStr, *Arg_Custom(SkDataBuffer))
    DeclareMeth_STATIC_RET(SkFsUtils, readDATA, bool, Arg_CStr, *Arg_Custom(SkDataBuffer))
#endif

SetupClassWrapper(SkFsUtils)
{
    AddEnumType(SkFilePermItem);
    AddStructType(SkFilePerm);
    AddStructType(SkPathInfo);
    AddStructType(SkFileInfo);
    SetStructSuper(SkFileInfo, SkPathInfo);

    AddMeth_STATIC_RET(SkFsUtils, adjustPathEndSeparator);
    AddMeth_STATIC_RET(SkFsUtils, resolveLinkTargetPath);
    AddMeth_STATIC_RET(SkFsUtils, fillPathInfo);
    AddMeth_STATIC_RET(SkFsUtils, fillFileInfo);
    AddMeth_STATIC_RET(SkFsUtils, exists);
    AddMeth_STATIC_RET(SkFsUtils, size);
    AddMeth_STATIC_RET(SkFsUtils, isFile);
    AddMeth_STATIC_RET(SkFsUtils, isRegularFile);
    AddMeth_STATIC_RET(SkFsUtils, isDir);
    AddMeth_STATIC_RET(SkFsUtils, isCharacterDevice);
    AddMeth_STATIC_RET(SkFsUtils, isBlockDevice);
    AddMeth_STATIC_RET(SkFsUtils, isFifo);
    AddMeth_STATIC_RET(SkFsUtils, isSymLink);
    AddMeth_STATIC_RET(SkFsUtils, isSocket);
    AddMeth_STATIC_RET(SkFsUtils, userID);
    AddMeth_STATIC_RET(SkFsUtils, groupID);
    AddMeth_STATIC_RET(SkFsUtils, isReadable);
    AddMeth_STATIC_RET(SkFsUtils, isWritable);
    AddMeth_STATIC_RET(SkFsUtils, isExecutable);
    AddMeth_STATIC_RET(SkFsUtils, chmod);
    AddMeth_STATIC_RET(SkFsUtils, chown);
    AddMeth_STATIC_RET(SkFsUtils, unixPermToString);
    AddMeth_STATIC_RET(SkFsUtils, cp);
    AddMeth_STATIC_RET(SkFsUtils, mv);
    AddMeth_STATIC_RET(SkFsUtils, rename);
    AddMeth_STATIC_RET(SkFsUtils, ln);
    AddMeth_STATIC_RET(SkFsUtils, touch);
    AddMeth_STATIC_RET(SkFsUtils, resize);
    AddMeth_STATIC_RET(SkFsUtils, mkfifo);
    AddMeth_STATIC_RET(SkFsUtils, rm);
    AddMeth_STATIC_RET(SkFsUtils, mkdir);
    AddMeth_STATIC_RET(SkFsUtils, mkdirhier);
#if defined(SPECIALK_APPLICATION)
    AddMeth_STATIC_RET(SkFsUtils, rmdir);
#endif
    AddMeth_STATIC_RET(SkFsUtils, chdir);
    AddMeth_STATIC_RET(SkFsUtils, pwd);
#if defined(SPECIALK_APPLICATION)
    AddMeth_STATIC_RET(SkFsUtils, ls);
    AddMeth_STATIC_RET_OVERLOAD(SkFsUtils, ls, 1);
    AddMeth_STATIC_RET(SkFsUtils, writeTEXT);
    AddMeth_STATIC_RET(SkFsUtils, readTEXT);
    AddMeth_STATIC_RET(SkFsUtils, writeJSON);
    AddMeth_STATIC_RET(SkFsUtils, readJSON);
    AddMeth_STATIC_RET(SkFsUtils, writeDATA);
    AddMeth_STATIC_RET(SkFsUtils, readDATA);
#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkPathInfo::SkPathInfo()
{
    exists = false;
}

void SkPathInfo::toMap(SkArgsMap &map)
{
    map["exists"] = exists;
    map["completeAbsolutePath"] = completeAbsolutePath;
    map["completeName"] = completeName;
    map["name"] = name;
    map["suffix"] = suffix;
    map["absoluteParentPath"] = absoluteParentPath;
}

SkString SkPathInfo::toString(bool indented, uint8_t spaces)
{
    SkArgsMap m;
    toMap(m);
    SkString json;
    m.toString(json, indented, spaces);
    return json.c_str();
}

SkFileInfo::SkFileInfo()
{
    size = false;
    isDir = false;
    isFile = false;
    isRegularFile = false;
    isSymLink = false;
    isFifo = false;
    isCharDevice = false;
    isBlockDevice = false;
    isSocket = false;
    isHidden = false;
    isReadable = false;
    isWritable = false;
    isExecutable = false;

    permRawMode = 0;
    device = 0;
    serialNO = 0;
    hLinks = 0;
    blockSize = 0;
    blocks = 0;
}

void SkFileInfo::toMap(SkArgsMap &map)
{
    SkPathInfo::toMap(map);

    map["size"] = size;

    if (!exists)
        return;

    map["mimeType"] = mimeType;
    map["isDir"] = isDir;
    map["isFile"] = isFile;
    map["isRegularFile"] = isRegularFile;
    map["isSymLink"] = isSymLink;
    map["symLinkTarget"] = symLinkTarget;
    map["isFifo"] = isFifo;
    map["isCharDevice"] = isCharDevice;
    map["isBlockDevice"] = isBlockDevice;
    map["isSocket"] = isSocket;
    map["isHidden"] = isHidden;
    map["isReadable"] = isReadable;
    map["isWritable"] = isWritable;
    map["isExecutable"] = isExecutable;
    map["permRawMode"] =  permRawMode;
    map["permissions"] = permissions;
    map["created"] = created;
    map["lastModified"] = lastModified;
    map["lastRead"] = lastRead;
    map["checkSum"] = checkSum;

    map["device"] = device;
    map["serialNO"] = serialNO;
    map["hLinks"] = hLinks;
    map["blockSize"] = (Int) blockSize;
    map["blocks"] = (Int) blocks;

    SkArgsMap userMap;
    user.toMap(userMap);
    map["user"] = userMap;

    SkArgsMap groupMap;
    group.toMap(groupMap);
    map["group"] = groupMap;
}

SkString SkFileInfo::toString(bool indented, uint8_t spaces)
{
    SkArgsMap m;
    toMap(m);
    SkString json;
    m.toString(json, indented, spaces);
    return json.c_str();
}

SkFilePerm::SkFilePerm()
{
    owner = SkFilePermItem::PM_NOPERM;
    group = SkFilePermItem::PM_NOPERM;
    other = SkFilePermItem::PM_NOPERM;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*SkFsUtils::SkFsUtils()
{

}*/

SkString SkFsUtils::adjustPathEndSeparator(CStr *path)
{
    if (SkFsUtils::exists(path) && !SkFsUtils::isDir(path))
        return path;

    CStr *slash = SK_PATH_SEPARATOR;
    SkString tempPath(path);

    if (!tempPath.equals(slash) && !tempPath.endsWith(slash))
        tempPath.concat(slash);

    return tempPath;
}

SkString SkFsUtils::resolveLinkTargetPath(CStr *target)
{
    char *path = ::realpath(target, nullptr);

    if (!path)
        return "";

    SkString s = path;

    if (!s.endsWith(SK_PATH_SEPARATOR))
        s.append(SK_PATH_SEPARATOR);

    if (path)
    {
        s = path;
        ::free(path);
    }

    return s;
}

SkString SkFsUtils::relativePath(CStr *fromAbsPath, CStr *target)
{
    SkString relPath = target;
    relPath.replace(fromAbsPath, "", 0, true);

    if (relPath.c_str()[0] == '/')
        return &relPath.c_str()[1];

    return relPath;
}

bool SkFsUtils::fillPathInfo(CStr *target, SkPathInfo &info)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot grab PathInfo (the target is empty)") == 0)
        return false;

    info.exists = SkFsUtils::exists(target);

    uint64_t sz = strlen(target);

    if (target[sz-1] == '/')
        sz--;

    char *tgt = new char [sz+1];
    ::memcpy(tgt, target, sz);
    tgt[sz] = '\0';

    char path_save[PATH_MAX];
    char abs_path[PATH_MAX];
    char *p;

    if(!(p = ::strrchr(tgt, '/')))
    {
        info.completeName = target;
        ::getcwd(abs_path, sizeof(abs_path));
    }

    else
    {
        info.completeName = &p[1];
        *p = '\0';
        ::getcwd(path_save, sizeof(path_save));
        ::chdir(tgt);
        ::getcwd(abs_path, sizeof(abs_path));
        ::chdir(path_save);
    }

    info.absoluteParentPath = abs_path;

    sz = info.completeName.size();
    char *baseName = new char [sz+1];
    ::memcpy(baseName, info.completeName.c_str(), sz);
    baseName[sz] = '\0';

    if(!(p = ::strrchr(baseName, '.')))
    {
        info.suffix.clear();
        info.name =  baseName;
    }

    else
    {
        info.suffix = &p[1];
        *p = '\0';
        info.name = baseName;
    }

    info.completeAbsolutePath = SkFsUtils::adjustPathEndSeparator(info.absoluteParentPath.c_str());
    info.completeAbsolutePath.concat(info.completeName);

    delete [] tgt;
    delete [] baseName;

    return true;
}

bool SkFsUtils::fillFileInfo(CStr *target,
                               SkFileInfo &info,
                               bool calculateCheckSum,
                               SkHashMode hm)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot grab FileInfos (the target is empty)") == 0)
        return false;

    SkFsUtils::fillPathInfo(target, info);

    SkInodeStat st;
    stat(target, &st);

    if (info.exists)
        info.size = st.st_size;

    else
    {
        info.size = 0;
        return false;
    }

    info.isDir = SkFsUtils::isDir(&st);
    info.isFile = SkFsUtils::isFile(&st);

    info.isSymLink = SkFsUtils::isSymLink(target);//USING lstat() inside

    if (info.isSymLink)
        info.symLinkTarget = SkFsUtils::resolveLinkTargetPath(target);

    info.isFifo = SkFsUtils::isFifo(&st);
    info.isCharDevice = SkFsUtils::isCharacterDevice(&st);
    info.isBlockDevice = SkFsUtils::isBlockDevice(&st);
    info.isSocket = SkFsUtils::isSocket(&st);
    info.isHidden = (info.name.charAt(0) == '.');

    info.isRegularFile = info.isFile
            && (!info.isSymLink
                && !info.isFifo
                && !info.isCharDevice
                && !info.isBlockDevice
                && !info.isSocket);

    info.isExecutable = SkFsUtils::isExecutable(target);
    info.isReadable = SkFsUtils::isReadable(target);
    info.isWritable = SkFsUtils::isWritable(target);

#if defined(SPECIALK_APPLICATION)
    if (info.isFile)
        info.mimeType = SkMimeType::getMimeType(info.suffix.c_str());
    else
#endif
        info.mimeType.clear();

    info.permRawMode = st.st_mode;
    info.permissions.clear();

    SkFsUtils::unixPermToString(info.permissions, &st);

    SkOsEnv::userInfos(st.st_uid, info.user);
    SkOsEnv::groupInfos(st.st_gid, info.group);

    //SkTimeSpec st_atim;		/* Time of last access.  */
    //SkTimeSpec st_mtim;		/* Time of last modification.  */
    //SkTimeSpec st_ctim;		/* Time of last status change.  */
    //info.created = ctime(&st.st_atim.tv_sec);

    if (calculateCheckSum && info.isRegularFile)
        info.checkSum = ::fileHash(target, hm);

    else
        info.checkSum.clear();

    info.device = st.st_dev;
    info.serialNO = st.st_ino;
    info.hLinks = st.st_nlink;
    info.blockSize = st.st_blksize;
    info.blocks = st.st_blocks;

    return true;
}

bool SkFsUtils::exists(CStr *target)
{
    return (::access(target, F_OK) == 0);
}

int64_t SkFsUtils::size(CStr *target)
{
    struct stat st;
    ::stat(target, &st);
    return st.st_size;
}

/*The following flags are defined for the st_mode field:

   S_IFMT     0170000   bitmask for the file type bitfields
   S_IFSOCK   0140000   socket
   S_IFLNK    0120000   symbolic link
   S_IFREG    0100000   regular file
   S_IFBLK    0060000   block device
   S_IFDIR    0040000   directory
   S_IFCHR    0020000   character device
   S_IFIFO    0010000   FIFO
   S_ISUID    0004000   set UID bit
   S_ISGID    0002000   set-group-ID bit (see below)
   S_ISVTX    0001000   sticky bit (see below)
   S_IRWXU    00700     mask for file owner permissions
   S_IRUSR    00400     owner has read permission
   S_IWUSR    00200     owner has write permission
   S_IXUSR    00100     owner has execute permission
   S_IRWXG    00070     mask for group permissions
   S_IRGRP    00040     group has read permission
   S_IWGRP    00020     group has write permission
   S_IXGRP    00010     group has execute permission
   S_IRWXO    00007     mask for permissions for others (not in group)
   S_IROTH    00004     others have read permission
   S_IWOTH    00002     others have write permission
   S_IXOTH    00001     others have execute permission*/

bool SkFsUtils::isFile(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isFile(&st);
}

bool SkFsUtils::isRegularFile(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isRegularFile(&st);
}

bool SkFsUtils::isDir(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isDir(&st);
}

bool SkFsUtils::isCharacterDevice(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isCharacterDevice(&st);
}

bool SkFsUtils::isBlockDevice(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isBlockDevice(&st);
}

bool SkFsUtils::isFifo(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isFifo(&st);
}

bool SkFsUtils::isSymLink(CStr *target)
{
    SkInodeStat st;
    lstat(target, &st);
    return SkFsUtils::isSymLink(&st);
}

bool SkFsUtils::isSocket(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::isSocket(&st);
}

uid_t SkFsUtils::userID(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::userID(&st);
}

gid_t SkFsUtils::groupID(CStr *target)
{
    SkInodeStat st;
    stat(target, &st);
    return SkFsUtils::groupID(&st);
}

bool SkFsUtils::isFile(SkInodeStat *status) {return S_ISREG(status->st_mode);}

bool SkFsUtils::isRegularFile(SkInodeStat *status)
{
    return SkFsUtils::isFile(status)
            && (!SkFsUtils::isSymLink(status)
                && !SkFsUtils::isFifo(status)
                && !SkFsUtils::isCharacterDevice(status)
                && !SkFsUtils::isBlockDevice(status)
                && !SkFsUtils::isSocket(status));
}

bool SkFsUtils::isDir(SkInodeStat *status) {return S_ISDIR(status->st_mode);}
bool SkFsUtils::isCharacterDevice(SkInodeStat *status) {return S_ISCHR(status->st_mode);}
bool SkFsUtils::isBlockDevice(SkInodeStat *status) {return S_ISBLK(status->st_mode);}
bool SkFsUtils::isFifo(SkInodeStat *status) {return S_ISFIFO(status->st_mode);}
bool SkFsUtils::isSymLink(SkInodeStat *status) {return S_ISLNK(status->st_mode);}
bool SkFsUtils::isSocket(SkInodeStat *status) {return S_ISSOCK(status->st_mode);}
uid_t SkFsUtils::userID(SkInodeStat *status) {return status->st_uid;}
gid_t SkFsUtils::groupID(SkInodeStat *status) {return status->st_gid;}

bool SkFsUtils::isReadable(CStr *target)
{
    return (::access(target, R_OK) == 0);
}

bool SkFsUtils::isWritable(CStr *target)
{
    return (::access(target, W_OK) == 0);
}

bool SkFsUtils::isExecutable(CStr *target)
{
    return (::access(target, X_OK) == 0);
}

/*
rwx	v	Permessi
111	7	lettura, scrittura ed esecuzione
110	6	lettura e scrittura
101	5	lettura ed esecuzione
100	4	solo lettura
011	3	scrittura ed esecuzione
010	2	solo scrittura
001	1	solo esecuzione
000	0	nessuno

int main(int argc, char **argv)
{
    char mode[] = "0777";
    char buf[100] = "/home/user/example.txt";
    int i;
    i = strtol(mode, 0, 8);
    if (chmod (buf,i) < 0)
    {
        fprintf(stderr, "%s: error in chmod(%s, %s) - %d (%s)\n",
        argv[0], buf, mode, errno, strerror(errno));
    }
    return(0);
}*/


bool SkFsUtils::chmod(CStr *target, SkFilePerm &perm)
{
    if (!SkFsUtils::checkEmptyPathError(target, "Cannot make change permissions (because the pathName is empty)"))
        return false;

    if (!SkFsUtils::exists(target))
    {
        StaticError("Cannot make change permissions (because the pathName NOT exists): " <<  target);
        return false;
    }
    if (!SkFsUtils::isWritable(target))
    {
        StaticError("Cannot make change permissions (because it is NOT writable): " <<  target);
        return false;
    }

    SkString m;
    mode_t mode = SkFsUtils::convertFilePermToMode(perm, m);

    StaticDebug("Changing permissions [" << m << "] to: " << target << " ..");

    if ((::chmod(target, mode) == 0))
        return true;

    StaticError("Cannot change permission: " <<  target);
    return false;
}

mode_t SkFsUtils::convertFilePermToMode(SkFilePerm &perm)
{
    SkString s;
    return convertFilePermToMode(perm, s);
}

mode_t SkFsUtils::convertFilePermToMode(SkFilePerm &perm, SkString &s)
{
    s = "0";
    s.concat(static_cast<uint>(perm.owner));
    s.concat(static_cast<uint>(perm.group));
    s.concat(static_cast<uint>(perm.other));

    return static_cast<mode_t>(strtol(s.c_str(), nullptr, 8));
}

bool SkFsUtils::chown(CStr *target, CStr *owner, CStr *group, bool resolveLinkToItsTarget)
{
    SkOsUser ownerProps;

    if (!SkOsEnv::userInfos(owner, ownerProps))
        return false;

    SkOsGroup groupProps;

    if (!SkOsEnv::groupInfos(group, groupProps))
        return false;

    return (SkFsUtils::chown(target, ownerProps, groupProps, resolveLinkToItsTarget));
}

bool SkFsUtils::chown(CStr *target, uid_t ownerID, gid_t groupID, bool resolveLinkToItsTarget)
{
    SkOsUser ownerProps;

    if (!SkOsEnv::userInfos(ownerID, ownerProps))
        return false;

    SkOsGroup groupProps;

    if (!SkOsEnv::groupInfos(groupID, groupProps))
        return false;

    return (SkFsUtils::chown(target, ownerProps, groupProps, resolveLinkToItsTarget));
}

bool SkFsUtils::chown(CStr *target, SkOsUser &owner, SkOsGroup &group, bool resolveLinkToItsTarget)
{
    if (!SkFsUtils::checkEmptyPathError(target, "Cannot make change ownership (because the pathName is empty)"))
        return false;

    if (!SkFsUtils::exists(target))
    {
        StaticError("Cannot make change ownership (because the pathName NOT exists): " <<  target);
        return false;
    }

    if (!SkFsUtils::isWritable(target))
    {
        StaticError("Cannot make change ownership (because it is NOT writable): " <<  target);
        return false;
    }

    StaticDebug("Changing ownership [uid: " << owner.uid << " -> " << owner.name << "]"
              << " and group [gid: " << group.gid << " -> " << group.name << "]: " << target << " ..");

    if (resolveLinkToItsTarget && (::chown(target, owner.uid, group.gid) == 0))
        return true;

    else
    {
        if (SkFsUtils::isSymLink(target) && (::lchown(target, owner.uid, group.gid) == 0))
            return true;

        else if (!SkFsUtils::isSymLink(target) && (::chown(target, owner.uid, group.gid) == 0))
            return true;
    }

    StaticError("Cannot change ownership and group to: " << target);
    return false;
}

SkString SkFsUtils::unixPermToString(CStr *target)
{
    SkInodeStat st;

    stat(target, &st);

    SkString permissions;
    SkFsUtils::unixPermToString(permissions, &st);
    return permissions;
}

void SkFsUtils::unixPermToString(SkString &permissions, SkInodeStat *status)
{
    if (!status->st_ino)
    {
        permissions = "---------";
        return;
    }

    mode_t &perm = status->st_mode;

    if (perm & S_IRUSR)
        permissions.append("r");
    else
        permissions.append("-");

    if (perm & S_IWUSR)
        permissions.append("w");
    else
        permissions.append("-");

    if (perm & S_IXUSR)
        permissions.append("x");
    else
        permissions.append("-");

    if (perm & S_IRGRP)
        permissions.append("r");
    else
        permissions.append("-");

    if (perm & S_IWGRP)
        permissions.append("w");
    else
        permissions.append("-");

    if (perm & S_IXGRP)
        permissions.append("x");
    else
        permissions.append("-");

    if (perm & S_IROTH)
        permissions.append("r");
    else
        permissions.append("-");

    if (perm & S_IWOTH)
        permissions.append("w");
    else
        permissions.append("-");

    if (perm & S_IXOTH)
        permissions.append("x");
    else
        permissions.append("-");
}

bool SkFsUtils::prepareCopyTarget(SkFileInfo &srcInfo, SkFileInfo &tgtInfo)
{
    if (!srcInfo.exists)
    {
        StaticError("Cannot prepare copy/move (because SOURCE does NOT exist): " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str());
        return false;
    }

    if (!srcInfo.isReadable)
    {
        StaticError("Cannot prepare copy/move (because SOURCE is NOT readable): " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str());
        return false;
    }

    /*if (tgtInfo.exists && tgtInfo.isDir)
    {
        if (!tgtInfo.isReadable)
        {
            StaticError("Cannot prepare copy/move (because TARGET-DIR is NOT readable): " << srcInfo.absoluteParentPath.c_str() << " > " << tgtInfo.absoluteParentPath.c_str());
            return false;
        }

        SkString &p = srcInfo.completeAbsolutePath;
        SkString p = SkFsUtils::adjustPathEndSeparator(srcInfo.completeAbsolutePath.c_str());
        p.concat(srcInfo.completeName);

        if (SkFsUtils::exists(p.c_str()))
        {
            if (!SkFsUtils::isReadable(p.c_str()))
            {
                StaticError("Cannot prepare copy/move (because TARGET is NOT readable): " << p << " > " << tgtInfo.completeAbsolutePath);
                return false;
            }

            if (!SkFsUtils::rm(p.c_str()))
            {
                StaticError("Cannot remove TARGET preparing copy/move: " << srcInfo.absoluteParentPath.c_str() << " > " << p.c_str());
                return false;
            }
        }
    }*/

    if (srcInfo.serialNO == tgtInfo.serialNO)
    {
        StaticError("Cannot prepare copy/move between files with the same serial number: " << srcInfo.completeAbsolutePath << " > " << tgtInfo.completeAbsolutePath);
        return false;
    }

    return true;
}

bool SkFsUtils::cp(CStr *source, CStr *target)
{
    if (SkFsUtils::checkEmptyPathError(source, "Cannot copy file (because the sourceName is empty)") == 0)
        return false;

    if (SkFsUtils::checkEmptyPathError(target, "Cannot copy file (because the targetName is empty)") == 0)
        return false;

    SkFileInfo srcInfo;
    SkFsUtils::fillFileInfo(source, srcInfo);

    SkFileInfo tgtInfo;
    SkFsUtils::fillFileInfo(target, tgtInfo);

    return SkFsUtils::cp(srcInfo, tgtInfo);
}

bool SkFsUtils::cp(SkFileInfo &srcInfo, SkFileInfo &tgtInfo)
{
    if (!srcInfo.isFile)
    {
        StaticError("Cannot copy file (because SOURCE is NOT a file): " <<  srcInfo.completeAbsolutePath);
        return false;
    }

    if (tgtInfo.exists && !tgtInfo.isFile)
    {
        StaticError("Cannot copy file (because TARGET exists and is NOT a file): " <<  tgtInfo.completeAbsolutePath);
        return false;
    }

    /*if (!tgtInfo.isFile)
    {
        StaticError("Cannot copy file (because TARGET is NOT a file): " <<  tgtInfo.completeAbsolutePath);
        return false;
    }*/

    if (!SkFsUtils::prepareCopyTarget(srcInfo, tgtInfo))
        return false;

    int rFD = ::open(srcInfo.completeAbsolutePath.c_str(), O_RDONLY);
    int wFD = ::open(tgtInfo.completeAbsolutePath.c_str(), O_WRONLY | O_CREAT, srcInfo.permRawMode);

    bool ok = true;

    // THIS IS NOT POSIX
    /*int64_t remain = srcInfo.size;
    StaticDebug("Copying: " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str() << " ..");

    while (remain > 0 && ok)
    {
        //Blast the bytes from one file to the other.
        int64_t ret = ::sendfile(wFD, rFD, nullptr,  static_cast<uint64_t>(remain));

        if (ret <= 0)
            ok = false;

        else
            remain -= static_cast<uint64_t>(ret);
    }*/

    char buffer[8192];
    Long bytesRead = 0;
    Long bytesWritten = 0;

    while ((bytesRead = ::read(rFD, buffer, sizeof(buffer))) > 0 && ok)
    {
        char *ptr = buffer;

        while ((bytesWritten = ::write(wFD, ptr, bytesRead)) > 0)
        {
            bytesRead -= bytesWritten;
            ptr += bytesWritten;
        }

        if (bytesWritten == -1)
            ok = false;
    }

    ::close (rFD);
    ::close (wFD);

    if (!ok)
        StaticError("Cannot copy file: " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str())
;
    return ok;
}

bool SkFsUtils::mv(CStr *source, CStr *target)
{
    if (SkFsUtils::checkEmptyPathError(source, "Cannot move/rename (because the sourceName is empty)") == 0)
        return false;

    if (SkFsUtils::checkEmptyPathError(target, "Cannot move/rename (because the targetName is empty)") == 0)
        return false;

    SkFileInfo srcInfo;
    SkFsUtils::fillFileInfo(source, srcInfo);

    SkFileInfo tgtInfo;
    SkFsUtils::fillFileInfo(target, tgtInfo);

    return SkFsUtils::mv(srcInfo, tgtInfo);
}

bool SkFsUtils::mv(SkFileInfo &srcInfo, SkFileInfo &tgtInfo)
{
    if (!SkFsUtils::prepareCopyTarget(srcInfo, tgtInfo))
        return false;

    if (!srcInfo.isWritable)
    {
        StaticError("Cannot move/rename (because SOURCE is NOT writable, removable): " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str());
        return false;
    }

    if (srcInfo.device == tgtInfo.device)
    {
        StaticDebug("Renaming: " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str() << " ..");

        if (::rename(srcInfo.completeAbsolutePath.c_str(), tgtInfo.completeAbsolutePath.c_str()) != 0)
        {
            StaticError("Cannot rename: " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str());
            return false;
        }

        return true;
    }

    StaticDebug("Moving file: " << srcInfo.completeAbsolutePath.c_str() << " > " << tgtInfo.completeAbsolutePath.c_str() << " ..");
    bool ok = cp(srcInfo, tgtInfo);

    if (ok)
        ok = rm(srcInfo.completeAbsolutePath.c_str());

    return ok;
}

bool SkFsUtils::rename(CStr *oldName, CStr *newName)
{
    return mv(oldName, newName);
}

bool SkFsUtils::rename(SkFileInfo &srcInfo, SkFileInfo &tgtInfo)
{
    return mv(srcInfo, tgtInfo);
}

bool SkFsUtils::ln(CStr *target, CStr *linkName)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot create link (because the pathName is empty)") == 0)
        return false;

    if (SkFsUtils::exists(linkName))
    {
        StaticError("Cannot create link (because the pathName ALREADY exists): " << linkName);
        return false;
    }

    StaticDebug("Creating symbolic link: " << linkName << " > " << target << " ..");
    bool ok = (::symlink(target, linkName) == 0);

    if (!ok)
        StaticError("Cannot create link: " << linkName << " > " << target);

    return ok;
}

bool SkFsUtils::touch(CStr *target, uint64_t size)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot touch file (because the pathName is empty)");

    if (!sz)
        return false;

    if (SkFsUtils::exists(target))
    {
        StaticError("Cannot touch file (because the pathName ALREADY exists): " << target);
        return false;
    }

    StaticDebug("Touching file (of zeros): " << target << " [size: " << size << " B] ..");

    bool ok = true;
    FILE *fp = fopen(target, "w");

    if (fp)
        ok = (::ftruncate(fileno(fp), static_cast<int64_t>(size)) == 0);

    else
    {
        StaticError("Cannot touch file (because cannot open for write): " << target);
        return false;
    }

    if (!ok)
    {
        StaticError("Cannot touch file: " << target);
        ok = false;
    }

    fclose(fp);
    return ok;
}

bool SkFsUtils::resize(CStr *target, uint64_t newSize)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot resize file (because the pathName is empty)");

    if (!sz)
        return false;

    if (!newSize)
    {
        StaticError("Cannot resize file (because new size is 0): " << target);
        return false;
    }

    SkFileInfo tgtInfo;
    SkFsUtils::fillFileInfo(target, tgtInfo);

    if (!tgtInfo.exists)
    {
        StaticError("Cannot resize file (because the pathName NOT exists): " << target);
        return false;
    }

    if (!tgtInfo.isRegularFile)
    {
        StaticError("Cannot resize file (because it is NOT a regular file): " << target);
        return false;
    }

    if (!SkFsUtils::isWritable(target))
    {
        StaticError("Cannot resize file (because SOURCE is NOT writable): " << target);
        return false;
    }

    StaticDebug("Resizing file: " << target << " [from " << sz << " B to " << newSize << " B] ..");
    bool ok = (::truncate(target, static_cast<off_t>(newSize)) == 0);

    if (!ok)
        StaticError("Cannot resize file: " << target << " [from " << sz << " B to " << newSize << " B]");

    return ok;
}

bool SkFsUtils::mkfifo(CStr *target, SkFilePerm &perm)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot create named-pipe (because the pathName is empty)") == 0)
        return false;

    if (SkFsUtils::exists(target))
    {
        StaticError("Cannot create named-pipe (because it ALREADY exists): " << target);
        return false;
    }

    StaticDebug("Making named-pipe: " << target << " ..");

    //PROBLEMs WITH MODE ARGUMENT FOR ::mkfifo
    /*SkString m;
    mode_t mode = SkFsUtils::convertFilePermToMode(perm, m);
    ::mkfifo(target, mode)
    StaticDebug("Making named-pipe [" << m << "] to: " << target << " ..");*/

    bool ok = (::mkfifo(target, S_IRUSR | S_IWUSR) == 0);

    if (ok)
        ok = SkFsUtils::chmod(target, perm);

    if (!ok)
        StaticError("Making named-pipe: " << target);

    return ok;
}

bool SkFsUtils::rm(CStr *target)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot remove file (because the pathName is empty)") == 0)
        return false;

    if (!SkFsUtils::isWritable(target))
    {
        StaticError("Cannot remove file (because it is NOT writable): " <<  target);
        return false;
    }

    StaticDebug("Removing target: " << target << " ..");

    if (SkFsUtils::exists(target))
        return (::remove(target) == 0);
        //unlink is NOT portable (it is Unix specific) and NOT remove directories
        //return (::unlink(target) == 0);

    StaticError("Cannot remove target: " <<  target);
    return false;
}

bool SkFsUtils::mkdir(CStr *target)
{
    if (SkFsUtils::checkEmptyPathError(target, "Cannot create directory (because the pathName is empty)") == 0)
        return false;

    if (SkFsUtils::exists(target))
    {
        StaticError("Cannot create directory (because the pathName ALREADY exists): " <<  target);
        return false;
    }

    StaticDebug("Making directory: " << target << " ..");
    bool ok = (::mkdir(target, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0);

    if (!ok)
        StaticError("Cannot create directory: " << target);

    return ok;
}

bool SkFsUtils::mkdirhier(CStr *target)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot create directory hierarchy (because the pathName is empty)");

    if (!sz)
        return false;

    if (SkFsUtils::exists(target))
    {
        StaticError("Cannot create directory hierarchy (because the pathName ALREADY exists): " <<  target);
        return false;
    }

    uint64_t plus = 1;

    if (target[sz-1] != SK_PATH_SEPARATOR[0])
        plus++;

    char *tgt = new char [sz+plus];
    ::memcpy(tgt, target, sz);

    if (plus == 1)
        tgt[sz] = '\0';
    else
    {
        tgt[sz] = SK_PATH_SEPARATOR[0];
        tgt[sz+1] = '\0';
    }

    StaticDebug("Making directory hierarchy: " << target << " ..");

    bool ok = true;
    char *p = ::strchr(tgt, SK_PATH_SEPARATOR[0]);

    while(p != nullptr)
    {
        *p = '\0';

        if (SkFsUtils::exists(tgt))
        {
            if (!SkFsUtils::isDir(tgt))
            {
                ok = false;
                StaticError("Cannot create directory hierarchy (because the pathName ALREADY exists and it is NOT a directory): " << target);
            }
        }

        else
            ok = mkdir(tgt);

        if (ok)
        {
            *p = SK_PATH_SEPARATOR[0];
            p++;
            p = ::strchr(p, SK_PATH_SEPARATOR[0]);
        }

        else
            p = nullptr;
    }

    delete [] tgt;
    return ok;
}

#if defined(SPECIALK_APPLICATION)
bool SkFsUtils::rmdir(CStr *target, bool removeRecursiveContents)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot remove directory (because the pathName is empty)");

    if (!sz)
        return false;

    if (!SkFsUtils::exists(target))
    {
        StaticError("Cannot remove directory (because the pathName NOT exists): " <<  target);
        return false;
    }

    if (!SkFsUtils::isDir(target))
    {
        StaticError("Cannot remove directory (because the pathName is NOT a directory): " <<  target);
        return false;
    }

    StaticDebug("Removing directory: " << target << " ..");

    SkFileInfosList *fl = new SkFileInfosList;
    SkFileInfo p;

    SkFsUtils::fillFileInfo(target, p);

    SkString s("FNFOLST_RMDIR");

    fl->setObjectName(s.c_str());

    if (!SkFsUtils::ls(target, fl, true))
    {
        fl->destroyLater();
        return false;
    }

    bool ok = true;

    if (fl->isEmpty())
        ok = SkFsUtils::rm(target);

    else
    {
        if (removeRecursiveContents)
        {
            //MUST CHECK CONTENTS AND REMOVE THEM

            if (!fl->open())
                return false;

            uint64_t i = fl->count();

            while(i>0 && ok)
            {
                if (!fl->select(i-1))
                    ok = false;

                ok = SkFsUtils::rm(fl->currentPath().c_str());

                i--;
            }

            if (ok)
                ok = SkFsUtils::rm(target);

            fl->close();
        }

        else
        {
            ok = false;
            StaticWarning("Cannot remove directory because it is NOT empty [count: " << fl->count() << "]: " << target);
        }
    }

    fl->destroyLater();
    return ok;
}
#endif

bool SkFsUtils::chdir(CStr *target)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot change current directory (because the pathName is empty)");

    if (!sz)
        return false;

    if (!SkFsUtils::exists(target))
    {
        StaticError("Cannot change current directory (because the pathName NOT exists): " <<  target);
        return false;
    }

    if (!SkFsUtils::isDir(target))
    {
        StaticError("Cannot change current directory (because it is NOT a directory): " <<  target);
        return false;
    }

    if (!SkFsUtils::isReadable(target))
    {
        StaticError("Cannot change current directory (because it is NOT readable): " <<  target);
        return false;
    }

    StaticDebug("Changing current directory: " << target << " ..");

    bool ok = (::chdir(target) == 0);

    if (!ok)
    {
        StaticError("Cannot change current directory: " <<  target);
        return false;
    }

    return ok;
}

SkString SkFsUtils::pwd()
{
    char currPath[PATH_MAX];
    ::getcwd(currPath, sizeof(currPath));
    return SkFsUtils::adjustPathEndSeparator(currPath);
}

#if defined(SPECIALK_APPLICATION)

bool SkFsUtils::ls(CStr *target,
                   SkFileInfosList *results,
                   //SkStringList nameFilters=SkStringList("*"),
                   bool recursive)
{
    uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot list directory contents (because the pathName is empty)");

    if (!sz)
        return false;

    if (!SkFsUtils::exists(target))
    {
        StaticError("Cannot open directory (because the pathName NOT exists): " <<  target);
        return false;
    }

    if (!SkFsUtils::isDir(target))
    {
        StaticError("Cannot open directory (because the pathName is NOT directory): " <<  target);
        return false;
    }

    if (!DynCast(SkFile, results)->open(SkFileMode::FLM_ONLYWRITE))
        return false;

    bool ok = true;

    if (recursive)
    {
        StaticDebug("Listing (recursive) directory: " << target << " ..");

        SkStringList subDirs;
        subDirs.append(target);

        while(!subDirs.isEmpty())
        {
            for(uint64_t i=0; i<subDirs.count() && ok; i++)
            {
                SkString dirName = subDirs.at(i);
                subDirs.remove(dirName);

                if (!lsPriv(dirName.c_str(), results, &subDirs))
                    ok = false;
            }
        }
    }

    else
    {
        StaticDebug("Listing (NOT recursive) directory: " << target << " ..");
        ok = lsPriv(target, results, nullptr);
    }

    results->close();

    if (!ok)
        SkFsUtils::rm(results->getFilePath());

    return ok;
}

bool SkFsUtils::lsPriv(CStr *target,
                       SkFileInfosList *results,
                       //QStringList nameFilters,
                       SkStringList *subDirs)
{
    if (!SkFsUtils::isReadable(target))
    {
        StaticError("Cannot open directory (because the pathName is NOT readable): " <<  target);
        return false;
    }

    DIR *d;
    d = opendir(target);

    struct dirent *dir;
    bool ok = true;

    if (d)
    {
        SkPathInfo info;

        while ((dir=::readdir(d)) != nullptr && ok)
        {
            if ((memcmp(dir->d_name, ".", strlen(dir->d_name)) != 0)
                    && (memcmp(dir->d_name, "..", strlen(dir->d_name)) != 0))
            {
                SkString p = SkFsUtils::adjustPathEndSeparator(target);
                p.concat(dir->d_name);

                if (!SkFsUtils::fillPathInfo(p .c_str(), info))
                    ok = false;

                bool isDir = SkFsUtils::isDir(info.completeAbsolutePath.c_str());
                results->append(info.completeAbsolutePath.c_str(), isDir);

                if (isDir && subDirs)
                    subDirs->append(info.completeAbsolutePath.c_str());
            }
        }

        ::closedir(d);
    }

    else
        return false;

    return ok;
}
#endif

bool SkFsUtils::writeTEXT(CStr *target, CStr *s)
{
    SkDataBuffer b;
    b.setData(s, SkString::size(s));
    return writeDATA(target, b);
}

bool SkFsUtils::readTEXT(CStr *source, SkString &text)
{
    SkDataBuffer b;

    if (!readDATA(source, b))
        return false;

    bool ok = b.copyTo(text);
    return ok;
}

bool SkFsUtils::writeJSON(CStr *target, SkArgsMap &m, bool indented, uint8_t spaces)
{
    SkVariant v(m);
    return writeJSON(target, v, indented, spaces);
}

bool SkFsUtils::writeJSON(CStr *target, SkVariantList &l, bool indented, uint8_t spaces)
{
    SkVariant v(l);
    return writeJSON(target, v, indented, spaces);
}

bool SkFsUtils::writeJSON(CStr *target, SkVariant &v, bool indented, uint8_t spaces)
{
    SkString json;
    v.toJson(json, indented, spaces);
    return writeTEXT(target, json.c_str());
}

bool SkFsUtils::readJSON(CStr *source, SkArgsMap &m)
{
    SkVariant v;

    if (!readJSON(source, v))
        return false;

    if (!v.isMap())
    {
        StaticError("Json is NOT a T_MAP: " <<  source);
        return false;
    }

    v.copyToMap(m);
    return true;
}

bool SkFsUtils::readJSON(CStr *source, SkVariantList &l)
{
    SkVariant v;

    if (!readJSON(source, v))
        return false;

    if (!v.isList())
    {
        StaticError("Json is NOT a T_LIST: " <<  source);
        return false;
    }

    v.copyToList(l);
    return true;
}

bool SkFsUtils::readJSON(CStr *source, SkVariant &v)
{
    SkString s;

    if (!readTEXT(source, s))
        return false;

    return v.fromJson(s.c_str());
}

bool SkFsUtils::writeDATA(CStr *target, SkDataBuffer &b)
{
    return writeDATA(target, b.data(), b.size());
}

bool SkFsUtils::writeDATA(CStr *target, CStr *data, ULong size)
{
    /*uint64_t sz = SkFsUtils::checkEmptyPathError(target, "Cannot write on file (because the pathName is empty)");

    if (!sz || !size)
        return false;

    SkFile *f = new SkFile;
    f->setObjectName(target);
    f->setFilePath(target);

    bool ok = false;

    if (f->open(SkFileMode::FLM_ONLYWRITE))
    {
        ok = f->write(data, size);
        f->close();
    }

    f->destroyLater();
    return ok;*/
    Long sz = SkFsUtils::checkEmptyPathError(target, "Cannot write on file (because the pathName is empty)");

    if (!sz || !size)
        return false;

    SkFlatFile f;
    f.setObjectName(target);
    f.setFilePath(target);

    bool ok = false;

    if (!f.open(SkFileMode::FLM_ONLYWRITE))
        return false;

    ok = f.write(data, size);
    f.close();

    return ok;
}

bool SkFsUtils::readDATA(CStr *source, SkDataBuffer &b)
{
    /*uint64_t sz = SkFsUtils::checkEmptyPathError(source, "Cannot read from file (because the pathName is empty)");

    if (!sz)
        return false;

    if (!SkFsUtils::exists(source))
    {
        StaticError("Cannot open file (because the pathName NOT exists): " <<  source);
        return false;
    }

    if (!SkFsUtils::isFile(source))
    {
        StaticError("Source is NOT a file: " <<  source);
        return false;
    }

    if (!SkFsUtils::isReadable(source))
    {
        StaticError("Cannot open file (because the pathName is NOT readable): " <<  source);
        return false;
    }

    SkFile *f = new SkFile;
    f->setObjectName(source);
    f->setFilePath(source);

    bool ok = false;

    if (f->open(SkFileMode::FLM_ONLYREAD))
    {
        ok = f->readAll(&b);
        f->close();
    }

    f->destroyLater();
    return ok;*/
    Long sz = SkFsUtils::checkEmptyPathError(source, "Cannot read from file (because the pathName is empty)");

    if (!sz)
        return false;

    if (!SkFsUtils::exists(source))
    {
        StaticError("Cannot open file (because the pathName NOT exists): " <<  source);
        return false;
    }

    if (!SkFsUtils::isFile(source))
    {
        StaticError("Source is NOT a file: " <<  source);
        return false;
    }

    if (!SkFsUtils::isReadable(source))
    {
        StaticError("Cannot open file (because the pathName is NOT readable): " <<  source);
        return false;
    }

    SkFlatFile f;
    f.setObjectName(source);
    f.setFilePath(source);

    bool ok = false;

    if (!f.open(SkFileMode::FLM_ONLYREAD))
        return false;

    ok = f.readAll(&b);
    f.close();

    return ok;
}

struct SkFsFromH_BIN
{
    const bool isDir;
    const char *relativePath;
    const char *name;
    const unsigned size;
    const bool isCompressed;
    const void *data;
};

struct SkFsFromH_TXT
{
    const bool isDir;
    const char *relativePath;
    const char *name;
    const unsigned size;
    const char *data;
};

void SkFsUtils::copyCodeHeaderFs(CVoid *array, unsigned int count, CStr *target, bool asBinary)
{
    SkString outputPath;

    if (asBinary)
    {
        const SkFsFromH_BIN *castedArray = (const SkFsFromH_BIN *) array;

        for(unsigned i=0; i<count; i++)
        {
            outputPath = SkFsUtils::adjustPathEndSeparator(target);
            outputPath.append(castedArray[i].relativePath);
            outputPath.append(castedArray[i].name);

            if (castedArray[i].isDir)
                SkFsUtils::mkdir(outputPath.c_str());

            else
            {
                if (castedArray[i].data)
                {
                    if (castedArray[i].isCompressed)
                    {
                        SkDataBuffer compressed;
                        SkDataBuffer uncompressed;
                        compressed.setData(castedArray[i].data, castedArray[i].size);
                        SkDataBuffer::decompress(compressed, uncompressed);
                        SkFsUtils::writeDATA(outputPath.c_str(), uncompressed);
                    }

                    else
                        SkFsUtils::writeDATA(outputPath.c_str(), SkArrayCast::toCStr(castedArray[i].data), castedArray[i].size);
                }
            }
        }
    }

    else
    {
        const SkFsFromH_TXT *castedArray = (const SkFsFromH_TXT *) array;

        for(unsigned i=0; i<count; i++)
        {
            outputPath = SkFsUtils::adjustPathEndSeparator(target);
            outputPath.append(castedArray[i].relativePath);
            outputPath.append(castedArray[i].name);

            if (castedArray[i].isDir)
                SkFsUtils::mkdir(outputPath.c_str());

            else if (castedArray[i].data)
                SkFsUtils::writeTEXT(outputPath.c_str(), castedArray[i].data);
        }
    }
}

uint64_t SkFsUtils::checkEmptyPathError(CStr *target, CStr *errMessage)
{
    if (SkString::isEmpty(target))
    {
        StaticError(errMessage);
        return 0;
    }

    uint64_t sz = ::strlen(target);

    return sz;
}
