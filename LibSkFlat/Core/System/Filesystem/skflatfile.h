#ifndef SKFLATFILE_H
#define SKFLATFILE_H

#include "Core/System/skabstractflatdevice.h"
#include "Core/Containers/skdatabuffer.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkFileMode
{
    FLM_NOMODE,
    FLM_ONLYREAD,               //the file is NOT created if it does not exist
    FLM_ONLYWRITE,              //the file is created if it does not exist; the file is TRUNCATED to zero if it exists
    FLM_ONLYWRITE_APPENDING,    //the file is created if it does not exist
    FLM_READWRITE,              //the file is NOT created if it does not exist
    FLM_READWRITE_APPENDING     //the file is created if it does not exist
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlatFile extends SkAbstractFlatDevice
{
    FILE *file;
    SkString filePath;
    SkFileMode openMode;

    public:
        SkFlatFile();

        void setFilePath(CStr *path);
        bool open(SkFileMode mode=SkFileMode::FLM_READWRITE);

        Long pos()                                                      override;
        bool rewind()                                                   override;
        bool seek(Long position)                                        override;
        bool atEof()                                                    override;
        Long canReadLine()                                              override;
        bool flush()                                                    override;
        Long size()                                                     override;
        Long bytesAvailable()                                           override;
        void close()                                                    override;

        bool exists();
        bool remove();

        SkFileMode currentMode();
        CStr *currentModeString();
        CStr *path();
        SkString hash(SkHashMode mode);

    protected:
        bool readInternal(char *data, Long &len, bool peek=false)       override;
        bool writeInternal(CStr *data, Long len)                        override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkString fileHash(CStr *path, SkHashMode mode=SkHashMode::HM_MD5);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFLATFILE_H
