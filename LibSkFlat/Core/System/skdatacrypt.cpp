#include "skdatacrypt.h"
#include "Core/sklogmachine.h"
#include "Core/Containers/skdatabuffer.h"
#include "Core/Containers/skarraycast.h"

#include <openssl/md5.h>
//#include <openssl/sha.h>
#include <openssl/pem.h>

#if defined(ENABLE_AES)
#include <openssl/aes.h>
#endif

#include <openssl/md4.h>
#include <openssl/ripemd.h>
#include <openssl/whrlpool.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_AES)

bool aes_init(uint8_t *key_data,
              int key_data_len,
              uint8_t *salt,
              SkCryptContext *e_ctx,
              SkCryptContext *d_ctx,
              SkCryptMode cm,
              SkHashMode hm);

uint8_t *aes_encrypt(SkCryptContext *e,
                     uint8_t *plaintext,
                     int *len);

uint8_t *aes_decrypt(SkCryptContext *e,
                     uint8_t *ciphertext,
                     int *len);

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/Containers/skargsmap.h"

DeclareWrapper_ENUM(SkHashMode)
{
    SetupEnumWrapper(SkHashMode);

    SetEnumItem(SkHashMode::HM_MD4);
    SetEnumItem(SkHashMode::HM_MD5);
    SetEnumItem(SkHashMode::HM_SHA1);
    SetEnumItem(SkHashMode::HM_SHA224);
    SetEnumItem(SkHashMode::HM_SHA256);
    SetEnumItem(SkHashMode::HM_SHA384);
    SetEnumItem(SkHashMode::HM_SHA512);
    SetEnumItem(SkHashMode::HM_RIPEMD160);
    SetEnumItem(SkHashMode::HM_WHIRLPOOL);
}

DeclareWrapper_ENUM(SkCryptMode)
{
    SetupEnumWrapper(SkCryptMode);

    SetEnumItem(SkCryptMode::CM_AES128_ECB);
    SetEnumItem(SkCryptMode::CM_AES128_CBC);
    SetEnumItem(SkCryptMode::CM_AES128_CFB1);
    SetEnumItem(SkCryptMode::CM_AES128_CFB8);
    SetEnumItem(SkCryptMode::CM_AES128_CFB128);
    SetEnumItem(SkCryptMode::CM_AES128_OFB);
    SetEnumItem(SkCryptMode::CM_AES128_CTR);
    SetEnumItem(SkCryptMode::CM_AES128_CCM);
    SetEnumItem(SkCryptMode::CM_AES128_GCM);
    SetEnumItem(SkCryptMode::CM_AES128_WRAP);
    SetEnumItem(SkCryptMode::CM_AES128_XTS);
    SetEnumItem(SkCryptMode::CM_AES192_ECB);
    SetEnumItem(SkCryptMode::CM_AES192_CBC);
    SetEnumItem(SkCryptMode::CM_AES192_CFB1);
    SetEnumItem(SkCryptMode::CM_AES192_CFB8);
    SetEnumItem(SkCryptMode::CM_AES192_CFB128);
    SetEnumItem(SkCryptMode::CM_AES192_OFB);
    SetEnumItem(SkCryptMode::CM_AES192_CTR);
    SetEnumItem(SkCryptMode::CM_AES192_CCM);
    SetEnumItem(SkCryptMode::CM_AES192_GCM);
    SetEnumItem(SkCryptMode::CM_AES192_WRAP);
    SetEnumItem(SkCryptMode::CM_AES256_ECB);
    SetEnumItem(SkCryptMode::CM_AES256_CBC);
    SetEnumItem(SkCryptMode::CM_AES256_CFB1);
    SetEnumItem(SkCryptMode::CM_AES256_CFB8);
    SetEnumItem(SkCryptMode::CM_AES256_CFB128);
    SetEnumItem(SkCryptMode::CM_AES256_OFB);
    SetEnumItem(SkCryptMode::CM_AES256_CTR);
    SetEnumItem(SkCryptMode::CM_AES256_CCM);
    SetEnumItem(SkCryptMode::CM_AES256_GCM);
    SetEnumItem(SkCryptMode::CM_AES256_WRAP);
    SetEnumItem(SkCryptMode::CM_AES256_XTS);
    SetEnumItem(SkCryptMode::CM_AES128_CBC_HMAC_SHA1);
    SetEnumItem(SkCryptMode::CM_AES256_CBC_HMAC_SHA1);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

DeclareWrapper_STRUCT_TOMAP(SkKeySalt)
{
    SetupStructWrapper(SkKeySalt);

    StructToMapProperty(hi);
    StructToMapProperty(lo);
}

DeclareWrapper_STRUCT_FROMMAP(SkKeySalt)
{
    MapToStructPropertyCasted(hi, uint32_t, toUInt32);
    MapToStructPropertyCasted(lo, uint32_t, toUInt32);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

DeclareWrapper(SkDataCrypt);

#if defined(ENABLE_AES)
    DeclareMeth_INSTANCE_RET(SkDataCrypt, open, bool, *Arg_Custom(SkKeySalt), Arg_StringRef)
    DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataCrypt, open, 1, bool, *Arg_Custom(SkKeySalt), Arg_StringRef, Arg_Enum(SkCryptMode))
    DeclareMeth_INSTANCE_RET_OVERLOAD(SkDataCrypt, open, 2, bool, *Arg_Custom(SkKeySalt), Arg_StringRef, Arg_Enum(SkCryptMode), Arg_Enum(SkHashMode))

    DeclareMeth_INSTANCE_VOID(SkDataCrypt, encrypt, Arg_Data(void), Arg_UInt64, *Arg_Custom(SkDataBuffer))
    DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, encrypt, 1, Arg_StringRef, *Arg_Custom(SkDataBuffer))
    DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, encrypt, 2, *Arg_Custom(SkDataBuffer), *Arg_Custom(SkDataBuffer))
    DeclareMeth_INSTANCE_VOID(SkDataCrypt, decrypt, Arg_Data(void), Arg_UInt64, Arg_StringRef)
    DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 1, *Arg_Custom(SkDataBuffer), Arg_StringRef)
    DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 2, Arg_Data(void), Arg_UInt64, *Arg_Custom(SkDataBuffer))
    DeclareMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 3, *Arg_Custom(SkDataBuffer), *Arg_Custom(SkDataBuffer))
    DeclareMeth_INSTANCE_VOID(SkDataCrypt, close)
#endif

DeclareMeth_STATIC_RET(SkDataCrypt, hash, SkString, Arg_Data(void), Arg_UInt64)
DeclareMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 1, SkString, Arg_Data(void), Arg_UInt64, Arg_Enum(SkHashMode))
DeclareMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 2, SkString, Arg_StringRef)
DeclareMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 3, SkString, Arg_StringRef, Arg_Enum(SkHashMode))
DeclareMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 4, SkString, *Arg_Custom(SkDataBuffer))
DeclareMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 5, SkString, *Arg_Custom(SkDataBuffer), Arg_Enum(SkHashMode))

SetupClassWrapper(SkDataCrypt)
{
    SetClassSuper(SkDataCrypt, SkFlatObject);

    AddEnumType(SkHashMode);
    AddEnumType(SkCryptMode);

    AddStructType(SkKeySalt);

#if defined(ENABLE_AES)
    AddMeth_INSTANCE_RET(SkDataCrypt, open);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataCrypt, open, 1);
    AddMeth_INSTANCE_RET_OVERLOAD(SkDataCrypt, open, 2);

    AddMeth_INSTANCE_VOID(SkDataCrypt, encrypt);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, encrypt, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, encrypt, 2);
    AddMeth_INSTANCE_VOID(SkDataCrypt, decrypt);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 1);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 2);
    AddMeth_INSTANCE_VOID_OVERLOAD(SkDataCrypt, decrypt, 3);
    AddMeth_INSTANCE_VOID(SkDataCrypt, close);
#endif

    AddMeth_STATIC_RET(SkDataCrypt, hash);
    AddMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 1);
    AddMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 2);
    AddMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 3);
    AddMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 4);
    AddMeth_STATIC_RET_OVERLOAD(SkDataCrypt, hash, 5);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkDataCrypt::SkDataCrypt()
{
    CreateClassWrapper(SkDataCrypt);
#if defined(ENABLE_AES)
    enabled = false;
    encryptor = nullptr;
    decryptor = nullptr;
#endif
}

#if defined(ENABLE_AES)

#include <openssl/evp.h>

bool SkDataCrypt::open(SkKeySalt &salt,
                       SkString &key,
                       SkCryptMode cryptMode,
                       SkHashMode hashMode)
{
    if (enabled)
    {
        FlatError("Cannot open, AES cipher is ALREADY open");
        return false;
    }

    encryptor = EVP_CIPHER_CTX_new();
    decryptor = EVP_CIPHER_CTX_new();

    //unsigned int s[] = {12345, 54321};
    uint32_t s[] = {salt.hi, salt.lo};
    uint8_t *key_data = (unsigned char *) key.c_str();
    uint64_t key_data_len = key.size();

    //gen key and iv. init the cipher ctx object
    if (!aes_init(key_data,
                  static_cast<int>(key_data_len),
                  SkArrayCast::toUInt8(&s),
                  encryptor,
                  decryptor,
                  cryptMode,
                  hashMode))
    {
        FlatError("Cannot initialize AES cipher");
        return false;
    }

    enabled = true;
    return true;
}

void SkDataCrypt::encrypt(void *input, uint64_t size, SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Cannot encrypt, AES cipher is NOT open");
        return;
    }

    uint8_t *in = SkArrayCast::toUInt8(input);

    int len = static_cast<int>(size);
    uint8_t *cipherData = aes_encrypt(encryptor, in, &len);

    output.setRawData(cipherData, static_cast<uint64_t>(len));
    //NOT delete [] plaintext;
}

void SkDataCrypt::encrypt(SkString &input, SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Cannot encrypt, AES cipher is NOT open");
        return;
    }

    encrypt((void *) input.c_str(), input.size(), output);
}

void SkDataCrypt::encrypt(SkDataBuffer &input, SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Cannot encrypt, AES cipher is NOT open");
        return;
    }

    encrypt((void *) input.data(), input.size(), output);
}

void SkDataCrypt::decrypt(void *input, uint64_t size, SkString &output)
{
    if (!enabled)
    {
        FlatError("Cannot decrypt, AES cipher is NOT open");
        return;
    }

    uint8_t *in = SkArrayCast::toUInt8(input);

    int len = static_cast<int>(size);
    char *plaintext = SkArrayCast::toChar(aes_decrypt(decryptor, in, &len));

    SkString o(plaintext, len);
    output.swap(o);

    delete [] plaintext;
}

void SkDataCrypt::decrypt(SkDataBuffer &input, SkString &output)
{
    if (!enabled)
    {
        FlatError("Cannot decrypt, AES cipher is NOT open");
        return;
    }

    decrypt((void *) input.data(), input.size(), output);
}

void SkDataCrypt::decrypt(void *input, uint64_t size, SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Cannot decrypt, AES cipher is NOT open");
        return;
    }

    uint8_t *in = SkArrayCast::toUInt8(input);

    int len = static_cast<int>(size);
    char *plaintext = SkArrayCast::toChar(aes_decrypt(decryptor, in, &len));

    output.setRawData(plaintext, static_cast<uint64_t>(len));
    //NOT delete [] plaintext;
}

void SkDataCrypt::decrypt(SkDataBuffer &input, SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Cannot decrypt, AES cipher is NOT open");
        return;
    }

    decrypt((void *) input.data(), input.size(), output);
}

void SkDataCrypt::close()
{
    if (!enabled)
    {
        FlatError("Cannot close, AES cipher is NOT open");
        return;
    }

    EVP_CIPHER_CTX_cleanup(encryptor);
    EVP_CIPHER_CTX_cleanup(decryptor);

    EVP_CIPHER_CTX_free(encryptor);
    EVP_CIPHER_CTX_free(decryptor);
}

#endif

SkString SkDataCrypt::hash(void *input, uint64_t size, SkHashMode mode)
{
    return bufferHash(input, size, mode);
}

SkString SkDataCrypt::hash(SkString &input, SkHashMode mode)
{
    return stringHash(input, mode);
}

SkString SkDataCrypt::hash(SkDataBuffer &input, SkHashMode mode)
{
    return bufferHash((void *) input.data(), input.size(), mode);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void binaryHash(void *buffer, uint64_t size, SkDataBuffer &output, SkHashMode mode)
{
    if (size == 0)
        return;

    uint8_t *fileBuffer = static_cast<uint8_t *>(buffer);

    uint64_t digestSize = 0;
    uint8_t *hash = nullptr;

    if (mode == SkHashMode::HM_MD4)
    {
        digestSize = MD4_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        MD4(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_MD5)
    {
        digestSize = MD5_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        MD5(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_SHA1)
    {
        digestSize = SHA_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        SHA1(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_SHA224)
    {
        digestSize = SHA224_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        SHA224(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_SHA256)
    {
        digestSize = SHA256_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        SHA256(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_SHA384)
    {
        digestSize = SHA384_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        SHA384(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_SHA512)
    {
        digestSize = SHA512_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        SHA512(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_RIPEMD160)
    {
        digestSize = RIPEMD160_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        RIPEMD160(fileBuffer, size, hash);
    }

    else if (mode == SkHashMode::HM_WHIRLPOOL)
    {
        digestSize = WHIRLPOOL_DIGEST_LENGTH;
        hash = new uint8_t [digestSize];
        WHIRLPOOL(fileBuffer, size, hash);
    }

    output.setData(hash, digestSize);
    delete [] hash;
}

SkString bufferHash(void *buffer, uint64_t size, SkHashMode mode)
{
    if (size == 0)
        return "-";

    SkDataBuffer b;
    binaryHash(buffer, size, b, mode);

    ostringstream sout;
    sout << hex << setfill('0');

    for(uint64_t i=0; i<b.size(); i++)
        sout << hex << setw(2) << setfill('0') << static_cast<int>(SkArrayCast::toUInt8(b.toVoid())[i]);

    return sout.str().c_str();
}

SkString stringHash(SkString &s, SkHashMode mode)
{
    return bufferHash((void *) s.c_str(), s.size(), mode);
}

#include "Core/System/Time/skdatetime.h"

SkString uniqueID(SkHashMode mode, CStr *surplus)
{
    random_device rd;
    mt19937 generator(rd());
    int minMs = 1;
    int maxMs = 1000000000;
    uniform_int_distribution<int> distribution(minMs, maxMs);

    SkString str(distribution(generator));
    str.append(SkDateTime::currentDateTimeISOString());
    SkString rndStr(distribution(generator));
    str.append(rndStr);

    if (!SkString::isEmpty(surplus))
        str.append(surplus);

    return stringHash(str, mode);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void b64enc(void *input, uint64_t size, SkDataBuffer &output)
{
    //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    BIO *b64_bio, *mem_bio;

    //Pointer to a "memory BIO" structure holding our base64 data.
    BUF_MEM *mem_bio_mem_ptr;

    //Initialize our base64 filter BIO.
    b64_bio = BIO_new(BIO_f_base64());

    //Initialize our memory sink BIO.
    mem_bio = BIO_new(BIO_s_mem());

    //Link the BIOs by creating a filter-sink BIO chain.
    BIO_push(b64_bio, mem_bio);

    //No newlines every 64 characters or less.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);

    //Records base64 encoded data.
    BIO_write(b64_bio, input, static_cast<int>(size));

    //Flush data.  Necessary for b64 encoding, because of pad characters.
    BIO_flush(b64_bio);

    //Store address of mem_bio's memory structure.
    BIO_get_mem_ptr(mem_bio, &mem_bio_mem_ptr);

    //Permit access to mem_ptr after BIOs are destroyed.
    BIO_set_close(mem_bio, BIO_NOCLOSE);

    //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    BIO_free_all(b64_bio);

    //Makes space for end null.
    BUF_MEM_grow(mem_bio_mem_ptr, (*mem_bio_mem_ptr).length/* + 1*/);

    //Adds null-terminator to tail.
    //(*mem_bio_mem_ptr).data[(*mem_bio_mem_ptr).length] = '\0';

    //Returns base-64 encoded data. (See: "buf_mem_st" struct).

    output.setData((*mem_bio_mem_ptr).data, (*mem_bio_mem_ptr).length, false);
    //CANNOT USE RAW DATA BECAUSE SURELY IT IS NOT ALLOCATED WITH new
    free((*mem_bio_mem_ptr).data);
}

void b64dec (void *input, uint64_t size, SkDataBuffer &output)
{
    //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    BIO *b64_bio, *mem_bio;

    uint64_t outputSize = (size*3)/4+1;
    //+1 = null.
    char *base64_decoded = new char [outputSize];
    memset(base64_decoded, 0, outputSize);

    //Initialize our base64 filter BIO.
    b64_bio = BIO_new(BIO_f_base64());

    //Initialize our memory source BIO.
    mem_bio = BIO_new(BIO_s_mem());

    //Base64 data saved in source.
    BIO_write(mem_bio, input, static_cast<int>(size));

    //Link the BIOs by creating a filter-source BIO chain.
    BIO_push(b64_bio, mem_bio);

    //Don't require trailing newlines.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);

    //Index where the next base64_decoded byte should be written.
    int decoded_byte_index = 0;

    //Read byte-by-byte.
    while ( 0 < BIO_read(b64_bio, base64_decoded+decoded_byte_index, 1) )
        decoded_byte_index++; //Increment the index until read of BIO decoded data is complete.
    //Once we're done reading decoded data, BIO_read returns -1 even though there's no error.

    //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    BIO_free_all(b64_bio);

    //Returns base-64 decoded data with trailing null terminator.
    output.setRawData(base64_decoded, outputSize, false);
    //USED RAW DATA
    //delete [] base64_decoded;
}

void b64enc(SkDataBuffer &input, SkDataBuffer &output)
{
    input.toBase64(output);
}

void b64dec(SkDataBuffer &input, SkDataBuffer &output)
{
    output.fromBase64(input);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_AES)

/*
 * Create a 256 bit key and IV using the supplied key_data. salt can be added for taste.
 * Fills in the encryption and decryption ctx objects and returns 0 on success
 */
/**/

#define AES_INIT(BITS, SUFFIX) \
    if (hm == SkHashMode::HM_MD4) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_md4(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_MD5) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_md5(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_SHA1) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_sha1(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_SHA224) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_sha224(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_SHA256) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_sha256(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_SHA384) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_sha384(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_SHA512) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_sha512(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_RIPEMD160) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_ripemd160(), salt, k, k_len, nrounds, key, iv); \
    else if (hm == SkHashMode::HM_WHIRLPOOL) \
        i = EVP_BytesToKey(EVP_aes_ ## BITS ## _ ## SUFFIX, EVP_whirlpool(), salt, k, k_len, nrounds, key, iv); \
    \
    if (i != BITS/8) \
    { \
        printf("Key size is %d bits - should be %d bits\n", i, BITS/8); \
        return false; \
    } \
    \
    EVP_CIPHER_CTX_init(e_ctx); \
    EVP_EncryptInit_ex(e_ctx, EVP_aes_ ## BITS ## _ ## SUFFIX, nullptr, key, iv); \
    EVP_CIPHER_CTX_init(d_ctx); \
    EVP_DecryptInit_ex(d_ctx, EVP_aes_ ## BITS ## _ ## SUFFIX, nullptr, key, iv)

bool aes_init(uint8_t *k,
              int k_len,
              uint8_t *salt,
              SkCryptContext *e_ctx,
              SkCryptContext *d_ctx,
              SkCryptMode cm,
              SkHashMode hm)
{
    int i=0, nrounds=5;
    unsigned char key[32], iv[32];

    if (cm == SkCryptMode::CM_AES128_ECB)
    {AES_INIT(128, ecb());}
    else if (cm == SkCryptMode::CM_AES128_CBC)
    {AES_INIT(128, cbc());}
    else if (cm == SkCryptMode::CM_AES128_CFB1)
    {AES_INIT(128, cfb1());}
    else if (cm == SkCryptMode::CM_AES128_CFB8)
    {AES_INIT(128, cfb8());}
    else if (cm == SkCryptMode::CM_AES128_CFB128)
    {AES_INIT(128, cfb128());}
    else if (cm == SkCryptMode::CM_AES128_OFB)
    {AES_INIT(128, ofb());}
    else if (cm == SkCryptMode::CM_AES128_CTR)
    {AES_INIT(128, ctr());}
    else if (cm == SkCryptMode::CM_AES128_CCM)
    {AES_INIT(128, ccm());}
    else if (cm == SkCryptMode::CM_AES128_GCM)
    {AES_INIT(128, gcm());}
    else if (cm == SkCryptMode::CM_AES128_WRAP)
    {AES_INIT(128, wrap());}
    else if (cm == SkCryptMode::CM_AES128_XTS)
    {AES_INIT(128, xts());}
    else if (cm == SkCryptMode::CM_AES192_ECB)
    {AES_INIT(192, ecb());}
    else if (cm == SkCryptMode::CM_AES192_CBC)
    {AES_INIT(192, cbc());}
    else if (cm == SkCryptMode::CM_AES192_CFB1)
    {AES_INIT(192, cfb1());}
    else if (cm == SkCryptMode::CM_AES192_CFB8)
    {AES_INIT(192, cfb8());}
    else if (cm == SkCryptMode::CM_AES192_CFB128)
    {AES_INIT(192, cfb128());}
    else if (cm == SkCryptMode::CM_AES192_OFB)
    {AES_INIT(192, ofb());}
    else if (cm == SkCryptMode::CM_AES192_CTR)
    {AES_INIT(192, ctr());}
    else if (cm == SkCryptMode::CM_AES192_CCM)
    {AES_INIT(192, ccm());}
    else if (cm == SkCryptMode::CM_AES192_GCM)
    {AES_INIT(192, gcm());}
    else if (cm == SkCryptMode::CM_AES192_WRAP)
    {AES_INIT(192, wrap());}
    else if (cm == SkCryptMode::CM_AES256_ECB)
    {AES_INIT(256, ecb());}
    else if (cm == SkCryptMode::CM_AES256_CBC)
    {AES_INIT(256, cbc());}
    else if (cm == SkCryptMode::CM_AES256_CFB1)
    {AES_INIT(256, cfb1());}
    else if (cm == SkCryptMode::CM_AES256_CFB8)
    {AES_INIT(256, cfb8());}
    else if (cm == SkCryptMode::CM_AES256_CFB128)
    {AES_INIT(256, cfb128());}
    else if (cm == SkCryptMode::CM_AES256_OFB)
    {AES_INIT(256, ofb());}
    else if (cm == SkCryptMode::CM_AES256_CTR)
    {AES_INIT(256, ctr());}
    else if (cm == SkCryptMode::CM_AES256_CCM)
    {AES_INIT(256, ccm());}
    else if (cm == SkCryptMode::CM_AES256_GCM)
    {AES_INIT(256, gcm());}
    else if (cm == SkCryptMode::CM_AES256_WRAP)
    {AES_INIT(256, wrap());}
    else if (cm == SkCryptMode::CM_AES256_XTS)
    {AES_INIT(256, xts());}
    else if (cm == SkCryptMode::CM_AES128_CBC_HMAC_SHA1)
    {AES_INIT(128, cbc_hmac_sha1());}
    else if (cm == SkCryptMode::CM_AES256_CBC_HMAC_SHA1)
    {AES_INIT(256, cbc_hmac_sha1());}

    return true;
}

/*
     * Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the supplied key material.
     * nrounds is the number of times the we hash the material. More rounds are more secure but
     * slower.
     */
/*i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt, k, k_len, nrounds, key, iv);

    if (i != 256/8)
    {
        printf("Key size is %d bits - should be %d bits\n", i, 256/8);
        return false;
    }

    EVP_CIPHER_CTX_init(e_ctx);
    EVP_CIPHER_CTX_init(d_ctx);

    EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), nullptr, key, iv);
    EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), nullptr, key, iv);*/

/*
 * Encrypt *len bytes of data
 * All data going in & out is considered binary (unsigned char[])
 */

uint8_t *aes_encrypt(SkCryptContext *e, uint8_t *plaintext, int *len)
{
    /* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1 bytes */
    int c_len = *len + AES_BLOCK_SIZE, f_len = 0;
    uint8_t *ciphertext = new uint8_t [c_len];

    /* allows reusing of 'e' for multiple encryption cycles */
    EVP_EncryptInit_ex(e, nullptr, nullptr, nullptr, nullptr);

    /* update ciphertext, c_len is filled with the length of ciphertext generated,
      *len is the size of plaintext in bytes */
    EVP_EncryptUpdate(e, ciphertext, &c_len, plaintext, *len);

    /* update ciphertext with the final remaining bytes */
    EVP_EncryptFinal_ex(e, ciphertext+c_len, &f_len);

    *len = c_len + f_len;
    return ciphertext;
}

/*
 * Decrypt *len bytes of ciphertext
 */

uint8_t *aes_decrypt(SkCryptContext *e, uint8_t *ciphertext, int *len)
{
    /* plaintext will always be equal to or lesser than length of ciphertext*/
    int p_len = *len, f_len = 0;
    uint8_t *plaintext = new uint8_t [p_len];

    EVP_DecryptInit_ex(e, nullptr, nullptr, nullptr, nullptr);
    EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, *len);
    EVP_DecryptFinal_ex(e, plaintext+p_len, &f_len);

    *len = p_len + f_len;
    return plaintext;
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*int fakeMain(int , char **argv)
{
    //"opaque" encryption, decryption ctx structures that libcrypto uses to record
    //status of enc/dec operations
    EVP_CIPHER_CTX en, de;

    //8 bytes to salt the key_data during key generation. This is an example of
    // compiled in salt. We just read the bit pattern created by these two 4 byte
    // integers on the stack as 64 bits of contigous salt material -
    // ofcourse this only works if sizeof(int) >= 4
    unsigned int salt[] = {12345, 54321};
    unsigned char *key_data;
    int key_data_len, i;
    char *input[] = {"a", "abcd", "this is a test", "this is a bigger test",
                     "\nWho are you ?\nI am the 'Doctor'.\n'Doctor' who ?\nPrecisely!",
                     nullptr};

    //the key_data is read from the argument list
    key_data = (unsigned char *)argv[1];
    key_data_len = strlen(argv[1]);

    //gen key and iv. init the cipher ctx object
    if (aes_init(key_data, key_data_len, (unsigned char *)&salt, &en, &de)) {
      printf("Couldn't initialize AES cipher\n");
      return -1;
    }

    //encrypt and decrypt each input string and compare with the original
    for (i = 0; input[i]; i++) {
      char *plaintext;
      unsigned char *ciphertext;
      int olen, len;

      //The enc/dec functions deal with binary data and not C strings. strlen() will
      // return length of the string without counting the '\0' string marker. We always
      // pass in the marker byte to the encrypt/decrypt functions so that after decryption
      // we end up with a legal C string
      olen = len = strlen(input[i])+1;

      ciphertext = aes_encrypt(&en, (unsigned char *)input[i], &len);
      plaintext = (char *)aes_decrypt(&de, ciphertext, &len);

      if (strncmp(plaintext, input[i], olen))
        printf("FAIL: enc/dec failed for \"%s\"\n", input[i]);
      else
        printf("OK: enc/dec ok for \"%s\"\n", plaintext);

      free(ciphertext);
      free(plaintext);
  }

  EVP_CIPHER_CTX_cleanup(&en);
  EVP_CIPHER_CTX_cleanup(&de);

  return 0;
}*/
