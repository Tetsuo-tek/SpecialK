/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKSYSTEM_H
#define SKSYSTEM_H

/**
 * @file sksystem.h
 */

#include "Core/Object/skflatobject.h"

//HERE WE GRAB HARDWARE-SYSTEM INFOs

struct SkRamInfo
{
    long pages;
    long page_size;

    u_int64_t total;
    u_int64_t free;

    SkRamInfo()
    {
        pages = 0;
        page_size = 0;

        total = 0;
        free = 0;
    }
};

class SPECIALK SkSysConf extends SkFlatObject
{
    public:
        SkSysConf();

        static void getMemoryInfo(SkRamInfo &info);
        static long getTickSpeed();
        static long getProcessorsCount();
        static long getOnlineProcessorsCount();
};


#endif // SKSYSTEM_H
