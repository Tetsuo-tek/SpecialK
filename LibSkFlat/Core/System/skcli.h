#ifndef SKCLI_H
#define SKCLI_H

#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skstringlist.h"
#include "Core/Containers/skvariant.h"

//CLI example:
/*
    $ ./application sss --help dd  -p jj pslfps sgijfgisjgs
    --help : dd
    -p : jj
    0 : sss <- FLOATING VALUE
    1 : pslfps
    2 : sgijfgisjgs
*/

struct SkCliParameter
{
    bool used;
    SkString label;
    SkString abbr;
    SkVariant value;
    SkVariant defaultValue;
    SkString desc;
};

class SkCli extends SkFlatObject
{
    SkStringList appCli;
    SkTreeMap<SkString, SkCliParameter *> allowedCli;
    SkVariantVector floatingArgs;
    SkVariant nv;
    SkString executableName;

    public:
        SkCli(int argc, char *argv[]);

        bool add(CStr *label, CStr *abbr, CStr *defaultValue, CStr *desc);

        void setValue(CStr *label, CStr *val);
        void resetValue(CStr *label);
        bool isUsed(CStr *label);

        SkVariant &value(CStr *label);
        SkVariant &floatingValue(ULong floatingID);//a value without label (--* or -*) is a Floating value

        bool check();

        SkStringList &userAppCli();
        SkString help();

        ULong floatingParametersCount();
        void labels(SkStringList &l);//added with add(..)
};

#endif // SKCLI_H
