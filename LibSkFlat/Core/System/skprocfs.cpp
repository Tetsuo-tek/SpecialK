#include "skprocfs.h"
#include <unistd.h>
/*
#include "Filesystem/skfsutils.h"

SkProcFs::SkProcFs()
{
    file = nullptr;
}

bool SkProcFs::cpuInfo(SkTreeMap<SkString, SkVariant> &m)
{
    SkString content;
    SkString procFile = "/proc/cpuinfo";

    if (!read(procFile.c_str(), content))
        return false;

    //cout << "!!!! " << content<< "\n";

    SkStringList rows;
    content.split("\n", rows);

    SkTreeMap<SkString, SkVariant> tempMap;

    SkString cpuName;
    tempMap.clear();

    int cpuCount = 0;

    for(ULong i=0; i<rows.count(); i++)
    {
        SkStringList pair;
        SkString &row = rows[i];
        row.trim();

        if (row.isEmpty())
            continue;

        row.split(":", pair);

        if (pair.count() < 2)
        {
            FlatWarning("Pair NOT valid [row: " << i << "; file: " << procFile << "] -> " << row);
            continue;
        }

        SkString k = pair.first();
        pair.removeFirst();

        k.trim();
        k.replace(" ", "_");

        if (k == "processor")
        {
            cpuName = "cpu";
            cpuName.concat(cpuCount);

            if (i>0)
            {
                m[cpuName] = tempMap;
                tempMap.clear();
                cpuCount++;
            }
        }

        SkString v = pair.join(":");
        v.trim();

        tempMap[k] = v;
    }

    cpuName = "cpu";
    cpuName.concat(cpuCount);
    m[cpuName] = tempMap;
    cpuCount++;

    m["cores"] = cpuCount;

    return true;
}

bool SkProcFs::stat(SkTreeMap<SkString, SkVariant> &m)
{
    SkString content;
    SkString procFile = "/proc/stat";

    if (!read(procFile.c_str(), content))
        return false;

    cout << "!!!! " << content<< "\n";

    SkStringList rows;
    content.split("\n", rows);

    for(ULong i=0; i<rows.count(); i++)
    {
        SkStringList splitted;
        SkString &row = rows[i];
        row.trim();

        if (row.isEmpty())
            continue;

        row.split(" ", splitted);

        SkString k = splitted.first();
        k.trim();

        if (k.startsWith("cpu"))
        {
            splitted[1].toLongLong();
        }
    }

    return true;
}

bool SkProcFs::read(CStr *procFilePath, SkString &content)
{
    content.clear();

    if (!SkFsUtils::isReadable(procFilePath) || !SkFsUtils::isFile(procFilePath))
    {
        FlatWarning("CANNOT read file: " << procFilePath);
        return false;
    }

    file = ::fopen(procFilePath, "r");

    if (!file)
    {
        FlatWarning("CANNOT open file: " << procFilePath);
        return false;
    }

    ULong len = 50*1024*1024;
    char *data = new char [len+1];
    memset(data, '\0', len+1);

    //atomic read
    len = ::fread(data, sizeof(char), len, file);
    ::fclose(file);

    if (len)
    {
        content.append(data, len);
        delete [] data;
        return true;
    }

    FlatWarning("File is EMPTY: " << procFilePath);
    delete [] data;
    return false;
}*/
