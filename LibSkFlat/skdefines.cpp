#include "skdefines.h"

#if defined(ENABLE_SKAPP)
    #include "Core/Object/skobject.h"
#else
    #include "Core/sklogmachine.h"
    #include "Core/Containers/skargsmap.h"
    #include "Core/Containers/skvariant.h"
    #include "Core/Containers/skstringlist.h"
#endif

static SkClassType **flatTypes = nullptr;
static uint64_t flatTypesCount = 0;

static SkClassTypeRegLambda **flatTypesRegLambdas = nullptr;
static uint64_t flatTypesRegLambdasCount = 0;

static SkStructType **structTypes = nullptr;
static uint64_t structTypesCount = 0;

static SkEnumType **enumTypes = nullptr;
static uint64_t enumTypesCount = 0;

// // // // // // // // // // // // // // // // // // // // //

void registerMethodArgsTypes(SkMethod &meth, CStr *pseudoTypes)
{
    SkString s(pseudoTypes);
    SkStringList l;
    s.split(",", l);

    SkStringList variantTypesNames;

    meth.args_T = static_cast<SkVariant_T *>(malloc(l.count()*sizeof(SkVariant_T)));

    for(uint64_t i=0; i<l.count(); i++)
    {
        l[i].trim();

        if (l[i] == "Arg_AbstractVariadic")
            meth.args_T[i] = SkVariant_T::T_NULL;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_CStr" || l[i] == "Arg_String" || l[i] == "Arg_StringRef")
            meth.args_T[i] = SkVariant_T::T_STRING;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_Bool" || l[i] == "Arg_Bool_REAL")
            meth.args_T[i] = SkVariant_T::T_BOOL;

        /*else if (l[i] == "Arg_Char" || l[i] == "Arg_Char_REAL")
            meth.args_T[i] = SkVariant_T::T_CHAR;*/

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_UInt64" || l[i] == "Arg_UInt64_REAL")
            meth.args_T[i] = SkVariant_T::T_UINT64;

        else if (l[i] == "Arg_UInt32" || l[i] == "Arg_UInt32_REAL")
            meth.args_T[i] = SkVariant_T::T_UINT32;

        else if (l[i] == "Arg_UInt16" || l[i] == "Arg_UInt16_REAL")
            meth.args_T[i] = SkVariant_T::T_UINT16;

        else if (l[i] == "Arg_UInt8" || l[i] == "Arg_UInt8_REAL")
            meth.args_T[i] = SkVariant_T::T_UINT8;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_Int64" || l[i] == "Arg_Int64_REAL")
            meth.args_T[i] = SkVariant_T::T_INT64;

        else if (l[i] == "Arg_Int32" || l[i] == "Arg_Int32_REAL")
            meth.args_T[i] = SkVariant_T::T_INT32;

        else if (l[i] == "Arg_Int16" || l[i] == "Arg_Int16_REAL")
            meth.args_T[i] = SkVariant_T::T_INT16;

        else if (l[i] == "Arg_Int8" || l[i] == "Arg_Int8_REAL")
            meth.args_T[i] = SkVariant_T::T_INT8;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_Float" || l[i] == "Arg_Float_REAL")
            meth.args_T[i] = SkVariant_T::T_FLOAT;

        else if (l[i] == "Arg_Double" || l[i] == "Arg_Double_REAL")
            meth.args_T[i] = SkVariant_T::T_DOUBLE;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i] == "Arg_UInt")
            meth.args_T[i] = SkVariant_T::T_UINT64;

        else if (l[i] == "Arg_Int")
            meth.args_T[i] = SkVariant_T::T_INT64;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i].startsWith("Arg_Enum("))
            meth.args_T[i] = SkVariant_T::T_INT16;

        // // // // // // // // // // // // // // // // // // // // //

        else if (l[i].startsWith("Arg_Custom(") || l[i].c_str()[0] == '*')
            meth.args_T[i] = SkVariant_T::T_RAWPOINTER;

        // // // // // // // // // // // // // // // // // // // // //

        variantTypesNames << SkVariant::variantTypeName(meth.args_T[i]);
    }

    meth.argsCount = variantTypesNames.count();

    StaticPlusDebug("Register arguments types and number [" << meth.argsCount<< "] for Method: "
               << meth.t->name << "::" << meth.name << "(" << variantTypesNames.join(", ") << ")");
}

void createParamsTypesListOnPDG(CStr *message, SkMethod &meth, SkVariant **p, uint64_t pCount)
{
    if (!logger->isPlusDebugEnabled())
        return;

    if (pCount)
    {
        SkStringList l;

        for(uint64_t i=0; i<pCount; i++)
            l << p[i]->variantTypeName();

        StaticPlusDebug(message << " [params: " << pCount << "; isOverLoad: " << SkVariant::boolToString(meth.isOverload) << "]: "
                   << meth.t->name << "::" << meth.name << "(" << l.join(", ") << ")");
    }

    else
        StaticPlusDebug(message << " [params: " << pCount << "; isOverLoad: " << SkVariant::boolToString(meth.isOverload) << "]: "
                   << meth.t->name << "::" << meth.name << "()");
}

#define PARAMS_1(P)    P p[0]
#define PARAMS_2(P)    P p[1], P p[0]
#define PARAMS_3(P)    P p[2], P p[1], P p[0]
#define PARAMS_4(P)    P p[3], P p[2], P p[1], P p[0]
#define PARAMS_5(P)    P p[4], P p[3], P p[2], P p[1], P p[0]
#define PARAMS_6(P)    P p[5], P p[4], P p[3], P p[2], P p[1], P p[0]
#define PARAMS_7(P)    P p[6], P p[5], P p[4], P p[3], P p[2], P p[1], P p[0]
#define PARAMS_8(P)    P p[7], P p[6], P p[5], P p[4], P p[3], P p[2], P p[1], P p[0]
#define PARAMS_9(P)    P p[8], P p[7], P p[6], P p[5], P p[4], P p[3], P p[2], P p[1], P p[0]
#define PARAMS_10(P)   P p[9], P p[8], P p[7], P p[6], P p[5], P p[4], P p[3], P p[2], P p[1], P p[0]

#define NO_PFX

#define FROM_PTR_PAR(COUNT) \
        PARAMS_ ## COUNT (NO_PFX)

#define FROM_NOT_PTR_PAR(COUNT) \
        PARAMS_ ## COUNT (&)


// // // // // // // // // // // // // // // // // // // // //

static SkString skVersionString;

StaticAutoLaunch(SkCoreInit, skCoreInit)
{
    skVersionString = SkString::number(SK_MAJORVERSION);
    skVersionString.append(".");
    skVersionString.concat(SK_MINORVERSION);
}
/*struct SkCoreInit
{
    SkCoreInit()
    {
        skVersionString = SkString::number(SK_MAJORVERSION);
        skVersionString.append(".");
        skVersionString.concat(SK_MINORVERSION);
    }

    SkString skVersionString;
} skCoreInit;*/

CStr *SpecialK_VERSION()
{
    return skVersionString.c_str();
}

uint64_t getClassTypesCount()
{
    return flatTypesCount;
}

SkClassType **classTypesPtr(uint64_t &count)
{
    count = flatTypesCount;
    return flatTypes;
}

SkClassType **classTypesPtr()
{
    return flatTypes;
}

bool existsClassType(CStr *typeName)
{
    for(uint64_t i=0; i<flatTypesCount; i++)
        if (SkString::compare(typeName, flatTypes[i]->name))
            return true;

    for(uint64_t i=0; i<flatTypesRegLambdasCount; i++)
        if (SkString::compare(typeName, flatTypesRegLambdas[i]->name))
            return true;

    return false;
}

int64_t classTypeID(CStr *typeName)
{
    for(uint64_t i=0; i<flatTypesCount; i++)
        if (SkString::compare(typeName, flatTypes[i]->name))
            return static_cast<int64_t>(i);

    return -1;
}

SkClassType *searchClassType(CStr *typeName)
{
    for(uint64_t i=0; i<flatTypesCount; i++)
        if (SkString::compare(typeName, flatTypes[i]->name))
            return flatTypes[i];

    uint64_t lastFlatTypesCount = flatTypesCount;

    for(uint64_t i=0; i<flatTypesRegLambdasCount; i++)
    {
        if (SkString::compare(typeName, flatTypesRegLambdas[i]->name))
        {
            flatTypesRegLambdas[i]->lambda();

            /*cout << "> " << typeName << "\n";
            return searchClassType(typeName);*/

            for(uint64_t w=lastFlatTypesCount; w<flatTypesCount; w++)
                if (SkString::compare(typeName, flatTypes[w]->name))
                    return flatTypes[w];
        }
    }

    return nullptr;
}

SkClassType *addClassType(CStr *typeName, ClassNew *instancer)
{
    flatTypes = static_cast<SkClassType**>(realloc(flatTypes, (flatTypesCount+1) * sizeof(SkClassType *)));

    SkClassType *t = static_cast<SkClassType *>(malloc(sizeof(SkClassType)));
    t->id = flatTypesCount;
    t->name = typeName;
    t->super = nullptr;

    t->instancer = instancer;

    t->methods = nullptr;
    t->methsCount = 0;

    t->staticMethods = nullptr;
    t->staticMethsCount = 0;

#if defined(ENABLE_SKAPP)
    t->slotsMethods = nullptr;
    t->slotsCount = 0;
#endif

    flatTypes[flatTypesCount] = t;

    StaticPlusDebug("Added ClassType '" << typeName << "' [ID: " << t->id << "]");
    flatTypesCount++;

    for(uint64_t i=0; i<flatTypesRegLambdasCount; i++)
        if (SkString::compare(typeName, flatTypesRegLambdas[i]->name))
        {
            if (i != flatTypesRegLambdasCount-1)
                flatTypesRegLambdas[i] = flatTypesRegLambdas[flatTypesRegLambdasCount-1];

            flatTypesRegLambdasCount--;

            if (flatTypesRegLambdasCount == 0)
            {
                free(flatTypesRegLambdas);
                flatTypesRegLambdas = nullptr;
            }

            else
                flatTypesRegLambdas = static_cast<SkClassTypeRegLambda**>(realloc(flatTypesRegLambdas, (flatTypesRegLambdasCount) * sizeof(SkClassTypeRegLambda *)));

            StaticPlusDebug("Removed REG Lambda for ClassType: " << typeName);
            break;
        }

    return t;
}

bool setClassSuper(CStr *typeName, CStr *superName)
{
    SkClassType *t = searchClassType(typeName);

    if (!t)
    {
        StaticError("CANNOT set object-type '" << typeName << "' as derived from '" << superName << "' [type NOT found]");
        return false;
    }

    if (t->super)
    {
        StaticError("The object-type '" << typeName << "' was ALREADY derived from '" << t->super->name << "'");
        return false;
    }

    t->super = searchClassType(superName);

    if (!t->super)
    {
        StaticError("CANNOT set object-type '" << typeName << "' as derived from '" << superName << "' [super NOT found]");
        return false;
    }

    StaticPlusDebug("The object-type '" << typeName << "' now is derived from '" << superName << "'");
    return true;
}

bool isSuperClassOf(CStr *superName, CStr *typeName)
{
    SkClassType *t = searchClassType(typeName);

    if (!t)
        return false;

    while(t->super)
    {
        t = t->super;

        if (SkString::compare(superName, t->name))
            return true;
    }

    return false;
}

uint64_t getClassTypesRegLambdasCount()
{
    return flatTypesRegLambdasCount;
}

SkClassTypeRegLambda **classTypesRegLambdasPtr(uint64_t &count)
{
    count = flatTypesRegLambdasCount;
    return flatTypesRegLambdas;
}

SkClassTypeRegLambda **classTypesRegLambdasPtr()
{
    return flatTypesRegLambdas;
}

void addClassTypeLambdaReg(CStr *typeName, ClassReg *lambda)
{
    flatTypesRegLambdas = static_cast<SkClassTypeRegLambda**>(realloc(flatTypesRegLambdas, (flatTypesRegLambdasCount+1) * sizeof(SkClassTypeRegLambda *)));

    SkClassTypeRegLambda *t = static_cast<SkClassTypeRegLambda *>(malloc(sizeof(SkClassTypeRegLambda)));
    t->name = typeName;
    t->lambda = lambda;

    flatTypesRegLambdas[flatTypesRegLambdasCount] = t;

    StaticPlusDebug("Added ClassType REG Lambda'" << typeName << "' [ID: " << flatTypesRegLambdasCount << "]");
    flatTypesRegLambdasCount++;
}

SkMethod *searchStaticMethod(uint64_t flatID, CStr *meth, SkVariant_T *argsTypes, uint64_t pCount)
{
    if (flatID >= flatTypesCount)
        return nullptr;

    SkClassType &t = *(flatTypes[flatID]);

    for(uint64_t i=0; i<t.staticMethsCount; i++)
        if (SkString::compare(t.staticMethods[i]->name, meth) && t.staticMethods[i]->argsCount == pCount)
        {
            if (memcmp(t.staticMethods[i]->args_T, argsTypes, pCount*sizeof(SkVariant_T)) != 0)
            {
                bool ok = true;

                for(uint64_t w=0; w<t.staticMethods[i]->argsCount; w++)
                    if (t.staticMethods[i]->args_T[w] != argsTypes[w])
                    {
                        if (t.staticMethods[i]->args_T[w] != SkVariant_T::T_NULL
                                || (t.staticMethods[i]->args_T[w] < SkVariant_T::T_BYTEARRAY
                                    && t.staticMethods[i]->args_T[w] > argsTypes[w]))
                        {
                            ok = false;
                            break;
                        }
                    }

                if (ok)
                    return t.staticMethods[i];
            }

            else
                return t.staticMethods[i];
        }

    if (t.super)
    {
        StaticPlusDebug("Searching recursive for " << t.name << "::" << meth << " Static Method on its super: " << t.super->name);
        int64_t flatTypeID = classTypeID(t.super->name);
        return searchStaticMethod(static_cast<uint64_t>(flatTypeID), meth, argsTypes, pCount);
    }

    SkStringList l;

    for(uint64_t i=0; i<pCount; i++)
        l << SkVariant::variantTypeName(argsTypes[i]);

    StaticError("Static Method NOT found: " << t.name << "::" << meth << "(" << l.join(", ") << ")");
    return nullptr;
}

SkMethod *addStaticVoidMeth(CStr *typeName, CStr *methName, StaticVoidMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion)
{
    int64_t typeID = classTypeID(typeName);
    SkClassType &t = *(flatTypes[typeID]);

    t.staticMethods = static_cast<SkMethod **>(realloc(t.staticMethods, (t.staticMethsCount+1) * sizeof(SkMethod *)));

    SkStaticVoidMethProto *m = new SkStaticVoidMethProto;
    m->funct = meth;

    SkMethod *method = new SkMethod;
    method->isStatic = true;
    method->hasReturn = false;
    method->isSlotMeth = false;
    method->isOverload = (overloadVersion>0);
    method->overloadVersion = overloadVersion;
    method->proto = m;
    method->name = methName;
    method->t = flatTypes[typeID];
    method->argsTypeRegLambda = argsTypeRegLambda;

    CStr *types = method->argsTypeRegLambda();
    registerMethodArgsTypes(*method, types);

    t.staticMethods[t.staticMethsCount] = method;

    StaticPlusDebug("Added FlatVoidStaticMeth: " << typeName << "::" << methName << "(" << types << ")");
    t.staticMethsCount++;
    return method;
}

SkMethod *addStaticRetMeth(CStr *typeName, CStr *methName, StaticRetMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion)
{
    int64_t typeID = classTypeID(typeName);
    SkClassType &t = *(flatTypes[typeID]);

    t.staticMethods = static_cast<SkMethod **>(realloc(t.staticMethods, (t.staticMethsCount+1) * sizeof(SkMethod *)));

    SkStaticRetMethProto *m = new SkStaticRetMethProto;
    m->funct = meth;

    SkMethod *method = new SkMethod;
    method->isStatic = true;
    method->hasReturn = true;
    method->isSlotMeth = false;
    method->isOverload = (overloadVersion>0);
    method->overloadVersion = overloadVersion;
    method->proto = m;
    method->name = methName;
    method->t = flatTypes[typeID];
    method->argsTypeRegLambda = argsTypeRegLambda;

    CStr *types = method->argsTypeRegLambda();
    registerMethodArgsTypes(*method, types);

    t.staticMethods[t.staticMethsCount] = method;

    StaticPlusDebug("Added FlatRetStaticMeth: " << typeName << "::" << methName << "(" << types << ")");
    t.staticMethsCount++;
    return method;
}

bool callStaticMethod(CStr *typeName, CStr *meth, SkVariant **p, SkVariant_T *argsTypes, uint64_t pCount, SkVariant *retVal)
{
    int64_t flatTypeID = classTypeID(typeName);

    if (flatTypeID == -1)
    {
        StaticError("ClassType NOT found: " << typeName);
        return false;
    }

    StaticPlusDebug("Searching for " << typeName << "::" << meth << " Flat Static method");
    SkMethod *m = searchStaticMethod(static_cast<uint64_t>(flatTypeID), meth, argsTypes, pCount);

    if (!m)
        return false;

    if (m->hasReturn)
        callStaticRetMethod(*m, p, pCount, retVal);

    else
        callStaticVoidMethod(*m, p, pCount);

    return true;
}

void callStaticVoidMethod(SkMethod &meth, SkVariant **p, uint64_t pCount)
{
    createParamsTypesListOnPDG("Executing StaticMethod WITHOUT return", meth, p, pCount);
    SkStaticVoidMethProto *proto = static_cast<SkStaticVoidMethProto *>(meth.proto);

    if (!pCount)
        proto->funct(nullptr);

    else if (pCount == 1)
        proto->funct(nullptr, FROM_PTR_PAR(1));

    else if (pCount == 2)
        proto->funct(nullptr, FROM_PTR_PAR(2));

    else if (pCount == 3)
        proto->funct(nullptr, FROM_PTR_PAR(3));

    else if (pCount == 4)
        proto->funct(nullptr, FROM_PTR_PAR(4));

    else if (pCount == 5)
        proto->funct(nullptr, FROM_PTR_PAR(5));

    else if (pCount == 6)
        proto->funct(nullptr, FROM_PTR_PAR(6));

    else if (pCount == 7)
        proto->funct(nullptr, FROM_PTR_PAR(7));

    else if (pCount == 8)
        proto->funct(nullptr, FROM_PTR_PAR(8));

    else if (pCount == 9)
        proto->funct(nullptr, FROM_PTR_PAR(9));

    else if (pCount == 10)
        proto->funct(nullptr, FROM_PTR_PAR(10));
}

void callStaticRetMethod(SkMethod &meth, SkVariant **p, uint64_t pCount, SkVariant *retVal)
{
    createParamsTypesListOnPDG("Executing StaticMethod WITH return", meth, p, pCount);
    SkStaticRetMethProto *proto = static_cast<SkStaticRetMethProto *>(meth.proto);

    if (!pCount)
        proto->funct(retVal);

    else if (pCount == 1)
        proto->funct(retVal, FROM_PTR_PAR(1));

    else if (pCount == 2)
        proto->funct(retVal, FROM_PTR_PAR(2));

    else if (pCount == 3)
        proto->funct(retVal, FROM_PTR_PAR(3));

    else if (pCount == 4)
        proto->funct(retVal, FROM_PTR_PAR(4));

    else if (pCount == 5)
        proto->funct(retVal, FROM_PTR_PAR(5));

    else if (pCount == 6)
        proto->funct(retVal, FROM_PTR_PAR(6));

    else if (pCount == 7)
        proto->funct(retVal, FROM_PTR_PAR(7));

    else if (pCount == 8)
        proto->funct(retVal, FROM_PTR_PAR(8));

    else if (pCount == 9)
        proto->funct(retVal, FROM_PTR_PAR(9));

    else if (pCount == 10)
        proto->funct(retVal, FROM_PTR_PAR(10));
}

SkMethod *addInstanceVoidMeth(CStr *typeName, CStr *methName, InstanceVoidMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion)
{
    int64_t typeID = classTypeID(typeName);
    SkClassType &t = *(flatTypes[typeID]);

    t.methods = static_cast<SkMethod **>(realloc(t.methods, (t.methsCount+1) * sizeof(SkMethod *)));

    SkInstanceVoidMethProto *m = new SkInstanceVoidMethProto;
    m->funct = meth;

    SkMethod *method = new SkMethod;
    method->isStatic = false;
    method->hasReturn = false;
    method->isSlotMeth = false;
    method->isOverload = (overloadVersion>0);
    method->overloadVersion = overloadVersion;
    method->proto = m;
    method->name = methName;
    method->t = flatTypes[typeID];
    method->argsTypeRegLambda = argsTypeRegLambda;

    CStr *types = method->argsTypeRegLambda();
    registerMethodArgsTypes(*method, types);

    t.methods[t.methsCount] = method;

    StaticPlusDebug("Added InstanceVoidMeth: " << typeName << "::" << methName << "(" << types << ")");
    t.methsCount++;
    return method;
}

SkMethod *searchInstanceMeth(uint64_t flatID, CStr *meth, SkVariant_T *argsTypes, uint64_t pCount)
{
    if (flatID >= flatTypesCount)
        return nullptr;

    SkClassType &t = *(flatTypes[flatID]);

    for(uint64_t i=0; i<t.methsCount; i++)
        if (SkString::compare(t.methods[i]->name, meth) && t.methods[i]->argsCount == pCount)
        {
            if (memcmp(t.methods[i]->args_T, argsTypes, pCount*sizeof(SkVariant_T)) != 0)
            {
                bool ok = true;

                for(uint64_t w=0; w<t.methods[i]->argsCount; w++)
                    if (t.methods[i]->args_T[w] != argsTypes[w])
                    {
                        if (t.methods[i]->args_T[w] != SkVariant_T::T_NULL
                                || (t.methods[i]->args_T[w] < SkVariant_T::T_BYTEARRAY
                                    && t.methods[i]->args_T[w] > argsTypes[w]))
                        {
                            ok = false;
                            break;
                        }
                    }

                if (ok)
                    return t.methods[i];
            }

            else
                return t.methods[i];
        }

    if (t.super)
    {
        StaticPlusDebug("Searching recursive for " << t.name << "::" << meth << " Instance Method on its super: " << t.super->name);
        int64_t flatTypeID = classTypeID(t.super->name);
        return searchInstanceMeth(static_cast<uint64_t>(flatTypeID), meth, argsTypes, pCount);
    }

    SkStringList l;

    for(uint64_t i=0; i<pCount; i++)
        l << SkVariant::variantTypeName(argsTypes[i]);

    StaticError("Instance Method NOT found: " << t.name << "::" << meth << "(" << l.join(", ") << ")");
    return nullptr;
}

SkMethod *addInstanceRetMeth(CStr *typeName, CStr *methName, InstanceRetMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion)
{
    int64_t typeID = classTypeID(typeName);
    SkClassType &t = *(flatTypes[typeID]);

    t.methods = static_cast<SkMethod **>(realloc(t.methods, (t.methsCount+1) * sizeof(SkMethod *)));

    SkInstanceRetMethProto *m = new SkInstanceRetMethProto;
    m->funct = meth;

    SkMethod *method = new SkMethod;
    method->isStatic = false;
    method->hasReturn = true;
    method->isSlotMeth = false;
    method->isOverload = (overloadVersion>0);
    method->overloadVersion = overloadVersion;
    method->proto = m;
    method->name = methName;
    method->t = flatTypes[typeID];
    method->argsTypeRegLambda = argsTypeRegLambda;

    CStr *types = method->argsTypeRegLambda();
    registerMethodArgsTypes(*method, types);

    t.methods[t.methsCount] = method;

    StaticPlusDebug("Added FlatRetMeth: " << typeName << "::" << methName << "(" << types << ")");
    t.methsCount++;
    return method;
}

bool callInstanceMethod(SkFlatObject *o, CStr *meth, SkVariant **p, SkVariant_T *argsTypes, uint64_t pCount, SkVariant *retVal)
{
    int64_t flatTypeID = classTypeID(o->typeName());

    if (flatTypeID == -1)
    {
        StaticError("ClassType NOT found: " << o->typeName());
        return false;
    }

    StaticPlusDebug("Searching for " << o->typeName() << "::" << meth << " InstanceMethod");
    SkMethod *m = searchInstanceMeth(static_cast<uint64_t>(flatTypeID), meth, argsTypes, pCount);

    if (!m)
        return false;

    if (m->hasReturn)
        callInstanceRetMethod(o, *m, p, pCount, retVal);

    else
        callInstanceVoidMethod(o, *m, p, pCount);

    return true;
}

void callInstanceVoidMethod(SkFlatObject *o, SkMethod &meth, SkVariant **p, uint64_t pCount)
{
    createParamsTypesListOnPDG("Executing InstanceMethod WITHOUT return", meth, p, pCount);
    SkInstanceVoidMethProto *proto = static_cast<SkInstanceVoidMethProto *>(meth.proto);

    /*if (!proto->funct)
    {
        StaticError("CANNOT Call ClassConstructor for NOT-INSTANCIABLE type: " TypeName(TYPE));
        return;
    }*/

    if (!pCount)
        proto->funct(o);

    else if (pCount == 1)
        proto->funct(o, FROM_PTR_PAR(1));

    else if (pCount == 2)
        proto->funct(o, FROM_PTR_PAR(2));

    else if (pCount == 3)
        proto->funct(o, FROM_PTR_PAR(3));

    else if (pCount == 4)
        proto->funct(o, FROM_PTR_PAR(4));

    else if (pCount == 5)
        proto->funct(o, FROM_PTR_PAR(5));

    else if (pCount == 6)
        proto->funct(o, FROM_PTR_PAR(6));

    else if (pCount == 7)
        proto->funct(o, FROM_PTR_PAR(7));

    else if (pCount == 8)
        proto->funct(o, FROM_PTR_PAR(8));

    else if (pCount == 9)
        proto->funct(o, FROM_PTR_PAR(9));

    else if (pCount == 10)
        proto->funct(o, FROM_PTR_PAR(10));
}

void callInstanceRetMethod(SkFlatObject *o, SkMethod &meth, SkVariant **p, uint64_t pCount, SkVariant *retVal)
{
    createParamsTypesListOnPDG("Executing InstanceMethod WITH return", meth, p, pCount);
    SkInstanceRetMethProto *proto = static_cast<SkInstanceRetMethProto *>(meth.proto);

    if (!pCount)
        proto->funct(o, retVal);

    else if (pCount == 1)
        proto->funct(o, retVal, FROM_PTR_PAR(1));

    else if (pCount == 2)
        proto->funct(o, retVal, FROM_PTR_PAR(2));

    else if (pCount == 3)
        proto->funct(o, retVal, FROM_PTR_PAR(3));

    else if (pCount == 4)
        proto->funct(o, retVal, FROM_PTR_PAR(4));

    else if (pCount == 5)
        proto->funct(o, retVal, FROM_PTR_PAR(5));

    else if (pCount == 6)
        proto->funct(o, retVal, FROM_PTR_PAR(6));

    else if (pCount == 7)
        proto->funct(o, retVal, FROM_PTR_PAR(7));

    else if (pCount == 8)
        proto->funct(o, retVal, FROM_PTR_PAR(8));

    else if (pCount == 9)
        proto->funct(o, retVal, FROM_PTR_PAR(9));

    else if (pCount == 10)
        proto->funct(o, retVal, FROM_PTR_PAR(10));
}

#if defined(ENABLE_SKAPP)

SkSlot *searchSlot(uint64_t flatID, CStr *meth)
{
    if (flatID >= flatTypesCount)
        return nullptr;

    SkClassType &t = *(flatTypes[flatID]);

    for(uint64_t i=0; i<t.slotsCount; i++)
        if (SkString::compare(t.slotsMethods[i]->getName(), meth))
            return t.slotsMethods[i];

    if (t.super)
    {
        StaticPlusDebug("Searching recursive for " << t.name << "::" << meth << " SlotMethod on its super: " << t.super->name);
        int64_t flatTypeID = classTypeID(t.super->name);
        return searchSlot(static_cast<uint64_t>(flatTypeID), meth);
    }

    StaticError("SlotMethod NOT found: " << t.name << "::" << meth);
    return nullptr;
}

SkSlot *addSlot(CStr *typeName, CStr *methName, SlotMeth *meth)
{
    int64_t typeID = classTypeID(typeName);
    SkClassType &t = *(flatTypes[typeID]);

    t.methods = static_cast<SkMethod **>(realloc(t.methods, (t.methsCount+1) * sizeof(SkMethod *)));

    SkSlotMethProto *m = new SkSlotMethProto;
    m->funct = meth;

    SkMethod *method = new SkMethod;
    method->isStatic = false;
    method->hasReturn = false;
    method->isSlotMeth = true;
    method->isOverload = false;
    method->overloadVersion = 0;
    method->proto = m;
    method->name = methName;
    method->t = flatTypes[typeID];
    method->argsTypeRegLambda = nullptr;

    t.methods[t.methsCount] = method;

    StaticPlusDebug("Added static Slot:" << typeName << "::" << methName);
    t.methsCount++;

    SkSlot *slt = new SkSlot(method);
    t.slotsMethods = static_cast<SkSlot **>(realloc(t.slotsMethods, (t.slotsCount+1) * sizeof(SkSlot *)));
    t.slotsMethods[t.slotsCount++] = slt;
    return slt;
}

SkSlot *addSlot(SkSlot *slt)
{
    SkMethod *method = slt->getFlatMethod();

    int64_t typeID = classTypeID(method->t->name);
    SkClassType &t = *(flatTypes[typeID]);

    StaticPlusDebug("Added Custom Slot: " << method->t->name << "::" << method->name);

    t.slotsMethods = static_cast<SkSlot **>(realloc(t.slotsMethods, (t.slotsCount+1) * sizeof(SkSlot *)));
    t.slotsMethods[t.slotsCount++] = slt;
    return slt;
}

bool removeSlot(SkSlot *slt)
{
    SkClassType &t = *slt->getFlatMethod()->t;

    for(uint64_t i=0; i<t.slotsCount; i++)
        if (t.slotsMethods[i] == slt)
        {
            if (slt->isCanonical())
            {
                StaticError("Cannot remove static SkSlot (isCanonical==true)");
                break;
            }

            delete t.slotsMethods[i];

            if (i < t.slotsCount-1)
            {
                uint64_t diffLen = t.slotsCount - i;
                memmove(&t.slotsMethods[i+1], &t.slotsMethods[i], diffLen);
                t.slotsCount--;
                t.slotsMethods = static_cast<SkSlot **>(realloc(t.slotsMethods, (t.slotsCount-1) * sizeof(SkSlot *)));
            }

        }

    return (slt == nullptr);
}

bool callSlot(SkObject *o, SkFlatObject *referer, CStr *meth, SkVariant **p, uint64_t pCount)
{
    if (o->isPreparedToDie())
    {
        if (pCount)
        {
            SkStringList l;

            for(uint64_t i=0; i<pCount; i++)
                l << p[i]->toString();

            ObjectDebug_EXT(o, "CANNOT call Slot, because the Object is prepared to die: " << meth << "(" << l.join(", ") << ")");
        }

        else
            ObjectDebug_EXT(o, "CANNOT call Slot, because the Object is prepared to die: " << meth);

        return false;
    }

    int64_t flatTypeID = classTypeID(o->typeName());

    if (flatTypeID == -1)
    {
        StaticError("ClassType NOT found: " << o->typeName());
        return false;
    }

    StaticPlusDebug("Searching for: " << o->typeName() << "::" << meth << " Slot method");

    SkSlot *slt = searchSlot(static_cast<uint64_t>(flatTypeID), meth);
    SkMethod *m = slt->getFlatMethod();

    if (!m)
        return false;

    callSlotMethod(o, referer, *m, p, pCount);
    return true;
}

void callSlotMethod(SkObject *o, SkFlatObject *referer, SkMethod &meth, SkVariant **p, uint64_t pCount)
{
    if (o->isPreparedToDie())
    {
        if (pCount)
        {
            SkStringList l;

            for(uint64_t i=0; i<pCount; i++)
                l << p[i]->toString();

            ObjectDebug_EXT(o, "CANNOT call Slot, because the Object is prepared to die: " << meth.name << "(" << l.join(", ") << ")");
        }

        else
            ObjectDebug_EXT(o, "CANNOT call Slot, because the Object is prepared to die: " << meth.name);

        return;
    }

    createParamsTypesListOnPDG("Executing Method WITHOUT return", meth, p, pCount);
    SkSlotMethProto *proto = static_cast<SkSlotMethProto *>(meth.proto);

    if (!pCount)
        proto->funct(o, referer);

    else if (pCount == 1)
        proto->funct(o, referer, FROM_PTR_PAR(1));

    else if (pCount == 2)
        proto->funct(o, referer, FROM_PTR_PAR(2));

    else if (pCount == 3)
        proto->funct(o, referer, FROM_PTR_PAR(3));

    else if (pCount == 4)
        proto->funct(o, referer, FROM_PTR_PAR(4));

    else if (pCount == 5)
        proto->funct(o, referer, FROM_PTR_PAR(5));

    else if (pCount == 6)
        proto->funct(o, referer, FROM_PTR_PAR(6));

    else if (pCount == 7)
        proto->funct(o, referer, FROM_PTR_PAR(7));

    else if (pCount == 8)
        proto->funct(o, referer, FROM_PTR_PAR(8));

    else if (pCount == 9)
        proto->funct(o, referer, FROM_PTR_PAR(9));

    else if (pCount == 10)
        proto->funct(o, referer, FROM_PTR_PAR(10));
}

#endif

uint64_t getStructTypesCount()
{
    return structTypesCount;
}

SkStructType **structTypesPtr(uint64_t &count)
{
    count = structTypesCount;
    return structTypes;
}

SkStructType **structTypesPtr()
{
    return structTypes;
}

bool existsStructType(CStr *typeName)
{
    for(uint64_t i=0; i<structTypesCount; i++)
        if (SkString::compare(typeName, structTypes[i]->name))
            return true;

    return false;
}

SkStructType *searchStructType(CStr *typeName)
{
    for(uint64_t i=0; i<structTypesCount; i++)
        if (SkString::compare(structTypes[i]->name, typeName))
            return structTypes[i];

    return nullptr;
}

SkStructType *addStructType(CStr *typeName, StructNew *instancer, StructDelete *destroyer, StructToMap *toMap, StructFromMap *fromMap)
{

    structTypes = static_cast<SkStructType**>(realloc(structTypes, (structTypesCount+1) * sizeof(SkStructType *)));

    //,
    SkStructType *t = static_cast<SkStructType *>(malloc(sizeof(SkStructType)));
    t->id = structTypesCount;
    t->name = typeName;
    t->super = nullptr;

    t->instancer = instancer;
    t->destroyer = destroyer;

    t->toMap = toMap;
    t->fromMap = fromMap;

    t->sizeOf = 0;//Setup by hand (inside macro) after this scope

    structTypes[structTypesCount] = t;

    StaticPlusDebug("Added FlatStructType '" << typeName << "' [ID: " << t->id << "]");
    structTypesCount++;
    return t;
}

bool setStructSuper(CStr *typeName, CStr *superName)
{
    SkStructType *t = searchStructType(typeName);

    if (!t)
    {
        StaticError("CANNOT set struct-type '" << typeName << "' as derived from '" << superName << "' [type NOT found]");
        return false;
    }

    SkStructType *super = searchStructType(superName);

    if (!super)
    {
        StaticError("CANNOT set struct-type '" << typeName << "' as derived from '" << superName << "' [super NOT found]");
        return false;
    }

    if (t->super)
        StaticWarning("The struct-type '" << typeName << "' was ALREADY derived from '" << t->super->name << "'");

    t->super = super;
    StaticPlusDebug("The struct-type '" << typeName << "' now is derived from '" << superName << "'");
    return true;
}

bool isSuperStructOf(CStr *superName, CStr *typeName)
{
    SkStructType *t = searchStructType(typeName);

    while(t->super)
    {
        t = t->super;

        if (SkString::compare(superName, t->name))
            return true;
    }

    return false;
}

uint64_t getEnumTypesCount()
{
    return enumTypesCount;
}

SkEnumType **enumTypesPtr(uint64_t &count)
{
    count = enumTypesCount;
    return enumTypes;
}

SkEnumType **enumTypesPtr()
{
    return enumTypes;
}

bool existsEnumType(CStr *typeName)
{
    for(uint64_t i=0; i<enumTypesCount; i++)
        if (SkString::compare(typeName, enumTypes[i]->name))
            return true;

    return false;
}

SkEnumType *searchEnumType(CStr *typeName)
{
    for(uint64_t i=0; i<enumTypesCount; i++)
        if (SkString::compare(enumTypes[i]->name, typeName))
            return enumTypes[i];

    return nullptr;
}

SkEnumType *addEnumType(CStr *typeName, SkArgsMap *enumeration)
{
    enumTypes = static_cast<SkEnumType**>(realloc(enumTypes, (enumTypesCount+1) * sizeof(SkEnumType *)));

    //,
    SkEnumType *t = static_cast<SkEnumType *>(malloc(sizeof(SkEnumType)));
    t->id = enumTypesCount;
    t->name = typeName;
    t->enumeration = enumeration;

    enumTypes[enumTypesCount] = t;

    StaticPlusDebug("Added FlatEnumType '" << typeName << "' [ID: " << t->id << "]");
    enumTypesCount++;
    return t;
}

// // // // // // // // // //  // // // // // // // // // // //

/*struct SkPointerTrace
{
    SkFlatObject *ctx;
    void *ptr;
    CStr *typeName;
    CStr *prettyFunction;
    uint64_t elementCount;
    uint64_t typeSize;
};

SkMutex tracePointersEx;
static bool tracePointers = false;
static SkTreeMap<void *, SkPointerTrace *> ptrs;//ptr,

void enablePointerTrace()
{
    StaticWarning("ENABLED SpecialK pointers tracing");
    tracePointers = true;
}

void disablePointerTrace()
{
    StaticWarning("DISABLED SpecialK pointers tracing");
    tracePointers = false;
}

void *newPtr(void *ptr, CStr *typeName, uint64_t typeSize, CStr *prettyFunction, SkFlatObject *ctx)
{
    if (tracePointers)
    {
        SkPointerTrace *t = new SkPointerTrace;
        t->ctx = ctx;
        t->ptr = ptr;
        t->typeName = typeName;
        t->elementCount = 1;
        t->typeSize = typeSize;
        t->prettyFunction = prettyFunction;

        tracePointersEx.lock();
        ptrs[t->ptr] = t;
        tracePointersEx.unlock();
    }

    return ptr;
}

void deletePtr(void *ptr)
{
    if (tracePointers)
    {
        tracePointersEx.lock();
        delete ptrs.remove(ptr).value();
        tracePointersEx.unlock();
    }
}

void *mallocPtr(CStr *typeName, uint64_t elementCount, uint64_t typeSize, CStr *prettyFunction, SkFlatObject *ctx)
{
    void *ptr = malloc(typeSize*elementCount);

    if (tracePointers)
    {
        SkPointerTrace *t = new SkPointerTrace;
        t->ctx = ctx;
        t->ptr = ptr;
        t->typeName = typeName;
        t->elementCount = elementCount;
        t->typeSize = typeSize;
        t->prettyFunction = prettyFunction;

        tracePointersEx.lock();
        ptrs[t->ptr] = t;
        tracePointersEx.unlock();
    }

    return ptr;
}

void *reallocPtr(void *ptr, uint64_t newCount)
{
    uint64_t typeSize;

    if (tracePointers)
    {
        tracePointersEx.lock();
        SkPointerTrace *t = ptrs[ptr];
        t->elementCount = newCount;
        typeSize = t->typeSize;
        tracePointersEx.unlock();
    }

    return realloc(ptr, typeSize*newCount);
}*/

// // // // // // // // // //  // // // // // // // // // // //
