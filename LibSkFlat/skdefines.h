/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKDEFINES_H
#define SKDEFINES_H

/**
 * @file skdefine.h
*/

#define SK_MAJORVERSION                     1
#define SK_MINORVERSION                     0
#define SK_CODENAME                         "PsychoDrama"

#include <string.h>
#include <inttypes.h>

//C++ headers
#include <random>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <algorithm>
#include <deque>
#include <list>
#include <vector>
#include <map>
#include <bitset>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

// // // // // // // // // // // // // // // // // // // // //

class SkFlatObject;
class SkVariant;
class SkArgsMap;

#if defined(ENABLE_SKAPP)
class SkSlot;
class SkObject;
#endif

//REDEFINITIONs

// // // // // // // // // // // // // // // // // // // // //

#if defined(__ANDROID__) || defined(__MACH__)
typedef unsigned long                       ulong;
#endif

typedef const char                          CStr;
typedef const void                          CVoid;
/*typedef int8_t                              Byte;
typedef uint8_t                             UByte;*/
typedef int16_t                             Short;
typedef uint16_t                            UShort;
typedef int32_t                             Int;
typedef uint32_t                            UInt;
typedef int64_t                             Long;
typedef uint64_t                            ULong;

typedef struct timespec                     SkTimeSpec;
typedef thread                              SkStdThread;
typedef mutex                               SkMutex;
typedef condition_variable                  SkWaitCondition;
typedef thread::id                          SkThID;

typedef void            (ClassReg)          ();
typedef SkFlatObject   *(ClassNew)          (CStr *);
typedef void            (ClassDelete)       (SkFlatObject *);
typedef void            (StaticVoidMeth)    (void *, ...);
typedef void            (InstanceVoidMeth)  (SkFlatObject *, ...);

#if defined(ENABLE_SKAPP)
typedef void            (SlotMeth)          (SkObject *, SkFlatObject *, ...);
#endif

typedef void            (StaticRetMeth)     (SkVariant *, ...);
typedef void            (InstanceRetMeth)   (SkFlatObject *, SkVariant *, ...);
typedef CStr           *(ArgsTypeString)    ();
typedef void           *(StructNew)         ();
typedef void            (StructDelete)      (void *);
typedef void            (StructToMap)       (void *, SkArgsMap *);
typedef void            (StructFromMap)     (SkArgsMap *, void *);

// void
typedef struct {StaticVoidMeth *funct;}     SkStaticVoidMethProto;
typedef struct {InstanceVoidMeth *funct;}   SkInstanceVoidMethProto;

// return
typedef struct {StaticRetMeth *funct;}      SkStaticRetMethProto;
typedef struct {InstanceRetMeth *funct;}    SkInstanceRetMethProto;

// slot
#if defined(ENABLE_SKAPP)
typedef struct {SlotMeth *funct;}           SkSlotMethProto;
#endif

// // // // // // // // // // // // // // // // // // // // //

enum SkVariant_T
{
    T_NULL,
    T_BOOL,
    //T_CHAR,
    T_INT8,
    T_UINT8,
    T_INT16,
    T_UINT16,
    T_INT32,
    T_UINT32,
    T_FLOAT,
    T_INT64,
    T_UINT64,
    T_DOUBLE,
    T_BYTEARRAY,
    T_STRING,
    T_LIST,
    T_PAIR,
    T_MAP,
    T_RAWPOINTER
};

// // // // // // // // // // // // // // // // // // // // //

struct SkClassType;

struct SkMethod
{
    CStr *name;
    bool hasReturn;
    bool isSlotMeth;
    bool isStatic;
    bool isOverload;
    uint64_t overloadVersion;
    void *proto;
    uint64_t argsCount;
    SkVariant_T *args_T;
    ArgsTypeString *argsTypeRegLambda;
    SkClassType *t;
};

// // // // // // // // // // // // // // // // // // // // //

struct SkClassType //NO typedef here, because it is used inside its own props
{
    uint64_t id;
    CStr *name;
    SkClassType *super;
    ClassNew *instancer;

    SkMethod **methods;
    uint64_t methsCount;

    SkMethod **staticMethods;
    uint64_t staticMethsCount;

#if defined(ENABLE_SKAPP)
    SkSlot **slotsMethods;
    uint64_t slotsCount;
#endif
};

// // // // // // // // // // // // // // // // // // // // //

struct SkClassTypeRegLambda
{
    CStr *name;
    ClassReg *lambda;
};

// // // // // // // // // // // // // // // // // // // // //

struct SkStructType //NO typedef here, because it is used inside its own props
{
    uint64_t id;
    CStr *name;
    SkStructType *super;

    StructNew *instancer;
    StructDelete *destroyer;

    StructToMap *toMap;
    StructFromMap *fromMap;

    uint64_t sizeOf;
};

// // // // // // // // // // // // // // // // // // // // //

struct SkEnumType
{
    uint64_t id;
    CStr *name;
    SkArgsMap *enumeration;
} ;

// // // // // // // // // // // // // // // // // // // // //

CStr *SpecialK_VERSION();

uint64_t getClassTypesCount();
SkClassType **classTypesPtr(uint64_t &count);
SkClassType **classTypesPtr();
bool existsClassType(CStr *typeName);
int64_t classTypeID(CStr *typeName);// THIS WILL NOT SEARCH ON REG_FUNCTs
SkClassType *searchClassType(CStr *typeName);// THIS WILL SEARCH ON REG_FUNCTs AND REGISTER IT IF FOUND
SkClassType *addClassType(CStr *typeName, ClassNew *instancer);
bool setClassSuper(CStr *typeName, CStr *superName);
bool isSuperClassOf(CStr *superName, CStr *typeName);

uint64_t getClassTypesRegLambdasCount();
SkClassTypeRegLambda **classTypesRegLambdasPtr(uint64_t &count);
SkClassTypeRegLambda **classTypesRegLambdasPtr();
void addClassTypeLambdaReg(CStr *typeName, ClassReg *lambda);

SkMethod *searchStaticMethod(uint64_t flatID, CStr *meth, SkVariant_T *argsTypes, uint64_t pCount);
SkMethod *addStaticVoidMeth(CStr *typeName, CStr *methName, StaticVoidMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion);
SkMethod *addStaticRetMeth(CStr *typeName, CStr *methName, StaticRetMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion);
bool callStaticMethod(CStr *typeName, CStr *meth, SkVariant **p, SkVariant_T *argsTypes, uint64_t pCount, SkVariant *retVal);
void callStaticVoidMethod(SkMethod &meth, SkVariant **p, uint64_t pCount);
void callStaticRetMethod(SkMethod &meth, SkVariant **p, uint64_t pCount, SkVariant *retVal);

SkMethod *searchInstanceMeth(uint64_t flatID, CStr *meth, SkVariant_T *argsTypes, uint64_t pCount);
SkMethod *addInstanceRetMeth(CStr *typeName, CStr *methName, InstanceRetMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion);
SkMethod *addInstanceVoidMeth(CStr *typeName, CStr *methName, InstanceVoidMeth *meth, ArgsTypeString *argsTypeRegLambda, uint64_t overloadVersion);
bool callInstanceMethod(SkFlatObject *o, CStr *meth, SkVariant **p, SkVariant_T *argsTypes, uint64_t pCount, SkVariant *retVal);
void callInstanceVoidMethod(SkFlatObject *o, SkMethod &meth, SkVariant **p, uint64_t pCount);
void callInstanceRetMethod(SkFlatObject *o, SkMethod &meth, SkVariant **p, uint64_t pCount, SkVariant *retVal);

#if defined(ENABLE_SKAPP)
    SkSlot *searchSlot(uint64_t flatID, CStr *meth);
    SkSlot *addSlot(CStr *typeName, CStr *methName, SlotMeth *meth);
    SkSlot *addSlot(SkSlot *slt);
    bool removeSlot(SkSlot *slt);
    bool callSlot(SkObject *o, SkFlatObject *referer, CStr *meth, SkVariant **p, uint64_t pCount);
    void callSlotMethod(SkObject *o, SkFlatObject *referer, SkMethod &meth, SkVariant **p, uint64_t pCount);
#endif

uint64_t getStructTypesCount();
SkStructType **structTypesPtr(uint64_t &count);
SkStructType **structTypesPtr();
bool existsStructType(CStr *typeName);
SkStructType *searchStructType(CStr *typeName);
SkStructType *addStructType(CStr *typeName, StructNew *instancer, StructDelete *destroyer, StructToMap *toMap, StructFromMap *fromMap);
bool setStructSuper(CStr *typeName, CStr *superName);
bool isSuperStructOf(CStr *superName, CStr *typeName);

uint64_t getEnumTypesCount();
SkEnumType **enumTypesPtr(uint64_t &count);
SkEnumType **enumTypesPtr();
bool existsEnumType(CStr *typeName);
SkEnumType *searchEnumType(CStr *typeName);
SkEnumType *addEnumType(CStr *typeName, SkArgsMap *enumeration);

template <class T>
T newObject(CStr *typeName, CStr *objectName=nullptr)
{
    SkClassType *t = searchClassType(typeName);

    if (!t)
        return nullptr;

    return dynamic_cast<T>(t->instancer(objectName));
}

/*void enablePointerTrace();
void disablePointerTrace();
void *newPtr(void *ptr, CStr *typeName, uint64_t typeSize, CStr *prettyFunction, SkFlatObject *ctx);
void deletePtr(void *ptr);
void *mallocPtr(CStr *typeName, uint64_t elementCount, uint64_t typeSize, CStr *prettyFunction, SkFlatObject *ctx);
void *reallocPtr(void *ptr, uint64_t newCount);*/

// // // // // // // // // // // // // // // // // // // // //

#include <stdarg.h>

// // // // // // // // // //  // // // // // // // // // // //
// Sk Dialect

//IT IS NOT POSSIBLE TO USE SkObject AS SUPERCLASS IF IT IS NOT PUBLIC
//HERE NOTHING IS SUBCLASSED AS PRIVATE OR PROTECTED, ALL IS PUBLIC
//SO, WARNING! BECAUSE NOT USING IT, U COULD GO WRONG :D
#define extends : public

#define FakeSingleLine(CODE) \
    do {CODE} while(false)

#define KillApp() \
    FakeSingleLine(StaticError("!!! This is a Suicide Solution [exit(1)] !!!"); ::exit(1);)

#define AssertKiller(COND) \
    FakeSingleLine( \
        if (COND) \
        { \
            StaticError("!!! Killing by ASSERTION -> iff [(" << #COND << ") == true] !!!"); \
            ::exit(1); \
        })

/*#define New(PTR_TYPE, ...)          static_cast<PTR_TYPE *>(newPtr(new PTR_TYPE(__VA_ARGS__), TypeName(PTR_TYPE), sizeof(PTR_TYPE), __PRETTY_FUNCTION__))
#define Del(PTR)                    {deletePtr(PTR); delete PTR;}voidSemicolonFake()

#define Malloc(PTR_TYPE, COUNT)     static_cast<PTR_TYPE *>(mallocPtr(TypeName(PTR_TYPE), COUNT, sizeof(PTR_TYPE), __PRETTY_FUNCTION__))
//#define Realloc(PTR, NEWCOUNT)      static_cast<PTR_TYPE *>(reallocPtr(PTR, NEWCOUNT))
#define Free(PTR)                   {deletePtr(PTR); free(PTR);}voidSemicolonFake()*/

// // // // // // // // // // // // // // // // // // // // //

#define Arg_AbstractVariadic    (*va_arg(vl, SkVariant *))

// Copy Arguments
#define Arg_UInt64              Arg_AbstractVariadic.toUInt64()
#define Arg_UInt32              Arg_AbstractVariadic.toUInt32()
#define Arg_UInt16              Arg_AbstractVariadic.toUInt16()
#define Arg_UInt8               Arg_AbstractVariadic.toUInt8()
#define Arg_Int64               Arg_AbstractVariadic.toInt64()
#define Arg_Int32               Arg_AbstractVariadic.toInt32()
#define Arg_Int16               Arg_AbstractVariadic.toInt16()
#define Arg_Int8                Arg_AbstractVariadic.toInt8()
#define Arg_Bool                Arg_AbstractVariadic.toBool()
#define Arg_Char                Arg_AbstractVariadic.toInt8()
#define Arg_UInt                Arg_AbstractVariadic.toUInt()
#define Arg_Int                 Arg_AbstractVariadic.toInt()
#define Arg_Float               Arg_AbstractVariadic.toFloat()
#define Arg_Double              Arg_AbstractVariadic.toDouble()

// Reference Arguments
#define Arg_UInt64_REAL         Arg_AbstractVariadic.realUInt64()
#define Arg_UInt32_REAL         Arg_AbstractVariadic.realUInt32()
#define Arg_UInt16_REAL         Arg_AbstractVariadic.realUInt16()
#define Arg_UInt8_REAL          Arg_AbstractVariadic.realUInt8()
#define Arg_Int64_REAL          Arg_AbstractVariadic.realInt64()
#define Arg_Int32_REAL          Arg_AbstractVariadic.realInt32()
#define Arg_Int16_REAL          Arg_AbstractVariadic.realInt16()
#define Arg_Int8_REAL           Arg_AbstractVariadic.realInt8()
#define Arg_Bool_REAL           Arg_AbstractVariadic.realBool()
#define Arg_Char_REAL           Arg_AbstractVariadic.realInt8()
#define Arg_Float_REAL          Arg_AbstractVariadic.realFloat()
#define Arg_Double_REAL         Arg_AbstractVariadic.realDouble()

// String types Arguments
#define Arg_CStr                Arg_AbstractVariadic.data()
#define Arg_String              Arg_AbstractVariadic.toString()
#define Arg_StringRef           Arg_AbstractVariadic.toStringRef()

// Generic types
#define Arg_Enum(TYPE)          static_cast<TYPE>(Arg_Int)

// Generic types pointers

//DOES NOT REQUIRE T_RAWPOINTER
#define Arg_Data(TYPE)          static_cast<TYPE *>(Arg_AbstractVariadic.toVoid())
//#define Arg_SSize               *Arg_Data(int64_t)
//#define Arg_USize               *Arg_Data(uint64_t)

//REQUIRES T_RAWPOINTER
#define Arg_Custom(TYPE)        static_cast<TYPE *>(variantToCustom(&Arg_AbstractVariadic))

//INSIDE A FUNCTION OR A METHOD
#define ArgNotUsed(ARG)         (void) ARG

#define StCast(TYPE, PTR)       static_cast<TYPE *>(PTR)
#define DynCast(TYPE, PTR)      dynamic_cast<TYPE *>(PTR)

// // // // // // // // // // // // // // // // // // // // //

#define TypeName(TYPE) #TYPE

// // // // // // // // // // // // // // // // // // // // //

#define METHOD_NAME(TYPE, METH) \
    skFlatWrap_ ## TYPE ## _ ## METH

#define METHOD_NAME_OVERLOAD(TYPE, METH, VERSION) \
    skFlatWrap_ ## TYPE ## _ ## METH ## _ ## VERSION

#define METHOD_NAME_ARGTYPEREG(TYPE, METH) \
    skFlatWrap_ ## TYPE ## _ ## METH ## _ARGTYPEREG

#define METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION) \
    skFlatWrap_ ## TYPE ## _ ## METH ## _ARGTYPEREG_ ## VERSION

#define CLASS_CTOR_NAME(TYPE) \
    TYPE ## ClassConstructor

// // // // // // // // // // // // // // // // // // // // //

#define SetupClassWrapper(TYPE) \
    TYPE ## FlatWrap :: TYPE ## FlatWrap()

#define SetClassSuper(TYPE, SUPER) \
    setClassSuper(TypeName(TYPE), TypeName(SUPER))

#define SetStructSuper(TYPE, SUPER) \
    setStructSuper(TypeName(TYPE), TypeName(SUPER))

#define CLASS_REG_SCOPE(TYPE) \
    { \
        typeFlatConfig_ ## TYPE = new TYPE ## FlatConfig; \
        typeFlatWrap_ ## TYPE = new  TYPE ## FlatWrap; \
    } \

#define CreateClassWrapper(TYPE) \
    if (typeFlatConfig_ ## TYPE == nullptr) \
    CLASS_REG_SCOPE(TYPE) \
    flatID = typeFlatConfig_ ## TYPE -> flatID

// // // // // // // // // // // // // // // // // // // // //

#define DECL_WRAP_STRUCT(TYPE) \
    static struct TYPE ## FlatWrap \
    {TYPE ## FlatWrap();}

#define DECL_CFG_STRUCT(TYPE, CTOR) \
    static struct TYPE ## FlatConfig \
    { \
        int flatID; \
        TYPE ## FlatConfig() \
        {flatID = addClassType(TypeName(TYPE), CTOR)->id;} \
    }

#define CLASS_CTOR_SCOPE(TYPE) \
    { \
        StaticPlusDebug("Calling Constructor for Class: " TypeName(TYPE)); \
        TYPE *o = new TYPE(); \
        if (objName) \
            o->setObjectName(objName); \
        return o; \
    }

#define CLASS_REGFUNCT_NAME(TYPE) \
    TYPE ## _ ## _CLASSREG_LAMBDA

#define CLASS_REGFUNCT_STRUCT(TYPE) \
    static struct TYPE ## TypeRegLambda \
    { \
        TYPE ## TypeRegLambda() \
        {addClassTypeLambdaReg(TypeName(TYPE), &CLASS_REGFUNCT_NAME(TYPE));} \
    } typeReg_ ## TYPE

#define DeclareWrapper(TYPE) \
    SkFlatObject *CLASS_CTOR_NAME(TYPE)(CStr *objName) \
    CLASS_CTOR_SCOPE(TYPE) \
    DECL_CFG_STRUCT(TYPE, &CLASS_CTOR_NAME(TYPE)) *typeFlatConfig_ ## TYPE = nullptr; \
    DECL_WRAP_STRUCT(TYPE) *typeFlatWrap_ ## TYPE = nullptr; \
    void CLASS_REGFUNCT_NAME(TYPE)() \
    CLASS_REG_SCOPE(TYPE) \
    CLASS_REGFUNCT_STRUCT(TYPE)

#define DeclareWrapper_NOT_INSTANCIABLE(TYPE) \
    DECL_CFG_STRUCT(TYPE, nullptr) *typeFlatConfig_ ## TYPE = nullptr; \
    DECL_WRAP_STRUCT(TYPE) *typeFlatWrap_ ## TYPE = nullptr; \
    void CLASS_REGFUNCT_NAME(TYPE)() \
    CLASS_REG_SCOPE(TYPE) \
    CLASS_REGFUNCT_STRUCT(TYPE)

#define DeclareWrapper_STATIC(TYPE) \
    DECL_CFG_STRUCT(TYPE, nullptr) typeFlatConfig_ ## TYPE; \
    DECL_WRAP_STRUCT(TYPE) typeFlatWrap_ ## TYPE

// // // // // // // // // // // // // // // // // // // // //
// SkObject

#define DECL_WRAPPER_BASE_OBJECT(TYPE, CONSTR) \
    \
    static void(** TYPE ## PreparedSlotsLambdas)() = nullptr; \
    static uint64_t TYPE ## PreparedSlotsCount = 0; \
    \
    static struct TYPE ## FlatConfig \
    { \
        TYPE ## FlatConfig() \
        { \
            flatID = addClassType(TypeName(TYPE), CONSTR)->id; \
            for(uint64_t i=0; i<TYPE ## PreparedSlotsCount; i++) \
                TYPE ## PreparedSlotsLambdas[i](); \
            free(TYPE ## PreparedSlotsLambdas); \
            TYPE ## PreparedSlotsLambdas = nullptr; \
            TYPE ## PreparedSlotsCount = 0; \
        } \
        static int addPrepareSlotRegLambda(void(*f)(void)) \
        { \
            TYPE ## PreparedSlotsLambdas = (void(**)()) (realloc(TYPE ## PreparedSlotsLambdas, (TYPE ## PreparedSlotsCount+1) * sizeof(void(*)()))); \
            TYPE ## PreparedSlotsLambdas[TYPE ## PreparedSlotsCount] = f; \
            TYPE ## PreparedSlotsCount++; \
            return TYPE ## PreparedSlotsCount; \
        } \
        uint64_t flatID; \
    } *typeFlatConfig_ ## TYPE = nullptr; \
    DECL_WRAP_STRUCT(TYPE) *typeFlatWrap_ ## TYPE = nullptr; \
    void CLASS_REGFUNCT_NAME(TYPE)() \
    CLASS_REG_SCOPE(TYPE) \
    CLASS_REGFUNCT_STRUCT(TYPE)

#define DeclareSkObjectWrapper(TYPE) \
    SkFlatObject *CLASS_CTOR_NAME(TYPE)(CStr *objName) \
    CLASS_CTOR_SCOPE(TYPE) \
    DECL_WRAPPER_BASE_OBJECT(TYPE, CLASS_CTOR_NAME(TYPE))

#define DeclareSkObjectWrapper_NOT_INSTANCIABLE(TYPE) \
    DECL_WRAPPER_BASE_OBJECT(TYPE, nullptr)

#define SLOT_REGFUNCT_NAME(TYPE, METH) \
    TYPE ## _ ## METH ## _SLOTREG_LAMBDA

#define PREPARE_SLOT_REGFUNCT_STRUCT(TYPE, METH) \
    void SLOT_REGFUNCT_NAME(TYPE, METH)() \
    { \
        /*SlotName(METH) = */addSlot(TypeName(TYPE), #METH, &METHOD_NAME(TYPE, METH)); \
    } \
    static struct TYPE ## _ ## METH \
    { \
        TYPE ## _ ## METH() \
        {TYPE ## FlatConfig :: addPrepareSlotRegLambda(&SLOT_REGFUNCT_NAME(TYPE, METH));} \
    } prepare_ ## _ ## METH ## TYPE

// // // // // // // // // // // // // // // // // // // // //

#define AddMeth_STATIC_VOID(TYPE, METH) \
    addStaticVoidMeth(TypeName(TYPE), #METH, &METHOD_NAME(TYPE, METH), &METHOD_NAME_ARGTYPEREG(TYPE, METH), 0)

#define AddMeth_STATIC_VOID_OVERLOAD(TYPE, METH, VERSION) \
    addStaticVoidMeth(TypeName(TYPE), #METH, &METHOD_NAME_OVERLOAD(TYPE, METH, VERSION), &METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION), VERSION)

// // // // // // // // // // // // // // // // // // // // //

#define AddMeth_INSTANCE_VOID(TYPE, METH) \
    addInstanceVoidMeth(TypeName(TYPE), #METH, &METHOD_NAME(TYPE, METH), &METHOD_NAME_ARGTYPEREG(TYPE, METH), 0)

#define AddMeth_INSTANCE_VOID_OVERLOAD(TYPE, METH, VERSION) \
    addInstanceVoidMeth(TypeName(TYPE), #METH, &METHOD_NAME_OVERLOAD(TYPE, METH, VERSION), &METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION), VERSION)

// // // // // // // // // // // // // // // // // // // // //

#define AddMeth_STATIC_RET(TYPE, METH) \
    addStaticRetMeth(TypeName(TYPE), #METH, &METHOD_NAME(TYPE, METH), &METHOD_NAME_ARGTYPEREG(TYPE, METH), 0)

#define AddMeth_STATIC_RET_OVERLOAD(TYPE, METH, VERSION) \
    addStaticRetMeth(TypeName(TYPE), #METH, &METHOD_NAME_OVERLOAD(TYPE, METH, VERSION), &METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION), VERSION)

// // // // // // // // // // // // // // // // // // // // //

#define AddMeth_INSTANCE_RET(TYPE, METH) \
    addInstanceRetMeth(TypeName(TYPE), #METH, &METHOD_NAME(TYPE, METH), &METHOD_NAME_ARGTYPEREG(TYPE, METH), 0)

#define AddMeth_INSTANCE_RET_OVERLOAD(TYPE, METH, VERSION) \
    addInstanceRetMeth(TypeName(TYPE), #METH, &METHOD_NAME_OVERLOAD(TYPE, METH, VERSION), &METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION), VERSION)

// // // // // // // // // // // // // // // // // // // // //

#define STATIC_VOID_SCOPE(TYPE, METH, ...) \
    { \
        va_list vl; \
        va_start(vl, o); \
        TYPE :: METH( __VA_ARGS__ ); \
        va_end(vl); \
        (void) o;\
    }

#define DeclareMeth_STATIC_VOID(TYPE, METH, ...) \
    CStr *METHOD_NAME_ARGTYPEREG(TYPE, METH)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME(TYPE, METH)(void *o, ...) \
    STATIC_VOID_SCOPE(TYPE, METH, __VA_ARGS__)

#define DeclareMeth_STATIC_VOID_OVERLOAD(TYPE, METH, VERSION, ...) \
    CStr *METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME_OVERLOAD(TYPE, METH, VERSION)(void *o, ...) \
    STATIC_VOID_SCOPE(TYPE, METH, __VA_ARGS__)

// // // // // // // // // // // // // // // // // // // // //

#define INSTANCE_VOID_SCOPE(TYPE, METH, ...) \
    { \
        va_list vl; \
        va_start(vl, o); \
        TYPE *casted = dynamic_cast<TYPE *>(o); \
        if (!casted) \
        { \
            va_end(vl); \
            return; \
        } \
        casted->METH( __VA_ARGS__ ); \
        va_end(vl); \
    }

#define DeclareMeth_INSTANCE_VOID(TYPE, METH, ...) \
    CStr *METHOD_NAME_ARGTYPEREG(TYPE, METH)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME(TYPE, METH)(SkFlatObject *o, ...) \
    INSTANCE_VOID_SCOPE(TYPE, METH, __VA_ARGS__)

#define DeclareMeth_INSTANCE_VOID_OVERLOAD(TYPE, METH, VERSION, ...) \
    CStr *METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME_OVERLOAD(TYPE, METH, VERSION)(SkFlatObject *o, ...) \
    INSTANCE_VOID_SCOPE(TYPE, METH, __VA_ARGS__)

// // // // // // // // // // // // // // // // // // // // //

#define STATIC_RET_METH_SCOPE(TYPE, METH, RETTYPE, ...) \
    { \
        va_list vl; \
        va_start(vl, ret); \
        if (ret) \
        { \
            RETTYPE v = TYPE :: METH( __VA_ARGS__ ); \
            ret->setVal(v); \
            ret->setOriginalRealType(TypeName(RETTYPE)); \
        } \
        va_end(vl); \
    }

#define DeclareMeth_STATIC_RET(TYPE, METH, RETTYPE, ...) \
    CStr *METHOD_NAME_ARGTYPEREG(TYPE, METH)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME(TYPE, METH)(SkVariant *ret, ...) \
    STATIC_RET_METH_SCOPE(TYPE, METH, RETTYPE, __VA_ARGS__)

#define DeclareMeth_STATIC_RET_OVERLOAD(TYPE, METH, VERSION, RETTYPE, ...) \
    CStr *METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME_OVERLOAD(TYPE, METH, VERSION)(SkVariant *ret, ...) \
    STATIC_RET_METH_SCOPE(TYPE, METH, RETTYPE, __VA_ARGS__)

// // // // // // // // // // // // // // // // // // // // //

#define INSTANCE_RET_METH_SCOPE(TYPE, METH, RETTYPE, ...) \
    { \
        va_list vl; \
        va_start(vl, ret); \
        if (ret) \
        { \
            TYPE *casted = dynamic_cast<TYPE *>(o); \
            if (!casted) \
            { \
                va_end(vl); \
                return; \
            } \
            RETTYPE v = casted->METH( __VA_ARGS__ ); \
            ret->setVal(v); \
            ret->setOriginalRealType(TypeName(RETTYPE)); \
        } \
        va_end(vl); \
    }

#define DeclareMeth_INSTANCE_RET(TYPE, METH, RETTYPE, ...) \
    CStr *METHOD_NAME_ARGTYPEREG(TYPE, METH)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME(TYPE, METH)(SkFlatObject *o, SkVariant *ret, ...) \
    INSTANCE_RET_METH_SCOPE(TYPE, METH, RETTYPE, __VA_ARGS__)

#define DeclareMeth_INSTANCE_RET_OVERLOAD(TYPE, METH, VERSION, RETTYPE, ...) \
    CStr *METHOD_NAME_ARGTYPEREG_OVERLOAD(TYPE, METH, VERSION)() \
    {return #__VA_ARGS__; } \
    void METHOD_NAME_OVERLOAD(TYPE, METH, VERSION)(SkFlatObject *o, SkVariant *ret, ...) \
    INSTANCE_RET_METH_SCOPE(TYPE, METH, RETTYPE, __VA_ARGS__)

// // // // // // // // // // // // // // // // // // // // //
// SkObject Slots-meth

#define DECL_SLOT_METHOD(TYPE, METH) \
    void METHOD_NAME(TYPE, METH)(SkObject *target, SkFlatObject *referer, ...) \
    { \
        va_list vl; \
        va_start(vl, referer); \
        TYPE *casted = dynamic_cast<TYPE *>(target); \
        if (!casted) \
        { \
            va_end(vl); \
            return; \
        } \
        casted->METH(referer, vl); \
        va_end(vl); \
    }

// // // // // // // // // // // // // // // // // // // // //
// FlatEnum

#define ENUM_MAP_NAME(TYPE)                   TYPE ## Enumeration

#define DeclareWrapper_ENUM(TYPE) \
    static SkArgsMap *ENUM_MAP_NAME(TYPE)=nullptr; \
    void TYPE ## EnumerationSetup(SkArgsMap *enumerationMap)

#define SetupEnumWrapper(TYPE) \
    int i=0; \
    (*enumerationMap)["t"] = TypeName(TYPE)

#define SetEnumItem(NAME) \
    (*enumerationMap)[#NAME] = i++

#define AddEnumType(TYPE) \
    ENUM_MAP_NAME(TYPE) = new SkArgsMap; \
    TYPE ## EnumerationSetup(ENUM_MAP_NAME(TYPE)); \
    addEnumType(TypeName(TYPE), ENUM_MAP_NAME(TYPE))

// // // // // // // // // // // // // // // // // // // // //
// FlatStruct

#define STRUCT_CTOR_NAME(TYPE)                  TYPE ## FlatStructConstructor
#define STRUCT_DTOR_NAME(TYPE)                  TYPE ## FlatStructDestructor
#define STRUCT_TOMAP_FUNCT_NAME(TYPE)               METHOD_NAME(TYPE, ToMap)
#define STRUCT_TOMAP_FUNCT_NAME_INTERNAL(TYPE)      METHOD_NAME(TYPE, ToMap_INTERNAL)
#define STRUCT_FROMMAP_FUNCT_NAME(TYPE)             METHOD_NAME(TYPE, FromMap)
#define STRUCT_FROMMAP_FUNCT_NAME_INTERNAL(TYPE)     METHOD_NAME(TYPE, FromMap_INTERNAL)

#define AddStructType(TYPE) \
    uint64_t TYPE ## FlatStructID = addStructType(TypeName(TYPE), \
                                                &STRUCT_CTOR_NAME(TYPE), \
                                                &STRUCT_DTOR_NAME(TYPE), \
                                                &STRUCT_TOMAP_FUNCT_NAME(TYPE), \
                                                &STRUCT_FROMMAP_FUNCT_NAME(TYPE))->id; \
    structTypesPtr()[TYPE ## FlatStructID]->sizeOf = sizeof(TYPE)

#define DeclareWrapper_STRUCT_TOMAP(TYPE) \
    void *STRUCT_CTOR_NAME(TYPE)() \
    { \
        StaticPlusDebug("Calling Constructor for Struct: " TypeName(TYPE)); \
        return new TYPE(); \
    } \
    void STRUCT_DTOR_NAME(TYPE)(void *ptr) \
    { \
        StaticPlusDebug("Calling Destructor for Struct: " TypeName(TYPE)); \
        delete static_cast<TYPE *>(ptr); \
    } \
    void STRUCT_TOMAP_FUNCT_NAME_INTERNAL(TYPE)(TYPE *src, SkArgsMap &tgt); \
    void STRUCT_TOMAP_FUNCT_NAME(TYPE)(void *srcPtr, SkArgsMap *tgt) \
    { \
        StaticPlusDebug(TypeName(TYPE) << "::toMap()"); \
        SkStructType *t = searchStructType(TypeName(TYPE)); \
        if (t->super) \
            t->super->toMap(srcPtr, tgt); \
        TYPE *src = static_cast<TYPE *>(srcPtr); \
        STRUCT_TOMAP_FUNCT_NAME_INTERNAL(TYPE)(src, *tgt); \
    } \
    void STRUCT_TOMAP_FUNCT_NAME_INTERNAL(TYPE)(TYPE *src, SkArgsMap &tgt)

#define SetupStructWrapper(TYPE) \
    tgt["t"] = TypeName(TYPE)

#define StructToMapProperty(NAME) \
    tgt[#NAME] = src->NAME

#define DeclareWrapper_STRUCT_FROMMAP(TYPE) \
    void STRUCT_FROMMAP_FUNCT_NAME_INTERNAL(TYPE)(SkArgsMap &src, TYPE *tgt); \
    void STRUCT_FROMMAP_FUNCT_NAME(TYPE)(SkArgsMap *src, void *tgtPtr) \
    { \
        StaticPlusDebug(TypeName(TYPE) << "::fromMap()"); \
        SkStructType *t = searchStructType(TypeName(TYPE)); \
        if (t->super) \
            t->super->fromMap(src, tgtPtr); \
        TYPE *tgt = static_cast<TYPE *>(tgtPtr); \
        STRUCT_FROMMAP_FUNCT_NAME_INTERNAL(TYPE)(*src, tgt); \
    } \
    void STRUCT_FROMMAP_FUNCT_NAME_INTERNAL(TYPE)(SkArgsMap &src, TYPE *tgt)

#define MapToStructProperty(NAME) \
     if (!src[#NAME].isNull()) tgt->NAME = src[#NAME]

#define MapToStructPropertyCasted(NAME, TO_TYPE, TO_METH) \
    if (!src[#NAME].isNull()) tgt->NAME = static_cast<TO_TYPE>(src[#NAME].TO_METH())

// // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // //
//SK_OBJLAMBDA

#define SK_OBJLAMBDA_T    std::function<void(SkObject *)>

// // // // // // // // // // // // // // // // // // // // //
//SIGNAL MACROs

#define SignalName(NAME)        NAME ## _SIGNAL

#define Signal(NAME) \
    void NAME(SkFlatObject *referer=nullptr) \
    { \
        if (!referer) \
            referer = this; \
        SignalName(NAME).trigger(referer); \
    } \
    void NAME(SkVariantVector &params) \
    {NAME(this, params);} \
    void NAME(SkFlatObject *referer, SkVariantVector &params) \
    { \
        if (!referer) \
            referer = this; \
        if (!params.isEmpty()) \
            SignalName(NAME).setParameters(params); \
        SignalName(NAME).trigger(referer); \
    } \
    void NAME ## Setup() \
    { \
        SignalName(NAME).setup(this, #NAME); \
        addSignal(&SignalName(NAME)); \
    } \
    SkSignal SignalName(NAME)

#define SignalSet(NAME)     NAME ## Setup()

// // // // // // // // // // // // // // // // // // // // //
//SLOT MACROs

#define SlotName(NAME)      NAME ## _SLOT

//MUST BE MORE EFFICIENT
//NOW, EACH INSTANCE MUST SEARCH ITS SLOTs FROM A STATIC RECIPIENT
//COULD BE ASSIGNED STATICALLY ONLY ONE TIME
#define Slot(NAME) \
    /*static constexpr*/ SkSlot * SlotName(NAME)/* = nullptr*/; \
    void NAME ## Setup() \
    {SlotName(NAME) = searchSlot(flatID, TypeName(NAME));} \
    void NAME(SkFlatObject *referer=nullptr); \
    void NAME(SkFlatObject *referer, va_list vl)

#define SlotImpl(OBJTYPE, NAME) \
    DECL_SLOT_METHOD(OBJTYPE, NAME) \
    PREPARE_SLOT_REGFUNCT_STRUCT(OBJTYPE, NAME); \
    void OBJTYPE::NAME(SkFlatObject *referer) {va_list vl; NAME(referer, vl);} \
    void OBJTYPE::NAME(SkFlatObject *referer, va_list vl)

#define SlotVirtual(NAME) \
    Slot(NAME) \
    {NAME ## Virtual(referer);} \
    virtual void NAME ## Virtual(SkFlatObject *) = 0

#define SlotVirtualImpl(OBJTYPE, NAME) \
    DECL_SLOT_METHOD(OBJTYPE, NAME) \
    PREPARE_SLOT_REGFUNCT_STRUCT(OBJTYPE, NAME)

// MUST BE AT THE END OF THE ConstructorImpl SCOPE
#define SlotSet(NAME)       NAME ## Setup()

#define SilentSlotArgsWarning() \
    ArgNotUsed(referer); \
    ArgNotUsed(vl)

// // // // // // // // // // // // // // // // // // // // //
//Constructors DECLARATION

#define AbstractConstructor(TYPE, SUPER) \
private: bool TYPE ## _inh; void objCtorScope(); \
    protected: TYPE(SkObject *parent, bool inheriting); \
    TYPE(CStr *name, SkObject *parent, bool inheriting)

#define Constructor(TYPE, SUPER) \
    AbstractConstructor(TYPE, SUPER); \
    public: \
    TYPE(); \
    TYPE(SkObject *parent); \
    TYPE(CStr *name); \
    TYPE(CStr *name, SkObject *parent)

// // // // // // // // // // // // // // // // // // // // //
//Constructors IMPLEMENTATION

//__VA_ARGS__ is used to optimize slot/meth/enum registration WITHOUT lambas

#define BaseConstructorImpl(TYPE, SUPER, ...) \
    SetupClassWrapper(TYPE) \
    {SetClassSuper(TYPE, SUPER); __VA_ARGS__} \
    TYPE::TYPE(SkObject *parent, bool) : SUPER(parent, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = true; \
        ObjectSetup(TYPE, true); \
        objCtorScope(); \
    } \
    TYPE::TYPE(CStr *name, SkObject *parent, bool) : SUPER(name, parent, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = true; \
        ObjectSetup(TYPE, true); \
        objCtorScope(); \
    } \
    void TYPE::objCtorScope()

#define ConstructorImpl(TYPE, SUPER, ...) \
    void TYPE ## MetaDestructor(SkObject *obj) \
    { \
        ObjectPlusDebug_EXT(obj, "Calling MetaDestructor for: " TypeName(TYPE)); \
        for(uint64_t i=0; i<obj->getDestructorCompains().count(); i++) \
            (*obj->getDestructorCompains().at(i))(obj); \
    } \
    DeclareSkObjectWrapper(TYPE); \
    TYPE::TYPE() : SUPER(nullptr, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = false; \
        ObjectSetup(TYPE, false); \
        objCtorScope(); \
    } \
    TYPE::TYPE(SkObject *parent) : SUPER(parent, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = false; \
        ObjectSetup(TYPE, false); \
        objCtorScope(); \
    } \
    TYPE::TYPE(CStr *name) : SUPER(name, nullptr, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = false; \
        ObjectSetup(TYPE, false); \
        objCtorScope(); \
    } \
    TYPE::TYPE(CStr *name, SkObject *parent) : SUPER(name, parent, true) \
    { \
        CreateClassWrapper(TYPE); \
        this->TYPE ## _inh = false; \
        ObjectSetup(TYPE, false); \
        objCtorScope(); \
    } \
    BaseConstructorImpl(TYPE, SUPER, __VA_ARGS__) \

#define AbstractConstructorImpl(TYPE, SUPER, ...) \
    void TYPE ## MetaDestructor(SkObject *obj) \
    { \
        ObjectPlusDebug_EXT(obj, "Calling MetaDestructor for: " TypeName(TYPE)); \
        for(uint64_t i=0; i<obj->getDestructorCompains().count(); i++) \
            (*obj->getDestructorCompains().at(i))(obj); \
    } \
    DeclareSkObjectWrapper_NOT_INSTANCIABLE(TYPE); \
    BaseConstructorImpl(TYPE, SUPER, __VA_ARGS__)

#define addDtorCompanion(NAME) \
    destructorCompains.append(new SK_OBJLAMBDA_T(NAME))

// // // // // // // // // // // // // // // // // // // // //
//SETUP

#define ObjectSetup(TYPE, INH) \
    FakeSingleLine( \
        bool inh = INH; \
        if (!inh) \
        { \
            destructor = SK_OBJLAMBDA_T(TYPE ## MetaDestructor); \
            /*registerOnSkApp();*/ \
        } \
        else \
        { \
            if (objName.empty()) \
            { \
                SkString tempObjName(TypeName(TYPE)); \
                createDefaultObjName(tempObjName); \
            } \
        } \
    )

// // // // // // // // // // // // // // // // // // // // //
//STATIC AUTO-LAUNCH FUNCTION

#define StaticAutoLaunch(STRUCT_NAME, INSTANCE_NAME) \
    struct STRUCT_NAME {STRUCT_NAME();}; \
    static STRUCT_NAME INSTANCE_NAME; \
    STRUCT_NAME::STRUCT_NAME() \

// // // // // // // // // // // // // // // // // // // // //

//LIBRARIES EXPORTs/IMPORTs

#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#  define SK_DECL_EXPORT        __declspec(dllexport)
#  define SK_DECL_IMPORT        __declspec(dllimport)
#else
#  define SK_DECL_EXPORT        __attribute__((visibility("default")))
#  define SK_DECL_IMPORT        __attribute__((visibility("default")))
#endif

#if defined(SPECIALK_LIBRARY)
#  define SPECIALK SK_DECL_EXPORT
#else
#  define SPECIALK SK_DECL_IMPORT
#endif

#if defined(SKAUDIO_LIBRARY)
#  define SKAUDIO SK_DECL_EXPORT
#else
#  define SKAUDIO SK_DECL_IMPORT
#endif

#if defined(SKVIDEO_LIBRARY)
#  define SKVISION SK_DECL_EXPORT
#else
#  define SKVISION SK_DECL_IMPORT
#endif

#if defined(SKMULTIMEDIA_LIBRARY)
#  define SKMULTIMEDIA SK_DECL_EXPORT
#else
#  define SKMULTIMEDIA SK_DECL_IMPORT
#endif

#if defined(SKIOT_LIBRARY)
#  define SKIOT SK_DECL_EXPORT
#else
#  define SKIOT SK_DECL_IMPORT
#endif

#if defined(SKDATABASE_LIBRARY)
#  define SKDATABASE SK_DECL_EXPORT
#else
#  define SKDATABASE SK_DECL_IMPORT
#endif

#if defined(SKGUI_LIBRARY)
#  define SKGUI SK_DECL_EXPORT
#else
#  define SKGUI SK_DECL_IMPORT
#endif

#if defined(SKGL_LIBRARY)
#  define SKGL SK_DECL_EXPORT
#else
#  define SKGL SK_DECL_IMPORT
#endif

#if defined(SKSCRIPT_LIBRARY)
#  define SKSCRIPT SK_DECL_EXPORT
#else
#  define SKSCRIPT SK_DECL_IMPORT
#endif

// // // // // // // // // // // // // // // // // // // // //

#endif // SKDEFINES_H
