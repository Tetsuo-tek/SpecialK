#ifndef SKFLOWAUDIOVORBISPUBLISHER_H
#define SKFLOWAUDIOVORBISPUBLISHER_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include <Multimedia/Audio/Encoders/skvorbisencoder.h>

class SkflowAudioVorbisPublisher extends SkFlowGenericPublisher
{
    SkOggVorbisTh *vorbisTh;
    SkAudioParameters params;

    public:
        Constructor(SkflowAudioVorbisPublisher, SkFlowGenericPublisher);

        bool setup(SkAudioParameters &audioParameters);

        void tick(float *data);

    private:
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onChannelReady()                                           override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWAUDIOVORBISPUBLISHER_H
