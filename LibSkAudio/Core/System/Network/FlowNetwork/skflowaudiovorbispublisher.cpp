#include "skflowaudiovorbispublisher.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkflowAudioVorbisPublisher, SkFlowGenericPublisher)
{
    vorbisTh = nullptr;
}

bool SkflowAudioVorbisPublisher::setup(SkAudioParameters &audioParameters)
{
    params = audioParameters;

    return SkFlowGenericPublisher::setup("OGA", FT_AUDIO_DATA, T_BYTEARRAY, "audio/ogg", nullptr);
}

void SkflowAudioVorbisPublisher::onStart()
{
    vorbisTh = new SkOggVorbisTh;
    vorbisTh->setObjectName(this, "OgaTh");

    //
    SkVorbisEncProperties vorbisProps;
    vorbisProps.mode = VBM_VBR;
    vorbisProps.vbrQuality = 0.7;
    //

    AssertKiller(!vorbisTh->init(params, vorbisProps));
    AssertKiller(vorbisTh->header().isEmpty());

    ObjectMessage("OGA HEADER produced: " << vorbisTh->header().size() << " B");

    vorbisTh->start();

    ulong tickIntervalUS = static_cast<ulong>(params.getTickTimePeriod() * 1000000.);

    if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
    {
        ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
        eventLoop()->changeFastZone(tickIntervalUS);
        eventLoop()->changeSlowZone(eventLoop()->getFastInterval()*4);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }
}

void SkflowAudioVorbisPublisher::onStop()
{
    if (vorbisTh && vorbisTh->isRunning())
    {
        vorbisTh->quit();
        vorbisTh->wait();
        vorbisTh->destroyLater();
        vorbisTh = nullptr;
    }
}

void SkflowAudioVorbisPublisher::tick(float *data)
{
    if (!hasTargets())
        return;

    vorbisTh->input()->addData(SkArrayCast::toChar(data), params.getCanonicalSize());

    SkRingBuffer *o = vorbisTh->output();

    if (!o->isEmpty())
    {
        uint64_t sz = o->size();
        char *ogg = new char [sz];

        o->getCustomBuffer(ogg, sz);

        if (SkAbstractFlowPublisher::hasTargets(channel))
            async->publish(channelID, ogg, sz);

#if defined(ENABLE_HTTP)
        if (SkAbstractFlowPublisher::hasTargets(mp))
            mp->send(ogg, sz);
#endif
        delete [] ogg;
    }
}

void SkflowAudioVorbisPublisher::onChannelReady()
{
    SkArgsMap props;
    params.toMap(props);

    props["stream"] = "OGG";
    props["codec"] = "VORBIS";
    props["min"] = -1.0f;
    props["max"] = 1.0f;

    async->setChannelProperties(channelID, props);
    async->setChannelHeader(channelID, vorbisTh->header());

/*#if defined(ENABLE_HTTP)
    if (http)
        mp->setHeader(&vorbisTh->header());
#endif*/
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
