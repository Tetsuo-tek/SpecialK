#include "skflowaudiopublisher.h"

#if defined(ENABLE_AUDIO)

#include "Core/Containers/skarraycast.h"
#include "Core/System/skdeviceredistr.h"
#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"

#include "Multimedia/Audio/Encoders/skaudiowave.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkFlowAudioPublisher, SkAbstractFlowPublisher)
{
    input = nullptr;
    buffer = nullptr;

    floatOutput = nullptr;
    intOutput = nullptr;
    shortOutput = nullptr;
    charOutput = nullptr;

    doublePublisher = nullptr;
    floatPublisher = nullptr;
    intPublisher = nullptr;
    shortPublisher = nullptr;
    charPublisher = nullptr;

    vuPublisher = nullptr;
    vuMonoPublisher = nullptr;
    clippingPublisher = nullptr;

#if defined(ENABLE_FFTW3LIB)
    fftPublisher = nullptr;
    fftGaussianEnabled = false;
#endif

    mgmPublisher = nullptr;

#if defined(ENABLE_AUDIOENCLIBS)
    vorbisPublisher = nullptr;
#endif

#if defined(ENABLE_PORTAUDIOLIB)
    pa = nullptr;
#endif
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAudioPublisher::setup(SkAudioParameters &audioParameters)
{
    if (!params.set(false, audioParameters.getChannels(), audioParameters.getSampleRate(), audioParameters.getBufferFrames(), audioParameters.getFormat()))
    {
        ObjectError("Configuration is NOT valid");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAudioPublisher::setInputBuffer(SkRingBuffer *inputProduction)
{
    input = inputProduction;
}

#if defined(ENABLE_FFTW3LIB)

void SkFlowAudioPublisher::enableFftGaussianFilter(bool enabled)
{
    fftGaussianEnabled = enabled;
}

#endif

#if defined(ENABLE_PORTAUDIOLIB)

SkStringList SkFlowAudioPublisher::getDevicesNames()
{
    SkPortAudio *pa = new SkPortAudio(this);

    AssertKiller(!pa->init());
    SkStringList &devList = pa->getDevicesNames();
    pa->close();
    pa->destroyLater();//it is asynch

    return devList;
}

void SkFlowAudioPublisher::enableCapture(CStr *deviceName)
{
    device = deviceName;
}

bool SkFlowAudioPublisher::startCapture()
{
    pa = new SkPortAudio(this);
    pa->setObjectName(this, "PortAudio");

    if (!pa->init())
    {
        pa->destroyLater();
        pa = nullptr;
        return false;
    }

    input = pa->getProduction().inputProduction;

    if (!pa->rec(device.c_str(), params))
    {
        pa->destroyLater();
        pa = nullptr;
        return false;
    }

    return true;
}

void SkFlowAudioPublisher::stopCapture()
{
    if (!pa)
        return;

    if (pa->isInitialized())
    {
        if (pa->isStreamActive())
            pa->stop();

        pa->close();
    }

    pa->destroyLater();
    pa = nullptr;

    device.clear();
}

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAudioPublisher::start()
{
#if defined(ENABLE_PORTAUDIOLIB)
    if (!device.isEmpty())
        startCapture();
#endif

    buffer = new double [params.getChannels()*params.getBufferFrames()];

    a.setObjectName(this, "AudioBuffer");
    a.setAudioParameters(params, input);

    SkAudioParameters p;

    doublePublisher = DynCast(SkFlowAudioPcmPublisher, createPublisher(new SkFlowAudioPcmPublisher(this)));
    p.set(true, params.getChannels(), params.getSampleRate(), params.getBufferFrames(), AFMT_DOUBLE);
    doublePublisher->setup(p);
    doublePublisher->start();

    floatPublisher = DynCast(SkFlowAudioPcmPublisher, createPublisher(new SkFlowAudioPcmPublisher(this)));
    p.set(true, params.getChannels(), params.getSampleRate(), params.getBufferFrames(), AFMT_FLOAT);
    a.addOutput("Float", p, nullptr, false);
    floatOutput = a.getOutputByName("Float");
    floatPublisher->setup(p);
    floatPublisher->start();

    intPublisher = DynCast(SkFlowAudioPcmPublisher, createPublisher(new SkFlowAudioPcmPublisher(this)));
    p.set(true, params.getChannels(), params.getSampleRate(), params.getBufferFrames(), AFMT_INT);
    a.addOutput("Int", p, nullptr, false);
    intOutput = a.getOutputByName("Int");
    intPublisher->setup(p);
    intPublisher->start();

    shortPublisher = DynCast(SkFlowAudioPcmPublisher, createPublisher(new SkFlowAudioPcmPublisher(this)));
    p.set(true, params.getChannels(), params.getSampleRate(), params.getBufferFrames(), AFMT_SHORT);
    a.addOutput("Short", p, nullptr, false);
    shortOutput = a.getOutputByName("Short");
    shortPublisher->setup(p);
    shortPublisher->start();

    charPublisher = DynCast(SkFlowAudioPcmPublisher, createPublisher(new SkFlowAudioPcmPublisher(this)));
    p.set(true, params.getChannels(), params.getSampleRate(), params.getBufferFrames(), AFMT_CHAR);
    a.addOutput("Char", p, nullptr, false);
    charOutput = a.getOutputByName("Char");
    charPublisher->setup(p);
    charPublisher->start();

    SkArgsMap props;
    params.toMap(props);

    clippingPublisher = DynCast(SkFlowGenericPublisher, createPublisher(new SkFlowGenericPublisher(this)));
    clippingPublisher->setup("Clipping", FT_AUDIO_CLIPPING, T_UINT8, nullptr, &props);
    clippingPublisher->start();

    props["min"] = -25.;
    props["max"] = 0.;

    vuPublisher = DynCast(SkFlowGenericPublisher, createPublisher(new SkFlowGenericPublisher(this)));
    vuPublisher->setup("VuMeter", FT_AUDIO_VUMETER, T_FLOAT, nullptr, &props);
    vuPublisher->start();

    vuMonoPublisher = DynCast(SkFlowGenericPublisher, createPublisher(new SkFlowGenericPublisher(this)));
    vuMonoPublisher->setup("VuMeterMono", FT_AUDIO_VUMETER, T_FLOAT, nullptr, &props);
    vuMonoPublisher->start();

#if defined(ENABLE_FFTW3LIB)
    fftPublisher = DynCast(SkFlowAudioFftPublisher, createPublisher(new SkFlowAudioFftPublisher(this)));
    fftPublisher->setup(params, a.getPlanarRawBufferData(), fftGaussianEnabled);
    fftPublisher->start();
#endif

    mgmPublisher = DynCast(SkFlowAudioMagmaPublisher, createPublisher(new SkFlowAudioMagmaPublisher(this)));
    mgmPublisher->setup(floatOutput->audioParameters);
    mgmPublisher->start();

#if defined(ENABLE_AUDIOENCLIBS)
    vorbisPublisher = DynCast(SkflowAudioVorbisPublisher, createPublisher(new SkflowAudioVorbisPublisher(this)));
    vorbisPublisher->setup(floatOutput->audioParameters);
    vorbisPublisher->start();
#endif

    ObjectMessage("Input canonical size is: " << input->getCanonicalSize() << " B");

    return true;
}

void SkFlowAudioPublisher::stop()
{
#if defined(ENABLE_PORTAUDIOLIB)
    if (pa)
        stopCapture();
#endif

    /*eventLoop()->changeFastZone(250000);
    eventLoop()->changeSlowZone(250000);
    eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);*/

    floatOutput = nullptr;
    intOutput = nullptr;
    shortOutput = nullptr;
    charOutput = nullptr;

    a.deleteAllOutputs();

    delete [] buffer;
    buffer = nullptr;

    doublePublisher->stop();
    doublePublisher->destroyLater();
    doublePublisher = nullptr;

    floatPublisher->stop();
    floatPublisher->destroyLater();
    floatPublisher = nullptr;

    intPublisher->stop();
    intPublisher->destroyLater();
    intPublisher = nullptr;

    shortPublisher->stop();
    shortPublisher->destroyLater();
    shortPublisher = nullptr;

    charPublisher->stop();
    charPublisher->destroyLater();
    charPublisher = nullptr;

#if defined(ENABLE_FFTW3LIB)
    fftPublisher->stop();
    fftPublisher->destroyLater();
    fftPublisher = nullptr;
#endif

    vuPublisher->stop();
    vuPublisher->destroyLater();
    vuPublisher = nullptr;

    vuMonoPublisher->stop();
    vuMonoPublisher->destroyLater();
    vuMonoPublisher = nullptr;

    clippingPublisher->stop();
    clippingPublisher->destroyLater();
    clippingPublisher = nullptr;

    mgmPublisher->stop();
    mgmPublisher->destroyLater();
    mgmPublisher = nullptr;

#if defined(ENABLE_AUDIOENCLIBS)
    vorbisPublisher->stop();
    vorbisPublisher->destroyLater();
    vorbisPublisher = nullptr;
#endif

}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAudioPublisher::setMute(bool muted)
{
    a.setMute(muted);
}

void SkFlowAudioPublisher::setGain(float value)
{
    a.setGain(value);
}

void SkFlowAudioPublisher::setBass(float value)
{
    a.setBass(value);
}

void SkFlowAudioPublisher::setMiddle(float value)
{
    a.setMiddle(value);
}

void SkFlowAudioPublisher::setTreble(float value)
{
    a.setTreble(value);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAudioPublisher::onChannelAdded(SkFlowChanID)
{
}

void SkFlowAudioPublisher::onChannelRemoved(SkFlowChanID)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkFlowAudioPublisher::tick()
{
    if (!input)
        return;

    if (input->isEmpty())
        return;

    if (!buffer || !hasTargets())
    {
        if (input && !input->isEmpty())
            input->clear();

        return;
    }

    uint ch = params.getChannels();
    bool optActive = false;

#if defined(ENABLE_AUDIOENCLIBS)
    optActive = vorbisPublisher->hasTargets();
#endif

    floatOutput->enabled = floatPublisher->hasTargets()|| mgmPublisher->hasTargets() || optActive;
    intOutput->enabled = intPublisher->hasTargets();
    shortOutput->enabled = shortPublisher->hasTargets();
    charOutput->enabled = charPublisher->hasTargets();

    a.tick();

    if (doublePublisher->hasTargets())
    {
        uint fr = params.getBufferFrames();
        uint64_t sz = fr*sizeof(double);

        for(uint z=0; z<ch; z++)
            memcpy(&buffer[z*fr], a.getPlanarRawBufferData(z), sz);

        sz *= ch;
        doublePublisher->tick(buffer, sz);
    }

    floatPublisher->tick(floatOutput->currentData, floatOutput->audioParameters.getCanonicalSize());
    intPublisher->tick(intOutput->currentData, intOutput->audioParameters.getCanonicalSize());
    shortPublisher->tick(shortOutput->currentData, shortOutput->audioParameters.getCanonicalSize());
    charPublisher->tick(charOutput->currentData, charOutput->audioParameters.getCanonicalSize());

    if (vuPublisher->hasTargets() || vuMonoPublisher->hasTargets() || clippingPublisher->hasTargets())
    {
        uint channels = params.getChannels();
        float vu[channels];

        for(uint z=0; z<channels; z++)
        {
            vu[z] = a.getLastChanVolumeUnits(z);

            if (clippingPublisher->hasTargets() && a.isClipping(z))
                clippingPublisher->tick(&z, sizeof(uint8_t));
        }

        if (vuPublisher->hasTargets())
            vuPublisher->tick(vu, channels*sizeof(float));

        if (vuMonoPublisher->hasTargets())
        {
            float vuMono = a.getLastMonoVolumeUnits();
            vuMonoPublisher->tick(&vuMono, sizeof(float));
        }
    }


#if defined(ENABLE_FFTW3LIB)
    fftPublisher->tick();
#endif

    mgmPublisher->tick(SkArrayCast::toFloat(floatOutput->currentData));

#if defined(ENABLE_AUDIOENCLIBS)
    vorbisPublisher->tick(SkArrayCast::toFloat(floatOutput->currentData));
#endif

}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkFlowAudioPublisher::hasTargets()
{
    return (doublePublisher->hasTargets()
            || floatPublisher->hasTargets()
            || intPublisher->hasTargets()
            || shortPublisher->hasTargets()
            || charPublisher->hasTargets()
            || vuPublisher->hasTargets()
            || vuMonoPublisher->hasTargets()
            || clippingPublisher->hasTargets()
            || mgmPublisher->hasTargets()
#if defined(ENABLE_FFTW3LIB)
            || fftPublisher->hasTargets()
#endif
#if defined(ENABLE_AUDIOENCLIBS)
            || vorbisPublisher->hasTargets()
#endif
            );
}

SkAudioParameters &SkFlowAudioPublisher::parameters()
{
    return params;
}

SkAudioBuffer &SkFlowAudioPublisher::audioBuffer()
{
    return a;
}

SkFlowAudioPcmPublisher *SkFlowAudioPublisher::getDoublePublisher()
{
    return doublePublisher;
}

SkFlowAudioPcmPublisher *SkFlowAudioPublisher::getFloatPublisher()
{
    return floatPublisher;
}

SkFlowAudioPcmPublisher *SkFlowAudioPublisher::getIntPublisher()
{
    return intPublisher;
}

SkFlowAudioPcmPublisher *SkFlowAudioPublisher::getShortPublisher()
{
    return shortPublisher;
}

SkFlowAudioPcmPublisher *SkFlowAudioPublisher::getCharPublisher()
{
    return charPublisher;
}

SkFlowGenericPublisher *SkFlowAudioPublisher::getVuPublisher()
{
    return vuPublisher;
}

SkFlowGenericPublisher *SkFlowAudioPublisher::getVuMonoPublisher()
{
    return vuMonoPublisher;
}

SkFlowGenericPublisher *SkFlowAudioPublisher::getClippingPublisher()
{
    return clippingPublisher;
}

#if defined(ENABLE_FFTW3LIB)

SkFlowAudioFftPublisher *SkFlowAudioPublisher::getFftPublisher()
{
    return fftPublisher;
}

#endif

SkFlowAudioMagmaPublisher *SkFlowAudioPublisher::getMagmaPublisher()
{
    return mgmPublisher;
}

#if defined(ENABLE_AUDIOENCLIBS)

SkflowAudioVorbisPublisher *SkFlowAudioPublisher::getOggVorbisPublisher()
{
    return vorbisPublisher;
}

#endif
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
