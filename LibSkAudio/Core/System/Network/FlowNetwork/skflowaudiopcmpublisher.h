#ifndef SKFLOWAUDIOPCMPUBLISHER_H
#define SKFLOWAUDIOPCMPUBLISHER_H

#if defined(ENABLE_AUDIO)

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include "Multimedia/Audio/skaudioparameters.h"

class SkFlowAudioPcmPublisher extends SkFlowGenericPublisher
{
    SkAudioParameters params;

    public:
        Constructor(SkFlowAudioPcmPublisher, SkFlowGenericPublisher);

        bool setup(SkAudioParameters &audioParameters, CStr *chNamePfx="");

        void tick(CVoid *data, ULong sz);

    private:
        void onStart()                                                  override;
        void onChannelReady()                                           override;

#if defined(ENABLE_HTTP)
        void setWaveBinHeader(SkAudioParameters &parameters, SkRawRedistrMountPoint *mp);
#endif
};

#endif

#endif // SKFLOWAUDIOPCMPUBLISHER_H
