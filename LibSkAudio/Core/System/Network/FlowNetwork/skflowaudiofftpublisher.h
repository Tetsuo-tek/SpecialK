#ifndef SKFLOWAUDIOFFTPUBLISHER_H
#define SKFLOWAUDIOFFTPUBLISHER_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_FFTW3LIB)

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include "Multimedia/Audio/skaudioparameters.h"
#include "Multimedia/skdirectfft.h"

class SkFlowAudioFftPublisher extends SkFlowGenericPublisher
{
    SkAudioParameters params;
    double **planarBuffer;

    bool fftGaussianEnabled;
    SkVector<SkDirectFFT *> ffts;
    UInt fftSize;
    float freqInterval;
    float *fftData;

    public:
        Constructor(SkFlowAudioFftPublisher, SkFlowGenericPublisher);

        bool setup(SkAudioParameters &audioParameters, double **planarDoubleBuffer, bool enableGaussianFilter, CStr *chNamePfx="");

        void tick();

    private:
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onChannelReady()                                           override;
};

#endif

#endif // SKFLOWAUDIOFFTPUBLISHER_H
