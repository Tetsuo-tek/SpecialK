#include "skflowaudiosubscriber.h"

#if defined(ENABLE_AUDIO)

#include "Core/Containers/skarraycast.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkFlowAudioSubscriber, SkAbstractFlowSubscriber)
{
    mgm = nullptr;
    aDec = nullptr;
}

void SkFlowAudioSubscriber::enableVuChan(bool enabled)
{
    vu.enabled = enabled;
}

void SkFlowAudioSubscriber::enableVuMonoChan(bool enabled)
{
    vuMono.enabled = enabled;
}

void SkFlowAudioSubscriber::enableClippingChan(bool enabled)
{
    clipping.enabled = enabled;
}

void SkFlowAudioSubscriber::enableFftChan(bool enabled)
{
    fft.enabled = enabled;
}

void SkFlowAudioSubscriber::subscribe(CStr *pcmChanName)
{
    setChannelName(pcmChanName);

    vu.chanName = ownerName();
    vu.chanName.append(".VuMeter");

    vuMono.chanName = ownerName();
    vuMono.chanName.append(".VuMeterMono");

    clipping.chanName = ownerName();
    clipping.chanName.append(".Clipping");

    if (vu.enabled && async->containsChannel(vu.chanName.c_str()))
        onChannelAdded(async->channelID(vu.chanName.c_str()));

    if (vuMono.enabled && async->containsChannel(vuMono.chanName.c_str()))
        onChannelAdded(async->channelID(vuMono.chanName.c_str()));

    if (clipping.enabled && async->containsChannel(clipping.chanName.c_str()))
        onChannelAdded(async->channelID(clipping.chanName.c_str()));

    if (fft.enabled && async->containsChannel(fft.chanName.c_str()))
        onChannelAdded(async->channelID(fft.chanName.c_str()));
}

void SkFlowAudioSubscriber::unsubscribe()
{
    if (!isReady())
        return;

    if (vu.ch && async->isSubscribed(vu.ch->chanID))
        async->unsubscribeChannel(vu.ch);

    if (vuMono.ch && async->isSubscribed(vuMono.ch->chanID))
        async->unsubscribeChannel(vuMono.ch);

    if (clipping.ch && async->isSubscribed(clipping.ch->chanID))
        async->unsubscribeChannel(clipping.ch);

    if (fft.ch && async->isSubscribed(fft.ch->chanID))
        async->unsubscribeChannel(fft.ch);

    async->unsubscribeChannel(source());
    reset();
}

void SkFlowAudioSubscriber::tick(SkFlowChannelData &chData)
{
    if (!isReady() || chData.data.isEmpty())
        return;

    if (chData.chanID == source()->chanID)
    {
        void *d = chData.data.toVoid();

        if (aDec)
        {
            mgm->tick(SkArrayCast::toChar(d), chData.data.size());

            uint frames = p.getBufferFrames();
            uint channels = p.getChannels();
            uint64_t sz = frames*sizeof(float);

            SkDataBuffer b;

            for(uint z=0; z<channels; z++)
                b.append(SkArrayCast::toCStr(aDec->pcm()[z]), sz);

            a.tick(SkArrayCast::toChar(b.toVoid()));
        }

        else
            a.tick(SkArrayCast::toChar(d));
    }
}

void SkFlowAudioSubscriber::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (ch->name == chanName())
    {
        AssertKiller(ch->flow_t != FT_AUDIO_DATA && ch->flow_t != FT_AUDIO_PREVIEW_DATA);

        ObjectMessage("Source pcm-channel is READY");

        //  //  //  //  //
        SkFlowSync *sync = async->buildSyncClient();

        AssertKiller(!sync->existsOptionalPairDb(ch->name.c_str()));
        AssertKiller(!sync->setCurrentDbName(ch->name.c_str()));

        SkVariant v;

        SkString streamFMT;
        SkString streamCDC;
        uint channels = 0;
        uint bufferFrames = 0;
        uint sampleRate = 0;
        SkAudioFmtType t;

        if (sync->getVariable("stream", v))
            streamFMT = v.toString();

        if (sync->getVariable("codec", v))
            streamCDC = v.toString();

        if (sync->getVariable("channels", v))
            channels = v.toUInt();

        if (sync->getVariable("bufferFrames", v))
            bufferFrames = v.toUInt();

        if (sync->getVariable("sampleRate", v))
            sampleRate = v.toUInt();

        if (sync->getVariable("fmt", v))
            t = static_cast<SkAudioFmtType>(v.toUInt());

        bool isPlanar = false;

        if (sync->getVariable("isPlanar", v))
            isPlanar = v.toBool();

        AssertKiller(!p.set(isPlanar, channels, sampleRate, bufferFrames, t));

        if (streamFMT == "PCM")
            AssertKiller(streamCDC != "RAW");

        else if (streamFMT == "MAGMA")
        {
            AssertKiller(streamCDC != "PSYA");

            mgm = new SkMagmaReader(this);
            mgm->setObjectName(this, "Decoder");

            AssertKiller(!ch->hasHeader || !mgm->open(ch->header)/* || !mgm->count()*/);
            aDec = DynCast(SkPsyADec, mgm->streams()[0]);
            AssertKiller(!aDec);
        }

        else
            AssertKiller(streamFMT != "PCM" && streamFMT!= "MAGMA");

        a.setAudioParameters(p);

        //ulong tickTimeMicros = paramsMap["tickTimePeriod"].toFloat()*1000000;

        ulong tickIntervalUS = 0;

        if (sync->getVariable("tickTimePeriod", v))
            tickIntervalUS = v.toFloat()*1000000;

        if (tickIntervalUS)
        {
            if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
            {
                ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
                eventLoop()->changeFastZone(tickIntervalUS);
                eventLoop()->changeSlowZone(tickIntervalUS*4);
                eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
            }
        }

        sync->close();
        sync->destroyLater();

        setSource(ch);
    }

    else if (vu.enabled && ch->name == vu.chanName)
    {
        vu.ch = ch;
        ObjectMessage("Enabling source optional channel: " << vu.chanName);
        async->subscribeChannel(vu.ch);
    }

    else if (vuMono.enabled && ch->name == vuMono.chanName)
    {
        vuMono.ch = ch;
        ObjectMessage("Enabling source optional channel: " << vuMono.chanName);
        async->subscribeChannel(vuMono.ch);
    }

    else if (clipping.enabled && ch->name == clipping.chanName)
    {
        clipping.ch = ch;
        ObjectMessage("Enabling source optional channel: " << clipping.chanName);
        async->subscribeChannel(clipping.ch);
    }

    else if (fft.enabled && ch->name == fft.chanName)
    {
        fft.ch = ch;
        ObjectMessage("Enabling source optional channel: " << fft.chanName);
        async->subscribeChannel(fft.ch);
    }
}

void SkFlowAudioSubscriber::onChannelRemoved(SkFlowChanID chanID)
{
    if (!isReady())
        return;

    SkFlowChannel *ch = async->channel(chanID);
    AssertKiller(!ch);

    if (ch->chanID == source()->chanID)
        reset();

    else if (vu.ch)
        vu.ch = nullptr;

    else if (vuMono.ch)
        vuMono.ch = nullptr;

    else if (clipping.ch)
        clipping.ch = nullptr;

    else if (fft.ch)
        fft.ch = nullptr;
}

bool SkFlowAudioSubscriber::hasVuChannel()
{
    return (vu.ch != nullptr);
}

SkFlowChannel *SkFlowAudioSubscriber::vuChannel()
{
    return vu.ch;
}

bool SkFlowAudioSubscriber::hasVuMonoChannel()
{

    return (vuMono.ch != nullptr);
}

SkFlowChannel *SkFlowAudioSubscriber::vuMonoChannel()
{
    return vuMono.ch;
}

bool SkFlowAudioSubscriber::hasClippingChannel()
{
    return (clipping.ch != nullptr);
}

SkFlowChannel *SkFlowAudioSubscriber::clippingChannel()
{
    return clipping.ch;
}

bool SkFlowAudioSubscriber::hasFftChannel()
{
    return (fft.ch != nullptr);
}

SkFlowChannel *SkFlowAudioSubscriber::fftChannel()
{
    return fft.ch;
}

SkAudioBuffer &SkFlowAudioSubscriber::audioBuffer()
{
    return a;
}

SkAudioParameters &SkFlowAudioSubscriber::parameters()
{
    return p;
}

double **SkFlowAudioSubscriber::internalPlanarBuffer()
{
    return a.getPlanarRawBufferData();
}

void SkFlowAudioSubscriber::close()
{
    reset();
}

void SkFlowAudioSubscriber::reset()
{
    SkAbstractFlowSubscriber::reset();

    vu = SkFlowSubscriberOptChans();
    vuMono = SkFlowSubscriberOptChans();
    clipping = SkFlowSubscriberOptChans();
    fft = SkFlowSubscriberOptChans();

    if (mgm)
    {
        if (mgm->isOpen())
            mgm->close();

        mgm->destroyLater();
    }

    mgm = nullptr;
    aDec = nullptr;
}

#endif
