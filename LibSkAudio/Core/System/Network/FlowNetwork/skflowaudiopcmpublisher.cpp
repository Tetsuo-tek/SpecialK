#include "skflowaudiopcmpublisher.h"
#include "Core/App/skeventloop.h"

#if defined(ENABLE_AUDIO)

#include "Core/Containers/skarraycast.h"
#include "Core/System/skdeviceredistr.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"
    #include "Multimedia/Audio/Encoders/skaudiowave.h"
#endif

ConstructorImpl(SkFlowAudioPcmPublisher, SkFlowGenericPublisher)
{
}

bool SkFlowAudioPcmPublisher::setup(SkAudioParameters &audioParameters, CStr *chNamePfx)
{
    SkAudioProps p = audioParameters.getProperties();

    if (!params.set(p))
    {
        ObjectError("Configuration is NOT valid");
        return false;
    }

    SkString chName;

    if (!SkString::isEmpty(chNamePfx))
    {
        chName = chNamePfx;
        chName.append(".");
    }

    SkAudioFmtType audio_t = params.getFormat();
    SkVariant_T data_t = T_NULL;

    if (audio_t == AFMT_DOUBLE)
    {
        data_t = T_DOUBLE;
        chName.append("Double");
    }

    else if (audio_t == AFMT_FLOAT)
    {
        data_t = T_FLOAT;
        chName.append("Float");
    }

    else if (audio_t == AFMT_INT)
    {
        data_t = T_INT32;
        chName.append("Int");
    }

    else if (audio_t == AFMT_SHORT)
    {
        data_t = T_INT16;
        chName.append("Short");
    }

    else if (audio_t == AFMT_CHAR)
    {
        data_t = T_INT8;
        chName.append("Char");
    }

    return SkFlowGenericPublisher::setup(chName.c_str(), FT_AUDIO_DATA, data_t, "audio/wav", nullptr);
}

void SkFlowAudioPcmPublisher::onStart()
{
    ulong tickIntervalUS = static_cast<ulong>(params.getTickTimePeriod() * 1000000.);

    if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
    {
        ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
        eventLoop()->changeFastZone(tickIntervalUS);
        eventLoop()->changeSlowZone(tickIntervalUS*4);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }
}

void SkFlowAudioPcmPublisher::tick(CVoid *data, ULong sz)
{
    if (!hasTargets())
        return;

    if (SkAbstractFlowPublisher::hasTargets(channel))
        async->publish(channelID, data, sz);

#if defined(ENABLE_HTTP)
    if (SkAbstractFlowPublisher::hasTargets(mp))
    {
        //Browser WANTS pcm-interleaved
        if (params.isPlanar())
        {
            SkAudioFmtType audio_t = params.getFormat();
            uint channels = params.getChannels();
            uint bufferFrames = params.getBufferFrames();

            if (audio_t == AFMT_DOUBLE)
            {
                const double *planar = StCast(const double, data);
                double interleaved[bufferFrames*channels];
                SkAudioQuantizer::planarToInterleaved_DOUBLE(channels, bufferFrames, planar, interleaved);
                mp->send(SkArrayCast::toCStr(interleaved), sz);
            }

            else if (audio_t == AFMT_FLOAT)
            {
                const float *planar = StCast(const float, data);
                float interleaved[bufferFrames*channels];
                SkAudioQuantizer::planarToInterleaved_FLOAT(channels, bufferFrames, planar, interleaved);
                mp->send(SkArrayCast::toCStr(interleaved), sz);
            }

            else if (audio_t == AFMT_INT)
            {
                const Int *planar = StCast(const Int, data);
                Int interleaved[bufferFrames*channels];
                SkAudioQuantizer::planarToInterleaved_INT(channels, bufferFrames, planar, interleaved);
                mp->send(SkArrayCast::toCStr(interleaved), sz);
            }

            else if (audio_t == AFMT_SHORT)
            {
                const short *planar = StCast(const short, data);
                short interleaved[bufferFrames*channels];
                SkAudioQuantizer::planarToInterleaved_SHORT(channels, bufferFrames, planar, interleaved);
                mp->send(SkArrayCast::toCStr(interleaved), sz);
            }

            else if (audio_t == AFMT_CHAR)
            {
                const char *planar = StCast(const char, data);
                char interleaved[bufferFrames*channels];
                SkAudioQuantizer::planarToInterleaved_CHAR(channels, bufferFrames, planar, interleaved);
                mp->send(SkArrayCast::toCStr(interleaved), sz);
            }
        }

        else
            mp->send(SkArrayCast::toCStr(data), sz);
    }
#endif
}

#if defined(ENABLE_HTTP)

void SkFlowAudioPcmPublisher::setWaveBinHeader(SkAudioParameters &parameters, SkRawRedistrMountPoint *mp)
{
    ObjectWarning("Saved WAV header on mountpoint");

    SkDataBuffer b;

    SkBufferDevice *dev = new SkBufferDevice(this);
    dev->open(b, BVM_ONLYWRITE);

    SkAudioWave::writeHeader(parameters, dev);
    mp->setHeader(&b);

    dev->close();
    dev->destroyLater();

    //
}

#endif

void SkFlowAudioPcmPublisher::onChannelReady()
{
    SkArgsMap props;
    params.toMap(props);
    props["stream"] = "PCM";
    props["codec"] = "RAW";

    SkAudioFmtType audio_t = params.getFormat();

    if (audio_t == AFMT_DOUBLE || audio_t == AFMT_FLOAT)
    {
        props["min"] = static_cast<float>(-1.0);
        props["max"] = static_cast<float>(1.0);
    }

    else if (audio_t == AFMT_INT)
    {
        props["min"] = static_cast<int32_t>(-2147483648);
        props["max"] = static_cast<int32_t>(2147483647);
    }

    else if (audio_t == AFMT_SHORT)
    {
        props["min"] = static_cast<int16_t>(-32768);
        props["max"] = static_cast<int16_t>(32767);
    }

    else if (audio_t == AFMT_CHAR)
    {
        props["min"] = static_cast<int8_t>(-128);
        props["max"] = static_cast<int8_t>(127);
    }

    async->setChannelProperties(channelID, props);

#if defined(ENABLE_HTTP)
    if (http)
        setWaveBinHeader(params, mp);
#endif
}


#endif
