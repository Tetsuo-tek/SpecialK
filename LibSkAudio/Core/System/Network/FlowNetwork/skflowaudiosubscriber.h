#ifndef SKFLOWAUDIOSUBSCRIBER_H
#define SKFLOWAUDIOSUBSCRIBER_H

#if defined(ENABLE_AUDIO)

#include "Core/System/Network/FlowNetwork/skabstractflowsubscriber.h"
#include <Multimedia/Audio/skaudiobuffer.h>

#include <Multimedia/Magma/skmagmareader.h>
#include <Multimedia/Audio/Encoders/Magma/skpsyadec.h>

class SkFlowAudioSubscriber extends SkAbstractFlowSubscriber
{
    SkFlowSubscriberOptChans vu;
    SkFlowSubscriberOptChans vuMono;
    SkFlowSubscriberOptChans clipping;
    SkFlowSubscriberOptChans fft;

    SkAudioParameters p;
    SkAudioBuffer a;

    SkMagmaReader *mgm;
    SkPsyADec *aDec;

    public:
        Constructor(SkFlowAudioSubscriber, SkAbstractFlowSubscriber);

        //useful when an audioBuffer is NOT reachable, if these channels are offered from source
        void enableVuChan(bool enabled);
        void enableVuMonoChan(bool enabled);
        void enableClippingChan(bool enabled);
        void enableFftChan(bool enabled);

        void subscribe(CStr *pcmChanName)                              override;
        void unsubscribe()                                             override;

        void close()                                                   override;

        bool hasVuChannel();
        SkFlowChannel *vuChannel();

        bool hasVuMonoChannel();
        SkFlowChannel *vuMonoChannel();

        bool hasClippingChannel();
        SkFlowChannel *clippingChannel();

        bool hasFftChannel();
        SkFlowChannel *fftChannel();

        SkAudioBuffer &audioBuffer();
        SkAudioParameters &parameters();
        double **internalPlanarBuffer();

    private:
        void reset();

        void onChannelAdded(SkFlowChanID chanID)                       override;
        void onChannelRemoved(SkFlowChanID chanID)                     override;

        void tick(SkFlowChannelData &chData)                           override;
};

#endif

#endif // SKFLOWAUDIOSUBSCRIBER_H
