#include "skflowaudiofftpublisher.h"
#include "Core/App/skeventloop.h"

#if defined(ENABLE_AUDIO) && defined(ENABLE_FFTW3LIB)

#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"
#endif

ConstructorImpl(SkFlowAudioFftPublisher, SkFlowGenericPublisher)
{
    planarBuffer = nullptr;

    fftSize = 0;
    freqInterval = 0.f;
    fftData = nullptr;
    fftGaussianEnabled = false;
}

bool SkFlowAudioFftPublisher::setup(SkAudioParameters &audioParameters, double **planarDoubleBuffer, bool enableGaussianFilter, CStr *chNamePfx)
{
    params = audioParameters;
    planarBuffer = planarDoubleBuffer;
    fftGaussianEnabled = enableGaussianFilter;

    SkString chName;

    if (!SkString::isEmpty(chNamePfx))
    {
        chName = chNamePfx;
        chName.append(".");
    }

    chName.append("FFT");

    return SkFlowGenericPublisher::setup(chName.c_str(), FT_AUDIO_FFT, T_FLOAT, nullptr, nullptr);
}

void SkFlowAudioFftPublisher::onStart()
{
    for(uint z=0; z<params.getChannels(); z++)
    {
        SkDirectFFT *fft = new SkDirectFFT;
        fft->setObjectName(this, "DirectFft");
        fft->setup(params.getBufferFrames(), params.getSampleRate(), fftGaussianEnabled);
        ffts << fft;
    }

    fftSize = (params.getBufferFrames()/2) + 1;
    freqInterval = (params.getSampleRate()/2.f + 1)/fftSize;

    fftData = new float [fftSize*params.getChannels()];

    ulong tickIntervalUS = static_cast<ulong>(params.getTickTimePeriod() * 1000000.);

    if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
    {
        ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
        eventLoop()->changeFastZone(tickIntervalUS);
        eventLoop()->changeSlowZone(tickIntervalUS*4);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    }
}

void SkFlowAudioFftPublisher::onStop()
{
    for(uint64_t z=0; z<ffts.count(); z++)
        delete ffts[z];

    delete [] fftData;
    fftData = nullptr;

    ffts.clear();
}

void SkFlowAudioFftPublisher::tick()
{
    if (!hasTargets())
        return;

    uint sz = fftSize * sizeof(float);
    uint channels = params.getChannels();

    for(uint z=0; z<channels; z++)
    {
        ffts[z]->calculate(planarBuffer[z]);
        memcpy(&fftData[z*fftSize], ffts[z]->getEnergies(), sz);
    }

    sz *= channels;

    if (SkAbstractFlowPublisher::hasTargets(channel))
        async->publish(channelID, fftData, sz);

#if defined(ENABLE_HTTP)
    if (SkAbstractFlowPublisher::hasTargets(mp))
        mp->send(SkArrayCast::toCStr(fftData), sz);
#endif
}

void SkFlowAudioFftPublisher::onChannelReady()
{
    SkArgsMap props;
    params.toMap(props);
    props["fftSize"] = fftSize;
    props["freqInterval"] = freqInterval;
    props["min"] = 0.;
    props["max"] = 25.;

    async->setChannelProperties(channelID, props);
}

#endif
