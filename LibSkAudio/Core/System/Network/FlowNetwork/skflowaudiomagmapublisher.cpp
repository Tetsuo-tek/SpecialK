#include "skflowaudiomagmapublisher.h"

#if defined(ENABLE_AUDIO)

#include "Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h"

ConstructorImpl(SkFlowAudioMagmaPublisher, SkFlowGenericPublisher)
{
    mgm = nullptr;
    aEnc = nullptr;
}

bool SkFlowAudioMagmaPublisher::setup(SkAudioParameters &audioParameters)
{
    params = audioParameters;

    return SkFlowGenericPublisher::setup("MGM", FT_AUDIO_DATA, T_BYTEARRAY, nullptr, nullptr);
}

void SkFlowAudioMagmaPublisher::onStart()
{
    mgm = new SkMagmaWriter(this);
    mgm->setObjectName(this, "Mgm");

    aEnc = new SkPsyAEnc(mgm);
    aEnc->setObjectName(this, "PsyAEnc");
    aEnc->setup(params, 8);
    mgm->addStream(aEnc);

    hdr.clear();

    if (mgm->open(hdr))
    {
        if (!hdr.isEmpty())
            ObjectMessage("MGM-writer HEADER produced: " << hdr.size() << " B");

        else
            ObjectError("MGM header NOT produced!");
    }

    else
        ObjectError("CANNOT open MGM-writer!");

    AssertKiller(hdr.isEmpty());
}

void SkFlowAudioMagmaPublisher::onStop()
{
    if (mgm->isOpen())
        mgm->close();

    mgm->destroyLater();
    mgm = nullptr;
    aEnc = nullptr;
}

void SkFlowAudioMagmaPublisher::tick(float *data)
{
    if (!hasTargets())
        return;

    SkDataBuffer output;

    aEnc->setData(data);
    mgm->tick(output);

    if (!output.isEmpty())
    {
        if (SkAbstractFlowPublisher::hasTargets(channel))
            async->publish(channelID, output.toVoid(), output.size());

#if defined(ENABLE_HTTP)
        if (SkAbstractFlowPublisher::hasTargets(mp))
            mp->send(output.data(), output.size());
#endif
    }
}

void SkFlowAudioMagmaPublisher::onChannelReady()
{
    SkArgsMap props;
    params.toMap(props);

    props["stream"] = "MAGMA";
    props["codec"] = "PSYA";
    props["min"] = -1.0f;
    props["max"] = 1.0f;

    async->setChannelProperties(channelID, props);
    async->setChannelHeader(channelID, hdr);
}

#endif
