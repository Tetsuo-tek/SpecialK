#ifndef SKFLOWAUDIOPUBLISHER_H
#define SKFLOWAUDIOPUBLISHER_H

#if defined(ENABLE_AUDIO)

#include "skflowaudiopcmpublisher.h"

#if defined(ENABLE_FFTW3LIB)
    #include "skflowaudiofftpublisher.h"
#endif

#include "skflowaudiomagmapublisher.h"

#if defined(ENABLE_AUDIOENCLIBS)
    #include "skflowaudiovorbispublisher.h"
#endif

#if defined(ENABLE_PORTAUDIOLIB)
    #include <Multimedia/Audio/PA/skportaudio.h>
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFlowAudioPublisher extends SkAbstractFlowPublisher
{
    SkAudioBuffer a;
    SkAudioParameters params;

    SkRingBuffer *input;
    double *buffer;

    SkAudioOutput *floatOutput;
    SkAudioOutput *intOutput;
    SkAudioOutput *shortOutput;
    SkAudioOutput *charOutput;

    SkFlowAudioPcmPublisher *doublePublisher;
    SkFlowAudioPcmPublisher *floatPublisher;
    SkFlowAudioPcmPublisher *intPublisher;
    SkFlowAudioPcmPublisher *shortPublisher;
    SkFlowAudioPcmPublisher *charPublisher;

    SkFlowGenericPublisher *vuPublisher;
    SkFlowGenericPublisher *vuMonoPublisher;
    SkFlowGenericPublisher *clippingPublisher;

#if defined(ENABLE_FFTW3LIB)
    SkFlowAudioFftPublisher *fftPublisher;
    bool fftGaussianEnabled;
#endif

    SkFlowAudioMagmaPublisher *mgmPublisher;

#if defined(ENABLE_AUDIOENCLIBS)
    SkflowAudioVorbisPublisher *vorbisPublisher;
#endif

#if defined(ENABLE_PORTAUDIOLIB)
    SkString device;
    SkPortAudio *pa;
#endif

    public:
        Constructor(SkFlowAudioPublisher, SkAbstractFlowPublisher);

        bool setup(SkAudioParameters &audioParameters);

        void setInputBuffer(SkRingBuffer *inputProduction);//when internal-PA is NOT used

#if defined(ENABLE_FFTW3LIB)
        void enableFftGaussianFilter(bool enabled);
#endif

#if defined(ENABLE_PORTAUDIOLIB)
        SkStringList getDevicesNames();
        void enableCapture(CStr *deviceName="default");//empty deviceName disables recording
#endif

        bool start()                                                    override;
        void stop()                                                     override;

        void setMute(bool muted);
        void setGain(float value);
        void setBass(float value);
        void setMiddle(float value);
        void setTreble(float value);

        void tick();

        bool hasTargets()                                               override;

        SkAudioParameters &parameters();
        SkAudioBuffer &audioBuffer();

        SkFlowAudioPcmPublisher *getDoublePublisher();
        SkFlowAudioPcmPublisher *getFloatPublisher();
        SkFlowAudioPcmPublisher *getIntPublisher();
        SkFlowAudioPcmPublisher *getShortPublisher();
        SkFlowAudioPcmPublisher *getCharPublisher();
        SkFlowGenericPublisher *getVuPublisher();
        SkFlowGenericPublisher *getVuMonoPublisher();
        SkFlowGenericPublisher *getClippingPublisher();

#if defined(ENABLE_FFTW3LIB)
        SkFlowAudioFftPublisher *getFftPublisher();
#endif

        SkFlowAudioMagmaPublisher *getMagmaPublisher();

#if defined(ENABLE_AUDIOENCLIBS)
        SkflowAudioVorbisPublisher *getOggVorbisPublisher();
#endif


    private:
        void onChannelAdded(SkFlowChanID chanID)                        override;
        void onChannelRemoved(SkFlowChanID chanID)                      override;

#if defined(ENABLE_PORTAUDIOLIB)
        bool startCapture();
        void stopCapture();
#endif
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWAUDIOPUBLISHER_H
