#ifndef SKFLOWAUDIOMAGMAPUBLISHER_H
#define SKFLOWAUDIOMAGMAPUBLISHER_H

#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"

#if defined(ENABLE_AUDIO)

#include "Multimedia/Magma/skmagmawriter.h"
#include "Multimedia/Audio/Encoders/Magma/skpsyaenc.h"

class SkFlowAudioMagmaPublisher extends SkFlowGenericPublisher
{
    SkAudioParameters params;
    SkMagmaWriter *mgm;
    SkPsyAEnc *aEnc;
    SkDataBuffer hdr;

    public:
        Constructor(SkFlowAudioMagmaPublisher, SkFlowGenericPublisher);

        bool setup(SkAudioParameters &audioParameters);

        void tick(float *data);

    private:
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onChannelReady()                                           override;
};

#endif

#endif // SKFLOWAUDIOMAGMAPUBLISHER_H
