#if defined(ENABLE_AUDIO)

#include "skaudiosynth.h"
#include "Core/System/Thread/skmutexlocker.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkSynthFunctions)
{
    SetupEnumWrapper(SkSynthFunctions);

    SetEnumItem(SkSynthFunctions::SYNTH_SINE);
    SetEnumItem(SkSynthFunctions::SYNTH_COSINE);
    SetEnumItem(SkSynthFunctions::SYNTH_TANGENT);
    SetEnumItem(SkSynthFunctions::SYNTH_LOGARITHMIC);
    SetEnumItem(SkSynthFunctions::SYNTH_TRIANGULAR);
    SetEnumItem(SkSynthFunctions::SYNTH_SAWTOOTH);
    SetEnumItem(SkSynthFunctions::SYNTH_SQUARE);
    SetEnumItem(SkSynthFunctions::SYNTH_PULSE);
    SetEnumItem(SkSynthFunctions::SYNTH_NOISE);
}

DeclareWrapper(SkAudioSynth);

//DeclareMeth_STATIC_RET(SkAudioSynth, fillNotesFrequencyMap, *Arg_Custom(SkMap<SkString, float>))
DeclareMeth_INSTANCE_VOID(SkAudioSynth, setup, *Arg_Custom(SkAudioParameters), *Arg_Custom(SkSynthProperties))
//DeclareMeth_INSTANCE_RET(SkAudioSynth,getAudioParameters, const SkAudioParameters&)
//DeclareMeth_INSTANCE_RET(SkAudioSynth,getSignalProperties, const SkSynthProperties&)
DeclareMeth_INSTANCE_RET(SkAudioSynth, getProduction, SkRingBuffer*)
DeclareMeth_INSTANCE_VOID(SkAudioSynth, tick)

SetupClassWrapper(SkAudioSynth)
{
    SetClassSuper(SkAudioSynth, SkFlatObject);
    AddEnumType(SkSynthFunctions);

    AddMeth_INSTANCE_VOID(SkAudioSynth, setup);
    AddMeth_INSTANCE_RET(SkAudioSynth, getProduction);
    AddMeth_INSTANCE_VOID(SkAudioSynth, tick);
}

// // // // // // // // // // // // // // // // // // // // //

SkAudioSynth::SkAudioSynth()
{
    CreateClassWrapper(SkAudioSynth);

    signalLoopBuffer = nullptr;
    periodSamples = 0;
    bufferCursor = 0;
}

void SkAudioSynth::fillNotesFrequencyMap(SkMap<SkString, float> &notes)
{
    notes.add("C0",    16.351f);
    notes.add("C#0",   17.324f);
    notes.add("D0",    18.354f);
    notes.add("D#0",   19.445f);
    notes.add("E0",    20.601f);
    notes.add("F0",    21.827f);
    notes.add("F#0",   23.124f);
    notes.add("G0",    24.499f);
    notes.add("G#0",   25.956f);
    notes.add("A0",    27.5f);
    notes.add("A#0",   29.135f);
    notes.add("B0",    30.868f);

    notes.add("C1",    32.703f);
    notes.add("C#1",   34.648f);
    notes.add("D1",    36.708f);
    notes.add("D#1",   38.891f);
    notes.add("E1",    41.203f);
    notes.add("F1",    43.654f);
    notes.add("F#1",   46.249f);
    notes.add("G1",    48.999f);
    notes.add("G#1",   51.913f);
    notes.add("A1",    55.f);
    notes.add("A#1",   58.27f);
    notes.add("B1",    61.735f);

    notes.add("C2",    65.406f);
    notes.add("C#2",   69.296f);
    notes.add("D2",    73.416f);
    notes.add("D#2",   77.782f);
    notes.add("E2",    82.407f);
    notes.add("F2",    87.307f);
    notes.add("F#2",   92.499f);
    notes.add("G2",    97.999f);
    notes.add("G#2",   103.826f);
    notes.add("A2",    110.f);
    notes.add("A#2",   116.541f);
    notes.add("B2",    123.471f);

    notes.add("C3",    130.813f);
    notes.add("C#3",   138.591f);
    notes.add("D3",    146.832f);
    notes.add("D#3",   155.563f);
    notes.add("E3",    164.814f);
    notes.add("F3",    174.614f);
    notes.add("F#3",   184.997f);
    notes.add("G3",    195.998f);
    notes.add("G#3",   207.652f);
    notes.add("A3",    220.f);
    notes.add("A#3",   233.082f);
    notes.add("B3",    246.942f);

    notes.add("C4",    261.626f);
    notes.add("C#4",   277.183f);
    notes.add("D4",    293.665f);
    notes.add("D#4",   311.127f);
    notes.add("E4",    329.628f);
    notes.add("F4",    349.228f);
    notes.add("F#4",   369.994f);
    notes.add("G4",    391.995f);
    notes.add("G#4",   415.305f);
    notes.add("A4",    440.f);
    notes.add("A#4",   466.164f);
    notes.add("B4",    493.883f);

    notes.add("C5",    523.251f);
    notes.add("C#5",   554.365f);
    notes.add("D5",    587.33f);
    notes.add("D#5",   622.254f);
    notes.add("E5",    659.255f);
    notes.add("F5",    698.456f);
    notes.add("F#5",   739.989f);
    notes.add("G5",    783.991f);
    notes.add("G#5",   830.609f);
    notes.add("A5",    880.f);
    notes.add("A#5",   932.328f);
    notes.add("B5",    987.767f);

    notes.add("C6",    1046.502f);
    notes.add("C#6",   1108.731f);
    notes.add("D6",    1174.659f);
    notes.add("D#6",   1244.508f);
    notes.add("E6",    1318.51f);
    notes.add("F6",    1396.913f);
    notes.add("F#6",   1479.978f);
    notes.add("G6",    1567.982f);
    notes.add("G#6",   1661.219f);
    notes.add("A6",    1760.f);
    notes.add("A#6",   1864.655f);
    notes.add("B6",    1975.533f);

    notes.add("C7",    2093.005f);
    notes.add("C#7",   2217.461f);
    notes.add("D7",    2349.318f);
    notes.add("D#7",   2489.016f);
    notes.add("E7",    2637.021f);
    notes.add("F7",    2793.826f);
    notes.add("F#7",   2959.955f);
    notes.add("G7",    3135.964f);
    notes.add("G#7",   3322.438f);
    notes.add("A7",    3520.f);
    notes.add("A#7",   3729.31f);
    notes.add("B7",    3951.066f);

    notes.add("C8",    4186.009f);
    notes.add("C#8",   4434.922f);
    notes.add("D8",    4698.636f);
    notes.add("D#8",   4978.032f);
    notes.add("E8",    5274.042f);
    notes.add("F8",    5587.652f);
    notes.add("F#8",   5919.91f);
    notes.add("G8",    6271.928f);
    notes.add("G#8",   6644.876f);
    notes.add("A8",    7040.f);
    notes.add("A#8",   7458.62f);
    notes.add("B8",    7902.132f);

    notes.add("C9",    8372.018f);
    notes.add("C#9",   8869.844f);
    notes.add("D9",    9397.272f);
    notes.add("D#9",   9956.064f);
    notes.add("E9",    10548.084f);
    notes.add("F9",    11175.304f);
    notes.add("F#9",   11839.82f);
    notes.add("G9",    12543.856f);
    notes.add("G#9",   13289.752f);
    notes.add("A9",    14080.f);
    notes.add("A#9",   14917.24f);
    notes.add("B9",    15804.264f);
}

void SkAudioSynth::setup(SkAudioParameters &audioParameters, SkSynthProperties &properties)
{
    SkMutexLocker locker(&bufferEx);
    params.set(true, 1, audioParameters.getSampleRate(), audioParameters.getBufferFrames(), SkAudioFmtType::AFMT_FLOAT);
    props = properties;
    calculate();
}

const SkAudioParameters &SkAudioSynth::getAudioParameters()
{
    return params;
}


const SkSynthProperties &SkAudioSynth::getSignalProperties()
{
    return props;
}

SkRingBuffer *SkAudioSynth::getProduction()
{
    return &production;
}

void SkAudioSynth::calculate()
{
    uint sampleRate = params.getSampleRate();

    periodSamples = (uint) ((float) sampleRate / props.frequency);

    uint halfPeriodSample = periodSamples / 2;
    float incrementAmmount = -2.f / (halfPeriodSample + 1);//triangular
    float nextSample = -1;

    /*if (props.type == "NOISE")
        qsrand((uint)QTime::currentTime().msec());*/

    float t = 0;

    for (uint i=0; i<periodSamples; i++)
    {
        t = (float) i / (float) sampleRate;

        if (props.type == SkSynthFunctions::SYNTH_SINE)
            signalLoopBuffer[i] = sinf(2.f * SK_PI * props.frequency * t);

        else if (props.type == SkSynthFunctions::SYNTH_COSINE)
            signalLoopBuffer[i] = cosf(2.f * SK_PI * props.frequency * t);

        else if (props.type == SkSynthFunctions::SYNTH_TANGENT)
            signalLoopBuffer[i] = tanf(2.f * SK_PI * props.frequency * t);

        else if (props.type == SkSynthFunctions::SYNTH_LOGARITHMIC)
            signalLoopBuffer[i] = logf(2.f * SK_PI * props.frequency * t);

        else if (props.type == SkSynthFunctions::SYNTH_TRIANGULAR)
        {
            signalLoopBuffer[i] = nextSample;

            if ((i % halfPeriodSample) == 0)
                incrementAmmount *= -1;

            nextSample += incrementAmmount;
        }

        else if (props.type == SkSynthFunctions::SYNTH_SAWTOOTH)
            signalLoopBuffer[i] = ((float) (2 * (i % periodSamples)) / ((float) periodSamples)) - 1;

        else if (props.type == SkSynthFunctions::SYNTH_SQUARE)
        {
            if ((i % halfPeriodSample) == 0)
                nextSample *= -1;

            signalLoopBuffer[i] = nextSample;
        }

        else if (props.type == SkSynthFunctions::SYNTH_PULSE)
        {
            if ((i % periodSamples) < (periodSamples*props.pulseLength))
                signalLoopBuffer[i] = 1;
            else
                signalLoopBuffer[i] = 0;
        }

        /*else if (props.type == SkSynthFunctions::SYNTH_NOISE)
            signalLoopBuffer[i] =  ((((float) (qrand() % RAND_MAX)) / RAND_MAX) * 2) - 1;*/

        signalLoopBuffer[i] *= props.amplitudeFactor;
    }
}

void SkAudioSynth::tick()
{
    uint cursorIncrement = bufferCursor + params.getBufferFrames();

    if (cursorIncrement < periodSamples)
    {
        production.addData((CStr *) &signalLoopBuffer[bufferCursor], params.getCanonicalSize());
        bufferCursor = cursorIncrement;
    }

    else
    {
        uint partSz = cursorIncrement - periodSamples;
        production.addData((CStr *) &signalLoopBuffer[bufferCursor], partSz);

        partSz = params.getCanonicalSize() - partSz;
        production.addData((CStr *) &signalLoopBuffer[0], partSz);

        bufferCursor = partSz;
    }
}

#endif
