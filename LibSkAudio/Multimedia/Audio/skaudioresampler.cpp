#if defined(ENABLE_AUDIO)

#include "skaudioresampler.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkAudioResampler);

DeclareMeth_INSTANCE_RET(SkAudioResampler, getOutputBufferFrames, unsigned int)
DeclareMeth_INSTANCE_VOID(SkAudioResampler, tick)


SetupClassWrapper(SkAudioResampler)
{
    SetClassSuper(SkAudioResampler, SkAudioQuantizer);

    AddMeth_INSTANCE_RET(SkAudioResampler, getOutputBufferFrames);
    AddMeth_INSTANCE_VOID(SkAudioResampler, tick);
}

// // // // // // // // // // // // // // // // // // // // //

SkAudioResampler::SkAudioResampler()
{
    CreateClassWrapper(SkAudioResampler);

    resamplingPlanarBuffer = nullptr;

    timePerInputSample = 0;
    timePerOutputSample = 0;
}

SkAudioResampler::~SkAudioResampler()
{
    if (planarDoubleBuffer)
    {
        //delete[] interleavedDoubleBuffer;
        delete[] planarDoubleBuffer;
    }
}

void SkAudioResampler::setup(SkAudioParameters &tempInputAudioParameters,
                            double **inputPlanarBuffer,
                            SkAudioParameters *tempOutputAudioParameters)
{
    SkAudioProps inputProps = tempInputAudioParameters.getProperties();
    SkAudioProps outputProps = tempOutputAudioParameters->getProperties();

    timePerInputSample = 1.0/inputProps.sampleRate;
    resamplingInputParameters = tempInputAudioParameters;
    audioParameters = tempOutputAudioParameters;

    resamplingPlanarBuffer = inputPlanarBuffer;

    //memcpy(&audioParameters, tempOutputAudioParameters, sizeof(SkAudioParameters));
    timePerOutputSample = 1.0/outputProps.sampleRate;

    //calculate resampled bufferFrames for output (buffer for this quantizer)
    outputProps.bufferFrames = ceil(((double) outputProps.sampleRate)
                                        / (((double) inputProps.sampleRate)
                                           / ((double) inputProps.bufferFrames)));

    inputProps.bufferFrames = outputProps.bufferFrames;
    tempInputAudioParameters.set(inputProps);

    totalBufferSamples = outputProps.bufferFrames * outputProps.nChannels;

    if (planarDoubleBuffer)
    {
        //delete[] interleavedDoubleBuffer;
        delete[] planarDoubleBuffer;
    }

    /*interleavedDoubleBuffer = new double[totalBufferSamples];
    memset(interleavedDoubleBuffer, 0, totalBufferSamples*8);*/

    planarDoubleBuffer = new double *[outputProps.nChannels];

    for(unsigned int z=0; z<outputProps.nChannels; z++)
    {
        planarDoubleBuffer[z] = new double [outputProps.bufferFrames];
        memset(planarDoubleBuffer[z], 0, outputProps.bufferFrames*sizeof(double));
    }
}

unsigned int SkAudioResampler::getOutputBufferFrames()
{
    return audioParameters.getBufferFrames();
}

void SkAudioResampler::tick()
{
    /*int interleavedCounter = 0;

    if (resamplingInputParameters.sampleRate > audioParameters.sampleRate)
    {
        for(unsigned int z=0; z<resamplingInputParameters.nChannels; z++)
        {
            int inputCounter = 0;

            for(int outputCounter=0; outputCounter < (int) audioParameters.bufferFrames; outputCounter++)
            {
                bool cyclingInput = true;

                while(cyclingInput)
                {
                    if (inputCounter*timePerInputSample > (outputCounter-1)*timePerOutputSample)
                    {
                        planarDoubleBuffer[z][outputCounter] = resamplingPlanarBuffer[z][inputCounter];
                        cyclingInput = false;
                    }

                    inputCounter++;
                }

                interleavedCounter = (resamplingInputParameters.nChannels * outputCounter) + z;
                interleavedDoubleBuffer[interleavedCounter] = planarDoubleBuffer[z][outputCounter];
            }
        }
    }*/

    /*if (resamplingInputParameters.sampleRate > audioParameters.sampleRate)
    {
        for(unsigned int z=0; z<resamplingInputParameters.nChannels; z++)
        {
            int inputCounter = 1;
            //int maxInputCounter = (int) resamplingInputParameters.bufferFrames -1;

            for(int outputCounter=0; outputCounter < (int) audioParameters.bufferFrames; outputCounter++)
            {
                bool cyclingInput = true;

                while(cyclingInput)
                {
                    if ((inputCounter+1)*timePerInputSample >= (outputCounter+1)*timePerOutputSample)
                    {
                        planarDoubleBuffer[z][outputCounter] = resamplingPlanarBuffer[z][inputCounter-1];
                        cyclingInput = false;
                    }
                    //else if (inputCounter >= maxInputCounter)
                    //{
                        //planarDoubleBuffer[z][outputCounter] = resamplingPlanarBuffer[z][maxInputCounter];
                        //cyclingInput = false;
                    //}
                    inputCounter++;
                }

                interleavedCounter = (resamplingInputParameters.nChannels * outputCounter) + z;
                interleavedDoubleBuffer[interleavedCounter] = planarDoubleBuffer[z][outputCounter];
            }
        }
    }

    else
    {
        for(unsigned int z=0; z<resamplingInputParameters.nChannels; z++)
        {
            int outputCounter = 0;
            int inputCounter=0;

            for(; inputCounter < (int) resamplingInputParameters.bufferFrames; inputCounter++)
            {
                bool cyclingOutput = true;

                while(cyclingOutput)
                {
                    if (inputCounter < ((int) resamplingInputParameters.bufferFrames)-1)
                    {
                        if ((inputCounter+1)*timePerInputSample <= (outputCounter+1)*timePerOutputSample)
                            cyclingOutput = false;
                    }

                    else if (outputCounter == (int) audioParameters.bufferFrames-1)
                        cyclingOutput = false;

                    planarDoubleBuffer[z][outputCounter] = resamplingPlanarBuffer[z][inputCounter];

                    interleavedCounter = (resamplingInputParameters.nChannels * outputCounter) + z;
                    interleavedDoubleBuffer[interleavedCounter] = planarDoubleBuffer[z][outputCounter];
                    outputCounter++;
                }
            }
        }
    }*/
}

#endif
