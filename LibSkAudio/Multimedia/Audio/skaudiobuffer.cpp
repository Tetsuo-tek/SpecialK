#if defined(ENABLE_AUDIO)

#include "skaudiobuffer.h"
#include "Core/System/Thread/skmutexlocker.h"
#include "Core/sklogmachine.h"

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STRUCT_TOMAP(SkAudioEqState)
{
    SetupStructWrapper(SkAudioEqState);

    StructToMapProperty(initialized);
    StructToMapProperty(vsa);

    StructToMapProperty(lf);
    StructToMapProperty(f1p0);
    StructToMapProperty(f1p1);
    StructToMapProperty(f1p2);
    StructToMapProperty(f1p3);

    StructToMapProperty(hf);
    StructToMapProperty(f2p0);
    StructToMapProperty(f2p1);
    StructToMapProperty(f2p2);
    StructToMapProperty(f2p3);

    StructToMapProperty(sdm1);
    StructToMapProperty(sdm2);
    StructToMapProperty(sdm3);

    StructToMapProperty(lg);
    StructToMapProperty(mg);
    StructToMapProperty(hg);
}

DeclareWrapper_STRUCT_FROMMAP(SkAudioEqState)
{
    MapToStructProperty(initialized).toBool();
    MapToStructProperty(vsa).toDouble();

    MapToStructProperty(lf).toDouble();
    MapToStructProperty(f1p0).toDouble();
    MapToStructProperty(f1p1).toDouble();
    MapToStructProperty(f1p2).toDouble();
    MapToStructProperty(f1p3).toDouble();

    MapToStructProperty(hf).toDouble();
    MapToStructProperty(f2p0).toDouble();
    MapToStructProperty(f2p1).toDouble();
    MapToStructProperty(f2p2).toDouble();
    MapToStructProperty(f2p3).toDouble();

    MapToStructProperty(sdm1).toDouble();
    MapToStructProperty(sdm2).toDouble();
    MapToStructProperty(sdm3).toDouble();

    MapToStructProperty(lg).toDouble();
    MapToStructProperty(mg).toDouble();
    MapToStructProperty(hg).toDouble();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_STRUCT_TOMAP(SkAudioCompressor)
{
    SetupStructWrapper(SkAudioCompressor);

    StructToMapProperty(expanderEnabled);
    StructToMapProperty(lowerThreshold);
    StructToMapProperty(lowerRatio);
    StructToMapProperty(expanderResizeCostant);

    StructToMapProperty(limiterEnabled);
    StructToMapProperty(upperThreshold);
    StructToMapProperty(upperRatio);
    StructToMapProperty(limiterThresholdZone);
    StructToMapProperty(limiterSurplusZone);
    StructToMapProperty(limiterClipperValue);
    StructToMapProperty(limiterResizeCostant);
}

DeclareWrapper_STRUCT_FROMMAP(SkAudioCompressor)
{
    MapToStructProperty(expanderEnabled).toBool();
    MapToStructProperty(lowerThreshold).toDouble();
    MapToStructProperty(lowerRatio).toUInt8();
    MapToStructProperty(expanderResizeCostant).toDouble();

    MapToStructProperty(limiterEnabled).toBool();
    MapToStructProperty(upperThreshold).toDouble();
    MapToStructProperty(upperRatio).toUInt8();
    MapToStructProperty(limiterThresholdZone).toDouble();
    MapToStructProperty(limiterSurplusZone).toDouble();
    MapToStructProperty(limiterClipperValue).toDouble();
    MapToStructProperty(limiterResizeCostant).toDouble();
}

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper(SkAudioBuffer);

DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setAudioParameters, *Arg_Custom(SkAudioParameters), Arg_Custom(SkRingBuffer))
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getAudioParameters, SkAudioParameters)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setMute, Arg_Bool)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setGain, Arg_Double)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setChanVolume, Arg_UInt, Arg_Double)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setBass, Arg_Double)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setMiddle, Arg_Double)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setTreble, Arg_Double)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setEqState, *Arg_Custom(SkAudioEqState))
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getEqState, const SkAudioEqState*)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setExpanderCompressor, Arg_Bool, Arg_Double, Arg_UInt8)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setLimiterCompressor, Arg_Bool, Arg_Double, Arg_UInt8)

DeclareMeth_INSTANCE_RET(SkAudioBuffer, getCompressor, const SkAudioCompressor*)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, setSilence, Arg_UInt64, Arg_Float)
//DeclareMeth_INSTANCE_VOID(SkAudioBuffer, mixBuffer, Arg_CStr)

DeclareMeth_INSTANCE_RET(SkAudioBuffer, addOutput, bool,
                   Arg_CStr, *Arg_Custom(SkAudioParameters),
                   Arg_Custom(SkRingBuffer)/*, Arg_Custom(SkAudioBuffer)*/, Arg_Bool)

DeclareMeth_INSTANCE_RET(SkAudioBuffer, getOutputsCount, uint64_t)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getOutputNames, SkStringList&)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getOutputByName, SkAudioOutput*, Arg_CStr)
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAudioBuffer, getOutputByName, 1, SkAudioOutput*, Arg_StringRef)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getOutputByID, SkAudioOutput*, Arg_UInt64)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, outputExists, bool, Arg_CStr)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, delOutput, bool, Arg_CStr)
DeclareMeth_INSTANCE_VOID(SkAudioBuffer, deleteAllOutputs)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, tick, bool)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, isClipping, bool, Arg_UInt)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getLastMonoVolumeUnits, float)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getLastChanVolumeUnits, float, Arg_UInt)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getLastChanMinValue, double, Arg_UInt)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getLastChanMaxValue, double, Arg_UInt)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getTickTimePeriod, float)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getInputCanonicalSize, uint64_t)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getInputOneSecondSize, uint64_t)
DeclareMeth_INSTANCE_RET(SkAudioBuffer, getInputProduction, SkRingBuffer*)

SetupClassWrapper(SkAudioBuffer)
{
    SetClassSuper(SkAudioBuffer, SkFlatObject);

    AddStructType(SkAudioEqState);
    AddStructType(SkAudioCompressor);

    AddMeth_INSTANCE_VOID(SkAudioBuffer, setAudioParameters);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getAudioParameters);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setMute);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setGain);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setChanVolume);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setBass);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setMiddle);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setTreble);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setEqState);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getEqState);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setExpanderCompressor);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setLimiterCompressor);

    AddMeth_INSTANCE_RET(SkAudioBuffer, getCompressor);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, setSilence);
    //AddMeth_INSTANCE_VOID(SkAudioBuffer, mixBuffer);

    AddMeth_INSTANCE_RET(SkAudioBuffer, addOutput);

    AddMeth_INSTANCE_RET(SkAudioBuffer, getOutputsCount);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getOutputNames);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getOutputByName);
    AddMeth_INSTANCE_RET_OVERLOAD(SkAudioBuffer, getOutputByName, 1);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getOutputByID);
    AddMeth_INSTANCE_RET(SkAudioBuffer, outputExists);
    AddMeth_INSTANCE_RET(SkAudioBuffer, delOutput);
    AddMeth_INSTANCE_VOID(SkAudioBuffer, deleteAllOutputs);
    AddMeth_INSTANCE_RET(SkAudioBuffer, tick);
    AddMeth_INSTANCE_RET(SkAudioBuffer, isClipping);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getLastMonoVolumeUnits);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getLastChanVolumeUnits);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getLastChanMinValue);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getLastChanMaxValue);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getTickTimePeriod);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getInputCanonicalSize);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getInputOneSecondSize);
    AddMeth_INSTANCE_RET(SkAudioBuffer, getInputProduction);
}

// // // // // // // // // // // // // // // // // // // // //

SkAudioBuffer::SkAudioBuffer()
{
    CreateClassWrapper(SkAudioBuffer);

    lastMinChansValuesList = nullptr;
    lastMaxChansValuesList = nullptr;
    lastVolumeUnitsChansValuesList = nullptr;
    lastMonoVolumeUnits = SILENCE_VU_UNIT;
    overAmplitude = nullptr;

    compressor.setExpander(false, 0.3, 2);
    compressor.setLimiter(false, 0.8, 4);

    index = 0;
    time = 0;

    tickTimePeriod = 0;

    inputCanonicalSize = 0;
    inputDataProduction = nullptr;
}

SkAudioBuffer::~SkAudioBuffer()
{
    destroyAll();
    deleteAllOutputs();
}

void SkAudioBuffer::destroyAll()
{
    if (planarDoubleBuffer)
    {
        for(uint z=0; z<audioParameters.getChannels(); z++)
            delete [] planarDoubleBuffer[z];

        delete [] planarDoubleBuffer;
        delete [] processingProperties.channelsVolume;
        delete [] lastMinChansValuesList;
        delete [] lastMaxChansValuesList;
        delete [] lastVolumeUnitsChansValuesList;
        delete [] overAmplitude;

        planarDoubleBuffer = nullptr;
        processingProperties.channelsVolume = nullptr;
        lastMinChansValuesList = nullptr;
        lastMaxChansValuesList = nullptr;
        lastVolumeUnitsChansValuesList = nullptr;
        overAmplitude = nullptr;
        inputDataProduction = nullptr;
    }
}

void SkAudioBuffer::initEqState(uint lowfreq, uint highfreq, uint rate)
{
    // Recommended frequencies are ...
    //
    // lowfreq = 880 Hz
    // highfreq = 5000 Hz
    //
    // Set mixfreq to whatever rate your system is using (eg 48Khz)

    // Clear state
    memset(&eqState, 0, sizeof(SkAudioEqState));

    // Very small amount (Denormal Fix)
    eqState.vsa = (1.0 / 4294967295.0);

    // Set Low/Mid/High gains to unity
    eqState.lg = 1.0;
    eqState.mg = 1.0;
    eqState.hg = 1.0;

    // Calculate filter cutoff frequencies
    eqState.lf = 2 * sin(M_PI * ((double) lowfreq / (double) rate));
    eqState.hf = 2 * sin(M_PI * ((double) highfreq / (double) rate));

    eqState.initialized = true;
}

void SkAudioBuffer::setAudioParameters(SkAudioParameters &parameters, SkRingBuffer *inputProductionBuffer)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    destroyAll();

    audioParameters = parameters;
    SkAudioProps props = audioParameters.getProperties();

    totalBufferSamples = props.bufferSamples;

    //880 / 44100 ; 5000 / 44100 -> ratios to obtain all samplerate tone controls
    initEqState(static_cast<uint>(0.019f * 880), static_cast<uint>(0.113f * 5000), props.sampleRate);

    processingProperties.channelsVolume = new double [props.nChannels];

    lastMinChansValuesList = new double [props.nChannels];
    lastMaxChansValuesList = new double [props.nChannels];
    lastVolumeUnitsChansValuesList = new float [props.nChannels];
    overAmplitude = new bool [props.nChannels];

    planarDoubleBuffer = new double *[props.nChannels];

    for(uint z=0; z<props.nChannels; z++)
    {
        processingProperties.channelsVolume[z] = 1;
        planarDoubleBuffer[z] = new double [props.bufferFrames];

        for(uint i=0; i<props.bufferFrames; i++)
            planarDoubleBuffer[z][i] = 0.;
    }

    inputDataProduction = inputProductionBuffer;
    inputCanonicalSize = props.canonicalSize;

    if (inputDataProduction)
        inputDataProduction->setCanonicalSize(inputCanonicalSize);

    tickTimePeriod = props.tickTimePeriod;
    lastMonoVolumeUnits = SILENCE_VU_UNIT;
}

SkAudioProps SkAudioBuffer::getAudioProperties()
{
    return audioParameters.getProperties();
}

SkAudioParameters SkAudioBuffer::getAudioParameters()
{
    return audioParameters;
}

SkAudioProcessing &SkAudioBuffer::getAudioProcessing()
{
    return processingProperties;
}

void SkAudioBuffer::setMute(bool enabled)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    processingProperties.mute = enabled;
}

void SkAudioBuffer::setGain(double value)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    processingProperties.gain = value;
}

void SkAudioBuffer::setChanVolume(uint chanIndex, double value)
{
    if (chanIndex < audioParameters.getChannels())
    {
        SkMutexLocker locker(&planarDoubleBufferEx);
        processingProperties.channelsVolume[chanIndex] = value;
    }
}

void SkAudioBuffer::setBass(double value)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    processingProperties.bass = value;
}

void SkAudioBuffer::setMiddle(double value)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    processingProperties.middle = value;
}

void SkAudioBuffer::setTreble(double value)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    processingProperties.treble = value;
}

void SkAudioBuffer::setEqState(SkAudioEqState &state)
{
    eqState = state;
}

const SkAudioEqState *SkAudioBuffer::getEqState()
{
    return &eqState;
}

void SkAudioBuffer::setExpanderCompressor(bool enabled, double threshold, uint8_t ratio)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    compressor.setExpander(enabled, threshold, ratio);
}

void SkAudioBuffer::setLimiterCompressor(bool enabled, double threshold, uint8_t ratio)
{
    SkMutexLocker locker(&planarDoubleBufferEx);
    compressor.setLimiter(enabled, threshold, ratio);
}

const SkAudioCompressor *SkAudioBuffer::getCompressor()
{
    return &compressor;
}

void SkAudioBuffer::setSilence(uint64_t tempIndex, float tempTime)
{
    index = tempIndex;
    time = tempTime;

    planarDoubleBufferEx.lock();

    uint channels = audioParameters.getChannels();

    for(uint z=0; z<channels; z++)
    {
        lastMinChansValuesList[z] = 0;
        lastMaxChansValuesList[z] = 0;
        overAmplitude[z] = false;
        lastVolumeUnitsChansValuesList[z] = SILENCE_VU_UNIT;
    }

    uint bufferFrames = audioParameters.getBufferFrames();

    for(uint z=0; z<channels; z++)
        for(uint i=0; i<bufferFrames; i++)
            planarDoubleBuffer[z][i] = 0;

    planarDoubleBufferEx.unlock();

    processOutputs();
}

void SkAudioBuffer::setBuffer(char *buffer, uint64_t tempIndex, float tempTime)
{
    index = tempIndex;
    time = tempTime;

    planarDoubleBufferEx.lock();

    uint bufferFrames = audioParameters.getBufferFrames();
    uint channels = audioParameters.getChannels();

    for(uint z=0; z<channels; z++)
    {
        lastMinChansValuesList[z] = 0;
        lastMaxChansValuesList[z] = 0;
        overAmplitude[z] = false;
    }

    uint inputIDX = 0;

    // [+++++-----]
    if (audioParameters.isPlanar())
    {
        for(uint z=0; z<channels; z++)
        {
            inputIDX = (z*bufferFrames);
            for(uint i=0; i<bufferFrames; i++)
            {
                bitConversion(buffer, inputIDX+i, z, i);
                process(z, i);
            }
        }
    }

    // [+-+-+-+-+-]
    else
    {
        for(uint i=0; i<bufferFrames; i++)
        {
            inputIDX = i*channels;
            for(uint z=0; z<channels; z++)
            {
                bitConversion(buffer, inputIDX+z, z, i);
                process(z, i);
            }
        }
    }

    for(uint z=0; z<channels; z++)
    {
        if (fabs(lastMaxChansValuesList[z]) == 0 || fabs(lastMinChansValuesList[z]) == 0)
            lastVolumeUnitsChansValuesList[z] = SILENCE_VU_UNIT;

        else
        {
            float currentAmplitude = static_cast<float>(fabs(lastMaxChansValuesList[z]) + fabs(lastMinChansValuesList[z])) / 2.f;
            lastVolumeUnitsChansValuesList[z] = 10.f * log10f(powf(currentAmplitude, 2));
        }
    }

    planarDoubleBufferEx.unlock();
    processOutputs();
}

void SkAudioBuffer::process(uint chanIDX, uint sampleIDX)
{
    double &val = planarDoubleBuffer[chanIDX][sampleIDX];

    if (processingProperties.mute)
        val = 0;

    else
    {
        if (processingProperties.bass != 1 || processingProperties.middle != 1 || processingProperties.treble != 1)
        {
            eqState.lg = processingProperties.bass;
            eqState.mg = processingProperties.middle;
            eqState.hg = processingProperties.treble;

            equalize(val);
        }

        val = val * processingProperties.gain * processingProperties.channelsVolume[chanIDX];

        compress(val);

        //CLIPPING
        if (!overAmplitude[chanIDX] && (val > 1 || val < -1))
            overAmplitude[chanIDX] = true;
    }

    //MIN - MAX values for each channel
    if (val < lastMinChansValuesList[chanIDX])
        lastMinChansValuesList[chanIDX] = val;

    else if (val > lastMaxChansValuesList[chanIDX])
        lastMaxChansValuesList[chanIDX] = val;
}

void SkAudioBuffer::equalize(double &inputValue)
{
    // Locals
    double l, m, h; // Low / Mid / High - Sample Values

    // Filter #1 (lowpass)
    eqState.f1p0 += (eqState.lf * (inputValue - eqState.f1p0)) + eqState.vsa;
    eqState.f1p1 += (eqState.lf * (eqState.f1p0 - eqState.f1p1));
    eqState.f1p2 += (eqState.lf * (eqState.f1p1 - eqState.f1p2));
    eqState.f1p3 += (eqState.lf * (eqState.f1p2 - eqState.f1p3));

    l = eqState.f1p3;

    // Filter #2 (highpass)
    eqState.f2p0 += (eqState.hf * (inputValue - eqState.f2p0)) + eqState.vsa;
    eqState.f2p1 += (eqState.hf * (eqState.f2p0 - eqState.f2p1));
    eqState.f2p2 += (eqState.hf * (eqState.f2p1 - eqState.f2p2));
    eqState.f2p3 += (eqState.hf * (eqState.f2p2 - eqState.f2p3));

    h = eqState.sdm3 - eqState.f2p3;

    // Calculate midrange (signal - (low + high))
    m = eqState.sdm3 - (h + l);

    // Scale, Combine and store
    l *= eqState.lg;
    m *= eqState.mg;
    h *= eqState.hg;

    // Shuffle history buffer
    eqState.sdm3 = eqState.sdm2;
    eqState.sdm2 = eqState.sdm1;
    eqState.sdm1 = inputValue;

    // Return result
    inputValue = l + m + h;
}

void SkAudioBuffer::compress(double &value)
{
    double tempAbsVal = fabs(value);

    //EXPANDER

    if (compressor.expanderEnabled && tempAbsVal < compressor.lowerThreshold)
    {
        //value *= (log(1-(tempAbsVal/compressor.lowerRatio)) / log(compressor.expanderResizeCostant));
        value *= (tempAbsVal/compressor.expanderResizeCostant);
    }

    //LIMITER

    if (compressor.limiterEnabled && tempAbsVal > compressor.upperThreshold)
    {
        bool isPositive = (value > 0);

        if (fabs(value) > compressor.limiterClipperValue)
            value = 1.0001;

        else
            value = compressor.upperThreshold + (compressor.limiterResizeCostant * (tempAbsVal - compressor.upperThreshold));

        if (!isPositive)
            value *= -1;
    }
}

bool SkAudioBuffer::addOutput(CStr *name,
                              SkAudioParameters &parameters,
                              SkRingBuffer *production,
                              /*SkAudioBuffer *linkedBuffer,*/
                              bool enabled)
{
    outputEx.lock();

    if (!outputsNames.contains(name))
    {
        /*if (linkedBuffer == this)
        {
            FlatError("CANNOT add the same BUFFER for source and output");
            outputEx.unlock();
            return false;
        }*/

        SkAudioOutput *tempOutput = new SkAudioOutput();
        tempOutput->name = name;
        tempOutput->audioParameters = parameters;
        tempOutput->resampler = nullptr;
        tempOutput->production = production;

        if (tempOutput->production)
        {
            production->clear();
            tempOutput->production->setCanonicalSize(tempOutput->audioParameters.getCanonicalSize());
        }

        tempOutput->currentData = new char [tempOutput->audioParameters.getCanonicalSize()];

        /*tempOutput->linkedBuffer = linkedBuffer;

        if (tempOutput->linkedBuffer)
            tempOutput->linkedBuffer->setAudioParameters(tempOutput->audioParameters, tempOutput->production);*/

        /*SkAudioProps props = audioParameters.getProperties();
        SkAudioProps outputProps = tempOutput->audioParameters.getProperties();*/

        if (audioParameters.getSampleRate() != tempOutput->audioParameters.getSampleRate())
        {
            /*tempOutput->resampler = new SkAudioResampler;

            SkAudioParameters tempResamplerInputAudioParameters = tempOutput->audioParameters;
            tempResamplerInputAudioParameters.sampleRate = props.sampleRate;
            tempResamplerInputAudioParameters.bufferFrames = props.bufferFrames;
            tempResamplerInputAudioParameters.bytesPerSample = 8;
            tempResamplerInputAudioParameters.fmt = props.fmt;

            tempOutput->resampler->setup(tempResamplerInputAudioParameters,
                                         planarDoubleBuffer,
                                         &(tempOutput->audioParameters));*/

            //tempOutput->audioParameters.bufferFrames = tempOutput->resampler->getOutputBufferFrames();

            FlatError("Resampler is NOT usable yet");
        }

        /*else
            tempOutput->audioParameters.bufferFrames = audioParameters.bufferFrames;*/

        tempOutput->enabled = enabled;

        outputsNames.append(name);
        outputs.append(tempOutput);

        FlatDebug("Output ADDED: '" << name << "'");

        outputEx.unlock();
        return true;
    }

    outputEx.unlock();
    return false;
}

void SkAudioBuffer::processOutputs()
{
    outputEx.lock();

    for (uint64_t i=0; i<outputs.count(); i++)
    {
        SkAudioOutput *tempOutput = outputs.at(i);

        if (tempOutput->enabled)
        {
            //char *tempOutputData = new char [tempOutput->audioParameters.getCanonicalSize()];

            /*if (tempOutput->resampler)
            {
                tempOutput->resampler->tick();
                tempOutput->resampler->convertSampleTypeTo(&(tempOutput->audioParameters), tempOutputData);
            }

            else*/
                //convertSampleTypeTo(&(tempOutput->audioParameters), tempOutputData);
                convertSampleTypeTo(&(tempOutput->audioParameters), tempOutput->currentData);

            if (tempOutput->production)
                tempOutput->production->addData(tempOutput->currentData, tempOutput->audioParameters.getCanonicalSize());

            //delete [] tempOutputData;

            /*if (tempOutput->linkedBuffer)
                tempOutput->linkedBuffer->tick();*/
        }
    }

    outputEx.unlock();
}

uint64_t SkAudioBuffer::getOutputsCount()
{
    return outputs.count();
}

SkStringList &SkAudioBuffer::getOutputNames()
{
    return outputsNames;
}

SkAudioOutput *SkAudioBuffer::getOutputByName(CStr *name)
{
    SkMutexLocker locker(&outputEx);

    if (!outputsNames.contains(name))
        return nullptr;

    return outputs.at(static_cast<uint64_t>(outputsNames.indexOf(name)));
}

SkAudioOutput *SkAudioBuffer::getOutputByName(SkString &name)
{
    SkMutexLocker locker(&outputEx);

    if (!outputsNames.contains(name))
        return nullptr;

    return outputs.at(static_cast<uint64_t>(outputsNames.indexOf(name)));
}

SkAudioOutput *SkAudioBuffer::getOutputByID(uint64_t i)
{
    SkMutexLocker locker(&outputEx);

    if (i >= outputs.count())
        return nullptr;

    return outputs.at(i);
}

bool SkAudioBuffer::outputExists(CStr *name)
{
    SkMutexLocker locker(&outputEx);
    return outputsNames.contains(name);
}

bool SkAudioBuffer::delOutput(CStr *name)
{
    outputEx.lock();

    if (outputsNames.contains(name))
    {
        SkAudioOutput *output = outputs.at(static_cast<uint64_t>(outputsNames.indexOf(name)));

        if (output->resampler)
            delete output->resampler;

        delete [] output->currentData;

        outputsNames.remove(name);
        outputs.remove(output);

        FlatDebug("Output REMOVED: '" << name << "'");
        delete output;

        outputEx.unlock();
        return true;
    }

    outputEx.unlock();
    return false;
}

void SkAudioBuffer::deleteAllOutputs()
{
    for(; !outputsNames.isEmpty(); )
        delOutput(outputsNames.at(0).c_str());
}

bool SkAudioBuffer::tick(char *buffer)
{
    if (buffer)
        setBuffer(buffer, 0, 0);

    else
    {
        if (!inputDataProduction || !inputDataProduction->isCanonicalSizeReached())
            return false;

        buffer = new char [inputDataProduction->getCanonicalSize()];

        if (inputDataProduction->getCanonicalBuffer(buffer))
            setBuffer(buffer, 0, 0);

        delete [] buffer;
    }

    float dbVal = 0;
    uint channels = audioParameters.getChannels();

    for(uint z=0; z<channels; z++)
        dbVal += lastVolumeUnitsChansValuesList[z];

    if (dbVal == 0)
        lastMonoVolumeUnits = dbVal;
    else
        lastMonoVolumeUnits = dbVal/channels;

    return true;
}

bool SkAudioBuffer::isClipping(uint chanIndex)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    if (chanIndex < audioParameters.getChannels())
        return overAmplitude[chanIndex];

    return false;
}

float SkAudioBuffer::getLastMonoVolumeUnits()
{
    return lastMonoVolumeUnits;
}

float SkAudioBuffer::getLastChanVolumeUnits(uint chanIndex)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    if (lastVolumeUnitsChansValuesList && chanIndex < audioParameters.getChannels())
        return lastVolumeUnitsChansValuesList[chanIndex];

    return SILENCE_VU_UNIT;
}

double SkAudioBuffer::getLastChanMinValue(uint chanIndex)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    if (lastMinChansValuesList && chanIndex < audioParameters.getChannels())
        return lastMinChansValuesList[chanIndex];

    return 0;
}

double SkAudioBuffer::getLastChanMaxValue(uint chanIndex)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    if (lastMaxChansValuesList && chanIndex < audioParameters.getChannels())
        return lastMaxChansValuesList[chanIndex];

    return 0;
}

float SkAudioBuffer::getTickTimePeriod()
{
    return tickTimePeriod;
}

uint64_t SkAudioBuffer::getInputCanonicalSize()
{
    return inputCanonicalSize;
}

uint64_t SkAudioBuffer::getInputOneSecondSize()
{
    return audioParameters.getOneSecondSize();
}

SkRingBuffer *SkAudioBuffer::getInputProduction()
{
    return inputDataProduction;
}

#endif
