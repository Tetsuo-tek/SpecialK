#include "skspeechtotext.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_SPEECH_TO_TEXT)

DeclareMeth_INSTANCE_RET(SkSpeechToText, getOutputBuffer, SkRingBuffer*)
DeclareMeth_INSTANCE_RET(SkSpeechToText, open, bool, *Arg_Custom(SkArgsMap), Arg_UInt, Arg_UInt)
DeclareMeth_INSTANCE_RET(SkSpeechToText, close, bool)
DeclareMeth_INSTANCE_VOID(SkSpeechToText, lock)
DeclareMeth_INSTANCE_VOID(SkSpeechToText, unlock)

ConstructorImpl(SkSpeechToText, SkObject,
                {
                    AddMeth_INSTANCE_RET(SkSpeechToText, getOutputBuffer);
                    AddMeth_INSTANCE_RET(SkSpeechToText, open);
                    AddMeth_INSTANCE_RET(SkSpeechToText, close);
                    AddMeth_INSTANCE_VOID(SkSpeechToText, lock);
                    AddMeth_INSTANCE_VOID(SkSpeechToText, unlock);
                })
{

    initialized = false;
    rate = 0;
    bufferFrames = 0;
    sphx_config = nullptr;
    ps = nullptr;
    ad = nullptr;
    utt_started = false;
    lastRecognizedSomeThing = false;

    SlotSet(tick);
    SignalSet(listening);
    SignalSet(recognized);
}

SkRingBuffer *SkSpeechToText::getOutputBuffer()
{
    return &outputBuffer;
}

/*SkRingBuffer *SkSpeechToText::getPcmInputBuffer()
{
    return &pcmInputBuffer;
}*/

static const arg_t cont_args_def[] = {
    POCKETSPHINX_OPTIONS,
    CMDLN_EMPTY_OPTION
};

bool SkSpeechToText::open(SkArgsMap &arguments, uint sampleRate, uint frames)
{
    if (initialized)
    {
        ObjectError("SphinxEngine is ALREADY initialized");
        return false;
    }

    sphxArguments = arguments;
    rate = static_cast<int>(sampleRate);
    bufferFrames = static_cast<int>(frames);

    uint64_t argc = sphxArguments.count()*2;
    char **argv = new char * [argc];

    SkStringList k;
    sphxArguments.keys(k);

    for(uint64_t i=0, z=0; i<sphxArguments.count(); i++)
    {
        uint64_t sz = k[i].length();
        argv[z] = new char [sz+1];
        memcpy(argv[z], k[i].data(), sz);
        argv[z++][sz] = '\0';

        CStr *v = sphxArguments[k[i]].data();
        sz = strlen(v);
        argv[z] = new char [sz+1];
        memcpy(argv[z], v, sz);
        argv[z++][sz] = '\0';
    }

    //LET'S PRINT "Listening ..."
    lastRecognizedSomeThing = true;

    SkString s;
    sphxArguments.toString(s, true);
    ObjectMessage("Starting SphinxEngine arguments: " << s << "..");

    sphx_config = cmd_ln_parse_r(nullptr,
                                 cont_args_def,
                                 static_cast<int32>(argc),
                                 argv,
                                 true);

    ps_default_search_args(sphx_config);
    ps = ps_init(sphx_config);

    if (!ps)
    {
        ObjectError("Cannot start SphinxEngine");
        KillApp();
    }

    if (!(ad = ad_open_sps(rate)))
    {
        ObjectError("Cannot start SphinxRngine audio-input");
        KillApp();
    }

    if (ad_start_rec(ad) < 0)
    {
        ObjectError("Failed to start recording");
        KillApp();
    }

    if (ps_start_utt(ps) < 0)
    {
        ObjectError("Failed to start utterance");
        KillApp();
    }

    utt_started = false;

    for(uint64_t i=0; i<argc; i++)
        delete [] argv[i];

    delete [] argv;

    initialized = true;
    ObjectMessage("SphinxRngine initialized");
    return true;
}

bool SkSpeechToText::close()
{
    if (!initialized)
    {
        ObjectError("SphinxEngine is NOT initialized yet");
        return false;
    }

    if (ad)
    {
        ad_stop_rec(ad);
        ad_close(ad);
        ad = nullptr;
    }

    if (ps)
    {
        ps_free(ps);
        ps = nullptr;
    }

    if (sphx_config)
    {
        cmd_ln_free_r(sphx_config);
        sphx_config = nullptr;
    }

    ObjectMessage("SphinxRngine CLOSED");
    return true;
}

void SkSpeechToText::lock()
{
    listenEx.lock();
    ObjectDebug("Listening LOCKED");
}

void SkSpeechToText::unlock()
{
    listenEx.unlock();
    ObjectDebug("Listening UNLOCKED");
}

SlotImpl(SkSpeechToText, tick)
{
    SilentSlotArgsWarning();

    int32 k;
    int16 adbuf[bufferFrames];
    bool in_speech = false;

    listenEx.lock();

    if ((k = ad_read(ad, adbuf, bufferFrames)) < 0)
    {
        ObjectError("Failed to read audio");
        KillApp();
    }

    if (k)
    {
        if (ps_process_raw(ps, adbuf, static_cast<uint64_t>(k), false, false) >= 0)
            in_speech = ps_get_in_speech(ps);
    }

    if (in_speech && !utt_started)
    {
        utt_started = true;
        if (lastRecognizedSomeThing)
            ObjectMessage("Listening...");

        lastRecognizedSomeThing = false;
        listening();
    }

    if (!in_speech && utt_started)
    {
        //speech -> silence transition, time to start new utterance
        ps_end_utt(ps);
        char const *hyp = ps_get_hyp(ps, nullptr);

        lastRecognizedSomeThing = !SkString::isEmpty(hyp);

        if (lastRecognizedSomeThing)
        {
            SkString txt(hyp);
            outputBuffer.addString(txt);
            ObjectMessage("User spoken: " << hyp)
            recognized();

        }

        if (ps_start_utt(ps) < 0)
        {
            ObjectError("Failed to start utterance");
            KillApp();
        }

        utt_started = false;

        if (lastRecognizedSomeThing)
            ObjectMessage("Ready ..");
    }

    listenEx.unlock();
}

#endif
