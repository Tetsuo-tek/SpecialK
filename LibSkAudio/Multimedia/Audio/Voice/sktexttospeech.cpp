#if defined(ENABLE_AUDIO) && defined(ENABLE_TEXT_TO_SPEECH)

#include "sktexttospeech.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skarraycast.h"
#include "Core/App/skeventloop.h"

ConstructorImpl(SkTextToSpeech, SkObject)
{
    initialized = false;
    lastTimePos = 0;

    currentWordsNumber = 0;
    currentSentencesNumber = 0;

    SlotSet(fastTick);

    SignalSet(speechStarted);
    SignalSet(speechFinished);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTextToSpeech::enable()
{
    if (initialized)
    {
        ObjectError("EspeakEngine is ALREADY initialized");
        return false;
    }

    bitset<sizeof(int)> bs =  {0};
    bs[0] = true;
    bs[1] = true;
    int options = bs.to_ulong();

    rate = espeak_Initialize(AUDIO_OUTPUT_SYNCHRONOUS,//AUDIO_OUTPUT_RETRIEVAL,//
                             0,
                             nullptr,
                             options);

    if (rate < 0)
    {
        ObjectError("Cannot initialize speaker");
        return false;
    }

    espeak_SetSynthCallback(synthCallback);
    initialized = true;
    ObjectMessage("Speaker initialized: " << rate << " Hz");
    return true;
}

bool SkTextToSpeech::disable()
{
    if (!initialized)
    {
        ObjectError("EspeakEngine is NOT initialized yet");
        return false;
    }

    espeak_Terminate();
    ObjectMessage("EspeakEngine CLOSED");

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SkTextToSpeech::setVoice(CStr *lang, uint8_t gender, uint8_t age)
{
    espeak_VOICE *v = espeak_GetCurrentVoice();

    v->languages = lang;
    v->gender = gender;
    v->age = age;

    ObjectMessage("Setup [Lang: " << v->languages
              << "; Gender: " << static_cast<int>(v->gender)
              << "; Age: " << static_cast<int>(v->age) << "]");

    espeak_SetVoiceByProperties(v);
}

bool SkTextToSpeech::setWordsForMinutes(int val)
{
    ObjectMessage("Setup [WordsForMinutes: " << val << "]");
    return (espeak_SetParameter(espeakRATE, val, 0) == EE_OK);
}

bool SkTextToSpeech::setVolume(int val)
{
    ObjectMessage("Setup [Volume: " << val << "]");
    return (espeak_SetParameter(espeakVOLUME, val, 0) == EE_OK);
}

bool SkTextToSpeech::setPitch(int val)
{
    ObjectMessage("Setup [Pitch: " << val << "]");
    return (espeak_SetParameter(espeakPITCH, val, 0) == EE_OK);
}

bool SkTextToSpeech::setRange(int val)
{
    ObjectMessage("Setup [Range: " << val << "]");
    return (espeak_SetParameter(espeakRANGE, val, 0) == EE_OK);
}

bool SkTextToSpeech::setPunctuations(espeak_PUNCT_TYPE val)
{
    ObjectMessage("Setup [Punctations: " << val << "]");
    return (espeak_SetParameter(espeakPUNCTUATION, val, 0) == EE_OK);
}

bool SkTextToSpeech::setCapitals(int val)
{
    ObjectMessage("Setup [Capitals: " << val << "]");
    return (espeak_SetParameter(espeakCAPITALS, val, 0) == EE_OK);
}

bool SkTextToSpeech::setWordsPauses(int val)
{
    ObjectMessage("Setup [WordPauses: " << val << "]");
    return (espeak_SetParameter(espeakWORDGAP, val, 0) == EE_OK);
}

bool SkTextToSpeech::setIntonation(int val)
{
    ObjectMessage("Setup [Intonation: " << val << "]");
    return (espeak_SetParameter(espeakINTONATION, val, 0) == EE_OK);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

int SkTextToSpeech::synthCallback(short *pcm, int count, espeak_EVENT *events)
{
    SkTextToSpeech *speaker = static_cast<SkTextToSpeech *>(events->user_data);

    while (events->type != espeakEVENT_LIST_TERMINATED)
    {
        int duration = events->audio_position - speaker->lastTimePos;

        if (events->type == espeakEVENT_PHONEME)
        {
            speaker->lastTimePos = events->audio_position;

            char name[9];
            memcpy(name, events->id.string, 8);
            name[9] = '\0';

            if (strlen(name))
            {
                SkTextToSpeechPhoneme *ph = new SkTextToSpeechPhoneme;
                ph->phoneme = name;
                ph->isInteruption = false;
                ph->word = speaker->currentWordsNumber;
                ph->sentence = speaker->currentSentencesNumber;
                ph->charPos = events->text_position;
                ph->timePos = events->audio_position;
                ph->duration = duration;

                speaker->currentSpokenPhonemes.enqueue(ph);
                //cout << "Ph:\t'" << name << "'\t";
            }

            /*else
                cout << "Ph:\t'EMPTY'\t";*/
        }

        else if (events->type == espeakEVENT_WORD)
        {
            //cout << "Word-number: '" << events->id.number << "'";
            speaker->currentWordsNumber = events->id.number;
        }

        else if (events->type == espeakEVENT_SENTENCE)
        {
            //cout << "Sentence-number: '" << events->id.number << "'";
            speaker->currentSentencesNumber = events->id.number;
        }

        else if (events->type == espeakEVENT_MARK || events->type == espeakEVENT_PLAY)
        {
            //cout << "MARK: '" << events->id.name << "'";
        }

        else if (events->type == espeakEVENT_MARK || events->type == espeakEVENT_PLAY)
        {
            //cout << "PLAY: '" << events->id.name << "'";
        }

        else if (events->type == espeakEVENT_LIST_TERMINATED)
        {
            //cout << "list terminated";
        }

        else if (events->type == espeakEVENT_END)
        {
            SkTextToSpeechPhoneme *ph = new SkTextToSpeechPhoneme;
            ph->phoneme = "(EMPTY)";
            ph->isInteruption = true;
            ph->word = speaker->currentWordsNumber;
            ph->sentence = speaker->currentSentencesNumber;
            ph->charPos = events->text_position;
            ph->timePos = events->audio_position;
            ph->duration = duration;

            speaker->currentSpokenPhonemes.enqueue(ph);
            //cout << "end sentence";
        }

        else if (events->type == espeakEVENT_MSG_TERMINATED)
        {
            //cout << "end message";
        }

        if (events->type == espeakEVENT_SAMPLERATE)
        {
            //cout << "samplerate (internal): " << events->id.number << " Hz";
        }

        /*cout << " -> " << "text_pos:" << events->text_position << " char - "
             << "audio_pos:" << events->audio_position << " ms - "
             << "duration:" << duration << " ms\n";*/

        events++;
    }

    if (!count || !pcm)
        return 0;

    ULong sz = count*sizeof(short);
    speaker->pcm.addData(SkArrayCast::toChar(pcm), sz);
    return 0;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkTextToSpeech::synth(CStr *txt)
{
    if (!currentSpokenPhonemes.isEmpty())
    {
        while(!currentSpokenPhonemes.isEmpty())
            delete currentSpokenPhonemes.dequeue();
    }

    currentWordsNumber = 0;
    currentSentencesNumber = 0;
    lastTimePos = 0;

    SkString text = txt;
    ObjectMessage("Speeching [sz: " << text.size() << "; len: " << text.length() << "]:  '" << text << "'");

    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkDirect);
    speechStarted();
    onSpeechStarted();

    espeak_ERROR espeakErr;

    if ((espeakErr = espeak_Synth(text.c_str(), text.size(),
                                  0, (espeak_POSITION_TYPE) 0, 0,
                                  espeakCHARS_AUTO, nullptr, this)) != EE_OK)
    {
        ObjectError("Cannot create EspeakSynth [espeakErr: " << espeakErr << "]");
        return false;
    }

    return true;
}

SlotImpl(SkTextToSpeech, fastTick)
{
    SilentSlotArgsWarning();

    if (!espeak_IsPlaying())
    {
        Detach(eventLoop()->fastZone_SIG, pulse, this, fastTick);
        ObjectMessage("Speech FINISHED");
        speechFinished();//WRONG ON RETRIEVAL
        onSpeechFinished();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

int SkTextToSpeech::sampleRate()
{
    return rate;
}

SkRingBuffer &SkTextToSpeech::pcmData()
{
    return pcm;
}

SkQueue<SkTextToSpeechPhoneme *> &SkTextToSpeech::phonemes()
{
    return currentSpokenPhonemes;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
