#ifndef SKSPEECHTOTEXT_H
#define SKSPEECHTOTEXT_H

#if defined(ENABLE_SPEECH_TO_TEXT)

#include "Core/Containers/skringbuffer.h"
#include "Core/Object/skobject.h"

#include "sphinxbase_prim_type.h"

#include <pocketsphinx.h>
#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>

class SKAUDIO SkSpeechToText extends SkObject
{
    public:
        Constructor(SkSpeechToText, SkObject);

        SkRingBuffer *getOutputBuffer();
        SkRingBuffer *getPcmInputBuffer();

        bool open(SkArgsMap &arguments, uint sampleRate, uint frames);
        bool close();

        void lock();
        void unlock();

        Slot(tick);
        Signal(listening);
        Signal(recognized);

    private:
        bool initialized;
        SkRingBuffer outputBuffer;
        //SkRingBuffer pcmInputBuffer;
        SkMutex listenEx;

        ps_decoder_t *ps;
        cmd_ln_t *sphx_config;
        ad_rec_t *ad;
        bool utt_started;

        bool lastRecognizedSomeThing;

        int rate;
        int bufferFrames;
        SkArgsMap sphxArguments;
};

#endif

#endif // SKSPEECHTOTEXT_H
