/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKTEXTTOSPEECH_H
#define SKTEXTTOSPEECH_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_TEXT_TO_SPEECH)

#include "Core/Object/skobject.h"
#include "Core/Containers/skringbuffer.h"

#include <espeak/speak_lib.h>

struct SkTextToSpeechPhoneme
{
    SkString phoneme;
    ULong word;
    ULong sentence;
    bool isInteruption;
    ULong charPos;//starting from 1
    ULong timePos;//ms
    ULong duration;//ms
};

class SKAUDIO SkTextToSpeech extends SkObject
{
    bool initialized;
    int rate;
    SkRingBuffer pcm;
    int lastTimePos;

    int currentWordsNumber;
    int currentSentencesNumber;
    SkQueue<SkTextToSpeechPhoneme *> currentSpokenPhonemes;

    public:
        Constructor(SkTextToSpeech, SkObject);

        bool enable();
        bool disable();

        void setVoice(CStr *lang, uint8_t gender, uint8_t age);

        //espeakRATE:    speaking speed in word per minute.  Values 80 to 450.
        bool setWordsForMinutes(int val);

        //espeakVOLUME:  volume in range 0-200 or more.
        //               0=silence, 100=normal full volume, greater values may produce amplitude compression or distortion
        bool setVolume(int val);

        //espeakPITCH:   base pitch, range 0-100.  50=normal
        bool setPitch(int val);

        //espeakRANGE:   pitch range, range 0-100. 0-monotone, 50=normal
        bool setRange(int val);

        //espeakPUNCTUATION:  which punctuation characters to announce:
        //  value in espeak_PUNCT_TYPE (none, all, some),
        //  see espeak_GetParameter() to specify which characters are announced.
        //
        //  enum {
        //      espeakPUNCT_NONE=0,
        //      espeakPUNCT_ALL=1,
        //      espeakPUNCT_SOME=2
        //  }
        bool setPunctuations(espeak_PUNCT_TYPE val);

        //espeakCAPITALS: announce capital letters by:
        //   0=none,
        //   1=sound icon,
        //   2=spelling,
        //   3 or higher, by raising pitch.  This values gives the amount in Hz by which the pitch
        //      of a word raised to indicate it has a capital letter.
        bool setCapitals(int val);

        //espeakWORDGAP:  pause between words, units of 10mS (at the default speed)
        bool setWordsPauses(int val);

        //espeakINTONATION : ???
        bool setIntonation(int val);

        bool synth(CStr *txt);

        int sampleRate();
        SkRingBuffer &pcmData();
        SkQueue<SkTextToSpeechPhoneme *> &phonemes();

        Slot(fastTick);

        Signal(speechStarted);
        Signal(speechFinished);

    protected:
        virtual void onSpeechStarted()      {}
        virtual void onSpeechFinished()     {}

    private:
        friend void synthCallback(short *, int , espeak_EVENT*);
        static int synthCallback(short *pcm, int count, espeak_EVENT *events);
};

#endif

#endif // SKTEXTTOSPEECH_H
