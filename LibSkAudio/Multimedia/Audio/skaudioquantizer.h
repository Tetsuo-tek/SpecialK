/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAUDIOQUANTIZER_H
#define SKAUDIOQUANTIZER_H

#if defined(ENABLE_AUDIO)

#include "skaudioparameters.h"

typedef struct
{
    double max;
    double k;
    uint bytesPerSample;
    bool isUnsigned;
    uint64_t zero;
} SkAudioConversionFactor;

//IT IS NOT TH-SAFE!
//IT NEEDS TO SUBCLASS IT AND DATA POINTERS WILL BE MANAGED OUT OF HERE

class SKAUDIO SkAudioQuantizer extends SkFlatObject
{
    public:
        SkAudioQuantizer();

        //REQUIRED if this type is not subclassed; otherwise use directly resources
        void setup(SkAudioParameters &audioParameters, double */*tempInterleavedDoubleBuffer*/, double **tempPlanarDoubleBuffer);

        static void conversionFactor(SkAudioFmtType fmt, SkAudioConversionFactor *factor);

        //you would control input and output parameters;
        //if they are equals, you will load CPU without any reasons!
        void convertSampleTypeTo(SkAudioParameters *tempAudioParameters, char *data);

        //the unsigned could be casted from signed!!
        //perhaps all U* methods have NO_SENSE!

        char *convertSampleTypeToData_CHAR(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_UCHAR(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_SHORT(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_USHORT(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_INT(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_UINT(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_FLOAT(SkAudioParameters *tempAudioParameters, char *outputBuffer);
        char *convertSampleTypeToData_DOUBLE(SkAudioParameters *tempAudioParameters, char *outputBuffer);

        //CONVERT INTO BUFFER[bufferFrames] WITH MIX-AVERAGEd OF ALL CHANNELS
        void convertToMono(double *outputBuffer);

        static void planarToInterleaved_DOUBLE(uint channels, uint bufferFrames, const double *planar, double *interleaved);
        static void planarToInterleaved_FLOAT(uint channels, uint bufferFrames, const float *planar, float *interleaved);
        static void planarToInterleaved_INT(uint channels, uint bufferFrames, const Int *planar, Int *interleaved);
        static void planarToInterleaved_SHORT(uint channels, uint bufferFrames, const Short *planar, Short *interleaved);
        static void planarToInterleaved_CHAR(uint channels, uint bufferFrames, const char *planar, char *interleaved);

        //THESE METHS ARE NOT PROTECTED FROM EX
        double **getPlanarRawBufferData();
        double *getPlanarRawBufferData(uint channelIDX);

        std::mutex &getBufferEx();

    protected:
        SkAudioParameters audioParameters;
        uint totalBufferSamples;

        //9 are enum-items of SkAudioFmtType
        SkAudioConversionFactor factors[9];

        //external pointer is not managed from quantizer
        //allocations and deallocations are performed out of here (i.e. in the subclasser)
        //ONLY A MUTEX IS SUPPLIED FOR MANAGING IT
        std::mutex planarDoubleBufferEx;
        double **planarDoubleBuffer;

        void bitConversion(char *inputBuffer, uint inputIDX, uint channelIDX, uint sampleIDX);

    private:
        void internalClip(double &inputValue);
};

#endif

#endif // SKAUDIOQUANTIZER_H
