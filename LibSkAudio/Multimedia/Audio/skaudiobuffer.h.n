/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo (at) biodroid-community (dot) net
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAUDIOBUFFER_H
#define SKAUDIOBUFFER_H

#include "skaudioresampler.h"
#include "Core/Containers/skstringlist.h"
#include "Core/Containers/skringbuffer.h"

struct SKAUDIO SkAudioProcessing
{
    bool mute;
    double gain;
    double *channelsVolume;
    double bass;
    double middle;
    double treble;

    SkAudioProcessing()
    {
        mute = false;
        gain = 1;
        channelsVolume = nullptr;
        bass  = 1;
        middle = 1;
        treble = 1;
    }
};

struct SKAUDIO SkAudioEqState
{
    bool initialized;

    // Very small amount (Denormal Fix)
    double vsa;

    // Filter #1 (Low band)
    double lf; // Frequency
    double f1p0; // Poles ...
    double f1p1;
    double f1p2;
    double f1p3;

    // Filter #2 (High band)
    double hf; // Frequency
    double f2p0; // Poles ...
    double f2p1;
    double f2p2;
    double f2p3;

    // Sample history buffer
    double sdm1; // Sample data minus 1
    double sdm2; // 2
    double sdm3; // 3

    // Gain Controls
    double lg; // low gain
    double mg; // mid gain
    double hg; // high gain
};

struct SKAUDIO SkAudioCompressor
{
    bool expanderEnabled;
    double lowerThreshold;
    uint8_t lowerRatio;
    double expanderResizeCostant;

    bool limiterEnabled;
    double upperThreshold;
    uint8_t upperRatio;
    double limiterThresholdZone;
    double limiterSurplusZone;
    double limiterClipperValue;
    double limiterResizeCostant;

    SkAudioCompressor()
    {
        expanderEnabled = false;
        lowerThreshold = 0;
        lowerRatio = 0;
        expanderResizeCostant = 0;

        limiterEnabled = false;
        upperThreshold = 0;
        upperRatio = 0;
        limiterThresholdZone = 0;
        limiterSurplusZone = 0;
        limiterClipperValue = 0;
        limiterResizeCostant = 0;
    }

    void setExpander(bool enabled, double threshold, uint8_t ratio)
    {
        expanderEnabled = enabled;
        lowerThreshold = threshold;
        lowerRatio = ratio;

        expanderResizeCostant = (1./lowerRatio);
    }

    void setLimiter(bool enabled, double threshold, uint8_t ratio)
    {
        limiterEnabled = enabled;
        upperThreshold = threshold;
        upperRatio = ratio;

        limiterThresholdZone = 1 - upperThreshold;
        limiterSurplusZone = limiterThresholdZone * upperRatio;
        limiterClipperValue = 1 + limiterSurplusZone;
        limiterResizeCostant = limiterThresholdZone / (limiterThresholdZone + limiterSurplusZone);
    }
};

class SkAudioBuffer;

struct SKAUDIO SkAudioOutput
{
    SkString name;
    bool enabled;
    SkAudioParameters audioParameters;
    SkAudioResampler *resampler;//MANAGED FROM HERE
    SkRingBuffer *production;//MANAGED OUT OF HERE
    SkAudioBuffer *linkedBuffer;//IT WILL BE TICKED ON SkAudioBuffer::processOutputs()

    SkAudioOutput()
    {
        enabled = false;
        resampler = nullptr;
        production = nullptr;
        linkedBuffer = nullptr;
    }
};

//IT IS NOT TH-SAFE!

class SKAUDIO SkAudioBuffer : public SkAudioQuantizer
{
    public:
        SkAudioBuffer();
        ~SkAudioBuffer() override;

        void setAudioParameters(SkAudioParameters &parameters, SkRingBuffer *inputProductionBuffer);

        SkAudioProps getAudioProperties();
        SkAudioParameters getAudioParameters();

        //WHEN CHANGING VALUES, IT REQUIRES TO LOCK bufferEx
        SkAudioProcessing &getAudioProcessing();

        void setSilenceDB(float value);

        void setMute(bool enabled);
        void setGain(double value);
        void setChanVolume(uint chanIndex, double value);
        void setBass(double value);
        void setMiddle(double value);
        void setTreble(double value);

        void setEqState(SkAudioEqState &state);
        const SkAudioEqState *getEqState();

        void setExpanderCompressor(bool enabled, double threshold, uint8_t ratio);
        void setLimiterCompressor(bool enabled, double threshold, uint8_t ratio);

        const SkAudioCompressor *getCompressor();

        void setSilence(uint64_t tempIndex, float tempTime);
        //void mixBuffer(CStr *buffer);

        //the single output-production-buffer will be managed out of here!
        bool addOutput(CStr *name,
                       SkAudioParameters &parameters,
                       SkRingBuffer *production,
                       SkAudioBuffer *linkedBuffer=nullptr,
                       bool enabled=true);

        size_t getOutputsCount();
        SkStringList &getOutputNames();
        SkAudioOutput *getOutputByName(CStr *name);
        SkAudioOutput *getOutputByName(SkString &name);
        SkAudioOutput *getOutputByID(size_t i);
        bool outputExists(CStr *name);
        bool delOutput(CStr *name);
        void deleteAllOutputs();

        //This method uses production buffers to automatically CONSUME and PRODUCE
        //data, respectively from-INPUT (grabbing from  SkRingBuffer *inputData)
        //to-OUTPUT (adding to SkAudioOutput::*production)
        bool tick();

        bool isClipping(uint chanIndex);

        //float getLastMonoVolumeUnits();
        float getLastChanVolumeUnits(uint chanIndex);
        double getLastChanMinValue(uint chanIndex);
        double getLastChanAvgValue(uint chanIndex);
        double getLastChanMaxValue(uint chanIndex);
        float getTickTimePeriod();
        size_t getInputCanonicalSize();
        size_t getInputOneSecondSize();
        SkRingBuffer *getInputProduction();

    private:
        //ALL IS TH-SAFE BY SkAudioQuantizer::planarDoubleBufferEx

        size_t inputCanonicalSize;
        SkRingBuffer *inputDataProduction;
        SkAudioProcessing processingProperties;
        SkAudioEqState eqState;
        SkAudioCompressor compressor;

        double *lastMinChansValuesList;
        double *lastAvgChansValuesList;
        double *lastMaxChansValuesList;

        float *sumOfSquares;
        float *db;
        float *lastVolumeUnitsChansValuesList;
        float lastMonoVolumeUnits;
        float silenceDB;

        bool *overAmplitude;

        uint64_t index;
        float time;
        float tickTimePeriod;

        void setBuffer(char *buffer, uint64_t tempIndex, float tempTime);

        SkMutex outputEx;
        SkStringList outputsNames;
        SkVector<SkAudioOutput *> outputs;

        void initEqState(uint lowfreq, uint highfreq, uint rate);

        void process(uint chanIDX, uint sampleIDX);
        void equalize(double &inputValue);
        void compress(double &value);

        void processOutputs();

        void resetOutput(int outputIndex);
        void destroyAll();
};

#endif // SKAUDIOBUFFER_H

