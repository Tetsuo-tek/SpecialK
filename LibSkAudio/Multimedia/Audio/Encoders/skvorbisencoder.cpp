#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skvorbisencoder.h"
#include <Core/sklogmachine.h>
#include <Core/Containers/skarraycast.h>

SkVorbisEncoder::SkVorbisEncoder()
{
    chanSize = 0;
    interleavedBuffer = nullptr;
    setObjectName("OggVorbisEncoder");
}

bool SkVorbisEncoder::initializeABR(CStr *encoderName,
                                    int nominalBitRate,
                                    int minBitRate,
                                    int maxBitRate)
{
    if (nominalBitRate < 1024)
    {
        FlatError("ABR encoding bitRate is NOT valid; it MUST greater than 1024 bps");
        return false;
    }

    if (minBitRate < -1 || minBitRate > nominalBitRate)
    {
        FlatError("ABR encoding minBitRate is NOT valid; it MUST be in the interval [-1, nominalBitRate] bps");
        return false;
    }

    if (maxBitRate < -1 || maxBitRate < nominalBitRate)
    {
        FlatError("ABR encoding minBitRate is NOT valid; it MUST be in the interval [-1, nominalBitRate] bps");
        return false;
    }

    SkVorbisEncProperties props;
    props.mode = SkVorbisEncMode::VBM_ABR;
    props.abrNominalBitRate = nominalBitRate;
    props.abrMinBitRate = minBitRate;
    props.abrMaxBitRate = maxBitRate;

    return initialize(encoderName, props);
}

bool SkVorbisEncoder::initializeVBR(CStr *encoderName, float quality)
{
    if (quality < -0.1f || quality > 1.0f)
    {
        FlatError("VBR encoding-quality MUST be in the interval [-0.1, 1.0]");
        return false;
    }

    SkVorbisEncProperties props;
    props.mode = SkVorbisEncMode::VBM_VBR;
    props.vbrQuality = quality;

    return initialize(encoderName, props);
}

bool SkVorbisEncoder::initialize(CStr *encoderName, SkVorbisEncProperties &p)
{
    if (audioParameters.getFormat() != SkAudioFmtType::AFMT_FLOAT)
    {
        FlatError("Only FLOAT type is ALLOWED to make a Vorbis encoding");
        return false;
    }

    uint channels = audioParameters.getChannels();
    long sampleRate = audioParameters.getSampleRate();

    vorbis_info_init(&vi);

    int ret = 0;

    if (p.mode == SkVorbisEncMode::VBM_VBR)
        ret = vorbis_encode_init_vbr(&vi, channels, sampleRate, p.vbrQuality);

    else if (p.mode == SkVorbisEncMode::VBM_ABR)
        ret = vorbis_encode_init(&vi, channels, sampleRate, p.abrMaxBitRate, p.abrNominalBitRate, p.abrMinBitRate);

    if (ret)
    {
        FlatError("Cannot setup the encoder");
        return false;
    }

    initializePacking();

    vorbis_analysis_init(&vd, &vi);
    vorbis_block_init(&vd, &vb);

    vorbis_comment_init(&vc);
    vorbis_comment_add_tag(&vc, "Encoder", encoderName);

    vorbis_analysis_headerout(&vd,
                              &vc,
                              &vorbis_header,
                              &vorbis_header_comm,
                              &vorbis_header_code);

    ogg_stream_packetin(&os, &vorbis_header);
    ogg_stream_packetin(&os, &vorbis_header_comm);
    ogg_stream_packetin(&os, &vorbis_header_code);

    flushHeader();

    if (audioParameters.isPlanar())
    {
        chanSize = inputProduction->getCanonicalSize() / channels;
        FlatDebug("Using PLANAR input buffer");
    }

    else
    {
        interleavedBuffer = new char [inputProduction->getCanonicalSize()];
        FlatDebug("Using INTERLEAVED input buffer");
    }

    return true;
}

void SkVorbisEncoder::tick()
{
    uint bufferFrames = audioParameters.getBufferFrames();
    uint channels = audioParameters.getChannels();

    if (inputProduction->isCanonicalSizeReached())
    {
        float **buffer = vorbis_analysis_buffer(&vd, static_cast<int>(bufferFrames));

        if (audioParameters.isPlanar())
            for(uint z=0; z<channels; z++)
                inputProduction->getCustomBuffer(SkArrayCast::toChar(buffer[z]), chanSize);

        else
        {
            inputProduction->getCanonicalBuffer(interleavedBuffer);

            uint bufferSamples = audioParameters.getBufferSamples();

            for(uint i=0, planarIDX=0; i<bufferSamples; i+=channels, planarIDX++)
                for(uint z=0; z<channels; z++)
                    buffer[z][planarIDX] = SkArrayCast::toFloat(interleavedBuffer)[i+z];
        }

        vorbis_analysis_wrote(&vd, static_cast<int>(bufferFrames));

        while (vorbis_analysis_blockout(&vd, &vb) == 1)
        {
            vorbis_analysis(&vb, nullptr);
            vorbis_bitrate_addblock(&vb);

            while (vorbis_bitrate_flushpacket(&vd, &op))
            {
                ogg_stream_packetin(&os, &op);
                pageOut();
            }
        }
    }
}

void SkVorbisEncoder::close()
{
    if (audioParameters.isPlanar())
        delete [] interleavedBuffer;

    vorbis_block_clear(&vb);
    vorbis_dsp_clear(&vd);
    vorbis_comment_clear(&vc);
    vorbis_info_clear(&vi);

    closePacking();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkOggVorbisTicker, SkObject)
{
    SlotSet(tick);
}

SlotImpl(SkOggVorbisTicker, tick)
{
    SilentSlotArgsWarning();

    if (enc.getInputProduction()->isEmpty())
        return;

    enc.tick();
}

SkVorbisEncoder &SkOggVorbisTicker::encoder()
{
    return enc;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkOggVorbisTh, SkThread)
{
    ticker = nullptr;
    encOK = false;
}

bool SkOggVorbisTh::init(SkAudioParameters &parameters, SkVorbisEncProperties &properties)
{
    params = parameters;
    props = properties;

    //SIMULATE
    SkVorbisEncoder encoder;
    encoder.setAudioParameters(params);

    encOK = false;

    if (props.mode == VBM_VBR)
    {
        encOK = encoder.initializeVBR("SONUSD-OGG/Vorbis", props.vbrQuality);

        if (!encOK)
            ObjectError("Cannot initialize OGG/Vorbis VBR-encoder");

    }

    else if (props.mode == VBM_ABR)
    {
        encOK = encoder.initializeABR("SONUSD-OGG/Vorbis",
                                      props.abrNominalBitRate,
                                      props.abrMinBitRate,
                                      props.abrMaxBitRate);

        if (!encOK)
            ObjectError("Cannot initialize OGG/Vorbis ABR-encoder");
    }

    if (encOK)
    {
        ULong hSize;
        const char *headerArray = encoder.getHeaderArray(hSize);
        ogaHeader.setData(headerArray, hSize);
        ObjectMessage("Header BUILT: " << hSize << " B");

        setLoopIntervals(params.getTickTimePeriod()*1000000, eventLoop()->getSlowInterval(), SK_TIMEDLOOP_RT);
    }

    return encOK;
}

SkDataBuffer &SkOggVorbisTh::header()
{
    return ogaHeader;
}

SkRingBuffer *SkOggVorbisTh::input()
{
    if (!ticker)
        return nullptr;

    return ticker->encoder().getInputProduction();
}

SkRingBuffer *SkOggVorbisTh::output()
{
    if (!ticker)
        return nullptr;

    return ticker->encoder().getOutputProduction();
}

bool SkOggVorbisTh::customRunSetup()
{
    if (!encOK)
        return false;

    ticker = new SkOggVorbisTicker;

    SkVorbisEncoder &encoder = ticker->encoder();
    encoder.setAudioParameters(params);

    if (props.mode == VBM_VBR)
    {
        encoder.initializeVBR("SONUSD-OGG/Vorbis", props.vbrQuality);
        ObjectMessage("Encoding OGG/Vorbis VBR is STARTED [quality: " << props.vbrQuality << "]");
    }

    else if (props.mode == VBM_ABR)
    {
        encOK = encoder.initializeABR("SONUSD-OGG/Vorbis",
                                      props.abrNominalBitRate,
                                      props.abrMinBitRate,
                                      props.abrMaxBitRate);

        ObjectMessage("Encoding OGG/Vorbis ABR is STARTED [bitRate: "
                      << props.abrNominalBitRate << " b/s; "
                      <<"min: " << props.abrMinBitRate << " b/s; "
                      <<"max: " << props.abrMaxBitRate << " b/s]");
    }

    ULong hSize;
    const char *headerArray = encoder.getHeaderArray(hSize);
    ogaHeader.setData(headerArray, hSize);

    Attach(thEventLoop()->fastZone_SIG, pulse, ticker, tick, SkDirect);
    return true;
}

void SkOggVorbisTh::customClosing()
{
    ticker = nullptr;
    encOK = false;
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


#endif
