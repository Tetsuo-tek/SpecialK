/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKVORBISENCODER_H
#define SKVORBISENCODER_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

#include "skoggpacketizer.h"

#include <math.h>
#include <vorbis/vorbisenc.h>

enum SkVorbisEncMode
{
    VBM_ABR,
    VBM_VBR
};

struct SkVorbisEncProperties
{
    SkVorbisEncMode mode;

    int abrNominalBitRate;
    int abrMinBitRate;
    int abrMaxBitRate;

    float vbrQuality;
} ;

class SKAUDIO SkVorbisEncoder extends SkOggPacketizer
{
    public:
        SkVorbisEncoder();

        bool initializeABR(CStr *encoderName,
                           int nominalBitRate,
                           int minBitRate=-1,
                           int maxBitRate=-1);

        //q -> -0.1 to 1.0
        bool initializeVBR(CStr *encoderName, float quality);

        void tick();
        void close();

    private:
        uint64_t chanSize;//USED WITH PLANAR BUFFER
        char *interleavedBuffer;

        vorbis_info      vi; //struct that stores all the static vorbis bitstream settings
        vorbis_comment   vc; //struct that stores all the user comments
        vorbis_dsp_state vd; //central working state for the packet->PCM decoder
        vorbis_block     vb; //local working space for packet->PCM decode

        ogg_packet vorbis_header;
        ogg_packet vorbis_header_comm;
        ogg_packet vorbis_header_code;

        bool initialize(CStr *encoderName, SkVorbisEncProperties &p);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "Core/System/Thread/skthread.h"

class SkOggVorbisTicker extends SkObject
{
    SkVorbisEncoder enc;

public:
    Constructor(SkOggVorbisTicker, SkObject);
    Slot(tick);
    SkVorbisEncoder &encoder();
};

class SkOggVorbisTh extends SkThread
{
    bool encOK;
    SkOggVorbisTicker *ticker;
    SkDataBuffer ogaHeader;
    SkVorbisEncProperties props;
    SkAudioParameters params;

public:
    Constructor(SkOggVorbisTh, SkThread);

    bool init(SkAudioParameters &parameters, SkVorbisEncProperties &properties);

    SkDataBuffer &header();
    SkRingBuffer *input();
    SkRingBuffer *output();

private:
    bool customRunSetup()                                   override;
    void customClosing()                                    override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKVORBISENCODER_H
