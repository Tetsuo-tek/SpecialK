#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

#include "skoggpacketizer.h"

#include <Core/App/skapp.h>
#include <Core/sklogmachine.h>
#include <Core/Containers/skarraycast.h>

SkOggPacketizer::SkOggPacketizer()
{
    b_o_s = 1;
    headerArray = nullptr;
    headerSize = 0;

    inputProduction = new SkRingBuffer;
    outputProduction = new SkRingBuffer;

    setObjectName("OggPacketizer");
}

SkOggPacketizer::~SkOggPacketizer()
{
    if (headerArray)
    {
        free(headerArray);
        headerArray = nullptr;
    }

    delete inputProduction;
    delete outputProduction;
}

void SkOggPacketizer::setAudioParameters(SkAudioParameters &tempAudioParameters)
{
    audioParameters = tempAudioParameters;
    inputProduction->setCanonicalSize(audioParameters.getCanonicalSize());
}

SkRingBuffer *SkOggPacketizer::getInputProduction()
{
    return inputProduction;
}

SkRingBuffer *SkOggPacketizer::getOutputProduction()
{
    return outputProduction;
}

void SkOggPacketizer::initializePacking()
{
    SkString name = objectName();
    name.append(".IN");
    inputProduction->setObjectName(name.c_str());
    name = objectName();
    name.append(".OUT");
    outputProduction->setObjectName(name.c_str());

    srand(static_cast<uint>(time(nullptr)));
    ogg_stream_init(&os, rand());
}

bool SkOggPacketizer::hasHeader()
{
    return (headerSize > 0);
}

CStr *SkOggPacketizer::getHeaderArray(uint64_t &arraySize)
{
    arraySize = static_cast<uint64_t>(headerSize);
    return headerArray;
}

void SkOggPacketizer::prepareEncodedPacket(unsigned char *buffer,
                                          long size,
                                          ogg_int64_t granulePos,
                                          ogg_int64_t packetno)
{
    op.packet = buffer;
    op.bytes = size;
    op.b_o_s = b_o_s;
    op.e_o_s = 0;
    op.granulepos = granulePos;
    op.packetno = packetno;

    ogg_stream_packetin(&os, &op);

    if (b_o_s)
        b_o_s = 0;
}

void SkOggPacketizer::flushHeader()
{
    while(true)
    {
        int result = ogg_stream_flush(&os, &og);

        if(result == 0)
            break;

        long oldSize = headerSize;

        headerSize = og.header_len + og.body_len;
        headerArray = SkArrayCast::toChar(realloc(headerArray, static_cast<uint64_t>(headerSize+oldSize)));

        memcpy(&headerArray[oldSize], og.header, static_cast<uint64_t>(og.header_len));
        memcpy(&headerArray[oldSize+og.header_len], og.body, static_cast<uint64_t>(og.body_len));

        headerSize = oldSize + headerSize;
    }

    FlatDebug("OGG HEADER SIZE " << headerSize << " B");
}

void SkOggPacketizer::pageOut()
{
    while(true)
    {
        int result = ogg_stream_pageout(&os, &og);

        if(result == 0)
            break;

        uint64_t packetSize = static_cast<uint64_t>(og.header_len + og.body_len);
        char *packetArray = new char [packetSize];
        memcpy(packetArray, og.header, static_cast<uint64_t>(og.header_len));
        memcpy(&packetArray[og.header_len], og.body, static_cast<uint64_t>(og.body_len));
        outputProduction->addData(packetArray, packetSize);
        delete [] packetArray;
    }
}

void SkOggPacketizer::closePacking()
{
    ogg_stream_clear(&os);
}

#endif
