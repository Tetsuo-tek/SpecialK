#if defined(ENABLE_AUDIO)

#include "skaudiowave.h"
#include <Core/Containers/skarraycast.h>

ConstructorImpl(SkAudioWave, SkFile)
{
    planarBuffer = nullptr;
    chanSize = 0;

    setObjectName("WaveEncoder");
}

bool SkAudioWave::setAudioParameters(SkAudioParameters &parameters)
{
    if (parameters.getFormat() != SkAudioFmtType::AFMT_SHORT)
    {
        ObjectError("Only SHORT type is ALLOWED to make a WAVE-PCM");
        return false;
    }

    audioParameters = parameters;
    inputProduction.setCanonicalSize(audioParameters.getCanonicalSize());

    if (audioParameters.isPlanar())
    {
        uint channels = audioParameters.getChannels();
        uint bufferFrames = audioParameters.getBufferFrames();
        chanSize = inputProduction.getCanonicalSize() / channels;

        planarBuffer = new short * [channels];

        for(uint z=0; z<channels; z++)
            planarBuffer[z] = new short [bufferFrames];

        ObjectDebug("Using PLANAR input buffer");
    }

    else
        ObjectDebug("Using INTERLEAVED input buffer");

    return true;
}

bool SkAudioWave::open()
{
    if (!SkFile::open(SkFileMode::FLM_READWRITE))
        return false;

    SkString name = objectName();
    name.append(".IN");
    inputProduction.setObjectName(name.c_str());

    bool ok = writeHeader(audioParameters, this);

    if (ok)
        ObjectDebug("Wave file open and initialized");

    return ok;
}

bool SkAudioWave::writeHeader(SkAudioParameters &parameters, SkAbstractDevice *dev)
{
    if (!dev->isOpen())
        return false;

    dev->write("RIFF", 4);
    dev->writeUInt32(0xFFFFFFFF);//Placeholder for the "RIFF" chunk size (filled by close())
    dev->write("WAVE", 4);
    dev->write("fmt ", 4);
    dev->writeUInt32(16);//"fmt " chunk size (always 16 for PCM)
    dev->writeUInt16(1);//type (1 => PCM)
    dev->writeUInt16(parameters.getChannels());
    dev->writeUInt32(parameters.getSampleRate());
    dev->writeUInt32(parameters.getOneSecondSize());//samplesBlockSize
    dev->writeUInt16(parameters.getChannels() * parameters.getBytesPerSample());//blockAlign
    dev->writeUInt16(parameters.getBytesPerSample() * 8);
    dev->write("data", 4);
    dev->writeUInt32(0xFFFFFFFF);//Subchunk2Size AS Placeholder for the "data" chunk size (filled by close())

    return true;
}

void SkAudioWave::tick()
{
    if (inputProduction.isCanonicalSizeReached())
    {
        if (audioParameters.isPlanar())
        {
            uint channels = audioParameters.getChannels();
            uint bufferFrames = audioParameters.getBufferFrames();

            for(uint z=0; z<channels; z++)
                inputProduction.getCustomBuffer(SkArrayCast::toChar(planarBuffer[z]), chanSize);

            for(uint i=0; i<bufferFrames; i++)
                for(uint z=0; z<channels; z++)
                    writeInt16(planarBuffer[z][i]);
        }

        else
            write(&inputProduction, inputProduction.getCanonicalSize());

        flush();
    }
}

void SkAudioWave::close()
{
    if (!isOpen())
        return;

    flush();

    seek(4);
    writeUInt32(size() - 8);

    seek(40);
    writeUInt32(size() - 44);

    if (audioParameters.isPlanar())
    {
        for(uint z=0; z<audioParameters.getChannels(); z++)
            delete [] planarBuffer[z];

        delete [] planarBuffer;
    }

    SkFile::close();
    ObjectDebug("Wave file closed");
}

SkRingBuffer *SkAudioWave::getInputProduction()
{
    return &inputProduction;
}

#endif
