/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAUDIOWAVE_H
#define SKAUDIOWAVE_H

#if defined(ENABLE_AUDIO)

#include "Core/Containers/skringbuffer.h"
#include "Core/System/Filesystem/skfile.h"
#include "Multimedia/Audio/skaudiobuffer.h"

//Max input-channels is 2
//As input-type it requires: 16 bit / signed / interleaved (if stereo)
//ONLY PCM-16 bit is supported

//THIS IS A FILE YOU MUST SET IT AND OPEN FIRST TO USE, CLOSING IT WHEN FINISHED
class SKAUDIO SkAudioWave extends SkFile
{
    public:
        Constructor(SkAudioWave, SkFile);

        bool setAudioParameters(SkAudioParameters &parameters);

        bool open();
        void tick();
        void close();

        static bool writeHeader(SkAudioParameters &parameters, SkAbstractDevice *dev);

        SkRingBuffer *getInputProduction();

    private:
        SkAudioParameters audioParameters;
        SkRingBuffer inputProduction;

        uint64_t chanSize;
        short **planarBuffer;
};

#endif

#endif // SKAUDIOWAVE_H

/*
Pos  	Sample Value            Description
1 - 4 	"RIFF"                  Marks the file as a riff file. Characters are each 1 byte long.
5 - 8 	File size (integer) 	Size of the overall file - 8 bytes, in bytes (32-bit integer). Typically, you'd fill this in after creation.
9 -12 	"WAVE"                  File Type Header. For our purposes, it always equals "WAVE".
13-16 	"fmt "                  Format chunk marker. Includes trailing null
17-20 	16                      Length of format data as listed above
21-22 	1                       Type of format (1 is PCM) - 2 byte integer
23-24 	2                       Number of Channels - 2 byte integer
25-28 	44100                   Sample Rate - 32 byte integer. Common values are 44100 (CD), 48000 (DAT). Sample Rate = Number of Samples per second, or Hertz.
29-32 	176400                  (Sample Rate * BitsPerSample * Channels) / 8.
33-34 	4                       (BitsPerSample * Channels) / 8. 1 - 8 bit mono 2 - 8 bit stereo/16 bit mono 4 - 16 bit stereo
35-36 	16                      Bits per sample
37-40 	"data"                  "data" chunk header. Marks the beginning of the data section.
41-44 	File size               (data) 	Size of the data section.
*/

