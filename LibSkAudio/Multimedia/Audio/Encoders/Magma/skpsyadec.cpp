#if defined(ENABLE_AUDIO)

#include "skpsyadec.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

ConstructorImpl(SkPsyADec, SkAbstractMagmaDecoder)
{
    t = MGM_AUDIO;

    pckID = 0;

    bits = 0;
    max_q = 0.f;

    planarBuffer = nullptr;
    mins = nullptr;
    ratiosMin = nullptr;
    ratiosMax = nullptr;
    ratios_q = nullptr;

    loaded = false;
}

bool SkPsyADec::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    pckID = 0;
    reader->open(header, SkBufferDeviceMode::BVM_ONLYREAD);

    char cc[4];
    cc[0] = reader->readInt8();
    cc[1] = reader->readInt8();
    cc[2] = reader->readInt8();
    cc[3] = reader->readInt8();

    if (cc[0] != 'P' || cc[1] != 'S' || cc[2] != 'Y' || cc[3] != 'A')
    {
        reader->close();
        return false;
    }

    bits = reader->readUInt8();
    props.nChannels = reader->readUInt8();
    props.bufferFrames = reader->readUInt32();
    props.sampleRate = reader->readUInt32();
    props.tickTimePeriod = reader->readFloat();

    tickTimePeriod = props.tickTimePeriod;

    FlatMessage("PSY-A audio parameters -> ["
                << "channels: " << props.nChannels << "; "
                << "frames: " << props.bufferFrames << "; "
                << "rate: " << props.sampleRate << "; "
                << "period: " << tickTimePeriod << " s]");

    reader->close();

    max_q = pow(2, bits) - 1;

    planarBuffer = new float * [props.nChannels];
    mins = new float [props.nChannels];
    ratiosMin = new float [props.nChannels];
    ratiosMax = new float [props.nChannels];
    ratios_q = new uint8_t * [props.nChannels];

    for(uint z=0; z<props.nChannels; z++)
    {
        planarBuffer[z] = new float [props.bufferFrames];
        memset(planarBuffer[z], static_cast<int>(0.f), props.bufferFrames);

        mins[z] = 0.f;

        ratiosMin[z] = 0.f;
        ratiosMax[z] = 0.f;

        ratios_q[z] = new uint8_t [props.bufferFrames];
        memset(ratios_q[z], 0, props.bufferFrames);
    }

    enabled = true;
    return true;
}

void SkPsyADec::close()
{
    if (!enabled)
        return;

    for(uint z=0; z<props.nChannels; z++)
    {
        delete [] planarBuffer[z];
        delete [] ratios_q[z];
    }

    delete [] planarBuffer;
    planarBuffer = nullptr;

    delete [] mins;
    mins = nullptr;

    delete [] ratiosMin;
    ratiosMin = nullptr;

    delete [] ratiosMax;
    ratiosMax = nullptr;

    delete [] ratios_q;
    ratios_q = nullptr;

    bits = 0;
    max_q = 0;

    enabled = false;
    loaded = false;
}

void SkPsyADec::setData(SkDataBuffer &input)
{
    if (!planarBuffer)
        return;

    reader->open(input, SkBufferDeviceMode::BVM_ONLYREAD);

    for(uint z=0; z<props.nChannels; z++)
    {
        pckID = reader->readUInt64();
        planarBuffer[z][0] = reader->readFloat();
        planarBuffer[z][props.bufferFrames-1] = reader->readFloat();
        mins[z] = reader->readFloat();
        ratiosMin[z] = reader->readFloat();
        ratiosMax[z] = reader->readFloat();

        planarBuffer[z][0] += fabs(mins[z]);

        ULong sz = props.bufferFrames;
        reader->read(SkArrayCast::toChar(ratios_q[z]), sz);

    }

    reader->close();
    loaded = true;
}

bool SkPsyADec::decode()
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    if (!loaded)
    {
        FlatError("Encoder is NOT ready to encode");
        return false;
    }

    for(uint z=0; z<props.nChannels; z++)
    {
        float lastDecoded = 0.f;
        float interval = ratiosMax[z]-ratiosMin[z];

        for(uint i=0; i<props.bufferFrames-1; i++)
        {
            float ratio = ((ratios_q[z][i+1] * interval) / max_q) + ratiosMin[z];

            if (i < props.bufferFrames-2)
                planarBuffer[z][i+1] = planarBuffer[z][i] / ratio;

            else
            {
                lastDecoded = planarBuffer[z][i] / ratio;
                lastDecoded -= fabs(mins[z]);
            }

            //cout << ratiosMin[z] << " " << ratiosMax[z] << " " <<  interval << "\n";
        }

        for(uint i=0; i<props.bufferFrames-2; i++)
            planarBuffer[z][i+1] -= fabs(mins[z]);
    }

    loaded = false;
    return true;
}

float **SkPsyADec::pcm()
{
    return planarBuffer;
}

uint SkPsyADec::channels()
{
    return props.nChannels;
}

uint SkPsyADec::frames()
{
    return props.bufferFrames;
}

uint SkPsyADec::rate()
{
    return props.sampleRate;
}

bool SkPsyADec::isLoaded()
{
    return loaded;
}

#endif
