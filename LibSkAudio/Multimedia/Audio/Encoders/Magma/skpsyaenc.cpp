#if defined(ENABLE_AUDIO)

#include "skpsyaenc.h"
#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

ConstructorImpl(SkPsyAEnc, SkAbstractMagmaEncoder)
{
    t = MGM_AUDIO;

    ratios = nullptr;
    ratios_q = nullptr;
    planarBuffer = nullptr;

    enabled = false;

    bits = 0;
    max_q = 0.f;

    min = 0.f;
    minRatio = 0.f;
    maxRatio = 0.f;
    ratiosInterval = 0.f;

    pckWriter = new SkBufferDevice(this);
}

bool SkPsyAEnc::setup(SkAudioParameters &parameters, uint encodingBits)
{
    if (enabled)
    {
        FlatError("Encoder is ALREADY enabled");
        return false;
    }

    if (parameters.getFormat() != AFMT_FLOAT)
    {
        FlatError("Input samples MUST be FLOAT32");
        return false;
    }

    props = parameters.getProperties();
    bits = encodingBits;
    tickTimePeriod = props.tickTimePeriod;

    FlatMessage("PSY-A audio parameters -> ["
                << "channels: " << props.nChannels << "; "
                << "frames: " << props.bufferFrames << "; "
                << "rate: " << props.sampleRate << "; "
                << "encbits: " << bits << "; "
                << "period: " << props.tickTimePeriod << " s]");

    return true;
}

bool SkPsyAEnc::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    max_q = pow(2, bits) - 1;

    ratios = new float [props.bufferFrames];
    memset(ratios, static_cast<int>(0.f), props.bufferFrames);

    ratios_q = new uint8_t[props.bufferFrames];
    memset(ratios_q, 0, props.bufferFrames);

    planarBuffer = new float * [props.nChannels];

    for(uint z=0; z<props.nChannels; z++)
        planarBuffer[z] = new float [props.bufferFrames];

    writer->open(header, SkBufferDeviceMode::BVM_ONLYWRITE);

    writer->writeInt8('P');
    writer->writeInt8('S');
    writer->writeInt8('Y');
    writer->writeInt8('A');//signal lives on 1-Dimension

    writer->writeUInt8(bits);
    writer->writeUInt8(props.nChannels);
    writer->writeUInt32(props.bufferFrames);
    writer->writeUInt32(props.sampleRate);
    writer->writeFloat(props.tickTimePeriod);

    writer->close();

    enabled = true;
    return true;
}

void SkPsyAEnc::close()
{
    if (!enabled)
        return;

    enabled = false;

    delete [] ratios;
    ratios = nullptr;

    delete [] ratios_q;
    ratios_q = nullptr;

    for(uint z=0; z<props.nChannels; z++)
        delete [] planarBuffer[z];

    delete [] planarBuffer;

    planarBuffer = nullptr;
}

void SkPsyAEnc::setData(void *planarFloatPCM)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return;
    }

    for(uint z=0; z<props.nChannels; z++)
        memcpy(planarBuffer[z], &SkArrayCast::toFloat(planarFloatPCM)[z*props.bufferFrames], sizeof(float)*props.bufferFrames);
}

bool SkPsyAEnc::encode(SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    /*
     * EXAMPLE:
     *
     *PCM Input:
     *
     * channels = 2
     * frames   = 1024
     * rate     = 44100
     * period   ~ 0,023
     *
     * FMT            CH  FRAMES     LEN
     *
     * for each chan:
     * float32        2 * 1024  =   8192 B for 0.023 Sec of audio (one PCM buffer)
     * float32    *   2 * 44100 = 352800 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     * PSYA Output with increment ratios quantization on 8 bits
     *
     *  for each chan:
     * (float32   *   5) + 1024 =  1044 B for 0.023 Sec of audio (one PCM buffer)
                                ~ 44526 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     *  total 2 chans:
     *  (all could be also gzipped, reducing another ~ 30 % if we are luckly)
     * (float32   *   5) + 1024 =  2088 B for 0.023 Sec of audio (one PCM buffer)
     *                          ~ 89052 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     * With quantization of 8 bits on ratios, we have a compression ~ 4:1
     */

    pckWriter->open(encoded, SkBufferDeviceMode::BVM_ONLYWRITE);

    for(uint z=0; z<props.nChannels; z++)
    {
        float first = planarBuffer[z][0];
        float last = planarBuffer[z][props.bufferFrames-1];

        min = -1.f;

        for(uint i=0; i<props.bufferFrames; i++)
            if (planarBuffer[z][i] < min)
                min = planarBuffer[z][i];

        min -= 0.5;//escaping zeros

        //  0   1   2   3  ..   1022    1023    1024
        //  X   x   x   x  ..   x       x       X
        //      r   r   r               r       r
        //      1   2   3               1023    1024

        float prev = 0.f;

        maxRatio = -2.f;
        minRatio = 2.f;

        //FIRST AND LAST ARE REAL SAMPLEs
        for(uint i=0; i<props.bufferFrames; i++)
        {
            //Signal will be in [0, 2+], instead of, (inside pseudo R+)
            planarBuffer[z][i] += fabs(min);

            AssertKiller(planarBuffer[z][i]<0);

            if (i==0)
            {
                prev = planarBuffer[z][0];
                ratios[i] = 0.f;
                continue;
            }

            //r1 = x0/x1 -> x1 = x0/r1
            ratios[i] = prev / planarBuffer[z][i];

            if (ratios[i] > maxRatio)
                maxRatio = ratios[i];

            else if (ratios[i] < minRatio)
                minRatio = ratios[i];

            prev = planarBuffer[z][i];
        }

        ratiosInterval = maxRatio-minRatio;

        for(uint i=1; i<props.bufferFrames-1; i++)
        {
            float y = ((ratios[i] - minRatio) * max_q) / ratiosInterval;

            if (y >= 255.f)
                ratios_q[i] = 255;

            else
                ratios_q[i] = round(y);

            AssertKiller((int)y>255);
        }

        pckWriter->writeUInt64(pckID++);
        pckWriter->writeFloat(first);
        pckWriter->writeFloat(last);
        pckWriter->writeFloat(min);
        pckWriter->writeFloat(minRatio);
        pckWriter->writeFloat(maxRatio);
        pckWriter->write(SkArrayCast::toCStr(ratios_q), props.bufferFrames);
    }

    pckWriter->close();
    output.append(encoded);

    return true;
}

/*ConstructorImpl(SkPsyAEnc, SkAbstractMagmaEncoder)
{
    t = MGM_AUDIO;

    SlotSet(encoderStarted);
    SlotSet(loadEncoded);

    encoder = new SkPsyAEncTh(this);
    Attach(encoder, started, this, encoderStarted, SkQueued);
}

bool SkPsyAEnc::setup(SkAudioParameters &parameters, uint encodingBits)
{
    if (enabled)
    {
        FlatError("Encoder is ALREADY enabled");
        return false;
    }

    if (parameters.getFormat() != AFMT_FLOAT)
    {
        FlatError("Input samples MUST be FLOAT32");
        return false;
    }

    props = parameters.getProperties();
    bits = encodingBits;
    tickTimePeriod = props.tickTimePeriod;

    encoder->init(props, bits);

    FlatMessage("PSY-A audio parameters -> ["
                << "channels: " << props.nChannels << "; "
                << "frames: " << props.bufferFrames << "; "
                << "rate: " << props.sampleRate << "; "
                << "encbits: " << bits << "; "
                << "period: " << props.tickTimePeriod << " s]");

    return true;
}

bool SkPsyAEnc::open(SkDataBuffer &header)
{
    if (enabled)
        return false;

    encoder->start();

    writer->open(header, SkBufferDeviceMode::BVM_ONLYWRITE);

    writer->writeInt8('P');
    writer->writeInt8('S');
    writer->writeInt8('Y');
    writer->writeInt8('A');//signal lives on 1-Dimension

    writer->writeUInt8(bits);
    writer->writeUInt8(props.nChannels);
    writer->writeUInt32(props.bufferFrames);
    writer->writeUInt32(props.sampleRate);
    writer->writeFloat(tickTimePeriod);

    writer->close();

    enabled = true;
    return true;
}

SlotImpl(SkPsyAEnc, encoderStarted)
{
    SilentSlotArgsWarning();
    FlatMessage("Encoder thread STARTED");
    Attach(encoder->getTicker(), encoded, this, loadEncoded, SkQueued);
}

void SkPsyAEnc::close()
{
    if (!enabled)
        return;

    enabled = false;

    encoder->quit();
    encoder->wait();
}

void SkPsyAEnc::setData(void *planarFloatPCM)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return;
    }

    SkPsyAEncPacket *pck = new SkPsyAEncPacket;

    pck->planarBuffer = new float * [props.nChannels];

    for(uint z=0; z<props.nChannels; z++)
    {
        pck->planarBuffer[z] = new float [props.bufferFrames];
        memcpy(pck->planarBuffer[z], &SkArrayCast::toFloat(planarFloatPCM)[z*props.bufferFrames], sizeof(float)*props.bufferFrames);
    }

    encoder->thEventLoop()->invokeSlot(encoder->getTicker()->encode_SLOT, encoder->getTicker(), pck);
}

bool SkPsyAEnc::encode(SkDataBuffer &output)
{
    if (!enabled)
    {
        FlatError("Encoder is NOT enabled yet");
        return false;
    }

    if (oPcks.isEmpty())
        return false;

    SkPsyAEncPacket *pck = oPcks.dequeue();

    if (!pck->encoded.isEmpty())
        output.append(pck->encoded);

    for(uint z=0; z<props.nChannels; z++)
        delete [] pck->planarBuffer[z];

    delete [] pck->planarBuffer;

    delete pck;
    return true;
}

SlotImpl(SkPsyAEnc, loadEncoded)
{
    SilentSlotArgsWarning();

    SkPsyAEncPacket *pck = dynamic_cast<SkPsyAEncPacket *>(referer);
    oPcks.enqueue(pck);
}

float SkPsyAEnc::cpuLoadAvg()
{
    if (!encoder->isRunning())
        return 0.f;

    return jobTimeAvg() / encoder->thEventLoop()->getLastPulseElapsedTimeAverage();
}

float SkPsyAEnc::jobTimeAvg()
{
    if (!encoder->isRunning())
        return 0.f;

    return encoder->thEventLoop()->getConsumedTimeAverage();
}*/

#endif
