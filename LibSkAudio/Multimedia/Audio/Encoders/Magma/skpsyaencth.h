#ifndef SKPSYAENCTH_H
#define SKPSYAENCTH_H

/*#if defined(ENABLE_AUDIO)

#include "Core/System/Thread/skthread.h"
#include "Multimedia/Audio/skaudioparameters.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkBufferDevice;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkPsyAEncPacket extends SkFlatObject
{
    float **planarBuffer;
    float min;
    float minRatio;
    float maxRatio;
    float ratiosInterval;
    SkDataBuffer encoded;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyAEncTicker extends SkObject
{
    public:
        Constructor(SkPsyAEncTicker, SkObject);

        void init(SkAudioProps &properties, uint encodingBits);
        void release();

        Slot(encode);
        Signal(encoded);

    private:
        SkAudioProps props;//PARTIALLY USED
        int bits;
        float max_q;
        float *ratios;
        uint8_t *ratios_q;
        SkBufferDevice *pckWriter;
        ULong pckID;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkPsyAEncTh extends SkThread
{
    public:
        Constructor(SkPsyAEncTh, SkThread);

        void init(SkAudioProps &properties, uint encodingBits);
        SkPsyAEncTicker *getTicker();

    private:
        SkPsyAEncTicker *ticker;
        SkAudioProps props;//PARTIALLY USED
        int bits;

        bool customRunSetup()                                   override;
        void customClosing()                                    override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif*/

#endif // SKPSYAENCTH_H
