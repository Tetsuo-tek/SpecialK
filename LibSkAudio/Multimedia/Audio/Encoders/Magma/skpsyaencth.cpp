#if defined(ENABLE_AUDIO)

/*#include "skpsyaencth.h"

#include "Core/System/skbufferdevice.h"
#include "Core/Containers/skarraycast.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyAEncTicker, SkObject)
{
    bits = 0;
    max_q = 0;

    ratios = nullptr;
    ratios_q = nullptr;

    setObjectName("MagmaAudioEncTicker");

    SlotSet(encode);
    SignalSet(encoded);

    pckWriter = new SkBufferDevice(this);
}

void SkPsyAEncTicker::init(SkAudioProps &properties, uint encodingBits)
{
    props = properties;
    bits = encodingBits;
    max_q = pow(2, bits) - 1;

    ratios = new float [props.bufferFrames];
    memset(ratios, static_cast<int>(0.f), props.bufferFrames);

    ratios_q = new uint8_t[props.bufferFrames];
    memset(ratios_q, 0, props.bufferFrames);
}

void SkPsyAEncTicker::release()
{
    delete [] ratios;
    ratios = nullptr;

    delete [] ratios_q;
    ratios_q = nullptr;
}

SlotImpl(SkPsyAEncTicker, encode)
{
    SilentSlotArgsWarning();*/

    /*
     * EXAMPLE:
     *
     *PCM Input:
     *
     * channels = 2
     * frames   = 1024
     * rate     = 44100
     * period   ~ 0,023
     *
     * FMT            CH  FRAMES     LEN
     *
     * for each chan:
     * float32        2 * 1024  =   8192 B for 0.023 Sec of audio (one PCM buffer)
     * float32    *   2 * 44100 = 352800 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     * PSYA Output with increment ratios quantization on 8 bits
     *
     *  for each chan:
     * (float32   *   5) + 1024 =  1044 B for 0.023 Sec of audio (one PCM buffer)
                                ~ 44526 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     *  total 2 chans:
     *  (all could be also gzipped, reducing another ~ 30 % if we are luckly)
     * (float32   *   5) + 1024 =  2088 B for 0.023 Sec of audio (one PCM buffer)
     *                          ~ 89052 B for 1     Sec of audio (~ 43 PCM buffers)
     *
     * With quantization of 8 bits on ratios, we have a compression ~ 4:1
     */

    /*SkPsyAEncPacket *pck = dynamic_cast<SkPsyAEncPacket *>(referer);
    pckWriter->open(pck->encoded, SkBufferDeviceMode::BVM_ONLYWRITE);

    for(uint z=0; z<props.nChannels; z++)
    {
        float first = pck->planarBuffer[z][0];
        float last = pck->planarBuffer[z][props.bufferFrames-1];

        pck->min = -1.f;

        for(uint i=0; i<props.bufferFrames; i++)
            if (pck->planarBuffer[z][i] < pck->min)
                pck->min = pck->planarBuffer[z][i];

        pck->min -= 0.5;//escaping zeros

        //  0   1   2   3  ..   1022    1023    1024
        //  X   x   x   x  ..   x       x       X
        //      r   r   r               r       r
        //      1   2   3               1023    1024

        float prev = 0.f;

        pck->maxRatio = -2.f;
        pck->minRatio = 2.f;

        //FIRST AND LAST ARE REAL SAMPLEs
        for(uint i=0; i<props.bufferFrames; i++)
        {
            //Signal will be in [0, 2+], instead of, (inside pseudo R+)
            pck->planarBuffer[z][i] += fabs(pck->min);

            AssertKiller(pck->planarBuffer[z][i]<0);

            if (i==0)
            {
                prev = pck->planarBuffer[z][0];
                ratios[i] = 0.f;
                continue;
            }

            //r1 = x0/x1 -> x1 = x0/r1
            ratios[i] = prev / pck->planarBuffer[z][i];

            if (ratios[i] > pck->maxRatio)
                pck->maxRatio = ratios[i];

            else if (ratios[i] < pck->minRatio)
                pck->minRatio = ratios[i];

            prev = pck->planarBuffer[z][i];
        }

        pck->ratiosInterval = pck->maxRatio-pck->minRatio;

        for(uint i=1; i<props.bufferFrames-1; i++)
        {
            float y = ((ratios[i] - pck->minRatio) * max_q) / pck->ratiosInterval;

            if (y >= 255.f)
                ratios_q[i] = 255;

            else
                ratios_q[i] = round(y);

            AssertKiller((int)y>255);
        }

        pckWriter->writeUInt64(pckID++);
        pckWriter->writeFloat(first);
        pckWriter->writeFloat(last);
        pckWriter->writeFloat(pck->min);
        pckWriter->writeFloat(pck->minRatio);
        pckWriter->writeFloat(pck->maxRatio);
        pckWriter->write(SkArrayCast::toCStr(ratios_q), props.bufferFrames);
    }

    pckWriter->close();

    encoded(pck);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkPsyAEncTh, SkThread)
{
    bits = 0;
    ticker = nullptr;
    setObjectName("MagmaAudioEncTh");
}

void SkPsyAEncTh::init(SkAudioProps &properties, uint encodingBits)
{
    props = properties;
    bits = encodingBits;

    setLoopIntervals(props.tickTimePeriod*1000000, eventLoop()->getSlowInterval(), SK_TIMEDLOOP_RT);
}

bool SkPsyAEncTh::customRunSetup()
{
    ticker = new SkPsyAEncTicker;
    ticker->init(props, bits);
    thEventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
    return true;
}

void SkPsyAEncTh::customClosing()
{
    ticker->release();
    ticker = nullptr;
}

SkPsyAEncTicker *SkPsyAEncTh::getTicker()
{
    return ticker;
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
