#ifndef SKPSYADEC_H
#define SKPSYADEC_H

#if defined(ENABLE_AUDIO)

#include "Multimedia/Magma/skabstractmagmadecoder.h"
#include "Multimedia/Audio/skaudioparameters.h"

class SkPsyADec extends SkAbstractMagmaDecoder
{
    public:
        Constructor(SkPsyADec, SkAbstractMagmaDecoder);

        bool open(SkDataBuffer &header)                 override;
        void close()                                    override;

        void setData(SkDataBuffer &input)               override;
        bool decode()                                   override;

        float **pcm();
        uint channels();
        uint frames();
        uint rate();

        bool isLoaded();

    private:
        SkAudioProps props;//PARTIALLY USED
        int bits;
        float max_q;

        float **planarBuffer;
        float *mins;
        float *ratiosMin;
        float *ratiosMax;
        uint8_t **ratios_q;

        bool loaded;
};

#endif

#endif // SKPSYADEC_H
