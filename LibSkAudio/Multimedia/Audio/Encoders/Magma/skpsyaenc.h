#ifndef SKPSYAENC_H
#define SKPSYAENC_H

#if defined(ENABLE_AUDIO)

#include "Multimedia/Magma/skabstractmagmaencoder.h"
#include "Multimedia/Audio/skaudioparameters.h"

class SkPsyAEnc extends SkAbstractMagmaEncoder
{
    SkAudioProps props;//PARTIALLY USED
    int bits;
    float max_q;
    float *ratios;
    uint8_t *ratios_q;
    SkBufferDevice *pckWriter;
    ULong pckID;

    float **planarBuffer;
    float min;
    float minRatio;
    float maxRatio;
    float ratiosInterval;
    SkDataBuffer encoded;

    public:
        Constructor(SkPsyAEnc, SkAbstractMagmaEncoder);

        bool setup(SkAudioParameters &parameters, uint encodingBits);

        bool open(SkDataBuffer &header)                 override;
        void close()                                    override;

        void setData(void *planarFloatPCM)              override;
        bool encode(SkDataBuffer &output)               override;
};

#endif

/*#include "skpsyaencth.h"

class SkPsyAEnc extends SkAbstractMagmaEncoder
{
    public:
        Constructor(SkPsyAEnc, SkAbstractMagmaEncoder);

        bool setup(SkAudioParameters &parameters, uint encodingBits);

        bool open(SkDataBuffer &header)                 override;
        void close()                                    override;

        void setData(void *planarFloatPCM)              override;
        bool encode(SkDataBuffer &output)               override;

        float cpuLoadAvg();
        float jobTimeAvg();

        Slot(encoderStarted);
        Slot(loadEncoded);

    private:
        SkPsyAEncTh *encoder;

        int bits;
        SkAudioProps props;//PARTIALLY USED

        SkQueue<SkPsyAEncPacket *> oPcks;
};

#endif*/

#endif // SKPSYAENC_H
