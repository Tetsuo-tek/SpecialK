/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKOGGPACKETIZER_H
#define SKOGGPACKETIZER_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_AUDIOENCLIBS)

#include <ogg/ogg.h>

#include "Multimedia/Audio/skaudiobuffer.h"

class SKAUDIO SkOggPacketizer extends SkFlatObject
{
    public:
        SkOggPacketizer();
        ~SkOggPacketizer();

        void setAudioParameters(SkAudioParameters &tempAudioParameters);

        SkRingBuffer *getInputProduction();
        SkRingBuffer *getOutputProduction();

        bool hasHeader();
        CStr *getHeaderArray(uint64_t &arraySize);

        virtual void tick(){}
        virtual void close(){}

    protected:
        SkAudioParameters audioParameters;

        SkObject *signalsParent;
        SkRingBuffer *inputProduction;
        SkRingBuffer *outputProduction;

        int b_o_s;

        char *headerArray;
        long headerSize;

        ogg_stream_state os; //take physical pages, weld into a logicalstream of packets
        ogg_page         og; //one Ogg bitstream page.  Vorbis packets are inside
        ogg_packet       op; //one raw packet of data for decode

        void initializePacking();
        void flushHeader();
        void pageOut();
        void closePacking();

        void prepareEncodedPacket(unsigned char *buffer,
                                  long size,
                                  ogg_int64_t granulePos,
                                  ogg_int64_t packetno);
};

#endif

#endif // SKOGGPACKETIZER_H
