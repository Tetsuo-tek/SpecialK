#if defined(ENABLE_AUDIO)

#include "skaudioquantizer.h"
#include "Core/System/Thread/skmutexlocker.h"
#include <Core/Containers/skarraycast.h>

SkAudioQuantizer::SkAudioQuantizer()
{
    planarDoubleBuffer = nullptr;

    for(uint64_t i=static_cast<uint64_t>(SkAudioFmtType::AFMT_CHAR); i<static_cast<uint64_t>(SkAudioFmtType::AFMT_DOUBLE); i++)
        conversionFactor(static_cast<SkAudioFmtType>(i), &factors[i]);
}

void SkAudioQuantizer::conversionFactor(SkAudioFmtType fmt, SkAudioConversionFactor *factor)
{
    factor->isUnsigned = (fmt == AFMT_UCHAR || fmt == AFMT_USHORT || fmt == AFMT_UINT);

    //

    if (fmt == SkAudioFmtType::AFMT_CHAR || fmt == SkAudioFmtType::AFMT_UCHAR)
        factor->bytesPerSample = FMTSIZE_CHAR;

    else if (fmt == SkAudioFmtType::AFMT_SHORT || fmt == SkAudioFmtType::AFMT_USHORT)
        factor->bytesPerSample = FMTSIZE_SHORT;

    else if (fmt == SkAudioFmtType::AFMT_INT || fmt == SkAudioFmtType::AFMT_UINT)
        factor->bytesPerSample = FMTSIZE_INT;

    else if (fmt == SkAudioFmtType::AFMT_FLOAT)
        factor->bytesPerSample = FMTSIZE_FLOAT;

    else if (fmt == SkAudioFmtType::AFMT_DOUBLE)
        factor->bytesPerSample = FMTSIZE_DOUBLE;

    factor->max = pow(2, (factor->bytesPerSample * 8)) - 1;
    factor->k = 2.0 / factor->max;

    if (factor->isUnsigned)
        factor->zero = (factor->max/2);

    else
        factor->zero = 0;
}

void SkAudioQuantizer::setup(SkAudioParameters &tempInputAudioParameters,
                            double */*tempInterleavedDoubleBuffer*/,
                            double **tempPlanarDoubleBuffer)
{
    audioParameters = tempInputAudioParameters;
    totalBufferSamples = audioParameters.getBufferSamples();
    planarDoubleBuffer = tempPlanarDoubleBuffer;
}

double **SkAudioQuantizer::getPlanarRawBufferData()
{
    return planarDoubleBuffer;
}

double *SkAudioQuantizer::getPlanarRawBufferData(uint channelIDX)
{
    if (channelIDX < audioParameters.getChannels())
        return planarDoubleBuffer[channelIDX];

    return nullptr;
}

void SkAudioQuantizer::convertSampleTypeTo(SkAudioParameters *tempAudioParameters, char *data)
{
    SkAudioFmtType fmt = tempAudioParameters->getFormat();

    if (fmt == AFMT_CHAR)
        convertSampleTypeToData_CHAR(tempAudioParameters, data);

    else if (fmt == AFMT_UCHAR)
        convertSampleTypeToData_UCHAR(tempAudioParameters, data);

    else if (fmt == AFMT_SHORT)
        convertSampleTypeToData_SHORT(tempAudioParameters, data);

    else if (fmt == AFMT_USHORT)
        convertSampleTypeToData_USHORT(tempAudioParameters, data);

    else if (fmt == AFMT_INT)
        convertSampleTypeToData_INT(tempAudioParameters, data);

    else if (fmt == AFMT_UINT)
        convertSampleTypeToData_UINT(tempAudioParameters, data);

    else if (fmt == AFMT_FLOAT)
        convertSampleTypeToData_FLOAT(tempAudioParameters, data);

    else if (fmt == AFMT_DOUBLE)
        convertSampleTypeToData_DOUBLE(tempAudioParameters, data);
}

void SkAudioQuantizer::internalClip(double &inputValue)
{
    if (inputValue > 1)
        inputValue = 1;

    else if (inputValue < -1)
        inputValue = -1;
}

#define CONVERTSAMPLETO(FMTSTRTYPE, FMTCTYPE, FMTARRCAST) \
    convertSampleTypeToData_ ## FMTSTRTYPE (SkAudioParameters *tempAudioParameters, char *outputBuffer) \
    {\
        SkMutexLocker locker(&planarDoubleBufferEx); \
        \
        double inputValue = 0; \
        uint channels = tempAudioParameters->getChannels(); \
        uint bufferFrames = tempAudioParameters->getBufferFrames(); \
        \
        if (tempAudioParameters->isPlanar()) \
        { \
            for(uint z=0; z<channels; z++) \
                for(uint i=0; i<bufferFrames; i++) \
                { \
                    inputValue = planarDoubleBuffer[z][i]; \
                    internalClip(inputValue); \
                    SkArrayCast::FMTARRCAST(outputBuffer)[i+(z*bufferFrames)] = static_cast<FMTCTYPE>((inputValue/factors[SkAudioFmtType::AFMT_ ## FMTSTRTYPE].k)+factors[SkAudioFmtType::AFMT_ ## FMTSTRTYPE].zero); \
                } \
        } \
        \
        else \
        { \
            for(uint i=0; i<bufferFrames; i++) \
                for(uint z=0; z<channels; z++) \
                { \
                    inputValue = planarDoubleBuffer[z][i]; \
                    internalClip(inputValue); \
                    SkArrayCast::FMTARRCAST(outputBuffer)[z+(i*channels)] = static_cast<FMTCTYPE>((inputValue/factors[SkAudioFmtType::AFMT_ ## FMTSTRTYPE].k)+factors[SkAudioFmtType::AFMT_ ## FMTSTRTYPE].zero); \
                } \
        } \
        \
        return outputBuffer; \
    }

char *SkAudioQuantizer::CONVERTSAMPLETO(CHAR,   int8_t,     toInt8)
char *SkAudioQuantizer::CONVERTSAMPLETO(UCHAR,  uint8_t,    toUInt8)
char *SkAudioQuantizer::CONVERTSAMPLETO(SHORT,  int16_t,    toInt16)
char *SkAudioQuantizer::CONVERTSAMPLETO(USHORT, uint16_t,   toUInt16)
char *SkAudioQuantizer::CONVERTSAMPLETO(INT,    int32_t,    toInt32)
char *SkAudioQuantizer::CONVERTSAMPLETO(UINT,   uint32_t,   toUInt32)

char *SkAudioQuantizer::convertSampleTypeToData_FLOAT(SkAudioParameters *tempAudioParameters, char *outputBuffer)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    uint channels = tempAudioParameters->getChannels();
    uint bufferFrames = tempAudioParameters->getBufferFrames();

    if (tempAudioParameters->isPlanar())
    {
        for(uint z=0; z<channels; z++)
            for(uint i=0; i<bufferFrames; i++)
                SkArrayCast::toFloat(outputBuffer)[i+(z*bufferFrames)] = static_cast<float>(planarDoubleBuffer[z][i]);
    }

    else
    {
        for(uint i=0; i<bufferFrames; i++)
            for(uint z=0; z<channels; z++)
                SkArrayCast::toFloat(outputBuffer)[z+(i*channels)] = static_cast<float>(planarDoubleBuffer[z][i]);
    }

    return outputBuffer;
}

char *SkAudioQuantizer::convertSampleTypeToData_DOUBLE(SkAudioParameters *tempAudioParameters, char *outputBuffer)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    uint channels = tempAudioParameters->getChannels();
    uint bufferFrames = tempAudioParameters->getBufferFrames();

    if (tempAudioParameters->isPlanar())
    {
        uint64_t sz = tempAudioParameters->getCanonicalSize() / channels;

        for(uint z=0; z<channels; z++)
            memcpy(&outputBuffer[z*sz], planarDoubleBuffer[z], sz);
    }

    else
    {
        for(uint i=0; i<bufferFrames; i++)
            for(uint z=0; z<channels; z++)
                SkArrayCast::toDouble(outputBuffer)[z+(i*channels)] = planarDoubleBuffer[z][i];
    }

    return outputBuffer;
}

void SkAudioQuantizer::convertToMono(double *outputBuffer)
{
    SkMutexLocker locker(&planarDoubleBufferEx);

    uint channels = audioParameters.getChannels();
    uint bufferFrames = audioParameters.getBufferFrames();

    if (channels == 1)
    {
        memcpy(outputBuffer, planarDoubleBuffer[0], bufferFrames * sizeof(double));
        return;
    }

    double val;

    for(uint i=0; i<bufferFrames; i++)
    {
        val = 0;

        for(uint z=0; z<channels; z++)
            val += planarDoubleBuffer[z][i];

        outputBuffer[i] = val/channels;
    }
}
#define PLANARTOINTERLEAVE(FMTSTRTYPE, FMTCTYPE) \
    planarToInterleaved_ ## FMTSTRTYPE (uint channels, uint bufferFrames, const FMTCTYPE *planar, FMTCTYPE *interleaved) \
    {\
        for(uint i=0; i<bufferFrames; i++) \
            for(uint z=0; z<channels; z++) \
                interleaved[z+(i*channels)] = planar[i+(z*bufferFrames)]; \
    }

void SkAudioQuantizer::PLANARTOINTERLEAVE(DOUBLE,   double)
void SkAudioQuantizer::PLANARTOINTERLEAVE(FLOAT,    float)
void SkAudioQuantizer::PLANARTOINTERLEAVE(INT,      Int)
void SkAudioQuantizer::PLANARTOINTERLEAVE(SHORT,    Short)
void SkAudioQuantizer::PLANARTOINTERLEAVE(CHAR,     char)

void SkAudioQuantizer::bitConversion(char *inputBuffer, uint inputIDX, uint channelIDX, uint sampleIDX)
{
    SkAudioFmtType fmt = audioParameters.getFormat();

    if (fmt == AFMT_CHAR || fmt == AFMT_UCHAR)
        planarDoubleBuffer[channelIDX][sampleIDX] = SkArrayCast::toInt8(inputBuffer)[inputIDX] * factors[fmt].k;

    else if (fmt == AFMT_SHORT || fmt == AFMT_USHORT)
        planarDoubleBuffer[channelIDX][sampleIDX] = SkArrayCast::toInt16(inputBuffer)[inputIDX] * factors[fmt].k;

    else if (fmt == AFMT_INT || fmt == AFMT_UINT)
        planarDoubleBuffer[channelIDX][sampleIDX] = SkArrayCast::toInt32(inputBuffer)[inputIDX] * factors[fmt].k;

    else if (fmt == AFMT_FLOAT)
        planarDoubleBuffer[channelIDX][sampleIDX] = static_cast<double>(SkArrayCast::toFloat(inputBuffer)[inputIDX]);

    else if (fmt == AFMT_DOUBLE)
        planarDoubleBuffer[channelIDX][sampleIDX] = SkArrayCast::toDouble(inputBuffer)[inputIDX];

}

std::mutex &SkAudioQuantizer::getBufferEx()
{
    return planarDoubleBufferEx;
}

#endif
