#if defined(ENABLE_AUDIO) && defined(ENABLE_PORTAUDIOLIB)

#include "skpastream.h"
#include "skportaudio.h"
#include <Core/Containers/skarraycast.h>

// // // // // // // // // // // // // // // // // // // // // // // // // // //

int audioCallback(CVoid *inputBuffer,
                  void *outputBuffer,
                  unsigned long ,//framesPerBuffer,
                  const PaStreamCallbackTimeInfo *,//timeInfo,
                  PaStreamCallbackFlags ,//statusFlags,
                  void *productionUserData)
{
    SkAudioProduction *production = static_cast<SkAudioProduction *>(productionUserData);

#if defined(__linux__)
    if (!production->thNamed)
    {
        production->thNamed = true;

        SkString name = production->driver->objectName();

        if (production->inputParamsPA && production->outputParamsPA)
            name.append(".IO");

        else if (production->inputParamsPA)
            name.append(".IN");

        else if (production->outputParamsPA)
            name.append(".OUT");

        pthread_setname_np(pthread_self(), name.c_str());
    }
#endif

   // production->bufferSize = production->frameSize * framesPerBuffer;

    /*cout << "AudioCB -> Fr: " << framesPerBuffer
         << "; Sz: " << production->bufferSize
         << "; T: " << timeInfo->currentTime << " s"
         << "; TADC: " << timeInfo->inputBufferAdcTime << " s"
         << "; ST: " << statusFlags
         << "\n";*/

    if (production->inputParamsPA)
    {
        production->inputProduction->addData((CStr *) inputBuffer, production->bufferSize);
        production->driver->dataLoaded();
    }

    if (production->outputParamsPA)
    {
        if (production->inputParamsPA)
            memcpy(outputBuffer, inputBuffer, production->bufferSize);

        else
        {
            //cout << "!!!! " << production->outputProduction->size() << " " << production->bufferSize << "\n";
            if (production->outputProduction->size() < production->bufferSize)
            {
                SkAudioParameters *p = production->outputAudioParameters;
                SkAudioFmtType fmt = p->getFormat();

                if (fmt == SkAudioFmtType::AFMT_CHAR
                        || fmt == SkAudioFmtType::AFMT_UCHAR
                        || fmt == SkAudioFmtType::AFMT_SHORT
                        || fmt == SkAudioFmtType::AFMT_INT)
                {
                    memset(outputBuffer, 0, production->bufferSize);
                }

                else if (fmt == SkAudioFmtType::AFMT_FLOAT)
                {
                    for(uint i=0; i<p->getBufferSamples(); i++)
                        SkArrayCast::toFloat(outputBuffer)[i] = 0.f;
                }

                if (production->outputProduction->isEmpty())
                    FlatDebug_EXT(production->outputProduction,
                                  "PortAudio UNDER-RUN; it requires "
                                  << production->bufferSize << " B"
                                  << ", but production is empty!");
                else
                    FlatDebug_EXT(production->outputProduction,
                                  "PortAudio UNDER-RUN; it requires "
                                  << production->bufferSize << " B"
                                  << ", but production has only " << production->outputProduction->size() << " B");

                return paContinue;
            }

            production->outputProduction->getCustomBuffer((char *) outputBuffer, production->bufferSize);
            production->driver->dataGotten();
        }
    }

    return paContinue;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkAudioError)
{
    SetupEnumWrapper(SkAudioError);

    SetEnumItem(SkAudioError::NoError);
    SetEnumItem(SkAudioError::SoundSystemAlreadyStarted);
    SetEnumItem(SkAudioError::SoundSystemAlreadyStopped);
    SetEnumItem(SkAudioError::DeviceNotInitialized);
    SetEnumItem(SkAudioError::DeviceNotFound);
    SetEnumItem(SkAudioError::StreamAlreadyStarted);
    SetEnumItem(SkAudioError::StreamAlreadyStopped);
    SetEnumItem(SkAudioError::DuplexIoFramesNotMatch);
    SetEnumItem(SkAudioError::DuplexIoFormatsNotMatch);
    SetEnumItem(SkAudioError::DuplexIoRatesNotMatch);
    SetEnumItem(SkAudioError::FormatNotSupported);
    SetEnumItem(SkAudioError::InputChannelsNotValid);
    SetEnumItem(SkAudioError::OutputChannelsNotValid);
    SetEnumItem(SkAudioError::InternalSoundSystemError);
}

DeclareWrapper_ENUM(SkAudioMode)
{
    SetupEnumWrapper(SkAudioMode);

    SetEnumItem(SkAudioMode::NullMode);
    SetEnumItem(SkAudioMode::Capture);
    SetEnumItem(SkAudioMode::Playback);
    SetEnumItem(SkAudioMode::Duplex);
}

DeclareMeth_INSTANCE_RET(SkPaStream, rec, bool, Arg_CStr,  *Arg_Custom(SkAudioParameters))
DeclareMeth_INSTANCE_RET(SkPaStream, play, bool, Arg_CStr,  *Arg_Custom(SkAudioParameters))
DeclareMeth_INSTANCE_RET(SkPaStream, start, bool, Arg_CStr,  *Arg_Custom(SkAudioParameters), *Arg_Custom(SkAudioParameters))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkPaStream, start, 1, bool, Arg_CStr,  *Arg_Custom(SkAudioParameters), *Arg_Custom(SkAudioParameters), Arg_Enum(SkAudioMode))
DeclareMeth_INSTANCE_VOID(SkPaStream, stop)
DeclareMeth_INSTANCE_RET(SkPaStream, isStreamActive, bool)
DeclareMeth_INSTANCE_RET(SkPaStream, getDeviceInfo, const PaDeviceInfo*)
DeclareMeth_INSTANCE_RET(SkPaStream, getDeviceName, CStr*)
DeclareMeth_INSTANCE_RET(SkPaStream, getMode, SkAudioMode)
DeclareMeth_INSTANCE_RET(SkPaStream, getModeName, CStr*)
//DeclareMeth_INSTANCE_RET(SkPaStream, getProduction, SkAudioProduction&)
DeclareMeth_INSTANCE_RET(SkPaStream, getStreamMode, SkAudioMode)
DeclareMeth_INSTANCE_RET(SkPaStream, getLastStreamCpuLoad, double)
DeclareMeth_INSTANCE_RET(SkPaStream, getLastError, SkAudioError)
DeclareMeth_INSTANCE_RET(SkPaStream, getLastErrorString, CStr*)

ConstructorImpl(SkPaStream, SkObject,
                {
                    AddEnumType(SkAudioError);
                    AddEnumType(SkAudioMode);

                    AddMeth_INSTANCE_RET(SkPaStream, rec);
                    AddMeth_INSTANCE_RET(SkPaStream, play);
                    AddMeth_INSTANCE_RET(SkPaStream, start);
                    AddMeth_INSTANCE_RET_OVERLOAD(SkPaStream, start, 1);
                    AddMeth_INSTANCE_VOID(SkPaStream, stop);
                    AddMeth_INSTANCE_RET(SkPaStream, isStreamActive);
                    AddMeth_INSTANCE_RET(SkPaStream, getDeviceInfo);
                    AddMeth_INSTANCE_RET(SkPaStream, getDeviceName);
                    AddMeth_INSTANCE_RET(SkPaStream, getMode);
                    AddMeth_INSTANCE_RET(SkPaStream, getModeName);
                    AddMeth_INSTANCE_RET(SkPaStream, getStreamMode);
                    AddMeth_INSTANCE_RET(SkPaStream, getLastStreamCpuLoad);
                    AddMeth_INSTANCE_RET(SkPaStream, getLastError);
                    AddMeth_INSTANCE_RET(SkPaStream, getLastErrorString);
                })
{
    SignalSet(started);
    SignalSet(stopped);
    SignalSet(errorNotify);

    SignalSet(dataLoaded);
    SignalSet(dataGotten);

    resetAudioStream();
}

bool SkPaStream::rec(CStr *name, SkAudioParameters &parameters)
{
    SkAudioParameters nullParameters;
    return start(name, parameters, nullParameters, SkAudioMode::Capture);
}

bool SkPaStream::play(CStr *name, SkAudioParameters &parameters)
{
    SkAudioParameters nullParameters;
    return start(name, nullParameters, parameters, SkAudioMode::Playback);
}

bool SkPaStream::start(CStr *name,
                       SkAudioParameters &inputParameters,
                       SkAudioParameters &outputParameters,
                       SkAudioMode audioMode)
{
    if (isStreamActive())
    {
        lastError = SkAudioError::StreamAlreadyStarted;
        errorNotify();
        ObjectError("Cannot start; device is ALREADY enabled");
        return false;
    }

    //CHECK NAME

    if (!devicesNames.contains(name))
    {
        lastError = SkAudioError::DeviceNotFound;
        errorNotify();
        ObjectError("Cannot start; device NOT found [" << name << "]");
        return false;
    }

    deviceID = devicesNames.indexOf(name);
    ObjectMessage("Selected DevID: " << deviceID);

    deviceName = name;
    deviceInfo = devices.at(deviceID);
    mode = audioMode;
    inputAudioParameters = inputParameters;
    outputAudioParameters = outputParameters;

    if (!validate())
        return false;

    if (!setupChannels())
        return false;

    setupParameters();

    if (!openAudioStream())
        return false;

    ObjectMessage(getModeName() << " on AudioDevice [name: " << deviceName <<  "]");
    return true;
}

void SkPaStream::stop()
{
    if (!isStreamActive())
    {
        lastError = SkAudioError::StreamAlreadyStopped;
        errorNotify();
        ObjectError("Cannot stop; device is ALREADY disabled");
        return;
    }

    ObjectDebug("Closing SkPaStream ..");
    closeAudioStream();

    /*production.inputProduction->clear();
    production.outputProduction->clear();*/
    production.thNamed = false;

    ObjectMessage("SkPaStream STOPPED [name: " << deviceName << "]");
    resetAudioStream();
}

bool SkPaStream::isStreamActive()
{
    /*int e = Pa_IsStreamActive(stream);

    if (stream && e < 0 && e != Pa)
        ObjectError("ERROR checking 'isStreamActive()'; [" << e << "] " << getLastErrorString());

    return (e>0);*/
    return (stream != nullptr);
}

bool SkPaStream::validate()
{
    SkAudioFmtType fmt = SkAudioFmtType::AFMT_NULL;
    uint64_t fmtSize = 0;
    unsigned int frames = 0;
    unsigned int chans = 0;

    if (mode == SkAudioMode::Duplex)
    {
        if (inputAudioParameters.getBufferFrames() != outputAudioParameters.getBufferFrames())
        {
            lastError = SkAudioError::DuplexIoFramesNotMatch;
            ObjectError("Cannot start Duplex; inputFrames MUST be the same for output");
            errorNotify();
            return false;
        }

        if (inputAudioParameters.getFormat() != outputAudioParameters.getFormat())
        {
            lastError = SkAudioError::DuplexIoFormatsNotMatch;
            ObjectError("Cannot start Duplex; FMT for input and autput differs");
            errorNotify();
            return false;
        }

        if (inputAudioParameters.getSampleRate() != outputAudioParameters.getSampleRate())
        {
            lastError = SkAudioError::DuplexIoRatesNotMatch;
            ObjectError("Cannot start Resamplig-Duplex; it is NOT supported");
            errorNotify();
            return false;
        }

        production.inputParamsPA = &inputParamsPA;
        production.inputAudioParameters = &inputAudioParameters;

        production.outputParamsPA = &outputParamsPA;
        production.outputAudioParameters = &outputAudioParameters;

        fmt = outputAudioParameters.getFormat();
        frames = outputAudioParameters.getBufferFrames();
        chans = outputAudioParameters.getChannels();
    }

    else
    {
        if (mode == SkAudioMode::Capture)
        {
            production.outputParamsPA = nullptr;
            production.inputParamsPA = &inputParamsPA;
            production.inputAudioParameters = &inputAudioParameters;

            fmt = inputAudioParameters.getFormat();
            frames = inputAudioParameters.getBufferFrames();
            chans = inputAudioParameters.getChannels();
        }

        else if (mode == SkAudioMode::Playback)
        {
            production.inputParamsPA = nullptr;
            production.outputParamsPA = &outputParamsPA;
            production.outputAudioParameters = &outputAudioParameters;

            fmt = outputAudioParameters.getFormat();
            frames = outputAudioParameters.getBufferFrames();
            chans = outputAudioParameters.getChannels();
        }
    }

    if (fmt == SkAudioFmtType::AFMT_CHAR)
    {
        fmtSize = FMTSIZE_CHAR;
        audioWord = paInt8;
    }

    else if (fmt == SkAudioFmtType::AFMT_UCHAR)
    {
        fmtSize = FMTSIZE_UCHAR;
        audioWord = paUInt8;
    }

    else if (fmt == SkAudioFmtType::AFMT_SHORT)
    {
        fmtSize = FMTSIZE_SHORT;
        audioWord = paInt16;
    }

    else if (fmt == SkAudioFmtType::AFMT_INT)
    {
        fmtSize = FMTSIZE_INT;
        audioWord = paInt32;
    }

    else if (fmt == SkAudioFmtType::AFMT_FLOAT)
    {
        fmtSize = FMTSIZE_FLOAT;
        audioWord = paFloat32;
    }

    else
    {
        lastError = SkAudioError::FormatNotSupported;
        errorNotify();

        audioWord = 0;
        ObjectError("Format NOT supported (Allowed formats are: FMT_CHAR, FMT_UCHAR, FMT_SHORT, FMT_FLOAT)");
        return false;
    }

    production.frameSize = chans * fmtSize;
    production.bufferSize = frames * production.frameSize;

    ObjectMessage("Validate [OK] - CH: " << chans
              << "; FR: " << frames
              << "; FMTSZ: " << fmtSize
              << "; BUFFSZ " << production.bufferSize << " B");

    return true;
}

bool SkPaStream::setupChannels()
{
    if (mode != SkAudioMode::Playback
                && inputAudioParameters.getChannels() > (unsigned int) deviceInfo->maxInputChannels)
    {
        lastError = SkAudioError::InputChannelsNotValid;
        errorNotify();

        ObjectError("Input channels NOT valid "
                  << inputAudioParameters.getChannels() << " -> "); printDevInfo(deviceID);

        return false;
    }

    //REPEATING "IF" HERE WILL INCLUDE ALSO DUPLEX IN THE CHOICE

    if (mode != SkAudioMode::Capture
             && outputAudioParameters.getChannels() > (unsigned int) deviceInfo->maxOutputChannels)
    {
        lastError = SkAudioError::OutputChannelsNotValid;
        errorNotify();

        ObjectError("Output channels NOT valid "
                    << outputAudioParameters.getChannels() << " -> "); printDevInfo(deviceID);

        return false;
    }

    ObjectMessage("Channels [OK]");
    return true;
}

void SkPaStream::setupParameters()
{
    if (mode != SkAudioMode::Playback)
    {
        inputParamsPA.device = deviceID;
        inputParamsPA.channelCount = inputAudioParameters.getChannels();
        inputParamsPA.sampleFormat = audioWord;
        inputParamsPA.suggestedLatency = deviceInfo->defaultLowInputLatency;
        inputParamsPA.hostApiSpecificStreamInfo = nullptr;
    }

    //REPEATING "IF" HERE WILL INCLUDE ALSO DUPLEX IN THE CHOICE

    if (mode != SkAudioMode::Capture)
    {
        outputParamsPA.device = deviceID;
        outputParamsPA.channelCount = outputAudioParameters.getChannels();
        outputParamsPA.sampleFormat = audioWord;
        outputParamsPA.suggestedLatency = deviceInfo->defaultLowOutputLatency;
        outputParamsPA.hostApiSpecificStreamInfo = nullptr;
    }
}

bool SkPaStream::openAudioStream()
{
    unsigned int sampleRate = 0;
    unsigned int bufferFrames = 0;

    lastPaError = paNoError;

    if (mode == SkAudioMode::Playback || mode == SkAudioMode::Duplex)
    {
        sampleRate = outputAudioParameters.getSampleRate();
        bufferFrames = outputAudioParameters.getBufferFrames();

        if (mode == SkAudioMode::Playback)
            ObjectDebug("Opening Playback stream [sr: " << sampleRate << " Hz; fr: " << bufferFrames << "] ...");

        else if (mode == SkAudioMode::Duplex)
            ObjectDebug("Opening Duplex stream [sr: " << sampleRate << " Hz; fr: " << bufferFrames << "] ...");

    }

    else
    {
        sampleRate = inputAudioParameters.getSampleRate();
        bufferFrames = inputAudioParameters.getBufferFrames();
        ObjectDebug("Opening Capture stream [sr: " << sampleRate << " Hz; fr: " << bufferFrames << "] ...");
    }

    //FOR NOW "paFramesPerBufferUnspecified" IS NOT SUPPORTED HERE
    //if (audioParameters.bufferFrames == 0)
    //  audioParameters.bufferFrames = paFramesPerBufferUnspecified;

    lastPaError = Pa_OpenStream(&stream,
                                production.inputParamsPA,
                                production.outputParamsPA,
                                sampleRate,
                                bufferFrames,
                                paNoFlag,
                                audioCallback,
                                &production);

    if (lastPaError != paNoError)
    {
        lastError = SkAudioError::InternalSoundSystemError;

        ObjectError("Cannot open stream; " << getLastErrorString());
        errorNotify();

        return false;
    }

    lastPaError = Pa_StartStream(stream);

    if (lastPaError != paNoError)
    {
        lastError = SkAudioError::InternalSoundSystemError;

        ObjectError("Cannot start stream; " << getLastErrorString());
        errorNotify();

        return false;
    }

    started();
    return true;
}

bool SkPaStream::closeAudioStream()
{
    if (!isStreamActive())
        return true;//IT IS ALREADY CLOSED ... NO ERROR

    lastPaError = Pa_CloseStream(stream);

    if (lastPaError != paNoError)
    {
        lastError = SkAudioError::InternalSoundSystemError;

        ObjectError("Cannot close stream; " << getLastErrorString());
        errorNotify();

        return false;
    }

    stream = nullptr;

    stopped();
    return true;
}

void SkPaStream::resetAudioStream()
{
    /*if (isStreamActive())
    {
        //NOT NOTIFIED BECAUSE IS PRIVATE
        ObjectError("Cannot RESET stream wheis it is ACTIVE");
        return;
    }*/

    if (production.inputProduction)
    {
        production.inputProduction->clear();
        production.outputProduction->clear();
    }

    deviceID = -1;
    deviceName.clear();
    deviceInfo = nullptr;
    mode = SkAudioMode::NullMode;
    audioWord = 0;
    inputAudioParameters.clear();
    inputParamsPA = PaStreamParameters();
    outputAudioParameters.clear();
    outputParamsPA = PaStreamParameters();

    stream = nullptr;
    lastPaError = paNoError;
    lastError = SkAudioError::NoError;
}

const PaDeviceInfo *SkPaStream::getDeviceInfo()
{
    return deviceInfo;
}

CStr *SkPaStream::getDeviceName()
{
    return deviceName.c_str();
}

SkAudioMode SkPaStream::getMode()
{
    return mode;
}

CStr *SkPaStream::getModeName()
{
    if (mode == SkAudioMode::Capture)
        return "CAPTURE";

    else if (mode == SkAudioMode::Playback)
        return "PLAYBACK";

    else if (mode == SkAudioMode::Duplex)
        return "DUPLEX";

    return "NULL";
}

SkAudioProduction &SkPaStream::getProduction()
{
    return production;
}

SkAudioMode SkPaStream::getStreamMode()
{
    return mode;
}

double SkPaStream::getLastStreamCpuLoad()
{
    return Pa_GetStreamCpuLoad(stream);
}

SkAudioError SkPaStream::getLastError()
{
    return lastError;
}

CStr *SkPaStream::getLastErrorString()
{
    if (lastError == SkAudioError::SoundSystemAlreadyStarted)
        return "Device NOT initialized";

    else if (lastError == SkAudioError::SoundSystemAlreadyStopped)
        return "Device NOT initialized";

    else if (lastError == SkAudioError::DeviceNotInitialized)
        return "Device NOT initialized";

    else if (lastError == SkAudioError::DeviceNotFound)
        return "Device NOT found";

    else if (lastError == SkAudioError::StreamAlreadyStarted)
        return "Stream was ALREADY started";

    else if (lastError == SkAudioError::StreamAlreadyStopped)
        return "Stream was ALREADY stopped";

    else if (lastError == SkAudioError::DuplexIoFramesNotMatch)
        return "Duplex IO frames does NOT match";

    else if (lastError == SkAudioError::DuplexIoFormatsNotMatch)
        return "Duplex IO formats does NOT match";

    else if (lastError ==SkAudioError:: DuplexIoRatesNotMatch)
        return "Duplex IO sample-rates does NOT match";

    else if (lastError == SkAudioError::FormatNotSupported)
        return "Format is NOT supported";

    else if (lastError == SkAudioError::InputChannelsNotValid)
        return "Input channels are NOT valid";

    else if (lastError == SkAudioError::OutputChannelsNotValid)
        return "Output channels are NOT valid";

    else if (lastError == SkAudioError::InternalSoundSystemError)
        return Pa_GetErrorText(lastPaError);

    return "No error";
}

void SkPaStream::printDevInfo(uint64_t id)
{
    const PaDeviceInfo *info = devices.at(id);

    cout << "Device: " << info->name << "\n"
         << " - DevID: " << id << "\n"
         << " - MaxInput: " << info->maxInputChannels << "\n"
         << " - MaxOutput: " << info->maxOutputChannels << "\n"
         << " - Default LOW InputLatency: " << info->defaultLowInputLatency << " s\n"
         << " - Default LOW OutputLatency: " << info->defaultLowOutputLatency << " s\n"
         << " - Default HIGH InputLatency: " << info->defaultHighInputLatency << " s\n"
         << " - Default HIGH OutputLatency: " << info->defaultHighOutputLatency << " s\n";
}

#endif
