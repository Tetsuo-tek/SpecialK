#if defined(ENABLE_AUDIO) && defined(ENABLE_PORTAUDIOLIB)

#include "skportaudio.h"

void SkPortAudioClose(SkObject *obj)
{
    SkPortAudio *f = static_cast<SkPortAudio *>(obj);
    ObjectDebug_EXT(f, "exec dtorCompanion -> SkPortAudioClose()");

    if (f->isInitialized())
        f->close();
}

ConstructorImpl(SkPortAudio, SkPaStream)
{
    portAudioInitialized = false;

    standardRates.append(2000);
    standardRates.append(4000);
    standardRates.append(8000);
    standardRates.append(9600);
    standardRates.append(11025);
    standardRates.append(12000);
    standardRates.append(16000);
    standardRates.append(22050);
    standardRates.append(24000);
    standardRates.append(32000);
    standardRates.append(37800);
    standardRates.append(44100);
    standardRates.append(48000);
    standardRates.append(64000);
    standardRates.append(88200);
    standardRates.append(96000);
    standardRates.append(192000);

    production.driver = this;

    addDtorCompanion(SkPortAudioClose);
}

bool SkPortAudio::init()
{
    if(!initializePA())
        return false;

    refreshDevices();
    return true;
}

bool SkPortAudio::isInitialized()
{
    return portAudioInitialized;
}

bool SkPortAudio::close()
{
    if (!portAudioInitialized)
    {
        ObjectError("Cannot close; SkPortAudio is NOT initialized");
        return false;
    }

    ObjectDebug("Closing SkPortAudio ..");

    if (isStreamActive())
        stop();

    closePA();
    ObjectMessage("SkPortAudio CLOSED");
    return true;
}

bool SkPortAudio::refreshDevices()
{
    if (!portAudioInitialized)
    {
        lastError = SkAudioError::DeviceNotInitialized;
        onError();
        ObjectError("Cannot refresh devices when SkPortAudio is NOT initialized");
        return false;
    }

    devices.clear();
    devicesNames.clear();
    int devCountHolder = Pa_GetDeviceCount();

    for (int i=0; i<devCountHolder; i++)
    {
        const PaDeviceInfo *info = Pa_GetDeviceInfo(i);
        devices.append(info);
        devicesNames.append(info->name);

        //printDevInfo(i);
    }

    return true;
}

SkStringList &SkPortAudio::getDevicesNames()
{
    return devicesNames;
}

SkVector<const PaDeviceInfo *> &SkPortAudio::getDevices()
{
    return devices;
}

const PaDeviceInfo *SkPortAudio::getDevice(uint64_t index)
{
    if (index < devices.count())
        return devices.at(index);

    return nullptr;
}

SkVector<uint> &SkPortAudio::getStandardRates()
{
    return standardRates;
}

bool SkPortAudio::initializePA()
{
    if (portAudioInitialized)
    {
        lastError = SkAudioError::SoundSystemAlreadyStarted;
        onError();

        ObjectError("Cannot start; SoundSystem is ALREADY enabled");
        return false;
    }

    ObjectDebug("Initializing PortAudio ..");

    lastPaError = Pa_Initialize();

    production.inputProduction = new SkRingBuffer;
    SkString name = objectName();
    name.append(".INPUT");
    production.inputProduction->setObjectName(name.c_str());

    production.outputProduction = new SkRingBuffer;
    name = objectName();
    name.append(".OUTPUT");
    production.outputProduction->setObjectName(name.c_str());

    if (lastPaError != paNoError)
    {
        lastError = SkAudioError::InternalSoundSystemError;
        onError();

        ObjectError("Cannot initialize PortAudio; " << Pa_GetErrorText(lastPaError));
        return false;
    }

    portAudioInitialized = true;
    ObjectMessage("PortAudio is initialized");
    return true;
}

void SkPortAudio::closePA()
{
    if (!portAudioInitialized)
    {
        lastError = SkAudioError::SoundSystemAlreadyStopped;
        onError();

        ObjectError("Cannot stop; SoundSystem is ALREADY stopped");
        return;
    }

    portAudioInitialized = false;

    delete production.inputProduction;
    production.inputProduction = nullptr;

    delete production.outputProduction;
    production.outputProduction = nullptr;

    Pa_Terminate();
    ObjectMessage("PortAudio is terminated");
}

#endif
