/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKPASTREAM_H
#define SKPASTREAM_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_PORTAUDIOLIB)

#include <portaudio.h>

#include "Core/Object/skobject.h"
#include "Multimedia/Audio/skaudiobuffer.h"

class SkPortAudio;

// // // // // // // // // // // // // // // // // // // // // // // // // // //

struct SkAudioProduction
{
    bool thNamed;

    SkPortAudio *driver;
    uint64_t bufferSize;
    uint64_t frameSize;

    PaStreamParameters *inputParamsPA;
    SkAudioParameters *inputAudioParameters;
    SkRingBuffer *inputProduction;

    PaStreamParameters *outputParamsPA;
    SkAudioParameters *outputAudioParameters;
    SkRingBuffer *outputProduction;

    SkAudioProduction()
    {
        thNamed = false;

        driver = nullptr;
        bufferSize = 0;
        frameSize = 0;

        inputParamsPA = nullptr;
        inputAudioParameters = nullptr;
        inputProduction = nullptr;

        outputParamsPA = nullptr;
        outputAudioParameters = nullptr;
        outputProduction = nullptr;
    }
};

// // // // // // // // // // // // // // // // // // // // // // // // // // //

enum SkAudioError
{
    NoError,
    SoundSystemAlreadyStarted,
    SoundSystemAlreadyStopped,
    DeviceNotInitialized,
    DeviceNotFound,
    StreamAlreadyStarted,
    StreamAlreadyStopped,
    DuplexIoFramesNotMatch,
    DuplexIoFormatsNotMatch,
    DuplexIoRatesNotMatch,
    FormatNotSupported,
    InputChannelsNotValid,
    OutputChannelsNotValid,
    InternalSoundSystemError
} ;

enum SkAudioMode
{
    NullMode,
    Capture,
    Playback,
    Duplex
};

// // // // // // // // // // // // // // // // // // // // // // // // // // //

class SKAUDIO SkPaStream extends SkObject
{
    public:
        Constructor(SkPaStream, SkObject);

        bool rec(CStr *name, SkAudioParameters &parameters);
        bool play(CStr *name, SkAudioParameters &parameters);

        bool start(CStr *name,
                   SkAudioParameters &inputParameters,
                   SkAudioParameters &outputParameters,
                   SkAudioMode audioMode=SkAudioMode::Duplex);

        void stop();

        bool isStreamActive();

        const PaDeviceInfo *getDeviceInfo();
        CStr *getDeviceName();
        SkAudioMode getMode();
        CStr *getModeName();
        SkAudioProduction &getProduction();

        SkAudioMode getStreamMode();
        double getLastStreamCpuLoad();

        SkAudioError getLastError();
        CStr *getLastErrorString();

        Signal(started);
        Signal(stopped);
        Signal(errorNotify);

        Signal(dataLoaded);
        Signal(dataGotten);

    private:
        int deviceID;
        SkString deviceName;
        const PaDeviceInfo *deviceInfo;
        SkAudioMode mode;
        PaSampleFormat audioWord;
        SkAudioParameters inputAudioParameters;
        PaStreamParameters inputParamsPA;
        SkAudioParameters outputAudioParameters;
        PaStreamParameters outputParamsPA;

        PaStream *stream;

    protected:
        SkStringList devicesNames;
        SkVector<const PaDeviceInfo *> devices;
        SkAudioProduction production;
        int lastPaError;
        SkAudioError lastError;

        bool validate();
        bool setupChannels();
        void setupParameters();

        bool openAudioStream();
        bool closeAudioStream();

        void resetAudioStream();

        void printDevInfo(uint64_t id);

        virtual void onStart()  {}
        virtual void onStop()   {}
        virtual void onError()  {}
};

// // // // // // // // // // // // // // // // // // // // // // // // // // //

#endif

#endif // SKPASTREAM_H
