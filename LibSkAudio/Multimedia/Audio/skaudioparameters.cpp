#include "skaudioparameters.h"
#include "Core/sklogmachine.h"

#if defined(ENABLE_AUDIO)

// // // // // // // // // // // // // // // // // // // // //

DeclareWrapper_ENUM(SkAudioFmtType)
{
    SetupEnumWrapper(SkAudioFmtType);

    SetEnumItem(SkAudioFmtType::AFMT_NULL);
    SetEnumItem(SkAudioFmtType::AFMT_CHAR);
    SetEnumItem(SkAudioFmtType::AFMT_UCHAR);
    SetEnumItem(SkAudioFmtType::AFMT_SHORT);
    SetEnumItem(SkAudioFmtType::AFMT_USHORT);
    SetEnumItem(SkAudioFmtType::AFMT_INT);
    SetEnumItem(SkAudioFmtType::AFMT_UINT);
    SetEnumItem(SkAudioFmtType::AFMT_FLOAT);
    SetEnumItem(SkAudioFmtType::AFMT_DOUBLE);
}

DeclareWrapper_STRUCT_TOMAP(SkAudioProps)
{
    SetupStructWrapper(SkAudioProps);

    StructToMapProperty(isPlanar);
    StructToMapProperty(nChannels);
    StructToMapProperty(sampleRate);
    StructToMapProperty(bufferFrames);
    StructToMapProperty(bufferSamples);
    StructToMapProperty(fmt);
    StructToMapProperty(bytesPerSample);
    StructToMapProperty(tickTimePeriod);
    StructToMapProperty(canonicalSize);
    StructToMapProperty(oneSecondSize);
}

DeclareWrapper_STRUCT_FROMMAP(SkAudioProps)
{
    MapToStructProperty(isPlanar).toBool();
    MapToStructProperty(nChannels).toUInt();
    MapToStructProperty(sampleRate).toUInt();
    MapToStructProperty(bufferFrames).toUInt();
    MapToStructProperty(bufferSamples).toUInt();
    MapToStructPropertyCasted(fmt, SkAudioFmtType, toInt);
    MapToStructPropertyCasted(bytesPerSample, SkAudioFmtType, toInt);
    MapToStructProperty(tickTimePeriod).toFloat();
    MapToStructProperty(canonicalSize).toUInt64();
    MapToStructProperty(oneSecondSize).toUInt64();
}

DeclareWrapper(SkAudioParameters);

DeclareMeth_INSTANCE_RET(SkAudioParameters, set, bool, Arg_Bool, Arg_UInt, Arg_UInt, Arg_UInt, Arg_Enum(SkAudioFmtType))
DeclareMeth_INSTANCE_RET_OVERLOAD(SkAudioParameters, set, 1, bool, *Arg_Custom(SkAudioProps))
DeclareMeth_INSTANCE_VOID(SkAudioParameters, clear)
DeclareMeth_INSTANCE_RET(SkAudioParameters, isPlanar, bool)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getChannels, uint)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getSampleRate, uint)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getBufferFrames, uint)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getBufferSamples, uint)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getFormat, SkAudioFmtSize)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getFormatName, CStr*)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getBytesPerSample, SkAudioFmtSize)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getTickTimePeriod, float)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getCanonicalSize, uint64_t)
DeclareMeth_INSTANCE_RET(SkAudioParameters, getOneSecondSize, uint64_t)
//DeclareMeth_INSTANCE_RET(SkAudioParameters, getProperties, SkAudioProps)
DeclareMeth_INSTANCE_RET(SkAudioParameters, toMap, bool, *Arg_Custom(SkArgsMap))
DeclareMeth_INSTANCE_RET(SkAudioParameters, fromMap, bool, *Arg_Custom(SkArgsMap))

DeclareMeth_STATIC_RET(SkAudioParameters, fmtToString, CStr*, Arg_Enum(SkAudioFmtType))
DeclareMeth_STATIC_RET(SkAudioParameters, fmtFromString, SkAudioFmtType, Arg_CStr)
DeclareMeth_STATIC_RET(SkAudioParameters, fmtToSize, SkAudioFmtSize, Arg_Enum(SkAudioFmtType))

SetupClassWrapper(SkAudioParameters)
{
    AddEnumType(SkAudioFmtType);
    AddStructType(SkAudioProps);

    AddMeth_INSTANCE_RET(SkAudioParameters, set);
    AddMeth_INSTANCE_RET_OVERLOAD(SkAudioParameters, set, 1);
    AddMeth_INSTANCE_VOID(SkAudioParameters, clear);
    AddMeth_INSTANCE_RET(SkAudioParameters, isPlanar);
    AddMeth_INSTANCE_RET(SkAudioParameters, getChannels);
    AddMeth_INSTANCE_RET(SkAudioParameters, getSampleRate);
    AddMeth_INSTANCE_RET(SkAudioParameters, getBufferFrames);
    AddMeth_INSTANCE_RET(SkAudioParameters, getBufferSamples);
    AddMeth_INSTANCE_RET(SkAudioParameters, getFormat);
    AddMeth_INSTANCE_RET(SkAudioParameters, getFormatName);
    AddMeth_INSTANCE_RET(SkAudioParameters, getBytesPerSample);
    AddMeth_INSTANCE_RET(SkAudioParameters, getTickTimePeriod);
    AddMeth_INSTANCE_RET(SkAudioParameters, getCanonicalSize);
    AddMeth_INSTANCE_RET(SkAudioParameters, getOneSecondSize);
    //AddMeth_INSTANCE_RET(SkAudioParameters, getProperties)
    AddMeth_INSTANCE_RET(SkAudioParameters, toMap);
    AddMeth_INSTANCE_RET(SkAudioParameters, fromMap);

    AddMeth_STATIC_RET(SkAudioParameters, fmtToString);
    AddMeth_STATIC_RET(SkAudioParameters, fmtFromString);
    AddMeth_STATIC_RET(SkAudioParameters, fmtToSize);
}
// // // // // // // // // // // // // // // // // // // // //

SkAudioParameters::SkAudioParameters()
{
    CreateClassWrapper(SkAudioParameters);

    clear();
}

SkAudioParameters::SkAudioParameters(bool isPlanar, uint nChannels, uint sampleRate, uint bufferFrames, SkAudioFmtType fmt)
{
    CreateClassWrapper(SkAudioParameters);

    set(isPlanar, nChannels, sampleRate, bufferFrames, fmt);
}

SkAudioParameters::SkAudioParameters(SkAudioProps &properties)
{
    CreateClassWrapper(SkAudioParameters);

    set(properties);
}

SkAudioParameters::SkAudioParameters(SkAudioParameters *other)
{
    CreateClassWrapper(SkAudioParameters);

    props.isPlanar = other->props.isPlanar;
    props.nChannels = other->props.nChannels;
    props.sampleRate = other->props.sampleRate;
    props.bufferFrames = other->props.bufferFrames;
    props.bufferSamples = other->props.bufferSamples;
    props.fmt = other->props.fmt;
    props.bytesPerSample = other->props.bytesPerSample;
    props.tickTimePeriod = other->props.tickTimePeriod;
    props.canonicalSize = other->props.canonicalSize;
    props.oneSecondSize = other->props.oneSecondSize;
}

bool SkAudioParameters::set(bool isPlanar, uint nChannels, uint sampleRate, uint bufferFrames, SkAudioFmtType fmt)
{
    if (!nChannels || !sampleRate || !bufferFrames || fmt == AFMT_NULL)
    {
        FlatError("AudioParameters NOT valid [" << "ch: " << nChannels << "; rate: " << sampleRate << "; fr: " << bufferFrames << "; fmt: " <<  fmt << "]");
        return false;
    }

    if (props.nChannels == 1)
        props.isPlanar = true;
    else
        props.isPlanar = isPlanar;

    props.nChannels = nChannels;
    props.sampleRate = sampleRate;
    props.bufferFrames = bufferFrames;
    props.bufferSamples = props.bufferFrames * props.nChannels;
    props.fmt = fmt;
    props.bytesPerSample = SkAudioParameters::fmtToSize(props.fmt);
    props.tickTimePeriod = 1.f / (static_cast<float>(props.sampleRate) / static_cast<float>(props.bufferFrames));
    props.canonicalSize = props.bufferSamples * props.bytesPerSample;
    props.oneSecondSize = props.sampleRate * props.nChannels * props.bytesPerSample;

    return true;
}

bool SkAudioParameters::set(SkAudioProps &properties)
{
    return set(properties.isPlanar, properties.nChannels, properties.sampleRate, properties.bufferFrames, properties.fmt);
}

void SkAudioParameters::clear()
{
    props.isPlanar = false;
    props.nChannels = 0;
    props.sampleRate = 0;
    props.bufferFrames = 0;
    props.bufferSamples = 0;
    props.fmt = SkAudioFmtType::AFMT_NULL;
    props.bytesPerSample = FMTSIZE_NULL;
    props.tickTimePeriod = 0;
    props.canonicalSize = 0;
    props.oneSecondSize = 0;
}

bool SkAudioParameters::isPlanar()
{
    return props.isPlanar;
}

uint SkAudioParameters::getChannels()
{
    return props.nChannels;
}

uint SkAudioParameters::getSampleRate()
{
    return props.sampleRate;
}

uint SkAudioParameters::getBufferFrames()
{
    return props.bufferFrames;
}

uint SkAudioParameters::getBufferSamples()
{
    return props.bufferSamples;
}

SkAudioFmtType SkAudioParameters::getFormat()
{
    return props.fmt;
}

CStr *SkAudioParameters::getFormatName()
{
    return SkAudioParameters::fmtToString(props.fmt);
}

SkAudioFmtSize SkAudioParameters::getBytesPerSample()
{
    return props.bytesPerSample;
}

float SkAudioParameters::getTickTimePeriod()
{
    return props.tickTimePeriod;
}

uint64_t SkAudioParameters::getCanonicalSize()
{
    return props.canonicalSize;
}

uint64_t SkAudioParameters::getOneSecondSize()
{
    return props.oneSecondSize;
}

SkAudioProps SkAudioParameters::getProperties()
{
    return props;
}

bool SkAudioParameters::toMap(SkArgsMap &m)
{
    if (props.fmt == SkAudioFmtType::AFMT_NULL)
    {
        FlatError("Cannot export AudioParameters to map, because it is NOT initialized");
        return false;
    }

    m["isPlanar"] = props.isPlanar;
    m["channels"] = props.nChannels;
    m["sampleRate"] = props.sampleRate;
    m["bufferFrames"] = props.bufferFrames;
    m["totalBufferSamples"] = props.bufferSamples;
    m["fmt"] = props.fmt;
    m["bytesPerSample"] = props.bytesPerSample;
    m["tickTimePeriod"] = props.tickTimePeriod;
    m["canonicalSize"] = props.canonicalSize;
    m["oneSecondSize"] = props.oneSecondSize;
    return true;
}

bool SkAudioParameters::fromMap(SkArgsMap &m)
{
    if (!m.contains("isPlanar")
            || !m.contains("channels")
            || !m.contains("sampleRate")
            || !m.contains("bufferFrames")
            || !m.contains("fmt"))
    {
        FlatError("Cannot import AudioParameters from map, because it is MALFORMED");
        return false;
    }

    return set(m["isPlanar"].toBool(),
               m["channels"].toUInt32(),
               m["sampleRate"].toUInt32(),
               m["bufferFrames"].toUInt32(),
               static_cast<SkAudioFmtType>(m["fmt"].toUInt32()));
}

CStr *SkAudioParameters::fmtToString(SkAudioFmtType fmt)
{
    if (fmt == SkAudioFmtType::AFMT_CHAR)
        return "S8";

    else if (fmt == SkAudioFmtType::AFMT_UCHAR)
        return "U8";

    else if (fmt == SkAudioFmtType::AFMT_SHORT)
        return "S16";

    else if (fmt == SkAudioFmtType::AFMT_USHORT)
        return "U16";

    else if (fmt == SkAudioFmtType::AFMT_INT)
        return "S32";

    else if (fmt == SkAudioFmtType::AFMT_UINT)
        return "U32";

    else if (fmt == SkAudioFmtType::AFMT_FLOAT)
        return "FLOAT";

    else if (fmt == SkAudioFmtType::AFMT_DOUBLE)
        return "DOUBLE";

    return "";
}

SkAudioFmtType SkAudioParameters::fmtFromString(CStr *fmtName)
{
    if (SkString::compare(fmtName, "S8"))
        return SkAudioFmtType::AFMT_CHAR;

    else if (SkString::compare(fmtName, "U8"))
        return SkAudioFmtType::AFMT_UCHAR;

    else if (SkString::compare(fmtName, "S16"))
        return SkAudioFmtType::AFMT_SHORT;

    else if (SkString::compare(fmtName, "U16"))
        return SkAudioFmtType::AFMT_USHORT;

    else if (SkString::compare(fmtName, "S32"))
        return SkAudioFmtType::AFMT_INT;

    else if (SkString::compare(fmtName, "U32"))
        return SkAudioFmtType::AFMT_UINT;

    else if (SkString::compare(fmtName, "FLOAT"))
        return SkAudioFmtType::AFMT_FLOAT;

    else if (SkString::compare(fmtName, "DOUBLE"))
        return SkAudioFmtType::AFMT_DOUBLE;

    return SkAudioFmtType::AFMT_NULL;
}

SkAudioFmtSize SkAudioParameters::fmtToSize(SkAudioFmtType fmt)
{
    SkAudioFmtSize bytesPerSample = 0;

    if (fmt == SkAudioFmtType::AFMT_CHAR)
        bytesPerSample = FMTSIZE_CHAR;

    else if (fmt == SkAudioFmtType::AFMT_UCHAR)
        bytesPerSample = FMTSIZE_UCHAR;

    else if (fmt == SkAudioFmtType::AFMT_SHORT)
        bytesPerSample = FMTSIZE_SHORT;

    else if (fmt == SkAudioFmtType::AFMT_USHORT)
        bytesPerSample = FMTSIZE_USHORT;

    else if (fmt == SkAudioFmtType::AFMT_INT)
        bytesPerSample = FMTSIZE_INT;

    else if (fmt == SkAudioFmtType::AFMT_UINT)
        bytesPerSample = FMTSIZE_UINT;

    else if (fmt == SkAudioFmtType::AFMT_FLOAT)
        bytesPerSample = FMTSIZE_FLOAT;

    else if (fmt == SkAudioFmtType::AFMT_DOUBLE)
        bytesPerSample = FMTSIZE_DOUBLE;

    else
        bytesPerSample = FMTSIZE_NULL;

    return bytesPerSample;
}

#endif
