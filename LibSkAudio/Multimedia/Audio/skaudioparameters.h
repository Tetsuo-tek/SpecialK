/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAUDIOPARAMETERS_H
#define SKAUDIOPARAMETERS_H

#if defined(ENABLE_AUDIO)

#include "Core/Containers/skargsmap.h"

enum SkAudioFmtType
{
    AFMT_NULL,
    AFMT_CHAR,
    AFMT_UCHAR,
    AFMT_SHORT,
    AFMT_USHORT,
    AFMT_INT,
    AFMT_UINT,
    AFMT_FLOAT,
    AFMT_DOUBLE
};

typedef unsigned char SkAudioFmtSize;

static const SkAudioFmtSize FMTSIZE_NULL = 0;
static const SkAudioFmtSize FMTSIZE_CHAR = sizeof(char);
static const SkAudioFmtSize FMTSIZE_UCHAR = FMTSIZE_CHAR;
static const SkAudioFmtSize FMTSIZE_SHORT = sizeof(short);
static const SkAudioFmtSize FMTSIZE_USHORT = FMTSIZE_SHORT;
static const SkAudioFmtSize FMTSIZE_INT = sizeof(int);
static const SkAudioFmtSize FMTSIZE_UINT = FMTSIZE_INT;
static const SkAudioFmtSize FMTSIZE_FLOAT = sizeof(float);
static const SkAudioFmtSize FMTSIZE_DOUBLE = sizeof(double);

struct SkAudioProps
{
    bool isPlanar;
    uint nChannels;
    uint sampleRate;
    uint bufferFrames;
    uint bufferSamples;
    SkAudioFmtType fmt;
    SkAudioFmtSize bytesPerSample;
    float tickTimePeriod;
    uint64_t canonicalSize;
    uint64_t oneSecondSize;
} ;

class SKAUDIO SkAudioParameters extends SkFlatObject
{
    public:
        SkAudioParameters();
        SkAudioParameters(bool isPlanar, uint nChannels, uint sampleRate, uint bufferFrames, SkAudioFmtType fmt);
        SkAudioParameters(SkAudioProps &properties);
        SkAudioParameters(SkAudioParameters *other);

        bool set(bool isPlanar, uint nChannels, uint sampleRate, uint bufferFrames, SkAudioFmtType fmt);
        bool set(SkAudioProps &properties);
        void clear();

        bool isPlanar();
        uint getChannels();
        uint getSampleRate();
        uint getBufferFrames();
        uint getBufferSamples();
        SkAudioFmtType getFormat();
        CStr *getFormatName();
        SkAudioFmtSize getBytesPerSample();
        float getTickTimePeriod();
        uint64_t getCanonicalSize();
        uint64_t getOneSecondSize();
        SkAudioProps getProperties();

        bool toMap(SkArgsMap &m);
        bool fromMap(SkArgsMap &m);

        static CStr *fmtToString(SkAudioFmtType fmt);
        static SkAudioFmtType fmtFromString(CStr *fmtName);
        static SkAudioFmtSize fmtToSize(SkAudioFmtType fmt);

    private:
        SkAudioProps props;
};

#endif

#endif // SKAUDIOPARAMETERS_H
