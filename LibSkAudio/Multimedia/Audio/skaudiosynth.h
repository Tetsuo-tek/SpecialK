/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#ifndef SKAUDIOSYNTH_H
#define SKAUDIOSYNTH_H

#if defined(ENABLE_AUDIO)

#include "skaudioparameters.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skringbuffer.h"

#define SK_PI 3.14159265358979323846264338327950288f

enum SkSynthFunctions
{
    SYNTH_SINE,
    SYNTH_COSINE,
    SYNTH_TANGENT,
    SYNTH_LOGARITHMIC,
    SYNTH_TRIANGULAR,
    SYNTH_SAWTOOTH,
    SYNTH_SQUARE,
    SYNTH_PULSE,
    SYNTH_NOISE
};

struct SKAUDIO SkSynthProperties
{
    SkSynthFunctions type;//SINE, COSINE, TANGENT, LOGARITHMIC, TRIANGULAR, SAWTOOTH, SQUARE, PULSE, NOISE
    float frequency;
    float amplitudeFactor;
    float pulseLength;

    SkSynthProperties()
    {
        type = SkSynthFunctions::SYNTH_SINE;
        frequency = 1000.f;
        amplitudeFactor = 1.f;
        pulseLength = 0.01f;
    }
};

class SKAUDIO SkAudioSynth extends SkFlatObject
{
    public:
        SkAudioSynth();

        static void fillNotesFrequencyMap(SkMap<SkString, float> &notes);

        void setup(SkAudioParameters &audioParameters, SkSynthProperties &properties);

        const SkAudioParameters &getAudioParameters();
        const SkSynthProperties &getSignalProperties();
        SkRingBuffer *getProduction();

        void tick();

    private:
        SkAudioParameters params;
        SkSynthProperties props;

        SkRingBuffer production;

        std::mutex bufferEx;
        float *signalLoopBuffer;
        uint periodSamples;
        uint bufferCursor;

        void calculate();
};

#endif

#endif // SKAUDIOSYNTH_H
