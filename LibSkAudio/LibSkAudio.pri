CONFIG += c++11
CONFIG -= qt

DEFINES += ENABLE_AUDIO

DEFINES += ENABLE_AUDIOENCLIBS
    LIBS += -logg
    LIBS += -lvorbis
    LIBS += -lvorbisenc
    #LIBS += -lFLAC
    #LIBS += -lFLAC++
    #LIBS += -lspeex

DEFINES += ENABLE_PORTAUDIOLIB
    LIBS += -lportaudio

DEFINES += ENABLE_TEXT_TO_SPEECH
    #LIBS += -lespeak-ng
    LIBS += -lespeak

#DEFINES += ENABLE_SPEECH_TO_TEXT
    #PKGCONFIG += pocketsphinx
    #LIBS += -lpocketsphinx

DEFINES += ENABLE_FFTW3LIB
    LIBS += -lfftw3


