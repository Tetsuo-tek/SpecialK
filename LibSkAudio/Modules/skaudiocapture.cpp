#include "skaudiocapture.h"
#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include "Core/Containers/skarraycast.h"

#if defined(ENABLE_AUDIO) && defined(ENABLE_FFTW3LIB) && defined(ENABLE_PORTAUDIOLIB)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkAudioCaptureCfg, SkAbstractModuleCfg)
{
}

void SkAudioCaptureCfg::allowedCfgKeysSetup()
{
    SkWorkerParam *p = nullptr;

    p = addParam("device", "Capturing device", T_STRING, true, "default", "Audio device name");

    SkPortAudio *pa = new SkPortAudio;
    pa->setObjectName(this, "PortAudio");

    pa->init();

    SkStringList &l = pa->getDevicesNames();

    for(uint64_t i=0; i<l.count(); i++)
        p->addOption(l[i].c_str(), l[i]);

    p = addParam("sampleRate", "Sample rate", SkVariant_T::T_UINT32, true, 44100, "Capturing sample rate");

    SkVector<uint> &r = pa->getStandardRates();

    for(uint64_t i=0; i<r.count(); i++)
    {
        SkString s;
        s.concat(r[i]);
        int32_t c = r[i];
        p->addOption(s.c_str(), c);
    }

    pa->close();
    pa->destroyLater();
    pa = nullptr;

    p = addParam("bufferFrames", "Buffer frames", T_UINT32, true, 1024, "Number of frames in the buffer");
    p->addOption("32", 32);
    p->addOption("64", 64);
    p->addOption("128", 128);
    p->addOption("256", 256);
    p->addOption("512", 512);
    p->addOption("1024", 1024);
    p->addOption("2048", 2048);
    p->addOption("4096", 4096);
    p->addOption("8192", 8192);

    p = addParam("format", "Format", T_UINT32, true, AFMT_FLOAT, "Sample format");
    p->addOption("Signed 8bit", AFMT_CHAR);
    p->addOption("UnSigned 8bit", AFMT_UCHAR);
    p->addOption("Signed 16 bit", AFMT_SHORT);
    p->addOption("UnSigned 16 bit", AFMT_USHORT);
    p->addOption("Signed 32 bit", AFMT_INT);
    p->addOption("UnSigned 32 bit", AFMT_UINT);
    p->addOption("Float 32 bit", AFMT_FLOAT);
    p->addOption("Double 64 bit", AFMT_DOUBLE);

    p = addParam("channels", "Channels", T_UINT8, true, 2, "Channels");
    p->addOption("Mono", 1);
    p->addOption("Stereo", 2);
    p->addOption("4", 4);
    p->addOption("5", 5);

    addParam("enableFftWindowGaussianFilter", "Enable Gaussian fft-window filtering", SkVariant_T::T_BOOL, false, false, "Enable Gaussian fft-window attenuation-filtering");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkAudioCapture, SkAbstractModule)
{
    publisher = new SkFlowAudioPublisher(this);

    SlotSet(setMute);
    SlotSet(setGain);
    SlotSet(setBass);
    SlotSet(setMiddle);
    SlotSet(setTreble);
}

void SkAudioCapture::onInit()
{
    SkWorkerParam *p = nullptr;

    p = addSingleControl("Mute", T_BOOL, false, "Set mute", setMute_SLOT);

    p = addSingleControl("Gain", T_FLOAT, 1.f, "Gain change", setGain_SLOT);
    p->setMin(0.f);
    p->setMax(10.f);
    p->setStep(0.01f);

    p = addSingleControl("Bass", T_FLOAT, 1.f, "Bass change", setBass_SLOT);
    p->setMin(0.f);
    p->setMax(10.f);
    p->setStep(0.01f);

    p = addSingleControl("Middle", T_FLOAT, 1.f, "Middle change", setMiddle_SLOT);
    p->setMin(0.f);
    p->setMax(10.f);
    p->setStep(0.01f);

    p = addSingleControl("Treble", T_FLOAT, 1.f, "Treble change", setTreble_SLOT);
    p->setMin(0.f);
    p->setMax(10.f);
    p->setStep(0.01f);
}

bool SkAudioCapture::onSetup()
{
    SkAbstractModuleCfg *cfg = config();
    device = cfg->getParameter("device")->value().toString();
    return true;
}

void SkAudioCapture::onStart()
{
    ObjectMessage("Initializing PortAudio (for INPUT) ..");

    setupPublisher(publisher);
    publisher->setObjectName(this, "Publisher");

}

void SkAudioCapture::onStop()
{
}

void SkAudioCapture::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        SkAbstractModuleCfg *cfg = config();

        SkAudioParameters p(false,
                            cfg->getParameter("channels")->value().toUInt(),
                            cfg->getParameter("sampleRate")->value().toUInt(),
                            cfg->getParameter("bufferFrames")->value().toUInt(),
                            static_cast<SkAudioFmtType>(cfg->getParameter("format")->value().toInt()));

        if (!publisher->setup(p))
        {
            requestToEnable(false);//
            return;
        }

        publisher->enableCapture(device.c_str());
        publisher->enableFftGaussianFilter(cfg->getParameter("enableFftWindowGaussianFilter")->value().toBool());

        publisher->start();
        ObjectMessage("Recording STARTED: " << device << " ..");
    }

    else
    {
        publisher->stop();
        ObjectMessage("Recording STOPPED: " << device << " ..");
    }
}

void SkAudioCapture::onFastTick()
{
    if (!isEnabled())
        return;

    publisher->tick();
}

void SkAudioCapture::onSlowTick()
{
}

void SkAudioCapture::onOneSecTick()
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkAudioCapture, setMute)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
        publisher->setMute(cmdMap["setMute"].toBool());

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

SlotImpl(SkAudioCapture, setGain)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
        publisher->setGain(cmdMap["setGain"].toFloat());

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

SlotImpl(SkAudioCapture, setBass)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
        publisher->setBass(cmdMap["setBass"].toFloat());

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

SlotImpl(SkAudioCapture, setMiddle)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
        publisher->setMiddle(cmdMap["setMiddle"].toFloat());

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

SlotImpl(SkAudioCapture, setTreble)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    SkVariant &v = Arg_AbstractVariadic;
    SkArgsMap cmdMap;
    v.copyToMap(cmdMap);

    if (analyzeTransaction(t, v))
        publisher->setTreble(cmdMap["setTreble"].toFloat());

    else
    {
        t->setError("Command NOT valid");
        t->setResponse(cmdMap);
    }

    t->goBack();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
