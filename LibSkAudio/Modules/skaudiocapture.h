#ifndef SKAUDIOCAPTURE_H
#define SKAUDIOCAPTURE_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_FFTW3LIB) && defined(ENABLE_PORTAUDIOLIB)

#include "Core/System/Network/FlowNetwork/skflowaudiopublisher.h"
#include "Modules/skabstractmodule.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAudioCaptureCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkAudioCaptureCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkAudioCapture extends SkAbstractModule
{
    SkString device;

    SkFlowAudioPublisher *publisher;

    public:
        Constructor(SkAudioCapture, SkAbstractModule);

        Slot(setMute);
        Slot(setGain);
        Slot(setBass);
        Slot(setMiddle);
        Slot(setTreble);

    protected:
        void onInit()                                                   override;
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKAUDIOCAPTURE_H
