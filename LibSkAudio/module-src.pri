HEADERS += \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiofftpublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiomagmapublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiopcmpublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiopublisher.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiosubscriber.h \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiovorbispublisher.h \
    $$PWD/Modules/skaudiocapture.h \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyadec.h \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyaenc.h \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyaencth.h \
    $$PWD/Multimedia/Audio/PA/skpastream.h \
    $$PWD/Multimedia/Audio/PA/skportaudio.h \
    $$PWD/Multimedia/Audio/Voice/sktexttospeech.h \
    $$PWD/Multimedia/Audio/Voice/skspeechtotext.h \
    $$PWD/Multimedia/Audio/skaudiobuffer.h \
    $$PWD/Multimedia/Audio/skaudioparameters.h \
    $$PWD/Multimedia/Audio/skaudioquantizer.h \
    $$PWD/Multimedia/Audio/skaudioresampler.h \
    $$PWD/Multimedia/Audio/skaudiosynth.h \
    $$PWD/Multimedia/Audio/Encoders/skaudiowave.h \
    $$PWD/Multimedia/Audio/Encoders/skoggpacketizer.h \
    $$PWD/Multimedia/Audio/Encoders/skvorbisencoder.h

SOURCES += \
    $$PWD//Modules/skaudiocapture.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiofftpublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiomagmapublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiopcmpublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiopublisher.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiosubscriber.cpp \
    $$PWD/Core/System/Network/FlowNetwork/skflowaudiovorbispublisher.cpp \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyadec.cpp \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyaenc.cpp \
    $$PWD/Multimedia/Audio/Encoders/Magma/skpsyaencth.cpp \
    $$PWD/Multimedia/Audio/PA/skpastream.cpp \
    $$PWD/Multimedia/Audio/PA/skportaudio.cpp \
    $$PWD/Multimedia/Audio/Voice/sktexttospeech.cpp \
    $$PWD/Multimedia/Audio/Voice/skspeechtotext.cpp \
    $$PWD/Multimedia/Audio/skaudiobuffer.cpp \
    $$PWD/Multimedia/Audio/skaudioparameters.cpp \
    $$PWD/Multimedia/Audio/skaudioquantizer.cpp \
    $$PWD/Multimedia/Audio/skaudioresampler.cpp \
    $$PWD/Multimedia/Audio/skaudiosynth.cpp \
    $$PWD/Multimedia/Audio/Encoders/skaudiowave.cpp \
    $$PWD/Multimedia/Audio/Encoders/skoggpacketizer.cpp \
    $$PWD/Multimedia/Audio/Encoders/skvorbisencoder.cpp

DISTFILES +=
