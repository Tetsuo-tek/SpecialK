TARGET = SkAudio
TEMPLATE = lib

# insert sources

include(LibSkAudio.pri)
include(module-src.pri)

DEFINES += SKAUDIO_LIBRARY

# include libSkCore

LIBS += -lSkCore
INCLUDEPATH += /usr/local/include/LibSkCore
include(/usr/local/include/LibSkCore/LibSkCore.pri)

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkAudio/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkAudio.pri

headersDataFiles.path = /usr/local/include/LibSkAudio/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles

