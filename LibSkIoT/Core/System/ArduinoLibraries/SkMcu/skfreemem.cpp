#include "skfreemem.h"

#if defined(ARDUINO_ARCH_AVR)
    extern unsigned int __heap_start;
    extern void *__brkval;

    struct __freelist {
      size_t sz;
      struct __freelist *nx;
    };

    extern struct __freelist *__flp;

    int freeListSize()
    {
      struct __freelist* current;
      int total = 0;

      for (current = __flp; current; current = current->nx)
      {
        total += 2; /* Add two bytes for the memory block's header  */
        total += (int) current->sz;
      }

      return total;
    }

#elif defined(ARDUINO_ARCH_SAM) || defined(ARDUINO_ARCH_SAMD)
    extern "C" char *sbrk(int i);

#endif

unsigned long skFreeMemory(bool useFreeList)
{

#if defined(ARDUINO_ARCH_AVR)

    if (useFreeList)
    {
        int free_memory=0;

        if ((int)__brkval == 0)
            free_memory = ((int)&free_memory) - ((int)&__heap_start);
        else
        {
            free_memory = ((int)&free_memory) - ((int)__brkval);
            free_memory += freeListSize();
        }

        return free_memory;
    }

    else
    {
        extern char *__brkval;
        char top;
        return &top - __brkval;
    }

#elif defined(ARDUINO_ARCH_SAMD) || defined(ARDUINO_ARCH_SAM)
    char stack_dummy = 0;
    return(&stack_dummy - sbrk(0));

/*#elif defined(ARDUINO_ARCH_SAM)
    //char stack_dummy = 0;
    //long f = (long)  (&stack_dummy - sbrk(0));
    //Serial.println(f);
    return(f);
    return 0;*/

#elif defined(ARDUINO_ARCH_ESP8266)
    return ESP.getFreeHeap();

#endif

    useFreeList = 0;
    return 0;
}
