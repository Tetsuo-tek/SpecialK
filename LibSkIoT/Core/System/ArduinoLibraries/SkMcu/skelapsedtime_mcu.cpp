#include "skelapsedtime.h"

#if !defined(PCSKETCH)

////////////////////////////////////////////////////////////////////////////////////

SkElapsedTimeMicros::SkElapsedTimeMicros()
{
    microseconds = 0;
}

void SkElapsedTimeMicros::start()
{
    microseconds = micros();
}

unsigned long SkElapsedTimeMicros::stop()
{
    return micros() - microseconds;
}

bool SkElapsedTimeMicros::check(unsigned long interval)
{
    return stop() >= interval;
}

////////////////////////////////////////////////////////////////////////////////////

SkElapsedTimeMillis::SkElapsedTimeMillis()
{
    milliseconds = 0;
}

void SkElapsedTimeMillis::start()
{
    milliseconds = millis();
}

unsigned long SkElapsedTimeMillis::stop()
{
    return millis() - milliseconds;
}

bool SkElapsedTimeMillis::check(unsigned long interval)
{
    return stop() >= interval;
}

////////////////////////////////////////////////////////////////////////////////////

#endif
