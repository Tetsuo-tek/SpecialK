#include "skmcu.h"

//PRIVATE

#if defined(MCU_LOG)
    #if defined(PCSKETCH)
        #define WRITE_TO_STODOUT(TXT) cout << TXT; cout .flush()
    #else
        #define WRITE_TO_STODOUT(TXT)  MCU_LOGDEV.print(TXT)
    #endif

    #define SkLog(PFX, TXT) \
        WRITE_TO_STODOUT(PFX); \
        WRITE_TO_STODOUT(" -> "); \
        WRITE_TO_STODOUT(TXT); \
        WRITE_TO_STODOUT("\n")
#endif


//PUBLIC

void skMsgLog(const char *text)
{
#if defined(MCU_LOG)
    SkLog(MCU_MSGLOGPFX, text);
#endif
}

void skWrnLog(const char *text)
{
#if defined(MCU_LOG)
    SkLog(MCU_WRNLOGPFX, text);
#endif
}

void skErrLog(const char *text)
{
#if defined(MCU_LOG)
    SkLog(MCU_ERRLOGPFX, text);
#endif
}
