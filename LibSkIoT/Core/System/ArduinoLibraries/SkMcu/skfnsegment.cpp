#include "skfnsegment.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFnSegment::SkFnSegment()
{
    cmd = SkStreamCommand::SC_NULL;
    error = false;
    streamID = -1;
    data = nullptr;
    sz = 0;
    pl = nullptr;
    plSz = 0;

    t =  SkStreamType::ST_NULL;
    m = SkStreamMode::SM_NULL;
    esz = -1;
    cnt = -1;
}

SkFnSegment::~SkFnSegment()
{
    clear();
}

void SkFnSegment::setup(FnClient &c)
{
    clnt = &c;
}

SkStreamCommand SkFnSegment::getCommand()
{
    return (SkStreamCommand) cmd;
}

int SkFnSegment::getStreamID()
{
    return streamID;
}

SkStreamType SkFnSegment::getStreamType()
{
    return t;
}

SkStreamMode SkFnSegment::getStreamMode()
{
    return m;
}

int SkFnSegment::getStreamElemSize()
{
    return esz;
}

int SkFnSegment::getStreamCount()
{
    return cnt;
}

uint8_t *SkFnSegment::getData(uint16_t offset)
{
    if (offset >= sz)
        return nullptr;

    return &data[offset];
}

uint16_t SkFnSegment::size()
{
    return sz;
}

void SkFnSegment::clear()
{
    cmd = SkStreamCommand::SC_NULL;
    streamID = -1;
    error = false;

    if (data)
    {
        free(data);
        data = nullptr;
        sz = 0;
    }

    if (pl)
    {
        pl = nullptr;
        plSz = 0;
    }

    t =  SkStreamType::ST_NULL;
    m = SkStreamMode::SM_NULL;
    esz = -1;
    cnt = -1;

    onClear();
}

bool SkFnSegment::hasError()
{
    return error;
}

void SkFnSegment::addData(void *d, uint16_t size)
{
    uint16_t holdPos = sz;
    sz += size;
    data = (uint8_t *) realloc(data, sz);
    memcpy(&data[holdPos], d, size);
}

void SkFnSegment::write(uint8_t b)
{
#if defined(PCSKETCH)
    clnt->write((const char *) &b, 1);
#else
    clnt->write(b);
#endif
}

void SkFnSegment::write(uint8_t *b, uint16_t size)
{
#if defined(PCSKETCH)
    clnt->write((const char *) b, size);
#else
    clnt->write(b, size);
#endif
}

void SkFnSegment::skipUntil(uint8_t b, long size)
{
#if defined(PCSKETCH)
    cout << "Checking next STX ..\n";

    QByteArray c = clnt->peek(size);
    int pos = c.indexOf(B_STX);

    if (pos == -1)
    {
        cout << "Discarding entire block of unknown-data: " << size << " B\n";
        clnt->read(size);
    }

    if (pos >= 0)
    {
        clear();
        clnt->read(pos);
        cout << "Discarding unknown-data: " << pos << " B\n";
    }

#else
    while(size > 0)
    {
        if (read(true) == B_STX)
        {
            clear();
            break;
        }

        else
        {
            read();
            size--;
        }
    }
#endif
}

uint8_t SkFnSegment::read(bool peeking)
{
#if defined(PCSKETCH)
    uint8_t b;
    if (peeking)
        clnt->peek((char *) &b, 1);
    else
        clnt->read((char *) &b, 1);

    return b;
#else
    return clnt->read();
#endif
}

int SkFnSegment::read(uint8_t *b, uint16_t size)
{
#if defined(PCSKETCH)
    return clnt->read((char *) b, size);

#else
    return clnt->readBytes(b, size);
#endif
}

#if defined(PCSKETCH)
const char *SkFnSegment::cmdToString(SkStreamCommand command)
{
    if (command == SkStreamCommand::SC_PING)
        return "SC_PING";

    else if (command == SkStreamCommand::SC_PONG)
        return "SC_PONG";

    else if (command == SkStreamCommand::SC_STREG)
        return "SC_STREG";

    else if (command == SkStreamCommand::SC_STNAME)
        return "SC_STNAME";

    else if (command == SkStreamCommand::SC_STUDM)
        return "SC_STUDM";

    else if (command == SkStreamCommand::SC_STMIN)
        return "SC_STMIN";

    else if (command == SkStreamCommand::SC_STMAX)
        return "SC_STMAX";

    else if (command == SkStreamCommand::SC_STOUT)
        return "SC_STOUT";

    else if (command == SkStreamCommand::SC_STMSGLOG)
        return "SC_STMSGLOG";

    else if (command == SkStreamCommand::SC_STWRNLOG)
        return "SC_STWRNLOG";

    else if (command == SkStreamCommand::SC_STERRLOG)
        return "SC_PING";
}
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFnSegmentWriter::SkFnSegmentWriter() : SkFnSegment()
{
}

void SkFnSegmentWriter::setCommand(SkStreamCommand command)
{
    cmd = command;
}

void SkFnSegmentWriter::setStreamID(uint8_t id)
{
    streamID = id;
}


void SkFnSegmentWriter::setStreamType(SkStreamType type)
{
    t = type;
}

void SkFnSegmentWriter::setStreamMode(SkStreamMode mode)
{
    m = mode;
}

void SkFnSegmentWriter::setStreamElemSize(uint8_t size)
{
    esz = size;
}

void SkFnSegmentWriter::setStreamCount(uint8_t count)
{
    cnt = count;
}

void SkFnSegmentWriter::addValue_U8(uint8_t v)
{
    addData(&v, sizeof(uint8_t));
}

void SkFnSegmentWriter::addValue_S8(int8_t v)
{
    addData(&v, sizeof(int8_t));
}

void SkFnSegmentWriter::addValue_U16(uint16_t v)
{
    addData(&v, sizeof(uint16_t));
}

void SkFnSegmentWriter::addValue_S16(int16_t v)
{
    addData(&v, sizeof(int16_t));
}

void SkFnSegmentWriter::addValue_U24(uint32_t v)
{
    addData(&v, sizeof(uint32_t)-1);
}

void SkFnSegmentWriter::addValue_S24(int32_t v)
{
    addData(&v, sizeof(int32_t)-1);
}

void SkFnSegmentWriter::addValue_U32(uint32_t v)
{
    addData(&v, sizeof(uint32_t));
}

void SkFnSegmentWriter::addValue_S32(int32_t v)
{
    addData(&v, sizeof(int32_t));
}

void SkFnSegmentWriter::addValue_U64(uint64_t v)
{
    addData(&v, sizeof(uint64_t));
}

void SkFnSegmentWriter::addValue_S64(int64_t v)
{
    addData(&v, sizeof(int64_t));
}

void SkFnSegmentWriter::setPayload(void *payload, uint16_t size)
{
    pl = (uint8_t *) payload;
    plSz = size;
}

void SkFnSegmentWriter::send()
{
    //  |    1    |    2    |    1    |  1 or 0 |    sz   |   psSz  |    1    |
    //  |   STX   |sz+plSz+2|   cmd   |    id   |   data  | payload |   ETX   |
    //                (*)                 (*)
    //        (could be (val)-1) -> (could not exist)

    uint16_t size = sz + plSz;

    write(B_STX);
    write((uint8_t *) &size, sizeof(uint16_t));
    write(cmd);

    if (streamID > -1)
        write((uint8_t) streamID);

    if (m != SkStreamMode::SM_NULL)
        write((uint8_t) m);

    if (t != SkStreamType::ST_NULL)
        write((uint8_t) t);

    if (esz > -1)
        write((uint8_t) esz);

    if (cnt > -1)
    {
        uint16_t count = cnt;
        write((uint8_t *) &count, sizeof(uint16_t));
    }

    if (data)
        write(data, sz);

    if (pl)
        write(pl, plSz);

    write(B_ETX);
    clear();
}

void SkFnSegmentWriter::onClear()
{
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFnSegmentReader::SkFnSegmentReader() : SkFnSegment()
{
    receiving = false;
    szToReceive = 0;
}


void SkFnSegmentReader::onClear()
{
    receiving = false;
    szToReceive = 0;
}

#if defined(PCSKETCH)
bool SkFnSegmentReader::tick()
{
    long size = clnt->available();

    if (size <= 0)
        return false;

    if (receive() && !isReceiving() && !hasError())
        return true;

    if (hasError())
        skipUntil(B_STX, size);

    return false;
}

bool SkFnSegmentReader::receive()
{
    if (clnt->available() <= 0)
        return false;

    if (!receiving)
    {
        receiving = (read(true)==B_STX);

        if (receiving)
        {
            read();
            cout << "STX captured [START]\n";
        }
    }

    if (receiving)
    {
        if (szToReceive == 0 && clnt->available() >= 2)
        {
            read((uint8_t *) &szToReceive, sizeof(uint16_t));
            cout << "Segment size: " << szToReceive << "\n";
        }

        if (cmd == SkStreamCommand::SC_NULL && clnt->available() >= 2)
        {
            cmd = read();

            if (cmd == SkStreamCommand::SC_STREG
                    || cmd == SkStreamCommand::SC_STNAME
                    || cmd == SkStreamCommand::SC_STOUT
                    || cmd == SkStreamCommand::SC_STMIN
                    || cmd == SkStreamCommand::SC_STMAX
                    || cmd == SkStreamCommand::SC_STUDM)
            {
                streamID = read();
            }
        }

        if (cmd == SkStreamCommand::SC_STREG)
        {
            if (m == SkStreamMode::SM_NULL
                    && clnt->available() >= 2)
            {
                m = (SkStreamMode) read();
                t = (SkStreamType) read();
            }

            if (t == SkStreamType::ST_SINGLE
                    && esz == -1
                    && clnt->available() >= 1)
            {
                esz = read();
                cnt = 1;
            }

            else if (t == SkStreamType::ST_ARRAY
                     && esz == -1
                     && cnt == -1
                     && clnt->available() >= 3)
            {
                esz = read();
                uint16_t z = 0;
                read((uint8_t *) &z, sizeof(uint16_t));
                cnt = z;
            }
        }

        if (cmd != SkStreamCommand::SC_NULL && clnt->available() >= szToReceive+1)
        {
            if (szToReceive)
            {
                uint8_t b[szToReceive];
                read(b, szToReceive);
                addData(b, szToReceive);
                szToReceive = 0;
                cout << "Received all data\n";
            }

            receiving = false;

            if (read() == B_ETX)
                cout << "ETX captured [STOP]\n";

            else
            {
                cout << "ETX not found!\n";
                receiving = false;
                error = true;
                return false;
            }

            return true;
        }
    }

    return false;
}

bool SkFnSegmentReader::isReceiving()
{
    return receiving;
}

#endif

