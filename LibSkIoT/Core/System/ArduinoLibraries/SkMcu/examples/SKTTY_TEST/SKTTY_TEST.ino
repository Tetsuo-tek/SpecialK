#include <sksat.h>
#include <skscheduler.h>

SkSat sat;
SkScheduler schd;
int freeMemID = 0;
int tickTimeID = 0;

void setup()
{
  Serial.begin(9600);
  
  freeMemID = sat.addSingle_O("MEM", sizeof(unsigned long));
  tickTimeID = sat.addSingle_O("TCK", sizeof(float));
  
  sat.begin(Serial, recvCallBack);
  
  String log = "Started [";
  log.concat("IDLE-TickTimeAvg: ");
  log.concat(sat.getIdleTickTimeUS());
  log.concat(" us]");

  sat.msgLog(log.c_str());
  
  /*log = "FreeMemory: ";
  log.concat((int) freeMemID);
  log.concat(" - TickStreamID: ");
  log.concat((int) tickTimeID);
  
  sat.msgLog(log.c_str());*/
  
  schd.add(sysInfoCallBack, 100000);
}

void loop()
{
  schd.tick();
  sat.tick(); 
}

void *ptr = nullptr;;

void sysInfoCallBack()
{
  if (ptr)
  {
    free(ptr);
    ptr = nullptr;
  }
  
  else
    ptr = malloc(800);
    
  unsigned long m = sat.getFreeMemory();
  float t = sat.getLastTickTimeUS();
  
  /*String log = "FreeMemory: ";
  log.concat(m);
  log.concat(" B - TickTimeAvg: ");
  log.concat(t);
  log.concat(" us");

  sat.msgLog(log.c_str());*/
  
  sat.sendData(freeMemID, &m);
  sat.sendData(tickTimeID, &t);
}

void recvCallBack(SkSat *sat, uint8_t id)
{
}
