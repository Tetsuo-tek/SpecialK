#ifndef SKSCHEDULER_H
#define SKSCHEDULER_H

#include "skelapsedtime.h"

class SkScheduler;

typedef struct
{
    uint8_t id;
    bool enabled;
    void (*cb)(SkScheduler *);
    unsigned long intervalUS;
    SkElapsedTimeMicros chrono;
} SkCallBack;

class SkScheduler
{
    public:
        SkScheduler();

        int add(void (*cb)(SkScheduler *), unsigned long intervalUS);
        bool changeInterval(uint8_t id, unsigned long intervalUS);
        bool removeAt(uint8_t id);

        //Call it on loop()
        void tick();

        uint8_t count();
        void clear();

    private:
        SkCallBack **callBacks;
        uint8_t cbCount;

        SkCallBack *addCB();
};

#endif // SKSCHEDULER_H
