#include "sksat.h"
#include "skfreemem.h"
#include "skidlespeedtest.h"

SkSat::SkSat()
{
    recvCB = nullptr;
    clnt = nullptr;
    streams = nullptr;
    streamsCount = 0;
    idleTtickTime_US = 0;
    tempTickCount = 0;
    tickTimeTot_US = 0.f;
    tickTimeAvg_US = 0.f;
}

SkStream *SkSat::addStream()
{
    if (streamsCount == 255)
        return nullptr;

    SkStream *stream = (SkStream *) malloc(sizeof(SkStream));
    stream->id = streamsCount++;
    stream->t = SkStreamType::ST_NULL;
    stream->m = SkStreamMode::SM_NULL;
    stream->name = nullptr;
    stream->container = nullptr;
    stream->range = nullptr;
    stream->isReady = false;

    streams = (SkStream **) realloc(streams, streamsCount*sizeof(SkStream*));
    streams[stream->id] = stream;

    return stream;
}

int SkSat::addStream(const char *name, SkStreamType t, SkStreamMode m, uint8_t elemSize, uint16_t count)
{
    if (t == SkStreamType::ST_PULSE && (elemSize > 0 || count > 0))
        return -1;

    if (t == SkStreamType::ST_SINGLE && (elemSize == 0 || count > 1))
        return -1;

    if (t == SkStreamType::ST_ARRAY && (elemSize == 0 || count == 0))
        return -1;

    if (t == SkStreamType::ST_DYNAMIC && (elemSize == 0 || count > 0))
        return -1;

    SkStream *stream = addStream();

    if (!stream)
        return -1;

    size_t sz = strlen(name);
    stream->name = (char *) malloc(sz+1);
    memcpy(stream->name, name, sz);
    stream->name[sz] = '\0';

    stream->t = t;
    stream->m = m;

    if (stream->t != SkStreamType::ST_PULSE)
    {
        stream->container = (SkStreamData *) malloc(sizeof(SkStreamData));

        stream->container->elemSize = elemSize;
        stream->container->count = count;
        stream->container->udm = nullptr;

        if (t == SkStreamType::ST_SINGLE || t == SkStreamType::ST_ARRAY)
            stream->container->data = malloc(stream->container->elemSize*count);

        else
            stream->container->data = nullptr;
    }

    return stream->id;
}

int SkSat::addPulse_I(const char *name)
{
    return addStream(name, SkStreamType::ST_PULSE, SkStreamMode::SM_INPUT);
}

int SkSat::addPulse_O(const char *name)
{
    return addStream(name, SkStreamType::ST_PULSE, SkStreamMode::SM_OUTPUT);
}

int SkSat::addSingle_I(const char *name, uint8_t elemSize)
{
    return addStream(name, SkStreamType::ST_SINGLE, SkStreamMode::SM_INPUT, elemSize, 1);
}

int SkSat::addSingle_O(const char *name, uint8_t elemSize)
{
    return addStream(name, SkStreamType::ST_SINGLE, SkStreamMode::SM_OUTPUT, elemSize, 1);
}

int SkSat::addArray_I(const char *name, uint8_t elemSize, uint16_t count)
{
    return addStream(name, SkStreamType::ST_ARRAY, SkStreamMode::SM_INPUT, elemSize, count);
}

int SkSat::addArray_O(const char *name, uint8_t elemSize, uint16_t count)
{
    return addStream(name, SkStreamType::ST_ARRAY, SkStreamMode::SM_OUTPUT, elemSize, count);
}

int SkSat::addDynamic_I(const char *name)
{
    return addStream(name, SkStreamType::ST_DYNAMIC, SkStreamMode::SM_INPUT, 1, 1);
}

int SkSat::addDynamic_O(const char *name)
{
    return addStream(name, SkStreamType::ST_DYNAMIC, SkStreamMode::SM_OUTPUT, 1, 1);
}

bool SkSat::setValueRange(uint8_t id, void *min, void *max)
{
    if (id >= streamsCount)
        return false;

    if (streams[id]->t == SkStreamType::ST_PULSE || streams[id]->t == SkStreamType::ST_DYNAMIC)
        return false;

    if (!streams[id]->container)
        return false;

    if (streams[id]->range)
    {
        if (streams[id]->range->min)
            free(streams[id]->range->min);

        if (streams[id]->range->max)
            free(streams[id]->range->max);

        free(streams[id]->range);
    }

    streams[id]->range = (SkStreamRange *) malloc(sizeof(SkStreamRange));

    if (min)
    {
        if (!streams[id]->range->min)
            streams[id]->range->min = malloc(streams[id]->container->elemSize);

        memcpy(streams[id]->range->min, min, streams[id]->container->elemSize);

        if (isOpen())
            streamMinNotify(id);
    }

    if (max)
    {
        if (!streams[id]->range->max)
            streams[id]->range->max = malloc(streams[id]->container->elemSize);

        memcpy(streams[id]->range->max, max, streams[id]->container->elemSize);

        if (isOpen())
            streamMaxNotify(id);
    }

    return true;
}

bool SkSat::setUDM(uint8_t id, const char *udm)
{
    if (id >= streamsCount)
        return false;

    if (streams[id]->t == SkStreamType::ST_PULSE || streams[id]->t == SkStreamType::ST_DYNAMIC)
        return false;

    if (!streams[id]->container)
        return false;

    size_t sz = strlen(udm);
    streams[id]->container->udm = (char *) realloc(streams[id]->container->udm, sz+1);
    memcpy(streams[id]->container->udm, udm, sz);
    streams[id]->container->udm[sz] = '\0';

    if (isOpen())
        streamUdmNotify(id);

    return true;
}

bool SkSat::delStream(uint8_t id)
{
    if (id >= streamsCount)
        return false;

    free(streams[id]->name);

    if (streams[id]->range)
    {
        if (streams[id]->range->min)
            free(streams[id]->range->min);

        if (streams[id]->range->max)
            free(streams[id]->range->max);

        free(streams[id]->range);
    }

    if (streams[id]->container)
    {
        if (streams[id]->container->data)
            free(streams[id]->container->data);

        if (streams[id]->container->udm)
            free(streams[id]->container->udm);

        free(streams[id]->container);
    }

    free(streams[id]);

    if (streamsCount == 1)
    {
        free(streams);
        streamsCount = 0;
        streams = nullptr;
        return true;
    }

    if (id == streamsCount-1)
    {
        streamsCount--;
        streams = (SkStream **) realloc(streams, streamsCount * sizeof(SkStream *));
    }

    else
    {
        SkStream **tempStreams = (SkStream **) malloc((streamsCount-1) * sizeof(SkStream *));

        if (id > 0)
            memcpy(tempStreams, streams, (id * sizeof(SkStream *)));

        if (id < streamsCount-1)
            memcpy(&tempStreams[id], &streams[id+1], (streamsCount-id) * sizeof(SkStream *));

        streamsCount--;
        free(streams);
        streams = tempStreams;
    }

    return true;
}

bool SkSat::removeAt(uint8_t id)
{
    return delStream(id);
}

bool SkSat::remove(const char *name)
{
    int id = streamID(name);

    if (id > -1)
        return removeAt(id);

    return false;
}

bool SkSat::begin(FnClient &c, void (*recvCallBack)(SkSat *, uint8_t))
{
    if (isOpen())
        return false;

    SkIdleSpeedTest test(2000000L);
    while(!test.tick());
    idleTtickTime_US = test.getIdleTickTimeUS();

    clnt = &c;
    cmd.setup(c);

    recvCB = recvCallBack;

    tickTimeTot_US = 0.f;
    tickTimeAvg_US = 0.f;
    tempTickCount = 0;

    sendSatHeader();

    tickChrono.start();
    oneSecChrono.start();

    return true;
}

void SkSat::sendSatHeader()
{
    for(uint8_t i=0; i<streamsCount; i++)
        streamNotify(i);
}

void SkSat::streamNotify(uint8_t id)
{
    cmd.setCommand(SkStreamCommand::SC_STREG);
    cmd.setStreamID(id);
    cmd.setStreamMode((SkStreamMode) streams[id]->m);
    cmd.setStreamType((SkStreamType) streams[id]->t);

    if (streams[id]->t == SkStreamType::ST_PULSE || streams[id]->t == SkStreamType::ST_DYNAMIC)
        cmd.send();

    else if (streams[id]->t == SkStreamType::ST_SINGLE)
    {
        cmd.setStreamElemSize(streams[id]->container->elemSize);
        cmd.send();
    }

    else if (streams[id]->t == SkStreamType::ST_ARRAY)
    {
        cmd.setStreamElemSize(streams[id]->container->elemSize);
        cmd.setStreamCount(streams[id]->container->count);
        cmd.send();
    }

    streamNameNotify(id);
    streamUdmNotify(id);
    streamMinNotify(id);
    streamMaxNotify(id);
}

void SkSat::streamNameNotify(uint8_t id)
{
    cmd.setCommand(SkStreamCommand::SC_STNAME);
    cmd.setStreamID(id);
    cmd.setPayload(streams[id]->name, strlen(streams[id]->name));
    cmd.send();
}

void SkSat::streamUdmNotify(uint8_t id)
{
    if (!streams[id]->container || !streams[id]->container->udm)
        return;

    cmd.setCommand(SkStreamCommand::SC_STUDM);
    cmd.setStreamID(id);
    cmd.setPayload(streams[id]->container->udm, strlen(streams[id]->container->udm));
    cmd.send();
}

void SkSat::streamMinNotify(uint8_t id)
{
    if (!streams[id]->range || !streams[id]->range->min)
        return;

    cmd.setCommand(SkStreamCommand::SC_STMIN);
    cmd.setStreamID(id);
    cmd.setPayload(streams[id]->range->min, streams[id]->container->elemSize);
    cmd.send();
}

void SkSat::streamMaxNotify(uint8_t id)
{
    if (!streams[id]->range || !streams[id]->range->max)
        return;

    cmd.setCommand(SkStreamCommand::SC_STMAX);
    cmd.setStreamID(id);
    cmd.setPayload(streams[id]->range->max, streams[id]->container->elemSize);
    cmd.send();
}

bool SkSat::isOpen()
{
    return (clnt != nullptr);
}

bool SkSat::sendCheck(uint8_t id)
{
    if (id >= streamsCount)
        return false;

    if (streams[id]->m != SM_OUTPUT)
        return false;

    if (streams[id]->isReady)
        return false;

    return true;
}

bool SkSat::sendPulse(uint8_t id)
{
    if (!sendCheck(id))
        return false;

    if (streams[id]->t != SkStreamType::ST_PULSE)
        return false;

    streams[id]->isReady = true;
    return true;
}

bool SkSat::sendData(uint8_t id, void *data)
{
    if (!sendCheck(id))
        return false;

    if (streams[id]->t != SkStreamType::ST_SINGLE && streams[id]->t != SkStreamType::ST_ARRAY)
        return false;

    memcpy(streams[id]->container->data, data, streams[id]->container->elemSize*streams[id]->container->count);
    streams[id]->isReady = true;
    return true;
}

bool SkSat::sendData(uint8_t id, void *data, uint16_t size)
{
    if (!sendCheck(id))
        return false;

    if (streams[id]->t != SkStreamType::ST_DYNAMIC)
        return false;

    streams[id]->container->elemSize = size;
    streams[id]->container->data = realloc(streams[id]->container->data, size);
    memcpy(streams[id]->container->data, data, size);
    streams[id]->isReady = true;
    return true;
}

void *SkSat::getData(uint8_t id)
{
    if (id >= streamsCount)
        return nullptr;

    if (streams[id]->t == SkStreamType::ST_PULSE)
        return nullptr;

    return streams[id]->container->data;
}

void SkSat::tick()
{
    tickTimeTot_US += tickChrono.stop();
    tempTickCount++;

    if (oneSecChrono.stop() >= 1000)
    {
        tickTimeAvg_US = tickTimeTot_US/(float)tempTickCount;
        tempTickCount = 0;
        tickTimeTot_US = 0;
        oneSecChrono.start();
    }

    receive();
    send();

    tickChrono.start();
}

void SkSat::receive()
{
    /*if (!cmd.receive())
        return;

    SkStreamCommand c = cmd.getCommand();

    if (c == SkStreamCommand::SC_PING)
    {
        cmd.setCommand(SkStreamCommand::SC_PONG);
        cmd.send();
    }

    else  if (c == SkStreamCommand::SC_PONG)
    {}

    else if (c == SkStreamCommand::SC_STOUT)
    {
        int id = cmd.getStreamID();

        if (id > -1)
            recvCB(this, (uint8_t) id);
    }

    cmd.clear();*/
}

void SkSat::send()
{
    for(uint8_t i=0; i<streamsCount; i++)
        if (streams[i]->t == SkStreamMode::SM_OUTPUT && streams[i]->isReady)
            send(streams[i]);
}

void SkSat::send(SkStream *s)
{

/*
    ST_PULSE,       //4                     <START>CSZ(uint16_t),CMD(uint8_t),id(uint8_t)<STOP>
    ST_SINGLE,      //4+elemSize            <START>CSZ(uint16_t),CMD(uint8_t),id(uint8_t),data(elemSize)<STOP>
    ST_ARRAY,       //4+(count*elemSize)    <START>CSZ(uint16_t),CMD(uint8_t),id(uint8_t),data(elemSize*count)<STOP>
    ST_DYNAMIC      //6+(count*elemSize)    <START>CSZ(uint16_t),CMD(uint8_t),id(uint8_t),count(uint16_t),data(1*count)<STOP>
*/

    cmd.setCommand(SkStreamCommand::SC_STOUT);
    cmd.setStreamID(s->id);

    if (s->t == SkStreamType::ST_SINGLE || s->t == SkStreamType::ST_ARRAY)
        cmd.setPayload((uint8_t *) s->container->data, (s->container->elemSize * s->container->count));

    else if (s->t == SkStreamType::ST_DYNAMIC)
    {
        cmd.addValue_U16(s->container->count);
        cmd.setPayload((uint8_t *) s->container->data, s->container->count);
    }

    cmd.send();

    s->isReady = false;
}

uint8_t SkSat::count()
{
    return streamsCount;
}

void SkSat::clear()
{
    if (streams)
    {
        for(uint8_t i=0; i<streamsCount; i++)
            free(streams[i]);

        free(streams);
        streams = nullptr;
        streamsCount = 0;
    }
}

void SkSat::end()
{
    if (!isOpen())
        return;

    clnt = nullptr;
    clear();
}

const char *SkSat::streamName(uint8_t id)
{
    if (id >= streamsCount || !streams[id])
        return nullptr;

    return streams[id]->name;
}

int SkSat::streamID(const char *name)
{
    size_t sz = strlen(name);

    for(uint8_t i=0; i<streamsCount; i++)
        if (sz == strlen(name) && memcmp(name, streams[i]->name, sz) == 0)
            return i;

    return -1;
}

unsigned long SkSat::getFreeMemory()
{
    return skFreeMemory();
}

float SkSat::getIdleTickTimeUS()
{
    return idleTtickTime_US;
}

float SkSat::getLastTickTimeUS()
{
    return tickTimeAvg_US;
}

void SkSat::msgLog(const char *text)
{
    cmd.setCommand(SkStreamCommand::SC_STMSGLOG);
    cmd.setPayload((void *) text, strlen(text));
    cmd.send();
}

void SkSat::wrnLog(const char *text)
{
    cmd.setCommand(SkStreamCommand::SC_STWRNLOG);
    cmd.setPayload((void *) text, strlen(text));
    cmd.send();
}

void SkSat::errLog(const char *text)
{
    cmd.setCommand(SkStreamCommand::SC_STERRLOG);
    cmd.setPayload((void *) text, strlen(text));
    cmd.send();
}
