HEADERS +=  \
    $$PWD/skelapsedtime.h \
    $$PWD/skfnsegment.h \
    $$PWD/skfreemem.h \
    $$PWD/skidlespeedtest.h \
    $$PWD/skmcu.h \
    $$PWD/sksat.h \
    $$PWD/skscheduler.h

SOURCES +=  \
    $$PWD/skelapsedtime_mcu.cpp \
    $$PWD/skfnsegment.cpp \
    $$PWD/skfreemem.cpp \
    $$PWD/skidlespeedtest.cpp \
    $$PWD/skmcu.cpp \
    $$PWD/sksat.cpp \
    $$PWD/skscheduler.cpp

DISTFILES +=
