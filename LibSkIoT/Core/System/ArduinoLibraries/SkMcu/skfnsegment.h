#ifndef SKFNSEGMENT_H
#define SKFNSEGMENT_H

#include "skmcu.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define B_NUL     0x00
#define B_SOH     0x01
#define B_STX     0x02
#define B_ETX     0x03
#define B_EOT     0x04
#define B_ENQ     0x05
#define B_ACK     0x06
#define B_BEL     0x07
#define B_BS      0x08
#define B_TAB     0x09
#define B_LF      0x0A
#define B_VT      0x0B
#define B_FF      0x0C
#define B_CR      0x0D
#define B_SO      0x0E
#define B_SI      0x0F

#define B_DLE     0x10
#define B_DC1     0x11
#define B_DC2     0x12
#define B_DC3     0x13
#define B_DC4     0x14
#define B_NAK     0x15
#define B_SYN     0x16
#define B_ETB     0x17
#define B_CAN     0x18
#define B_EM      0x19
#define B_SUB     0x1A
#define B_ESC     0x1B
#define B_FS      0x1C
#define B_GS      0x1D
#define B_RS      0x1E
#define B_US      0x1F

#define B_DEL     0x7F

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
    <STX>   [uint8_t]  Command START
    <CSZ>   [uint16_t] Command TOTAL-SIZE; STX, CSZ, CMD, ID and ETX are excuded
    <CMD>   [uint8_t]  Command flag
    <ID>    [uint8_t]  StreamID
    <M>     [uint8_t]  StreamMode
    <T>     [uint8_t]  StreamTYpe
    <ESZ>   [uint8_t]  StreamElementSize
    <CNT>   [uint16_t] StreamElementsCount
    <NAM>   [cstr]     StreamName
    <UDM>   [cstr]     StreamValueUDM
    <MIN>   [elemSize] StreamMinValue
    <MAX>   [elemSize] StreamMaxValue
    <ETX>   [uint8_t]  Command END
*/

typedef enum
{
    SC_NULL,

    //BETWEEN SATs
    SC_PING,        //<STX><CSZ><CMD><ETX>
    SC_PONG,        //<STX><CSZ><CMD><ETX>

    //To HUB
    SC_STREG,       //PULSE     -> <STX><CSZ><CMD><ID><M><T><ETX>
                    //SINGLE    -> <STX><CSZ><CMD><ID><M><T><ESZ><ETX>
                    //ARRAY     -> <STX><CSZ><CMD><ID><M><T><ESZ><CNT><ETX>
                    //DYNAMIC   -> <STX><CSZ><CMD><ID><M><T><ETX>


    SC_STNAME,      //<STX><CSZ><CMD><ID><NAM><ETX>
    SC_STUDM,       //<STX><CSZ><CMD><ID><UDM><ETX>
    SC_STMIN,       //<STX><CSZ><CMD><ID><MIN><ETX>
    SC_STMAX,       //<STX><CSZ><CMD><ID><MAX><ETX>

    //To/From HUB
    SC_STOUT,       //PULSE     -> <STX><CSZ><CMD><ID><ETX>
                    //SINGLE    -> <STX><CSZ><CMD><ID><DATA(elemSize)><ETX>
                    //ARRAY     -> <STX><CSZ><CMD><ID><DATA(elemSize*count)><ETX>
                    //DYNAMIC   -> <STX><CSZ><CMD><ID><DATA(count)><ETX>

    SC_STMSGLOG,    //<STX><CSZ><CMD><text><ETX>
    SC_STWRNLOG,    //<STX><CSZ><CMD><text><ETX>
    SC_STERRLOG     //<STX><CSZ><CMD><text><ETX>

} SkStreamCommand;

typedef enum
{
    ST_NULL,
    ST_PULSE,       //NO-DATA
    ST_SINGLE,      //DATA of elemSize Bytes as fixed length
    ST_ARRAY,       //DATA of elemSize*count Bytes as fixed length
    ST_DYNAMIC      //DATA of count Bytes as dynamic length
} SkStreamType;

typedef enum
{
    SM_NULL,
    SM_INPUT,       //Hub -> TO THIS DEVICE
    SM_OUTPUT       //Hub <- FROM THIS DEVICE
} SkStreamMode;

typedef enum
{
    SDT_INT8,
    SDT_UINT8,
    SDT_INT16,
    SDT_UINT16,
    SDT_INT24,
    SDT_UINT24,
    SDT_INT32,
    SDT_UINT32,
    SDT_INT64,
    SDT_UINT64,
    SDT_FLOAT,
    SDT_DOUBLE
} SkStreamDataType;
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFnSegment
{
    protected:
        SkFnSegment();
        virtual ~SkFnSegment();

    public:
        void setup(FnClient &c);
        SkStreamCommand getCommand();
        int getStreamID();
        SkStreamType getStreamType();
        SkStreamMode getStreamMode();
        int getStreamElemSize();
        int getStreamCount();
        uint8_t *getData(uint16_t offset=0);
        uint16_t size();
        void clear();
        bool hasError();

#if defined(PCSKETCH)
        static const char *cmdToString(SkStreamCommand command);
#endif

    protected:
        FnClient *clnt;
        uint8_t cmd;
        bool error;

        //optional values
        int streamID;
        SkStreamMode m;
        SkStreamType t;
        int esz;
        int cnt;

        uint8_t *data;
        uint16_t sz;

        uint8_t *pl;
        uint16_t plSz;

        void addData(void *d, uint16_t size);

        void write(uint8_t b);
        void write(uint8_t *b, uint16_t size);

        void skipUntil(uint8_t b, long size);

        uint8_t read(bool peeking=false);
        int read(uint8_t *b, uint16_t size);

        virtual void onClear(){}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFnSegmentWriter : public SkFnSegment
{
    public:
        SkFnSegmentWriter();

        void setCommand(SkStreamCommand command);
        void setStreamID(uint8_t id);
        void setStreamType(SkStreamType type);
        void setStreamMode(SkStreamMode mode);
        void setStreamElemSize(uint8_t size);
        void setStreamCount(uint8_t count);

        void addValue_U8(uint8_t v);
        void addValue_S8(int8_t v);
        void addValue_U16(uint16_t v);
        void addValue_S16(int16_t v);
        void addValue_U24(uint32_t v);
        void addValue_S24(int32_t v);
        void addValue_U32(uint32_t v);
        void addValue_S32(int32_t v);
        void addValue_U64(uint64_t v);
        void addValue_S64(int64_t v);

        void setPayload(void *payload, uint16_t size);

        //Command SEND is SYNCHRONOUS
        void send();

    private:
        void onClear() override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkFnSegmentReader : public SkFnSegment
{
    public:
        SkFnSegmentReader();

#if defined(PCSKETCH)
        bool tick();
        bool isReceiving();
#endif

    private:
        bool receiving;
        uint16_t szToReceive;

#if defined(PCSKETCH)
        //return true if the received command is complete
        bool receive();
#endif

        void onClear() override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKFNSEGMENT_H
