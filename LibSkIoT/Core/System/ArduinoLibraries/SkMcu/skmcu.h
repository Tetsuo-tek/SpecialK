#ifndef SKMCU_H
#define SKMCU_H

#define MAXLOGSTR       80
#define MCU_MSGLOGPFX   "Msg -> "
#define MCU_WRNLOGPFX   "Wrn -> "
#define MCU_ERRLOGPFX   "Err -> "

#if defined(PCSKETCH)
    #if defined(QT_SPECIALK_APPLICATION) || defined(QT_CORE_LIB)
        #include <QIODevice>
        #define FnClient    QIODevice

        #include "iostream"
        using namespace std;

    #elif defined(SPECIALK_APPLICATION)
        #include <Core/System/skabstractdevice.h>
        #define FnClient    SkAbstractDevice

    #endif


    #define available       bytesAvailable

#else
    #include <Arduino.h>

    #if !defined(nullptr)
        #define nullptr     NULL
    #endif

    #define FnClient        Stream

    #define MCU_LOG
    #define MCU_LOGDEV      Serial

#endif

void skMsgLog(const char *text);
void skWrnLog(const char *text);
void skErrLog(const char *text);

#endif // SKMCU_H
