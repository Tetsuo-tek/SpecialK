#ifndef SKSAT_H
#define SKSAT_H

#include "skelapsedtime.h"
#include "skfnsegment.h"

typedef struct
{
    //uint8_t t;                  //It is a SkStreamDataType
    uint8_t elemSize;           //not used when Stream->t is PULSE; becomes size when Stream->t is DYNAMIC
    uint16_t count;             //not used when Stream->t is DYNAMIC
    void *data;                 //not used when Stream->t is PULSE
    char *udm;                  //optional
} SkStreamData;

typedef struct
{
    void *min;                  //optional (len: elemSize)
    void *max;                  //optional (len: elemSize)
} SkStreamRange;                //optional

typedef struct
{
    uint8_t id;

    //MUST ADD enabled and MUST PUT it and isReady inside a subStructure SkStreamOutput
    bool isReady;               //not used when Stream->m is INPUT

    char *name;
    uint8_t m;                  //SkStreamMode
    uint8_t t;                  //SkStreamType
    SkStreamData *container;    //not used when Stream->t is PULSE
    SkStreamRange *range;       //optional
} SkStream;

class SkSat
{
    public:
        SkSat();

        //Streams MUST be added BEFORE to begin
        //add* return the id of the built stream
        int addPulse_I(const char *name);
        int addPulse_O(const char *name);
        int addSingle_I(const char *name, uint8_t elemSize);
        int addSingle_O(const char *name, uint8_t elemSize);
        int addArray_I(const char *name, uint8_t elemSize, uint16_t count);
        int addArray_O(const char *name, uint8_t elemSize, uint16_t count);
        int addDynamic_I(const char *name);
        int addDynamic_O(const char *name);

        //elemSize is the size for pointers min and max
        bool setValueRange(uint8_t id, void *min, void *max);

        bool setUDM(uint8_t id, const char *udm);

        bool removeAt(uint8_t id);
        bool remove(const char *name);

        //It MUST begin AFTER the streams are added
        bool begin(FnClient &c, void (*recvCallBack)(SkSat *, uint8_t));

        bool isOpen();

        //sendData* real-actions happen on tick(), so they are asynchronous requests

        //PULSE OUTPUT
        bool sendPulse(uint8_t id);

        //SINGLE(count is 0) / ARRAY(count is known) OUTPUT (and elemSize is known)
        bool sendData(uint8_t id, void *data);

        //DYNAMIC OUTPUT (count is unkonwn)
        bool sendData(uint8_t id, void *data, uint16_t size);

        void *getData(uint8_t id);

        //Call it on loop()
        void tick();

        uint8_t count();
        void clear();
        void end();

        //return NULL if "id" is NOT found
        const char *streamName(uint8_t id);

        //return -1 if "name" is NOT found
        int streamID(const char *name);

        unsigned long getFreeMemory();
        float getIdleTickTimeUS();
        float getLastTickTimeUS();

        void msgLog(const char *text);
        void wrnLog(const char *text);
        void errLog(const char *text);

    private:
        FnClient *clnt;
        SkStream **streams;
        uint8_t streamsCount;

        float idleTtickTime_US;

        SkElapsedTimeMillis oneSecChrono;
        SkElapsedTimeMicros tickChrono;
        float tickTimeTot_US;
        float tickTimeAvg_US;
        unsigned long tempTickCount;

        SkFnSegmentWriter cmd;

        void (*recvCB)(SkSat *, uint8_t);

        SkStream *addStream();
        int addStream(const char *name, SkStreamType t, SkStreamMode m, uint8_t elemSize=0, uint16_t count=0);
        bool delStream(uint8_t id);

        void sendSatHeader();

        bool sendCheck(uint8_t id);
        void send();
        void send(SkStream *s);

        void receive();

        void streamNotify(uint8_t id);
        void streamNameNotify(uint8_t id);
        void streamUdmNotify(uint8_t id);
        void streamMinNotify(uint8_t id);
        void streamMaxNotify(uint8_t id);
};

template<typename T> T &getData(SkSat *sat, uint8_t id)
{
    return *((T*) sat->getData(id));
}

#endif // SKSAT_H
