#include "skidlespeedtest.h"

SkIdleSpeedTest::SkIdleSpeedTest(unsigned long idleCheckTicks)
{
    ticks = idleCheckTicks;
    counter = 0;
    idleTickTimeUs = 0;
    checked = false;
}

bool SkIdleSpeedTest::tick()
{
    if (counter == 0)
        start();

    else if(counter == ticks)
    {
      idleTickTimeUs = ((float) stop() / (float) ticks) * 1000.0;
      counter = 0;
      checked = true;
      return true;
    }

    counter++;
    return false;
}

bool SkIdleSpeedTest::isChecked()
{
    return checked;
}

float SkIdleSpeedTest::getIdleTickTimeUS()
{
    return idleTickTimeUs;
}

void SkIdleSpeedTest::reset()
{
    counter = 0;
    idleTickTimeUs = 0;
    checked = false;
}
