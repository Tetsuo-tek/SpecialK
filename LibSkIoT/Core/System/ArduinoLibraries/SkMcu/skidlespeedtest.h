#ifndef SKIDLESPEEDTEST_H
#define SKIDLESPEEDTEST_H

#include <skelapsedtime.h>

class SkIdleSpeedTest : private SkElapsedTimeMillis
{
    public:
        SkIdleSpeedTest(unsigned long idleCheckTicks);

        bool tick();
        bool isChecked();
        float getIdleTickTimeUS();
        void reset();

    private:
        float idleTickTimeUs;
        unsigned long ticks;
        unsigned long counter;
        bool checked;
};

#endif //SKIDLESPEEDTEST_H
