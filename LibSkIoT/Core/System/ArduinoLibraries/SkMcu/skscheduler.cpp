#include "skscheduler.h"

SkScheduler::SkScheduler()
{
    callBacks = nullptr;
    cbCount = 0;
}

SkCallBack *SkScheduler::addCB()
{
    if (cbCount == 255)
        return nullptr;

    //NOT using malloc here
    //because there is SkElapsedTimeMicros having its own constructor
    SkCallBack *callBack = new SkCallBack;

    callBack->id = cbCount++;
    callBack->enabled = false;
    callBack->intervalUS = 0;

    callBacks = (SkCallBack **) realloc(callBacks, cbCount*sizeof(SkCallBack*));
    callBacks[callBack->id] = callBack;

    return callBack;
}

int SkScheduler::add(void (*cb)(SkScheduler *), unsigned long intervalUS)
{
    if (!intervalUS)
        return -1;

    SkCallBack *callBack = addCB();

    if (!callBack)
        return -1;

    callBack->cb = cb;
    callBack->intervalUS = intervalUS;
    callBack->enabled = true;
    callBack->chrono.start();

    return callBack->id;
}

bool SkScheduler::changeInterval(uint8_t id, unsigned long intervalUS)
{
    if (id >= cbCount)
        return false;

    if (!intervalUS)
        return false;

    callBacks[id]->intervalUS = intervalUS;
    callBacks[id]->chrono.start();

    return true;
}

bool SkScheduler::removeAt(uint8_t id)
{
    if (id >= cbCount)
        return false;

    //NOT using free() here
    //because there it is allocated with new
    delete callBacks[id];

    if (cbCount == 1)
    {
        free(callBacks);
        cbCount = 0;
        callBacks = nullptr;
        return true;
    }

    if (id == cbCount-1)
    {
        cbCount--;
        callBacks = (SkCallBack **) realloc(callBacks, cbCount * sizeof(SkCallBack *));
    }

    else
    {
        SkCallBack **tempCallBacks = (SkCallBack **) malloc((cbCount-1) * sizeof(SkCallBack *));

        if (id > 0)
            memcpy(tempCallBacks, callBacks, (id * sizeof(SkCallBack *)));

        if (id < cbCount-1)
            memcpy(&tempCallBacks[id], &callBacks[id+1], (cbCount-id) * sizeof(SkCallBack *));

        cbCount--;
        free(callBacks);
        callBacks = tempCallBacks;
    }

    return true;
}

void SkScheduler::tick()
{
    for(uint8_t i=0; i<cbCount; i++)
        if (callBacks[i]->chrono.check(callBacks[i]->intervalUS))
        {
            if (callBacks[i]->enabled)
                callBacks[i]->cb(this);

            callBacks[i]->chrono.start();
        }
}

uint8_t SkScheduler::count()
{
    return cbCount;
}

void SkScheduler::clear()
{
    if (callBacks)
    {
        //NOT using free() here
        //because there it is allocated with new
        for(uint8_t i=0; i<cbCount; i++)
            delete callBacks[i];

        free(callBacks);
        callBacks = nullptr;
        cbCount = 0;
    }
}


