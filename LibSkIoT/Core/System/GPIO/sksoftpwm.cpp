#include "sksoftpwm.h"
#include "skgpio.h"

ConstructorImpl(SkSoftPwm, SkThread)
{
    freq = 0;
    freqIntervalUS = 0;
    duty = 0;
    currentIntervalUS = 0;
    boardPin = -1;
    pin = nullptr;

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
}

void SkSoftPwm::setup(int digitalOutputPin, uint frequency, uint8_t resolutionBits)
{
    boardPin = digitalOutputPin;

    freq = frequency;
    double secs = 1. / freq;
    freqIntervalUS = secs*1000000;

    setLoopIntervals(freqIntervalUS/pow(2, resolutionBits), freqIntervalUS, SkLoopTimerMode::SK_TIMEDLOOP_RT);

    ObjectMessage("PWM Frequency is: " << freq << " Hz [" << freqIntervalUS << " us]");
}

void SkSoftPwm::setDuty(float v)
{
    if (v > 1.)
        return;

    duty = v;
    currentIntervalUS = freqIntervalUS * duty;
    ObjectMessage("PWM DutyCycle is: " << duty*100 << " % [" << currentIntervalUS << "/" << freqIntervalUS << " us]");
}

bool SkSoftPwm::customRunSetup()
{
    Attach(thEventLoop()->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkDirect);
    Attach(thEventLoop()->slowZone_SIG, pulse, this, slowTick, SkAttachMode::SkDirect);
    Attach(thEventLoop()->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkDirect);

    pin = new SkGPIO(boardPin);
    pin->setup(SkPinType::GPT_DGT, SkPinMode::GP_OUTPUT);

    return true;
}

void SkSoftPwm::customClosing()
{
    pin->write(LOW);
    delete pin;
    pin = nullptr;
}

static bool b = false;
SlotImpl(SkSoftPwm, fastTick)
{
    SilentSlotArgsWarning();

    if (b && chrono.stop() >= currentIntervalUS)
    {
        pin->write(LOW);
        b = false;
        //chrono.start();
    }
}

SlotImpl(SkSoftPwm, slowTick)
{
    SilentSlotArgsWarning();

    if (!b && duty > 0)
    {
        pin->write(HIGH);
        b = true;
        chrono.start();
    }
}

SlotImpl(SkSoftPwm, oneSecTick)
{
    SilentSlotArgsWarning();

    //cout << thEventLoop()->getLastPulseElapsedTimeAverage() << "us\n";
}
