#ifndef SKI2CBUS_H
#define SKI2CBUS_H

#include "Core/Object/skflatobject.h"

#if defined(ENABLE_I2CLIB)

#define I2C_BUF_MAX 1024

class SkI2cBus extends SkFlatObject
{
    public:
        SkI2cBus();
        ~SkI2cBus();

        int setup();
        int setSlave(char slave_addr);
        int readBytes(char reg_addr, char *data, uint64_t length);
        int readByte(char reg_addr, char *data);
        int writeByte(char reg_addr, char byte);
        int writeRawByte(char byte);
        int writeBytes(char reg_addr, char *data, uint64_t length);

    private:
        int fd;
};

#endif

#endif // SKI2CBUS_H
