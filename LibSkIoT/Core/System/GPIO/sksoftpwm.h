#ifndef SKSOFTPWM_H
#define SKSOFTPWM_H

#include "Core/System/Thread/skthread.h"

class SkGPIO;

class SkSoftPwm extends SkThread
{
    public:
        Constructor(SkSoftPwm, SkThread);

        void setup(int digitalOutputPin, uint frequency, uint8_t resolutionBits);
        void setDuty(float v);//[0 , 1]

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

    private:
        uint freq;
        uint freqIntervalUS;
        float duty;
        int currentIntervalUS;
        int boardPin;
        SkGPIO *pin;

        SkElapsedTimeMicros chrono;

        bool customRunSetup()   override;
        void customClosing()    override;
};

#endif // SKSOFTPWM_H
