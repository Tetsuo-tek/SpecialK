#include "ski2cbus.h"

#if defined(ENABLE_I2CLIB)

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>

#include <linux/i2c-dev.h>

SkI2cBus::SkI2cBus()
{
    fd = -1;
}

SkI2cBus::~SkI2cBus()
{
    close(fd);
}

int SkI2cBus::setup()
{
    int adapter_nr;
    char filename[20];

    // setup
    adapter_nr = 1;  // probably dynamically determined
    memset(filename, '\0', sizeof(char) * 20);
    snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);

    // open i2c connection
    fd = open(filename, O_RDWR);

    if (fd < 0)
    {
        fd = -1;
        return -1;
    }

    return 0;
}

int SkI2cBus::setSlave(char slave_addr)
{
    return ioctl(fd, I2C_SLAVE , slave_addr);
}

int SkI2cBus::readByte(char reg_addr, char *data)
{
    char buf[1];

    buf[0] = reg_addr;

    if (write(fd, buf, 1) != 1)
        return -1;

    if (read(fd, data, 1) != 1)
        return -2;

    return 0;
}

int SkI2cBus::readBytes(char reg_addr, char *data, uint64_t length)
{
    char buf[1];

    buf[0] = reg_addr;

    if (write(fd, buf, 1) != 1)
        return -1;

    if (read(fd, data, length) != static_cast<int>(length))
        return -2;

    return 0;
}

int SkI2cBus::writeByte(char reg_addr, char byte)
{
    char buf[2];

    buf[0] = reg_addr;
    buf[1] = byte;

    if (write(fd, buf, 2) != 1)
        return -1;

    return 0;
}

int SkI2cBus::writeRawByte(char byte)
{
    if (write(fd, &byte, 1) != 1)
        return -1;

    return 0;
}

int SkI2cBus::writeBytes(char reg_addr, char *data, uint64_t length)
{
    int i;
    char buf[I2C_BUF_MAX];

    //create buf
    memset(buf, '\0', sizeof(char) * I2C_BUF_MAX);
    buf[0] = reg_addr;

    for (i = 1; i < (int) length + 1; i++)
        buf[i] = data[i];

    //write bytes
    if (write(fd, buf, length + 1) != 1)
        return -1;

    return 0;
}

#endif
