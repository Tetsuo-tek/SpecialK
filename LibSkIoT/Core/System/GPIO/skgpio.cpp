#include "skgpio.h"
#include "Core/sklogmachine.h"

struct SkIoSystemInit
{
    SkIoSystemInit() {init();}

    void init()
    {
#if defined(RASPBERRY)
        AssertKiller(!bcm2835_init());
#endif

        enabled = true;
    }

    ~SkIoSystemInit() {quit();}

    void quit()
    {
        if (!enabled)
            return;

#if defined(RASPBERRY)
        bcm2835_close();
#endif
        enabled = false;
    }

    private:
        bool enabled;
};

static SkIoSystemInit GpioSystem;

#include <unistd.h>

void waitForMicros(uint64_t micros)
{
#if defined(RASPBERRY)
    bcm2835_delayMicroseconds(micros);
#else
    usleep(micros);
#endif
}

void waitForMillis(uint64_t millis)
{
    #if defined(RASPBERRY)
        bcm2835_delay(millis);
    #else
        usleep(millis*1000);
    #endif
}

void waitForSeconds(uint64_t secs)
{
    sleep(secs);
}

#if defined(RASPBERRY)
    #define PIN(P) static_cast<RPiGPIOPin>(P)
#else
    #define PIN(P) P
#endif

SkGPIO::SkGPIO(int boardPin)
{
    pin = boardPin;
    mode = SkPinMode::GP_NONE;
    type = SkPinType::GPT_NONE;
}

void SkGPIO::setup(SkPinType pinType, SkPinMode pinMode)
{
    mode = pinMode;
    type = pinType;

#if defined(RASPBERRY)
    if (mode == SkPinMode::GP_INPUT)
        bcm2835_gpio_fsel(PIN(pin), BCM2835_GPIO_FSEL_INPT);

    else if (mode == SkPinMode::GP_OUTPUT)
        bcm2835_gpio_fsel(PIN(pin), BCM2835_GPIO_FSEL_OUTP);
#endif
}

void SkGPIO::write(uint64_t val)
{
#if defined(RASPBERRY)
    bcm2835_gpio_write(PIN(pin), static_cast<uint8_t>(val));
#else
    ArgNotUsed(val);
#endif
}

uint64_t SkGPIO::read()
{
#if defined(RASPBERRY)
    return bcm2835_gpio_lev(PIN(pin));
#else
    return 0;
#endif
}

SkPinType SkGPIO::getType()
{
    return type;
}

SkPinMode SkGPIO::getMode()
{
    return mode;
}

int SkGPIO::getBoardPin()
{
    return pin;
}

uint64_t SkGPIO::waitForValue(uint64_t val, uint64_t timeout_micros)
{
    chrono.start();

    while(chrono.stop()<timeout_micros)
        if (read() == val)
            return chrono.stop();

    return timeout_micros;
}
