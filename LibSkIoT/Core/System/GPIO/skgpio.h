#ifndef SKGPIO_H
#define SKGPIO_H

#include "Core/System/Time/skelapsedtime.h"

#if defined(RASPBERRY)
    #include "Core/System/GPIO/RPi/bcm2835.h"
#else
    #define HIGH    1
    #define LOW     0
#endif

enum SkPinMode
{
    GP_NONE,
    GP_INPUT,
    GP_OUTPUT
};

enum SkPinType
{
    GPT_NONE,
    GPT_DGT,
    GPT_ADC,
    GPT_DAC,
    GPT_PWM
};

void waitForMicros(uint64_t micros);
void waitForMillis(uint64_t millis);
void waitForSeconds();

class SkGPIO extends SkFlatObject
{
    public:
        SkGPIO(int boardPin);

        void setup(SkPinType pinType, SkPinMode pinMode);

        void write(uint64_t val);
        uint64_t read();

        //it is a BUSY-WAITING JAIL
        uint64_t waitForValue(uint64_t val, uint64_t timeout_micros);

        SkPinType getType();
        SkPinMode getMode();
        int getBoardPin();

    private:
        int pin;
        SkPinMode mode;
        SkPinType type;
        SkElapsedTimeMicros chrono;
};

#endif // SKGPIO_H
