#ifndef SKREVPICTRL_H
#define SKREVPICTRL_H

#include "Core/Object/skflatobject.h"
#include "piControl.h"

class SkRevPiCtrl extends SkFlatObject
{
    public:
        SkRevPiCtrl();

        bool open();
        void close();

        bool isOpen();

        int read(uint32_t offset, uint32_t Length, uint8_t *pData);
        int write(uint32_t offset, uint32_t Length, uint8_t *pData);

        int getDeviceInfo(SDeviceInfo *pDev);
        int getDeviceInfoList(SDeviceInfo *pDev);

        int getBitValue(SPIValue *pSpiValue);
        int setBitValue(SPIValue *pSpiValue);

        int getVariableInfo(SPIVariable *pSpiVariable);
        int findVariable(CStr *name);

        bool reset();

        static CStr *moduleTypeName(uint16_t t);

    private:
        int fd;

};

#endif // SKREVPICTRL_H
