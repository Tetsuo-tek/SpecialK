#include "skrevpictrl.h"
#include "piControl.h"

#include "Core/sklogmachine.h"
#include "Core/Containers/sktreemap.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

StaticAutoLaunch(SkRevPiStatic, revPiStatic)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkRevPiCtrl::SkRevPiCtrl()
{
    fd = -1;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkRevPiCtrl::open()
{
    if (isOpen())
    {
        FlatError("Device is ALREADY open");
        return false;
    }

    fd = ::open(PICONTROL_DEVICE, O_RDWR);
    FlatMessage("Device OPEN");
    return (fd > -1);
}

void SkRevPiCtrl::close()
{
    if (!isOpen())
    {
        FlatError("Device is ALREADY closed");
        return;
    }

    ::close(fd);
    fd = -1;
    FlatMessage("Device CLOSED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkRevPiCtrl::isOpen()
{
    return (fd>=0);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/***********************************************************************************/
/*!
 * @brief Get Processdata
 *
 * Gets Processdata from a specific position
 *
 * @param[in]   Offset
 * @param[in]   Length
 * @param[out]  pData
 *
 * @return Number of Bytes read or error if negative
 *
 ************************************************************************************/

int SkRevPiCtrl::read(uint32_t offset, uint32_t Length, uint8_t *pData)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    int bytesRead = 0;

    if (::lseek(fd, offset, SEEK_SET) < 0)
        return -2;

    /* read */
    bytesRead = ::read(fd, pData, Length);

    if (bytesRead < 0)
        return -3;

    return bytesRead;
}


/***********************************************************************************/
/*!
 * @brief Set Processdata
 *
 * Writes Processdata at a specific position
 *
 * @param[in]   Offset
 * @param[in]   Length
 * @param[out]  pData
 *
 * @return Number of Bytes written or error if negative
 *
 ************************************************************************************/

int SkRevPiCtrl::write(uint32_t offset, uint32_t Length, uint8_t *pData)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    int bytesWritten = 0;

    if (lseek(fd, offset, SEEK_SET) < 0)
        return -2;

    bytesWritten = ::write(fd, pData, Length);

    if (bytesWritten < 0)
        return -3;

    return bytesWritten;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/***********************************************************************************/
/*!
 * @brief Get Device Info
 *
 * Get Description of connected devices.
 *
 * @param[in/out]   Pointer to an array of 20 entries of type SDeviceInfo.
 *
 * @return Number of detected devices
 *
 ************************************************************************************/

int SkRevPiCtrl::getDeviceInfo(SDeviceInfo *pDev)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    return ::ioctl(fd, KB_GET_DEVICE_INFO, pDev);
}

int SkRevPiCtrl::getDeviceInfoList(SDeviceInfo *pDev)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    return ::ioctl(fd, KB_GET_DEVICE_INFO_LIST, pDev);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/***********************************************************************************/
/*!
 * @brief Get Bit Value
 *
 * Get the value of one bit in the process image.
 *
 * @param[in/out]   Pointer to SPIValue.
 *
 * @return 0 or error if negative
 *
 ************************************************************************************/

int SkRevPiCtrl::getBitValue(SPIValue *pSpiValue)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    pSpiValue->i16uAddress += pSpiValue->i8uBit / 8;
    pSpiValue->i8uBit %= 8;
        
    return ::ioctl(fd, KB_GET_VALUE, pSpiValue);
}

/***********************************************************************************/
/*!
 * @brief Set Bit Value
 *
 * Set the value of one bit in the process image.
 *
 * @param[in/out]   Pointer to SPIValue.
 *
 * @return 0 or error if negative
 *
 ************************************************************************************/

int SkRevPiCtrl::setBitValue(SPIValue *pSpiValue)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    pSpiValue->i16uAddress += pSpiValue->i8uBit / 8;
    pSpiValue->i8uBit %= 8;

    return ::ioctl(fd, KB_SET_VALUE, pSpiValue);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/***********************************************************************************/
/*!
 * @brief Get Variable Info
 *
 * Get the info for a variable.
 *
 * @param[in/out]   Pointer to SPIVariable.
 *
 * @return 0 or error if negative
 *
 ************************************************************************************/

int SkRevPiCtrl::getVariableInfo(SPIVariable *pSpiVariable)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    return ::ioctl(fd, KB_FIND_VARIABLE, pSpiVariable);
}

/***********************************************************************************/
/*!
 * @brief Get Variable offset by name
 *
 * Get the offset of a variable in the process image. This does NOT work for variable of type bool.
 *
 * @param[in]   pointer to string with name of variable
 *
 * @return      >= 0    offset
                < 0     in case of error
 *
 ************************************************************************************/

int SkRevPiCtrl::findVariable(CStr *name)
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return -1;
    }

    int ret = 0;
    SPIVariable var;

    strncpy(var.strVarName, name, sizeof(var.strVarName));
    var.strVarName[sizeof(var.strVarName) - 1] = 0;
        
    ret = ::ioctl(fd, KB_FIND_VARIABLE, &var);

    if (ret < 0)
    {FlatError("Variable NOT found: " << var.strVarName);}

    else
    {
        ret = var.i16uAddress;
        FlatError("Variable: " << var.strVarName);
    }

    return ret;     
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool SkRevPiCtrl::reset()
{
    if (!isOpen())
    {
        FlatError("Device is NOT open");
        return false;
    }

    return !(::ioctl(fd, KB_RESET, NULL) < 0);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *SkRevPiCtrl::moduleTypeName(uint16_t t)
{
    t &= PICONTROL_NOT_CONNECTED_MASK;

    if (t == 95)
        return "RevPi Core";

    else if (t == 96)
        return "RevPi DIO";

    else if (t == 97)
        return "RevPi DI";

    else if (t == 98)
        return "RevPi DO";

    else if (t == 103)
        return "RevPi AIO";

    else if (t == 104)

        return "RevPi Compact";

    else if (t == 105)
        return "RevPi Connect";

    else if (t == 109)
        return "RevPi CON CAN";

    else if (t == 110)
        return "RevPi CON M-Bus";

    else if (t == 111)
        return "RevPi CON BT";

    else if (t == 118)
        return "RevPi MIO";

    else if (t == 135)
        return "RevPi Flat";

    else if (t == PICONTROL_SW_MODBUS_TCP_SLAVE)
        return "ModbusTCP Slave Adapter";

    else if (t == PICONTROL_SW_MODBUS_RTU_SLAVE)
        return "ModbusRTU Slave Adapter";

    else if (t == PICONTROL_SW_MODBUS_TCP_MASTER)
        return "ModbusTCP Master Adapter";

    else if (t == PICONTROL_SW_MODBUS_RTU_MASTER)
        return "ModbusRTU Master Adapter";

    else if (t == PICONTROL_SW_PROFINET_CONTROLLER)
        return "Profinet Controller Adapter";

    else if (t == PICONTROL_SW_PROFINET_DEVICE)
        return "Profinet Device Adapter";

    else if (t == PICONTROL_SW_REVPI_SEVEN)
        return "RevPi7 Adapter";

    else if (t == PICONTROL_SW_REVPI_CLOUD)
        return "RevPi Cloud Adapter";

    else if (t == 71)
        return "Gateway CANopen";

    else if (t == 72)
        return "Gateway CC-Link";

    else if (t == 73)
        return "Gateway DeviceNet";

    else if (t == 74)
        return "Gateway EtherCAT";

    else if (t == 75)
        return "Gateway EtherNet/IP";

    else if (t == 76)
        return "Gateway Powerlink";

    else if (t == 77)
        return "Gateway Profibus";

    else if (t == 78)
        return "Gateway Profinet RT";

    else if (t == 79)
        return "Gateway Profinet IRT";

    else if (t == 80)
        return "Gateway CANopen Master";

    else if (t == 81)
        return "Gateway SercosIII";

    else if (t == 82)
        return "Gateway Serial";

    else if (t == 85)
        return "Gateway EtherCAT Master";

    else if (t == 92)
        return "Gateway ModbusRTU";

    else if (t == 93)
        return "Gateway ModbusTCP";

    else if (t == 100)
        return "Gateway DMX";

    else
        return "unknown moduletype";
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
