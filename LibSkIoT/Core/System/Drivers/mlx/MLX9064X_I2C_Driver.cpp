#include "MLX9064X_I2C_Driver.h"
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#if defined(ENABLE_I2CLIB)

#include <linux/i2c-dev.h>

#include <linux/i2c.h>
#define I2C_MSG_FMT __u8

#include <sys/ioctl.h>

static int i2c_fd = -1;

bool MLX9064x_I2CInit(CStr *i2c_device)
{
    if (MLX9064x_I2CIsOpen())
    {
        printf("I2C ALREADY open!\n");
        return false;
    }

    i2c_fd = open(i2c_device, O_RDWR);

    return MLX9064x_I2CIsOpen();
}

bool MLX9064x_I2CIsOpen()
{
    return i2c_fd != -1;
}

//Read a number of words from startAddress. Store into Data array.
//Returns 0 if successful, -1 if error
int MLX9064x_I2CRead(uint8_t slaveAddr, uint16_t startAddress, uint16_t nWordsRead, uint16_t* data)
{
    if (!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return -1;
    }

    //int result;
    char cmd[2] = {(char)(startAddress >> 8), (char)(startAddress & 0xFF)};
    char buf[1664];
    uint16_t *p = data;
    struct i2c_msg i2c_messages[2];
    struct i2c_rdwr_ioctl_data i2c_messageset[1];

    i2c_messages[0].addr = slaveAddr;
    i2c_messages[0].flags = 0;
    i2c_messages[0].len = 2;
    i2c_messages[0].buf = (I2C_MSG_FMT*) cmd;

    i2c_messages[1].addr = slaveAddr;
    i2c_messages[1].flags = I2C_M_RD | I2C_M_NOSTART;
    i2c_messages[1].len = nWordsRead * 2;
    i2c_messages[1].buf = (I2C_MSG_FMT*) buf;

    //result = write(i2c_fd, cmd, 3);
    //result = read(i2c_fd, buf, nMemAddressRead*2);
    i2c_messageset[0].msgs = i2c_messages;
    i2c_messageset[0].nmsgs = 2;

    memset(buf, 0, nWordsRead * 2);

    if (ioctl(i2c_fd, I2C_RDWR, &i2c_messageset) < 0)
    {
        printf("I2C Read Error!\n");
        return -1;
    }

    for(int count = 0; count < nWordsRead; count++)
    {
        int i = count << 1;
        *p++ = static_cast<uint16_t>(buf[i] << 8) | buf[i+1];
    }

    return 0; //Success
}

//Set I2C Freq, in kHz
//MLX9064x_I2CFreqSet(1000) sets frequency to 1MHz
void MLX9064x_I2CFreqSet(int /*freq*/)
{
    //i2c.frequency(1000 * freq);
    //Wire.setClock((long)1000 * freq);
}

//Write two bytes to a two byte address
int MLX9064x_I2CWrite(uint8_t slaveAddr, uint16_t writeAddress, uint16_t data)
{
    if (!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return -1;
    }

    char cmd[4] = {
        (char)(writeAddress >> 8),
        (char)(writeAddress & 0x00FF),
        (char)(data >> 8),
        (char)(data & 0x00FF)
    };

    //int result;

    struct i2c_msg i2c_messages[1];
    struct i2c_rdwr_ioctl_data i2c_messageset[1];

    i2c_messages[0].addr = slaveAddr;
    i2c_messages[0].flags = 0;
    i2c_messages[0].len = 4;
    i2c_messages[0].buf = (I2C_MSG_FMT*)cmd;

    i2c_messageset[0].msgs = i2c_messages;
    i2c_messageset[0].nmsgs = 1;

    if (ioctl(i2c_fd, I2C_RDWR, &i2c_messageset) < 0)
    {
        printf("I2C Write Error!\n");
        return -1;
    }

    return 0; //Success
}

bool MLX9064x_I2Close()
{
    if (!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return false;
    }

    close(i2c_fd);
    i2c_fd = -1;
    return true;
}

#elif defined(RPI_I2C)
//#include "Core/System/GPIO/RPi/bcm2835.h"
#include <bcm2835.h>

static bool initialized = false;

bool MLX9064x_I2CInit(CStr *)
{
    if (MLX9064x_I2CIsOpen())
    {
        printf("I2C ALREADY open!\n");
        return false;
    }

    //bcm2835_set_debug(1);

    if (!bcm2835_init() || !bcm2835_i2c_begin())
        return false;

    //bcm2835_i2c_set_baudrate(400000);
    initialized = true;

    return true;
}

bool MLX9064x_I2CIsOpen()
{
    return initialized;
}

int MLX9064x_I2CRead(uint8_t slaveAddr, uint16_t startAddress, uint16_t nMemAddressRead, uint16_t *data)
{
    if (!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return -1;
    }

    int result;

    char cmd[2] = {(char)(startAddress >> 8), (char)(startAddress & 0xFF)};

    bcm2835_i2c_setSlaveAddress(slaveAddr);

    char buf[1664];
    uint16_t *p = data;

    result = bcm2835_i2c_write_read_rs(cmd, 2, buf, nMemAddressRead*2);

    for(int count = 0; count < nMemAddressRead; count++){
    int i = count << 1;
        *p++ = ((uint16_t)buf[i] << 8) | buf[i+1];
    }
    return 0;
}

void MLX9064x_I2CFreqSet(int freq)
{
}

int MLX9064x_I2CWrite(uint8_t slaveAddr, uint16_t writeAddress, uint16_t data)
{
    if (!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return -1;
    }

    int result;
    char cmd[4] = {(char)(writeAddress >> 8), (char)(writeAddress & 0x00FF), (char)(data >> 8), (char)(data & 0x00FF)};
    result = bcm2835_i2c_write(cmd, 4);
    return 0;
}

bool MLX9064x_I2Close()
{
    if(!MLX9064x_I2CIsOpen())
    {
        printf("I2C NOT open!\n");
        return false;
    }

    bcm2835_i2c_end();
    initialized = false;
    return bcm2835_close();
}
#endif
