TARGET = SkIoT
TEMPLATE = lib

# insert sources

include(LibSkIoT.pri)
include(module-src.pri)

DEFINES += SKAUDIO_LIBRARY

# include libSkCore

LIBS += -lSkCore
INCLUDEPATH += /usr/local/include/LibSkCore
include(/usr/local/include/LibSkCore/LibSkCore.pri)

# installation

target.path = /usr/local/lib
INSTALLS += target

for(header, HEADERS) {
  path = /usr/local/include/LibSkIoT/$${relative_path($${dirname(header)})}
  eval(headers_$${path}.files += $$header)
  eval(headers_$${path}.path = $$path)
  eval(INSTALLS *= headers_$${path})
}

MODULES += LibSkIoT.pri

headersDataFiles.path = /usr/local/include/LibSkIoT/
headersDataFiles.files = $$MODULES

INSTALLS += headersDataFiles

