HEADERS +=  \
    $$PWD/Core/System/Drivers/mlx/MLX90640_API.h \
    $$PWD/Core/System/Drivers/mlx/MLX90641_API.h \
    $$PWD/Core/System/Drivers/mlx/MLX9064X_I2C_Driver.h \
    $$PWD/Core/System/Drivers/revPi/piControl.h \
    $$PWD/Core/System/Drivers/revPi/skrevpictrl.h \
    $$PWD/Core/System/GPIO/ski2cbus.h \
    $$PWD/Core/System/GPIO/skgpio.h \
    $$PWD/Core/System/GPIO/sksoftpwm.h \
    $$PWD/Modules/skrevpi.h \
    $$PWD/Modules/skultrasonicping.h

SOURCES +=  \
    $$PWD/Core/System/Drivers/mlx/MLX90640_API.cpp \
    $$PWD/Core/System/Drivers/mlx/MLX90641_API.cpp \
    $$PWD/Core/System/Drivers/mlx/MLX9064X_I2C_Driver.cpp \
    $$PWD/Core/System/Drivers/revPi/skrevpictrl.cpp \
    $$PWD/Core/System/GPIO/ski2cbus.cpp \
    $$PWD/Core/System/GPIO/skgpio.cpp \
    $$PWD/Core/System/GPIO/sksoftpwm.cpp \
    $$PWD/Modules/skrevpi.cpp \
    $$PWD/Modules/skultrasonicping.cpp

DISTFILES +=
