#ifndef SKREVPI_H
#define SKREVPI_H

#if defined(RASPBERRY)

#include <Modules/skabstractmodule.h>
#include <Core/System/Drivers/revPi/skrevpictrl.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum SkRevPiVarDirection
{
    RevPi_IN,
    RevPi_OUT
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkRevPiDevice;

class SkRevPiVar extends SkFlatObject
{
    public:
        SkRevPiVar(SkRevPiDevice *ownerDevice);

        void setControl(SkRevPiCtrl *piControl);
        void setup(Short index, SkRevPiVarDirection direction, SkVariantVector &rscList);
        void setChanID(SkFlowChanID chanID);
        void setValue(const SkVariant &val);
        void setInterval(double seconds);

        void read();
        void write();

        CStr *name();
        SkVariant &value();
        SkVariant_T valueType();
        uint8_t bits();
        SkRevPiVarDirection direction();
        SkFlowChanID chanID();
        CStr *comment();

    private:
        SkElapsedTime readChrono;
        double readIntervalMS;

        SkRevPiDevice *owner;
        SkRevPiCtrl *piCtrl;
        SkVariantVector rsc;
        Short v_index;
        SkString v_name;
        SkRevPiVarDirection v_direction;
        SkVariant_T t;
        SkVariant v;
        SkString dfltValStr;
        SkFlowChanID v_chanID;
        uint8_t bitLen;
        uint8_t bitPos;
        uint8_t osetFromDev;
        SkString description;

        SPIVariable spiVar;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkRevPiDevice extends SkFlatObject
{
    public:
        SkRevPiDevice();

        void setControl(SkRevPiCtrl *piControl);
        void setup(SDeviceInfo &device, SkArgsMap &rscMap);

        SkRevPiVar *get(CStr *varName);

        SkBinaryTreeVisit<SkString, SkRevPiVar *> *inputIterator();
        SkBinaryTreeVisit<SkString, SkRevPiVar *> *outputIterator();

        void clear();

        bool isActive();
        CStr *name();
        CStr *id();
        CStr *guid();
        UShort productType();
        CStr *productTypeName();
        uint offset();
        CStr *comment();
        uint8_t address();

    private:
        SkRevPiCtrl *piCtrl;
        SDeviceInfo dev;
        SkString d_name;
        SkString d_id;
        SkString d_guid;
        uint osetAbsolute;
        SkString description;

        SkTreeMap<SkString, SkRevPiVar *> inputVars;
        SkTreeMap<SkString, SkRevPiVar *> outputVars;

        void buildVariables(SkArgsMap &variables, SkRevPiVarDirection direction);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkRevPiCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkRevPiCfg, SkAbstractModuleCfg);

        SkRevPiVar *findVariable(CStr *varName);
        SkVector<SkRevPiDevice *> &devices();

    private:
        SkRevPiCtrl piCtrl;
        SkVector<SkRevPiDevice *> devs;

        void allowedCfgKeysSetup()              override;
        bool importConfigRsc();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkRevPi extends SkAbstractModule
{
    public:
        Constructor(SkRevPi, SkAbstractModule);

        Slot(toggleRelay);
        Slot(toggleWatchDog);
        Slot(switchBooleanOut);
        Slot(changeIntegerOut);

    private:
        //A1    0(GREEN)/1(RED)
        //A2    2(GREEN)/3(RED)
        //A3    4(GREEN)/5(RED)
        SkRevPiCtrl piCtrl;
        SkRevPiCfg *cfg;

        //REL   6(TOGGLE)
        //WD    7(TOGGLE)
        bitset<8> revPiLedBits;
        SkRevPiVar *revPiLed;

        SkVector<SkRevPiDevice *>  *devices;

        void allowedExtendedCmdsSetup()         override;
        void allowedExtendedCtrlSetup()         override;
        bool onFlowChannelsSetup()                override;

        bool onSetup()                          override;
        void onStart()                          override;
        void onStop()                           override;

        void onSetEnabled(bool enabled)         override;

        void onFastTick()                       override;
        void onSlowTick()                       override;
        void onOneSecTick()                     override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKREVPI_H
