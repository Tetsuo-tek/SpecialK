#ifndef SKULTRASONIC_H
#define SKULTRASONIC_H

#if defined(UMBA)
#include <Modules/skabstractmodule.h>

class SkGPIO;

class SkUltrasonic extends SkAbstractModule
{
    public:
        Constructor(SkUltrasonic, SkAbstractModule);

        int64_t getLastDistance();

    private:
        //SkSr04 *sr04;
        SkGPIO *triggerPin;
        int triggerBoardPin;

        SkGPIO *echoPin;
        int echoBoardPin;

        uint64_t distanceChan;

        int64_t distanceHold;
        int64_t distance;

        void allowedCfgKeysSetup()              override;
        void allowedExtendedCmdsSetup()         override;

        bool onSetup()                          override;
        void onStart()                          override;
        void onStop()                           override;

        void onSetEnabled(bool enabled)         override;

        void onFastTick()                       override;
        void onSlowTick()                       override;
        void onOneSecTick()                     override;

        double getDelay();
};
#endif

#endif // ULTRASONIC_H
