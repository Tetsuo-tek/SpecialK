#include "skrevpi.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/Containers/skarraycast.h"

#if defined(RASPBERRY)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkRevPiVar::SkRevPiVar(SkRevPiDevice *ownerDevice)
{
    owner = ownerDevice;
    v_index = -1;
    bitLen = 0;
    bitPos = 0;
    osetFromDev = 0;
    piCtrl = nullptr;

    spiVar.i16uAddress = 0;
    spiVar.i8uBit = 0;
    spiVar.i16uLength = 0;

    readIntervalMS = 0.;
}

void SkRevPiVar::setControl(SkRevPiCtrl *piControl)
{
    piCtrl = piControl;
    FlatDebug("Set CONTROL to: " << piControl->objectName());
}

void SkRevPiVar::setup(Short index, SkRevPiVarDirection direction, SkVariantVector &rscList)
{
    rsc = rscList;
    v_index = index;
    v_direction = direction;

    v_name = rsc[0].toString();

    memcpy(spiVar.strVarName, v_name.c_str(), v_name.size());
    spiVar.strVarName[v_name.size()] = '\0';

    setObjectName(owner, v_name.c_str());

    dfltValStr = rsc[1].toString();
    bitLen = rsc[2].toUInt8();
    osetFromDev = rsc[3].toUInt8();
    description = rsc[6].toString();
    bitPos = rsc[7].toUInt8();

    if (piCtrl->getVariableInfo(&spiVar)<0)
    {FlatError("CANNOT read variable information");}

    if (spiVar.i16uLength == 1)
        t = T_BOOL;

    else if (spiVar.i16uLength > 1 && spiVar.i16uLength <= 8)
        t = T_UINT8;

    else if (spiVar.i16uLength > 8 && spiVar.i16uLength <= 16)
        t = T_UINT16;

    else if (spiVar.i16uLength > 16 && spiVar.i16uLength <= 32)
        t = T_UINT32;

    SkString directionStr;

    if (direction == RevPi_IN)
        directionStr = "IN";
    else
        directionStr = "OUT";

    FlatMessage("New SPI Variable CACHED -> "
              << " [address: " << spiVar.i16uAddress << " (0x" << hex << spiVar.i16uAddress << ")"
              << "; type: " << SkVariant::variantTypeName(t)
              << "; direction: " << directionStr
              << "; bits: " << dec << spiVar.i16uLength
              << "; offset: " << static_cast<uint>(spiVar.i8uBit)
              << "]");
}

void SkRevPiVar::setChanID(SkFlowChanID chanID)
{
    v_chanID = chanID;
}

void SkRevPiVar::setValue(const SkVariant &val)
{
    v = val;
}

void SkRevPiVar::setInterval(double seconds)
{
    readIntervalMS = seconds;

    FlatMessage("SET variable read time interval [seconds]: " << readIntervalMS);
}

void SkRevPiVar::read()
{
    if (v_direction == RevPi_IN && readChrono.stop() < readIntervalMS)
        return;

    if (t == T_BOOL)
    {
        SPIValue tempSpiValue;
        tempSpiValue.i16uAddress = spiVar.i16uAddress;
        tempSpiValue.i8uBit = spiVar.i8uBit;

        if (piCtrl->getBitValue(&tempSpiValue) < 0)
        {
            FlatError("Cannot read bit");
            return;
        }

        v = (tempSpiValue.i8uValue != 0);
    }

    else if (t == T_UINT8)
    {
        uint8_t i8uValue;

        if (piCtrl->read(spiVar.i16uAddress, 1, &i8uValue)<0)
        {
            FlatError("Cannot read value");
            return;
        }

        v = i8uValue;
    }

    else if (t == T_UINT16)
    {
        uint16_t i16uValue;

        if (piCtrl->read(spiVar.i16uAddress, 2, SkArrayCast::toUInt8(&i16uValue))<0)
        {
            FlatError("Cannot read value");
            return;
        }

        v = i16uValue;
    }

    else if (t == T_UINT32)
    {
        uint32_t i32uValue;

        if (piCtrl->read(spiVar.i16uAddress, 4, SkArrayCast::toUInt8(&i32uValue))<0)
        {
            FlatError("Cannot read value");
            return;
        }

        v = i32uValue;
    }

    readChrono.start();
}

void SkRevPiVar::write()
{
    if (t == T_BOOL)
    {
        SPIValue tempSpiValue;
        tempSpiValue.i16uAddress = spiVar.i16uAddress;
        tempSpiValue.i8uBit = spiVar.i8uBit;

        bitset<8> bits = {0};
        bits[spiVar.i8uBit] = v.toBool();

        tempSpiValue.i8uValue = static_cast<uint8_t>(bits.to_ulong());

        if (piCtrl->setBitValue(&tempSpiValue) < 0)
            FlatError("Cannot read bit");
    }

    else if (t == T_UINT8)
    {
        uint8_t i8uValue = v.toUInt8();
        if (piCtrl->write(spiVar.i16uAddress, 1, &i8uValue) < 0)
        {FlatError("Cannot write value");}
    }

    else if (t == T_UINT16)
    {
        uint32_t i16uValue = v.toUInt16();
        if (piCtrl->write(spiVar.i16uAddress, 2, SkArrayCast::toUInt8(&i16uValue)) < 0)
        {FlatError("Cannot write value");}
    }

    else if (t == T_UINT32)
    {
        uint32_t i32uValue = v.toUInt32();
        if (piCtrl->write(spiVar.i16uAddress, 4, SkArrayCast::toUInt8(&i32uValue)) < 0)
        {FlatError("Cannot write value");}
    }
}

CStr *SkRevPiVar::name()
{
    return v_name.c_str();
}

SkVariant &SkRevPiVar::value()
{
    return v;
}

SkVariant_T SkRevPiVar::valueType()
{
    return t;
}

uint8_t SkRevPiVar::bits()
{
    return spiVar.i16uLength;
}

SkRevPiVarDirection SkRevPiVar::direction()
{
    return v_direction;
}

SkFlowChanID SkRevPiVar::chanID()
{
    return v_chanID;
}

CStr *SkRevPiVar::comment()
{
    return description.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkRevPiDevice::SkRevPiDevice()
{
}

void SkRevPiDevice::setControl(SkRevPiCtrl *piControl)
{
    piCtrl = piControl;
    osetAbsolute = 0;

    SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = inputVars.iterator();

    while(itr->next())
    {
        SkRevPiVar *var = itr->item().value();
        var->setControl(piCtrl);
    }

    delete itr;

    itr = outputVars.iterator();

    while(itr->next())
    {
        SkRevPiVar *var = itr->item().value();
        var->setControl(piCtrl);
    }

    delete itr;
}

void SkRevPiDevice::setup(SDeviceInfo &device, SkArgsMap &rscMap)
{
    dev = device;
    d_name = SkRevPiCtrl::moduleTypeName(device.i16uModuleType);

    SkString s = d_name;
    s.replace(" ", "_");
    setObjectName(d_name.c_str());

    d_id = rscMap["id"].toString();
    d_guid = rscMap["guid"].toString();

    osetAbsolute = rscMap["offset"].toUInt();
    description = rscMap["comment"].toString();

    stringstream o;
    o << "Found module ";

    if(dev.i8uActive)
        o << "READY";

    else if (dev.i16uModuleType & PICONTROL_NOT_CONNECTED)
        o << "OFF-LINE";

    else
        o << "NOT-CONFIGURED";

    o << " -> "
      << " [id: " << d_id
      << "; guid: " << d_guid
      << "; product_t " << dev.i16uModuleType << " (0x" << hex << dev.i16uModuleType << ")"
      << "; addr: " << dec << static_cast<uint>(dev.i8uAddress) << " (0x" << hex << static_cast<uint>(dev.i8uAddress) << ")"
      << "; offset: " << dec << osetAbsolute << " (0x" << hex << osetAbsolute << ")"
      << "; version: "  << dec << dev.i16uSW_Major << "." << dev.i16uSW_Minor
      << "; HW-rev: " << dev.i16uHW_Revision
      << "; SW-rev: " << dev.i32uSVN_Revision
      //<< "; SN: " << dev.i32uSerialnumber
      << "]";

    if(dev.i8uActive)
    {FlatMessage(o.str().data());}

    else
    {FlatWarning(o.str().data());}

    SkArgsMap in;
    rscMap["inp"].copyToMap(in);

    FlatMessage("Checking INPUT list [" << in.count() << " variables] ..");
    buildVariables(in, RevPi_IN);

    SkArgsMap out;
    rscMap["out"].copyToMap(out);

    FlatMessage("Checking OUTPUT list [" << in.count() << " variables] ..");
    buildVariables(out, RevPi_OUT);
}

void SkRevPiDevice::buildVariables(SkArgsMap &variables, SkRevPiVarDirection direction)
{
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = variables.iterator();

    while(itr->next())
    {
        Short index = static_cast<Short>(itr->item().key().toInt());
        SkVariant &v = itr->item().value();

        SkVariantVector variable;
        v.copyToList(variable);

        //NOT EXPORTED FROM PICTORY
        if (!variable[4].toBool())
            continue;

        SkRevPiVar *var = new SkRevPiVar(this);
        var->setControl(piCtrl);
        var->setup(index, direction, variable);
        var->read();

        if (direction == RevPi_IN)
            inputVars[var->name()] = var;

        else
            outputVars[var->name()] = var;
    }

    delete itr;
}

SkRevPiVar *SkRevPiDevice::get(CStr *varName)
{
    if (inputVars.contains(varName))
        return inputVars[varName];

    if (!outputVars.contains(varName))
        return nullptr;

    return outputVars[varName];
}

SkBinaryTreeVisit<SkString, SkRevPiVar *> *SkRevPiDevice::inputIterator()
{
    return inputVars.iterator();
}

SkBinaryTreeVisit<SkString, SkRevPiVar *> *SkRevPiDevice::outputIterator()
{
    return outputVars.iterator();
}

void SkRevPiDevice::clear()
{
    FlatWarning("Clearing device variables [In: " << inputVars.count() << "; out: " << outputVars.count() << "]");
    SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = inputVars.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    inputVars.clear();

    itr = outputVars.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    outputVars.clear();
    FlatMessage("Device EMPTY");
}

bool SkRevPiDevice::isActive()
{
    return dev.i8uActive;
}

CStr *SkRevPiDevice::name()
{
    return d_name.c_str();
}

CStr *SkRevPiDevice::id()
{
    return d_id.c_str();
}

CStr *SkRevPiDevice::guid()
{
    return d_guid.c_str();
}

UShort SkRevPiDevice::productType()
{
    return dev.i16uModuleType;
}

CStr *SkRevPiDevice::productTypeName()
{
    return d_name.c_str();
}

uint SkRevPiDevice::offset()
{
    return osetAbsolute;
}

CStr *SkRevPiDevice::comment()
{
    return description.c_str();
}

uint8_t SkRevPiDevice::address()
{
    return dev.i8uAddress;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkRevPiCfg, SkAbstractModuleCfg)
{
}

void SkRevPiCfg::allowedCfgKeysSetup()
{
    piCtrl.setObjectName(this, "Ctrl");
    AssertKiller(!importConfigRsc());

    SkWorkerParam *p = nullptr;

    p = addParam("tickInterval", "Tick time interval", T_DOUBLE, true, 0.005, "Sets base tick-time-interval for RevPi module [s]");
    p->setMin(0.005);
    p->setMax(0.999);
    p->setStep(0.001);

    addParam("realTimeMode", "Enable real-time", T_BOOL, true, false, "Sets real-time mode for the RevPi module tick");

    double baseInterval = eventLoop()->getFastInterval()/1000000.;

    for(ULong i=0; i<devs.count(); i++)
    {
        SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devs[i]->inputIterator();

        while(itr->next())
        {
            SkRevPiVar *var = itr->item().value();

            SkString key = var->name();
            key.append("_RefreshTime");

            SkString label("[");
            label.append(var->name());
            label.append("] Refresh interval");

            p = addParam(key.c_str(), label.c_str(), T_DOUBLE, false, baseInterval, "Sets time interval to read [s]");
            p->setMin(0.);
            p->setStep(baseInterval);
        }

        delete itr;
    }
}

bool SkRevPiCfg::importConfigRsc()
{
    SkArgsMap m;

    if (!m.fromFile("/etc/revpi/config.rsc"))
        return false;

    SkVariantList devices;
    m["Devices"].copyToList(devices);

    ObjectMessage("Caching process-image ..");

    if (!piCtrl.open())
        return false;

    SDeviceInfo devList[REV_PI_DEV_CNT_MAX];
    int devCount = piCtrl.getDeviceInfoList(devList);

    SkTreeMap<uint8_t, SDeviceInfo *> t;

    for(int i=0; i<devCount; i++)
    {
        SDeviceInfo &dev = devList[i];
        t[dev.i8uAddress] = &dev;
    }

    AssertKiller(t.count() != devices.count());

    SkAbstractListIterator<SkVariant> *itr = devices.iterator();

    while(itr->next())
    {
        SkArgsMap rscMap;
        itr->item().copyToMap(rscMap);

        SkRevPiDevice *dev = new SkRevPiDevice;
        dev->setControl(&piCtrl);
        dev->setup(*t[rscMap["position"].toUInt8()], rscMap);

        devs << dev;
    }

    delete itr;

    piCtrl.close();
    ObjectMessage("process-image CACHED");
    return true;
}

SkRevPiVar *SkRevPiCfg::findVariable(CStr *varName)
{
    for(ULong i=0; i<devs.count(); i++)
    {
        SkRevPiVar *var = devs[i]->get(varName);

        if (var)
            return var;
    }

    return nullptr;
}

SkVector<SkRevPiDevice *> &SkRevPiCfg::devices()
{
    return devs;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkRevPi, SkAbstractModule)
{
    cfg = nullptr;
    revPiLed = nullptr;
    devices = nullptr;

    SlotSet(toggleRelay);
    SlotSet(toggleWatchDog);
    SlotSet(switchBooleanOut);
    SlotSet(changeIntegerOut);
}

bool SkRevPi::onSetup()
{
    piCtrl.setObjectName(this, "Ctrl");

    cfg = dynamic_cast<SkRevPiCfg *>(config());
    devices = &cfg->devices();

    for(ULong i=0; i<devices->count(); i++)
    {
        devices->at(i)->setControl(&piCtrl);

        SkWorkerParam *p = nullptr;

        SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devices->at(i)->inputIterator();

        while(itr->next())
        {
            SkRevPiVar *var = itr->item().value();

            SkString key = var->name();
            key.append("_RefreshTime");

            if ((p = cfg->getParameter(key.c_str())))
                var->setInterval(p->value().toDouble());
        }

        delete itr;
    }

    revPiLed = cfg->findVariable("RevPiLED");

    piCtrl.open();

    if (revPiLed)
    {
        revPiLedBits = revPiLed->value().toUInt8();
        revPiLedBits[5] = true;
        revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
        revPiLed->write();
    }

    piCtrl.close();

    return true;
}

void SkRevPi::allowedExtendedCmdsSetup()
{

}

void SkRevPi::allowedExtendedCtrlSetup()
{
    if (revPiLed)
    {
        bool b = revPiLedBits[6];
        addSingleControl("Switch relay", T_BOOL, b, "Switch the RevPi-relay contact", toggleRelay_SLOT);

        /*b = revPiLedBits[7];
        addSingleControl("Restart watchdog", T_BOOL, "Restart the RevPi-watchdog within 1 second", toggleWatchDog_SLOT)
                ->parameter()->setValue(b);*/

        SkWorkerCommand *booleanCtrl = addMultiControl("Boolean Output Switcher", "Switch the boolean output value", switchBooleanOut_SLOT);
        SkWorkerCommand *integerCtrl = addMultiControl("Integer Output Regolator", "Change the integer output value", changeIntegerOut_SLOT);

        SkWorkerParam *p = nullptr;

        for(ULong i=0; i<devices->count(); i++)
        {
            SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devices->at(i)->outputIterator();

            while(itr->next())
            {
                if (itr->item().key() == "RevPiLED")
                    continue;

                SkRevPiVar *var = itr->item().value();

                if (var->valueType() == T_BOOL)
                    booleanCtrl->addParam(var->name(), var->name(), T_BOOL, true, false, "")->setValue(var->value());

                else if (SkVariant::isInteger(var->valueType()))
                {
                    p = integerCtrl->addParam(var->name(), var->name(), var->valueType(), true, 0, "");
                    p->setMin(0);
                    p->setMax(static_cast<Int>(pow(2, var->bits()))-1);
                    p->setStep(1);
                    p->setValue(var->value());
                }
            }

            delete itr;
        }
    }
}

bool SkRevPi::onFlowChannelsSetup()
{
    for(ULong i=0; i<devices->count(); i++)
    {
        SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devices->at(i)->inputIterator();

        while(itr->next())
        {
            SkRevPiVar *var = itr->item().value();
            SkFlowChanID chanID;

            if (!addStreamingChannel(chanID, FT_MCU_PIN_INPUT, var->valueType(), var->name()))
                return false;

            var->setChanID(chanID);
        }

        delete itr;

        itr = devices->at(i)->outputIterator();

        while(itr->next())
        {
            SkRevPiVar *var = itr->item().value();
            SkFlowChanID chanID;

            if (!addStreamingChannel(chanID, FT_MCU_PIN_OUTPUT, var->valueType(), var->name()))
                return false;

            var->setChanID(chanID);
        }

        delete itr;
    }

    return true;
}

void SkRevPi::onStart()
{
    piCtrl.open();

    if (revPiLed)
    {
        revPiLedBits[5] = false;
        revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
        revPiLed->write();
    }
}

void SkRevPi::onStop()
{
    piCtrl.close();

    /*for(ULong i=0; i<devices->count(); i++)
    {
        devices->at(i)->clear();
        delete devices->at(i);
    }

    devices->clear();*/
    revPiLed = nullptr;
}

void SkRevPi::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        eventLoop()->changeFastZone(::round(cfg->getParameter("tickInterval")->value().toDouble()*1000000.));
        eventLoop()->changeSlowZone(200000);

        if (cfg->getParameter("realTimeMode")->value().toBool())
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        else
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }

    else
    {
        if (revPiLed)
        {
            revPiLedBits[4] = false;
            revPiLedBits[5] = false;
            revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
            revPiLed->write();
        }

        eventLoop()->changeFastZone(500000);
        eventLoop()->changeSlowZone(500000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
    }
}

void SkRevPi::onFastTick()
{
    for(ULong i=0; i<devices->count(); i++)
    {
        SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devices->at(i)->inputIterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();

            SkRevPiVar *var = itr->item().value();
            SkVariant lastVal = var->value();

            var->read();

            if (var->value() != lastVal)
            {
                if (var->valueType() == T_BOOL)
                    flow->publish(var->chanID(), var->value().toBool());

                else if (var->valueType() == T_UINT8)
                    flow->publish(var->chanID(), var->value().toUInt8());

                else if (var->valueType() == T_UINT16)
                    flow->publish(var->chanID(), var->value().toUInt16());

                else if (var->valueType() == T_UINT32)
                    flow->publish(var->chanID(), var->value().toUInt32());

                flow->setVariable(k.c_str(), var->value());

                if (k == "Core_Frequency"
                        || k == "Core_Temperature"
                        || k == "RevPiIOCycle"
                        || k == "RevPiStatus")
                {
                    continue;
                }

                ObjectMessage("INPUT variable has CHANGED -> " << k << " : " << var->value());
            }
        }

        delete itr;
    }

    if (revPiLed)
    {
        revPiLedBits[4] = !revPiLedBits[4];
        revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
        revPiLed->write();
    }
}

void SkRevPi::onSlowTick()
{
    for(ULong i=0; i<devices->count(); i++)
    {
        SkBinaryTreeVisit<SkString, SkRevPiVar *> *itr = devices->at(i)->outputIterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();

            SkRevPiVar *var = itr->item().value();
            SkVariant lastVal = var->value();

            var->read();

            if (var->value() != lastVal)
            {
                if (var->valueType() == T_BOOL || var->valueType() == T_UINT8)
                    flow->publish(var->chanID(), var->value().toUInt8());

                else if (var->valueType() == T_UINT16)
                    flow->publish(var->chanID(), var->value().toUInt16());

                else if (var->valueType() == T_UINT32)
                    flow->publish(var->chanID(), var->value().toUInt32());

                flow->setVariable(k.c_str(), var->value());

                ObjectMessage("OUTPUT variable has CHANGED -> " << k << " : " << var->value());
            }
        }

        delete itr;
    }

    if (revPiLed)
    {
        revPiLedBits[4] = !revPiLedBits[4];
        revPiLedBits[5] = !revPiLedBits[5];
        revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
        revPiLed->write();
    }
}

void SkRevPi::onOneSecTick()
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SkRevPi, toggleRelay)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;
    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["toggleRelay"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (revPiLed && ctrl->load(cmdMap))
    {
        updateCommand(ctrl);

        //NOT GETTING VALUE BECAUSE HERE WE MAKE A TOGGLE
        //(more users could be connected and the switch could become inconsistent with the value)
        //cmdMap["toggleRelay"].toBool()

        revPiLedBits[6] = !revPiLedBits[6];
        revPiLed->setValue(static_cast<uint8_t>(revPiLedBits.to_ulong()));
        revPiLed->write();

        if (t)
        {
            //t->setResponse(cmdMap);
            t->goBack();
        }
    }

    else
    {
        if (t)
        {
            if (!revPiLed)
                t->setError("RevPiLED variable is NOT exported from Pictory");
            else
                t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

SlotImpl(SkRevPi, toggleWatchDog)
{
    SilentSlotArgsWarning();

}

SlotImpl(SkRevPi, switchBooleanOut)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;
    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["switchBooleanOut"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (ctrl->load(cmdMap))
    {
        updateCommand(ctrl);
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = ctrl->parameters().iterator();

        while(itr->next())
        {
            SkWorkerParam *p = itr->item().value();
            SkRevPiVar *var = cfg->findVariable(p->key());

            if (var)
            {
                bool checked = cmdMap[p->key()].toBool();

                if (var->value().toBool() != checked)
                {
                    //cout << "!!!!" << p->key() << " " << var->value().toBool() << " " << checked <<"\n";

                    var->setValue(checked);
                    var->write();

                    //IT PERMITs TO READ NEW VALUE AT THE NEXT SlotTick
                    //(notifying, saving on db and pubblishing it)
                    var->setValue(!checked);
                }
            }

            else
            {ObjectError("Variable NOT found: " << p->key());}
        }

        delete itr;
    }

    else
    {
        if (t)
        {
            t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

SlotImpl(SkRevPi, changeIntegerOut)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;
    v.copyToMap(cmdMap);

    SkWorkerCommand *ctrl = commands()["changeIntegerOut"];
    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    if (ctrl->load(cmdMap))
    {
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = ctrl->parameters().iterator();

        while(itr->next())
        {
            SkWorkerParam *p = itr->item().value();
            SkRevPiVar *var = cfg->findVariable(p->key());

            if (var)
            {
                UInt val = p->value().toUInt();

                if (var->value().toUInt() != val)
                {
                    var->setValue(val);
                    var->write();

                    //IT PERMITs TO READ NEW VALUE AT THE NEXT SlotTick
                    //(notifying, saving on db and pubblishing it)
                    var->setValue(0);
                }
            }

            else
            {ObjectError("Variable NOT found: " << p->key());}
        }

        delete itr;
    }

    else
    {
        if (t)
        {
            t->setError("Command NOT valid");

            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
