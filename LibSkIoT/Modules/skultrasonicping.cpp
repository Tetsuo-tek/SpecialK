#include "skultrasonicping.h"

#include "Core/System/GPIO/skgpio.h"
#include "Core/System/Network/FlowNetwork/skflowserver.h"

#if defined(UMBA)
ConstructorImpl(SkUltrasonic, SkAbstractModule)
{
    distanceChan = 0;

    triggerPin = nullptr;
    triggerBoardPin = 0;

    echoPin = nullptr;
    echoBoardPin = 0;

    distance = 0;
    distanceHold = 0;
}

bool SkUltrasonic::onSetup()
{
    triggerBoardPin = configMap["triggerBoardPin"].toInt();
    echoBoardPin = configMap["echoBoardPin"].toInt();

    return true;
}

void SkUltrasonic::allowedCfgKeysSetup()
{
    addCfgParam("triggerBoardPin", SkVariant_T::T_UINT16, true, 27, "Trigger pin (sending ultrasonic sequences)");
    addCfgParam("echoBoardPin", SkVariant_T::T_UINT16, true, 22, "Echo pin (receiving ultrasonic sequences)");
    setConfigValueDefault("interval", 100000);
}

void SkUltrasonic::allowedExtendedCmdsSetup()
{

}

void SkUltrasonic::onStart()
{
    //distanceChan = flowServer->addStreamingChannel(SkVariant_T::T_INT32, "Distance", nullptr, "cm");

    SkVariant min(0);
    SkVariant max(300);

    //flowServer->setPublisherValueLimits(distanceChan, min, max);
}

void SkUltrasonic::onStop()
{
}

void SkUltrasonic::onSetEnabled(bool enabled)
{
    if (enabled)
    {
        triggerPin = new SkGPIO(triggerBoardPin);
        triggerPin->setup(SkPinType::GPT_DGT, SkPinMode::GP_OUTPUT);

        triggerPin->write(LOW);

        echoPin = new SkGPIO(echoBoardPin);
        echoPin->setup(SkPinType::GPT_DGT, SkPinMode::GP_INPUT);
    }

    else
    {
        delete triggerPin;
        triggerPin = nullptr;

        delete echoPin;
        echoPin = nullptr;
    }
}

void SkUltrasonic::onFastTick()
{
    int samples = 5;
    double travelTimeUsec[samples] = {0.0};

    for(int i=0; i<samples; i++)
        travelTimeUsec[i] = getDelay();

    SkSort<double> s;
    s.setup(SkOrderAlgo::OA_INSERTION);
    s.sort(travelTimeUsec, samples);

    distance = static_cast<int64_t>(travelTimeUsec[samples/2]/58.2);

    //flowServer->publish(distanceChan, static_cast<int32_t>(distance));
    ObjectPlusDebug("delay: " << travelTimeUsec[samples/2] << " us; distance: " << distance << " cm")
}

void SkUltrasonic::onSlowTick()
{
    //cout << distance << "\n";

    if (distance != distanceHold)
    {
        //flowServer->setVariable("distance", distance);
        distanceHold = distance;
    }
}

void SkUltrasonic::onOneSecTick()
{
}

double SkUltrasonic::getDelay()
{
    triggerPin->write(HIGH);
    waitForMicros(10);
    triggerPin->write(LOW);

    echoPin->waitForValue(HIGH, 5000);
    return echoPin->waitForValue(LOW, 30000);
}

int64_t SkUltrasonic::getLastDistance()
{
    return distance;
}
#endif
