CONFIG += c++11
CONFIG -= qt

DEFINES += PCSKETCH
DEFINES += GPIOWORD=uint32_t

contains(QMAKE_HOST.arch, x86) {
    DEFINES += ARCH=SatArch::ARCH_X86
}
else : contains(QMAKE_HOST.arch, i686) {
    DEFINES += ARCH=SatArch::ARCH_X86
}
else : contains(QMAKE_HOST.arch, x86_64) {
    DEFINES += ARCH=SatArch::ARCH_AMD64
}
else : contains(QMAKE_HOST.arch, amd64) {
    DEFINES += ARCH=SatArch::ARCH_AMD64
}
else : contains(QMAKE_HOST.arch, aarch64) {
    message("SAT-ARCH: aarch64")
    DEFINES += ARCH=SatArch::ARCH_ARM64
}
else : contains(QMAKE_HOST.arch, armv7l) {
    DEFINES += ARCH=SatArch::ARCH_ARM
}
else : contains(QMAKE_HOST.arch, armv6l) {
    DEFINES += ARCH=SatArch::ARCH_ARM
}
else : {
    DEFINES += ARCH=SatArch::ARCH_UNKNOWN
}

DEFINES += RAM_SZ=0

#DEFINES += ENABLE_

