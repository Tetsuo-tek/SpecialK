contains(DEFINES, "ENABLE_QTAPP") : {
    INCLUDEPATH += $$PWD/../LibSkCore
    include($$PWD/../LibSkCore/LibSkCore.pri)
    include($$PWD/../LibSkCore/module-src.pri)

    CONFIG += console
    CONFIG -= link_pkgconfig

    DEFINES += ENABLE_AES
    DEFINES += ENABLE_XML
    DEFINES -= ENABLE_STATGRAB

    LIBS    -= -lstatgrab
    LIBS    -= -lhiredis
    LIBS    -= -lpthread
}

contains(DEFINES, "ENABLE_QT_FLOWPORT") : {
    INCLUDEPATH += $$PWD/../LibSkFlat
    include($$PWD/../LibSkFlat/module-src.pri)
}

CONFIG += c++17
CONFIG += qt

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

equals(TEMPLATE , lib) : {
    message("Template: LIBRARY")
}

else : {
    message("Template: APPLICATION")
    target.path = /usr/local/bin
    INSTALLS += target
}

message("OS: $$QMAKE_HOST.os")
message("CPUs: $$QMAKE_HOST.cpu_count")
message("HOSTNAME: $$QMAKE_HOST.name")
message("SYS VERS: $$QMAKE_HOST.version")
message("SYS VERS STRING: $$QMAKE_HOST.version_string")
message("ARCH: $$QMAKE_HOST.arch")
message("Config: $$CONFIG")
message("IncludePath: $$INCLUDEPATH")
message("Defines: $$DEFINES")
message("Libs: $$LIBS")
