#include "qtskappctrl.h"

#if defined(ENABLE_QTAPP)

#include <QDebug>
#include <QCoreApplication>

SkApp *skApp = nullptr;

QtSkAppCtrl::QtSkAppCtrl(int argc, char *argv[])
{
    setObjectName("QtSkAppCtrl");
    setTimerType(Qt::PreciseTimer);
    closed = false;
    skApp = new SkApp(argc, argv);
    logger->enableEscapes(false);
}

void QtSkAppCtrl::init(ulong fastTickInterval, ulong slowTickInterval)
{
    AssertKiller(fastTickInterval < 1000);
    setInterval(fastTickInterval/1000);

    connect(this, SIGNAL(timeout()), this, SLOT(tick()), Qt::DirectConnection);
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(aboutToClose()), Qt::DirectConnection);

    skApp->init(fastTickInterval, slowTickInterval, SK_FREELOOP, false);
    QtObjectMessage("Initialized");
}

void QtSkAppCtrl::launch()
{
    skApp->exec(nullptr, false);
    QTimer::start();
    QtObjectMessage("started");
}

void QtSkAppCtrl::tick()
{
    if (!skApp->loopStep(closed))
        qApp->quit();
}

void QtSkAppCtrl::aboutToClose()
{
    QtObjectWarning("Qt application is going to die ..");

    disconnect(this, SIGNAL(timeout()), this, SLOT(tick()));
    disconnect(qApp, SIGNAL(aboutToQuit()), this, SLOT(aboutToClose()));

    if (!closed)
    {
        skApp->quit();
        while(skApp->loopStep(closed));
    }

    QtObjectMessage("Expired");
}

#endif
