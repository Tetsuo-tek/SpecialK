#ifndef QTSKAPPCTRL_H
#define QTSKAPPCTRL_H

#if defined(ENABLE_QTAPP)

#include <QTimer>

#include "Core/App/skapp.h"

#define QtObjectMessage(LOGITEM)              SK_LOG(SkLogType::Msg,          LOGITEM, this, qobjMsg)
#define QtObjectWarning(LOGITEM)              SK_LOG(SkLogType::Wrn,          LOGITEM, this, qobjMsg)
#define QtObjectError(LOGITEM)                SK_LOG(SkLogType::Err,          LOGITEM, this, qobjMsg)
#define QtObjectDebug(LOGITEM)                SK_LOG(SkLogType::Dbg,          LOGITEM, this, qobjMsg)
#define QtObjectPlusDebug(LOGITEM)            SK_LOG(SkLogType::PlusDbg,      LOGITEM, this, qobjMsg)

#define QtObjectMessage_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::Msg,          LOGITEM, OBJ, qobjMsg)
#define QtObjectWarning_EXT(OBJ, LOGITEM)     SK_LOG(SkLogType::Wrn,          LOGITEM, OBJ, qobjMsg)
#define QtObjectError_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Err,          LOGITEM, OBJ, qobjMsg)
#define QtObjectDebug_EXT(OBJ, LOGITEM)       SK_LOG(SkLogType::Dbg,          LOGITEM, OBJ, qobjMsg)
#define QtObjectPlusDebug_EXT(OBJ, LOGITEM)   SK_LOG(SkLogType::PlusDbg,      LOGITEM, OBJ, qobjMsg)

class QtSkAppCtrl extends QTimer
{
    Q_OBJECT

    bool closed;

    public:
        explicit QtSkAppCtrl(int argc, char *argv[]);

        void init(ulong fastTickInterval=10000, ulong slowTickInterval=100000);
        void launch();

    private slots:
        void tick();
        void aboutToClose();

    signals:
        void fastTick();
};

#endif

#endif // QTSKAPPCTRL_H
