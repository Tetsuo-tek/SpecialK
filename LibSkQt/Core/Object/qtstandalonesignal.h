#ifndef QTSTANDALONESIGNAL_H
#define QTSTANDALONESIGNAL_H

#if defined(ENABLE_QTAPP)

#include <QObject>
#include "Core/Containers/skvariant.h"

class QtStandaloneSignal : public QObject
{
    Q_OBJECT

    public:
        explicit QtStandaloneSignal(QObject *parent=nullptr);

    signals:
        void pulse(SkVariantVector &params);
};

#endif

#endif // QTSTANDALONESIGNAL_H
