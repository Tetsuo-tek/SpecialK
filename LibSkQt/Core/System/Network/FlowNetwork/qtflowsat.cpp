#if defined(ENABLE_QTAPP)

#include "qtflowsat.h"
#include "Core/System/Network/FlowNetwork/skabstractflowpublisher.h"
#include "Core/System/Network/FlowNetwork/skabstractflowsubscriber.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtFlowSat::QtFlowSat(QObject *parent) : QObject{parent}
{
    async = nullptr;
    tickChan = -1;

    setObjectName("QtFlowSat");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


void QtFlowSat::setup(CStr *userName, CStr *password, CStr *address)
{
    robotAddress = address;
    user = userName;
    passwd = password;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::open()
{
    if (robotAddress.isEmpty() || user.isEmpty() || passwd.isEmpty())
        return false;

    if (async)
        close();

    async = new SkFlowAsync;
    async->setObjectName("QtFlowSat.Async");

    {
        SkAbstractListIterator<SkAbstractFlowSubscriber *> *itr = subscribersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->setClient(async);

        delete itr;
    }

    {
        SkAbstractListIterator<SkAbstractFlowPublisher *> *itr = publishersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->setClient(async);

        delete itr;
    }

    if (!async->connect(robotAddress.c_str())
        || !async->login(user.c_str(), passwd.c_str())
        || !async->addStreamingChannel(tickChan, FT_TICK, T_NULL, "Tick"))
    {
        async->destroyLater();
        async = nullptr;

        return false;
    }

    QtAttach(async, disconnected, this, SIGNAL(disconnected()),             Qt::DirectConnection);
    QtAttach(async, disconnected, this, SLOT(onDisconnection()),            Qt::DirectConnection);

    QtAttach(async, channelAdded,
             this, SLOT(onChannelAdded(SkVariantVector &)),                 Qt::DirectConnection);

    QtAttach(async, channelRemoved,
             this, SLOT(onChannelRemoved(SkVariantVector &)),               Qt::DirectConnection);

    QtAttach(async, channelHeaderSetup,
             this, SLOT(onChannelHeaderSetup(SkVariantVector &)),           Qt::DirectConnection);

    QtAttach(async, channelPublishStartRequest,
             this, SLOT(onChannelPublishStartRequest(SkVariantVector &)),   Qt::DirectConnection);

    QtAttach(async, channelPublishStopRequest,
             this, SLOT(onChannelPublishStopRequest(SkVariantVector &)),    Qt::DirectConnection);

    QtAttach(skApp->fastZone_SIG,   pulse,  this, SLOT(tick()),             Qt::DirectConnection);
    QtAttach(skApp->slowZone_SIG,   pulse,  this, SLOT(onSlowTick()),       Qt::DirectConnection);
    QtAttach(skApp->oneSecZone_SIG, pulse,  this, SLOT(onOneSecTick()),     Qt::DirectConnection);
    QtAttach(skApp->oneSecZone_SIG, pulse,  this, SLOT(checkInternals()),    Qt::DirectConnection);

    ready();
    onReady();

    return true;
}

void QtFlowSat::close()
{
    if (!async)
        return;

    {
        SkAbstractListIterator<SkAbstractFlowSubscriber *> *itr = subscribersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->unSetClient();

        delete itr;
    }

    {
        SkAbstractListIterator<SkAbstractFlowPublisher *> *itr = publishersHolder_ASYNC.iterator();

        while(itr->next())
            itr->item()->unSetClient();

        delete itr;
    }

    QtDetach(async, disconnected, this, SIGNAL(disconnected()));
    QtDetach(async, disconnected, this, SLOT(onDisconnection()));

    QtDetach(async, channelAdded,
             this, SLOT(onChannelAdded(SkVariantVector &)));

    QtDetach(async, channelRemoved,
             this, SLOT(onChannelRemoved(SkVariantVector &)));

    QtDetach(async, channelHeaderSetup,
             this, SLOT(onChannelHeaderSetup(SkVariantVector &)));

    QtDetach(async, channelPublishStartRequest,
             this, SLOT(onChannelPublishStartRequest(SkVariantVector &)));

    QtDetach(async, channelPublishStopRequest,
             this, SLOT(onChannelPublishStopRequest(SkVariantVector &)));

    QtDetach(skApp->fastZone_SIG,   pulse,  this, SLOT(tick()));
    QtDetach(skApp->slowZone_SIG,   pulse,  this, SLOT(onSlowTick()));
    QtDetach(skApp->oneSecZone_SIG, pulse,  this, SLOT(onOneSecTick()));

    if (async->isConnected())
    {
        async->close();
        disconnected();
    }

    async->destroyLater();
    async = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowSat::onChannelAdded(SkVariantVector &p)
{
    SkFlowChanID chanID = static_cast<SkFlowChanID>(p.first().toInt());
    onChannelAdded(chanID);
    channelAdded(chanID);
}

void QtFlowSat::onChannelRemoved(SkVariantVector &p)
{
    SkFlowChanID chanID = static_cast<SkFlowChanID>(p.first().toInt());
    onChannelRemoved(chanID);
    channelRemoved(chanID);
}

void QtFlowSat::onChannelHeaderSetup(SkVariantVector &p)
{
    SkFlowChanID chanID = static_cast<SkFlowChanID>(p.first().toInt());
    onChannelHeaderSetup(chanID);
    channelHeaderSetup(chanID);
}

void QtFlowSat::onChannelPublishStartRequest(SkVariantVector &p)
{
    SkFlowChanID chanID = static_cast<SkFlowChanID>(p.first().toInt());
    onChannelPublishStartRequest(chanID);
    channelPublishStartRequest(chanID);
}

void QtFlowSat::onChannelPublishStopRequest(SkVariantVector &p)
{
    SkFlowChanID chanID = static_cast<SkFlowChanID>(p.first().toInt());
    onChannelPublishStopRequest(chanID);
    channelPublishStopRequest(chanID);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowSat::tick()
{
    if (!async)
        return;

    while(async->nextData())
        onFlowDataCome(async->getCurrentData());

    onFastTick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowSat::checkInternals()
{
    if (checkChrono.stop() > 2.)
    {
        if (async && async->isConnected())
            async->checkService();

        checkChrono.start();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowSync *QtFlowSat::buildSyncClient()
{
    if (!async)
        return nullptr;

    return async->buildSyncClient();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::setCurrentDbName(CStr *dbName)
{
    if (!async)
        return false;

    return async->setCurrentDbName(dbName);
}

bool QtFlowSat::setVariable(CStr *key, const SkVariant &value, CStr *temporaryCurrentDbName)
{
    if (!async)
        return false;

    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->setVariable(key, value);

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->setVariable(key, value);

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

bool QtFlowSat::delVariable(CStr *key, CStr *temporaryCurrentDbName)
{
    if (!async)
        return false;

    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->delVariable(key);

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->delVariable(key);

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

bool QtFlowSat::flushall(CStr *temporaryCurrentDbName)
{
    if (!async)
        return false;

    if (SkString::isEmpty(temporaryCurrentDbName))
        return async->flushall();

    SkString holdCurrentDbName;

    if (!SkString::compare(temporaryCurrentDbName, async->getCurrentDbName()))
    {
        holdCurrentDbName = async->getCurrentDbName();
        setCurrentDbName(temporaryCurrentDbName);
    }

    bool ok = async->flushall();

    if (!holdCurrentDbName.isEmpty())
        setCurrentDbName(holdCurrentDbName.c_str());

    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime, CStr *udm)
{
    if (!async)
        return false;

    return async->addStreamingChannel(chanID, flow_t, data_t, name, mime, udm);
}

bool QtFlowSat::removeChannel(SkFlowChanID chanID)
{
    if (!async)
        return false;

    return async->removeChannel(chanID);
}

bool QtFlowSat::setChannelHeader(SkFlowChanID chanID, SkDataBuffer &data)
{
    if (!async)
        return false;

    return async->setChannelHeader(chanID, data);
}

bool QtFlowSat::setChannelProperties(SkFlowChanID chanID, SkArgsMap &props)
{
    if (!async)
        return false;

    return async->setChannelProperties(chanID, props);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::publish(SkFlowChanID chanID, int8_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, uint8_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, int16_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, uint16_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, int32_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, uint32_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, int64_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, uint64_t v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, float v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, double v)
{
    if (!async)
        return false;

    return async->publish(chanID, v);
}

bool QtFlowSat::publish(SkFlowChanID chanID, CStr *str)
{
    if (!async)
        return false;

    return async->publish(chanID, str);
}

bool QtFlowSat::publish(SkFlowChanID chanID, SkString &str)
{
    if (!async)
        return false;

    return async->publish(chanID, str);
}

bool QtFlowSat::publish(SkFlowChanID chanID, SkVariantList &l)
{
    if (!async)
        return false;

    return async->publish(chanID, l);
}

bool QtFlowSat::publish(SkFlowChanID chanID, SkArgsMap &m)
{
    if (!async)
        return false;

    return async->publish(chanID, m);
}

bool QtFlowSat::publish(SkFlowChanID chanID, CVoid *data, uint64_t sz)
{
    if (!async)
        return false;

    return async->publish(chanID, data, sz);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::isSubscribed(SkFlowChanID chanID)
{
    if (!async)
        return false;

    return async->isSubscribed(chanID);
}

bool QtFlowSat::subscribeChannel(SkFlowChannel *ch)
{
    if (!async)
        return false;

    return async->subscribeChannel(ch);
}

bool QtFlowSat::subscribeChannel(CStr *name, SkFlowChanID &chanID)
{
    if (!async)
        return false;

    return async->subscribeChannel(name, chanID);
}

bool QtFlowSat::unsubscribeChannel(SkFlowChanID chanID)
{
    if (!async)
        return false;

    return async->unsubscribeChannel(chanID);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowSat::setupSubscriber(SkAbstractFlowSubscriber *subscriber)
{
    if (async)
        subscriber->setClient(async);

    subscribersHolder_ASYNC << subscriber;
}

void QtFlowSat::setupPublisher(SkAbstractFlowPublisher *publisher)
{
    if (async)
        publisher->setClient(async);

    publishersHolder_ASYNC << publisher;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::containsChannel(CStr *name)
{
    if (!async)
        return false;

    return async->containsChannel(name);
}

bool QtFlowSat::containsChannel(SkFlowChanID chanID)
{
    if (!async)
        return false;

    return async->containsChannel(chanID);
}

void QtFlowSat::channels(SkStringList &names)
{
    if (!async)
        return;

    return async->channels(names);
}

SkFlowChannel *QtFlowSat::channel(CStr *name)
{
    if (!async)
        return nullptr;

    return async->channel(name);
}

SkFlowChannel *QtFlowSat::channel(SkFlowChanID chanID)
{
    if (!async)
        return nullptr;

    return async->channel(chanID);
}

int64_t QtFlowSat::channelID(CStr *name)
{
    if (!async)
        return -1;

    return async->channelID(name);
}

uint64_t QtFlowSat::channelsCount()
{
    if (!async)
        return 0;

    return async->channelsCount();
}

SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *QtFlowSat::channelsIterator()
{
    if (!async)
        return nullptr;

    return async->channelsIterator();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSat::isOpen()
{
    if (!async)
        return false;

    return async->isConnected();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
