#ifndef QTFLOWSAT_H
#define QTFLOWSAT_H

#if defined(ENABLE_QTAPP)

#include <Core/App/qtskappctrl.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>

class SkAbstractFlowSubscriber;
class SkAbstractFlowPublisher;

class QtFlowSat extends QObject
{
    Q_OBJECT

    SkFlowAsync *async;
    SkFlowChanID tickChan;

    SkString robotAddress;
    SkString user;
    SkString passwd;

    SkList<SkAbstractFlowSubscriber *> subscribersHolder_ASYNC;
    SkList<SkAbstractFlowPublisher *> publishersHolder_ASYNC;

    SkElapsedTime checkChrono;

    public:
        explicit QtFlowSat(QObject *parent=nullptr);

        void setup(CStr *userName, CStr *password, CStr *address);

        bool open();
        void close();

        SkFlowSync *buildSyncClient();

        bool setCurrentDbName(CStr *dbName);
        bool setVariable(CStr *key, const SkVariant &value, CStr *temporaryCurrentDbName="");
        bool delVariable(CStr *key, CStr *temporaryCurrentDbName="");
        bool flushall(CStr *temporaryCurrentDbName="");

        bool addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T data_t, CStr *name, CStr *mime=nullptr, CStr *udm=nullptr);
        bool removeChannel(SkFlowChanID chanID);
        bool setChannelHeader(SkFlowChanID chanID, SkDataBuffer &data);
        bool setChannelProperties(SkFlowChanID chanID, SkArgsMap &props);

        bool publish(SkFlowChanID chanID, int8_t v);
        bool publish(SkFlowChanID chanID, uint8_t v);
        bool publish(SkFlowChanID chanID, int16_t v);
        bool publish(SkFlowChanID chanID, uint16_t v);
        bool publish(SkFlowChanID chanID, int32_t v);
        bool publish(SkFlowChanID chanID, uint32_t v);
        bool publish(SkFlowChanID chanID, int64_t v);
        bool publish(SkFlowChanID chanID, uint64_t v);
        bool publish(SkFlowChanID chanID, float v);
        bool publish(SkFlowChanID chanID, double v);
        bool publish(SkFlowChanID chanID, CStr *str);
        bool publish(SkFlowChanID chanID, SkString &str);
        bool publish(SkFlowChanID chanID, SkVariantList &l);
        bool publish(SkFlowChanID chanID, SkArgsMap &m);
        bool publish(SkFlowChanID chanID, CVoid *data, uint64_t sz);

        bool isSubscribed(SkFlowChanID chanID);
        bool subscribeChannel(SkFlowChannel *ch);
        bool subscribeChannel(CStr *name, SkFlowChanID &chanID);
        bool unsubscribeChannel(SkFlowChanID chanID);

        void setupSubscriber(SkAbstractFlowSubscriber *subscriber);
        void setupPublisher(SkAbstractFlowPublisher *publisher);

        bool containsChannel(CStr *name);
        bool containsChannel(SkFlowChanID chanID);
        void channels(SkStringList &names);
        SkFlowChannel *channel(CStr *name);
        SkFlowChannel *channel(SkFlowChanID chanID);
        int64_t channelID(CStr *name);
        uint64_t channelsCount();
        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *channelsIterator();

        bool isOpen();

    protected:
        virtual void onReady()                                          {}

    signals:
        void ready();
        void disconnected();

        //FOR QML WRAPPER (chanID extracted from p)
        void channelAdded(SkFlowChanID);
        void channelRemoved(SkFlowChanID);
        void channelHeaderSetup(SkFlowChanID);
        void channelPublishStartRequest(SkFlowChanID);
        void channelPublishStopRequest(SkFlowChanID);

    protected slots:
        virtual void onFlowDataCome(SkFlowChannelData &)                {}

        //FOR SUBCLASSERs (chanID extracted from p)
        virtual void onChannelAdded(SkFlowChanID)                       {}
        virtual void onChannelRemoved(SkFlowChanID)                     {}
        virtual void onChannelHeaderSetup(SkFlowChanID)                 {}
        virtual void onChannelPublishStartRequest(SkFlowChanID)         {}
        virtual void onChannelPublishStopRequest(SkFlowChanID)          {}

        virtual void onFastTick()                                       {}
        virtual void onSlowTick()                                       {}
        virtual void onOneSecTick()                                     {}

        virtual void onDisconnection()                                  {}

    private slots:
        void tick();
        void checkInternals();

        //FOR SkSlot CONVERSION TO QtSlot
        void onChannelAdded(SkVariantVector &p);
        void onChannelRemoved(SkVariantVector &p);
        void onChannelHeaderSetup(SkVariantVector &p);
        void onChannelPublishStartRequest(SkVariantVector &p);
        void onChannelPublishStopRequest(SkVariantVector &p);
};

#endif

#endif // QTFLOWSAT_H
