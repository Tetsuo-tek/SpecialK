#if defined(ENABLE_QT_FLOWPORT)

#include "qtflowasync.h"
#include "Core/Containers/skarraycast.h"

#include <QLocalSocket>
#include <QTcpSocket>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtFlowAsync::QtFlowAsync(QObject *parent) : QtAbstractFlow(parent)
{
    wrk = nullptr;

    currentSubscribedGrabbing = nullptr;
    currentGrabbingDataSize = 0;

    lastResponse = FRSP_NORSP;
    lastTransactionSize = 0;

    maxDataQueueCount = 1000;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowAsync::setWorker(QObject *worker)
{
    wrk = worker;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtFlowSync *QtFlowAsync::buildSyncClient()
{
    QtFlowSync *sync = new QtFlowSync(this);

    if ((isTcpSocket() && !sync->tcpConnect(getTcpAddress(), getTcpPort()))
        || (isUnixSocket() && !sync->localConnect(getUnixPath()))
        || !sync->login(userName(), userToken()))
    {
        return nullptr;
    }

    return sync;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::checkService()
{
    if (!isConnected())
        return false;

    p->sendStartOfTransaction(FCMD_CHK_SERVICE);
    p->sendEndOfTransaction();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::addStreamingChannel(SkFlowChanID &chanID, SkFlow_T flow_t, SkVariant_T t, CStr *name, CStr *mime, CStr *udm)
{
    if (!isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    SkString chName(this->name);
    chName.append(".");
    chName.append(name);

    if (openingChannels.contains(chName))
    {
        QtObjectWarning("Channel is ALREADY in the opening mode: " << chName);
        return false;
    }

    openingChannels[chName] = &chanID;

    p->sendStartOfTransaction(FCMD_ADD_STREAMING_CHAN);

    SkVariantList l;
    l << flow_t << t << name;

    if (SkString::isEmpty(mime))
        l << "";
    else
        l << mime;

    if (SkString::isEmpty(udm))
        l << "";
    else
        l << udm;

    SkVariant v(l);
    SkString json;
    v.toJson(json);
    p->sendString(json);

    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to add a new Channel: " << name);

    //NO RESPONSE
    return true;
}

bool QtFlowAsync::addServiceChannel(SkFlowChanID &chanID, CStr *name)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    SkString chName(this->name);
    chName.append(".");
    chName.append(name);

    if (openingChannels.contains(chName))
    {
        QtObjectWarning("Channel is ALREADY in the opening mode: " << chName);
        return false;
    }

    openingChannels[chName] = &chanID;

    p->sendStartOfTransaction(FCMD_ADD_SERVICE_CHAN);

    SkVariantList l;
    l << name;

    SkVariant v(l);
    SkString json;
    v.toJson(json);
    p->sendString(json);

    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to ADD a new ServiceChannel: " << name);

    //NO RESPONSE
    return true;
}

bool QtFlowAsync::removeChannel(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        QtObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_DEL_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to remove a Channel: " << channelsIndexes[chanID]->name.c_str());
    //NO RESPONSE
    return true;
}

/*bool QtFlowAsync::getChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        QtObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_CHAN_HEADER);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to REMOVE a channel: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}*/

bool QtFlowAsync::setChannelHeader(SkFlowChanID &chanID, SkDataBuffer &b)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsIndexes.contains(chanID))
    {
        QtObjectWarning("Channel NOT found: " << chanID);
        return false;
    }

    p->sendStartOfTransaction(FCMD_SET_CHAN_HEADER);
    p->sendChanID(chanID);
    p->sendBuffer(b);
    p->sendEndOfTransaction();

    QtObjectMessage("Setup HEADER [" << b.size() << " B] for a channel: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}

bool QtFlowAsync::setChannelProperties(SkFlowChanID chanID, SkArgsMap &props)
{
    if (!containsChannel(chanID))
    {
        QtObjectError("ChanID NOT found: " << chanID);
        return false;
    }

    SkFlowChannel *ch = channel(chanID);

    SkString holdDbName;

    if (ch->name != currentDbName)
        holdDbName = currentDbName;

    setCurrentDbName(ch->name.c_str());

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = props.iterator();

    while(itr->next())
        setVariable(itr->item().key().c_str(), itr->item().value());

    delete itr;

    if (!holdDbName.isEmpty())
        setCurrentDbName(holdDbName.c_str());

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::attach(SkFlowChanID sourceID, SkFlowChanID targetID)
{
    p->sendStartOfTransaction(FCMD_ATTACH_CHAN);
    p->sendChanID(sourceID);
    p->sendChanID(targetID);
    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to ATTACH channels: " << channelsIndexes[sourceID]->name << " ->" << channelsIndexes[targetID]->name);
    //NO RESPONSE
    return true;
}

bool QtFlowAsync::detach(SkFlowChanID sourceID, SkFlowChanID targetID)
{
    p->sendStartOfTransaction(FCMD_DETACH_CHAN);
    p->sendChanID(sourceID);
    p->sendChanID(targetID);
    p->sendEndOfTransaction();

    QtObjectMessage("REQUESTED to DETACH channels: " << channelsIndexes[sourceID]->name << " ->" << channelsIndexes[targetID]->name);
    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val)
{
    p->sendStartOfTransaction(FCMD_EXEC_SERVICE_REQUEST);
    p->sendChanID(chanID);
    p->sendCStr(cmd);
    p->sendJSON(val);
    p->sendEndOfTransaction();

    QtObjectMessage("ServiceChannel REQUEST sent: " << channelsIndexes[chanID]->name);
    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::publish(SkFlowChanID chanID)
{return publish(chanID, nullptr, 0);}

bool QtFlowAsync::publish(SkFlowChanID chanID, int8_t v)
{return publish(chanID, &v, sizeof(int8_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, uint8_t v)
{return publish(chanID, &v, sizeof(uint8_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, int16_t v)
{return publish(chanID, &v, sizeof(int16_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, uint16_t v)
{return publish(chanID, &v, sizeof(uint16_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, int32_t v)
{return publish(chanID, &v, sizeof(int32_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, uint32_t v)
{return publish(chanID, &v, sizeof(uint32_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, int64_t v)
{return publish(chanID, &v, sizeof(int64_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, uint64_t v)
{return publish(chanID, &v, sizeof(uint64_t));}

bool QtFlowAsync::publish(SkFlowChanID chanID, float v)
{return publish(chanID, &v, sizeof(float));}

bool QtFlowAsync::publish(SkFlowChanID chanID, double v)
{return publish(chanID, &v, sizeof(double));}

bool QtFlowAsync::publish(SkFlowChanID chanID, CStr *str)
{return publish(chanID, str, SkString::size(str));}

bool QtFlowAsync::publish(SkFlowChanID chanID, SkString &str)
{return publish(chanID, str.c_str(), str.size());}

bool QtFlowAsync::publish(SkFlowChanID chanID, SkVariantList &l)
{
    SkString json;
    SkVariant v(l);
    v.toJson(json);
    return publish(chanID, json.c_str(), json.size());
}

bool QtFlowAsync::publish(SkFlowChanID chanID, SkAbstractMap<SkString, SkVariant> &m)
{
    SkString json;
    SkVariant v(m);
    v.toJson(json);
    return publish(chanID, json.c_str(), json.size());
}

bool QtFlowAsync::publish(SkFlowChanID chanID, CVoid *data, uint64_t sz)
{return publishPacketizedData(chanID, data, sz);}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::publishPacketizedData(SkFlowChanID chanID, CVoid *val, uint64_t sz)
{
    if (chanID < 0)
        return false;

    if (!isConnected())
    {
        QtObjectError("Cannot send protocol-transaction [isOpen: " << SkVariant::boolToString(false) << /*", canWrite: " << SkVariant::boolToString(canWrite) <<*/ "]");
        return false;
    }

    if (!channelsIndexes[chanID]->isPublishingEnabled)
        return false;

    p->sendStartOfTransaction(FCMD_PUBLISH);
    p->sendChanID(chanID);
    p->sendBuffer(val, sz);
    bool ok = p->sendEndOfTransaction();

    if (ok)
        channelDataPublished(chanID);

    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::isSubscribed(SkFlowChanID chanID)
{
    return subscribedChannels.contains(chanID);
}

bool QtFlowAsync::subscribeChannel(CStr *name, SkFlowChanID &chanID)
{
    if (channelsNames.contains(name))
    {
        SkFlowChannel *channel = channelsNames[name];
        chanID = channel->chanID;
        return subscribeChannel(channel);
    }

    if (subscribingChannels.contains(name))
    {
        QtObjectWarning("Channel is ALREADY in the subscribing mode: " << name);
        return false;
    }

    subscribingChannels[name] = &chanID;
    QtObjectMessage("REQUESTED to subscribe a Channel: " << name);

    return true;
}

bool QtFlowAsync::unsubscribeChannel(SkFlowChanID chanID)
{
    if (!subscribedChannels.contains(chanID))
    {
        ProtocolRecvError("Channel is NOT subscribed yet: " << chanID);
        return false;
    }

    return unsubscribeChannel(channelsIndexes[chanID]);
}

bool QtFlowAsync::subscribeChannel(SkFlowChannel *ch)
{
    if (!isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (subscribedChannels.contains(ch->chanID))
    {
        ProtocolRecvError("Channel is ALREADY subscribed: " << ch->name.c_str());
        return false;
    }

    p->sendStartOfTransaction(FCMD_SUBSCRIBE_CHAN);
    p->sendChanID(ch->chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE

    subscribedChannels.add(ch->chanID);
    QtObjectMessage("SUBSCRIBED a Channel: " << ch->name.c_str());

    return true;
}

bool QtFlowAsync::unsubscribeChannel(SkFlowChannel *ch)
{
    if (!isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (!subscribedChannels.contains(ch->chanID))
    {
        ProtocolRecvError("Channel is NOT subscribed yet: " << ch->name.c_str());
        return false;
    }

    p->sendStartOfTransaction(FCMD_UNSUBSCRIBE_CHAN);
    p->sendChanID(ch->chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE

    subscribedChannels.remove(ch->chanID);
    QtObjectMessage("UNSUBSCRIBED a Channel: " << ch->name.c_str());

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ULong QtFlowAsync::getDataCount()
{
    return channelsData.count();
}

bool QtFlowAsync::hasNextData()
{
    return !channelsData.isEmpty();
}

bool QtFlowAsync::nextData()
{
    if (!hasNextData())
        return false;

    SkFlowChannelData *d = channelsData.dequeue();
    currentData = *d;
    delete d;

    return true;
}

SkFlowChannelData &QtFlowAsync::getCurrentData()
{
    return currentData;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *QtFlowAsync::getCurrentDbName()
{
    return currentDbName.c_str();
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

qint64 QtFlowAsync::bytesToWrite()
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    return socket->bytesToWrite();
}

bool QtFlowAsync::flush()
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (isUnixSocket())
        return unxSck->flush();

    return tcpSck->flush();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtFlowAsync::onReadyRead()
{
    while (isConnected() && socket->bytesAvailable()>0)
        if (!analyze())
            break;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::analyze()
{
    //HERE WE GRAB THE RESPONSE
    if (lastResponse == FRSP_NORSP && socket->bytesAvailable() >= /*sizeof(uint32_t) + */sizeof(int16_t))
        lastTransactionSize = p->recvStartOfTransaction(lastResponse);

    /*if (lastResponse != FRSP_NORSP && lastTransactionSize > 0 && socket->bytesAvailable() < lastTransactionSize)
        return;*/

    //cout << "!!!!!!!!!! " << p->responseToString(lastResponse) << " " << socket->bytesAvailable()<< "\n";

    if (lastResponse == FRSP_CHK_FLOW)
    {
        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CURRENTDB_CHANGED)
    {
        uint64_t sz;
        p->recvSize(sz);

        currentDbName.clear();
        p->recvString(currentDbName, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_ADDED)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChannel *channel = new SkFlowChannel;
        channel->isPublishingEnabled = false;

        p->recvChanType(channel->chan_t);
        p->recvChanID(channel->chanID);

        uint64_t sz;
        p->recvSize(sz);

        if (sz && !p->recvString(channel->name, sz))
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            delete channel;
            return false;
        }

        p->recvSize(sz);

        if (sz && !p->recvString(channel->hashID, sz))
        {
            ProtocolRecvError("CANNOT RECV channel hashID");
            return false;
        }

        if (channelsIndexes.contains(channel->chanID))
        {
            ProtocolRecvError("Channel is ALREADY stored: " << channel->name.c_str() << " [ChanID: " << channel->chanID << "]");
            delete channel;
            return false;
        }

        if (channel->chan_t == StreamingChannel)
        {
            p->recvFlowType(channel->flow_t);
            p->recvVariantType(channel->data_t);

            p->recvSize(sz);

            if (sz && !p->recvString(channel->mime, sz))
            {
                ProtocolRecvError("CANNOT RECV channel->mime");
                delete channel;
                return false;
            }

            p->recvSize(sz);

            if (sz && !p->recvString(channel->udm, sz))
            {
                ProtocolRecvError("CANNOT RECV channel->udm");
                delete channel;
                return false;
            }

            p->recvSize(sz);

            SkString s;

            if (sz && !p->recvString(s, sz))
            {
                ProtocolRecvError("CANNOT RECV props->hasHeader");
                delete channel;
                return false;
            }

            SkVariant v;
            v.fromJson(s.c_str());
            channel->hasHeader = v.toBool();

            if (channel->hasHeader)
            {
                p->recvSize(sz);

                if (!p->recvBuffer(channel->header, sz))
                {
                    ProtocolRecvError("CANNOT RECV channel->mpPath");
                    delete channel;
                    return false;
                }
            }

            p->recvSize(sz);

            if (sz && !p->recvString(channel->mpPath, sz))
            {
                ProtocolRecvError("CANNOT RECV channel->mpPath");
                delete channel;
                return false;
            }
        }

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            delete channel;
            return false;
        }

        channelsNames[channel->name] = channel;
        channelsIndexes[channel->chanID] = channel;

        if (openingChannels.contains(channel->name))
        {
            SkFlowChanID *chanID = openingChannels[channel->name];
            *chanID = channel->chanID;
            openingChannels.remove(channel->name);
            openChannels.add(chanID);
        }

        if (channel->chan_t == StreamingChannel)
        {
            // BAH

            if (subscribingChannels.contains(channel->name))
            {
                *(subscribingChannels[channel->name]) = channel->chanID;
                subscribingChannels.remove(channel->name);
                subscribeChannel(channel);
            }

            QtObjectMessage("ADDED StreamingChannel: "
                            << "[ID: " << channel->chanID
                            << "; name: " << channel->name
                            << "; flow_T: " << QtFlowProto::flowTypeToString(channel->flow_t)
                            << "; val_T: " << SkVariant::variantTypeName(channel->data_t)
                            << "; mime_T: " << channel->mime
                            << "; udm: " << channel->udm
                            << "; http: " << channel->mpPath
                            << "]");
        }

        else if (channel->chan_t == ServiceChannel)
        {
            QtObjectMessage("ADDED ServiceChannel: "
                            << "[ID: " << channel->chanID
                            << "; name: " << channel->name
                            << "]");
        }

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;

        onChannelAdded(channel->chanID);
        channelAdded(channel->chanID);
    }

    else if (lastResponse == FRSP_CHANNEL_REMOVED)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            QtObjectError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];

        //
        SkFlowChanID *chID = nullptr;
        SkBinaryTreeVisit<SkFlowChanID *, nullptr_t> *itr = openChannels.iterator();

        while(itr->next())
            if (*(itr->item().key()) == channel->chanID)
            {
                QtObjectWarning("ChanID RESET [" << channel->chanID << "-1]: " << channel->name.c_str());
                chID = itr->item().key();
                *(chID) = -1;
                break;
            }

        delete itr;

        if (chID)
            openChannels.remove(chID);
        //
        if (channel->chan_t == StreamingChannel)
        {
            if (subscribedChannels.contains(channel->chanID))
            {
                subscribedChannels.remove(chanID);
                QtObjectWarning("AUTOMATIC UNSUBSCRIBE for a removed Channel: " << channel->name.c_str());
            }

            QtObjectMessage("StreamingChannel REMOVED: "
                            << "[ID: " << channel->chanID
                            << "; name: " << channel->name
                            << "]");
        }

        else
        {
            QtObjectMessage("ServiceChannel REMOVED: "
                            << "[ID: " << channel->chanID
                            << "; name: " << channel->name
                            << "]");
        }

        channelRemoved(channel->chanID);
        onChannelRemoved(channel->chanID);

        channelsIndexes.remove(channel->chanID);
        channelsNames.remove(channel->name);

        delete channel;
        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_HEADER)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];

        uint64_t sz;
        p->recvSize(sz);

        if (!sz)
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            return false;
        }

        channel->header.clear();

        if (sz && !p->recvBuffer(channel->header, sz))
        {
            ProtocolRecvError("CANNOT RECV channel->name");
            return false;
        }

        channel->hasHeader = !channel->header.isEmpty();

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        channelHeaderSetup(channel->chanID);
        onChannelHeaderSetup(channel->chanID);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SEND_RESPONSE_TO_REQUESTER)
    {
        uint64_t sz;
        p->recvSize(sz);

        SkVariant response;
        p->recvJSON(response, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        //cout << "#### " << response.toString() << "\n";

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SEND_REQUEST_TO_SERVICE)
    {
        SkFlowChanID chanID;
        p->recvChanID(chanID);

        uint64_t sz;
        p->recvSize(sz);

        SkString hashStr;
        p->recvString(hashStr, sz);

        p->recvSize(sz);

        SkString cmdName;
        p->recvString(cmdName, sz);

        p->recvSize(sz);

        SkVariant val;
        p->recvJSON(val, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        //cout << "#### " << response.toString() << "\n";

        /*if (!wrk)
        {
            QtObjectError("Worker is NULL");
            return;
        }

        SkArgsMap m;
        val.copyToMap(m);

        SkWorkerTransaction *t = new SkWorkerTransaction(this);
        t->setProperty("hash", hashStr);
        t->setProperty("chanID", chanID);
        t->setCommand(wrk, nullptr, onWrkEvaluatedCmd_SLOT, cmdName.c_str(), &m);
        t->invoke();*/

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_PUBLISH_START)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];
        channel->isPublishingEnabled = true;

        channelPublishStartRequest(channel->chanID);
        onChannelPublishStartRequest(channel->chanID);

        QtObjectMessage("Server has requested for channel-publish START: " << channel->name.c_str());

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_CHANNEL_PUBLISH_STOP)
    {
        if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        if (!channelsIndexes.contains(chanID))
        {
            ProtocolRecvError("Channel is NOT stored [ChanID: " << chanID << "]");
            return false;
        }

        SkFlowChannel *channel = channelsIndexes[chanID];
        channel->isPublishingEnabled = false;

        channelPublishStopRequest(channel->chanID);
        onChannelPublishStopRequest(channel->chanID);

        QtObjectMessage("Server has requested for channel-publish STOP: " << channel->name.c_str());

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    else if (lastResponse == FRSP_SUBSCRIBED_DATA)
    {
        /*if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
            return false;

        SkFlowChanID chanID;
        p->recvChanID(chanID);

        SkFlowChannelData *d = new SkFlowChannelData;
        d->chanID = chanID;

        uint64_t sz;
        p->recvSize(sz);

        // // //
        if (!sz)
        {
            delete d;
            return false;
        }
        // // //

        if (sz)
            p->recvBuffer(d->data, sz);

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        channelsData.enqueue(d);

        if (channelsData.count() > maxDataQueueCount)
        {
            SkFlowChannelData *d = channelsData.dequeue();
            delete d;

            QtObjectWarning("Removing channelData item because count > maxDataQueueCount: " << maxDataQueueCount);
        }

        channelDataAvailable(d->chanID);

        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;*/

        if (!currentSubscribedGrabbing)
        {
            if (socket->bytesAvailable() < (sizeof(SkFlowChanID) + sizeof(uint32_t)))
                return false;

            SkFlowChanID chanID;
            p->recvChanID(chanID);

            currentSubscribedGrabbing = new SkFlowChannelData;
            currentSubscribedGrabbing->chanID = chanID;

            p->recvSize(currentGrabbingDataSize);

            // // // PULSE DOES NOT PASS
            /*if (!currentGrabbingDataSize)
            {
                ProtocolRecvError("Subscribed arriving-data size CANNOT BE 0");
                delete currentSubscribedGrabbing;
                currentSubscribedGrabbing = nullptr;
                return false;
            }*/
            // // //
        }

        ULong sz = socket->bytesAvailable();

        if (sz < currentGrabbingDataSize)
        {
            if (!p->recvBuffer(currentSubscribedGrabbing->data, sz))
            {
                ProtocolRecvError("CANNOT GRAB channel->data");
                return false;
            }

            currentGrabbingDataSize -= currentSubscribedGrabbing->data.size();
            return false;
        }

        else
        {
            if (!p->recvBuffer(currentSubscribedGrabbing->data, currentGrabbingDataSize))
            {
                ProtocolRecvError("CANNOT GRAB channel->data");
                return false;
            }
        }

        if (!p->recvEndOfTransaction())
        {
            ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
            return false;
        }

        channelsData.enqueue(currentSubscribedGrabbing);

        if (channelsData.count() > maxDataQueueCount)
        {
            SkFlowChannelData *d = channelsData.dequeue();
            delete d;

            QtObjectWarning("Removing channelData item because count > maxDataQueueCount: " << maxDataQueueCount);
        }

        channelDataAvailable(currentSubscribedGrabbing->chanID);

        currentSubscribedGrabbing = nullptr;
        currentGrabbingDataSize = 0;
        lastResponse = FRSP_NORSP;
        lastTransactionSize = 0;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowAsync::onLogin()
{
    p->sendStartOfTransaction(FCMD_SET_ASYNC);
    p->sendEndOfTransaction();

    setCurrentDbName(userName());

    connect(socket, &QIODevice::readyRead, this, &QtFlowAsync::onReadyRead, Qt::QueuedConnection);
    return true;
}

void QtFlowAsync::onDisconnected()
{
    lastResponse = FRSP_NORSP;

    SkBinaryTreeVisit<SkFlowChanID *, nullptr_t> *itr = openChannels.iterator();

    while(itr->next())
        *(itr->item().key()) = -1;

    delete itr;

    openingChannels.clear();
    openChannels.clear();

    subscribingChannels.clear();
    subscribedChannels.clear();

    while(hasNextData())
    {
        SkFlowChannelData *channelData = channelsData.dequeue();
        delete channelData;
    }

    disconnect(socket, &QIODevice::readyRead, this, &QtFlowAsync::onReadyRead);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
