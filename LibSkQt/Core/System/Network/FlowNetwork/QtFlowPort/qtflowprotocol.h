#ifndef SKFLOWPROTOCOL_H
#define SKFLOWPROTOCOL_H

#if defined(ENABLE_QT_FLOWPORT)

#include <QIODevice>
#include <QBuffer>
#include <QDebug>

#include "Core/Containers/skvariant.h"
#include "Core/Containers/skstring.h"
#include "Core/Containers/skstringlist.h"
#include "Core/Containers/sktreemap.h"
#include "Core/Containers/skargsmap.h"
#include "Core/Containers/skarraycast.h"

#define FSPROTO_RW_TIMEOUT      5000

#include "Core/System/Network/FlowNetwork/skflowcommon.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// BEGIN skdefines.h (EXTRACT)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//typedef Short                             SkFlowChanID;

#define QtObjectError(LOG) \
    qDebug().nospace() << "[!!!!] -> " << LOG;

#define QtObjectWarning(LOG) \
    qDebug().nospace() << "[----] -> " << LOG;

#define QtObjectMessage(LOG) \
    qDebug().nospace() << "[****] -> " << LOG;

#define FakeSingleLine(CODE) \
    do {CODE} while(false)

#define ReadBinValue(DEV, TYPE, VAR, DFLT) \
    FakeSingleLine( \
        VAR = DFLT; \
        DEV->read(SkArrayCast::toChar(&VAR), sizeof(TYPE));)

#define WriteSuccess(BOOLVAR) \
    bool BOOLVAR = false; ArgNotUsed(BOOLVAR); BOOLVAR =

#define WriteBinValue(DEV, TYPE, VAR) \
    (DEV->write(SkArrayCast::toChar(&VAR), sizeof(TYPE)) > 0)

/*#define QtKillApp() \
    FakeSingleLine(QtObjectError("[!!!!] This is a Suicide Solution [exit(1)]"); ::exit(1);)

#define QtAssertKiller(COND) \
    FakeSingleLine( \
        if (COND) \
        { \
            QtObjectError("[!!!!] Killing by ASSERTION -> iff (" << #COND << "=true)"); \
            ::exit(1); \
        })*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// END skdefines.h
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class QtFlowProto extends QObject
{
    Q_OBJECT

    public:
        explicit QtFlowProto(QObject *parent);
        ~QtFlowProto() override;

        void setup(QIODevice *socketDevice);

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// SEND (buffered)

        void sendStartOfTransaction(SkFlowCommand cmd);
        void sendStartOfTransaction(SkFlowResponse rsp);
        void sendChanType(SkFlowChannel_T t);
        void sendFlowType(SkFlow_T t);
        void sendVariantType(SkVariant_T t);
        void sendChanID(SkFlowChanID chanID);
        void sendSize(uint32_t sz);
        void sendBool(bool v);
        void sendCStr(CStr *str);
        void sendString(SkString &str);
        void sendJSON(SkVariant &val);
        void sendVariant(SkVariant &val);
        void sendBuffer(SkDataBuffer &b);
        void sendBuffer(CVoid *data, uint64_t tempSize);
        void sendEndOfList();
        bool sendEndOfTransaction();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// RECV

        uint32_t recvStartOfTransaction(SkFlowCommand &cmd);
        uint32_t recvStartOfTransaction(SkFlowResponse &rsp);
        void recvChanType(SkFlowChannel_T &t);
        void recvFlowType(SkFlow_T &t);
        void recvVariantType(SkVariant_T &t);
        void recvChanID(SkFlowChanID &chanID);
        void recvSize(uint64_t &sz);
        bool recvBool();
        bool recvString(SkString &s, uint64_t &tempSize);
        bool recvJSON(SkVariant &val, uint64_t &tempSize);
        bool recvVariant(SkVariant &val, uint64_t &tempSize);
        bool recvBuffer(SkDataBuffer &b, uint64_t &tempSize);
        bool recvList(SkStringList &l);
        bool recvEndOfTransaction();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// BEGIN skabstractdevice.h (EXTRACT)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        bool writeInt8(int8_t val);
        bool writeInt16(int16_t val);
        bool writeInt32(int32_t val);
        bool writeInt64(int64_t val);

        bool writeUInt8(uint8_t val);
        bool writeUInt16(uint16_t val);
        bool writeUInt32(uint32_t val);
        bool writeUInt64(uint64_t val);

        int8_t readInt8();
        int16_t readInt16();
        int32_t readInt32();
        int64_t readInt64();

        uint8_t readUInt8();
        uint16_t readUInt16();
        uint32_t readUInt32();
        uint64_t readUInt64();

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// END skabstractdevice.h (EXTRACT)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        static CStr *commandToString(SkFlowCommand cmd);
        static SkFlowCommand commandToBin(CStr *cmd);

        static CStr *responseToString(SkFlowResponse rsp);
        static SkFlowResponse responseToBin(CStr *rsp);

        static CStr *flowTypeToString(SkFlow_T flow_t);
        static SkFlow_T flowTypeToBin(CStr *flow_t);

    protected:
        QIODevice *sck;
        QBuffer *sendingBufferDevice;
        QByteArray sendingBuffer;

        SkFlowCommand currentSendCmdTransaction;
        SkFlowResponse currentSendRspTransaction;
        SkFlowCommand currentRecvCmdTransaction;
        SkFlowResponse currentRecvRspTransaction;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWPROTOCOL_H
