#if defined(ENABLE_QT_FLOWPORT)

#include "qtflowsync.h"
#include <Core/Containers/skarraycast.h>

#include <QLocalSocket>
#include <QTcpSocket>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtFlowSync::QtFlowSync(QObject *parent) : QtAbstractFlow(parent)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSync::updateChannels()
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    if (!channelsNames.isEmpty())
        resetChannels();

    SkStringList chans;
    getChannelsList(chans);

    for(uint16_t i=0; i<chans.count(); i++)
    {
        SkFlowChannel *channel = new SkFlowChannel;
        channel->name = chans[i];
        getChannelProperties(channel);
        channelsNames[channel->name] = channel;
        channelsIndexes[channel->chanID] = channel;
    }

    onChannelsChanged();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSync::getChannelsList(SkStringList &channels)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_CHANS_LIST);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvList(channels))
    {
        ProtocolRecvError("CANNOT RECV channels");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool QtFlowSync::getChannelProperties(SkFlowChannel *channel)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    channel->isPublishingEnabled = false;

    p->sendStartOfTransaction(FCMD_GET_CHAN_PROPS);
    p->sendString(channel->name);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    p->recvChanID(channel->chanID);

    uint64_t sz;

    p->recvSize(sz);

    if (sz && !p->recvString(channel->udm, sz))
    {
        ProtocolRecvError("CANNOT RECV props->udm");
        delete channel;
        return false;
    }

    p->recvSize(sz);

    if (sz && !p->recvString(channel->mime, sz))
    {
        ProtocolRecvError("CANNOT RECV props->mime");
        delete channel;
        return false;
    }

    p->recvVariantType(channel->data_t);

    p->recvSize(sz);

    if (sz && !p->recvJSON(channel->min, sz))
    {
        ProtocolRecvError("CANNOT RECV props->min");
        delete channel;
        return false;
    }

    p->recvSize(sz);

    if (sz && !p->recvJSON(channel->max, sz))
    {
        ProtocolRecvError("CANNOT RECV props->max");
        delete channel;
        return false;
    }

    p->recvSize(sz);

    SkString s;

    if (sz && !p->recvString(s, sz))
    {
        ProtocolRecvError("CANNOT RECV props->hasHeader");
        delete channel;
        return false;
    }

    SkVariant v;
    v.fromJson(s.c_str());
    channel->hasHeader = v.toBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        delete channel;
        return false;
    }

    return true;
}

/*bool QtFlowSync::getChannelHeader(SkFlowChanID chanID, SkDataBuffer &buf)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    return false;
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSync::existsOptionalPairDb(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_EXISTS_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool exists = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return exists;
}

bool QtFlowSync::addDatabase(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_ADD_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool ok = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return ok;
}

bool QtFlowSync::getCurrentDbName(SkString &dbName)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_DATABASE);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz=0;
    p->recvSize(sz);

    if (!p->recvString(dbName, sz))
    {
        ProtocolRecvError("CANNOT RECV currentDbName");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool QtFlowSync::variablesKeys(SkStringList &keys)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLES_KEYS);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvList(keys))
    {
        ProtocolRecvError("CANNOT RECV keys");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool QtFlowSync::getAllVariables(SkArgsMap &db)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLES);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz = 0;
    p->recvSize(sz);

    db.clear();
    SkDataBuffer b;
    SkVariant v;

    if (sz)
    {
        p->recvBuffer(b, sz);
        v.setVal(b.toVoid(), b.size(), T_MAP);
        v.copyToMap(db);
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

bool QtFlowSync::existsVariable(CStr *key)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_EXISTS_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    bool exists = p->recvBool();

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return exists;
}

bool QtFlowSync::getVariable(CStr *key, SkVariant &value)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GET_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    uint64_t sz = 0;
    p->recvSize(sz);

    SkVariant_T t;
    p->recvVariantType(t);

    SkDataBuffer b;

    if (sz && !p->recvBuffer(b, sz))
    {
        ProtocolRecvError("CANNOT RECV value data");
        return false;
    }

    value.setVal(b.toVoid(), sz, t);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSync::dataGrabbingRegister(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_REGISTER_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool QtFlowSync::dataGrabbingUnRegister(SkFlowChanID chanID)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_UNREGISTER_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool QtFlowSync::grabLastChannelData(SkFlowChanID chanID, SkDataBuffer &data)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_GRAB_LASTDATA_CHAN);
    p->sendChanID(chanID);
    p->sendEndOfTransaction();

    socket->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    ULong sz;
    p->recvSize(sz);

    if(sz && !p->recvBuffer(data, sz))
    {
        ProtocolRecvError("CANNOT RECV recvBuffer");
        return false;
    }

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowSync::sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val)
{
    p->sendStartOfTransaction(FCMD_EXEC_SERVICE_REQUEST);
    p->sendChanID(chanID);
    p->sendCStr(cmd);
    p->sendJSON(val);
    p->sendEndOfTransaction();

    SkFlowCommand fwCmd;
    p->recvStartOfTransaction(fwCmd);

    ULong sz;
    p->recvSize(sz);
    p->recvJSON(val, sz);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    QtObjectMessage("ServiceChannel REQUEST sent: " << chanID);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
