#ifndef SKABSTRACTFLOW_H
#define SKABSTRACTFLOW_H

#if defined(ENABLE_QT_FLOWPORT)

#include "qtflowprotocol.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class QLocalSocket;
class QTcpSocket;

struct SkFlowChannel
{
    SkFlowChanID chanID;
    SkFlow_T flow_t;
    SkFlowChannel_T chan_t;
    SkVariant_T data_t;
    SkString name;
    SkString hashID;
    SkString udm;
    SkString mime;
    SkVariant min;
    SkVariant max;
    bool hasHeader;
    bool isPublishingEnabled;
    SkDataBuffer header;
    SkString mpPath;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define ProtocolRecvError(MSG) \
    do \
    { \
        QtObjectError("{FATAL} => " << MSG); \
        if (isConnected()) \
            close(); \
    } while(false)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class QtAbstractFlow extends QObject
{
    Q_OBJECT

    public:
        ~QtAbstractFlow() override;

        bool localConnect(CStr *path);
        bool tcpConnect(CStr *address, int16_t port);

        //The socket must be connected first
        //bool open(QIODevice *dev);
        void close();

        bool login(CStr *userName, CStr *userToken);

        bool setCurrentDbName(CStr *dbName);
        bool setVariable(CStr *key, const SkVariant &value);
        bool delVariable(CStr *key);
        bool flushall();

        bool isUnixSocket();
        bool isTcpSocket();
        CStr  *getUnixPath();
        CStr  *getTcpAddress();
        int16_t getTcpPort();

        bool isConnected();
        bool isAuthorized();

        bool containsChannel(CStr *name);
        bool containsChannel(SkFlowChanID chanID);
        void channels(SkStringList &names);
        SkFlowChannel *channel(CStr *name);
        SkFlowChannel *channel(uint64_t chanID);
        int64_t channelID(CStr *name);
        uint64_t channelsCount();

        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *channelsIterator();

        CStr *userName();
        CStr *userToken();

    public slots:
        void onDisconnect();

    signals:
        void connected();
        void disconnected();

    protected:
        explicit QtAbstractFlow(QObject *parent);

        QtFlowProto *p;
        QIODevice *socket;
        QTcpSocket *tcpSck;
        QLocalSocket *unxSck;
        SkString unixPath;
        SkString tcpAddress;
        int16_t tcpPort;
        SkString name;
        SkString token;
        bool auth;

        SkTreeMap<SkString, SkFlowChannel *> channelsNames;
        SkTreeMap<SkFlowChanID, SkFlowChannel *> channelsIndexes;

        virtual bool onOpenUnixSocket() {return true;}
        virtual bool onOpenTcpSocket()  {return true;}

        virtual bool onLogin()          {return true;}

        virtual void onOpen()           {}
        virtual void onClose()          {}
        virtual void onDisconnected()   {}

        bool open();
        void resetChannels();//automatically executed on open if there are channels
};

#endif

#endif // SKABSTRACTFLOW_H
