#if defined(ENABLE_QT_FLOWPORT)

#include "qtflowprotocol.h"
#include <QAbstractSocket>

/*static CStr *txtCommands[] =
    {
        "NOCMD",

        "LOGIN",

        "SET_ASYNC",
        "CHK_SERVICE",

        "GET_CHANS_LIST",
        "GET_CHAN_PROPS",

        //"GET_CHAN_HEADER",
        "SET_CHAN_HEADER",

        "EXISTS_DATABASE",
        "ADD_DATABASE",
        "SET_DATABASE",
        "GET_DATABASE",
        "GET_VARIABLES_KEYS",
        "GET_VARIABLES",
        "GET_VARIABLES_JSON",
        "EXISTS_VARIABLE",
        "SET_VARIABLE",
        "SET_VARIABLE_JSON",
        "GET_VARIABLE",
        "GET_VARIABLE_JSON",
        "DEL_VARIABLE",
        "FLUSHALL",

        "ADD_STREAMING_CHAN",
        "ADD_SERVICE_CHAN",
        "DEL_CHAN",

        "ATTACH_CHAN",
        "DETACH_CHAN",

        "EXEC_SERVICE_REQUEST",
        "RETURN_SERVICE_RESPONSE",

        "GRAB_REGISTER_CHAN",
        "GRAB_UNREGISTER_CHAN",
        "GRAB_LASTDATA_CHAN",

        "PUBLISH",

        "SUBSCRIBE",
        "UNSUBSCRIBE",

        "QUIT"
};

static CStr *txtResponses[] =
    {
        "NORSP",

        "CHK_FLOW",

        "CURRENTDB_CHANGED",

        "CHANNEL_ADDED",
        "CHANNEL_REMOVED",
        "CHANNEL_HEADER",

        "SEND_REQUEST_TO_SERVICE",
        "SEND_RESPONSE_TO_REQUESTER",

        "CHANNEL_PUBLISH_START",
        "CHANNEL_PUBLISH_STOP",

        "SUBSCRIBED_DATA",

        "OK",
        "KO"
};

static CStr *txtFlow_t[] =
    {
        "BLOB",

        "TICK",
        "EVENTS",
        "PAIRS",
        "LOGS",
        "DATETIME_CLOCK",

        "GPIO_ANALOG_INPUT",
        "GPIO_ANALOG_OUTPUT",
        "GPIO_DIGITAL_INPUT",
        "GPIO_DIGITAL_OUTPUT",

        "CTRL_PULSE",
        "CTRL_SWITCH",
        "CTRL_PWM",
        "CTRL_JOY",

        "AUDIO_DATA",
        "AUDIO_PREVIEW_DATA",
        "AUDIO_FFT",
        "AUDIO_VUMETER",
        "AUDIO_CLIPPING",
        "AUDIO_CTRL_VOLUME",
        "AUDIO_CTRL_GAIN",
        "AUDIO_CTRL_BASS",
        "AUDIO_CTRL_MIDDLE",
        "AUDIO_CTRL_TREBLE",

        "SPEECH_TO_TEXT",
        "TEXT_TO_SPEECH",

        "VIDEO_DATA",
        "VIDEO_PREVIEW_DATA",
        "VIDEO_CTRL_FADE",
        "VIDEO_CTRL_INTENSITY",
        "VIDEO_CTRL_HUE",
        "VIDEO_CTRL_SATURATION",
        "VIDEO_CTRL_SHARPNESS",
        "VIDEO_CTRL_MONOCHROMATIC",
        "VIDEO_CTRL_FPS",
        "VIDEO_CTRL_COMPRESSION",

        "CV_MOVEMENT",
        "CV_EDGES",
        "CV_GESTURES",
        "CV_POSTURE",
        "CV_BONES",
        "CV_CLOUD_3DPOINTS",
        "CV_OBJECT_DETECTED_FRAME",
        "CV_OBJECT_DETECTED_BOX",
        "CV_OBJECT_RECOGNIZED_IDENTITY",

        "MULTIMEDIA_DATA",
        "MULTIMEDIA_PREVIEW_DATA"
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

static SkTreeMap<SkString, SkFlowCommand> cmdTxtToBin;
static SkTreeMap<SkString, SkFlowResponse> rspTxtToBin;
static SkTreeMap<SkString, SkFlow_T> flowTxtToBin;

struct SkFlowProtoInit
{
    SkFlowProtoInit()
    {
        int max = FCMD_QUIT+1;

        for(int cmd=0; cmd<max; cmd++)
            cmdTxtToBin[txtCommands[cmd]] = static_cast<SkFlowCommand>(cmd);

        max = FRSP_KO+1;

        for(int rsp=0; rsp<max; rsp++)
            rspTxtToBin[txtResponses[rsp]] = static_cast<SkFlowResponse>(rsp);

        max = FT_MULTIMEDIA_PREVIEW_DATA+1;

        for(int ft=0; ft<max; ft++)
            flowTxtToBin[txtFlow_t[ft]] = static_cast<SkFlow_T>(ft);

        if (cmdTxtToBin.count() != FCMD_QUIT+1)
        {
            cout << "\033[1;31m\n!!!Flow PROTOCOL commands NOT valid! "
                 << "[bins: " << (FCMD_QUIT+1) << "; txt: " << cmdTxtToBin.count() <<"]\n\033[0m";
            KillApp();
        }

        if (rspTxtToBin.count() != FRSP_KO+1)
        {
            cout << "\033[1;31m\n!!!Flow PROTOCOL responses NOT valid! "
                 << "[bins: " << (FRSP_KO+1) << "; txt: " << rspTxtToBin.count() <<"]\n\033[0m";
            KillApp();
        }

        if (flowTxtToBin.count() != FT_MULTIMEDIA_PREVIEW_DATA+1)
        {
            cout << "\033[1;31m\n!!!Flow_T NOT valid! "
                 << "[bins: " << (FT_MULTIMEDIA_PREVIEW_DATA+1) << "; txt: " << flowTxtToBin.count() <<"]\n\033[0m";
            KillApp();
        }
    }
};

static SkFlowProtoInit protoInit;*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtFlowProto::QtFlowProto(QObject *parent) : QObject(parent)
{
    currentSendCmdTransaction = FCMD_NOCMD;
    currentSendRspTransaction = FRSP_NORSP;

    currentRecvCmdTransaction = FCMD_NOCMD;
    currentRecvRspTransaction = FRSP_NORSP;

    sck = nullptr;
    sendingBufferDevice = nullptr;
}

QtFlowProto::~QtFlowProto()
{
    if (sendingBufferDevice->isOpen())
        sendingBufferDevice->close();
}

void QtFlowProto::setup(QIODevice *socketDevice)
{
    sck = socketDevice;

    //sendingBuffer.setObjectName(this, "TxBuffer");
    sendingBufferDevice = new QBuffer(sck);
    //sendingBufferDevice->setObjectName(this,"TxBufferDev");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// SEND

void QtFlowProto::sendStartOfTransaction(SkFlowCommand cmd)
{
    if (sendingBufferDevice->isOpen())
    {
        QtObjectError("CMD transaction ALREADY open: " << commandToString(currentSendCmdTransaction));
        return;
    }

    sendingBufferDevice->setBuffer(&sendingBuffer);
    sendingBufferDevice->open(QBuffer::WriteOnly);

    currentSendCmdTransaction = cmd;
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint16_t, cmd);
}

void QtFlowProto::sendStartOfTransaction(SkFlowResponse rsp)
{
    if (sendingBufferDevice->isOpen())
    {
        QtObjectError("RSP transaction ALREADY open: " << responseToString(currentSendRspTransaction));
        return;
    }

    sendingBufferDevice->setBuffer(&sendingBuffer);
    sendingBufferDevice->open(QBuffer::WriteOnly);

    currentSendRspTransaction = rsp;
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint16_t, rsp);
}

void QtFlowProto::sendChanType(SkFlowChannel_T t)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint8_t, t);
}

void QtFlowProto::sendFlowType(SkFlow_T t)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint16_t, t);
}

void QtFlowProto::sendVariantType(SkVariant_T t)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint16_t, t);
}

void QtFlowProto::sendChanID(SkFlowChanID chanID)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, SkFlowChanID, chanID);
}

void QtFlowProto::sendSize(uint32_t sz)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint32_t, sz);
}

void QtFlowProto::sendBool(bool v)
{
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint8_t, v);
}

void QtFlowProto::sendCStr(CStr *str)
{
    uint32_t sz = strlen(str);
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(str, sz);
}

void QtFlowProto::sendString(SkString &str)
{
    uint64_t sz = str.size();
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(str.c_str(), sz);
}

void QtFlowProto::sendJSON(SkVariant &val)
{
    SkString str;
    val.toJson(str);
    sendString(str);
}

void QtFlowProto::sendVariant(SkVariant &val)
{
    uint64_t sz = val.size();
    sendSize(sz);
    sendVariantType(val.variantType());

    if (!sz)
        return;

    sendingBufferDevice->write(val.data(), sz);
}

void QtFlowProto::sendBuffer(SkDataBuffer &b)
{
    uint64_t sz = b.size();
    sendSize(sz);

    if (!sz)
        return;

    sendingBufferDevice->write(b.data(), sz);
}

void QtFlowProto::sendBuffer(CVoid *data, uint64_t tempSize)
{
    sendSize(tempSize);

    if (!tempSize)
        return;

    sendingBufferDevice->write(static_cast<CStr *>(data), tempSize);
}

void QtFlowProto::sendEndOfList()
{
    uint32_t val = 0;
    WriteSuccess(ok) WriteBinValue(sendingBufferDevice, uint32_t, val);
}

bool QtFlowProto::sendEndOfTransaction()
{
    if (!sendingBufferDevice->isOpen())
    {
        QtObjectError("Transaction is NOT open");
        return false;
    }

    bool isConnected = sck->isOpen();

    if (!isConnected)
    {
        QtObjectError("CANNOT send protocol-transaction [isOpen: " << SkVariant::boolToString(isConnected) << "]");
        return false;
    }

    uint32_t val = 0;
    WriteSuccess(writeOk) WriteBinValue(sendingBufferDevice, uint32_t, val);
    sendingBufferDevice->close();

    bool ok = (sck->write(sendingBuffer) == sendingBuffer.size());

    sendingBuffer.clear();
    currentSendCmdTransaction = FCMD_NOCMD;
    currentSendRspTransaction = FRSP_NORSP;

    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// RECV

uint32_t QtFlowProto::recvStartOfTransaction(SkFlowCommand &cmd)
{
    if (currentRecvCmdTransaction != FCMD_NOCMD)
        QtObjectError("Another Cmd transaction is open: " << commandToString(currentRecvCmdTransaction));

    uint32_t transactionSz = 0;//sck->readUInt32();

    ReadBinValue(sck, uint16_t, cmd, FCMD_NOCMD);

    currentRecvCmdTransaction = cmd;
    return transactionSz-sizeof(uint16_t);
}

uint32_t QtFlowProto::recvStartOfTransaction(SkFlowResponse &rsp)
{
    if (currentRecvRspTransaction != FRSP_NORSP)
        QtObjectError("Another Rsp transaction is open: " << responseToString(currentRecvRspTransaction));

    uint32_t transactionSz = 0;//sck->readUInt32();

    ReadBinValue(sck, uint16_t, rsp, FRSP_NORSP);

    currentRecvRspTransaction = rsp;
    return transactionSz-sizeof(uint16_t);
}

void QtFlowProto::recvChanType(SkFlowChannel_T &t)
{
    ReadBinValue(sck, uint8_t, t, ChannelNotValid);
}

void QtFlowProto::recvFlowType(SkFlow_T &t)
{
    ReadBinValue(sck, uint16_t, t, FT_BLOB);
}

void QtFlowProto::recvVariantType(SkVariant_T &t)
{
    ReadBinValue(sck, uint16_t, t, T_NULL);
}

void QtFlowProto::recvChanID(SkFlowChanID &chanID)
{
    ReadBinValue(sck, SkFlowChanID, chanID, -1);
}

void QtFlowProto::recvSize(uint64_t &sz)
{
    //IT IS RIGHT TO BE 32
    ReadBinValue(sck, uint32_t, sz, 0);
}

bool QtFlowProto::recvBool()
{
    bool b = false;
    ReadBinValue(sck, uint8_t, b, false);
    return b;
}
bool QtFlowProto::recvString(SkString &s, uint64_t &tempSize)
{
    if (tempSize)
    {
        //sck->waitForReadyRead(FSPROTO_RW_TIMEOUT);

        char str[tempSize+1];
        int readCount = sck->read(str, tempSize);

        if (readCount <= 0)
        {
            str[0] = '\0';
            return false;
        }

        str[readCount] = '\0';
        s.append(str);
        return true;
    }

    return false;
}

bool QtFlowProto::recvJSON(SkVariant &val, uint64_t &tempSize)
{
    SkString s;

    if (!recvString(s, tempSize))
        return false;

    //val = s;
    return val.fromJson(s.c_str());
}

bool QtFlowProto::recvVariant(SkVariant &val, uint64_t &tempSize)
{
    SkVariant_T t;
    recvVariantType(t);

    SkDataBuffer b;

    if (!tempSize || !recvBuffer(b, tempSize))
        return false;

    val.setVal(b.toVoid(), tempSize, t);
    return true;
}

bool QtFlowProto::recvBuffer(SkDataBuffer &b, uint64_t &tempSize)
{
    if (tempSize)
    {
        char buf[tempSize];
        int readCount = sck->read(buf, tempSize);

        if (readCount <= 0)
            return false;

        tempSize -= readCount;
        b.append(b, readCount);
        return true;
    }

    return false;
}

bool QtFlowProto::recvList(SkStringList &l)
{
    uint64_t sz=0;

    do
    {
        recvSize(sz);

        if (sz)
        {
            SkString item;

            if (!recvString(item, sz))
                return false;

            l << item;
        }

    } while(sz>0);

    return true;
}

bool QtFlowProto::recvEndOfTransaction()
{
    uint32_t ret = 1;
    ReadBinValue(sck, uint32_t, ret, 1);

    //MUST BE 0 TO BE OK
    if (ret)
    {
        QtObjectError("FAILED to RECV EndOfTransaction [CurrentRsp: " << responseToString(currentRecvRspTransaction) << ", ret: "<< ret << "!=0]");
        return false;
    }

    currentRecvCmdTransaction = FCMD_NOCMD;
    currentRecvRspTransaction = FRSP_NORSP;

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// BEGIN skabstractdevice.h (EXTRACT)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtFlowProto::writeInt8(int8_t val)
{WriteSuccess(ok) WriteBinValue(sck, int8_t, val); return ok;}

bool QtFlowProto::writeInt16(int16_t val)
{WriteSuccess(ok) WriteBinValue(sck, int16_t, val); return ok;}

bool QtFlowProto::writeInt32(int32_t val)
{WriteSuccess(ok) WriteBinValue(sck, int32_t, val); return ok;}

bool QtFlowProto::writeInt64(int64_t val)
{WriteSuccess(ok) WriteBinValue(sck, int64_t, val); return ok;}

bool QtFlowProto::writeUInt8(uint8_t val)
{WriteSuccess(ok) WriteBinValue(sck, uint8_t, val); return ok;}

bool QtFlowProto::writeUInt16(uint16_t val)
{WriteSuccess(ok) WriteBinValue(sck, uint16_t, val); return ok;}

bool QtFlowProto::writeUInt32(uint32_t val)
{WriteSuccess(ok) WriteBinValue(sck, uint32_t, val); return ok;}

bool QtFlowProto::writeUInt64(uint64_t val)
{WriteSuccess(ok) WriteBinValue(sck, uint64_t, val); return ok;}

int8_t QtFlowProto::readInt8()
{int8_t val; ReadBinValue(sck, int8_t, val, 0); return val;}

int16_t QtFlowProto::readInt16()
{int16_t val; ReadBinValue(sck, int16_t, val, 0); return val;}

int32_t QtFlowProto::readInt32()
{int32_t val; ReadBinValue(sck, int32_t, val, 0); return val;}

int64_t QtFlowProto::readInt64()
{int64_t val; ReadBinValue(sck, int64_t, val, 0); return val;}

uint8_t QtFlowProto::readUInt8()
{uint8_t val; ReadBinValue(sck, uint8_t, val, 0); return val;}

uint16_t QtFlowProto::readUInt16()
{uint16_t val; ReadBinValue(sck, uint16_t, val, 0); return val;}

uint32_t QtFlowProto::readUInt32()
{uint32_t val; ReadBinValue(sck, uint32_t, val, 0); return val;}

uint64_t QtFlowProto::readUInt64()
{uint64_t val; ReadBinValue(sck, uint64_t, val, 0); return val;}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// END skabstractdevice.h (EXTRACT)
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *QtFlowProto::commandToString(SkFlowCommand cmd)
{
    if (cmd > SkFlowCommand::FCMD_QUIT)
        return nullptr;

    //return txtCommands[cmd];
    return getTxtCommands()[cmd];
}

SkFlowCommand QtFlowProto::commandToBin(CStr *cmd)
{
    if (getCmdTxtToBin().contains(cmd))
        return FCMD_NOCMD;

    //return cmdTxtToBin[cmd];
    return getCmdTxtToBin()[cmd];
}

CStr *QtFlowProto::responseToString(SkFlowResponse rsp)
{
    if (rsp > SkFlowResponse::FRSP_KO)
        return nullptr;

    //return txtResponses[rsp];
    return getTxtResponses()[rsp];
}

SkFlowResponse QtFlowProto::responseToBin(CStr *rsp)
{
    //return rspTxtToBin[rsp];
    return getRspTxtToBin()[rsp];
}

CStr *QtFlowProto::flowTypeToString(SkFlow_T flow_t)
{
    //return txtFlow_t[flow_t];
    return getTxtFlow_t()[flow_t];
}

SkFlow_T QtFlowProto::flowTypeToBin(CStr *flow_t)
{
    //return flowTxtToBin[flow_t];
    return getFlowTxtToBin()[flow_t];
}

#endif
