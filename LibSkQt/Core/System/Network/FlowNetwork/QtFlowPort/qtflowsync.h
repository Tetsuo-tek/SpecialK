#ifndef SKFLOWSYNC_H
#define SKFLOWSYNC_H

#if defined(ENABLE_QT_FLOWPORT)

#include "qtabstractflow.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class QtFlowSync extends QtAbstractFlow
{
    Q_OBJECT

    public:
        QtFlowSync(QObject *parent);

        bool existsOptionalPairDb(CStr *dbName);
        bool addDatabase(CStr *dbName);
        bool getCurrentDbName(SkString &dbName);
        bool variablesKeys(SkStringList &keys);
        bool getAllVariables(SkArgsMap &db);
        bool existsVariable(CStr *key);
        bool getVariable(CStr *key, SkVariant &value);

        bool dataGrabbingRegister(SkFlowChanID chanID);
        bool dataGrabbingUnRegister(SkFlowChanID chanID);
        bool grabLastChannelData(SkFlowChanID chanID, SkDataBuffer &data);

        bool sendServiceRequest(SkFlowChanID chanID, CStr *cmd, SkVariant &val);

        bool updateChannels();

    protected:
        virtual void onChannelsChanged(){}

    private:
        bool getChannelsList(SkStringList &channels);
        bool getChannelProperties(SkFlowChannel *props);
        bool getChannelHeader(SkFlowChanID chanID, SkDataBuffer &buf);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // SKFLOWSYNC_H
