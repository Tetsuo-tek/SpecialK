#if defined(ENABLE_QT_FLOWPORT)

#include "qtabstractflow.h"
#include "qtflowsync.h"
#include <Core/Containers/skarraycast.h>

#include <QIODevice>
#include <QLocalSocket>
#include <QTcpSocket>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

QtAbstractFlow::QtAbstractFlow(QObject *parent) : QObject(parent)
{
    p = nullptr;

    socket = nullptr;
    tcpSck = nullptr;
    unxSck = nullptr;

    tcpPort = 0;
    auth = false;
}

QtAbstractFlow::~QtAbstractFlow()
{
    if (channelsCount())
        resetChannels();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtAbstractFlow::localConnect(CStr *path)
{
    if (socket && isConnected())
    {
        QtObjectError("Socket is ALREADY connected: " << socket);
        return false;
    }

    unxSck = new QLocalSocket(this);
    //sck->setObjectName(this, "LocalSocket");

    //connect(tcpSck, &QIODevice::connected, this, &QtAbstractFlow::onConnect);
    connect(unxSck, &QIODevice::aboutToClose, this, &QtAbstractFlow::onDisconnect, Qt::QueuedConnection);

    QString pathStr(path);
    unxSck->connectToServer(pathStr);//NOT CHECKING CONNECTION SUCCESS

    bool ok = unxSck->waitForConnected(FSPROTO_RW_TIMEOUT);

    if (!ok)
    {
        QtObjectError("CANNOT connect to FsService");

        unxSck->deleteLater();
        unxSck = nullptr;
        return false;
    }

    socket = unxSck;
    unixPath = path;
    tcpAddress.clear();
    tcpPort = 0;

    return open();
}

bool QtAbstractFlow::tcpConnect(CStr *address, int16_t port)
{
    if (socket && isConnected())
    {
        QtObjectError("Socket is ALREADY connected");
        return false;
    }

    tcpSck = new QTcpSocket(this);
    //sck->setObjectName(this, "TcpSocket");

    connect(tcpSck, &QAbstractSocket::disconnected, this, &QtAbstractFlow::onDisconnect, Qt::QueuedConnection);

    QString addressStr(address);
    tcpSck->connectToHost(addressStr, port);//NOT CHECKING CONNECTION SUCCESS

    bool ok = tcpSck->waitForConnected(FSPROTO_RW_TIMEOUT);

    if (!ok)
    {
        QtObjectError("CANNOT connect to FsService");
        tcpSck->deleteLater();
        tcpSck = nullptr;
        return false;
    }

    socket = tcpSck;
    unixPath.clear();
    tcpAddress = address;
    tcpPort = port;

    return open();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*bool QtAbstractFlow::open(QIODevice *dev)
{
    if (socket)
    {
        socket->deleteLater();
        socket = nullptr;
        auth = false;
    }

    socket = dev;
    return open();
}*/

bool QtAbstractFlow::open()
{
    if (channelsCount() > 0)
        resetChannels();

    if (isUnixSocket())
        onOpenUnixSocket();
    else
        onOpenTcpSocket();

    p = new QtFlowProto(this);
    //p->setObjectName(this, "Proto");
    p->setup(socket);

    onOpen();
    connected();

    QtObjectMessage("Connected");
    return true;
}

void QtAbstractFlow::close()
{
    if (!socket)
    {
        QtObjectError("Socket is NOT connected yet");
        return;
    }

    if (!isConnected())
        return;

    QtObjectMessage("Closing ..");

    if (isConnected())
    {
        if (isUnixSocket())
            unxSck->disconnectFromServer();

        else
            tcpSck->disconnectFromHost();

        //This produce a deadlock on slowmachine, using DummyModule or other modules with slow tick
        /*p->sendStartOfTransaction(FCMD_QUIT);
        p->sendEndOfTransaction();*/
    }

    onClose();
    p->deleteLater();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void QtAbstractFlow::onDisconnect()
{
    QtObjectMessage("Disconnected");
    onDisconnected();
    disconnected();

    if (isUnixSocket())
    {
        unxSck->deleteLater();
        unxSck = nullptr;
    }

    else
    {
        tcpSck->deleteLater();
        tcpSck = nullptr;
    }

    socket = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtAbstractFlow::login(CStr *userName, CStr *userToken)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    QtObjectWarning("Trying to login: " << userName << "@" << userToken);

    p->sendStartOfTransaction(FCMD_LOGIN);
    p->sendCStr(userName);
    p->sendCStr(userToken);
    p->sendEndOfTransaction();

    //sck->waitForBytesWritten(FSPROTO_RW_TIMEOUT);
    socket->waitForReadyRead(FSPROTO_RW_TIMEOUT);

    SkFlowCommand cmd;
    p->recvStartOfTransaction(cmd);

    if (!p->recvEndOfTransaction())
    {
        ProtocolRecvError("CANNOT RECV recvEndOfTransaction");
        return false;
    }

    name = userName;
    token = userToken;
    auth = true;

    return onLogin();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtAbstractFlow::setCurrentDbName(CStr *dbName)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_SET_DATABASE);
    p->sendCStr(dbName);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool QtAbstractFlow::setVariable(CStr *key, const SkVariant &value)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    SkVariant v(value);
    p->sendStartOfTransaction(FCMD_SET_VARIABLE);
    p->sendCStr(key);
    p->sendVariant(v);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool QtAbstractFlow::delVariable(CStr *key)
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_DEL_VARIABLE);
    p->sendCStr(key);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

bool QtAbstractFlow::flushall()
{
    if (!socket || !isConnected())
    {
        QtObjectError("Socket is NOT connected");
        return false;
    }

    p->sendStartOfTransaction(FCMD_FLUSHALL);
    p->sendEndOfTransaction();

    //NO RESPONSE
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtAbstractFlow::containsChannel(CStr *name)
{
    return channelsNames.contains(name);
}

bool QtAbstractFlow::containsChannel(SkFlowChanID chanID)
{
    return channelsIndexes.contains(chanID);
}

void QtAbstractFlow::channels(SkStringList &names)
{
    channelsNames.keys(names);
}

SkFlowChannel *QtAbstractFlow::channel(CStr *name)
{
    if (!channelsNames.contains(name))
    {
        QtObjectError("ChanName UNKNOWN: " << name);
        return nullptr;
    }

    return channelsNames[name];
}

SkFlowChannel *QtAbstractFlow::channel(uint64_t chanID)
{
    if (chanID >= channelsIndexes.count())
    {
        QtObjectError("ChanID UNKNOWN: " << chanID);
        return nullptr;
    }

    return channelsIndexes[chanID];
}

int64_t QtAbstractFlow::channelID(CStr *name)
{
    if (!channelsNames.contains(name))
    {
        QtObjectError("ChanName UNKNOWN: " << name);
        return -1;
    }

    return channelsNames[name]->chanID;
}

uint64_t QtAbstractFlow::channelsCount()
{
    return channelsNames.count();
}

SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *QtAbstractFlow::channelsIterator()
{
    return channelsIndexes.iterator();
}

void QtAbstractFlow::resetChannels()
{
    SkBinaryTreeVisit<SkString , SkFlowChannel*> *itr = channelsNames.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    channelsNames.clear();
    channelsIndexes.clear();
}

CStr *QtAbstractFlow::userName()
{
    return name.c_str();
}

CStr *QtAbstractFlow::userToken()
{
    return token.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool QtAbstractFlow::isUnixSocket()
{
    return (unxSck !=nullptr);
}

bool QtAbstractFlow::isTcpSocket()
{
    return (tcpSck != nullptr);
}

CStr  *QtAbstractFlow::getUnixPath()
{
    return unixPath.c_str();
}

CStr  *QtAbstractFlow::getTcpAddress()
{
    return tcpAddress.c_str();
}

int16_t QtAbstractFlow::getTcpPort()
{
    return tcpPort;
}

bool QtAbstractFlow::isConnected()
{
    if (!socket)
        return false;

    if (isUnixSocket())
        return unxSck->isOpen();

    return (tcpSck->state() == QAbstractSocket::ConnectedState);
}

bool QtAbstractFlow::isAuthorized()
{
    return auth;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
