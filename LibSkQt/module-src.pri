contains(DEFINES, ENABLE_QTAPP) : {
    HEADERS += \
        $$PWD/Core/App/qtskappctrl.h \
        $$PWD/Core/Object/qtstandalonesignal.h \
        $$PWD/Core/System/Network/FlowNetwork/qtflowsat.h

    SOURCES += \
        $$PWD/Core/App/qtskappctrl.cpp \
        $$PWD/Core/Object/qtstandalonesignal.cpp \
        $$PWD/Core/System/Network/FlowNetwork/qtflowsat.cpp
}

contains(DEFINES, ENABLE_QT_FLOWPORT) : {
    HEADERS += \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtabstractflow.h \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowasync.h \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowprotocol.h \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowsync.h

    SOURCES += \
        $$PWD/Core/App/qtskappctrl.cpp \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtabstractflow.cpp \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowasync.cpp \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowprotocol.cpp \
        $$PWD/Core/System/Network/FlowNetwork/QtFlowPort/qtflowsync.cpp
}


